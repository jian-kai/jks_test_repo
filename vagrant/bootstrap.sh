# config UTF-8 for server environment
sudo localedef -v -c -i en_US -f UTF-8 en_US.UTF-8 
sudo dpkg-reconfigure locales

echo 'Install curl for installation tool'
sudo apt-get install -y curl

# echo 'Install mysql'
# curl -OL https://dev.mysql.com/get/mysql-apt-config_0.8.3-1_all.deb
# sudo dpkg -i mysql-apt-config*
# sudo apt-get update
# rm mysql-apt-config*
# sudo apt-get install mysql-server
# systemctl status mysql
# mysql_secure_installation
# echo 'Checking mysql server'
# mysqladmin -u root -p version

echo 'Install python'
sudo apt-get install -y python2.7

echo 'Install nvm to handle nodejs version'
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.26.1/install.sh | bash
source ~/.nvm/nvm.sh


echo 'Install nodejs'
. $HOME/.nvm/nvm.sh && nvm install 8.0.0
. $HOME/.nvm/nvm.sh && nvm alias default 8.0.0
. $HOME/.nvm/nvm.sh && npm install -g @angular/cli@latest

echo 'Install Redis server'
sudo apt-get -y install redis-server

echo 'Install Mysql'
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get install -y mysql-server libmysqlclient-dev
mysql -uroot -proot -e "CREATE DATABASE shave2u  DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci”

echo 'Install packages needed for project'
cd /home/vagrant/frontend && npm install
cd /home/vagrant/backend && npm install



