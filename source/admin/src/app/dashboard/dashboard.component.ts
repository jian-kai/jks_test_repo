import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../core/services/dialog.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss', '../../assets/sass/table.scss']
})
export class DashboardComponent implements OnInit {
  public options: any = {};
  public orderStates: any;
  public ordersPending: any;
  public isLoading: boolean = true;
  public isLoadingOrders: boolean = true;
  public isLoadingChart: boolean = true;
  public countries: any;
  public countryId: number;
  public chartType: string = 'day';
  public currency: string;
  public saleInfo: any = {};
  public barChartOptions: any = {
    // scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    // scaleIntegersOnly: true,
    // interval: 2,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          // steps : 1,
          //  stepValue : 1,
          //  max : 100,

          // steps : 1,
          // stepValue : 1,
          // max : 100,
          // interval: 20,
          // beginAtZero: true,
          // stepSize: 1,
          // callback: function(value) {if (value % 1 === 0) {return value;}},
          min: 0,
          // callback: label => `${label}%`

        }
      }]
    }
  };
  public barChartLabels: string[];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartData: any[];
  // public hasDenied: boolean;
  public currentUser: object;

  lineChartColors: Array<any> = [
    { // Revenue
      backgroundColor: '#028b72',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    // { // Ala carte
    //   backgroundColor: '#028b72',
    //   borderColor: 'rgba(77,83,96,1)',
    //   pointBackgroundColor: 'rgba(77,83,96,1)',
    //   pointBorderColor: '#fff',
    //   pointHoverBackgroundColor: '#fff',
    //   pointHoverBorderColor: 'rgba(77,83,96,1)'
    // },
  ];

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,
    private _dialogService: DialogService) {
    // this.hasDenied = this.globalService.hasDenied();
    this.currentUser = this.storage.getCurrentUserItem();
  }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.globalService.getCountryList().then(data => {
      this.countries = data;
      this.countryId = this.countries[0].id;
      this.getSales();
      this.getOrderPendingList();
    });
  }

  // get order pending list
  getOrderPendingList() {
    console.log('this.countryId', this.countryId)
    let _country = this.countries.filter(country => country.id === +this.countryId)[0];
    // if (this.hasDenied) {
    //   this.options = { CountryId: this.currentUser['Country'].id };
    // } else {
      this.options = { CountryId: `${_country.id}`, type: `${this.chartType}` };
    // }
    let params = this.globalService._URLSearchParams(this.options);
    params.set('orderBy', '["createdAt_DESC"]');
    params.set('limit', '20');

    this._http._getList(`${this.appConfig.config.api.order_pendding}?` + params.toString()).subscribe(
      data => {
        this.ordersPending = data;
        if (this.ordersPending && this.ordersPending.rows && this.ordersPending.rows.length > 0) {
          this.ordersPending.rows.forEach(item => {
            item.currency = item.Country.currencyDisplay;
          });
        }
        // get Order States
        this.globalService.getOrderStates().then((data) => {
          this.orderStates = data;
        });
        this.isLoadingOrders = false;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error)
      }
    )
  }

  getSales() {
    let _country = this.countries.filter(country => country.id === +this.countryId)[0];
    // if (this.hasDenied) {
    //   this.options = { countryCode: this.currentUser['Country'].code };
    // } else {
      this.options = { countryCode: `${_country.code}`, type: `${this.chartType}` };
    // }
    if(this.allowAccessTo('reports/master')) {
      this.options.reportType = 'master';
    } else if(this.allowAccessTo('reports/appco')) {
      this.options.reportType = 'appco';
    } else if(this.allowAccessTo('reports/mo')) {
      this.options.reportType = 'mo';
      this.options.MarketingOfficeId = this.currentUser['MarketingOfficeId'];
    }
    let params = this.globalService._URLSearchParams(this.options);
    this.isLoadingChart = true;
    this._http._getList(`${this.appConfig.config.api.sales_reports}/summary?` + params.toString()).subscribe(
      _data => {
        _data.revenues.forEach((element, index) => {
          _data.revenues[index] = Math.round(element * 100) / 100;
        });
        _data.totalRevenue = Math.round(_data.totalRevenue * 100) / 100;

        this.saleInfo = _data;
        this.barChartLabels = _data.labels;
        this.currency = _country.currencyDisplay;
        this.barChartData = [
          // {data: _data.subscriptionItems, label: 'Subscription'},
          { data: _data.revenues, label: `Revenue (${_country.currencyDisplay})` }
        ]
        let value = typeof this.saleInfo.totalRevenue === 'number' ? this.saleInfo.totalRevenue.toFixed(2) : this.saleInfo.totalRevenue;
        this.saleInfo.totalRevenue = `${_country.currencyDisplay}${value}`;
        this.isLoadingChart = false;
      },
      error => {
        this.alertService.error(error)
        console.log(' error ', error.error)
      }
    )
  }

  // events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }

  reloadChart(_chartType: string) {
    this.chartType = _chartType;
    this.getSales();
  }

  changeCountry(_value) {
	let _elem_this = jQuery(_value.target);
	jQuery('.dropdown-menu li').removeClass('active');
	_elem_this.closest('li').addClass('active');
	let code_country:number = parseInt(_elem_this.attr('data-value'));
    this.countryId = code_country;
    this.getSales();
    this.getOrderPendingList();
  }

  allowAccessTo(_path: string) {
    let _permission_of_path : any = {};
    if (this.currentUser && this.currentUser['Role'] && this.currentUser['Role'].roleDetails && this.currentUser['Role'].roleDetails.length > 0) {
      _permission_of_path = this.currentUser['Role'].roleDetails.filter(per => per.Path && per.Path.pathValue === _path);
      if (_permission_of_path && _permission_of_path[0]) {
        _permission_of_path = _permission_of_path[0];
      }
    }
    if (_permission_of_path.canView) {
      return true;
    }
    return false;
  }

}
