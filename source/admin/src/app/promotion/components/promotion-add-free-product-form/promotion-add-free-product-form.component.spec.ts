import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionAddFreeProductFormComponent } from './promotion-add-free-product-form.component';

describe('PromotionAddFreeProductFormComponent', () => {
  let component: PromotionAddFreeProductFormComponent;
  let fixture: ComponentFixture<PromotionAddFreeProductFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionAddFreeProductFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionAddFreeProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
