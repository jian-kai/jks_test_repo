import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';


import { GlobalService } from '../../../core/services/global.service';
import { HttpService } from '../../../core/services/http.service';
import { PromotionService } from '../../../core/services/promotion.service';
import { AppConfig } from 'app/config/app.config';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'promotion-translate-form',
  templateUrl: './promotion-translate-form.component.html',
  styleUrls: ['./promotion-translate-form.component.scss', '../../../../assets/sass/subscription.scss']
})
export class PromotionTranslateFormComponent implements OnInit {
  @Input() promotionTranslateForm;
  @Input() parentOnActive;
  private promotionId: number;
  constructor(private promotionService: PromotionService,
    private globalService: GlobalService,
    private _http: HttpService,
    private appConfig: AppConfig,
    private _router: Router,
    private activateRoute: ActivatedRoute, ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.promotionId = +params['id']// (+) converts string 'id' to a number
    });
  }

}
