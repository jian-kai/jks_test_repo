import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionTranslateFormComponent } from './promotion-translate-form.component';

describe('PromotionTranslateFormComponent', () => {
  let component: PromotionTranslateFormComponent;
  let fixture: ComponentFixture<PromotionTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
