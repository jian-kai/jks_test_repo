import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PromotionService } from '../../../core/services/promotion.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'promotion-add-user-form',
  templateUrl: './promotion-add-user-form.component.html',
  styleUrls: ['./promotion-add-user-form.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class PromotionAddUserFormComponent implements OnInit {
  @Input() addUserForm;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public users: any;
  public countryId: number;
  public totalDeliverTimes: number;
  public planTypes: any = [];
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private promotionService: PromotionService,
    private alertService: AlertService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    this.countryId = this.addUserForm.parent.controls.CountryId.value;
    this.globalService.getPlanTypes().then(data => {
      this.planTypes = data;
    })
    setTimeout(_ => this.getproductList());
    this.getproductList();
    this.addUserForm.parent.controls.CountryId.valueChanges.subscribe(data => {
      this.clearPlanDetails();
    })
  }
  // get product list
  getproductList() {
    this.options = {
      // CountryId: this.countryId,
      // status: 'active'
    }
    let params = this.globalService._URLSearchParams(this.options);
    this._http._getList(`${this.appConfig.config.api.customer_users}?` + params.toString()).subscribe(
      data => {
        this.users = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log('error', error);
      }
    )
  }

  // clear plan details
  clearPlanDetails() {
    let control = <FormArray>this.addUserForm;
    control.controls.forEach((element, index) => {
      control.removeAt(index);
    });
  }

  filterUsers() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.users.forEach((user, index) => {
        let has = false;
        if (user.firstName && user.firstName.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
          has = true;
        } else if (user.lastName && user.lastName.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
          has = true;
        } else if (user.email && user.email.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
          has = true;
        }
        if (has) {
          this.filteredList.push(user);
        }
      });
    } else {
      this.filteredList = [];
    }
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.addUserForm.controls.length) {
      _val = [];
    } else {
      _val = this.addUserForm.value;
      _val.forEach(element => {
        if (element.user.email === item.email) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.addUserForm;
      let prCtrl = this.promotionService.initUserDetails();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { user: item});

      control.push(prCtrl);
      this.addUserForm.parent.controls.mailChimpListId.setValidators(null);
      this.addUserForm.parent.controls.mailChimpListId.updateValueAndValidity();
    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.addUserForm.controls.product.setValue(_val);
  }

  removeItem(_i: number) {
    let control = <FormArray>this.addUserForm;
    control.removeAt(_i);
    if (control.length === 0) {
      this.addUserForm.parent.controls.mailChimpListId.setValidators(Validators.required);
      this.addUserForm.parent.controls.mailChimpListId.updateValueAndValidity();
      if (!this.addUserForm.parent.controls.mailChimpListId.value) {
        this.addUserForm.setValidators(Validators.required);
        this.addUserForm.updateValueAndValidity();
      }
    }
  }
  
}