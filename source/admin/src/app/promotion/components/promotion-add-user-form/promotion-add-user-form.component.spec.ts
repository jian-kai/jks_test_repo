import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionAddUserFormComponent } from './promotion-add-user-form.component';

describe('PromotionAddUserFormComponent', () => {
  let component: PromotionAddUserFormComponent;
  let fixture: ComponentFixture<PromotionAddUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionAddUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionAddUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
