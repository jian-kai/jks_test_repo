import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionAddProductFormComponent } from './promotion-add-product-form.component';

describe('PromotionProductDetailsFormComponent', () => {
  let component: PromotionAddProductFormComponent;
  let fixture: ComponentFixture<PromotionAddProductFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionAddProductFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionAddProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
