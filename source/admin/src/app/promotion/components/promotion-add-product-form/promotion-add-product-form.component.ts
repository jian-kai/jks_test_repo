import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PromotionService } from '../../../core/services/promotion.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'promotion-add-product-form',
  templateUrl: './promotion-add-product-form.component.html',
  styleUrls: ['./promotion-add-product-form.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class PromotionAddProductFormComponent implements OnInit {
  @Input() addProductForm;
  @Input() fname;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public products: any;
  public countryId: number;
  public totalDeliverTimes: number;
  public planTypes: any = [];
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  public applyAll : boolean = false;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private promotionService: PromotionService,
    private alertService: AlertService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    this.addProductForm.setValidators(Validators.required);
    this.countryId = this.addProductForm.parent.controls.CountryId.value;
    this.globalService.getPlanTypes().then(data => {
      this.planTypes = data;
    })
    setTimeout(_ => this.getproductList());
    this.getproductList();
    this.addProductForm.parent.controls.CountryId.valueChanges.subscribe(data => {
      this.clearPlanDetails();
      this.getproductList();
    })

    if (this.addProductForm.parent.controls.applyAllProducts.value == 'yes') {
      this.checkApplyAll();
    }
  }
  // get product list
  getproductList() {
    this.options = {
      CountryId: this.countryId,
      status: 'active'
    }
    let params = this.globalService._URLSearchParams(this.options);
    this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.products = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log('error', error);
      }
    )
  }

  // clear plan details
  clearPlanDetails() {
    let control = <FormArray>this.addProductForm;
    control.controls.forEach((element, index) => {
      control.removeAt(index);
    });
    this.countryId = this.addProductForm.parent.controls.CountryId.value;
  }

  filterProducts() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.products.forEach((product, index) => {
        let has = false;
        product.productTranslate.forEach(item => {
          if (item.name.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
            return has = true;
          }
        });
        if (has) {
          this.filteredList.push(product);
        }
      });
    } else {
      this.filteredList = [];
    }
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    item.productCountry = item.productCountry.filter(tmp => tmp.Country.id === this.countryId).length > 0 ?
                          item.productCountry.filter(tmp => tmp.Country.id === this.countryId) :
                          item.productCountry
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.addProductForm.controls.length) {
      _val = [];
    } else {
      _val = this.addProductForm.value;
      _val.forEach(element => {
        if (element.product.productCountry[0].id === item.productCountry[0].id) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.addProductForm;
      let prCtrl = this.promotionService.initProductDetails();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { product: item});

      control.push(prCtrl);
    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.addProductForm.controls.product.setValue(_val);
  }

  removeItem(_i: number) {
    let control = <FormArray>this.addProductForm;
    control.removeAt(_i);
  }

  checkApplyAll() {
    this.applyAll = true;
    this.clearPlanDetails();
    this.addProductForm.setValidators(null);
    this.addProductForm.updateValueAndValidity();
    this.addProductForm.parent.controls.applyAllProducts.setValue('yes');
  }

  uncheckApplyAll() {
    this.applyAll = false;
    this.addProductForm.setValidators(Validators.required);
    this.addProductForm.updateValueAndValidity();
    this.addProductForm.parent.controls.applyAllProducts.setValue();
  }

  // add all
  handleApplyAll(_event) {
    if(_event.target.checked){
      this.checkApplyAll();
    } else {
      this.uncheckApplyAll();
    }
  }
}