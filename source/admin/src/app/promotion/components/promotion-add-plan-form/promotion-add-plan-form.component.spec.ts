import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionAddPlanFormComponent } from './promotion-add-plan-form.component';

describe('PromotionAddPlanFormComponent', () => {
  let component: PromotionAddPlanFormComponent;
  let fixture: ComponentFixture<PromotionAddPlanFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionAddPlanFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionAddPlanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
