import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PromotionService } from '../../../core/services/promotion.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'promotion-add-plan-form',
  templateUrl: './promotion-add-plan-form.component.html',
  styleUrls: ['./promotion-add-plan-form.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class PromotionAddPlanFormComponent implements OnInit {
  @Input() addPlanForm;
  @Input() f;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public plans: any;
  public countryId: number;
  public totalDeliverTimes: number;
  public planTypes: any = [];
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedPlan: any = [];
  public queryForm: FormGroup;
  public applyAll : boolean = false;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private promotionService: PromotionService,
    private alertService: AlertService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    this.addPlanForm.setValidators(Validators.required);
    this.countryId = this.addPlanForm.parent.controls.CountryId.value;
    this.globalService.getPlanTypes().then(data => {
      this.planTypes = data;
    })

    setTimeout(_ => this.getPlanList());
    this.getPlanList();
    this.addPlanForm.parent.controls.CountryId.valueChanges.subscribe(data => {
      this.clearPlanDetails();
      this.getPlanList();
    })

    if (this.addPlanForm.parent.controls.applyAllPlans.value == 'yes') {
      this.checkApplyAll();
    }
  }
  // get plan list
  getPlanList() {
    this.options = {
      CountryId: this.countryId,
      status: 'active'
    }
    let params = this.globalService._URLSearchParams(this.options);
    this._http._getList(`${this.appConfig.config.api.plan_list}/select-list?` + params.toString()).subscribe(
      data => {
        this.plans = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log('error', error);
      }
    )
  }

  // clear plan details
  clearPlanDetails() {
    let control = <FormArray>this.addPlanForm;
    control.controls.forEach((element, index) => {
      control.removeAt(index);
    });
    this.countryId = this.addPlanForm.parent.controls.CountryId.value;
  }

  filterPlans() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.plans.forEach((plan, index) => {
        let has = false;
        plan.planTranslate.forEach(item => {
          if (item.name.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
            return has = true;
          }
        });
        if (has) {
          this.filteredList.push(plan);
        }
      });
    } else {
      this.filteredList = [];
    }
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    item.planCountry = item.planCountry.filter(tmp => tmp.Country.id === this.countryId).length > 0 ?
                          item.planCountry.filter(tmp => tmp.Country.id === this.countryId) :
                          item.planCountry
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.addPlanForm.controls.length) {
      _val = [];
    } else {
      _val = this.addPlanForm.value;
      _val.forEach(element => {
        if (element.plan.planCountry[0].id === item.planCountry[0].id) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.addPlanForm;
      let prCtrl = this.promotionService.initPlanDetails();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { plan: item});

      control.push(prCtrl);
     
    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.addPlanForm.controls.plan.setValue(_val);
  }

  removeItem(_i: number) {
    let control = <FormArray>this.addPlanForm;
    control.removeAt(_i);
  }

  checkApplyAll() {
    this.applyAll = true;
    this.clearPlanDetails();
    this.addPlanForm.setValidators(null);
    this.addPlanForm.updateValueAndValidity();
    this.addPlanForm.parent.controls.applyAllPlans.setValue('yes');
  }

  uncheckApplyAll() {
    this.applyAll = false;
    this.addPlanForm.setValidators(Validators.required);
    this.addPlanForm.updateValueAndValidity();
    this.addPlanForm.parent.controls.applyAllPlans.setValue('');
  }

  // add all
  handleApplyAll(_event) {
    if(_event.target.checked){
      this.checkApplyAll();
    } else {
      this.uncheckApplyAll();
    }
  }
}
