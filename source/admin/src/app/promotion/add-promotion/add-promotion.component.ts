import { Component, ElementRef, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { LoadingService } from '../../core/services/loading.service';
import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { PromotionService } from '../../core/services/promotion.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { MdDatepicker, MdDatepickerModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { numberValidator } from '../../core/directives/number-validator.directive';

@Component({
  selector: 'app-add-promotion',
  templateUrl: './add-promotion.component.html',
  styleUrls: ['./add-promotion.component.scss', '../../../assets/sass/promotion.scss'],
  providers: [DatePipe]
})
export class AddPromotionComponent implements OnInit {
  @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;
  private currentUser: any;
  public promotionForm: FormGroup;
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public addImageFlag: boolean = false;
  public countries: any;
  public mailChimpList: any;
  public activeSubmitForm: boolean = false;

  public minDate:Date ;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private promotionService: PromotionService,
    private _router: Router,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private datePipe: DatePipe) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.promotionForm = builder.group({
      discount: ['', [Validators.required, numberValidator()]], // percent value
      maxDiscount: [''],
      minSpend: [''],
      allowMinSpendForTotalAmount: [''],
      timePerUser: ['', [numberValidator()]],
      promoCode: [''],
      promoType: ['', Validators.required],
      isFreeShipping: [''],
      image: ['', Validators.required],
      emailTemplateId: [''],
      emailTemplateOptions: [''],
      expiredAt: ['', Validators.required], // date
      activeAt: [''],
      promotionTranslate: builder.array([]),
      addPlanForm: builder.array([]),
      applyAllPlans: [],
      addProductForm: builder.array([]),
      applyAllProducts: [],
      addFreeProductForm: builder.array([]),
      addUserForm: builder.array([]),
      mailChimpListId: [''],
      CountryId: ['', Validators.required],
      isOnline: [true],
      isOffline: [true]
    });
    // this.selectCountryForm = builder.group({
    //   countryId: ['', Validators.required],
    // });

    this.selectTranslateForm = builder.group({
      langCode: ['', Validators.required],
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });

    this.globalService.getCountryList().then(data => {
      this.countries = data;
      this.promotionForm.controls.CountryId.setValue(this.countries[0].id);
    })
  }
  addEvent(type: string, $event) {
    //console.log($event);    
    this.minDate = $event;
    if(this.promotionForm.controls.expiredAt.value <= this.minDate){
      this.promotionForm.controls.expiredAt.setValue(new Date(''));
    };
  }
  ngOnInit() {
    // add default Promotion Translate form
    this.addPromotionTranslate('en');
    this.addImageFlag = true;
    this.promotionForm.controls.promoType.setValue('uniqueCode');
    this.promotionForm.controls.expiredAt.setValue(this.minDate);
    this.promotionForm.controls.activeAt.setValue(new Date());
    this.promotionForm.controls.promoType.valueChanges.subscribe(data => {
      if (data === 'genericCode') {
        this.promotionForm.controls.addUserForm.setValidators(null);
        this.promotionForm.controls.mailChimpListId.setValidators(null);
        this.promotionForm.controls.addUserForm.updateValueAndValidity();
        this.promotionForm.controls.mailChimpListId.updateValueAndValidity();

        this.promotionForm.controls.promoCode.setValidators(Validators.required);
        this.promotionForm.controls.promoCode.updateValueAndValidity();
        this.setErrorsForAddUser(null);
      } else {
        this.promotionForm.controls.addUserForm.setValidators(Validators.required);
        this.promotionForm.controls.mailChimpListId.setValidators(Validators.required);
        this.promotionForm.controls.addUserForm.updateValueAndValidity();
        this.promotionForm.controls.mailChimpListId.updateValueAndValidity();

        this.promotionForm.controls.promoCode.setValidators(null);
        this.promotionForm.controls.promoCode.updateValueAndValidity();
        this.setErrorsForAddUser(true);
      }

    });
    this.promotionForm.controls.mailChimpListId.valueChanges.subscribe(data => {
      if (data) {
        this.promotionForm.controls.addUserForm.setValidators(null);
        this.promotionForm.controls.addUserForm.updateValueAndValidity();
      } else {
        this.setErrorsForAddUser(true);
      }
    });
    this.getMailChimpList();
  }

  getMailChimpList() {
    this.globalService.mailChimpList().then(data => {
      this.mailChimpList = data;
    })
  }

  setErrorsForAddUser(_value: any) {
    if (this.promotionForm.controls.addUserForm.value.length === 0) {
      if (_value) {
        this.promotionForm.controls.addUserForm.setErrors({ "required": _value });
        this.promotionForm.controls.mailChimpListId.setErrors({ "required": _value });
      } else {
        this.promotionForm.controls.addUserForm.setErrors(null);
        this.promotionForm.controls.mailChimpListId.setErrors(null);
      }

    }
  }

  //add Promotion Translate
  addPromotionTranslate(_langCode: string) {
    let control = <FormArray>this.promotionForm.controls['promotionTranslate'];
    let hasExist: boolean = false;
    control.controls.forEach(element => {
      if (element.value.langCode === _langCode) {
        hasExist = true;
      }
    });
    if (!hasExist) {
      let trCtrl = this.promotionService.initPromotionTranslate();
      trCtrl = this.globalService.setValueFormBuilder(trCtrl, { langCode: _langCode });
      control.push(trCtrl);
    }
  }

  // remove promotion translate
  removePromotionTranslate(_i: number) {
    let control = <FormArray>this.promotionForm.controls['promotionTranslate'];
    control.removeAt(_i);
  }

	setActiveBtn() {
		this.activeSubmitForm = true;
	}
	
  doAddPromotion() {
    if (this.promotionForm.valid) {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        if (result) {
          console.log('this.planForm.value', this.promotionForm.value)
          this.loadingService.display(true);
          let formData: FormData = new FormData();
          let promotionForm = this.promotionForm;
          for (var key in promotionForm.value) {
            switch (key) {
              case "promotionTranslate":
                let i = 0;
                let indexplanDetails = 0;
                promotionForm.value[key].forEach((item, index) => {

                  for (let key in item) {
                    if ((key === 'id' && !item[key])) {
                      continue;;
                    }
                    formData.append(`promotionTranslate[${i}].` + key, item[key]);
                  }
                  i++;
                });
                break;

              case "addProductForm":
                if (promotionForm.value[key].length > 0) {
                  i = 0;
                  // let indexplanDetails = 0;
                  promotionForm.value[key].forEach((item, index) => {
                    formData.append(`productCountryIds[${i}]`, item.product.productCountry[0].id);
                    i++;
                  });
                } else {
                  if (this.promotionForm.value.applyAllProducts && this.promotionForm.value.applyAllProducts === 'yes') {
                    formData.append(`productCountryIds`, 'all');
                  }
                }

                break;

              case "addPlanForm":
                if (promotionForm.value[key].length > 0) {
                  i = 0;
                  promotionForm.value[key].forEach((item, index) => {
                    formData.append(`planCountryIds[${i}]`, item.plan.planCountry[0].id);
                    i++;
                  });
                } else {
                  if (this.promotionForm.value.applyAllPlans && this.promotionForm.value.applyAllPlans === 'yes') {
                    formData.append(`planCountryIds`, 'all');
                  }
                }
                break;

              case "addFreeProductForm":
                if (promotionForm.value[key].length > 0) {
                  i = 0;
                  // let indexplanDetails = 0;
                  promotionForm.value[key].forEach((item, index) => {
                    formData.append(`freeProductCountryIds[${i}]`, item.product.productCountry[0].id);
                    i++;
                  });
                }
                break;
  

              case "addUserForm":
                if (promotionForm.value[key].length > 0) {
                  i = 0;
                  promotionForm.value[key].forEach((item, index) => {
                    formData.append(`userIds[${i}]`, item.user.id);
                    i++;
                  });
                }
                break;

              case "promoType":
                if (promotionForm.value[key] === 'genericCode') {
                  i = 0;
                  formData.append(`isGeneric`, "true");
                } else {
                  formData.append(`isGeneric`, "false");
                }
                break;

              case "isOnline":
                formData.append(key, promotionForm.value[key]);
                break;

              case "isOffline":
                formData.append(key, promotionForm.value[key]);
                break;

              default:
                if (promotionForm.value[key]) {
                  if (key === 'expiredAt' || (key === 'activeAt')) {
                    formData.append(key, this.datePipe.transform(promotionForm.value[key], 'yyyy-MM-dd'));
                  } else {
                    formData.append(key, promotionForm.value[key]);
                  }
                }
                break;
            }
          }
          this._http._createWithFormData(`${this.appConfig.config.api.promotions}`, formData, 'form-data').subscribe(
            data => {
              if (data.ok) {
                this.loadingService.display(false);
                this._router.navigate(['/promotions'], { queryParams: {} });
              }
            },
            error => {
              this.loadingService.display(false);
              this.alertService.error(error);
            }
          )
        }
      }));
    }
  }

  // go to the list screen
  gotoList() {
    this._router.navigate(['/promotions']);
  }

}
