import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';


@Component({
  selector: 'app-promotion-details',
  templateUrl: './promotion-details.component.html',
  styleUrls: ['./promotion-details.component.scss']
})
export class PromotionDetailsComponent implements OnInit {
  public isLoading: boolean = true;
  public promotionId: string;
  public promotionInfo: any;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,
    private route: ActivatedRoute,
    private _dialogService: DialogService) { }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.route.params.subscribe(params => {
      this.promotionId = params['id'];
      if (!this.promotionId) {
        this.backToList();
      }
      this.getPromotionDetail();
    });
  }

   // get order detail info
  getPromotionDetail() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.promotions}/${this.promotionId}`).subscribe(
      data => {
        this.promotionInfo = data;
        console.log('this.promotionInfo', this.promotionInfo);

        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  // back to order list
  backToList() {
    let path = "/promotions";
    this._router.navigate([path], { queryParams: {} });
  }

}
