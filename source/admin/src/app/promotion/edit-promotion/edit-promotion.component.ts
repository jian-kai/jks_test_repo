import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { Location, DatePipe } from '@angular/common';
import { MdDatepicker } from '@angular/material';

import { LoadingService } from '../../core/services/loading.service';
import { StorageService } from '../../core/services/storage.service';
import { AppConfig } from 'app/config/app.config';
import { AlertService } from '../../core/services/alert.service';
import { HttpService } from '../../core/services/http.service';
import { numberValidator } from '../../core/directives/number-validator.directive';
import { GlobalService } from '../../core/services/global.service';
import { DialogService } from '../../core/services/dialog.service';
import { PromotionService } from '../../core/services/promotion.service';

@Component({
  selector: 'app-edit-promotion',
  templateUrl: './edit-promotion.component.html',
  styleUrls: ['./edit-promotion.component.scss'],
  providers: [DatePipe]
})
export class EditPromotionComponent implements OnInit {

  @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;
  public isLoading: boolean = true;
  public detailsId: number;
  private currentUser: any;
  public detailsInfo: any;
  public detailsForm: FormGroup;
  public countries: any;
  public expiredAtMin: Date;
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public activeSubmitForm: boolean = false;
  public mailChimpList: any;

  constructor(private loadingService: LoadingService,
    private activateRoute: ActivatedRoute,
    private builder: FormBuilder,
    private _http: HttpService,
    private alertService: AlertService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private promotionService: PromotionService,
    private location: Location,
    private _dialogService: DialogService,
    private datePipe: DatePipe,
    private storage: StorageService) {

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    // get detail by id
    this.loadingService.display(true);
    this.activateRoute.params.subscribe(params => {
      this.detailsId = +params['id'] // (+) converts string 'id' to a number
    });

    this._http._getDetail(`${this.appConfig.config.api.promotions}/${this.detailsId}`).subscribe(
      data => {
        this.detailsInfo = data;
        this._ngOnInit();
      },
      error => {
        this.alertService.error(error);
        this.loadingService.display(false);
        console.log(error.error);
      }
    );

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => {
        if (!language.includes(country.languageCode)) {
          language.push(country.languageCode);
        }

        return language;
      }, []);
    });

    this.globalService.getCountryList().then(data => {
      this.countries = data;
    });

    this.globalService.mailChimpList().then(data => {
      this.mailChimpList = data;
    });
  }

  _ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });

    this.initFormData();
    this.expiredAtMin = new Date(this.detailsInfo.activeAt);

    this.detailsForm.controls.promoType.valueChanges.subscribe(type => {
      this.validateFields(type);
    });

    this.detailsForm.controls.mailChimpListId.valueChanges.subscribe(data => {
      if (data) {
        this.detailsForm.controls.addUserForm.setErrors(null);;
      } else {
        this.setErrorsForAddUser(true);
      }
    });

    this.detailsForm.controls.addUserForm.valueChanges.subscribe(data => {
      if (data.length !== 0) {
        this.detailsForm.controls.mailChimpListId.setErrors(null);
      } else {
        this.setErrorsForAddUser(true);
      }
    });

    this.loadingService.display(false);
  }

  ngOnInit() {
    
  }

  setErrorsForAddUser(value: boolean) {
    if (value) {
      if (this.detailsForm.controls.addUserForm.value.length === 0 && !this.detailsForm.controls.mailChimpListId.value) {
        this.detailsForm.controls.addUserForm.setErrors({ 'required': true });
        this.detailsForm.controls.mailChimpListId.setErrors({ 'required': true });
      }
    } else {
      this.detailsForm.controls.mailChimpListId.setErrors(null);
      this.detailsForm.controls.addUserForm.setErrors(null);
    }
  }

  validateFields(type: string) {
    if (type === 'genericCode') {
      this.setErrorsForAddUser(false);
      this.detailsForm.controls.promoCode.setValidators(Validators.required);
    } else {
      this.setErrorsForAddUser(true);
      this.detailsForm.controls.promoCode.setValidators(null);
    }

    this.detailsForm.controls.promoCode.updateValueAndValidity();
  }

  // add Promotion Translate
  addPromotionTranslate(translateInfo: any) {
    let control = <FormArray>this.detailsForm.controls.promotionTranslate;
    let translateCode = translateInfo.langCode.toLowerCase();
    let translateId = translateInfo.id ? translateInfo.id : '';
    let translateName = translateInfo.name ? translateInfo.name : '';
    let translateDescription = translateInfo.description ? translateInfo.description : '';

    let trCtrl = this.promotionService.initPromotionTranslate();
    trCtrl.controls.id.setValue(translateId);
    trCtrl.controls.name.setValue(translateName);
    trCtrl.controls.description.setValue(translateDescription);

    trCtrl = this.globalService.setValueFormBuilder(trCtrl, {langCode: translateCode});
    control.push(trCtrl);
  }

  // remove promotion translate
  removePromotionTranslate(_i: number) {
    let control = <FormArray>this.detailsForm.controls.promotionTranslate;
    control.removeAt(_i);
  }

  setActiveBtn() {
    this.activeSubmitForm = true;
  }

  logFormValidationErrors() {
    Object.keys(this.detailsForm.controls).forEach(key => {

      const controlErrors: ValidationErrors = this.detailsForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log(`Validation error: ${key} - ${keyError}`);
        });
      }
    });

    return false;
  }

  updateActiveDate(date: Date) {
    if (this.detailsForm.controls.expiredAt.value < date) {
      this.detailsForm.controls.expiredAt.setValue(new Date(''));
    } else {
      this.expiredAtMin = date;
    }
  }

  // set value for form data
  initFormData() {
    let code = this.detailsInfo.isGeneric && this.detailsInfo.promotionCodes ? this.detailsInfo.promotionCodes[0].code : '';
    let promotionType = this.detailsInfo.isGeneric ? 'genericCode' : 'uniqueCode';

    this.detailsForm = this.builder.group({
      id: [this.detailsInfo.id],
      discount: [this.detailsInfo.discount, [Validators.required, numberValidator()]], // percent value
      maxDiscount: [this.detailsInfo.maxDiscount],
      minSpend: [this.detailsInfo.minSpend],
      timePerUser: [this.detailsInfo.timePerUser, [numberValidator()]],
      allowMinSpendForTotalAmount: [this.detailsInfo.allowMinSpendForTotalAmount],
      promoType: [promotionType, Validators.required],
      isFreeShipping: [this.detailsInfo.isFreeShipping],
      promoCode: [code],
      expiredAt: [new Date(this.detailsInfo.expiredAt), Validators.required], // date
      activeAt: [new Date(this.detailsInfo.activeAt)],
      CountryId: [this.detailsInfo.Country.id, Validators.required],
      bannerUrl: [this.detailsInfo.bannerUrl, Validators.required],
      applyAllProducts: [this.detailsInfo.productCountryIds == 'all' ? 'yes' : ''],
      applyAllPlans: [this.detailsInfo.planCountryIds == 'all' ? 'yes' : ''],
      addProductForm: this.builder.array([]),
      addPlanForm: this.builder.array([]),
      addUserForm: this.builder.array([]),
      promotionTranslate: this.builder.array([]),
      mailChimpListId: ['']
    });

    this.selectTranslateForm = this.builder.group({
      langCode: ['', Validators.required]
    });

    if (this.detailsInfo.productCountryIds != 'all' && this.detailsInfo.productCountries) {
      let control = <FormArray>this.detailsForm.controls.addProductForm;

      this.detailsInfo.productCountries.forEach(item => {
        let prCtrl = this.promotionService.initProductDetails();
        prCtrl = this.globalService.setValueFormBuilder(prCtrl, {product: item.Product});

        control.push(prCtrl);
      });
    }

    if (this.detailsInfo.planCountryIds != 'all' && this.detailsInfo.planCountries) {
      let control = <FormArray>this.detailsForm.controls.addPlanForm;

      this.detailsInfo.planCountries.forEach(item => {
        let prCtrl = this.promotionService.initPlanDetails();
        prCtrl = this.globalService.setValueFormBuilder(prCtrl, {plan: item.Plan});

        control.push(prCtrl);
      });
    }

    if (this.detailsInfo.users && this.detailsInfo.users.length > 0) {
      let control = <FormArray>this.detailsForm.controls.addUserForm;

      this.detailsInfo.users.forEach(item => {
        let prCtrl = this.promotionService.initUserDetails();
        prCtrl = this.globalService.setValueFormBuilder(prCtrl, {user: item});

        control.push(prCtrl);
      });
    }

    if (this.detailsInfo.promotionTranslate && this.detailsInfo.promotionTranslate.length > 0) {
      this.detailsInfo.promotionTranslate.forEach(item => {
        this.addPromotionTranslate(item);
      });
    }
  }

  doSubmit() {
    this.validateFields(this.detailsForm.controls.promoType.value);

    if (!this.detailsForm.valid) {
      return this.logFormValidationErrors();
    }

    this._dialogService.confirm().afterClosed().subscribe(result => {
      if (result) {
        this.processSubmitData();
      }
    });
  }

  processSubmitData() {
    // this.loadingService.display(true);

    let formData: FormData = new FormData();
    let detailsForm = this.detailsForm;

    for (var key in detailsForm.value) {
      switch (key) {
        case 'promotionTranslate':
          let i = 0;
          let indexplanDetails = 0;

          detailsForm.value[key].forEach((item, index) => {
            for (let key in item) {
              if ((key === 'id' && !item[key])) {
                continue;;
              }
              formData.append(`promotionTranslate[${i}].` + key, item[key]);
            }
            i++;
          });
          break;

        case 'addProductForm':
          if (detailsForm.value[key].length > 0) {
            i = 0;
            detailsForm.value[key].forEach((item, index) => {
              formData.append(`productCountryIds[${i}]`, item.product.productCountry[0].id);
              i++;
            });
          } else {
            if (this.detailsForm.value.applyAllProducts && this.detailsForm.value.applyAllProducts === 'yes') {
              formData.append('productCountryIds', 'all');
            }
          }
          break;

        case 'addPlanForm':
          if (detailsForm.value[key].length > 0) {
            i = 0;
            detailsForm.value[key].forEach((item, index) => {
              formData.append(`planCountryIds[${i}]`, item.plan.planCountry[0].id);
              i++;
            });
          } else {
            if (this.detailsForm.value.applyAllPlans && this.detailsForm.value.applyAllPlans === 'yes') {
              formData.append('planCountryIds', 'all');
            }
          }
          break;

        case 'addUserForm':
          if (detailsForm.value[key].length > 0) {
            i = 0;
            detailsForm.value[key].forEach((item, index) => {
              formData.append(`userIds[${i}]`, item.user.id);
              i++;
            });
          }
          break;

        case 'promoType':
          if (detailsForm.value[key] === 'genericCode') {
            i = 0;
            formData.append('isGeneric', 'true');
          } else {
            formData.append('isGeneric', 'false');
          }
          break;

        default:
          if (detailsForm.value[key]) {
            if (key === 'expiredAt' || (key === 'activeAt')) {
              formData.append(key, this.datePipe.transform(detailsForm.value[key], 'yyyy-MM-dd'));
            } else {
              formData.append(key, detailsForm.value[key]);
            }
          }
          break;
      }
    }

    this._http._updatePut(`${this.appConfig.config.api.promotions}/${this.detailsInfo.id}`, formData, 'form-data').subscribe(
      data => {
        if (data.ok) {
          this.loadingService.display(false);
          this.location.back();
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    );
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }

}
