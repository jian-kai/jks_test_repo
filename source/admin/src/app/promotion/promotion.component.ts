import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../core/services/dialog.service';
import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss',
  '../../assets/sass/promotion.scss',
  '../../assets/sass/table.scss']
})
export class PromotionComponent extends ListViewsComponent {
  public promotionList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public orderByDate: string;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', 'createdAt_DESC');
    }
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.promotions}?` + this.options.toString()).subscribe(
      data => {
        this.promotionList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    // if (_type === "ASC") {
    //   this.filterby('orderBy', '["name"]');
    // } else {
    //   this.filterby('orderBy', '["name_DESC"]');
    // }
  }

    // sort by date
  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }

  // delete product 
  deletePromotion(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let pathDelete = `${this.appConfig.config.api.promotions}/${_id}`;
        this._http._delete(`${pathDelete}`).subscribe(
          data => {
            if (data.ok) {
              this.getData();
            }
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
            console.log('error', error);
          }
        )
      }
    }));
  }
  changedatalist(list){ 
    this.promotionList.rows =  list ;  
  }
}
