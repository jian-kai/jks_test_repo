import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss', '../../../assets/sass/table.scss']
})
export class OrderHistoryComponent extends ListViewsComponent {
  public ordersHistory: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public showing: string;
  public orderByDate: string;
  public orderStates: any;
  public countries: any;
  public fromDate: any;
  public toDate: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public hasDenied: boolean;
  public timer = null;
  public isDownloadTaxInvoice: boolean = false;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService, ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.hasDenied = this.globalService.hasDenied();
  }

  // get order pending list
  getData() {
    this.isDownloadTaxInvoice = false;
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    this.loadingService.display(true);
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', '["createdAt_DESC"]');
      this.orderByDate = 'DESC';
    }

    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    }else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 1);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }
    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    }else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }

    // [Admin][Revamp] Searching without filtering | Remove fromDate & toDate if we have param 's'
    let searchKey = this.options.get('s');
    if (searchKey) {
      let realOrderId = this.globalService.getOrderIdFromOrderNumber(searchKey);

      if (realOrderId) {
        this.options.set('s', realOrderId);
      }

      this.options.delete('fromDate');
      this.options.delete('toDate');
    }

    this._http._getList(`${this.appConfig.config.api.order_history}?` + this.options.toString()).subscribe(
      data => {
        this.ordersHistory = data;
        if (this.ordersHistory && this.ordersHistory.rows && this.ordersHistory.rows.length > 0) {
          this.ordersHistory.rows.forEach(item => {
            item.currency = item.Country.currencyDisplay;

            item.productSkus = '';
            if (item.orderDetail) {
              item.orderDetail.forEach(detail => {
                if (detail.PlanCountry) {
                  // Has shave plan
                  item.productSkus = item.productSkus ? (item.productSkus + ', ') : '';
                  item.productSkus += detail.PlanCountry.Plan.sku;
                } else if(detail.ProductCountry) {
                  // Has product
                  item.productSkus = item.productSkus ? (item.productSkus + ', ') : '';
                  item.productSkus += detail.ProductCountry.Product.sku;
                }
              });
            }
          });
        }
        this.totalItems = data.count;
        this.loadingService.display(false);

        setTimeout(() => {
          this.globalService.makeTableHorizontalScrollable();
          //this.globalService.addScrollTable();
          //this.globalService.dragMouse();
        }, 500);
      },
      error => {
        this.alertService.error(error);
        console.log(error.error)
      }
    )
    this.globalService.getOrderStates().then((data) => {
      this.orderStates = data;
    });
  }

  // sort by date
  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)
  }

  changeFromDate(_fromDate) {
    if (_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
    // this.getData();
  }

  changeToDate(_toDate) {
    if (_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // update status
  updateStatus(orderID: number, state: string) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        let datas = { states: state };
        this._http._updatePut(`${this.appConfig.config.api.orders}/${orderID}`, datas).subscribe(
          res => {
            this.getData();
          },
          error => {
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  downloadCSV() {
    this.options.set('historyType', 'history');
    // this.options.set('orderBy', '["createdAt_DESC"]');
    this.loadingService.display(true);
    this._http._getDetailCsv(`${this.appConfig.config.api.orders}/download?` + this.options.toString(), 300000).subscribe(
      data => {
        let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement('a');
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
          dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', `History orders report for ${this.globalService.formatDate(this.fromDate)} to ${this.globalService.formatDate(this.toDate)}.csv`);
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )

  }
  changedatalist(list) {
    this.ordersHistory.rows = list;
  }

  downloadTaxInvoice() {
    this.loadingService.display(true);
    if (!this.options.get('historyType')) {
      this.options.set('historyType', 'history');
    }

    this._http._getDetail(`${this.appConfig.config.api.orders}/download/tax-invoice?` + this.options.toString()).subscribe(
      data => {
        this.loadingService.display(false);
        if(data.ok) {
          this.isDownloadTaxInvoice = true;
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )

  }

}
