import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { MdDatepickerModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';

@Component({
  selector: 'app-order-upload',
  templateUrl: './order-upload.component.html',
  styleUrls: ['./order-upload.component.scss', '../../../assets/sass/table.scss']
})
export class OrderUploadComponent extends ListViewsComponent {
  public orderUploadList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public orderStates: any;
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public _test;
  public fromDate: any;
  public toDate: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public orderByDate: string;
  public hasDenied: boolean;
  public timer = null;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    public modalService: ModalService,
    private _dialogService: DialogService,
    routerService: RouterService,
    el: ElementRef, ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.hasDenied = this.globalService.hasDenied();
  }

  // get Sales Reports list
  getData() {
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }

    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', '["createdAt_DESC"]');
      this.orderByDate = 'DESC';
    }

    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    }else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 7);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }
    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    }else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }

    // [Admin][Revamp] Searching without filtering | Remove fromDate & toDate if we have param 's'
    if (this.options.get('s')) {      
      this.options.delete('fromDate');
      this.options.delete('toDate');
    }

    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.order_upload}?` + this.options.toString()).subscribe(
      data => {
        console.log('data', data);
        this.orderUploadList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
        //console.log(data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error([error]);
      }
    )
    this.globalService.getOrderStates().then((data) => {
      this.orderStates = data;
    });
  }
  changedatalist(list) {
    this.orderUploadList.rows = list;
  }
  // re-format date to param
  formatDate(_date: Date) {
    let year = _date.getFullYear().toString();
    let month = (_date.getMonth() + 1).toString();
    let day = _date.getDate().toString();

    if (month.length === 1) {
      month = '0' + month;
    }
    if (day.length === 1) {
      day = '0' + day;
    }

    return year + '-' + month + '-' + day;
  }

  changeFromDate(_fromDate) {
    if (_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
  }

  changeToDate(_toDate) {
    if (_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    console.log(_type);
    this.getData();
  }

  // sort by date
  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)

  }

  downloadCSV() {
    this.loadingService.display(true);
    this._http._getDetailCsv(`${this.appConfig.config.api.order_upload_download}?` + this.options.toString()).subscribe(
      data => {
        let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement('a');
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
          dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', `Order upload for ${this.formatDate(this.fromDate)} to ${this.formatDate(this.toDate)}.csv`);
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error([error]);
      }
    )
  }
}
