import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCreateDetailsComponent } from './bulk-create-details.component';

describe('BulkCreateDetailsComponent', () => {
  let component: BulkCreateDetailsComponent;
  let fixture: ComponentFixture<BulkCreateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkCreateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCreateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
