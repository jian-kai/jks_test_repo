import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from '../../../app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { OrderNoPipe } from '../../core/pipes/order-no.pipe';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';
import { DatePipe } from '@angular/common';
import { GTMService } from '../../core/services/gtm.service';


@Component({
  selector: 'app-order-pending',
  templateUrl: './order-pending.component.html',
  styleUrls: ['./order-pending.component.scss', '../../../assets/sass/table.scss'],
  providers: [OrderNoPipe, DatePipe, GTMService]
})
export class OrderPendingComponent extends ListViewsComponent {
  public ordersPending: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public showing: string;
  public orderByDate: string;
  public orderStates: any;
  public countries: any;
  public fromDate: any;
  public toDate: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public hasDenied: boolean;
  public timer = null;
  public limit;
  public currentPage;
  public isDownloadTaxInvoice: boolean = false;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    private orderNoPipe: OrderNoPipe,
    private datePipe: DatePipe,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService,
    public _GTMService: GTMService) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.hasDenied = this.globalService.hasDenied();
  }

  // get order pending list
  getData() {
    this.isDownloadTaxInvoice = false;
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    this.loadingService.display(true);
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', '["createdAt_DESC"]');
      this.orderByDate = 'DESC';
    }

    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    } else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 1);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }
    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    } else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }

    // [Admin][Revamp] Searching without filtering | Remove fromDate & toDate if we have param 's'
    let searchKey = this.options.get('s');
    if (searchKey) {
      let realOrderId = this.globalService.getOrderIdFromOrderNumber(searchKey);

      if (realOrderId) {
        this.options.set('s', realOrderId);
      }

      this.options.delete('fromDate');
      this.options.delete('toDate');
    }

    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.order_pendding}?` + this.options.toString()).subscribe(
      data => {
        this.ordersPending = data;
        if (this.ordersPending && this.ordersPending.rows && this.ordersPending.rows.length > 0) {
          this.ordersPending.rows.forEach(item => {
            item.currency = item.Country.currencyDisplay;
            item.hasSubscription = !!item.subscriptionIds;

            item.productSkus = '';
            if (item.orderDetail) {
              item.orderDetail.forEach(detail => {
                if (detail.PlanCountry) {
                  // Has shave plan
                  item.productSkus = item.productSkus ? (item.productSkus + ', ') : '';
                  item.productSkus += detail.PlanCountry.Plan.sku;
                } else if(detail.ProductCountry) {
                  // Has product
                  item.productSkus = item.productSkus ? (item.productSkus + ', ') : '';
                  item.productSkus += detail.ProductCountry.Product.sku;
                }
              });
            }
          });
        }
        this.totalItems = data.count;
        this.loadingService.display(false);
        console.log(data);

        setTimeout(() => {
          this.globalService.makeTableHorizontalScrollable();
          this.globalService.fixDropdownTable();
          //this.globalService.addScrollTable();
          //this.globalService.dragMouse();
        }, 500);
      },
      error => {
        this.alertService.error(error);
        console.log(error.error)
      }
    )

    this.globalService.getOrderStates().then((data) => {
      this.orderStates = data;
    });
  }
  changedatalist(list){ 
    this.ordersPending.rows =  list ;  
  }
  // sort by date
  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)

  }

  changeFromDate(_fromDate) {
    if(_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
    // this.getData();
  }

  changeToDate(_toDate) {
    if(_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // update status
  updateStatus(item: any, state: string) {
    let orderID: number = item.id;
    console.log('state', state)
    if (state === 'Canceled') {
      this.cancelOrder(item);
    } else {
      let orderIDPP = this.orderNoPipe.transform(orderID, {countryCode: item.Country.code, createdAt: this.datePipe.transform(item.createdAt, 'yyyyMMdd')});
      let msg;
      if(state === 'Completed') {
        msg = 'You are going to complete the order <font color="#fe5000">{{value}}</font>. Are you sure?'.replace('{{value}}', orderIDPP);
      }
      this._dialogService.confirm(msg).afterClosed().subscribe((result => {
        if (result) {
          let datas = { states: state };
          this._http._updatePut(`${this.appConfig.config.api.orders}/${orderID}`, datas).subscribe(
            res => {
              this.getData();
            },
            error => {
              this.alertService.error(error);
            }
          )
        }
      }));
    }
  }

  downloadCSV() {
    if (!this.options.get('historyType')) {
      this.options.set('historyType', 'pending');
    }
    this.loadingService.display(true);
    this._http._getDetailCsv(`${this.appConfig.config.api.orders}/download?` + this.options.toString(), 300000).subscribe(
      data => {
        let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement('a');
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
          dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', `Pending orders report for ${this.globalService.formatDate(this.fromDate)} to ${this.globalService.formatDate(this.toDate)}.csv`);
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )

  }

  cancelOrder(item) {
    // let orderIDPP = this.orderNoPipe.transform(item.id, item.Country.code);
    let orderIDPP = this.orderNoPipe.transform(item.id, {countryCode: item.Country.code, createdAt: this.datePipe.transform(item.createdAt, 'yyyyMMdd')});
    let _orderId = item.id;
    let cancleMsg = 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?'.replace('{{value}}', orderIDPP);
    this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let options = { userTypeId: 1 };
        let params = this.globalService._URLSearchParams(options);
        this._http._delete(`${this.appConfig.config.api.admin_orders}/${_orderId}?${params.toString()}`).subscribe(
          data => {
            // this.loadingService.display(false);
            // to check if is Online/E-Commerce chanel to GTM reverse transaction
            if (item && !item.SellerUserId) {
              this._http._getDetail(`${this.appConfig.config.api.orders}/${_orderId}`).subscribe(
                data => {
                  this._GTMService.reverseTransactionData(data);
                  this.loadingService.display(false);
                },
                error => {
                  this.loadingService.display(false);
                  this.alertService.error(error);
                  console.log(error.error);
                }
              )
            }

            this._dialogService.result('Order cancellation request was successful').afterClosed().subscribe((result => {
              this.getData();
            }));
          },
          error => {
            // this.resError = {
            //   type: 'error',
            //   message: JSON.parse(error).message
            // };
            let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this._dialogService.result(message).afterClosed().subscribe((result => {
              this.loadingService.display(false);
            }));
            console.log(`error = ${error}`);
            this.alertService.error(error);
          }
        );
      }
    }));
  }

  checkAllowToCancel(createdAt) {
    // let currentDate = new Date(); 
    // let _createdAt = new Date(createdAt);
    // var seconds: any  = 0;
    // seconds = (currentDate.getTime() - _createdAt.getTime());
    // var one_day=1000*60*60*24; // miniseconds in 1 day
    // let diffHrs = Math.abs((seconds) / 36e5);
    // if (diffHrs > 24) {
    //   return false;
    // }
    return true;
  }

  downloadTaxInvoice() {
    this.loadingService.display(true);
    if (!this.options.get('historyType')) {
      this.options.set('historyType', 'pending');
    }

    this._http._getDetail(`${this.appConfig.config.api.orders}/download/tax-invoice?` + this.options.toString()).subscribe(
      data => {
        this.loadingService.display(false);
        if(data.ok) {
          this.isDownloadTaxInvoice = true;
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )

  }

}
