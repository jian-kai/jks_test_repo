import { Component, ElementRef, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { OrderNoPipe } from '../../core/pipes/order-no.pipe';
import { DatePipe } from '@angular/common';
import { lengthValidator } from '../../core/validators/length.validator';
import { GTMService } from '../../core/services/gtm.service';
declare var jQuery: any;

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
  providers: [OrderNoPipe, DatePipe, GTMService]
})
export class OrderDetailsComponent implements OnInit {
  afterShipUrl: string = 'https://shaves2u.aftership.com';
  urbanfoxUrl: string = 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number=';
  public isLoading: boolean = true;
  public orderId: string;
  public orderInfo: any;
  public trackingForm: FormGroup;
  public orderStates: any;
  public showCommentForm: boolean = false;
  public commentFormbuttom: string = 'Add';
  public path = '';
  public hasTaxInvoice: boolean = false;
  public orderLogForm: FormGroup;
  @ViewChild('datetimepicker') datetimepicker: ElementRef;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,
    private route: ActivatedRoute,
    private _dialogService: DialogService,
    private orderNoPipe: OrderNoPipe,
    private datePipe: DatePipe,
    public _GTMService: GTMService) {
    this.trackingForm = builder.group({
      tracking: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.route.params.subscribe(params => {
      this.orderId = params['id'];
      this.getOrderDetail();
      this.checkTaxInvoice();
    });
    if (this._router.url.indexOf('/orders/history') !== -1) {
      this.path = 'orders/history';
    } else {
      this.path = 'orders/pending';
    }
  }
  deleteProcessing(data, msg: string) {
    const index: number = data.indexOf(msg);
    if (index !== -1) {
      data.splice(index, 1);
    }
    return data;
  }
  // get order detail info
  getOrderDetail() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.orders}/${this.orderId}`).subscribe(
      data => {
        this.orderInfo = data;
        this.orderInfo.subscriptionIds = JSON.parse(this.orderInfo.subscriptionIds);
        // this.orderInfo.subscriptionIds = this.orderInfo.subscriptionIds.replace('["','').replace('"]','').replace('","', ',');
        // get Order States
        this.globalService.getOrderStates().then((_data) => {
          if (data.states === 'Payment Received') {
            //console.log(_data);
            this.orderStates = _data;
          } else {
            this.orderStates = this.deleteProcessing(_data, 'Processing');
          }
          console.log(this.orderStates);

        });
        console.log(data);
        this.loadingService.display(false);
        console.log(this.orderInfo);
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  checkTaxInvoice() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.check_tax_invoice}?orderId=${this.orderId}`).subscribe(
      data => {
        if(data.ok) {
          this.hasTaxInvoice = true;
        }
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  initOrderLogForm(data?: object) {
    this.orderLogForm = this.builder.group({
      id: [''],
      message: ['', [Validators.required, lengthValidator({ max: 300 })]],
      // createdAt: ['', Validators.required],
    });
    if (data) {
      this.orderLogForm.patchValue(data);
    } else {
      // let _createdAt = new Date();
      // this.orderLogForm.patchValue({createdAt: _createdAt});
    }
  }

  // update tracking number
  updateTrackingNumber(_tracking: string) {
    let datas = { tracking: _tracking };
    this.updateOrder(datas);
  }

  // update order status
  updateStatus(status: string) {
    if (status === 'Canceled') {
      this.cancelOrder(this.orderInfo);
    } else {
      let datas = { states: status };
      this.updateOrder(datas);
    }
  }


  // update order
  updateOrder(datas) {
    console.log(datas.states);
    if (datas.states == 'On Hold' || datas.states == 'Processing' || datas.states == 'Payment Received' || datas.states == 'Completed' || datas.states == 'Pending') {
      if (datas) {
        this._http._updatePut(`${this.appConfig.config.api.orders}/${this.orderInfo.id}`, datas).subscribe(
          res => {
            this.getOrderDetail();
          },
          error => {
            this.alertService.error(error);
          }
        )
      }
    } else {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        if (result) {
          this._http._updatePut(`${this.appConfig.config.api.orders}/${this.orderInfo.id}`, datas).subscribe(
            res => {
              this.getOrderDetail();
            },
            error => {
              this.alertService.error(error);
            }
          )
        }
      }));
    }
  }

  cancelOrder(item) {
    let orderIDPP = this.orderNoPipe.transform(item.id, { countryCode: item.Country.code, createdAt: this.datePipe.transform(this.orderInfo.createdAt, 'yyyyMMdd') });
    let _orderId = item.id;
    let cancleMsg = 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?'.replace('{{value}}', orderIDPP);
    this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let options = { userTypeId: 1 };
        let params = this.globalService._URLSearchParams(options);
        this._http._delete(`${this.appConfig.config.api.admin_orders}/${_orderId}?${params.toString()}`).subscribe(
          data => {
            // this.loadingService.display(false);
            // to check if is Online/E-Commerce chanel to GTM reverse transaction
            if (item && !item.SellerUserId) {
              this._GTMService.reverseTransactionData(item);
            }

            this._dialogService.result('Order cancellation request was successful').afterClosed().subscribe((result => {
              this.getOrderDetail();
            }));
          },
          error => {
            // this.resError = {
            //   type: 'error',
            //   message: JSON.parse(error).message
            // };
            let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this._dialogService.result(message).afterClosed().subscribe((result => {
              this.loadingService.display(false);
            }));
            console.log(`error = ${error}`);
            this.alertService.error(error);
          }
        );
      }
    }));
  }

  // back to order list
  backToList() {
    this.location.back();
    // let path = "/orders/" + this.globalService.checkTypeOfOrder(this.orderInfo.states);
    // this._router.navigate([path], { queryParams: {} });
  }

  showCmtForm() {
    this.showCommentForm = true;
    this.initOrderLogForm();
    this.commentFormbuttom = 'Add';
  }

  actionRemask() {
    let _value = this.orderLogForm.value;
    if (_value.id) {
      this.editRemask(_value)
    } else {
      this.addRemask(_value)
    }
  }

  cancelCmt() {
    this.showCommentForm = false;
  }

  editRemask(data) {

    if (data) {
      this._http._updatePut(`${this.appConfig.config.api.orders}/${this.orderInfo.id}/remarks/${data.id}`, data).subscribe(
        res => {
          this.getOrderDetail();
          this.showCommentForm = false;
        },
        error => {
          this.alertService.error(error);
        }
      )
    }

  }

  addRemask(data) {
    // this._dialogService.confirm().afterClosed().subscribe((result => {
    //   if (result) {
    //     this._http._create(`${this.appConfig.config.api.orders}/${this.orderInfo.id}/remarks`, data).subscribe(
    //       res => {
    //         this.getOrderDetail();
    //         this.showCommentForm = false;
    //       },
    //       error => {
    //         this.alertService.error(error);
    //       }
    //     )
    //   }
    // }));
    if (data) {
      this._http._create(`${this.appConfig.config.api.orders}/${this.orderInfo.id}/remarks`, data).subscribe(
        res => {
          this.getOrderDetail();
          this.showCommentForm = false;
        },
        error => {
          this.alertService.error(error);
        }
      )
    }
  }

  removeRemask(_orderLog: object, _i: number) {
    let cancleMsg = 'Are you sure to delete this comment ?';
    this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
      if (result) {
        this._http._delete(`${this.appConfig.config.api.orders}/${this.orderInfo.id}/remarks/${_orderLog['id']}`).subscribe(
          res => {
            this.getOrderDetail();
          },
          error => {
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  openEditRemask(_orderLog: object, _i: number) {
    this.showCommentForm = true;
    this.initOrderLogForm(_orderLog);
    this.commentFormbuttom = 'Save';
    console.log('_orderLog', _orderLog);

  }

  checkAllowToCancel(createdAt) {
    // let currentDate = new Date(); 
    // let _createdAt = new Date(createdAt);
    // var seconds: any  = 0;
    // seconds = (currentDate.getTime() - _createdAt.getTime());
    // var one_day=1000*60*60*24; // miniseconds in 1 day
    // let diffHrs = Math.abs((seconds) / 36e5);
    // if (diffHrs > 24) {
    //   return false;
    // }
    return true;
  }

  downloadTaxInvoice() {
    this._http._getDetail(`${this.appConfig.config.api.download_tax_invoice}/${this.orderId}`).subscribe(
      data => {
        if(data) {
          // let blob = new Blob(['\ufeff' + data], { type: 'application/pdf;charset=utf-8;' });
          let dwldLink = document.createElement('a');
          // let url = URL.createObjectURL(blob);
          let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
          if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute('target', '_blank');
          }
          dwldLink.setAttribute('href', data.url);
          // dwldLink.setAttribute('download', data.name);
          dwldLink.style.visibility = 'hidden';
          document.body.appendChild(dwldLink);
          dwldLink.click();
          document.body.removeChild(dwldLink);
        }
  
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }
}
