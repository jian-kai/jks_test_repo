import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { FaqService } from '../../core/services/faq.service';

@Component({
  selector: 'add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})
export class AddFaqComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  public faqForm: FormGroup;
  public countries: any;
  public directoryCountries: any;
  public languageCodeList: any;

  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private faqService: FaqService,
  ) {
    this.faqForm = this.faqService.initFaq();
    this.globalService.getCountryList().then((data) => {
      this.countries = data;
    });
    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {

  }

  addFAQTranslate() {
    let controlFAQDetails = <FormArray>this.faqForm.controls['faqTranslate'];
    let prDtsCtrl = this.faqService.initFaqTranslate();
    // prDtsCtrl.patchValue({});
    controlFAQDetails.push(prDtsCtrl);
  }

  doAddFaq() {
    let _data = this.faqForm.value;
    this.loadingService.display(true);
    this._http._create(`${this.appConfig.config.api.faqs}`, _data).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
        console.log('data', data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  gotoList() {
    this.location.back();
  }
}
