import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { FaqService } from '../../core/services/faq.service';


@Component({
  selector: 'edit-faq',
  templateUrl: './edit-faq.component.html',
  styleUrls: ['./edit-faq.component.scss']
})
export class EditFaqComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  public faqForm: FormGroup;
  public countries: any;
  public directoryCountries: any;
  public languageCodeList: any;
  public faqId: number;

  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private faqService: FaqService,
  ) {
    this.faqForm = this.faqService.initFaq();
    this.globalService.getCountryList().then((data) => {
      this.countries = data;
    });
    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
    this.activateRoute.params.subscribe(params => {
      this.faqId = +params['id']// (+) converts string 'id' to a number
    });
  }

  ngOnInit() {
    this.getInfo();
  }

  // get info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.faqs}/${this.faqId}`).subscribe(
      data => {
        this.faqForm.patchValue(data.data);
        data.data.faqTranslate.forEach(element => {
          let controlFAQTranslate = <FormArray>this.faqForm.controls['faqTranslate'];
          let prDtsCtrl = this.faqService.initFaqTranslate();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlFAQTranslate.push(prDtsCtrl);
        });
        console.log('data - ', data)
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  addFAQTranslate() {
    let controlFAQDetails = <FormArray>this.faqForm.controls['faqTranslate'];
    let prDtsCtrl = this.faqService.initFaqTranslate();
    // prDtsCtrl.patchValue({});
    controlFAQDetails.push(prDtsCtrl);
  }

  doEditFaq() {
    let _data = this.faqForm.value;
    this.loadingService.display(true);
    this._http._updatePut(`${this.appConfig.config.api.faqs}/${this.faqId}`, _data).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
        console.log('data', data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  gotoList() {
    this.location.back();
  }
}
