import { Component, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';
import { LoadingService } from '../core/services/loading.service';
import { HttpService } from '../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { AlertService } from '../core/services/alert.service';
import { DialogService } from '../core/services/dialog.service';
import { ModalService } from '../core/services/modal.service';
import {GlobalService} from '../core/services/global.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss', '../../assets/sass/table.scss']
})
export class FaqComponent extends ListViewsComponent {
  public faqs : any;
  public isLoading: boolean = true;
  public hasDenied: boolean;
  public countries: any;

  constructor(
    location: Location,
    routerService: RouterService,
    el: ElementRef,
    public globalService: GlobalService,
    private _http: HttpService,
    public modalService: ModalService,
    private alertService: AlertService,
    private appConfig: AppConfig,
    private dialogService: DialogService,
    private loadingService: LoadingService
  ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.hasDenied = this.globalService.hasDenied();
  }

  // get user list
  getData() {
    this.loadingService.display(true);
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    this._http._getList(`${this.appConfig.config.api.faqs}?` + this.options.toString()).subscribe(
      data => {
        this.faqs = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // add new user
  addTerm() {
    this.modalService.reset('add-term-modal');
    this.modalService.open('add-term-modal');
  }

  // edit User
  editTerm(_termItem: any) {
    this.modalService.open('edit-term-modal', _termItem);
  }

  // when user edit,add term
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }

  delete(item: any) {
    this.dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        let pathDelete = `${this.appConfig.config.api.faqs}/${item.id}`;
        this._http._delete(`${pathDelete}`).subscribe(
          data => {
            if (data.ok) {
              this.getData();
            }
          },
          error => {
            this.alertService.error(error);
            console.log('error', error);
          }
        )
      }
    }));
  }
  changedatalist(list){ 
    this.faqs.rows =  list ;  
  }
}
