import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { FaqService } from '../../core/services/faq.service';


@Component({
  selector: 'aq-form',
  templateUrl: './aq-form.component.html',
  styleUrls: ['./aq-form.component.scss']
})
export class AqFormComponent implements OnInit {
  @Input() aqForm;
  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private faqService: FaqService,
  ) {
    
  }

  ngOnInit() {
    // console.log('aqForm ', this.aqForm)
  }
  
  addFAQTranslate() {
    // let controlFAQDetails = <FormArray>this.faqForm.controls['faqTranslate'];
    let prDtsCtrl = this.faqService.initFaqTranslate();
    // prDtsCtrl.patchValue({});
    this.aqForm.push(prDtsCtrl);
  }

  removeQA(_i: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        if (this.aqForm.controls[_i].value.id) {
          let pathDelete = `${this.appConfig.config.api.faqs}/faq-translates/${this.aqForm.controls[_i].value.id}`;
          this._http._delete(`${pathDelete}`).subscribe(
            data => {
              if (data.ok) {
                this.aqForm.removeAt(_i);
              }
            },
            error => {
              this.alertService.error(error);
              console.log('error', error);
            }
          )
        } else {
          this.aqForm.removeAt(_i);
        }
      }
    }));
  }

}
