import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AqFormComponent } from './aq-form.component';

describe('AqFormComponent', () => {
  let component: AqFormComponent;
  let fixture: ComponentFixture<AqFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AqFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AqFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
