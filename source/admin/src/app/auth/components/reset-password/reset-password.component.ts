import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { numberValidator } from '../../../core/directives/number-validator.directive';
import {Router, ActivatedRoute, Params, NavigationCancel } from '@angular/router';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import * as $ from 'jquery';

import { HttpService } from '../../../core/services/http.service';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AuthService } from '../../../auth/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { CountriesService } from '../../../core/services/countries.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  public resetPasswordForm: FormGroup;
  public stage: string; // form | result
  public email: string;
  public token: string;

  constructor(private builder: FormBuilder,
    private storage: StorageService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private _http: HttpService,
    private appConfig: AppConfig,
    private countriesService: CountriesService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location) {

      this.resetPasswordForm = builder.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
      });

    }

  ngOnInit() {
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.email = decodeURIComponent(params.get('email'));
    this.token = params.get('token');
    if (!this.email || !this.token) {
      console.log('Invalid link!')
    }
    this.validateToken();
  }

  validateToken() {
    let credentials = { token: this.token  };
    this._http._create(`${this.appConfig.config.api.validate_token}`, credentials).subscribe(
      data => {
        if (data.ok) {
          this.stage = "form";
        } else {
          this.reSendMail();
        }
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  reSendMail() {
    let credentials = { email: this.email  };
    this._http._create(`${this.appConfig.config.api.reset_password}`, credentials).subscribe(
      data => {
        this.stage = "resend";
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  doReset() {
    // this.stage = 'forgotPassword';
    let credentials = { token: this.token , password : this.resetPasswordForm.value.password };
    this._http._create(`${this.appConfig.config.api.update_password}`, credentials).subscribe(
      data => {
        if (data.ok) {
          this.stage = "success";
        }        
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  continue() {
    this.router.navigate(['/']);
  }

}
