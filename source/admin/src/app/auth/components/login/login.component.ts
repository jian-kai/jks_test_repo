import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { UsersService } from '../../services/users.service';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../../core/services/alert.service';
import { StorageService } from '../../../core/services/storage.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private _usersService: UsersService;
  private _router: Router;
  public loginForm: FormGroup;
  public stage: string; // signin | emailForm | sendEmail
  public emailForm: FormGroup;

  constructor(
    el: ElementRef,
    usersService: UsersService,
    router: Router,
    builder: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    private storage: StorageService,
    private _http: HttpService,
    private appConfig: AppConfig,
  ) {
    this._usersService = usersService;
    this._router = router;
    this.isLogged();
    // set controls for email form
    this.emailForm = builder.group({
      email: ['', Validators.required],
    });

    // set controls for login form
    this.loginForm = builder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.stage = 'signin';
  }

  // check if logged in so redirect
  isLogged() {
    if (this.storage.getCurrentUserItem()) {
      // logged in so redirect to dashboard page
      this._router.navigate([''], { queryParams: {} });
    }
  }

  onSubmit(credentials) {
    this._usersService.login(credentials).subscribe(
      data => {
        this.alertService.clearMessage();
        // store token into localstore
        this.storage.setAuthToken(data.token);
        this._usersService._loggedIn.next(true);
        this.storage.setCurrentUser(data.user);
        this._router.navigate([''], { queryParams: {} });
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  // resetPassword
  resetPassword() {
    this.stage = 'emailForm';
  }

  // send mail contains link reset pass
  sendMailResetPw() {
    let credentials = { email: this.emailForm.value.email };
    this._http._create(`${this.appConfig.config.api.reset_password}`, credentials).subscribe(
      data => {
        this.stage = "sendEmail";
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // send email again
  sendEmailAgain() {
    this.sendMailResetPw();
  }

  // back to sign in
  backtoSignin() {
    this.stage = 'signin';
  }


}
