import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';

import { StorageService } from '../../core/services/storage.service';
import { RequestService } from '../../core/services/request.service';
import { AppConfig } from 'app/config/app.config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class UsersService {

  _loggedIn = new BehaviorSubject(false);
  public _router;

  constructor(private http: Http,
    private storage: StorageService,
    private request: RequestService,
    private appConfig: AppConfig,
    private router: Router,) {
    if (!!this.storage.getAuthToken()) {
      this._loggedIn.next(true);
    }
    this._router = router;
  }

  login(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.login}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  // register
  _register(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.register}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  logout() {
    this.storage.removeAuthToken();
    this.storage.removeCurrentUser();
    this._loggedIn.next(false);
    this._router.navigate(['/login']);
    // window.location.reload();
  }

  isLoggedIn() {
    return this._loggedIn.getValue();
  }

  getLoggedIn() {
    return this._loggedIn;
  }
  // response data successfully
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  // handle if have errors
  private handleErrorObservable(error: Response | any) {
    if (error.status === 400) {
      return Observable.throw(error._body);
    }
    return Observable.throw(error.json());
  }

}
