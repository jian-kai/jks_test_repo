import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRoute, Params, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { StorageService } from '../../core/services/storage.service';
import { GlobalService } from '../../core/services/global.service';

@Injectable()
export class AuthService implements CanActivate {
	public code: string;
	constructor(private location: Location, private router: Router,
		private storage: StorageService,
		public globalService: GlobalService
	) {
		let params = new URLSearchParams(this.location.path(false).split('?')[1]);
		this.code = params.get('code');
		if (this.code) {
			// alert(this.code);
		}

	}

	public auth(_provider: string, authConfig: any) {
		localStorage.setItem('authConfig', authConfig);
		localStorage.setItem('provider', _provider);
		if (_provider === 'facebook') {
			window.location.href = 'https://www.facebook.com/v2.8/dialog/oauth?client_id=' + authConfig.clientId + '&redirect_uri=' + authConfig.redirectURI + '&scope=email';
		}
	}
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		if (this.storage.getCurrentUserItem()) {
			let currentUser = this.storage.getCurrentUserItem();
			let _permission_of_path : any = {};
			if (currentUser && currentUser.Role && currentUser.Role.roleDetails && currentUser.Role.roleDetails.length > 0) {
				currentUser.Role.roleDetails.forEach(per => {
					if (per.Path && state.url.includes(per.Path.pathValue)) {
						_permission_of_path = per;
					}
				});
			}

			let _spl = state.url.split('/');
			let isAddOrEdit : boolean = false;
			console.log('_spl ', _spl);
			if (_spl && _spl.length > 0) {
				_spl.forEach(val => {
					if (val === 'add' && _permission_of_path.canCreate) {
					}
					if (val === 'edit' && _permission_of_path.canEdit) {
					}
				});
			}
			console.log('state.url ', state.url)
			if ((!isAddOrEdit && _permission_of_path.canView) || state.url === '/' || state.url === '/settings') {
				return true;
			} else {
				this.router.navigate(['/'], { queryParams: {} });
				return false;
			}
		}

		// not logged in so redirect to login page with the return url
		this.router.navigate(['/login'], { queryParams: {} });
		return false;
	}

}