import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { UserService } from '../../core/services/user.service';


@Component({
  selector: 'app-edit-privacy',
  templateUrl: './edit-privacy.component.html',
  styleUrls: ['./edit-privacy.component.scss'],
  providers: [UserService]
})
export class EditPrivacyComponent implements OnInit {
  public privacyForm: FormGroup;
  public countries: any;
  public privacyId: number;
  public froalaEditorOptions = {
    heightMin: 300,
  }

  public isLoading: boolean;

  public languageCodeList: any;

  public trumbowygEditorOptions: any;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private userService: UserService,
    private location: Location) {
    this.privacyForm = builder.group({
      CountryId: ['', Validators.required],
      content: ['', Validators.required],
      langCode: ['', Validators.required],
    });
    this.globalService.getCountryList().then((data) => {
      this.countries = data;
    });
    this.activateRoute.params.subscribe(params => {
      this.privacyId = +params['id']// (+) converts string 'id' to a number
    });

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {
    this.getInfo(this.privacyId);
  }

  doEditprivacy() {
    let _data = this.privacyForm.value;
    this.loadingService.display(true);
    this._http._updatePut(`${this.appConfig.config.api.privacy}/${this.privacyId}`, _data).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
        console.log('data', data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  // get user info by ID
  getInfo(_privacyId: number) {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.privacy}/${_privacyId}`).subscribe(
      data => {
        this.privacyForm.patchValue(data.data);
        this.trumbowygEditorOptions = {
          formGroup: this.privacyForm,
          formControlName: 'content'
        }
        console.log('data - ', data)
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error)
      }
    )
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }

}

