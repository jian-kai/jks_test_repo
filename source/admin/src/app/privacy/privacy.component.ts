import { Component, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';
import { LoadingService } from '../core/services/loading.service';
import { HttpService } from '../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { AlertService } from '../core/services/alert.service';
import { DialogService } from '../core/services/dialog.service';
import { ModalService } from '../core/services/modal.service';
import { GlobalService } from '../core/services/global.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss', '../../assets/sass/table.scss']
})
export class PrivacyComponent extends ListViewsComponent {
  public privacy : any = {};
  public isLoading: boolean;

  constructor(
    location: Location,
    routerService: RouterService,
    el: ElementRef,
    private _http: HttpService,
    public modalService: ModalService,
    private alertService: AlertService,
    private appConfig: AppConfig,
    private dialogService: DialogService,
    private loadingService: LoadingService,
    public globalService: GlobalService
  ) {
    super(location, routerService, el);
  }

  // get user list
  getData() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.privacy}?` + this.options.toString()).subscribe(
      data => {
        // this.privacy = data.data;
        this.privacy['rows'] = data.data;
        this.totalItems = data.data.length;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error([error]);
      }
    )
  }

  // add new user
  addTerm() {
    this.modalService.reset('add-term-modal');
    this.modalService.open('add-term-modal');
  }

  // edit User
  editTerm(_termItem: any) {
    this.modalService.open('edit-term-modal', _termItem);
  }

  // when user edit,add term
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }

}
