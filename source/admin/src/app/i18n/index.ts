import { en } from './en';
import { vn } from './vn';
import { ko } from './ko';

export const translator = {
  en: en,
  vn: vn,
  ko: ko
}