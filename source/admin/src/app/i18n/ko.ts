export const ko = {
  menu: {
    products: '제작품',
    subscriptions: '구독',
    articles: '조항',
    helps: '고객지원',
    support: '지원하다',
    about: '약',
    login: '로그인',
    logout: '로그 아웃',
    register: '레지스터',
    checkout: '점검',
    faq: '자주하는 질문',
    trackOrder: '내 주문 추적'
  },
  product: {
    field: {
    product: '제작품',
    price: '가격',
    quantity: '수량',
    subtotal: '소계',
    }    
  
  },
  // message
  msg: {
    validation: {
      required: 'Please enter a value for this field.',
      requiredMultiFile: 'Please select at least one file to upload.',
      requiredFile: 'Please select a file to upload.',
      character_string: 'Contents for this textbox must have at least one Alphabetic character',
      special_characters: "Can't enter special characters",
      select_required: 'Please select a value for this field.',
      email: 'This field must be a valid email address.',
      maxlength: 'This field can be at most XXX characters long.',
      minlength: 'This field can be at least XXX characters long',
      password_format: 'Password should be atleast 6 characters long and should contain one number,one character',
      password_not_match: 'Password not match',
    }
  }
};
  