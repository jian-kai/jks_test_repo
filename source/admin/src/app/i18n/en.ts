export const en = {
  menu: {
    products: 'Products',
    subscriptions: 'Subscriptions',
    articles: 'Articles',
    helps: 'Helps',
    support: 'Support',
    about: 'About',
    login: 'Login',
    logout: 'Logout',
    register: 'Register',
    checkout: 'Checkout',
    faq: 'FAQ',
    trackOrder: 'Track my order'
  },
  product: {
    field: {
      product: 'Product',
      price: 'Price',
      quantity: 'Quantity',
      subtotal: 'Subtotal',
    }    

  },
  // dialog
  dialog: {
    confirm: {
      title: 'Confirm',
      content: 'Are you sure?'
    }
  },
  // message
  msg: {
    validation: {
      required: 'Please enter a value for this field.',
      requiredMultiFile: 'Please select at least one file to upload.',
      requiredFile: 'Please select a file to upload.',
      character_string: 'Contents for this textbox must have at least one Alphabetic character',
      special_characters: "Can't enter special characters",
      select_required: 'Please select a value for this field.',
      email: 'This field must be a valid email address.',
      maxlength: 'This field can be at most {{value}} characters long.',
      minlength: 'This field can be at least XXX characters long',
      password_format: 'Password should be at least 6 characters long and should contain one number,one character',
      password_not_match: 'Password not match',
      selectListToSubscribe: 'Please select at least one list to subscribe to',
      selectListToAdd: 'Please select at least one list to add to',
      selectUserOrGroupEmail: 'Please select at least one user or a group of email'
    }
  }
};
