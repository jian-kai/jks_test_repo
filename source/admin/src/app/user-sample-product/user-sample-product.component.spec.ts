import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSampleProductComponent } from './user-sample-product.component';

describe('UserSampleProductComponent', () => {
  let component: UserSampleProductComponent;
  let fixture: ComponentFixture<UserSampleProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSampleProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSampleProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
