import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../core/services/dialog.service';
import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';

@Component({
  selector: 'app-user-sample-product',
  templateUrl: './user-sample-product.component.html',
  styleUrls: ['./user-sample-product.component.scss', '../../assets/sass/table.scss']
})
export class UserSampleProductComponent extends ListViewsComponent {
  public userList : any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userItem: any;
  public showing: string;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public countries: any;
  public fromDate: any;
  public toDate: any;
  public orderByDate: string;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    public modalService: ModalService,
    private _dialogService: DialogService,
    routerService: RouterService,
    el: ElementRef,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    } else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 1);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }

    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    } else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.customer_users}/ba-guest?` + this.options.toString()).subscribe(
      data => {
        this.userList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // sort by order_by
  sortByName(type: string) {
    this.orderBy = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["firstName"]');
    } else {
      this.filterby('orderBy', '["firstName_' + type + '"]');
    }
    
    this.getData();
  }
  
  // sort by date
  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }
  
    changeFromDate(_fromDate) {
      if(_fromDate) {
        this.filterby('fromDate', this.globalService.formatDate(_fromDate));
      } else {
        this.filterby('fromDate', 'all');
      }
      this.fromDate = new Date(_fromDate);
      // this.getData();
    }
  
    changeToDate(_toDate) {
      if(_toDate) {
        this.filterby('toDate', this.globalService.formatDate(_toDate));
      } else {
        this.filterby('toDate', 'all');
      }
      this.toDate = new Date(_toDate);
    }

  // add new user
  addUserCustomer() {
    this.modalService.open('add-user-customer-modal');
  }

  // edit User
  editUserCustomer(_userItem : number) {
    this.userItem = _userItem;
    this.modalService.open('edit-user-customer-modal', this.userItem);
  }
 
  // when user edit profile
  hasChangeUserCustomer(status: boolean) {
    if (status)
      this.getData();
  }

  downloadCSV() {
    // let optParam = {
    //   fromDate: this.formatDate(this.fromDate),
    //   toDate:  this.formatDate(this.toDate),
    //   orderBy: '["createdAt_DESC"]'
    // };
    
    // let params = this.globalService._URLSearchParams(optParam);
    this.loadingService.display(true);
    this._http._getDetailCsv(`${this.appConfig.config.api.customer_users}/ba-guest/download?` + this.options.toString()).subscribe(
      data => {
        let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement('a');
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
          dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', `Users Sample Product for ${this.formatDate(this.fromDate)} to ${this.formatDate(this.toDate)}.csv`);
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )

  }
  
  // re-format date to param
  formatDate(_date: Date) {
    let year = _date.getFullYear().toString();
    let month = (_date.getMonth() + 1).toString();
    let day = _date.getDate().toString();

    if(month.length === 1) {
      month = '0' + month;
    }
    if(day.length === 1) {
      day = '0' + day;
    }

    return year + '-' + month + '-' + day;
  }
}
