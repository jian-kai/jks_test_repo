export class BaseConfig {
  private config: any;

  constructor(private options: any) { 
    this.config = {
      apiURL: options.apiURL,
      baseUrl: options.baseUrl,
      multipleLanguage: {
        countryCodeService: 'http://ws.geonames.org/countryCode',
        username: 'kimthi'
      },
      api: {
        login: options.apiURL + '/api/admin/login',
        register: options.apiURL + '/api/users/register',
        reset_password: options.apiURL + '/api/admin/reset-password',
        users: options.apiURL + '/api/admin',
        customers: options.apiURL + '/api/admin/users/[id]',
        customer_users: options.apiURL + '/api/admin-users',
        change_password: options.apiURL + '/api/admin/[userID]/password',
        roles: options.apiURL + '/api/admin/roles',
        checkout: options.apiURL + '/api/checkout',
        // delivery_address_list: options.apiURL + '/api/users/[]/deliveries',
        delivery_address: options.apiURL + '/api/users/[userID]/deliveries',
        // edit_delivery_address: options.apiURL + '/api/users/[userID]/deliveries',

        products: options.apiURL + '/api/admin-products',
        product_details: options.apiURL + '/api/products',
        product_types: options.apiURL + '/api/product-types',
        admin_product_types: options.apiURL + '/api/admin-product-types',
        exports:options.apiURL + '/api/file-upload/[adminId]',

        articles: options.apiURL + '/api/admin-articles',
        article_types: options.apiURL + '/api/admin-article-types',
        orders: options.apiURL + '/api/orders',
        bulk_orders: options.apiURL + '/api/bulk-orders',
        admin_orders: options.apiURL + '/api/admin-orders',
        place_order: options.apiURL + '/api/payment/orders',
        order_pendding: options.apiURL + '/api/orders/pending',
        order_history: options.apiURL + '/api/orders/history',
        order_states: options.apiURL + '/api/orders/states',

        subscriptions: options.apiURL + '/api/subscriptions',
        plan_types: options.apiURL + '/api/admin-plan-types',
        plan_types_create_update: options.apiURL + '/api/plan-types',

        plan_list: options.apiURL + '/api/admin-plans',
        plans: options.apiURL + '/api/plans',
        subscribers_sellers: options.apiURL + '/api/subscribers/sellers',
        subscribers_users: options.apiURL + '/api/subscribers/users',
        subscribers_free_trial: options.apiURL + '/api/subscribers/trial/users',

        promotions: options.apiURL + '/api/admin-promotions',

        countries: options.apiURL + '/api/countries',
        countries_list: options.apiURL + '/api/admin-country-lists',
        countries_subscriber: options.apiURL + '/api/countries/subscriber',
        admin_countries: options.apiURL + '/api/admin-countries',
        directory_countries: options.apiURL +  '/api/directory-countries',
        stripe_pubkey: options.apiURL + '/api/countries/stripe-pubkey',
        stripe_checkout: options.apiURL + '/payment-gateway/stripe',
        checkEmail: options.apiURL + '/api/users/check-email',
        resendActiveEmail: options.apiURL + '/api/users/resend-active-email',
        validate_token: options.apiURL + '/api/admin/validate-token',
        update_password: options.apiURL + '/api/admin/update-password',

        sales_reports: options.apiURL + '/api/sales-reports',
        sales_reports_download: options.apiURL + '/api/sales-reports/download',
        sales_reports_appco: options.apiURL + '/api/sales-reports/appco',
        sales_reports_appco_download: options.apiURL + '/api/sales-reports/appco/download',
        sales_reports_mo: options.apiURL + '/api/sales-reports/mo',
        sales_reports_mo_download: options.apiURL + '/api/sales-reports/mo/download',
        channel_type: options.apiURL + '/api/sellers/channel-type',
        location_code: options.apiURL + '/api/sales-reports/location-code',
        user_role: options.apiURL + '/api/roles',
        path: options.apiURL + '/api/path',

        order_upload: options.apiURL + '/api/order-upload',
        order_upload_download: options.apiURL + '/api/order-upload/download',
        terms: options.apiURL + '/api/admin-terms',
        privacy: options.apiURL + '/api/admin-privacy',
        faqs: options.apiURL + '/api/admin-faqs',
        faqs_types: options.apiURL + '/api/faqs/types',
        states: options.apiURL + '/api/admin-states',
        product_types_translate: options.apiURL + '/api/product-types-translate',
        plan_type_translates: options.apiURL + '/api/plan-type-translates',
        supported_languages: options.apiURL + '/api/admin-supported-languages',
        
        free_trial: options.apiURL + '/api/plans/free-trial',
        admin_user_cards: options.apiURL + '/api/admin/users/[userID]/cards',
        place_order_trial: options.apiURL + '/api/payment/trial-plan',

        marketing_offices: options.apiURL + '/api/marketing-offices',
        plan_groups: options.apiURL + '/api/admin-plan-groups',

        check_tax_invoice: options.apiURL + '/api/file-upload/check-tax-invoice-exists',
        download_tax_invoice: options.apiURL + '/api/file-upload/download/tax-invoice',
        download_bulk_tax_invoice: options.apiURL + '/api/file-upload/download/bulk-tax-invoice',
        download_file: options.apiURL + '/api/file-upload/download',
        plan_option_translate: options.apiURL + '/api/plans/plan-option-translate',
        message: options.apiURL + '/api/admin-message',
      },
      auth: {
        facebook: {
          authEndpoint: options.baseUrl + '/auth/facebook',
          redirectURI : options.baseUrl + '/auth'
        }
      },
      supportedLanguage: [
        {
          key: 'en',
          value: 'Malaysia',
          countryCode: 'MY'
        },
        {
          key: 'en',
          value: 'Singapore',
          countryCode: 'SG'
        },
        {
          key: 'en',
          value: 'Thailand',
          countryCode: 'TH'
        },
        {
          key: 'vn',
          value: 'Vietname',
          countryCode: 'VN'
        }
      ],
      roles: {
        super: "super admin",
        admin: "admin",
        staff: "staff",
        report: 'report'
      },
      order_status: {
        pending: [
          "On Hold",
          "Payment Received",
          "Processing Order",
          "Deliver Order"
        ],
        history: [
          "Order Received",
          "Cancel"
        ]
      },
      limitRows: 50,
    };
  }

  public getConfig() {
    return this.config;
  }
}