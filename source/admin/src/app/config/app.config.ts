import { Inject, Injectable, Output , EventEmitter} from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BaseConfig } from './base.config';
import { StorageService } from '../core/services/storage.service';
import { RequestService } from '../core/services/request.service';
import { CountriesService } from '../core/services/countries.service';

@Injectable()
export class AppConfig {
  @Output() configEvent: EventEmitter<Object> = new EventEmitter();
  public config: any = null;
  private env: any = null;

  constructor(private http: Http, private storage: StorageService, private request: RequestService, private countriesService: CountriesService) {
    countriesService.country.subscribe(country => {
      this.getStripeConfig();
    });
  }

  /**
   * Use to get the data found in the second file (config file)
   */
  public getConfig(key: any) {
    return key ? this.config[key] : this.config;
  }

  /**
   * Use to get the data found in the first file (env file)
   */
  public getEnv(key: any) {
    return this.env[key];
  }

  private getWorkingEnv() {
    return new Promise((resolve, reject) => {
      this.http.get('./../../app/config/env.json').map(res => res.json()).catch((error: any): any => {
        console.log('Configuration file "env.json" could not be read');
        resolve(true);
        return Observable.throw(error.json().error || 'Server error');
      }).subscribe((envResponse) => {
        resolve(envResponse);
      });
    });
  }

  private getEnvConfig(envResponse) {
    this.env = envResponse;
    return new Promise((resolve, reject) => {
      let request: any = null;

      envResponse.env = envResponse.env || 'local';
      request = this.http.get('./../../app/config/environment/config.' + envResponse.env + '.json');

      if (request) {
        request
          .map(res => res.json())
          .catch((error: any) => {
            console.error('Error reading ' + envResponse.env + ' configuration file');
            resolve(error);
            return Observable.throw(error.json().error || 'Server error');
          })
          .subscribe((responseData) => {
            this.config = responseData;
            let baseConfig = new BaseConfig(responseData);
            Object.assign(this.config, baseConfig.getConfig());
            resolve(true);
            console.log('app finish');
          });
      } else {
        console.error('Env config file "env.json" is not valid');
        resolve(true);
      }
    });
  }

  private getStripeConfig() {
    return new Promise((resolve, reject) => {
      // set country code to header
      this.http.get(`${this.config.api.stripe_pubkey}?type=website`, {headers: this.request.getHeaders('json')})
        .map(res => res.json())
        .subscribe((responseData) => {
          this.config.stripePubkey = responseData.stripePub;
          this.configEvent.emit(this.config);
          resolve(true);
        });
    });
  }

  /**
   * This method:
   *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
   *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
   */
  public load() {
    return this.getWorkingEnv()
      .then(envResponse => this.getEnvConfig(envResponse))
      .then(() => this.getStripeConfig());
  }
}