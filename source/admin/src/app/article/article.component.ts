import { Component, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';
import { LoadingService } from '../core/services/loading.service';
import { HttpService } from '../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { AlertService } from '../core/services/alert.service';
import { DialogService } from '../core/services/dialog.service';
import { ModalService } from '../core/services/modal.service';
import { GlobalService } from '../core/services/global.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss', '../../assets/sass/table.scss']
})
export class ArticleComponent extends ListViewsComponent {
  public articles : any = {};
  public articleTypes: any;
  public isLoading: boolean;
  public orderBy: string = "ASC";

  constructor(
    location: Location,
    routerService: RouterService,
    el: ElementRef,
    private _http: HttpService,
    public modalService: ModalService,
    private alertService: AlertService,
    private appConfig: AppConfig,
    private dialogService: DialogService,
    private loadingService: LoadingService,
    public globalService: GlobalService
  ) {
    super(location, routerService, el);
  }

  // get user list
  getData() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.articles}?` + this.options.toString()).subscribe(
      data => {
        // this.privacy = data.data;
        this.articles = data;
        this.totalItems = data.rows;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error([error]);
      }
    )

    this.getArticleTypes();
  }

	// get product type list
	getArticleTypes() {
    this._http._getList(`${this.appConfig.config.api.article_types}`).subscribe(
      data => {
        this.articleTypes = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
	}

  // add new user
  addTerm() {
    this.modalService.reset('add-term-modal');
    this.modalService.open('add-term-modal');
  }

  // edit User
  editTerm(_termItem: any) {
    this.modalService.open('edit-term-modal', _termItem);
  }

  // when user edit,add term
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }

  // delete Article 
  deleteArticle(_id: number) {
    this.dialogService.confirm().afterClosed().subscribe(result => {
      if (result) {
        this.loadingService.display(true);
        this._http._delete(`${this.appConfig.config.api.articles}/${_id}`).subscribe(
          data => {
            this.loadingService.display(false);
            this.getData();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
            this.dialogService.result(JSON.parse(error).message).afterClosed().subscribe(result => {
            })
          }
        )
      }
    });
  }

  // sort by order_by
  sortByTitle(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', '["title"]');
    } else {
      this.filterby('orderBy', '["title_DESC"]');
    }
  }

}
