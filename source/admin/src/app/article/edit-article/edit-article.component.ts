import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { DialogService } from '../../core/services/dialog.service';
import { LoadingService } from '../../core/services/loading.service';
import { FileValidator } from '../../core/directives/required-file.directive';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss'],
  providers: [DatePipe]
})
export class EditArticleComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public currentUser: any;
  public articleForm: FormGroup;
  public articleInfo: any;
  public isLoading: boolean = true;
  public articleId : number;
  public articleTypes: any;

  constructor(
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private _dialogService: DialogService,
    private location: Location,
    private loadingService: LoadingService,
    private activateRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    this.articleForm = builder.group({
      id: [''],
      hashTag: ['', Validators.required],
      title: ['', Validators.required],
      content: ['', Validators.required],
      ArticleTypeId: ['', Validators.required],
      bannerUrl: ['', [FileValidator.validate]],
      isFeatured: [''],
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.activateRoute.params.subscribe(params => {
      this.articleId = +params['id']// (+) converts string 'id' to a number
    });
  }

  ngOnInit() {
    this.getInfo();
  }

  // get user info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.articles}/${this.articleId}`).subscribe(
      data => {
        this.getArticleTypes();
        this.articleForm.patchValue(data);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

	// get article type list
	getArticleTypes() {
    this._http._getList(`${this.appConfig.config.api.article_types}`).subscribe(
      data => {
        this.articleTypes = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
	}

  setInitData(_catItem) {
    this.articleInfo = _catItem;
    this.articleForm = this.globalService.setValueFormBuilder(this.articleForm, _catItem);
  }

  // do edit Article
  doEditArticle() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        // let credentials = this.categoryForm.value;
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let articleForm = this.articleForm;
        for (var key in articleForm.value) {
          if (articleForm.value[key]) {
            if (key === 'expiredAt' || (key === 'activeAt')) {
              formData.append(key, this.datePipe.transform(articleForm.value[key], 'yyyy-MM-dd'));
            } else {
              formData.append(key, articleForm.value[key]);
            }
          }
        }
        this._http._updatePut(`${this.appConfig.config.api.articles}/${this.articleId}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }
}