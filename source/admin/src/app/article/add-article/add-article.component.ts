import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';

import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { DialogService } from '../../core/services/dialog.service';
import { LoadingService } from '../../core/services/loading.service';
import { FileValidator } from '../../core/directives/required-file.directive';

@Component({
  selector: 'add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss'],
  providers: [DatePipe]
})
export class AddArticleComponent implements OnInit {
  public articleForm: FormGroup;
  public languageCodeList: any;
  public articleTypes: any;


  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private datePipe: DatePipe) {
    this.articleForm = builder.group({
      hashTag: ['', Validators.required],
      title: ['', Validators.required],
      content: ['', Validators.required],
      ArticleTypeId: ['', Validators.required],
      bannerUrl: ['', [FileValidator.validate]],
      isFeatured: [''],
    });
    
    this.getArticleTypes();
  }

  ngOnInit() {
  }

	// get product type list
	getArticleTypes() {
    this._http._getList(`${this.appConfig.config.api.article_types}`).subscribe(
      data => {
        this.articleTypes = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
	}

  doAddArticle() {
    this._dialogService.confirm().afterClosed().subscribe(result => {
      if (result) {
        // let _data = this.articleForm.value;
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let articleForm = this.articleForm;
        for (var key in articleForm.value) {
          if (articleForm.value[key]) {
            if (key === 'expiredAt' || (key === 'activeAt')) {
              formData.append(key, this.datePipe.transform(articleForm.value[key], 'yyyy-MM-dd'));
            } else {
              formData.append(key, articleForm.value[key]);
            }
          }
        }
        this._http._createWithFormData(`${this.appConfig.config.api.articles}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
            console.log('data', data);
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    });
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }

}
