import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'app-edit-article-type-modal',
  templateUrl: './edit-article-type-modal.component.html',
  styleUrls: ['./edit-article-type-modal.component.scss']
})
export class EditArticleTypeModalComponent extends ModalComponent {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public currentUser: any;
  public articleTypeForm: FormGroup;
  public articleTypeItem: any;
  public languageCodeList: any;
  public countries: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'app-edit-article-type-modal';
    this.articleTypeForm = builder.group({
      type: ['', Validators.required],
      langCode: ['', Validators.required],
      CountryId: ['', Validators.required],
    });
    this.globalService.getCountryList().then((data) => {
      this.countries = data;
    });
    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  setInitData(_item) {
    this.articleTypeItem = _item;
    console.log('this.articleTypeItem', this.articleTypeItem)
    this.articleTypeForm.patchValue(this.articleTypeItem);
  }

  // do Add User
  doEditArticleType() {
    let datas = this.articleTypeForm.value;
    this._http._updatePut(`${this.appConfig.config.api.article_types}/${this.articleTypeItem.id}`, datas).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }


}

