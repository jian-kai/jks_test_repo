import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditArticleTypeModalComponent } from './edit-article-type-modal.component';

describe('EditArticleTypeModalComponent', () => {
  let component: EditArticleTypeModalComponent;
  let fixture: ComponentFixture<EditArticleTypeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditArticleTypeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditArticleTypeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
