import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddArticleTypeModalComponent } from './add-article-type-modal.component';

describe('AddArticleTypeModalComponent', () => {
  let component: AddArticleTypeModalComponent;
  let fixture: ComponentFixture<AddArticleTypeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddArticleTypeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddArticleTypeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
