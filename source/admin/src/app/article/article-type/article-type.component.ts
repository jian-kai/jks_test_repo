import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';

@Component({
  selector: 'app-article-type',
  templateUrl: './article-type.component.html',
  styleUrls: ['./article-type.component.scss', '../../../assets/sass/table.scss']
})
export class ArticleTypeComponent extends ListViewsComponent {
  public articleTypes: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public showing: string;
  public catID: number;
  public totalItems: number = 0;
  public options: URLSearchParams;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    this.loadingService.display(true);

    this._http._getList(`${this.appConfig.config.api.article_types}?` + this.options.toString()).subscribe(
				data => {
          this.articleTypes = data;
          this.totalItems = data.count;
          this.loadingService.display(false);
				},
				error => {
          this.loadingService.display(false);
					this.alertService.error(error);
				}
			)
  }

  // add new Article Types
  addArticleType() {
    this.modalService.reset('app-add-article-type-modal');
    this.modalService.open('app-add-article-type-modal');
  }

  // edit Article Types
  editArticleType(_item: any) {
    this.modalService.open('app-edit-article-type-modal', _item);
  }

  // delete Article Types 
  deleteArticleTypes(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe(result => {
      if (result) {
        this.loadingService.display(true);
        this._http._delete(`${this.appConfig.config.api.article_types}/${_id}`).subscribe(
          data => {
            this.loadingService.display(false);
            this.getData();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
            this._dialogService.result(JSON.parse(error).message).afterClosed().subscribe(result => {
            })
          }
        )
      }
    });
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', '["type"]');
    } else {
      this.filterby('orderBy', '["type_DESC"]');
    }
  }

  resetPage() {
    // this.options.page = 1;
  }

  // when user edit profile
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }

  // show number per page
  changePerPage(limit: number) {
    this.resetPage();
    // this.options.limit = limit;
    this.getData();
  }

}
