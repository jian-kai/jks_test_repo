import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanGroupsComponent } from './plan-groups.component';

describe('PlanGroupsComponent', () => {
  let component: PlanGroupsComponent;
  let fixture: ComponentFixture<PlanGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
