import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';
import { DialogService } from '../../../core/services/dialog.service';
import { PlanService } from '../../../core/services/plan.service';
import { FileValidator } from '../../../core/directives/required-file.directive';

@Component({
  selector: 'edit-plan-group-modal',
  templateUrl: './edit-plan-group.component.html',
  styleUrls: ['./edit-plan-group.component.scss'],
  providers: [DatePipe]
})
export class EditPlanGroupComponent implements OnInit {
  public planGroupForm: FormGroup;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  // private planTypeItem: any;
  public isLoading: boolean = true;
  public planGroupId : number;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private datePipe: DatePipe,
    public modalService: ModalService,
    private _dialogService: DialogService,
    el: ElementRef,
    private planService: PlanService,
    private location: Location,
    private activateRoute: ActivatedRoute,
  ) {
      this.planGroupForm = builder.group({
        id: [''],
        url: ['', [FileValidator.validate]],
        planGroupTranslate: builder.array([], Validators.compose([Validators.required])),
      });
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
      this.activateRoute.params.subscribe(params => {
        this.planGroupId = +params['id']// (+) converts string 'id' to a number
      });
  }
  
  ngOnInit() {
    this.getInfo();
  }

  // get user info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.plan_groups}/${this.planGroupId}`).subscribe(
      data => {
        this.planGroupForm.patchValue(data.data);
        data.data.planGroupTranslate.forEach(element => {
          let controlTranslate = <FormArray>this.planGroupForm.controls['planGroupTranslate'];
          let prDtsCtrl = this.planService.initPlanGroupTranslate();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlTranslate.push(prDtsCtrl);
        });
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  // do edit plan group
  doEditPlanGroup() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        // let credentials = this.planGroupForm.value;
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let planGroupForm = this.planGroupForm;
        for (var key in planGroupForm.value) {
          switch (key) {
            case "planGroupTranslate":
              let i = 0;
              let indexplanDetails = 0;
              planGroupForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;
                  }
                  formData.append(`planGroupTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            default:
              if (planGroupForm.value[key]) {
                if (key === 'expiredAt' || (key === 'activeAt')) {
                  formData.append(key, this.datePipe.transform(planGroupForm.value[key], 'yyyy-MM-dd'));
                } else {
                  formData.append(key, planGroupForm.value[key]);
                }
              }
              break;
          }
        }
        this._http._updatePut(`${this.appConfig.config.api.plan_groups}/${this.planGroupId}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }
}