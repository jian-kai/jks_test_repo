import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPlanGroupComponent } from './edit-plan-group.component';

describe('EditPlanGroupComponent', () => {
  let component: EditPlanGroupComponent;
  let fixture: ComponentFixture<EditPlanGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPlanGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPlanGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
