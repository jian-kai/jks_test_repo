import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { FileValidator } from '../../../core/directives/required-file.directive';

@Component({
  selector: 'add-plan-group-modal',
  templateUrl: './add-plan-group.component.html',
  styleUrls: ['./add-plan-group.component.scss'],
  providers: [DatePipe]
})
export class AddPlanGroupComponent implements OnInit {
  public planGroupForm: FormGroup;
  public isLoading: boolean = true;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    private location: Location,
    private datePipe: DatePipe,
    el: ElementRef,) {
      this.planGroupForm = builder.group({
        url: ['', [FileValidator.validate]],
        planGroupTranslate: builder.array([], Validators.compose([Validators.required])),
      });
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
  }

  ngOnInit() {
    this.planGroupForm.patchValue({order: 0});
  }

  // do add plan group
  doAddPlanGroup() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        // let credentials = this.planGroupForm.value;
        // for (var key in credentials) {
        //   if (!credentials[key] || credentials[key] === 'null') {
        //     delete credentials[key];
        //   }
        // }
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let planGroupForm = this.planGroupForm;
        for (var key in planGroupForm.value) {
          switch (key) {
            case "planGroupTranslate":
              let i = 0;
              let indexplanDetails = 0;
              planGroupForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;
                  }
                  formData.append(`planGroupTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            default:
              if (planGroupForm.value[key]) {
                if (key === 'expiredAt' || (key === 'activeAt')) {
                  formData.append(key, this.datePipe.transform(planGroupForm.value[key], 'yyyy-MM-dd'));
                } else {
                  formData.append(key, planGroupForm.value[key]);
                }
              }
              break;
          }
        }
        this._http._createWithFormData(`${this.appConfig.config.api.plan_groups}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }

}
