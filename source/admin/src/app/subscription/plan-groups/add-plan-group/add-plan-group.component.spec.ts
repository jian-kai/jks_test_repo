import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlanGroupComponent } from './add-plan-group.component';

describe('AddPlanGroupComponent', () => {
  let component: AddPlanGroupComponent;
  let fixture: ComponentFixture<AddPlanGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlanGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlanGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
