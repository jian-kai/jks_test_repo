import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanGroupTranslateFormComponent } from './plan-group-translate-form.component';

describe('PlanGroupTranslateFormComponent', () => {
  let component: PlanGroupTranslateFormComponent;
  let fixture: ComponentFixture<PlanGroupTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanGroupTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanGroupTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
