import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../../core/services/loading.service';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { PlanService } from '../../../core/services/plan.service';

@Component({
  selector: 'plan-group-translate-form',
  templateUrl: './plan-group-translate-form.component.html',
  styleUrls: ['./plan-group-translate-form.component.scss']
})
export class PlanGroupTranslateFormComponent implements OnInit {
  @Input() planGroupTranslateForm;
  @Input() planGroupId;
  public directoryCountries: any;
  public languageCodeList: any;

  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private planService: PlanService,
  ) {
    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {
  }

  addTranslate() {
    let prDtsCtrl = this.planService.initPlanGroupTranslate();
    this.planGroupTranslateForm.push(prDtsCtrl);
  }

  removeTranslate(_i: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        if (this.planGroupTranslateForm.controls[_i].value.id) {
          let pathDelete = `${this.appConfig.config.api.plan_groups}/${this.planGroupId}/plan-group-translates/${this.planGroupTranslateForm.controls[_i].value.id}`;
          this._http._delete(`${pathDelete}`).subscribe(
            data => {
              if (data.ok) {
                this.planGroupTranslateForm.removeAt(_i);
              }
            },
            error => {
              this.alertService.error(error);
              console.log('error', error);
            }
          )
        } else {
          this.planGroupTranslateForm.removeAt(_i);
        }
      }
    }));
  }

}