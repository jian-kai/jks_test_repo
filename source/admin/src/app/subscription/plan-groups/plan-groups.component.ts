import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan-groups',
  templateUrl: './plan-groups.component.html',
  styleUrls: ['./plan-groups.component.scss', '../../../assets/sass/table.scss']
})
export class PlanGroupsComponent implements OnInit {
  public planGroupList : any;
  public isLoading: boolean = true;
  public options : any = {};
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public planGroupItem: any;
  public showing: string;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,) {
      this.defaultOptions = {limit: `${this.appConfig.config.limitRows}`, page: 1}
    }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
    this.getPlanGroupList();
  }
  // get plan group list
  getPlanGroupList() {
    this.loadingService.display(true);
    let params = this.globalService._URLSearchParams(this.options);
    
    this._http._getList(`${this.appConfig.config.api.plan_groups}?` + params.toString()).subscribe(
      data => {
        data.forEach(element => {
          if(element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN')) {
          element.planGroupTranslate = element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN');
          } else {
            element.planGroupTranslate = element.planGroupTranslate[0];
          }
        });
        this.planGroupList = data;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

    // add new plan group
  addPlanGroup() {
    this.modalService.open('add-plan-group-modal', {order: 0});
  }

  // edit plan group
  editPlanGroup(_planGroupItem : any) {
    this.planGroupItem = _planGroupItem;
    this.modalService.open('edit-plan-group-modal', this.planGroupItem);
  }

  // when has change data
  hasChangePlanType(status: boolean) {
    if (status)
      this.getPlanGroupList();
  }
}
