import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { LoadingService } from '../../../core/services/loading.service';
import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { PlanService } from '../../../core/services/plan.service';
import { FileValidator } from '../../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.scss', '../../../../assets/sass/subscription.scss']
})
export class AddPlanComponent implements OnInit {
  public currentUser: any;
  public planForm: FormGroup;
  public planTypes: any;
  public planGroups: any;
  public addImagesFlag: boolean = false;
  public images = [];
  public setfiles = [];
  public countries: any;
  public selectCountryForm: FormGroup;
  public planCountryForms = [];
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public activeSubmitForm: boolean = false;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private planService: PlanService,
    private _router: Router,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location, ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.planForm = builder.group({
      PlanGroupId: [''],
      PlanTypeId: ['', Validators.required],
      sku: ['', Validators.required],
      slug: ['', Validators.required],
      images: ['', [FileValidator.validate]],
      defaultImageIndex: ['', Validators.required],
      planTranslate: builder.array([]),
      planDetails: [null],
      planCountry: builder.array([]),
      order: [0],
      isTrial: [''],
    });
    this.selectCountryForm = builder.group({
      countryId: ['', Validators.required],
    });

    this.selectTranslateForm = builder.group({
      langCode: ['', Validators.required],
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {
    this.globalService.getPlanTypes().then((data) => {
      this.planTypes = data;
      this.planForm.patchValue({ PlanTypeId: this.planTypes[0].id })
    });

    this._http._getList(`${this.appConfig.config.api.plan_groups}`).subscribe(
      data => {
        data.forEach(element => {
          if(element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN')) {
          element.planGroupTranslate = element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN');
          } else {
            element.planGroupTranslate = element.planGroupTranslate[0];
          }
        });
        this.planGroups = data;
        this.planForm.patchValue({ PlanGroupId: this.planGroups[0].id })
      },
      error => {
        this.alertService.error(error);
      }
    )

    this.globalService.getCountryList().then(data => {
      this.countries = data;
      // add default Product Country form for CurrentUser
      this.addPlanCountry(this.currentUser.Country.id);
    })

    // add default Product Translate form for CurrentUser
    this.addPlanTranslate('en');
    this.addImagesFlag = true;

  }

  // add images for products
  addImages() {
    this.addImagesFlag = true;
  }
	setActiveBtn() {
		this.activeSubmitForm = true;
	}
  doAddPlan() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        console.log('this.planForm.value', this.planForm.value)
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let productForm = this.planForm;
        for (var key in productForm.value) {
          switch (key) {
            case "planTranslate":
              let i = 0;
              let indexplanDetails = 0;
              productForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;;
                  }
                  formData.append(`planTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            case "planCountry":
              let j = 0;
              productForm.value[key].forEach((item, index) => {
                for (let key in item) {
                  if ((key === 'id' && !item[key]) || (key === 'planDetails' || key === 'planTrialProducts' || key === 'planOptions')) {
                    continue;;
                  }
                  formData.append(`planCountry[${j}].` + key, item[key]);
                }
                item.planDetails.forEach((_planDetails, index) => {
                  formData.append(`planCountry[${j}].` + `planDetails[${index}].` + 'ProductCountryId', _planDetails.product.productCountry[0].id);
                  let _deliverPlan: any = {};
                  _planDetails.deliverPlan.forEach(element => {
                    _deliverPlan[element.times] = element.qty;
                  });
                  _deliverPlan = JSON.stringify(_deliverPlan);
                  formData.append(`planCountry[${j}].` + `planDetails[${index}].` + 'deliverPlan', _deliverPlan);
                  indexplanDetails++;
                });
                //trial products
                item.planTrialProducts.forEach((_planTrialProducts, index) => {
                  formData.append(`planCountry[${j}].` + `planTrialProducts[${index}].` + 'ProductCountryId', _planTrialProducts.product.productCountry[0].id);

                  formData.append(`planCountry[${j}].` + `planTrialProducts[${index}].` + 'qty', _planTrialProducts.qty);
                });

                //plan options
                item.planOptions.forEach((_planOption, indexOp) => {
                  if (_planOption.id) {
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'id', _planOption.id);
                  }
                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'description', _planOption.description);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'url', _planOption.url);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'actualPrice', _planOption.actualPrice);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'sellPrice', _planOption.sellPrice);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'pricePerCharge', _planOption.pricePerCharge);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'savePercent', _planOption.savePercent);
                  
                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'hasShaveCream', _planOption.hasShaveCream ? _planOption.hasShaveCream : false);

                  //plan option translate
                  _planOption.planOptionTranslate.forEach((_planOptionTranslate, indexOpPro) => {
                    if (_planOptionTranslate.id) {
                      formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'id', _planOptionTranslate.id);
                    }
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'name', _planOptionTranslate.name);
  
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'langCode', _planOptionTranslate.langCode);
                  });
                  //plan option products
                  _planOption.planOptionProducts.forEach((_planOptionProduct, indexOpPro) => {
                    if (_planOptionProduct.id) {
                      formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'id', _planOptionProduct.id);
                    }
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'qty', _planOptionProduct.qty);

                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'ProductCountryId', _planOptionProduct.product.productCountry[0].id);
                  });
                });
                j++;
              });
              break;

            case "images":
              productForm.value[key].forEach((image, index) => {
                formData.append(`images[${index}].image`, image);
                if (index === productForm.value["defaultImageIndex"]) {
                  image.isDefault = true;
                  formData.append(`images[${index}].isDefault`, image.isDefault);
                } else {
                  formData.append(`images[${index}].isDefault`, "false");
                }
              });
              break;

            default:
              if (key !== 'defaultImageIndex') {
                formData.append(key, productForm.value[key]);
              }
              break;
          }
        }

        this._http._getDetail(`${this.appConfig.config.api.plans}/slug/${this.planForm.value.slug}`).subscribe(
          planslug => {
            if(planslug.slug) {
              this.loadingService.display(false);
              this.alertService.error(JSON.stringify({ok: false, message: "This SLUG is existent, please update"}));
            } else {
              this._http._getDetail(`${this.appConfig.config.api.plans}/sku/${this.planForm.value.sku}`).subscribe(
                plansku => {
                  if(plansku.sku) {
                    this.loadingService.display(false);
                    this.alertService.error(JSON.stringify({ok: false, message: "This SKU is existent, please update"}));
                  } else {
                    this._http._createWithFormData(`${this.appConfig.config.api.plans}`, formData, 'form-data').subscribe(
                      data => {
                        if (data.ok) {
                          this.loadingService.display(false);
                          this.location.back();
                          // this._router.navigate(['/subscriptions/plans'], { queryParams: {} });
                        }
                      },
                      error => {
                        this.loadingService.display(false);
                        this.alertService.error(error);
                      }
                    )
                  }
                },
                error => {
                  this.loadingService.display(false);
                  this.alertService.error(error);
                }
              )
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // add plan country
  addPlanCountry(_countryId: number) {
    let countryItem = this.countries.filter(country => country.id == _countryId)[0];
    let planCountryItem = {
      CountryId: countryItem.id,
      CountryName: countryItem.name,
      currencyDisplay: countryItem.currencyDisplay,
      isOnline: true,
      isOffline: true,
    }
    let control = <FormArray>this.planForm.controls['planCountry'];
    let prCoCtrl = this.planService.initPlanCountry();

    prCoCtrl = this.globalService.setValueFormBuilder(prCoCtrl, planCountryItem);
    prCoCtrl.controls.isOnline.setValue(true);
    prCoCtrl.controls.isOffline.setValue(true);
    // add plan details
    // let controlPRDetails = <FormArray>prCoCtrl.controls['planDetails'];
    // let prDtCtrl = this.planService.initPlanDetails();
    // prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, { PlanCountryId: countryItem.id })
    // controlPRDetails.push(prDtCtrl);

    control.push(prCoCtrl);
    console.log('control', control)
  }

  removePlanCountry(_i: number) {
    let control = <FormArray>this.planForm.controls['planCountry'];
    control.removeAt(_i);
  }

  //add Plan Translate
  addPlanTranslate(_langCode: string) {
    let control = <FormArray>this.planForm.controls['planTranslate'];
    let prTrCtrl = this.planService.initPlanTranslate();
    prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, { langCode: _langCode });

    // add plan details
    // let controlPRDetails = <FormArray>prTrCtrl.controls['planDetails'];
    // let prDtCtrl = this.planService.initPlanDetails();
    // prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, { langCode: _langCode, type: 'center' })
    // controlPRDetails.push(prDtCtrl);

    control.push(prTrCtrl);
  }

  // remove plan translate
  removePlanTranslate(_i: number) {
    let control = <FormArray>this.planForm.controls['planTranslate'];
    control.removeAt(_i);
  }

  // has change plan type
  hasChangePlanType() {
    // this.planService.;
  }

  // has change plan group
  hasChangePlanGroup() {
    // this.planService.;
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }
}
