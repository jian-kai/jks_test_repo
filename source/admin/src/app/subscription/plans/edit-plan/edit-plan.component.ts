import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { LoadingService } from '../../../core/services/loading.service';
import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { PlanService } from '../../../core/services/plan.service';
import { FileValidator } from '../../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'app-edit-plan',
  templateUrl: './edit-plan.component.html',
  styleUrls: ['./edit-plan.component.scss', '../../../../assets/sass/subscription.scss']
})
export class EditPlanComponent implements OnInit {
  public currentUser: any;
  public planForm: FormGroup;
  public planInfo: any = {};
  public planTypes: any;
  public planGroups: any;
  public addImagesFlag: boolean = false;
  public images = [];
  public setfiles = [];
  public countries: any;
  public selectCountryForm: FormGroup;
  public planCountryForms = [];
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public slug: number;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private planService: PlanService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location, ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    // get plan details by plan id
    this.loadingService.display(true);
    this.activateRoute.params.subscribe(params => {
      this.slug = params['slug']// (+) converts string 'id' to a number
    });
    this._http._getDetail(`${this.appConfig.config.api.plans}/${this.slug}`).subscribe(
      data => {
        this.planInfo = data;
        this.initFormData();
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
        this.loadingService.display(false);
        console.log(error.error);
      }
    )

    this.planForm = builder.group({
      PlanGroupId: [''],
      PlanTypeId: ['', Validators.required],
      sku: ['', Validators.required],
      slug: ['', Validators.required],
      images: ['', [FileValidator.validate]],
      defaultImageIndex: ['', Validators.required],
      planTranslate: builder.array([]),
      planDetails: [null],
      planCountry: builder.array([]),
      order: [0],
      isTrial: [''],
    });
    this.selectCountryForm = builder.group({
      countryId: ['', Validators.required],
    });

    this.selectTranslateForm = builder.group({
      langCode: ['', Validators.required],
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {
    this.globalService.getPlanTypes().then((data) => {
      this.planTypes = data;
      // this.planForm.patchValue({ PlanTypeId: this.planTypes[0].id })
    });

    this._http._getList(`${this.appConfig.config.api.plan_groups}`).subscribe(
      data => {
        data.forEach(element => {
          if(element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN')) {
          element.planGroupTranslate = element.planGroupTranslate.find(value => value.langCode.toUpperCase() === 'EN');
          } else {
            element.planGroupTranslate = element.planGroupTranslate[0];
          }
        });
        this.planGroups = data;
      },
      error => {
        this.alertService.error(error);
      }
    )

    this.globalService.getCountryList().then(data => {
      this.countries = data;
    })


  }

  // set value for form data
  initFormData() {
    // this.productForm.controls['ProductTypeId'].setValue(this.productInfo.ProductType.id);
    this.planForm.patchValue(this.planInfo);
    this.planForm.controls['sku'].setValue(this.planInfo.sku);
    this.planForm.controls['slug'].setValue(this.planInfo.slug ? this.planInfo.slug : '');
    this.images = this.planInfo.planImages;
    this.planForm.controls['order'].setValue(this.planInfo.order);
    this.planForm.controls['images'].setValue(this.images);
    this.addImagesFlag = true;
    this.planForm.patchValue({ PlanTypeId: this.planInfo.PlanType.id });
    console.log("this.planForm.controls['images']", this.planForm.controls['images'])
    // this.productForm.controls['isFeatured'].setValue(this.productInfo.isFeatured);

    //planTranslate
    this.planInfo.planTranslate.forEach(translate => {
      this.addPlanTranslate(translate.langCode, translate);

    });
    //planCountry
    this.globalService.getCountryList().then(data => {
      this.countries = data;
      this.planInfo.planCountry.forEach(element => {
        this.addPlanCountry(element.CountryId, element);
      });
    })
  }


  // add images for products
  addImages() {
    this.addImagesFlag = true;
  }

  doEditPlan() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        console.log('this.planForm.value', this.planForm.value)
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let planForm = this.planForm;
        for (var key in planForm.value) {
          switch (key) {
            case "planTranslate":
              let i = 0;
              let indexplanDetails = 0;
              planForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;;
                  }
                  formData.append(`planTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            case "planCountry":
              let j = 0;
              planForm.value[key].forEach((item, index) => {
                for (let key in item) {
                  if ((key === 'id' && !item[key]) || (key === 'planDetails' || key === 'planTrialProducts' || key === 'planOptions')) {
                    continue;;
                  }
                  formData.append(`planCountry[${j}].` + key, item[key]);
                }
                item.planDetails.forEach((_planDetails, index) => {
                  if (_planDetails.id) {
                    formData.append(`planCountry[${j}].` + `planDetails[${index}].` + 'id', _planDetails.id);
                  }
                  formData.append(`planCountry[${j}].` + `planDetails[${index}].` + 'ProductCountryId', _planDetails.product.productCountry[0].id);
                  let _deliverPlan: any = {};
                  _planDetails.deliverPlan.forEach(element => {
                    _deliverPlan[element.times] = element.qty;
                  });
                  _deliverPlan = JSON.stringify(_deliverPlan);
                  formData.append(`planCountry[${j}].` + `planDetails[${index}].` + 'deliverPlan', _deliverPlan);
                  indexplanDetails++;
                });

                //trial products
                item.planTrialProducts.forEach((_planTrialProducts, index) => {
                  if (_planTrialProducts.id) {
                    formData.append(`planCountry[${j}].` + `planTrialProducts[${index}].` + 'id', _planTrialProducts.id);
                  }
                  formData.append(`planCountry[${j}].` + `planTrialProducts[${index}].` + 'ProductCountryId', _planTrialProducts.product.productCountry[0].id);

                  formData.append(`planCountry[${j}].` + `planTrialProducts[${index}].` + 'qty', _planTrialProducts.qty);
                });

                //plan options
                item.planOptions.forEach((_planOption, indexOp) => {
                  if (_planOption.id) {
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'id', _planOption.id);
                  }
                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'description', _planOption.description);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'url', _planOption.url);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'actualPrice', _planOption.actualPrice);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'sellPrice', _planOption.sellPrice);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'pricePerCharge', _planOption.pricePerCharge);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'savePercent', _planOption.savePercent);

                  formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + 'hasShaveCream', _planOption.hasShaveCream ? _planOption.hasShaveCream : false);

                  //plan option translate
                  _planOption.planOptionTranslate.forEach((_planOptionTranslate, indexOpPro) => {
                    if (_planOptionTranslate.id) {
                      formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'id', _planOptionTranslate.id);
                    }
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'name', _planOptionTranslate.name);
  
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionTranslate[${indexOpPro}].` + 'langCode', _planOptionTranslate.langCode);
                  });
                  //plan option products
                  _planOption.planOptionProducts.forEach((_planOptionProduct, indexOpPro) => {
                    if (_planOptionProduct.id) {
                      formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'id', _planOptionProduct.id);
                    }
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'qty', _planOptionProduct.qty);
  
                    formData.append(`planCountry[${j}].` + `planOptions[${indexOp}].` + `planOptionProducts[${indexOpPro}].` + 'ProductCountryId', _planOptionProduct.product.productCountry[0].id);
                  });
                });

                j++;
              });
              break;

            case "images":
              let _imgs = [];
              planForm.value[key].forEach((image, index) => {
                if (image) {
                  _imgs[index] = {};
                  if (image.id) {
                    for (let key in image) {
                      let value = image[key];
                      if (key !== 'isDefault') {
                        _imgs[index][key] = value;
                      }
                    }
                  } else {
                    _imgs[index].image = image;
                  }
                  if (index === planForm.value["defaultImageIndex"]) {
                    image.isDefault = true;
                    _imgs[index].isDefault = image.isDefault;
                  } else {
                    _imgs[index].isDefault = 'false';
                  }
                }

              });
              let _arr = this.globalService.reindexingArray(_imgs);
              _arr.forEach((image, index) => {
                for (let key in image) {
                  let value = image[key];
                  formData.append(`images[${index}].` + `${key}`, value);
                }
              });
              break;

            default:
              formData.append(key, planForm.value[key]);
              break;
          }
        }
        this._http._updatePut(`${this.appConfig.config.api.plans}/${this.planInfo.id}`, formData, 'form-data').subscribe(
          data => {
            if (data.ok) {
              this.loadingService.display(false);
              this.location.back();
              // this._router.navigate(['/subscriptions/plans'], { queryParams: {} });
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // add plan country
  addPlanCountry(_countryId: number, _setData?: any) {
    let countryItem = this.countries.filter(country => country.id == _countryId)[0];
    let planDetails: any = [];
    let planTrialProducts: any = [];
    let planOptions: any = [];
    let planCountryItem = {
      CountryId: countryItem.id,
      CountryName: countryItem.name,
      currencyDisplay: countryItem.currencyDisplay
    }
    let control = <FormArray>this.planForm.controls['planCountry'];
    let CoCtrl = this.planService.initPlanCountry();
    CoCtrl.patchValue(planCountryItem)
    // CoCtrl = this.globalService.setValueFormBuilder(CoCtrl, planCountryItem);
    if (_setData) {
      planDetails = _setData.planDetails;
      delete _setData.planDetails;
      CoCtrl.controls.isOnline.setValue(_setData.isOnline);
      CoCtrl.controls.isOffline.setValue(_setData.isOffline);
      console.log('_setData', _setData)
      CoCtrl.patchValue(_setData)
      // CoCtrl = this.globalService.setValueFormBuilder(CoCtrl, _setData);

      // add plan details
      let controlPRDetails = <FormArray>CoCtrl.controls['planDetails'];
      console.log('planDetails', planDetails);
      planDetails.forEach(element => {
        let prDtCtrl = this.planService.initPlanDetails();
        // set product
        let _product = element.ProductCountry.Product;
        _product.productCountry = [];
        _product.productCountry.push({ id: element.ProductCountryId });
        prDtCtrl.patchValue({ id: element.id, product: _product });
        // prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, { id: element.id, product: _product });
        // set deliverPlan
        let _deliverPlan: any;
        _deliverPlan = JSON.parse(element.deliverPlan);
        let controlDeliverPlan = <FormArray>prDtCtrl.controls['deliverPlan'];

        for (let key in _deliverPlan) {
          let _value = _deliverPlan[key];
          let deliverPlanInit = this.planService.initDeliverPlan();
          deliverPlanInit.patchValue({ times: key, qty: _value });
          // deliverPlanInit = this.globalService.setValueFormBuilder(deliverPlanInit, { times: key, qty: _value })
          controlDeliverPlan.push(deliverPlanInit);
        }
        controlPRDetails.push(prDtCtrl);
      });


      // add plan trial products
      planTrialProducts = _setData.planTrialProducts;
      let controlPlanTrialProducts = <FormArray>CoCtrl.controls['planTrialProducts'];
      console.log('planTrialProducts', planTrialProducts);
      planTrialProducts.forEach(element => {
        let prDtCtrl = this.planService.initPlanTrialProducts();
        // set product
        let _product = element.ProductCountry.Product;
        _product.productCountry = [];
        _product.productCountry.push({ id: element.ProductCountryId });
        prDtCtrl.patchValue({ id: element.id, product: _product, qty: element.qty });

        controlPlanTrialProducts.push(prDtCtrl);
      });


      // add plan options
      planOptions = _setData.planOptions;
      let controlPlanOptions = <FormArray>CoCtrl.controls['planOptions'];
      console.log('planOptions', planOptions);
      planOptions.forEach(element => {
        let plOpCtrl = this.planService.initPlanOptions();
        plOpCtrl.patchValue(
          { 
            id: element.id,
            description: element.description,
            actualPrice: element.actualPrice,
            pricePerCharge: element.pricePerCharge,
            sellPrice: element.sellPrice,
            savePercent: element.savePercent,
            url: element.url,
            hasShaveCream: element.hasShaveCream
          }
        );
        controlPlanOptions.push(plOpCtrl);
        // set option translate
        element.planOptionTranslate.forEach(ele => {
          if(ele.name) {
            let controlPlanOptionTranslate = <FormArray>plOpCtrl.controls['planOptionTranslate'];
            let plOpTranCtrl = this.planService.initPlanOptionTranslate();
            
            plOpTranCtrl = this.globalService.setValueFormBuilder(plOpTranCtrl, ele);
            controlPlanOptionTranslate.push(plOpTranCtrl);
          }
        });
        // set product
        element.planOptionProducts.forEach(ele => {
          if(ele.ProductCountry) {
            let controlPlanOptionProducts = <FormArray>plOpCtrl.controls['planOptionProducts'];
            let plOpProdCtrl = this.planService.initPlanOptionProducts();
  
            let _product = ele.ProductCountry.Product;
            _product.productCountry = [];
            _product.productCountry.push({ id: ele.ProductCountryId, sellPrice: ele.ProductCountry.sellPrice });
  
            plOpProdCtrl.patchValue({ id: ele.id, qty: ele.qty, product: _product });
            controlPlanOptionProducts.push(plOpProdCtrl);
          }
        });
      });
    } else {
      CoCtrl.controls.isOnline.setValue(true);
      CoCtrl.controls.isOffline.setValue(true);
    }

    control.push(CoCtrl);
    console.log('control', control);
  }

  removePlanCountry(_i: number, _id?: number) {
    let control = <FormArray>this.planForm.controls['planCountry'];
    if (control.controls[_i].value.id) {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        this._http._delete(`${this.appConfig.config.api.plans}/plan-country/${control.controls[_i].value.id}`)
          .subscribe(result => {
            control.removeAt(_i);
          });
      }));
    } else {
      control.removeAt(_i);
    }
  }

  //add Plan Translate
  addPlanTranslate(_langCode: string, _setDataTranslate?: any) {
    let control = <FormArray>this.planForm.controls['planTranslate'];
    let prTrCtrl = this.planService.initPlanTranslate();
    if (_setDataTranslate) {
      prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, _setDataTranslate);
    } else {
      prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, { langCode: _langCode });
    }

    control.push(prTrCtrl);
  }

  // remove plan translate
  removePlanTranslate(_i: number) {
    let control = <FormArray>this.planForm.controls['planTranslate'];
    control.removeAt(_i);
  }

  // has change plan type
  hasChangePlanType() {
    // this.planService.;
  }

  // has change plan group
  hasChangePlanGroup() {
    // this.planService.;
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }
}