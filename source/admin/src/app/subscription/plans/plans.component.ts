import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss',
  '../../../assets/sass/subscription.scss',
  '../../../assets/sass/table.scss']
})
export class PlansComponent extends ListViewsComponent {
  public planList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public planGroups: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public timer = null;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    
    if (!this.productTypes) {
      this.globalService.getPlanTypes().then((data) => {
        this.productTypes = data;
      });
    }
    
    if (!this.planGroups) {
      this.globalService.getPlanGroups().then((data) => {
        this.planGroups = data;
      });
    }
    

    // get all plans
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.plan_list}?` + this.options.toString()).subscribe(
      data => {
        this.planList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }
  changedatalist(list){ 
    this.planList.rows =  list ;  
  }
  // sort by order_by
  sortByName(_type: string) {
    // if (_type === "ASC") {
    //   this.filterby('orderBy', '["name"]');
    // } else {
    //   this.filterby('orderBy', '["name_DESC"]');
    // }
  }

   // sort by order_by
  sortByDate(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', 'createdAt');
    } else {
      this.filterby('orderBy', 'createdAt_DESC');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500);
  }

  // delete product 
  deletePlan(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updatePlanStatus(_id, 'removed');
      }
    }));
  }

  // active product 
  activePlan(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updatePlanStatus(_id, 'active');
      }
    }));
  }

  // update Plan status: 'removed' | 'active'
  updatePlanStatus(_id: number, _status: string) {
    this.loadingService.display(true);
    let formData: FormData = new FormData();
    formData.append('status', _status);
    this._http._updatePut(`${this.appConfig.config.api.plans}/${_id}`, formData, "form-data").subscribe(
      data => {
        if (data.ok) {
          this.loadingService.display(false);
          this.getData();
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

}
