import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'plan-country-form',
  templateUrl: './plan-country-form.component.html',
  styleUrls: ['./plan-country-form.component.scss']
})
export class PlanCountryFormComponent implements OnInit {
  @Input() planCountryForm;
  @Input() parentOnActive;
  public products: any;
  public options: any = {};
  public countryId: number;
  public planTypes: any;

  constructor(private builder: FormBuilder,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private alertService: AlertService,
    private _dialogService: DialogService,
    private planService: PlanService
  ) {
  }

  ngOnInit() {
    this.countryId = this.planCountryForm.controls.CountryId.value;
    this.getproductList();
    this.globalService.getPlanTypes().then(data => {
      this.planTypes = data;
    })
    // this.planCountryForm.controls.isOffline.setValue(true);
    // this.planCountryForm.controls.isOnline.setValue(true);
    // this.planCountryForm.controls.isOnline.valueChanges.subscribe(data => {
    //   if (!data) {
    //     if (!this.planCountryForm.controls.isOffline.value) {
    //       this.planCountryForm.controls.isOffline.setValue(true);
    //     }
    //   }
    // });
    // this.planCountryForm.controls.isOffline.valueChanges.subscribe(data => {
    //   if (!data) {
    //     if (!this.planCountryForm.controls.isOnline.value) {
    //       this.planCountryForm.controls.isOnline.setValue(true);
    //     }
    //   }
    // });

    this.planCountryForm.controls.actualPrice.valueChanges.subscribe(value => {
      if (!value) {
        this.planCountryForm.controls.pricePerCharge.setValue(0);
      } else {
        if (this.planCountryForm.value.savePercent) {
          this.planCountryForm.controls.pricePerCharge.setValue((Number(value) - (Number(value) * Number(this.planCountryForm.value.savePercent)) / 100));
        } else {
          this.planCountryForm.controls.pricePerCharge.setValue(value);
        }
      }
    });

    this.planCountryForm.controls.savePercent.valueChanges.subscribe(value => {
      if (!value) {
        this.planCountryForm.controls.pricePerCharge.setValue(this.planCountryForm.value.actualPrice);
      } else {
        if (this.planCountryForm.value.actualPrice) {
          this.planCountryForm.controls.pricePerCharge.setValue((Number(this.planCountryForm.value.actualPrice) - (Number(this.planCountryForm.value.actualPrice) * Number(value)) / 100));
        } else {
          this.planCountryForm.controls.pricePerCharge.setValue(0);
        }
      }
    });

  }

  // get product list
  getproductList() {
    this.options = {
      CountryId: this.countryId,
      status: 'active'
    }
    let params = this.globalService._URLSearchParams(this.options);
    this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.products = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log('error', error);
      }
    )
  }

  // add a product details with translate group
  addPlanOptions(): void {
    let controlPRDetails = <FormArray>this.planCountryForm.controls['planOptions'];
    let prDtsCtrl = this.planService.initPlanOptions();
    controlPRDetails.push(prDtsCtrl);
  }

  // remove product details
  removePlanOptions(_j: number) {
    let control = <FormArray>this.planCountryForm.controls['planOptions'];
    if (control.controls[_j].value.id) {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        if(result) {
          this._http._delete(`${this.appConfig.config.api.plans}/plan-option/${control.controls[_j].value.id}`)
            .subscribe(result => {
              control.removeAt(_j);
            });
        }
      }));
    } else {
      control.removeAt(_j);
    }
    return false;
  }

}
