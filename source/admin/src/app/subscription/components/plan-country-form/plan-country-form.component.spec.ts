import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanCountryFormComponent } from './plan-country-form.component';

describe('PlanCountryFormComponent', () => {
  let component: PlanCountryFormComponent;
  let fixture: ComponentFixture<PlanCountryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanCountryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanCountryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
