import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOptionFormComponent } from './plan-option-form.component';

describe('PlanOptionFormComponent', () => {
  let component: PlanOptionFormComponent;
  let fixture: ComponentFixture<PlanOptionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOptionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
