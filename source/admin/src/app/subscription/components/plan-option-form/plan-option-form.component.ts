import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from '../../../../app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PlanService } from '../../../core/services/plan.service';
import { AlertService } from '../../../core/services/alert.service';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'plan-option-form',
  templateUrl: './plan-option-form.component.html',
  styleUrls: ['./plan-option-form.component.scss'],
  providers: [TranslatePipe]
})
export class PlanOptionFormComponent implements OnInit {
  @Input() planOptionsForm;
  @Input() products;
  @Input() planTypes;
  @Input() parentOnActive;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public countryId: number;
  public currencyDisplay: any;
  public totalDeliverTimes: number;
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private alertService: AlertService,
    private builder: FormBuilder,
    private planService: PlanService,
    private _dialogService: DialogService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    this.countryId = this.planOptionsForm.parent.parent.controls.CountryId.value;
    this.currencyDisplay = this.planOptionsForm.parent.parent.controls.currencyDisplay.value;
    this.getproductList();
    this.globalService.getPlanTypes().then(data => {
      this.planTypes = data;
    })

    let zero = 0;
    this.planOptionsForm.controls.actualPrice.valueChanges.subscribe(value => {
      if (!value) {
        this.planOptionsForm.controls.pricePerCharge.setValue(zero.toFixed(2));
      } else {
        if (this.planOptionsForm.value.savePercent) {
          this.planOptionsForm.controls.pricePerCharge.setValue( ( Number(value) - (Number(value)*Number(this.planOptionsForm.value.savePercent))/100 ).toFixed(2) );
        } else {
          this.planOptionsForm.controls.pricePerCharge.setValue(value.toFixed(2));
        }
      }
    });

    this.planOptionsForm.controls.savePercent.valueChanges.subscribe(value => {
      if (!value) {
        this.planOptionsForm.controls.pricePerCharge.setValue(this.planOptionsForm.value.actualPrice.toFixed(2));
      } else {
        if (this.planOptionsForm.value.actualPrice) {
          this.planOptionsForm.controls.pricePerCharge.setValue( ( Number(this.planOptionsForm.value.actualPrice) - (Number(this.planOptionsForm.value.actualPrice)*Number(value))/100 ).toFixed(2) );
        } else {
          this.planOptionsForm.controls.pricePerCharge.setValue(zero.toFixed(2));
        }
      }
    });
  }


   // get product list
   getproductList() {
    this.options = {
      CountryId: this.countryId,
      status: 'active'
    }
    let params = this.globalService._URLSearchParams(this.options);
    this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.products = data.rows;
      },
      error => {
        this.alertService.error(error);
        console.log('error', error);
      }
    )
  }
}
