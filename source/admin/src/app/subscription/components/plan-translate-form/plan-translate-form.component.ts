import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';


import { GlobalService } from '../../../core/services/global.service';
import { HttpService } from '../../../core/services/http.service';
import { PlanService } from '../../../core/services/plan.service';
import { AppConfig } from 'app/config/app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'plan-translate-form',
  templateUrl: './plan-translate-form.component.html',
  styleUrls: ['./plan-translate-form.component.scss', '../../../../assets/sass/subscription.scss']
})
export class PlanTranslateFormComponent implements OnInit {
	@Input() planTranslateForm;
	@Input() parentOnActive;
  private planId: number;
  constructor(private planService: PlanService,
    private globalService: GlobalService,
    private _http: HttpService,
    private appConfig: AppConfig,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.planId = +params['id']// (+) converts string 'id' to a number
    });
  }

  // add a plan details with translate group
  addPlanDetails(): void {
    let controlPRDetails = <FormArray>this.planTranslateForm.controls['planDetails'];
    let prDtsCtrl = this.planService.initPlanDetails();
    prDtsCtrl = this.globalService.setValueFormBuilder(prDtsCtrl, { langCode: this.planTranslateForm.controls.langCode.value, type: 'center' })
    controlPRDetails.push(prDtsCtrl);
  }

  // remove plan details
  removePlanDetails(_j: number) {
    let controlPRDetails = <FormArray>this.planTranslateForm.controls['planDetails'];
    if (controlPRDetails.controls[_j].value.id) {
      // remove plan details on database
      let pathDelete = `${this.appConfig.config.api.plans}/${this.planId}/plan-details/${controlPRDetails.controls[_j].value.id}`;
      this._http._delete(`${pathDelete}`).subscribe(
        data => {
          if (data.ok) {
            controlPRDetails.removeAt(_j);
          }
        },
        error => {
          this.alertService.error(error);
          console.log('error', error);
        }
      )
    } else {
      controlPRDetails.removeAt(_j);
    }
    return false;
  }

}
