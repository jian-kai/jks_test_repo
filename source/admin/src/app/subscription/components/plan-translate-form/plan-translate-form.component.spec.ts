import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanTranslateFormComponent } from './plan-translate-form.component';

describe('PlanTranslateFormComponent', () => {
  let component: PlanTranslateFormComponent;
  let fixture: ComponentFixture<PlanTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
