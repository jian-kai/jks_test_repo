import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOptionTranslateFormComponent } from './plan-option-translate-form.component';

describe('PlanOptionTranslateFormComponent', () => {
  let component: PlanOptionTranslateFormComponent;
  let fixture: ComponentFixture<PlanOptionTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOptionTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOptionTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
