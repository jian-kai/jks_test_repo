import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from '../../../../app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'plan-option-product-form',
  templateUrl: './plan-option-product-form.component.html',
  styleUrls: ['./plan-option-product-form.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class PlanOptionProductFormComponent implements OnInit {
  @Input() planOptionProductsForm;
  @Input() products;
  @Input() planTypes;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public countryId: number;
  public totalDeliverTimes: number;
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private planService: PlanService,
    private _dialogService: DialogService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    console.log('planOptionProductsForm', this.planOptionProductsForm);
    this.countryId = this.planOptionProductsForm.parent.parent.parent.controls.CountryId.value;
    // this.totalDeliverTimes = this.planTypes.filter(item => item.id == this.planOptionProductsForm.parent.parent.parent.controls.PlanTypeId.value)[0].totalDeliverTimes;
    // console.log('this.totalDeliverTimes', this.totalDeliverTimes)
    // this.planOptionProductsForm.parent.parent.parent.controls.PlanTypeId.valueChanges.subscribe(data => {
    //   this.totalDeliverTimes = this.planTypes.filter(item => item.id == data)[0].totalDeliverTimes;
    //   if (!this.totalDeliverTimes) {
    //     this.totalDeliverTimes = 1;
    //   }
    //   console.log('this.totalDeliverTimes', this.totalDeliverTimes)
    //   this.clearPlanDetails();
    // })
  }

  // // clear plan details
  // clearPlanDetails() {
  //   let control = <FormArray>this.planOptionProductsForm;
  //   control.controls.forEach((element, index) => {
  //     control.removeAt(index);
  //   });
  // }

  filterProducts() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.products.forEach((product, index) => {
        let has = false;
        product.productTranslate.forEach(item => {
          if (item.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
            return has = true;
          }
        });
        if (has) {
          this.filteredList.push(product);
        }
      });
    } else {
      this.filteredList = [];
    }
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    item.productCountry = item.productCountry.filter(tmp => tmp.Country.id === this.countryId).length > 0 ?
                          item.productCountry.filter(tmp => tmp.Country.id === this.countryId) :
                          item.productCountry
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.planOptionProductsForm.controls.length) {
      _val = [];
    } else {
      _val = this.planOptionProductsForm.value;
      _val.forEach(element => {
        if (element.product.productCountry[0].id === item.productCountry[0].id) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.planOptionProductsForm;
      let prCtrl = this.planService.initPlanOptionProducts();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { product: item, qty: 0});  
      
      control.push(prCtrl);
      this.changePlanOptionPrice(control);
    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.planOptionProductsForm.controls.product.setValue(_val);
  }

  removeItem(planOptionProductId: any, idx: number) {
    let control = <FormArray>this.planOptionProductsForm;
    if (control.controls[idx].value.id) {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        if(result) {
          this._http._delete(`${this.appConfig.config.api.plans}/plan-option-product/${planOptionProductId}`)
          .subscribe(result => {
            // let control = <FormArray>this.planOptionProductsForm;
            control.removeAt(idx);
            console.log(result)
          });
        }      
      }));
    } else {
      control.removeAt(idx);
    }
    this.changePlanOptionPrice(control);
  }

  changePlanOptionPrice(control: any) {
    let price:number = 0.00;
    control.value.forEach(element => {
      console.log('element.product.productCountry[0].sellPrice', element.product.productCountry[0].sellPrice);
      price = price + parseFloat(element.product.productCountry[0].sellPrice);
    });
    let savePercent = 0;
    this.planOptionProductsForm.parent.controls.actualPrice.setValue(price.toFixed(2));
    this.planOptionProductsForm.parent.controls.sellPrice.setValue(price.toFixed(2));
    this.planOptionProductsForm.parent.controls.pricePerCharge.setValue(price.toFixed(2));
    this.planOptionProductsForm.parent.controls.savePercent.setValue(savePercent.toFixed(2));
  }
}
