import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOptionProductFormComponent } from './plan-option-product-form.component';

describe('PlanOptionProductFormComponent', () => {
  let component: PlanOptionProductFormComponent;
  let fixture: ComponentFixture<PlanOptionProductFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOptionProductFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOptionProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
