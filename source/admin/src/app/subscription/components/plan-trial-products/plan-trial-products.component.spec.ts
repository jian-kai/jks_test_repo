import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanTrialProductsComponent } from './plan-trial-products.component';

describe('PlanTrialProductsComponent', () => {
  let component: PlanTrialProductsComponent;
  let fixture: ComponentFixture<PlanTrialProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanTrialProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanTrialProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
