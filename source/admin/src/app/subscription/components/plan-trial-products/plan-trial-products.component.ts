import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'plan-trial-products',
  templateUrl: './plan-trial-products.component.html',
  styleUrls: ['./plan-trial-products.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class PlanTrialProductsComponent implements OnInit {
  @Input() planTrialProductsForm;
  @Input() products;
  @Input() planTypes;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public countryId: number;
  public totalDeliverTimes: number;
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private planService: PlanService,
    private _dialogService: DialogService) {
      this.queryForm = builder.group({
        query: [''],
      });
  }

  ngOnInit() {
    this.countryId = this.planTrialProductsForm.parent.controls.CountryId.value;
    this.totalDeliverTimes = this.planTypes.filter(item => item.id == this.planTrialProductsForm.parent.parent.parent.controls.PlanTypeId.value)[0].totalDeliverTimes;
    console.log('this.totalDeliverTimes', this.totalDeliverTimes)
    // setTimeout(_ => this.getproductList());
    // this.getproductList();
    this.planTrialProductsForm.parent.parent.parent.controls.PlanTypeId.valueChanges.subscribe(data => {
      this.totalDeliverTimes = this.planTypes.filter(item => item.id == data)[0].totalDeliverTimes;
      if (!this.totalDeliverTimes) {
        this.totalDeliverTimes = 1;
      }
      console.log('this.totalDeliverTimes', this.totalDeliverTimes)
      this.clearPlanDetails();
    })
  }
  // get product list
  // getproductList() {
  //   this.options = {
  //     CountryId: this.countryId,
  //     status: 'active'
  //   }
  //   let params = this.globalService._URLSearchParams(this.options);
  //   this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
  //     data => {
  //       this.products = data.rows;
  //     },
  //     error => {
  //       console.log('error', error);
  //     }
  //   )
  // }

  // clear plan details
  clearPlanDetails() {
    let control = <FormArray>this.planTrialProductsForm;
    control.controls.forEach((element, index) => {
      control.removeAt(index);
    });
  }

  filterProducts() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.products.forEach((product, index) => {
        let has = false;
        product.productTranslate.forEach(item => {
          if (item.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
            return has = true;
          }
        });
        if (has) {
          this.filteredList.push(product);
        }
      });
    } else {
      this.filteredList = [];
    } 
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    item.productCountry = item.productCountry.filter(tmp => tmp.Country.id === this.countryId).length > 0 ?
                          item.productCountry.filter(tmp => tmp.Country.id === this.countryId) :
                          item.productCountry
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.planTrialProductsForm.controls.length) {
      _val = [];
    } else {
      _val = this.planTrialProductsForm.value;
      _val.forEach(element => {
        if (element.product.productCountry[0].id === item.productCountry[0].id) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.planTrialProductsForm;
      let prCtrl = this.planService.initPlanTrialProducts();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { product: item, qty: 0});
      // add deliverPlan
      // let controlDeliverPlan = <FormArray>prCtrl.controls['deliverPlan'];
      // for (var index = 1; index <= this.totalDeliverTimes; index++) {
      //   let deliverPlanInit = this.planService.initDeliverPlan();
      //   deliverPlanInit = this.globalService.setValueFormBuilder(deliverPlanInit, { times: index, qty: 0 })
      //   controlDeliverPlan.push(deliverPlanInit);
      // }      
      
      control.push(prCtrl);
     
    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.planTrialProductsForm.controls.product.setValue(_val);
  }

  removeItem(planDetailId: any, idx: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      this._http._delete(`${this.appConfig.config.api.plans}/plan-trial-products/${planDetailId}`)
        .subscribe(result => {
          let control = <FormArray>this.planTrialProductsForm;
          control.removeAt(idx);
          console.log(result)
        });
    }));
  }
}
