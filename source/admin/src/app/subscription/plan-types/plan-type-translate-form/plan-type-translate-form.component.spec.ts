import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanTypeTranslateFormComponent } from './plan-type-translate-form.component';

describe('PlanTypeTranslateFormComponent', () => {
  let component: PlanTypeTranslateFormComponent;
  let fixture: ComponentFixture<PlanTypeTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanTypeTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanTypeTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
