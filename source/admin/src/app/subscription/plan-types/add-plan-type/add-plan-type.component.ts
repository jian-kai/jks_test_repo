import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';


@Component({
  selector: 'add-plan-type-modal',
  templateUrl: './add-plan-type.component.html',
  styleUrls: ['./add-plan-type.component.scss']
})
export class AddPlanTypeComponent implements OnInit {
  public planTypeForm: FormGroup;
  public isLoading: boolean = true;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    private location: Location,
    el: ElementRef,) {
      this.planTypeForm = builder.group({
        name: ['', [Validators.required, Validators.maxLength(200)]],
        totalDeliverTimes: [''],
        totalChargeTimes: [''],
        subsequentDeliverDuration: [''],
        subsequentChargeDuration: [''],
        prefix: ['', Validators.required],
        order: [''],
        planTypeTranslate: builder.array([], Validators.compose([Validators.required])),
      });
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
  }

  ngOnInit() {
    this.planTypeForm.patchValue({order: 0});
  }

  // do add plan type
  doAddPlanType() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        let credentials = this.planTypeForm.value;
        for (var key in credentials) {
          if (!credentials[key] || credentials[key] === 'null') {
            delete credentials[key];
          }
        }
        this.loadingService.display(true);
        this._http._create(`${this.appConfig.config.api.plan_types_create_update}`, credentials).subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }

}
