import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { isNumber } from 'util';

@Component({
  selector: 'app-plan-types',
  templateUrl: './plan-types.component.html',
  styleUrls: ['./plan-types.component.scss',
    '../../../assets/sass/subscription.scss',
    '../../../assets/sass/table.scss']
})
export class PlanTypesComponent implements OnInit {
  public planTypeList : any;
  public isLoading: boolean = true;
  public options : any = {};
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public planTypeItem: any;
  public showing: string;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,) {
      this.defaultOptions = {limit: `${this.appConfig.config.limitRows}`, page: 1}
    }

  ngOnInit() {
    // let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    // if (params && params.get('limit')) {
    //   this.options.limit = params.get('limit');
    // }
    // if (params && params.get('page')) {
    //   this.options.page = params.get('page');
    // }
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
    // this.pagerService.getPager.subscribe((page: number) => {
    //   if (page) {
    //     this.options.page = page;
    //     this.getPlanTypeList();
    //   }
    // });
    this.getPlanTypeList();
  }
  // get user list
  getPlanTypeList() {
    // if (!this.globalService.objectIsEmpty(this.options)) {
    //   this._router.navigate(['/subscriptions/plan-types'], { queryParams: this.options });
    // } else {
    //   this.options = this.defaultOptions;
    // }
    this.loadingService.display(true);
    let params = this.globalService._URLSearchParams(this.options);
    
    this._http._getList(`${this.appConfig.config.api.plan_types}?` + params.toString()).subscribe(
      data => {
        this.planTypeList = data;
        this.loadingService.display(false);
        console.log(data);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }
  changedatalist(list){ 
    this.planTypeList =  list ;  
  }
  tableHearSort(event, list, name, typesort?: string, postion?: string) {
    // let list = this.productList.rows;
    $(event.target).parent().find('th').removeClass('sorted');
    $(event.target).parent().find('th').not(event.target).attr('sortby', '');
    $(event.target).addClass('sorted');
    let sortby = $(event.target).attr('sortby');
    let sortbyNow;
    if (sortby == 'ASC') {
      $(event.target).attr('sortby', 'DESC');
      sortbyNow = 'DESC';
    } else {
      $(event.target).attr('sortby', 'ASC');
      sortbyNow = 'ASC';
    }
    let newsdata = [];
    let keysSorted = Object.keys(list).sort(function (a, b) {
      let nameA, nameB;
      if (typeof (postion) != 'undefined' && postion != '') {
        let spl = postion.split('.');
        nameA = list[a];
        nameB = list[b];
        spl.forEach(e => {
          nameA = nameA[e];
          nameB = nameB[e];
        });
        if (Array.isArray(nameA) || Array.isArray(nameB)) {
          let nameAarray = nameA.filter(item => item.langCode === 'EN');
          let nameBarray = nameB.filter(item => item.langCode === 'EN');
          nameA = nameAarray[0][name];
          nameB = nameBarray[0][name];
          //console.log(nameA , nameB);
        } else {
          nameA = nameA[name];
          nameB = nameB[name];
        }
      } else {
        nameA = list[a][name];
        nameB = list[b][name];
        //console.log(nameA);
      }
      //console.log(nameB);
      if (typesort == 'string') {
        if (sortbyNow == 'ASC') {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
          return 0; //default return value (no sorting)
        } else {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
          return 0; //default return value (no sorting)
        }
      } else if (typesort == 'number') {    
        if(!isNumber(nameA)) {
          nameA = nameA.replace(/[^0-9]/g,'');
        }    
        if(!isNumber(nameA)) {
          nameB = nameB.replace(/[^0-9]/g,'');
        }  
        console.log(nameB); 
        if (sortbyNow == 'ASC') {
          return nameB - nameA;
        } else {
          return nameA - nameB;
        }
      } else if (typesort == 'date') {
        if (sortbyNow == 'ASC') {
          if ((new Date(nameA).getTime() > new Date(nameB).getTime())) {
            return -1;
          } else if ((new Date(nameA).getTime() < new Date(nameB).getTime())) {
            return 1;
          } else {
            return 0;
          }
        } else {
          if ((new Date(nameA).getTime() > new Date(nameB).getTime())) {
            return 1;
          } else if ((new Date(nameA).getTime() < new Date(nameB).getTime())) {
            return -1;
          } else {
            return 0;
          }
        }
      }

    });
    for (let keys in keysSorted) {
      newsdata[keys] = list[keysSorted[keys]];
    }
    if (newsdata.length > 0) {
      list = newsdata;
      this.changedatalist(list)
    }
  }
    // add new plan type
  addPlanType() {
    // this.modalService.open('add-plan-type-modal', {totalDeliverTimes: 1,totalChargeTimes: 1});
    this.modalService.open('add-plan-type-modal', {order: 0});
  }

  // edit plan type
  editPlanType(_planTypeItem : any) {
    this.planTypeItem = _planTypeItem;
    this.modalService.open('edit-plan-type-modal', this.planTypeItem);
  }

  // when has change data
  hasChangePlanType(status: boolean) {
    if (status)
      this.getPlanTypeList();
  }
}
