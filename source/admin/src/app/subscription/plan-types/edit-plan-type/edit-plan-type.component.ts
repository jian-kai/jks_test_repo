import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { Location } from '@angular/common';
import { DialogService } from '../../../core/services/dialog.service';
import { PlanService } from '../../../core/services/plan.service';

@Component({
  selector: 'edit-plan-type',
  templateUrl: './edit-plan-type.component.html',
  styleUrls: ['./edit-plan-type.component.scss']
})
export class EditPlanTypeComponent  implements OnInit {
  public planTypeForm: FormGroup;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  private planTypeItem: any;
  public isLoading: boolean = true;
  public planTypeId : number;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    el: ElementRef,
    private planService: PlanService,
    private location: Location,
    private activateRoute: ActivatedRoute,
  ) {
      this.planTypeForm = builder.group({
        name: ['', [Validators.required, Validators.maxLength(200)]],
        totalDeliverTimes: [''],
        totalChargeTimes: [''],
        subsequentDeliverDuration: [''],
        subsequentChargeDuration: [''],
        prefix: ['', Validators.required],
        order: [''],
        planTypeTranslate: builder.array([], Validators.compose([Validators.required])),
      });
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
      this.activateRoute.params.subscribe(params => {
        this.planTypeId = +params['id']// (+) converts string 'id' to a number
      });
  }
  
  ngOnInit() {
    this.getInfo();
  }

  // get user info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.plan_types_create_update}/${this.planTypeId}`).subscribe(
      data => {
        this.planTypeForm.patchValue(data.data);
        data.data.planTypeTranslate.forEach(element => {
          let controlTranslate = <FormArray>this.planTypeForm.controls['planTypeTranslate'];
          let prDtsCtrl = this.planService.initPlanTypeTranslate();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlTranslate.push(prDtsCtrl);
        });
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  // do edit plan type
  doEditPlanType() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        let credentials = this.planTypeForm.value;
        this.loadingService.display(true);
        this._http._updatePut(`${this.appConfig.config.api.plan_types_create_update}/${this.planTypeId}`, credentials).subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }

}