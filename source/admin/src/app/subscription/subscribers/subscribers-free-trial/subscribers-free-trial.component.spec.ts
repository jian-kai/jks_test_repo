import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribersFreeTrialComponent } from './subscribers-free-trial.component';

describe('SubscribersFreeTrialComponent', () => {
  let component: SubscribersFreeTrialComponent;
  let fixture: ComponentFixture<SubscribersFreeTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribersFreeTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribersFreeTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
