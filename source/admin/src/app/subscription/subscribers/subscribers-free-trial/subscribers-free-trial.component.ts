import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PagerService } from '../../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { ListViewsComponent } from '../../../core/components/list-views/list-views.component';
import { RouterService } from '../../../core/helpers/router.service';

@Component({
  selector: 'app-subscribers-free-trial',
  templateUrl: './subscribers-free-trial.component.html',
  styleUrls: ['./subscribers-free-trial.component.scss',
    '../../../../assets/sass/subscription.scss',
    '../../../../assets/sass/table.scss']
})
export class SubscribersFreeTrialComponent extends ListViewsComponent {
  public subscriberList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public apiUrl : string = `${this.appConfig.config.api.subscribers_free_trial}`;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public fromDate: any;
  public toDate: any;
  public cancellationSub: any;
  public title: string = 'Subscribers Free Trial';


constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    } else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 30);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }

    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    } else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }

    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }

    this.apiUrl = `${this.appConfig.config.api.subscribers_free_trial}`;
    let params = this.globalService._URLSearchParams(this.options);
    
    this.options.set('orderBy', `createdAt_DESC`);

    // [Admin][Revamp] Searching without filtering | Remove fromDate & toDate if we have param 's'
    if (this.options.get('s')) {      
      this.options.delete('fromDate');
      this.options.delete('toDate');
    }
    
    this.loadingService.display(true);
    this._http._getList(`${this.apiUrl}?` + this.options.toString()).subscribe(
      data => {
        this.subscriberList = data;
        this.totalItems = data.count;
        console.log('this.totalItems', this.totalItems)
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', '["firstName"]');
    } else {
      this.filterby('orderBy', '["firstName_DESC"]');
    }
  }

  changeFromDate(_fromDate) {
    if(_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
    // this.getData();
  }

  changeToDate(_toDate) {
    if(_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // delete product 
  deleteProduct(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updateProductStatus(_id, 'removed');
      }
    }));
  }

  // active product 
  activeProduct(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updateProductStatus(_id, 'active');
      }
    }));
  }

  // update Product status: 'removed' | 'active'
  updateProductStatus(_id: number, _status: string) {
    this.loadingService.display(true);
    let formData: FormData = new FormData();
    formData.append('status', _status);
    this._http._updatePut(`${this.appConfig.config.api.products}/${_id}`, formData, "form-data").subscribe(
      data => {
        if (data.ok) {
          this.loadingService.display(false);
          this.getData();
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }
  
  // download CSV file
  downloadCSV() {
    this.loadingService.display(true);
    this._http._getDetailCsv(`${this.apiUrl}/download?` + this.options.toString()).subscribe(
      data => {
        let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement('a');
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
          dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', `Subscribers Free Trial for ${this.globalService.formatDate(this.fromDate)} to ${this.globalService.formatDate(this.toDate)}.csv`);
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  changedatalist(list){ 
    this.subscriberList.rows =  list ;  
  }

  cancel(item) {
    this.cancellationSub = item;
    this.modalService.open('edit-subscribers-modal', item.id);

    // let cancleMsg = 'Are you sure?';
    // this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
    //   if (result) {
    //     this._http._delete(`${this.apiUrl}/${item.id}`).subscribe(
    //       data => {
    //         // this.loadingService.display(false);
    //         this._dialogService.result('Subscriber Free Trial cancellation request was successful').afterClosed().subscribe((result => {
    //           this.getData();
    //         }));
    //       },
    //       error => {
    //         let message = typeof error === 'string' ? error : JSON.parse(error).message
    //         this.loadingService.display(false);
    //         this._dialogService.result(message).afterClosed().subscribe((result => {
    //           this.loadingService.display(false);
    //         }));
    //         console.log(error);
    //         this.alertService.error(error);
    //       }
    //     );
    //   }
    // }));
  }

  hasChangeSubscriber(status: boolean) {
    if (status && this.cancellationSub.status !== 'Canceled') {
      let cancleMsg = 'Are you sure you want to cancel this subscriber?';
      this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
        if (result) {
          this._http._delete(`${this.apiUrl}/${this.cancellationSub.id}`).subscribe(
            data => {
              // this.loadingService.display(false);
              this._dialogService.result('Subscriber Free Trial cancellation request was successful').afterClosed().subscribe((result => {
                this.getData();
              }));
            },
            error => {
              let message = typeof error === 'string' ? error : JSON.parse(error).message
              this.loadingService.display(false);
              this._dialogService.result(message).afterClosed().subscribe((result => {
                this.loadingService.display(false);
              }));
              console.log(error);
              this.alertService.error(error);
            }
          );
        }
      }));
    }
  }

  hasAddedSubscriber(status: boolean) {
    this.getData();
  }

  addNewSubscriber() {
    this.modalService.open('add-subscribers-modal', true);
  }
}