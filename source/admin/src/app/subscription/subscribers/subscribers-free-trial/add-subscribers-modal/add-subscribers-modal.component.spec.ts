import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubscribersModalComponent } from './add-subscribers-modal.component';

describe('AddSubscribersModalComponent', () => {
  let component: AddSubscribersModalComponent;
  let fixture: ComponentFixture<AddSubscribersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubscribersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubscribersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
