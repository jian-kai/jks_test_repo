import { Component, ElementRef, OnInit, Output, EventEmitter, Input, AfterViewInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from '../../../../../app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { LoadingService } from '../../../../core/services/loading.service';

@Component({
  selector: 'add-subscribers-modal',
  templateUrl: './add-subscribers-modal.component.html',
  styleUrls: ['./add-subscribers-modal.component.scss']
})
export class AddSubscribersModalComponent extends ModalComponent {
  public userInfo: any;
  public currentUser: any;
  public subscriberForm: FormGroup;
  public countries: any;
  public freeTrialList: any;
  private roles: any;
  private enableSelectCountry: boolean = false;
  public isLoading: boolean = true;
  public isLoadingFreeTrial: boolean = false;
  public subsId: any;
  public customerUser: any;
  public selectedPlan: any;
  @Output() hasAdded: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private _dialogService: DialogService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'add-subscribers-modal';

    this.subscriberForm = builder.group({
      email: ['', Validators.required],
      CountryId: ['', Validators.required],
      planCountryId: ['', Validators.required],
      PlanOptionId: ['', Validators.required]
    });
  }

  setInitData(param) {
    this.subscriberForm = this.builder.group({
      email: ['', Validators.required],
      CountryId: ['', Validators.required],
      planCountryId: ['', Validators.required],
      PlanOptionId: ['', Validators.required]
    });
    this.selectedPlan = null;
    this.getSubscriberCountries();
  }

  getSubscriberCountries() {
    this.isLoading = true;
    return this._http._getList(`${this.appConfig.config.api.countries_subscriber}`).subscribe(
      data => {
        this.countries = data;
        this.isLoading = false;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error)
        this.isLoading = false;
      }
    )
  }

  changeCountry() {
    if(this.subscriberForm.value.CountryId) {
      this.isLoadingFreeTrial = true;
      this.selectedPlan = null;
      this.subscriberForm.controls.planCountryId.setValue('');
      this.subscriberForm.controls.PlanOptionId.setValue('');
      // get all plans
      let options = {
        userTypeId: 1,
        CountryId: this.subscriberForm.value.CountryId
      };
      let params = this.globalService._URLSearchParams(options);
      this._http._getList(`${this.appConfig.config.api.free_trial}?` + params.toString()).subscribe(
        data => {
          if (data) {
            this.freeTrialList = [];
            data.forEach(element => {
              element.Plans.forEach(plan => {
                this.freeTrialList.push(plan);
              });
            });
            if(this.subscriberForm.value.planCountryId) {
              this.selectedPlan = this.freeTrialList.filter(plan => plan.id === parseInt(this.subscriberForm.value.planCountryId))[0];
            }
            // this.freeTrialList = data;
          }
          this.isLoadingFreeTrial = false;
        },
        error => {
          console.log(error.error)
          this.isLoadingFreeTrial = false;
        }
      )
    }
  }

  changePlan() {
    this.subscriberForm.controls.PlanOptionId.setValue('');
    if(this.subscriberForm.value.planCountryId) {
      this.selectedPlan = this.freeTrialList.filter(plan => plan.planCountry[0].id === parseInt(this.subscriberForm.value.planCountryId))[0];
      console.log('this.selectedPlan', this.selectedPlan);
    }
  }

  // do edit
  doAddSubscriber() {
    this.isLoading = true;

    // check email is exist or not
    return new Promise((resolve, reject) => {
      this._http._create(this.appConfig.config.api.checkEmail, { email: this.subscriberForm.value.email }).subscribe(
        data => {
          if(!data.isExisted && !data.user) {
            this._dialogService.result('user not existed').afterClosed().subscribe(result => {
              if (result) {
                this.isLoading = false;
                return;
              }
            });
          } else {
            this.customerUser = data.user;
            this._http._getList(`${this.appConfig.config.api.admin_user_cards}`.replace('[userID]', this.customerUser.id)).subscribe(
              data => {
                // check user has Card
                if(data[0]) {
                  this.customerUser.cards = data;
                  
                  // check user has Delivery Address
                  if(this.customerUser && !this.customerUser.deliveryAddresses[0]) {
                    this._dialogService.result('this account missing Deliverry Address').afterClosed().subscribe((result => {
                      if (result) {
                        this.isLoading = false;
                        return;
                      }
                    }));
                  } else {
                    // check user has free trial
                    if (this.customerUser && this.customerUser.subscriptions.length > 0) {
                      let checkIsTrialSub : boolean = false;
                      this.customerUser.subscriptions.forEach(item => {
                        if (item.isTrial && item.status !== "Canceled") {
                          checkIsTrialSub = true;
                        }
                      });
                      console.log('checkIsTrialSub --- ', checkIsTrialSub)
                      if (checkIsTrialSub) {
                        this._dialogService.result('Please be informed this account is not eligible for Shave plan Free Trial').afterClosed().subscribe((result => {
                          if (result) {
                            this.isLoading = false;
                            return;
                          }
                        }));
                      } else {
                        this.makeSubscriber();
                      }
                    } else {
                      this.makeSubscriber();
                    }
                  }
                } else {
                  this._dialogService.result('this account missing Card').afterClosed().subscribe(result => {
                    if (result) {
                      this.isLoading = false;
                      return;
                    }
                  });
                }
              },
              error => {
                this.isLoading = false;
                console.log(error.error)
                this.alertService.error(error);
              }
            )
          }
        },
        error => {
          this.isLoading = false;
              this.alertService.error(error);
        }
      )
    });
  }

  makeSubscriber() {
    let cardId;
    let deliveryAddressId;
    let billingAddressId;

    // get card to add subscriber
    this.customerUser.cards.forEach(card => {
      if(card.isDefault) {
        cardId = card.id;
      }
    });
    if(!cardId) {
      cardId = this.customerUser.cards[0].id;
    }

    // get delivery address to add subscriber
    if(this.customerUser.defaultShipping) {
      deliveryAddressId = this.customerUser.defaultShipping;
    } else {
      deliveryAddressId = this.customerUser.deliveryAddresses[0].id;
    }

    // get billing address to add subscriber
    if(this.customerUser.defaultBilling) {
      billingAddressId = this.customerUser.defaultBilling;
    } else {
      billingAddressId = this.customerUser.deliveryAddresses[0].id;
    }

    let _orderData = {
      userTypeId: 1,
      UserId: this.customerUser.id,
      planCountryId: parseInt(this.subscriberForm.value.planCountryId),
      PlanOptionId: this.subscriberForm.value.PlanOptionId,
      email: this.subscriberForm.value.email,
      CardId: cardId,
      DeliveryAddressId: deliveryAddressId,
      BillingAddressId: billingAddressId,
      isWeb: true
    }

    this.createFreeTrialOrder(_orderData)
    .then(result => {
      this.hasAdded.emit(true);
      this.isLoading = false;
      this.close();
    })
    .catch(error => {
      this.isLoading = false;
    });
  }

  createFreeTrialOrder(orderData) {
    // get utm_source
    // let utmSource = this.storage.getGtmSource();
    // utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    // if(utmSource && utmSource !== '') {
    //   orderData.utmSource = utmSource;
    //   orderData.utmMedium = this.storage.getGtmMedium();
    //   orderData.utmCampaign = this.storage.getGtmCampaign();
    //   orderData.utmTerm = this.storage.getGtmTerm();
    //   orderData.utmContent = this.storage.getGtmContent();
    // }

    return new Promise((resolved, reject) => {
      // if(this.stepsData.payment.data.paymentMethod === 'stripe') {
        this._http._create(`${this.appConfig.config.api.place_order_trial}/stripe`, orderData)
          .subscribe(
            res => {
              this._dialogService.result('Add Shave plan Free Trial was successfully').afterClosed().subscribe((result => {
                if (result) {
                  this.isLoading = false;
                  return;
                }
              }));
              console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.isLoading = false;
              reject(error);
            }
          );
      // }
    });
  }
}
