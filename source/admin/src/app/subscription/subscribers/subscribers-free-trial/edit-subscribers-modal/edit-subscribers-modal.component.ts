import { Component, ElementRef, OnInit, Output, EventEmitter, Input, AfterViewInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from '../../../../../app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';

@Component({
  selector: 'edit-subscribers-modal',
  templateUrl: './edit-subscribers-modal.component.html',
  styleUrls: ['./edit-subscribers-modal.component.scss']
})
export class EditSubscribersModalComponent extends ModalComponent {
  public userInfo: any;
  public currentUser: any;
  public subscriberForm: FormGroup;
  public countries: any;
  private roles: any;
  private enableSelectCountry: boolean = false;
  public isLoading: boolean = true;
  public subsId: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Input() title: string;
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'edit-subscribers-modal';

    this.subscriberForm = builder.group({
      cancellationReason: ['', Validators.required]
    });
  }

  setInitData(_subsId) {
    this.subsId = _subsId;
    this._http._getDetail(`${this.appConfig.config.api.subscriptions}?id=${_subsId}`).subscribe(
      data => {
        this.subscriberForm = this.globalService.setValueFormBuilder(this.subscriberForm, data[0]);
        this.isLoading = false;
      },
      error => {
        this.isLoading = false;
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  // do edit
  doEditSubscriber() {
    let credentials = this.subscriberForm.value;
    credentials.id = this.subsId;
    this._http._update(`${this.appConfig.config.api.subscriptions}/${this.subsId}`, credentials).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }
}
