import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubscribersModalComponent } from './edit-subscribers-modal.component';

describe('EditSubscribersModalComponent', () => {
  let component: EditSubscribersModalComponent;
  let fixture: ComponentFixture<EditSubscribersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubscribersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubscribersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
