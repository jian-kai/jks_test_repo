import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PagerService } from '../../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'app-subscriber-details',
  templateUrl: './subscriber-details.component.html',
  styleUrls: ['./subscriber-details.component.scss']
})
export class SubscriberDetailsComponent implements OnInit {
  public isLoading: boolean = true;
  public userId: string;
  public userType: string;
  public orderInfo: any;
  public trackingForm: FormGroup;
  public orderStates: any;
  private apiUrl : string = "";
  public cancelModalTitle: string = 'Subscribers';

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,
    private route: ActivatedRoute,
    private _dialogService: DialogService) {
    this.trackingForm = builder.group({
      tracking: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.route.params.subscribe(params => {
      this.userId = params['id'];
      if (!this.userId) {
        this.backToList();
      }
      this.userType = params['type'];
      if (this.userType && this.userType === 'users') {
        this.apiUrl = `${this.appConfig.config.api.subscriptions}?id=${this.userId}`;
      } else if (this.userType && this.userType === 'sellers') {
        this.apiUrl = `${this.appConfig.config.api.subscriptions}?SellerUserId=${this.userId}`;
      } else {
        this.backToList();
      }
      this.getOrderDetail();
    });
  }

  // get order detail info
  getOrderDetail() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.apiUrl}`).subscribe(
      data => {
        this.orderInfo = data;
        console.log('this.orderInfo', this.orderInfo);
        
        // get Order States
        this.globalService.getOrderStates().then((_data) => {
          this.orderStates = _data;
        });
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  // update tracking number
  updateTrackingNumber(_tracking: string) {
    let datas = { tracking: _tracking };
    this.updateOrder(datas);
  }

  // update order status
  updateStatus(status: string) {
    let datas = { states: status };
    this.updateOrder(datas);
  }

  cancelSubs() {
    this.modalService.open('edit-subscribers-modal', this.orderInfo[0].id);
  }

  doCancelApi() {
    this._http._delete(`${this.appConfig.config.api.subscribers_free_trial}/${this.orderInfo[0].id}`).subscribe(
      data => {
        if (this.orderInfo[0].status === 'Canceled') {
          // just change reason
          this.getOrderDetail();
        } else {
          this._dialogService.result('Subscriber cancellation request was successful').afterClosed().subscribe((result => {
            this.getOrderDetail();
          }));
        }
      },
      error => {
        let message = typeof error === 'string' ? error : JSON.parse(error).message
        this.loadingService.display(false);
        this._dialogService.result(message).afterClosed().subscribe((result => {
          this.loadingService.display(false);
        }));
        this.alertService.error(error);
      }
    );
  }

  cancelSubsModal(status: boolean) {
    if (status && this.orderInfo[0].status !== 'Canceled') {
      let cancelMsg = 'Are you sure you want to cancel this subscriber?';
      this._dialogService.confirm(cancelMsg).afterClosed().subscribe((result => {
        if (result) {
          this.doCancelApi();
        }
      }));
    } else if (this.orderInfo[0].status === 'Canceled') {
      this.doCancelApi();
    }
  }

  // update order
  updateOrder(datas) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this._http._updatePut(`${this.appConfig.config.api.orders}/${this.orderInfo.id}`, datas).subscribe(
          res => {
            this.getOrderDetail();
          },
          error => {
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // back to order list
  backToList() {
    this.location.back();
    // let path = "/subscribers/" + this.globalService.checkTypeOfOrder(this.orderInfo.states);
    // this._router.navigate([path], { queryParams: {} });
  }
}
