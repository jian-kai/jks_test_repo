import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserCustomerModalComponent } from './edit-user-customer-modal.component';

describe('EditUserCustomerModalComponent', () => {
  let component: EditUserCustomerModalComponent;
  let fixture: ComponentFixture<EditUserCustomerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserCustomerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserCustomerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
