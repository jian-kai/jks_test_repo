import { Component, ElementRef, OnInit, Output, EventEmitter, Input, AfterViewInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { DialogService } from '../../core/services/dialog.service';

@Component({
  selector: 'edit-user-customer-modal',
  templateUrl: './edit-user-customer-modal.component.html',
  styleUrls: ['./edit-user-customer-modal.component.scss']
})
export class EditUserCustomerModalComponent extends ModalComponent{
  public currentUser: any;
  public userForm: FormGroup;
  public countries: any;
  public roles: any;
  public enableSelectCountry : boolean = false;
  public adminIds: any;
  public userItem: any;

  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private _dialogService: DialogService
  ) {
    super(modalService, el);
    this.id = 'edit-user-customer-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.userForm = builder.group({
      firstName: [''],
      lastName: [''],
      email: ['', Validators.required],
    });   
    
  }

  setInitData(_userItem) {
    this.userItem = _userItem;
    this.userForm = this.globalService.setValueFormBuilder(this.userForm, _userItem);
    if (this.globalService.isSupperAdmin()) {
      this.userForm.addControl("CountryId",  new FormControl("", Validators.required));
      this.userForm.controls["CountryId"].setValue(this.currentUser.Country.id);
      this.globalService.getCountryList().then((data) => {
        this.countries = data;
      });
      this.globalService.getUserList().then((data) => {
        this.adminIds = data;
      });
    }

    if (this.globalService.isSupperAdmin() || this.globalService.isAdmin()) {
      this.userForm.addControl("AdminId",  new FormControl("", Validators.required));
    }
  }

  // do add user salesman
  doEditCustomer() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        let credentials = this.userForm.value;
        this._http._updatePut(`${this.appConfig.config.api.customer_users}/salesman/${this.userItem.id}`, credentials).subscribe(
          data => {
            this.hasChange.emit(true);
            this.close();
          },
          error => {
            this.alertService.error(error);
          }
        )
      }
    }));
  }

}