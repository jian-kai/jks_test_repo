import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserCustomerModalComponent } from './add-user-customer-modal.component';

describe('AddUserCustomerModalComponent', () => {
  let component: AddUserCustomerModalComponent;
  let fixture: ComponentFixture<AddUserCustomerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserCustomerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserCustomerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
