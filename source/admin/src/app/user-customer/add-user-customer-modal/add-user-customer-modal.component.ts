import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import {HttpService} from '../../core/services/http.service';
import {GlobalService} from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../core/services/storage.service';
import {AlertService} from '../../core/services/alert.service';

@Component({
  selector: 'add-user-customer-modal',
  templateUrl: './add-user-customer-modal.component.html',
  styleUrls: ['./add-user-customer-modal.component.scss']
})
export class AddUserCustomerModalComponent extends ModalComponent{
  private userInfo : any;
  private currentUser: any;
  public userForm: FormGroup;
  private countries: any;
  private roles: any;
  private enableSelectCountry : boolean = false;
  private adminIds: any;

  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService
  ) {
    super(modalService, el);
    this.id = 'add-user-customer-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.userForm = builder.group({
      firstName: [''],
      lastName: [''],
      email: ['', Validators.required],
    });

    if (this.globalService.isSupperAdmin()) {
      this.userForm.addControl("CountryId",  new FormControl("", Validators.required));
      this.userForm.controls["CountryId"].setValue(this.currentUser.Country.id);
      this.globalService.getCountryList().then((data) => {
        this.countries = data;
      });
      this.globalService.getUserList().then((data) => {
        this.adminIds = data;
      });
    }

    if (this.globalService.isSupperAdmin() || this.globalService.isAdmin()) {
      this.userForm.addControl("AdminId",  new FormControl("", Validators.required));
    }
    
    
  }

  // do add user salesman
  doAddSalesman() {
    let credentials = this.userForm.value;
    this._http._create(`${this.appConfig.config.api.customer_users}/salesman`, credentials).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

}