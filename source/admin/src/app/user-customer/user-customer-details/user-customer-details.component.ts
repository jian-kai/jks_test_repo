import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';


@Component({
  selector: 'app-user-customer-details',
  templateUrl: './user-customer-details.component.html',
  styleUrls: ['./user-customer-details.component.scss']
})
export class UserCustomerDetailsComponent implements OnInit {
  public isLoading: boolean = true;
  public customerId: string;
  public customerInfo: any;
  public deliveryAddressList: Array<any> = [];
  public billingAddressList: Array<any> = [];
  public defaultShipping: any;
  public defaultBilling: any;
  cardsList: Array<any>;
  
  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private location: Location,
    private _router: Router,
    public modalService: ModalService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.isLoading = true;
    this.route.params.subscribe(params => {
      this.customerId = params['id'];
      if (!this.customerId) {
        this.backToList();
      }
      this.getUserDetail();
    });
  }

  //get user info
  getUserDetail() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.customers}`.replace('[id]', this.customerId)).subscribe(
      data => {
        this.customerInfo = data;
        console.log('infor-- ', this.customerInfo);
        this.loadingService.display(false);
        this.isLoading = false;
      },
      error => {
        this.alertService.error(error);
        this.loadingService.display(false);
        this.isLoading = false;
        console.log(error.error);
      }
    )
  }

  // back to user list
  backToList() {
    let path = "/users/customers";
    this._router.navigate([path], { queryParams: {} });
  }

  backToPage() {
    this.location.back();
  }

}
