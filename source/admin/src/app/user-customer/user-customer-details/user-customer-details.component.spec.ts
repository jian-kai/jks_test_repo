import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCustomerDetailsComponent } from './user-customer-details.component';

describe('UserCustomerDetailsComponent', () => {
  let component: UserCustomerDetailsComponent;
  let fixture: ComponentFixture<UserCustomerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCustomerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCustomerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
