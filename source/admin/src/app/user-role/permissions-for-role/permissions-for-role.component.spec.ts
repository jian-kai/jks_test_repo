import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionsForRoleComponent } from './permissions-for-role.component';

describe('PermissionsForRoleComponent', () => {
  let component: PermissionsForRoleComponent;
  let fixture: ComponentFixture<PermissionsForRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionsForRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsForRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
