import { Component, ElementRef, OnInit, Output, EventEmitter, Input,  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-permissions-for-role',
  templateUrl: './permissions-for-role.component.html',
  styleUrls: ['./permissions-for-role.component.scss'],
  providers: [UserService]
})
export class PermissionsForRoleComponent implements OnInit {
  @Input() permissionsForm;
  constructor(private builder: FormBuilder,
    private userService: UserService  
  ) { }

  ngOnInit() {
    // this.permissionsForm.controls.PathId.valueChanges.subscribe(data => {
    //   let _data : any = {};
    //   if (!data) {
    //     this.userService.checkAllPermissions(this.permissionsForm, false);
    //   } else {
    //     this.permissionsForm.controls.PathId.setValue(this.permissionsForm.value.PathIdTmp);
    //     this.userService.checkAllPermissions(this.permissionsForm, true)
    //   }
    //   console.log('this.permissionsForm.controls ', this.permissionsForm.value);
    // });
  }
  selectPath(event) {
    console.log('event.target.checked - ', event.target.checked)
    if (event.target.checked) {
      this.permissionsForm.controls.PathId.setValue(this.permissionsForm.value.PathIdTmp);
      this.userService.checkAllPermissions(this.permissionsForm, true)
    } else {
      this.permissionsForm.controls.PathId.setValue('');
      this.userService.checkAllPermissions(this.permissionsForm, false);
    }
    console.log('this.permissionsForm.value - ', this.permissionsForm.value)
  }

}
