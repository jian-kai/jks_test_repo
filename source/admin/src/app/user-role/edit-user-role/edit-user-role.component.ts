import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { LoadingService } from '../../core/services/loading.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-edit-user-role',
  templateUrl: './edit-user-role.component.html',
  styleUrls: ['./edit-user-role.component.scss'],
  providers: [UserService]
})
export class EditUserRoleComponent implements OnInit {
  public currentUser: any;
  public roleForm: FormGroup;
  public roleInfo : any;
  public roleId : number;
  public pathList : any = [];

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private userService: UserService,
    private location: Location,) {
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser || !this.currentUser.id) {
        console.log('User is not exist');
        return;
      }

      this.activateRoute.params.subscribe(params => {
        this.roleId = +params['id']// (+) converts string 'id' to a number
      });

      this.roleForm = builder.group({
        roleName: ['', Validators.required],
        description: ['', Validators.required],
        roleDetails: builder.array([]),
      });
    }

  ngOnInit() {
    this.loadingService.display(true);
    // get role detail
    this.getDetail().then(
      data => {
        this.loadingService.display(false);
        this.roleInfo = data;
        this.roleForm.patchValue(this.roleInfo);
        // get path list
        this.getPaths().then(
          data => {
            this.pathList = data;
            this.initRoleDetails();
            console.log('this.pathList - ', this.pathList);
          },
          error => {
            console.log('error - ', error.error);
          }
        )
        console.log('this.roleInfo - ', this.roleInfo);
      },
      error => {
        this.loadingService.display(false);
        console.log('error - ', error.error);
      }
    )

    

    
    this.loadingService.status.subscribe((value: boolean) => {
      // this.isLoading = value;
    });
  }

  getDetail() {
    return new Promise((resolve, reject) => {
      this._http._getDetail(`${this.appConfig.config.api.user_role}/${this.roleId}`).subscribe(
        data => {
          resolve(data);
        },
        error => {
          this.alertService.error(error);
          reject(error)
        }
      )
    })
  }

  getPaths() {
    return new Promise( (resolve, reject ) => {
      this._http._getList(`${this.appConfig.config.api.path}`).subscribe(
        data => {
          resolve(data);
        },
        error => {
          this.alertService.error(error);
          reject(error)
        }
      )
    })
  }

  initRoleDetails() {
    let _roleDetails : any = [] = this.roleInfo.roleDetails;
    console.log('_roleDetails', _roleDetails)
    this.pathList.forEach(path => {
      let control = <FormArray>this.roleForm.controls['roleDetails'];
      let prCoCtrl = this.userService.initRoleDetails();
      prCoCtrl.patchValue(path);
      prCoCtrl.patchValue({PathIdTmp: path.id});
      prCoCtrl.patchValue({PathId: path.id});
      prCoCtrl.patchValue({
        canView: false,
        canCreate: false,
        canEdit: false,
        canDelete: false,
        canUpload: false,
        canDownload: false,
      })
      let _getPath;
      if (this.roleInfo && this.roleInfo.roleDetails && this.roleInfo.roleDetails.length > 0) {
        _getPath = this.roleInfo.roleDetails.filter(role => role.Path.pathValue === path.pathValue);
        if (_getPath && _getPath[0]) {
          _getPath = _getPath[0];
          prCoCtrl.patchValue(_getPath);
        }
      }
      control.push(prCoCtrl);
      
    });
    console.log('this.roleForm -- ', this.roleForm)
  }

  // do edit
  doEditRole() {
    let _data = this.roleForm.value;

    console.log('_data', _data);
    this.loadingService.display(true);
    this._http._updatePut(`${this.appConfig.config.api.user_role}/${this.roleId}`, _data).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
        console.log('data', data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }

}
