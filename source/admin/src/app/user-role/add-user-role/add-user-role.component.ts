import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { LoadingService } from '../../core/services/loading.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-add-user-role',
  templateUrl: './add-user-role.component.html',
  styleUrls: ['./add-user-role.component.scss'],
  providers: [UserService]
})
export class AddUserRoleComponent implements OnInit {
  public currentUser: any;
  public roleForm: FormGroup;
  public roleInfo : any;
  public roleId : number;
  public pathList : any = [];

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private userService: UserService,
    private location: Location,) {
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser || !this.currentUser.id) {
        console.log('User is not exist');
        return;
      }

      this.activateRoute.params.subscribe(params => {
        this.roleId = +params['id']// (+) converts string 'id' to a number
      });

      this.roleForm = builder.group({
        roleName: ['', Validators.required],
        description: ['', Validators.required],
        roleDetails: builder.array([]),
      });
    }

  ngOnInit() {
    this.loadingService.display(true);

    // get path list
    this.getPaths().then(
      data => {
        this.loadingService.display(false);
        this.pathList = data;
        this.initRoleDetails();
        console.log('this.pathList - ', this.pathList);
      },
      error => {
        this.loadingService.display(false);
        console.log('error - ', error.error);
      }
    )
    console.log('this.roleInfo - ', this.roleInfo);

    
    this.loadingService.status.subscribe((value: boolean) => {
      // this.isLoading = value;
    });
  }

  getDetail() {
    return new Promise((resolve, reject) => {
      this._http._getDetail(`${this.appConfig.config.api.user_role}/${this.roleId}`).subscribe(
        data => {
          resolve(data);
        },
        error => {
          this.alertService.error(error);
          reject(error)
        }
      )
    })
  }

  getPaths() {
    return new Promise( (resolve, reject ) => {
      this._http._getList(`${this.appConfig.config.api.path}`).subscribe(
        data => {
          resolve(data);
        },
        error => {
          this.alertService.error(error);
          reject(error)
        }
      )
    })
  }

  initRoleDetails() {
    this.pathList.forEach(path => {
      let control = <FormArray>this.roleForm.controls['roleDetails'];
      let prCoCtrl = this.userService.initRoleDetails();
      let _data : any = {};
      _data = {
        PathId: path.id,
        PathIdTmp: path.id,
        pathValue: path.pathValue,
        canView: false,
        canCreate: false,
        canEdit: false,
        canUpload: false,
        canDownload: false,
        canDelete: false,
      }
      prCoCtrl.patchValue(_data);
      control.push(prCoCtrl);
      
    });
    console.log('this.roleForm -- ', this.roleForm)
  }

  // do add
  doAddRole() {
    let _data = this.roleForm.value;

    console.log('_data', _data);
    this.loadingService.display(true);
    this._http._create(`${this.appConfig.config.api.user_role}`, _data).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
        console.log('data', data);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/subscriptions/plans']);
  }

}
