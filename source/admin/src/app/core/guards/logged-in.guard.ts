import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { UsersService } from '../../auth/services/users.service';

@Injectable()
export class LoggedInGuard implements CanActivate {
  
  constructor(private users: UsersService) { }

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.users.isLoggedIn();
  }
}
