import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild, Renderer } from '@angular/core';
declare var jQuery: any;
import { FileValidator } from '../../directives/required-file.directive';

@Component({
  selector: 'app-import-file',
  templateUrl: './import-file.component.html',
  styleUrls: ['./import-file.component.scss']
})
export class ImportFileComponent implements OnInit {
  @Input() group;
  @Input() controlName: string;
  public file : any;
  public imageUrl : string;
  public show: boolean = true;
  public isEdit: boolean = false;
  constructor( private _elemRef: ElementRef, private _renderer: Renderer ) {}

  ngOnInit() {

  }
  fileChange(e) {
    let file: File = (e.srcElement || e.target).files[0];
    if (file) {
        let file_tmp : any = {name: file.name}
        let reader = new FileReader();
        let target:EventTarget;
        // reader.onload = (event: any) => {
        //   file_tmp.imageUrl = event.target.result;
        // }
        this.file = (file_tmp);
        reader.readAsDataURL(file);
      this.group.controls[this.controlName].setValue(file);
      this.show = false;
    }
  }

  removeAddCase() {
    this.file = null;
    this.show = true;
    this.group.controls[this.controlName].setValue('');
  }



}
