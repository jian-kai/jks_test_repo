import { Component, OnInit, ElementRef, Input, OnDestroy, Injectable, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';

import { ModalService } from '../../services/modal.service';
@Injectable()
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Input() onInit: any;
  private element: any;
  private modalEle: any;

  constructor(public modalService: ModalService, private el: ElementRef) {
    this.element = $(el.nativeElement);
  }

  ngOnInit(): void {
    let modal = this;
    this.element.hide();

    console.log(`init model:: ${this.id}`);

    // ensure id attribute exists
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    // move element to bottom of page (just before </body>) so it can be displayed above everything else
    this.element.appendTo('body');

    // close modal on background click
    this.element.on('click', function (e: any) {
      var target = $(e.target);
      if (!target.closest('.modal-dialog').length) {
        modal.close();
      }
    });

    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);

  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(): void {
    this.modalEle = this.element.find('.modal');
    this.element.show();
    this.element.find('.modal-backdrop').addClass('show');
    this.modalEle.show();
    this.modalEle.addClass('show');
  }

  // close modal
  close(): void {
    this.element.hide();
    this.element.find('.modal-backdrop').removeClass('show');
    this.modalEle.hide();
    this.modalEle.removeClass('show');
  }
}
