import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';
declare var jQuery: any;

@Directive({
  selector: '[trumbowygEditor]'
})
export class TrumbowygEditorDirective {
  @Input('trumbowygEditor') options: any;
  @Input() formN;
  public nativeElement : any;
  constructor(private el: ElementRef) {
  }
  ngAfterViewInit(): void {
    setTimeout(()=>{
      let _options = this.options;
      this.nativeElement = jQuery(this.el.nativeElement);
      this.nativeElement.trumbowyg();
      this.nativeElement.trumbowyg('html', _options.formGroup.value[_options.formControlName])
      let _trumbowyg = this.nativeElement.trumbowyg();
      
      this.nativeElement.on('tbwchange',  {_trumbowyg, _options}, function(_event){
        let _trumbowyg = _event.data._trumbowyg;
        let _options = _event.data._options;
        _options.formGroup.controls[_options.formControlName].setValue(_trumbowyg.trumbowyg('html'));
      });
    },1000);
  }

}
