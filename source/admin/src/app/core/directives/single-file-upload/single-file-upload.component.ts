import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild, Renderer } from '@angular/core';
declare var jQuery: any;
import { FileValidator } from '../../directives/required-file.directive';

@Component({
  selector: 'single-file-upload',
  templateUrl: './single-file-upload.component.html',
  styleUrls: ['./single-file-upload.component.scss']
})
export class SingleFileUploadComponent implements OnInit {
  @Input() group;
  @Input() controlName: string;
  public file: any;
  public imageUrl: string;
  public show: boolean = true;
  public isEdit: boolean = false;
  public imagesize2mb: boolean = false;
  public errors: any;
  public image_upload =  {
    image_formats : ['jpg', 'gif', 'jpeg', 'png'],
    max_file_size_bytes: 15728640,
  }
  constructor(private _elemRef: ElementRef, private _renderer: Renderer) { }

  ngOnInit() {
    if (this.group.controls['id'] && this.group.controls['id'].value) {
      this.isEdit = true;
      this.file = {
        name: this.group.controls['id'].value,
        imageUrl: this.group.controls[this.controlName].value
      };
      if (this.file.imageUrl) {
        this.show = false;
      } else {
        this.show = true;
      }
    }
  }
  
  fileChange(e) {
    let file: File = (e.srcElement || e.target).files[0];
    this.errors = '';
    if (file) {
      let extn = file.name.split(".").pop().toLowerCase();
      if ($.inArray(extn, this.image_upload.image_formats) !== -1) {
        if (file.size > this.image_upload.max_file_size_bytes) {
          this.errors = 'You should upload image maximum size 15 MB';
        }else {
          let file_tmp: any = { name: file.name }
          let reader = new FileReader();
          let target: EventTarget;
          reader.onload = (event: any) => {
            file_tmp.imageUrl = event.target.result;
          }
          this.file = (file_tmp);
          reader.readAsDataURL(file);
          this.group.controls[this.controlName].setValue(file);
          this.show = false;
        }
      }else {
        this.errors = 'Your image file is unsupported, please try again';
      }
    }
  }
  
  removeAddCase() {
    this.file = null;
    this.show = true;
    this.group.controls[this.controlName].setValue('');
  }



}
