
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormControl } from '@angular/forms';
export function numberValidator(prms : any = {}): ValidatorFn {
  return (control: FormControl): {[key: string]: boolean} => {
    if(Validators.required(control)) {
      return null;
    }

    let val: number = control.value;

    if(isNaN(val) || /\D/.test(val.toString())) {

      return {"validateNumber": true};
    } else if(!isNaN(prms.min) && !isNaN(prms.max)) {

      return val < prms.min || val > prms.max ? {"validateNumber": true} : null;
    } else if(!isNaN(prms.min)) {

      return val < prms.min ? {"validateNumber": true} : null;
    } else if(!isNaN(prms.max)) {

      return val > prms.max ? {"validateNumber": true} : null;
    } else {

      return null;
    }
  };
}