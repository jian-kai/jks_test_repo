import { Directive, Input, ElementRef, OnInit } from '@angular/core';
declare var jQuery: any;
@Directive({
  selector: '[datetimepicker]'
})
export class DatetimepickerDirective {

  constructor(private el: ElementRef) { }
  ngOnInit() {
    let formatDt = 'YYYY/MM/DD HH:mm:ss';
    console.log(jQuery(this.el.nativeElement).find("input[type='text']"))
    jQuery(this.el.nativeElement).find("input[type='text']").datetimepicker({
      useCurrent: false,
      //showSeconds: false,
      format: formatDt,
      showTodayButton: true,
    });
  }
}
