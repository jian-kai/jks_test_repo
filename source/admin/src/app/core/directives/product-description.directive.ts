import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import showdown from 'showdown';

@Directive({
  selector: 'appProductDescription'
})
export class ProductDescriptionDirective implements OnInit {
  @Input() productTranslate: Array<any> = [];
  converter: any;

  constructor(private el: ElementRef, private translate: TranslateService) {
    this.converter = new showdown.Converter();
  }

  ngOnInit() {
    let currentLang = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    let translate = this.productTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    this.el.nativeElement.innerHTML = this.converter.makeHtml(translate.description.replace(/\\n/g, '\n'));
    let ul = this.el.nativeElement.querySelector('ul');
    if(ul) {
      ul.style = 'list-style: inside;';
    }
  }
}
