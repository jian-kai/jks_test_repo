import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { AppConfig } from 'app/config/app.config';
import { HttpService } from '../../../core/services/http.service';
import { DialogService } from '../../../core/services/dialog.service';
declare var jQuery: any;

@Component({
  selector: 'multiple-file-upload',
  templateUrl: './multiple-file-upload.component.html',
  styleUrls: ['./multiple-file-upload.component.scss']
})
export class MultipleFileUploadComponent implements OnInit {
  @Input() group;
  @Input() controlName: string;
  @Input() productID: number;
  @Input() planID: number;
  public show: boolean = true;
  public defaultImageIndex: boolean = false;
  public newFiles = [];
  public files = [];
  public url: string;
  public isEdit: boolean = false;
  public count: number = 0;
  public imagesize15mb: boolean = false;

  @ViewChild('ulImages') ulImages: ElementRef;
  constructor(private appConfig: AppConfig,
    private _http: HttpService,
    private _dialogService: DialogService
  ) { }

  ngOnInit() {
    console.log('this.group', this.group)
    if (this.group.controls[this.controlName].value && this.group.controls[this.controlName].value.length > 0) {
      this.isEdit = true;
      this.group.controls[this.controlName].value.forEach((element, index) => {
        if (this.group.controls['defaultImageIndex'] && element.isDefault) {
          this.group.controls['defaultImageIndex'].setValue(index);
        }
        this.newFiles.push(element);
        element.index = this.count;
        this.files.push(element);
        this.count++;
      });
    }
    if (this.group.controls['defaultImageIndex']) {
      this.defaultImageIndex = true;
    }
  }
  
  fileChange(e) {
    // console.log(e);
     console.log(e.target.files[0].size);
     if (e.target.type == 'file' && e.target.size > 0 && e.target.files[0].size >= 15728640) {
      this.imagesize15mb = true;
     } else {
      this.imagesize15mb = false;
      this.show = false;
      let fileList: FileList = (e.srcElement || e.target).files;
      if (fileList.length > 0) {
        for (var _index = 0; _index < fileList.length; _index++) {
          let file: File = fileList[_index];
          let file_tmp: any = { index: this.count, name: file.name }
          this.newFiles[this.count] = file;
          // this.newFiles.push(file);
          let reader = new FileReader();
          let target: EventTarget;
          reader.onload = (event: any) => {
            file_tmp.url = event.target.result;
          }
          this.files.push(file_tmp);
          reader.readAsDataURL(file);
          this.count++;
        }
        this.group.controls[this.controlName].setValue(this.newFiles);
        this.setDefaultImageIndex();
      }
      this.show = true;
     }
   }
   
  removeAddCase(_index: number) {
    this.files = this.files.filter(file => file.index != _index);
    // this.newFiles = this.newFiles.filter((file, index) => index != _index);
    this.newFiles.forEach((element, index) => {
      if (index === _index) {
        // delete this.newFiles[_index];
        this.newFiles.splice(_index, 1);
      }
    })
    if (this.newFiles.length <= 0) {
      this.group.controls[this.controlName].setValue('');
    } else {
      this.group.controls[this.controlName].setValue(this.newFiles);
    }
    if (this.group.controls['defaultImageIndex'] && _index == this.group.controls['defaultImageIndex'].value) {
      this.setDefaultImageIndex(_index);
    }
  }


  removeEditCase(_index: number) {
    this.files = this.files.filter(file => file.index != _index);
    // this.newFiles = this.files.filter(file => file.index != _index && file.id);
    this.newFiles.forEach((element, index) => {
      if (!element.hasOwnProperty("id") && index === _index) {
        delete this.newFiles[_index];
        // this.newFiles.splice(_index, 1);
      }
    })
    if (this.newFiles[_index] && this.newFiles[_index].hasOwnProperty("id")) {
      console.log('this.newFiles[_index]', this.newFiles[_index]);
      // if (this.newFiles[_index].id) {
      //   this.removeImageFromDB(this.newFiles[_index].id);
      // }
      this.newFiles[_index].isRemoved = true;
    }
    if (this.files.length <= 0) {
      this.group.controls[this.controlName].setValue('');
    }
    this.setDefaultImageIndex(_index);
  }

  setDefaultImageIndex(_index?: number) {
    let _reset: boolean = false;
    if (this.group.controls['defaultImageIndex']) {
      if (!this.group.controls['defaultImageIndex'].value) {
        this.group.controls['defaultImageIndex'].setValue(0);
      }
      if ((this.newFiles[this.group.controls['defaultImageIndex'].value] && this.newFiles[this.group.controls['defaultImageIndex'].value].isRemoved)
        || _index === this.group.controls['defaultImageIndex'].value || !this.newFiles[this.group.controls['defaultImageIndex'].value]) {
        _reset = true;
      }
      if (_reset) {
        this.group.controls['defaultImageIndex'].setValue(null);
        this.newFiles.forEach((element, index) => {
          if (!element.isRemoved && !this.group.controls['defaultImageIndex'].value) {
            this.group.controls['defaultImageIndex'].setValue(index);
          }
        });
      }
    }
  }

  getKey(data) {
    for (var prop in data)
      return prop;
  }

  removeImageFromDB(_id: number) {
    if (this.productID) {
      // this._dialogService.confirm().afterClosed().subscribe((result => {
      //   if (result) {
      //   }
      // }));
      let pathDelete = `${this.appConfig.config.api.products}/${this.productID}/product-images/${_id}`;
      this._http._delete(`${pathDelete}`).subscribe(
        data => {
          if (data.ok) {
          }
        },
        error => {
          console.log('error', error);
        }
      )
    }
    if (this.planID) {
      // this._dialogService.confirm().afterClosed().subscribe((result => {
      //   if (result) {
      //   }
      // }));
      let pathDelete = `${this.appConfig.config.api.plans}/plan-images/${_id}`;
      this._http._delete(`${pathDelete}`).subscribe(
        data => {
          if (data.ok) {
          }
        },
        error => {
          console.log('error', error);
        }
      )
    }
  }
}
