import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import {AlertService} from '../../services/alert.service';
import { fadeOutAnimation } from '../../../core/animations/fade-out.animation';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  animations: [fadeOutAnimation]
})
export class AlertComponent implements OnInit {
  private subscription: Subscription;
  public message: any;
  constructor(
    private alertService: AlertService
  ) {
    this.message = null;
  }

  ngOnInit() {
    this.subscription = this.alertService.getMessage().subscribe(message=> {
      this.message = message;
      setTimeout(()=>{
          this.message = null;
      },10000);
    })
  }

}
