import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { PagerService } from '../../services/pager-service.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss', '../../../../assets/sass/pagination.scss']
})
export class PaginationComponent implements OnInit {

  // array of all items to be paged
  private totalPages: number;

  @Input() options: any;

  @Input() classStyle: string = "";

  @Input() idStyle: string = "";

  constructor(private http: Http, private pagerService: PagerService) { }

  ngOnInit() {
    this.totalPages = Math.ceil(this.options.totalItems / this.options.pageSize);
    this.options.currentPage = +this.options.currentPage; 
  }
  setPage(page: number) {
    if (page < 1 || page > this.totalPages) {
        return;
    }
    this.pagerService.setPage(page);
  }

}
