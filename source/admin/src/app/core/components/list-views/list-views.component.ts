import { Component, OnInit, ElementRef, AfterViewInit, Compiler } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Location, LowerCasePipe } from '@angular/common';
import { RouterService } from '../../../core/helpers/router.service';
import { isNumber, isString } from 'util';
import { element } from 'protractor';
import { ButtonAiComponent } from '../button-ai/button-ai.component';

@Component({
  selector: 'app-list-views',
  templateUrl: './list-views.component.html',
  styleUrls: ['./list-views.component.scss']
})
export class ListViewsComponent implements OnInit {
  currentPage: number = 1;
  limit: number = 50;
  totalItems: number = 0;
  currentSortField: string;
  options: URLSearchParams;
  mutiaction: boolean = false;
  multipleChoiceForTables: boolean = false;
  public timer = null;
  limitPageList: Array<number> = [50, 100, 150, 200];
  constructor(private location: Location,
    private routerService: RouterService,
    private el: ElementRef) { }

  ngOnInit() {
    let options = new URLSearchParams(this.location.path(false).split('?')[1]);
    if (!this.options) {
      this.options = new URLSearchParams();
    }
    options.paramsMap.forEach((value, key) => {
      if (value[0] && value[0] !== 'all') {
        // this.options[key] = this.fixedEncodeURI(value[0]);
        this.options.set(`${key}`, this.fixedEncodeURI(value[0]));
      } else {
        this.options.delete(key);
      }
    });
    if (this.options && this.options.get('limit')) {
      this.limit = +options.get('limit');
    } else {
      this.options.set('limit', `${this.limit}`);
    }
    if (this.options && this.options.get('page')) {
      this.currentPage = +options.get('page');
    }
    if (this.options && this.options.get('orderBy')) {
      // this.currentSortField = this.fixedEncodeURI(options.get('orderBy'));
    }
    console.log('this.options', this.fixedEncodeURI(this.options.toString()));
    this.getData();

    this.routerService.runGetdata.subscribe(data => {
      if (data) {
        this.getData();
        this.mutiaction = false;
      }
    });
    this.routerService.runPagination.subscribe(data => {
      if (data) {
        let datalimit = data;
        this.options.set('page', '1');
        this.options.set('limit', datalimit);
        console.log('limit', datalimit);
        this.limit = datalimit;
        this.currentPage = 1;
        this.getData();
      }
    });
  }

  fixedEncodeURI(str) {
    return decodeURIComponent(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
  }

  replaceSpecialChar(str) {
    if (str) {
      return str.replace('["', '').replace('"]', '');
    }
    return '';
  }

  ngAfterViewInit() {
    this.checkSortField();
  }

  getData() {
    // interface for fetch data
  }
  onLimitChange(event) {
    console.log('onLimitChange ------- ');
    this.limit = event.currentTarget.value;
    this.currentPage = 1;
    this.options.set('page', `${this.currentPage}`);
    this.options.set('limit', `${this.limit}`);
    this.routerService.appendRouterParams({
      page: this.currentPage,
      limit: this.limit
    });
    this.getData();
    $('select[name="items-per-page"]').val(this.limit).change();
  }

  pageChange(page) {
    this.currentPage = page;
    this.options.set('page', page);
    // append router params
    let _o = [];
    _o['page'] = (page);
    this.routerService.appendRouterParams(_o);

    // reget data
    this.getData();
  }

  filterby(field, value) {
    if (value === 'all' || !value) {
      this.options.delete(field);
    } else {
      this.options.set(field, (value));
    }
    // append router params
    let _o = [];
    _o[field] = (value);
    this.routerService.appendRouterParams(_o);

    // reget data
    this.getData();
  }
  customizeSearchBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)

  }
  checkSortField() {
    if (this.currentSortField) {
      let sortField = this.el.nativeElement.querySelector(`th[data-field=${this.currentSortField.replace('_DESC', '')}]`);
      if (this.currentSortField.indexOf('_DESC') > -1) {
        sortField.classList.add('sorting_desc');
      } else {
        sortField.classList.add('sorting_asc');
      }
      sortField.classList.remove('sorting');
    }
  }

  sortBy(field, $event) {
    console.log('sortBy');
    // reset all sorting class
    if (field !== this.currentSortField) {
      if (this.el.nativeElement.querySelector('.sorting_asc')) {
        this.el.nativeElement.querySelector('.sorting_asc').classList.add('sorting');
        this.el.nativeElement.querySelector('.sorting_asc').classList.remove('sorting_asc');
      }
      if (this.el.nativeElement.querySelector('.sorting_desc')) {
        this.el.nativeElement.querySelector('.sorting_desc').classList.add('sorting');
        this.el.nativeElement.querySelector('.sorting_desc').classList.remove('sorting_desc');
      }
    }

    // perform sorting
    let currentEle = $event.currentTarget;
    if (currentEle.classList.value === 'sorting') {
      this.options.set('orderBy', `${field}`);
      this.routerService.appendRouterParams({ orderBy: field });
      currentEle.classList.add('sorting_asc');
      currentEle.classList.remove('sorting');
    } else if (currentEle.classList.value === 'sorting_asc') {
      this.options.set('orderBy', `${field}_DESC`);
      this.routerService.appendRouterParams({ orderBy: `${field}_DESC` });
      currentEle.classList.add('sorting_desc');
      currentEle.classList.remove('sorting_asc');
    } else {
      this.options.set('orderBy', `${field}`);
      this.routerService.appendRouterParams({ orderBy: field });
      currentEle.classList.add('sorting_asc');
      currentEle.classList.remove('sorting_desc');
    }
    this.currentSortField = field;
    this.getData();
  }

  changedatalist(list) {
  }
  tableHearSort(event, list, name, typesort?: string, namekey?: string) {
    // let list = this.productList.rows;
    //console.log(list);
    $(event.target).parent().find('th').removeClass('sorted');
    $(event.target).parent().find('th').not(event.target).attr('sortby', '');
    $(event.target).addClass('sorted');
    let sortby = $(event.target).attr('sortby');
    let sortbyNow;
    if (sortby == 'ASC') {
      $(event.target).attr('sortby', 'DESC');
      sortbyNow = 'DESC';
    } else {
      $(event.target).attr('sortby', 'ASC');
      sortbyNow = 'ASC';
    }
    let newsdata = [];
    let keysSorted = Object.keys(list).sort(function (a, b) {
      let nameA, nameB;
      if (typeof (namekey) != 'undefined' && namekey != '') {
        let spl = namekey.split('.');
        nameA = list[a];
        nameB = list[b];
        spl.forEach(e => {
          nameA = nameA[e];
          nameB = nameB[e];
        });
        if (Array.isArray(nameA) || Array.isArray(nameB)) {
          let nameAarray = nameA.filter(item => item.langCode === 'EN');
          let nameBarray = nameB.filter(item => item.langCode === 'EN');
          nameA = nameAarray[0][name];
          nameB = nameBarray[0][name];
          //console.log(nameA , nameB);
        } else {
          nameA = nameA[name];
          nameB = nameB[name];
        }
      } else {
        nameA = list[a][name];
        nameB = list[b][name];
        //console.log(nameA);
      }

      if (typesort == 'string') {
        if (isString(nameA)) {
          nameA = nameA.toLowerCase();
        }
        if (isString(nameB)) {
          nameB = nameB.toLowerCase();
        }
        //console.log(nameB);
        if (sortbyNow == 'ASC') {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
          return 0; //default return value (no sorting)
        } else {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
          return 0; //default return value (no sorting)
        }
      } else if (typesort == 'number') {
        if (!isNumber(nameA)) {
          nameA = nameA.replace(/[^0-9\.]/g, '');
        }
        if (!isNumber(nameB)) {
          nameB = nameB.replace(/[^0-9\.]/g, '');
        }
        if (sortbyNow == 'ASC') {
          return nameB - nameA;
        } else {
          return nameA - nameB;
        }
      } else if (typesort == 'date') {
        if (sortbyNow == 'ASC') {
          if ((new Date(nameA).getTime() > new Date(nameB).getTime())) {
            return -1;
          } else if ((new Date(nameA).getTime() < new Date(nameB).getTime())) {
            return 1;
          } else {
            return 0;
          }
        } else {
          if ((new Date(nameA).getTime() > new Date(nameB).getTime())) {
            return 1;
          } else if ((new Date(nameA).getTime() < new Date(nameB).getTime())) {
            return -1;
          } else {
            return 0;
          }
        }
      }

    });
    for (let keys in keysSorted) {
      newsdata[keys] = list[keysSorted[keys]];
    }
    if (newsdata.length > 0) {
      list = newsdata;
      this.changedatalist(list)
    }
  }
  checkboxchecked(event, id) {
    if (event.target.checked && this.mutiaction === false) {
      this.mutiaction = true;
    }
    let checkedcurrent = $('.actionactive.checkboxall').attr('idchecked');
    let checkedcurrentnew = $(event.target).val();
    //console.log(checkedcurrentnew);
    if (checkedcurrentnew === 'all') {
      if (event.target.checked) {
        let checkallid: any = '';
        $('.actionactive').each(function (i, e) {
          let valuenow = $(e).attr('value');
          if (valuenow != 'all') {
            $(e).prop("checked", true);
            if (checkallid == '') {
              checkallid = valuenow;
            } else {
              checkallid = checkallid + ',' + valuenow;
            }
          }
        });
        $('.actionactive.checkboxall').attr('idchecked', checkallid);
      } else {
        $('.actionactive.checkboxall').attr('idchecked', '');
        this.mutiaction = false;
        $('.actionactive').each(function (i, e) {
          $(e).prop("checked", false);
        });
      }
    } else {
      if (checkedcurrent == undefined || checkedcurrent == '') {
        if (event.target.checked) {
          $('.actionactive.checkboxall').attr('idchecked', checkedcurrentnew);
        }
      } else {
        if (event.target.checked) {
          let addnewvalue = checkedcurrent + ',' + checkedcurrentnew;
          $('.actionactive.checkboxall').attr('idchecked', addnewvalue);
        } else {
          let spl = checkedcurrent.split(',');
          let value = '';
          spl.forEach(e => {
            if (e != checkedcurrentnew) {
              if (value == '') {
                value = e;
              } else {
                value = value + ',' + e;
              }
            }
          })
          $('.actionactive.checkboxall').attr('idchecked', value).prop('checked', false);
          if (value == '') {
            this.mutiaction = false;
          };
        }
      }
    }
  }
}
