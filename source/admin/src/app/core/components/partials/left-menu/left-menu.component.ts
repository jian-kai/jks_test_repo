import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../../core/services/global.service';
import { StorageService } from '../../../../core/services/storage.service';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  public hasDenied: boolean;
  public userInfo: any;
  constructor(public globalService: GlobalService,
    private storage: StorageService,
  ) {
    this.userInfo = this.storage.getCurrentUserItem();
    console.log('this.userInfo ', this.userInfo)
  }

  ngOnInit() {
    this.hasDenied = this.globalService.hasDenied();
  }

  allowAccessTo(_path: string) {
    let _permission_of_path : any = {};
    if (this.userInfo && this.userInfo.Role && this.userInfo.Role.roleDetails && this.userInfo.Role.roleDetails.length > 0) {
      _permission_of_path = this.userInfo.Role.roleDetails.filter(per => per.Path && per.Path.pathValue === _path);
      if (_permission_of_path && _permission_of_path[0]) {
        _permission_of_path = _permission_of_path[0];
      }
    }
    if (_permission_of_path.canView) {
      return true;
    }
    return false;
  }

}
