import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { ModalService } from '../../../../services/modal.service';
import { UsersService } from '../../../../../auth/services/users.service';
import { LoadingService } from '../../../../../core/services/loading.service';
import { StorageService } from '../../../../../core/services/storage.service';
import { AppConfig } from '../../../../../config/app.config';
import { HttpService } from '../../../../services/http.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CountriesService } from '../../../../services/countries.service';
import { GlobalService } from '../../../../../core/services/global.service';
import { AlertService } from '../../../../../core/services/alert.service';

@Component({
  selector: 'top-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [AlertService]
})
export class MenuComponent implements OnInit {
  cartItem: Array<any>;
  isLoggedIn: boolean;
  supportedLanguage: Array<any>;
  countries: Array<Object> = [];
  currentCountry: Object;
  smallLogo: String;
  public totalItemCart: number = 0;
  public userInfo: any; //BehaviorSubject<any>;
  public hasDenied: boolean;

  constructor(private modalService: ModalService, private translate: TranslateService,
    private userService: UsersService,
    private loadingService: LoadingService,
    private storage: StorageService,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private countriesService: CountriesService,
    private router: Router,
    public globalService: GlobalService,
    private alertService: AlertService
  ) { 
    this.supportedLanguage = appConfig.config.supportedLanguage;
    this.cartItem = this.storage.getCartItem();
  }

  ngOnInit() {
    this.hasDenied = this.globalService.hasDenied();
    this.userInfo = this.storage.getCurrentUserItem();

    // get currenct country
    this.currentCountry = this.storage.getCountry();

    // get countries from host
    this.getCountries();

    // handle country change event
    this.countriesService.country.subscribe(country => {
      this.currentCountry = country;
    });

    // handle route change
    // this.router.events.subscribe((val) => {
    //   if(this.router.url !== '/') {
    //     this.smallLogo = 'small-logo'; 
    //   } else {
    //     this.smallLogo = '';
    //   }
    // });
    this.smallLogo = 'small-logo'; 
  }

  getCountries() {
    this.httpService._getList(this.appConfig.config.api.countries).subscribe(
      data => {
        this.countries = data;
        this.countriesService.setCountries(data);
      },
      error => {
        this.alertService.error(error);
        console.log(`Cannot get country: ${error}`)
      }
    )
  }

  getLoggedIn() {
    return this.userService.getLoggedIn();
  }
  openModal(id: string){
    console.log(`openModal ${id}`);
    this.modalService.open(id);
  }
  closeModal(id: string){
    this.modalService.close(id);
  }
  logout(event) {
    event.preventDefault();
    this.userService.logout();
  }
  ngOnDestroy() {
    // this.translate.onLangChange.unsubscribe();
  }

  redirectToLoginPage() {
    this.router.navigate(['/login']);
  }

  // displayShoppingCart() {
  //   if($(".shopping-cart").hasClass( "show" )) {
  //     $(".shopping-cart").removeClass("show");
  //   } else {
  //     $(".shopping-cart").addClass("show");
  //   }
  //   $(".shopping-cart").fadeToggle( "fast");
  // }

  changeAvatar() {
    this.openModal('change-avatar-modal');
  }

  changeCountry(event, country) {
    this.countriesService.changeCountry(country);
    this.translate.use(country.defaultLang);
    event.preventDefault();
  }

}
