import { Component, OnInit } from '@angular/core';
import { AppConfig } from 'app/config/app.config';
import { HttpService } from '../../../../core/services/http.service';
import { SlugifyPipe } from '../../../../core/pipes/slugify.pipe';
import { AlertService } from '../../../../core/services/alert.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  providers: [SlugifyPipe]
})
export class FooterComponent implements OnInit {
  public productTypes;
  public baseUrl: string;
  public isLoadingProductTypes : boolean = true;
  constructor(private appConfig: AppConfig,
    private httpService: HttpService,
    private slugifyPipe: SlugifyPipe,
    private alertService: AlertService) { }

  ngOnInit() {
    this.baseUrl = this.appConfig.config.baseUrl;
    console.log('this.appConfig', this.appConfig.config.baseUrl)
    // this.baseUrl = this.appConfig.apiURL
  }

  getProductType() {
    if  (!this.productTypes) {
      this.isLoadingProductTypes = true;
      this.httpService._getList(`${this.appConfig.config.api.admin_product_types}/`).subscribe(
        data => {
          this.isLoadingProductTypes = false;
          if (data && data.rows) {
            this.productTypes = data.rows;
          }
          console.log('data', data)
        },
        error => {
          this.isLoadingProductTypes = false;
          this.alertService.error(error);
          console.log(error.error)
        }
      )
    }    
  }

  redirectToProductPage(typeName: string) {
    let _link = `${this.baseUrl}/products?type=${this.slugifyPipe.transform(typeName)}`
    window.open(
      _link,
      '_blank'
    );
  }

}
