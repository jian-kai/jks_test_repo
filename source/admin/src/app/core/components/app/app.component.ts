import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';

import { translator } from '../../../i18n';
import {StorageService} from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private http: Http,
    public translate: TranslateService,
    public storage: StorageService,
    public userService: UsersService
  ) {

    // set transatation data
    translate.setTranslation('en', translator.en);
    translate.setTranslation('vn', translator.vn);

    // use english as default language
    translate.setDefaultLang('en');
  }
  ngOnInit() {
   
  } 

}
