import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonAiComponent } from './button-ai.component';

describe('ButtonAiComponent', () => {
  let component: ButtonAiComponent;
  let fixture: ComponentFixture<ButtonAiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonAiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonAiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
