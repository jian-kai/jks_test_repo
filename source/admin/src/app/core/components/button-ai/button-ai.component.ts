import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalService } from '../../../core/services/modal.service';
import { DialogService } from '../../../core/services/dialog.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { LoadingService } from '../../../core/services/loading.service';
import { AlertService } from '../../../core/services/alert.service';
import { RouterService } from '../../../core/helpers/router.service';
@Component({
  selector: 'app-button-ai',
  templateUrl: './button-ai.component.html',
  styleUrls: ['./button-ai.component.scss']
})
export class ButtonAiComponent implements OnInit {
  @Input() active: boolean;
  @Input() apikey: string;
  @Input() apivalue: any;

  // @Output() runGetdata = new EventEmitter();
  constructor(
    private _http: HttpService,
    private loadingService: LoadingService,
    private appConfig: AppConfig,
    private alertService: AlertService,
    private routerService: RouterService,
    public modalService: ModalService, private _dialogService: DialogService
  ) { }

  ngOnInit() {
  }
  changeStatusMutiActives() {
    // nothing
  }
  postUpdateStatusAtiveInactive(productIds: Array<number>, status: any) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        //this.loadingService.display(true);
        let actionids: string = this.apivalue.actionids;
        let actionstatus: string = this.apivalue.actionstatus;

        if (actionstatus == 'isActive') {
          if (status == 'active') {
            status = 'true';
          } else {
            status = 'false';
          }
        }
        let formData: object = { [actionids]: productIds, [actionstatus]: status };
        let apikeyurl = this.appConfig.config.apiURL + '/api/' + this.apikey;
        console.log(formData, apikeyurl);
        $('.actionactive.checkboxall').attr('idchecked', '');
        this._http._create(apikeyurl, formData).subscribe(
          data => {
            if (data.ok) {
              this.active = false;
              this.routerService.reGetdata();
              //this.loadingService.display(false);           
            }
          },
          error => {
            this.loadingService.display(false);
            this.active = false;
            this.alertService.error(error);
          }
        )
      }
    }));
  }
  changeStatusMutiActive(status) {
    let idchecked: any = $('.actionactive.checkboxall').attr('idchecked');
    if (idchecked != undefined && idchecked != '') {
      let spl = idchecked.split(',');
      //console.log(idchecked);
      let productIds: Array<number> = [];
      spl.forEach(e => {
        productIds.push(e);
      });
      //console.log(productIds.length);
      if (productIds.length > 0 && status == 'removed' || status == 'active') {
        this.postUpdateStatusAtiveInactive(productIds, status);
      }
    }
    //$('.actionactive.checkboxall').attr('idchecked', '');
  }
}
