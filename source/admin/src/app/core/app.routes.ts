import { LoggedInGuard } from './guards/logged-in.guard';
import { AuthService } from '../auth/services/auth.service';
import { DashboardComponent } from './../dashboard/dashboard.component';
import { LoginComponent } from './../auth/components/login/login.component';
import { SettingComponent } from './../setting/setting.component';
import { UserComponent } from './../user/user.component';
import { OrderHistoryComponent } from "./../order/order-history/order-history.component";
import { OrderPendingComponent } from "./../order/order-pending/order-pending.component";
import { BulkOrderComponent } from "./../order/bulk-order/bulk-order.component";
import { ResetPasswordComponent } from "../auth/components/reset-password/reset-password.component";
import { OrderDetailsComponent } from './../order/order-details/order-details.component';
import { BulkCreateDetailsComponent } from './../order/bulk-create-details/bulk-create-details.component';
import { OrderUploadComponent } from './../order/order-upload/order-upload.component';
// Country
import { CountryComponent } from './../country/country.component';
import { AddCountryComponent } from './../country/add-country/add-country.component';
import { EditCountryComponent } from './../country/edit-country/edit-country.component';

import { UserCustomerComponent } from './../user-customer/user-customer.component';
import { UserCustomerDetailsComponent } from './../user-customer/user-customer-details/user-customer-details.component';

import { ProductComponent } from './../product/product.component';

// Product Category
import { ProductCategoryComponent } from './../product/product-category/product-category.component';
import { AddProductCategoryComponent } from './../product/product-category/add-product-category/add-product-category.component';
import { EditProductCategoryComponent } from './../product/product-category/edit-product-category/edit-product-category.component';

import { ProductListComponent } from './../product/product-list/product-list.component';
import { AddProductComponent } from './../product/add-product/add-product.component';
import { EditProductComponent } from './../product/edit-product/edit-product.component';

import { SubscriptionComponent } from './../subscription/subscription.component';
import { AddPlanComponent } from './../subscription/plans/add-plan/add-plan.component';
import { EditPlanComponent } from './../subscription/plans/edit-plan/edit-plan.component';
//Plan Groups
import { PlanGroupsComponent } from './../subscription/plan-groups/plan-groups.component';
import { EditPlanGroupComponent } from './../subscription/plan-groups/edit-plan-group/edit-plan-group.component';
import { AddPlanGroupComponent } from './../subscription/plan-groups/add-plan-group/add-plan-group.component';
//Plan Types
import { PlanTypesComponent } from './../subscription/plan-types/plan-types.component';
import { EditPlanTypeComponent } from './../subscription/plan-types/edit-plan-type/edit-plan-type.component';
import { AddPlanTypeComponent } from './../subscription/plan-types/add-plan-type/add-plan-type.component';

import { PlansComponent } from './../subscription/plans/plans.component';

import { ReportComponent } from './../report/report.component';
import { AppcoReportComponent } from './../report/appco-report/appco-report.component';
import { MoReportComponent } from './../report/mo-report/mo-report.component';

import { PromotionComponent } from './../promotion/promotion.component';
import { AddPromotionComponent } from './../promotion/add-promotion/add-promotion.component';
import { EditPromotionComponent } from './../promotion/edit-promotion/edit-promotion.component';
import { SubscribersComponent } from './../subscription/subscribers/subscribers.component';
import { SubscribersFreeTrialComponent } from './../subscription/subscribers/subscribers-free-trial/subscribers-free-trial.component';
import { SubscriberDetailsComponent } from './../subscription/subscribers/subscriber-details/subscriber-details.component';
import { PromotionDetailsComponent } from './../promotion/promotion-details/promotion-details.component';

import { UserSampleProductComponent } from './../user-sample-product/user-sample-product.component';
import { TermComponent } from './../term/term.component';
import { AddTermComponent } from './../term/add-term/add-term.component';
import { EditTermComponent } from './../term/edit-term/edit-term.component';

import { PrivacyComponent } from './../privacy/privacy.component';
import { AddPrivacyComponent } from './../privacy/add-privacy/add-privacy.component';
import { EditPrivacyComponent } from './../privacy/edit-privacy/edit-privacy.component';

import { ArticleComponent } from './../article/article.component';
import { AddArticleComponent } from './../article/add-article/add-article.component';
import { EditArticleComponent } from './../article/edit-article/edit-article.component';

import { ArticleTypeComponent } from './../article/article-type/article-type.component';

import { FaqComponent } from './../faq/faq.component';
import { AddFaqComponent } from './../faq/add-faq/add-faq.component';
import { EditFaqComponent } from './../faq/edit-faq/edit-faq.component';

import { UserRoleComponent } from './../user-role/user-role.component';
import { UserRoleListComponent } from './../user-role/user-role-list/user-role-list.component';
import { AddUserRoleComponent } from './../user-role/add-user-role/add-user-role.component';
import { EditUserRoleComponent } from './../user-role/edit-user-role/edit-user-role.component';
import { PathComponent } from './../permission/path/path.component';
import { ExportComponent } from './../export/export.component';
import { MessageComponent } from './../cancellation/message/message.component';

export const routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']},},
  { path: 'login', component: LoginComponent },
  { path: 'settings', component: SettingComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'settings', component: SettingComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'users/staffs', component: UserComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin']}, },
  { path: 'admin/reset-password', component: ResetPasswordComponent, data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'bulk-orders', component: BulkOrderComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'orders/pending', component: OrderPendingComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'orders/history', component: OrderHistoryComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'orders/:type/:id', component: OrderDetailsComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'orders/upload', component: OrderUploadComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'orders/:id', component: OrderDetailsComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'bulk-orders/:id', component: BulkCreateDetailsComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  
  // country
  { path: 'countries', component: CountryComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'countries/add', component: AddCountryComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'countries/edit/:id', component: EditCountryComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },

  { path: 'users/customers', component: UserCustomerComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'users/customers/details/:id', component: UserCustomerDetailsComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'users/sample-product', component: UserSampleProductComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  {
    path: 'users/roles', component: UserRoleComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']},
    children: [
      { path: '', component: UserRoleListComponent, pathMatch: 'full' },
      { path: 'add', component: AddUserRoleComponent },
      { path: 'edit/:id', component: EditUserRoleComponent }
    ]
  },

  {
    path: 'products', component: ProductComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']},
    children: [
      { path: '', component: ProductListComponent, pathMatch: 'full' },
      { path: 'category', component: ProductCategoryComponent },
      { path: 'category/add', component: AddProductCategoryComponent },
      { path: 'category/edit/:id', component: EditProductCategoryComponent },
      { path: 'add', component: AddProductComponent },
      { path: 'edit/:id', component: EditProductComponent }
    ]
  },
  {
    path: 'subscriptions', component: SubscriptionComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']},
    children: [
      { path: '', component: SubscribersComponent, pathMatch: 'full' },
      { path: 'subscribers', component: SubscribersComponent },
      { path: 'subscribers-free-trial', component: SubscribersFreeTrialComponent },
      { path: 'subscribers/details/:type/:id', component: SubscriberDetailsComponent },
      { path: 'subscribers/details/:type/:id', component: SubscriberDetailsComponent },
      { path: 'plan-groups', component: PlanGroupsComponent },
      { path: 'plan-groups/add', component: AddPlanGroupComponent },
      { path: 'plan-groups/edit/:id', component: EditPlanGroupComponent },
      { path: 'plan-types', component: PlanTypesComponent },
      { path: 'plan-types/add', component: AddPlanTypeComponent },
      { path: 'plan-types/edit/:id', component: EditPlanTypeComponent },
      { path: 'plans', component: PlansComponent },
      { path: 'plans/add', component: AddPlanComponent },
      { path: 'plans/edit/:slug', component: EditPlanComponent },
      { path: 'cancellation', component: MessageComponent }
    ]
  },
  { path: 'reports/master', component: ReportComponent, canActivate: [AuthService] , data: {roles: ['super admin', 'admin', 'staff', 'report']},},
  { path: 'reports/appco', component: AppcoReportComponent, canActivate: [AuthService] , data: {roles: ['super admin', 'admin', 'staff', 'report']},},
  { path: 'reports/mo', component: MoReportComponent, canActivate: [AuthService] , data: {roles: ['super admin', 'admin', 'staff', 'report']},},
  { path: 'promotions', component: PromotionComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'promotions/details/:id', component: PromotionDetailsComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'promotions/add', component: AddPromotionComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  { path: 'promotions/edit/:id', component: EditPromotionComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff']}, },
  
  // FAQ
  { path: 'utils/faq', component: FaqComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/faq/add', component: AddFaqComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/faq/edit/:id', component: EditFaqComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  // Term
  { path: 'utils/terms', component: TermComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/terms/add', component: AddTermComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/terms/edit/:id', component: EditTermComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  // Privacy
  { path: 'utils/privacy', component: PrivacyComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/privacy/add', component: AddPrivacyComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'utils/privacy/edit/:id', component: EditPrivacyComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  // Articles
  { path: 'articles', component: ArticleComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'articles/add', component: AddArticleComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  { path: 'articles/edit/:id', component: EditArticleComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  // Article Types
  { path: 'articles/types', component: ArticleTypeComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },
  // Path
  { path: 'permission/paths', component: PathComponent, canActivate: [AuthService], data: {roles: ['super admin']}, },

  //Export
  { path: 'exports', component: ExportComponent, canActivate: [AuthService], data: {roles: ['super admin', 'admin', 'staff', 'report']}, },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }

];
