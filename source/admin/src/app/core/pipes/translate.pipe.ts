import { TranslateService } from '@ngx-translate/core';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translatePipe',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslateService) {}

  transform(arrayTranslate: any, args?: any): any {
    this.translate.currentLang = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    let translate = arrayTranslate.find(value => value.langCode.toUpperCase() === this.translate.currentLang);
    translate = translate || arrayTranslate.find(value => value.isDefault === true);
    if (!translate) {
      translate = arrayTranslate[0];
    }
    return translate ? translate[args] : '';
  }

}