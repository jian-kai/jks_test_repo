import { Pipe, PipeTransform } from '@angular/core';

const countries = {
  'MYS': 'MY',
  'SGP': 'SG',
  'KOR': 'KR'
}

const size = 9;
const bulkSize = 8;

@Pipe({
  name: 'orderNo'
})
export class OrderNoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let tpm = value + "";
    if(!args.isBulk) {
      while (tpm.length < size) tpm = "0" + tpm;
      let countryCode = countries[args.countryCode] || args.countryCode;
      return `#${countryCode}${tpm}-${args.createdAt}`;
    } else {
      while (tpm.length < bulkSize) tpm = "0" + tpm;
      let countryCode = countries[args.countryCode] || args.countryCode;
      return `#${countryCode}B${tpm}-${args.createdAt}`;
    }
  }

}
