import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonPipe'
})
export class JsonPipePipe implements PipeTransform {

  transform(_json: any, key?: any): any {
    _json = JSON.parse(_json);

    return _json[key];
  }

}
