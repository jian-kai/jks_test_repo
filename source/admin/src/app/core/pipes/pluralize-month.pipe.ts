import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pluralizeMonth'
})
export class PluralizeMonthPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value > 1) {
      return value + ' months';
    }
    return value + ' month';
  }

}
