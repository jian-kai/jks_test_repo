import { Pipe, PipeTransform } from '@angular/core';
import { StorageService } from '../services/storage.service';
@Pipe({
  name: 'smsCurrency',
  pure: false
})
export class SmsCurrencyPipe implements PipeTransform {
  constructor(private storage: StorageService) { }
  transform(value: any, args?: any): any {
    let country = this.storage.getCountry();
    let lang = this.storage.getLanguage();
    if (country.code.toUpperCase() === 'KOR' && lang.toUpperCase() === 'KO') {
      value = typeof value === 'number' ? value.toFixed(2) : value;
      return `${value}원`;
    }
    else {
      let country = this.storage.getCountry();
      value = typeof value === 'number' ? value.toFixed(2) : value;
      return `${country.currencyDisplay}${value}`;
    }
  }

}
