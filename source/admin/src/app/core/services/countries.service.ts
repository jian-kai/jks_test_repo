import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { StorageService } from './storage.service';

@Injectable()
export class CountriesService {
  @Output() country: EventEmitter<Object> = new EventEmitter();
  countries: Array<any>;

  constructor(
    private http: Http,
    private storageService: StorageService,
    private builder: FormBuilder,
  ) { }

  changeCountry(country) {
    // store country into local store
    this.storageService.setCountry(country);
    // emit country change
    this.country.emit(country);
  }

  setCountries(countries) {
    this.countries = countries;
  }

  getCountries() {
    return this.countries;
  }

  // initialize State
  initState() {
    return this.builder.group({
      // id: [''],
      name: ['', Validators.required],
      isDefault: [''],
    });
  }

  // initialize Language
  initLanguage() {
    return this.builder.group({
      languageCode: ['', Validators.required],
      languageName: ['', Validators.required],
    });
  }
}
