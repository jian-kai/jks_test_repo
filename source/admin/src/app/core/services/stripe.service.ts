import { Injectable } from '@angular/core';
import { AppConfig } from '../../config/app.config';

@Injectable()
export class StripeService {
  constructor(private appConfig: AppConfig) {}
  private currentPubkey = this.appConfig.getConfig('stripePubkey');
  private stripe = window['Stripe'];
  
  getInstance() {
    this.stripe.setPublishableKey(this.appConfig.getConfig('stripePubkey'));
    return this.stripe;
  }

}
