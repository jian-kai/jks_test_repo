import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FileValidator } from '../../core/directives/required-file.directive';
import { numberValidator } from '../../core/directives/number-validator.directive';

@Injectable()
export class PlanService {
  constructor(private builder: FormBuilder,) { }

  // initialize Plan Translate
  initPlanTranslate() {
    return this.builder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      shortDescription: ['', Validators.required],
      description: ['', Validators.required],
      subsequentDescription: ['', Validators.required],
      langCode: ['', Validators.required],
    });
  }

  // initialize Plan Country
  initPlanCountry() {
    return this.builder.group({
      id: [''],
      CountryId: [''],
      CountryName: [''],
      currencyDisplay: [''],
      actualPrice: ['', Validators.required],
      sellPrice: ['', Validators.required],
      planDetails: this.builder.array([], Validators.compose([Validators.required])),
      planTrialProducts: this.builder.array([]),
      planOptions: this.builder.array([]),
      pricePerCharge: ['', Validators.required],
      savePercent: [''],
      isOnline: [''],
      isOffline: [''],
      order: [0, Validators.required]
    });
  }

  // initialize Plan Details
  initPlanDetails() {
    return this.builder.group({
      id: [null],
      product: [null, Validators.required],
      deliverPlan: this.builder.array([])
    });
  }

  initPlanTrialProducts() {
    return this.builder.group({
      id: [null],
      product: [null, Validators.required],
      qty: [null, Validators.required],
    });
  }

  initPlanOptions() {
    return this.builder.group({
      id: [''],
      // description: ['', Validators.required],
      planOptionTranslate: this.builder.array([]),
      actualPrice: ['', Validators.required],
      sellPrice: ['', Validators.required],
      pricePerCharge: ['', Validators.required],
      savePercent: [''],
      url: ['', Validators.required],
      planOptionProducts: this.builder.array([]),
      hasShaveCream: [''],
    });
  }

  initPlanOptionProducts() {
    return this.builder.group({
      id: [null],
      qty: [null, Validators.required],
      product: [null, Validators.required],
    });
  }

  // init deliverPlan
  initDeliverPlan() {
    return this.builder.group({
      times: ['', Validators.required],
      qty: ['', numberValidator()],
    });
  }

  // initialize product type translate
  initPlanTypeTranslate() {
    return this.builder.group({
      // id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      langCode: ['', Validators.required],
    });
  }

  // initialize plan group translate
  initPlanGroupTranslate() {
    return this.builder.group({
      // id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      langCode: ['', Validators.required],
    });
  }

  // initialize plan option translate
  initPlanOptionTranslate() {
    return this.builder.group({
      id: [null],
      name: ['', Validators.required],
      langCode: ['', Validators.required],
    });
  }
}