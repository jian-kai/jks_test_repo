import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PagerService {
  public getPager = new BehaviorSubject<number>(null);
  private keepAfterRouteChange = false;
  constructor(private router: Router) {
    // clear pager on route change unless 'keepAfterRouteChange' flag is true
    router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          // clear alert messages
          this.clear();
        }
    });
  }

  public setPage(page: number) {
    if (page)
      this.getPager.next(page);
  }

  public clear() {
    this.getPager.next(null);
  }
}
