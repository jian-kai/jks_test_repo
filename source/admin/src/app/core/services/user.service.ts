import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FileValidator } from '../../core/directives/required-file.directive';


@Injectable()
export class UserService {

  constructor(private builder: FormBuilder,) { }

  // initialize Role Detail
  initRoleDetails() {
    return this.builder.group({
      canView: [''],
      canCreate: [''],
      canEdit: [''],
      canUpload: [''],
      canDownload: [''],
      canDelete: [''],
      PathId: [''],
      PathIdTmp: [''],
      pathValue: ['']
    });
  }

  checkAllPermissions(permissionsForm : any, value: boolean) {
    let _value: boolean = value ?  value : false;
    permissionsForm.controls.canView.setValue(value);
    permissionsForm.controls.canCreate.setValue(value);
    permissionsForm.controls.canEdit.setValue(value);
    permissionsForm.controls.canUpload.setValue(value);
    permissionsForm.controls.canDownload.setValue(value);
    permissionsForm.controls.canDelete.setValue(value);
  }

}
