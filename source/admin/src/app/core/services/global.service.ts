import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../../core/services/storage.service';
import { AppConfig } from 'app/config/app.config';
import { HttpService } from '../../core/services/http.service';
import { AlertService } from '../../core/services/alert.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class GlobalService {
	public userInfo: any;
;
	constructor(private translate: TranslateService,
		private storageService: StorageService,
		private appConfig: AppConfig,
		private _http: HttpService,
		private alertService: AlertService
	) {
		this.userInfo = this.storageService.getCurrentUserItem();
	}
	_URLSearchParams(options) {
		let params = new URLSearchParams();
		for (let key in options) {
			params.set(key, decodeURIComponent(options[key]))
		}
		return params;
	}

	_fillterProductLanguage(arr, language) {
		let index = 0;
		for (var j = 0; j < arr.length; j++) {
			if (arr[j].langCode === language) {
				index = j;
			}
		}
		return index;
	}

	public copyArray(content) {
		let arr = [];
		content.forEach((x) => {
			arr.push(Object.assign({}, x));
		})
		arr.map((x) => { x.status = 'default' });
		return content.concat(arr);
	}

	public getCurrentUser(): Observable<any> {
		// this.storageService
		return;
	}

	// merge data
	public mergeObject(output: any, input: any, ): any {
		return Object.assign(output, input);
	}

	//  Set Value for FormBuilder Control
	setValueFormBuilder(_formBuilder, _objectValue) {
		Object.keys(_objectValue).forEach(name => {
			if (_formBuilder.controls[name]) {
				_formBuilder.controls[name].setValue(_objectValue[name]);
			}
		});
		return _formBuilder;
	}

	// is super admin role
	isSupperAdmin() {
		let currentUser = this.storageService.getCurrentUserItem();
		return (currentUser.Role && currentUser.Role.roleName.indexOf('admin') > -1);
		// return this.checkRole(`${this.appConfig.config.roles.super}`);
	}

	// is admin role
	isAdmin() {
		return this.checkRole(`${this.appConfig.config.roles.admin}`);
	}

	// is staff role
	isStaff() {
		return this.checkRole(`${this.appConfig.config.roles.staff}`);
	}

	isReport() {
		return this.checkRole(`${this.appConfig.config.roles.report}`);
	}

	checkRole(roleName: string) {
		let currentUser = this.storageService.getCurrentUserItem();
		if (!currentUser || !currentUser.id) {
			console.log('User is not exist');
			return;
		}
		if (currentUser.Role && currentUser.Role.roleName === roleName) {
			return true;
		}
		return;
	}

	isAuthenticated() {
		let currentUser = this.storageService.getCurrentUserItem();
		if (!currentUser || !currentUser.id) {
			console.log('User is not exist');
			return;
		}
	}

	hasDenied() {
		// if (this.isReport()) {
		// 	return true;
		// }
		return false;
	}

	// get country list
	getCountryList() {
		return new Promise((resolve, reject) => {
			let countries = [];
			return this._http._getList(`${this.appConfig.config.api.countries_list}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					reject(error);
					console.log(error.error)
				}
			)
		});
	}

	// get role list
	getRoleList() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.roles}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error)
				}
			)
		});
	}

	// get Marketing Offices list
	getMarketingOfficesList() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.marketing_offices}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error)
				}
			)
		});
	}

	// get order states
	getOrderStates() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.order_states}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error)
				})
		});
	}

	// check type of order
	checkTypeOfOrder(state: string) {
		let args_pending = `${this.appConfig.config.order_status.pending}`;
		let args_history = `${this.appConfig.config.order_status.history}`;
		if (args_pending.indexOf(state) > -1) {
			return "pending";
		}
		if (args_history.indexOf(state) > -1) {
			return "history";
		}
		return "";
	}

	// get Directory countries
	getDirectoryCountries() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.directory_countries}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error);
				}
			)
		});
	}

	// get user list by current user token
	getUserList() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.users}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error);
				}
			)
		});
	}

	// get product type list
	getProductTypes() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.admin_product_types}`).subscribe(
				data => {
					resolve(data.rows);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error);
				}
			)
		})
	}

	// get plan type list
	getPlanTypes() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.plan_types}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error);
				}
			)
		})
	}

	// get plan groups
	getPlanGroups() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.plan_groups}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					this.alertService.error(error);
					resolve(error);
					console.log(error.error);
				}
			)
		})
	}

	objectIsEmpty(obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key))
				return false;
		}
		return true;
	}

	deliverTimesArr(deliverTimes: number) {
		let deliverTimesArr: any = [];
		for (var index = 1; index <= deliverTimes; index++) {
			deliverTimesArr.push(index);
		}
		return deliverTimesArr;
	}

	// get next deliver date
	getNextDeliverDate(_startDate: any, _totalDeliverTimes: number, _deliverTime: number) {
		let _month: number = (12 / _totalDeliverTimes) * (_deliverTime - 1);
		let startDate = new Date(_startDate);
		startDate.setMonth(startDate.getMonth() + _month);
		return startDate;
	}

	// get next deliver date for Free Trial
	getNextDeliverDateFreeTrial(_startDate: any, _subsequentDeliverDuration: any, _deliverTime: number) {
		let _month: number = +_subsequentDeliverDuration.split(' ')[0] * _deliverTime;
		let startDate = new Date(_startDate);
		startDate.setMonth(startDate.getMonth() + _month);
		return startDate;
	}

	// get mailChimp List
	mailChimpList() {
		return new Promise((resolve, reject) => {
			this._http._getList(`${this.appConfig.config.api.promotions}/email-group`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					resolve(error);
					console.log(error.error)
				})
		});
	}

	// reindex array
	reindexingArray(_arr: any) {
		let __arr = [];
		_arr.forEach(element => {
			__arr.push(element);
		});
		return __arr;
	}

	// re-format date to param
	formatDate(_date: Date) {
		let year = _date.getFullYear().toString();
		let month = (_date.getMonth() + 1).toString();
		let day = _date.getDate().toString();

		if (month.length === 1) {
			month = '0' + month;
		}
		if (day.length === 1) {
			day = '0' + day;
		}

		return year + '-' + month + '-' + day;
	}

	/**
	 * Get permission of the path
	 * @param {*} path 
	 * @return: {}
	 */
	getPermissionOfPath(path: string) {
		let _permission_of_path: any = {};
		let _userInfo = this.storageService.getCurrentUserItem();
		if (_userInfo && _userInfo.Role && _userInfo.Role.roleDetails && _userInfo.Role.roleDetails.length > 0) {
			_permission_of_path = _userInfo.Role.roleDetails.filter(per => per.Path.pathValue === path);
			if (_permission_of_path && _permission_of_path[0]) {
				_permission_of_path = _permission_of_path[0];
			}
		}
		return _permission_of_path;
	}

	/**
	 * Check if allow get access to view
	 * @param {*} path 
	 * @return: boolean
	 */
	canView(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canView) {
			return true;
		}
		return false;
	}

	/**
	 * Check if allow get access to create
	 * @param {*} path 
	 * @return: boolean
	 */
	canCreate(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canCreate) {
			return true;
		}
		return false;
	}

	/**
	 * Check if allow get access to Edit
	 * @param {*} path 
	 * @return: boolean
	 */
	canEdit(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canEdit) {
			return true;
		}
		return false;
	}

	/**
	 * Check if allow get access to Delete
	 * @param {*} path 
	 * @return: boolean
	 */
	canDelete(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canDelete) {
			return true;
		}
		return false;
	}

	/**
	 * Check if allow get access to Upload
	 * @param {*} path 
	 * @return: boolean
	 */
	canUpload(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canUpload) {
			return true;
		}
		return false;
	}

	/**
	 * Check if allow get access to Download
	 * @param {*} path 
	 * @return: boolean
	 */
	canDownload(path: string) {
		let _permission_of_path = this.getPermissionOfPath(path);
		if (_permission_of_path.canDownload) {
			return true;
		} else {
			return false;
		}

	}

	htmlToPlaintext(text) {
		return text ? String(text).replace(/<[^>]+>/gm, '') : '';
	}

	getOrderIdFromOrderNumber(orderNumber) {
		let pattern = /^([A-Z]{2})([0-9]{9})(\-[0-9]{8})?$/i;

		let realOrderId = orderNumber.replace(pattern, '$2');
	  if (realOrderId !== orderNumber) {
	    // order number format matched -> search by order id
	    return realOrderId;
	  }

	  return '';
	}

	getBulkOrderIdFromOrderNumber(orderNumber) {
		let pattern = /^([A-Z]{2}B)([0-9]{8})(\-[0-9]{8})?$/i;

		let realOrderId = orderNumber.replace(pattern, '$2');
	  if (realOrderId !== orderNumber) {
	    // order number format matched -> search by order id
	    return realOrderId;
	  }

	  return '';
	}

	makeTableHorizontalScrollable() {
		let $_scrollerWrapper = $('.table-scroller-wrapper');
		let $_topScroller = $_scrollerWrapper.children('.top-scroller');
		let $_botScroller = $_scrollerWrapper.children('.bot-scroller');
		let tableWidth = $_botScroller.children('table').outerWidth();

		$_scrollerWrapper.find('.scroller-content').css('width', tableWidth + 'px');

		$_topScroller.off().unbind();
		$_topScroller.scroll(function(e) {
			$_botScroller.scrollLeft($(this).scrollLeft());
		});

		$_botScroller.off().unbind();
		$_botScroller.scroll(function(e) {
			$_topScroller.scrollLeft($(this).scrollLeft());
		});
	}

	fixDropdownTable() {
		let $_scrollerWrapper = $('.table-scroller-wrapper');
		let dropdownMenu;
		$_scrollerWrapper.on('show.bs.dropdown', function (e) {
			dropdownMenu = $(e.target).find('.dropdown-menu');
    	$('body').append(dropdownMenu.detach());
    	var eOffset = $(e.target).offset();
    	dropdownMenu.css({
        'display': 'block',
        'top': eOffset.top + $(e.target).outerHeight(),
        'left': eOffset.left,
				'width':'184px',
    	});
			dropdownMenu.addClass("mobPosDropdown");
		});
	 
		$_scrollerWrapper .on('hide.bs.dropdown', function (e) {
			$(e.target).append(dropdownMenu.detach());
    	dropdownMenu.hide();
		})
	}
	
	addScrollTable() {
		var SETTINGS = {
			navBarTravelling: false,
			navBarTravelDirection: "",
			navBarTravelDistance: 150
		}
		var scrollBlock = document.getElementById("scroll-block");
		var longTable = document.getElementById("long-table");

		scrollBlock.setAttribute("data-overflowing", determineOverflow(longTable, scrollBlock));

		var last_known_scroll_position = 0;
		var ticking = false;
		var btnPrev = document.getElementById("btn-prev");
		var btnNext = document.getElementById("btn-next");

		function doSomething(scroll_pos) {
			scrollBlock.setAttribute("data-overflowing", determineOverflow(longTable, scrollBlock));
		}
	
		scrollBlock.addEventListener("scroll", function() {
				last_known_scroll_position = window.scrollY;
				if (!ticking) {
						window.requestAnimationFrame(function() {
								doSomething(last_known_scroll_position);
								ticking = false;
						});
				}
				ticking = true;
		});

		btnPrev.addEventListener("click", function() {
			// If in the middle of a move return
			if (SETTINGS.navBarTravelling === true) {
					return;
			}
			// If we have content overflowing both sides or on the left
			if (determineOverflow(longTable, scrollBlock) === "left" || determineOverflow(longTable, scrollBlock) === "both") {
					// Find how far this panel has been scrolled
					var availableScrollLeft = scrollBlock.scrollLeft;
					// If the space available is less than two lots of our desired distance, just move the whole amount
					// otherwise, move by the amount in the settings
					if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
							longTable.style.transform = "translateX(" + availableScrollLeft + "px)";
					} else {
							longTable.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
					}
					// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
					longTable.classList.remove("none-transition");
					// Update our settings
					SETTINGS.navBarTravelDirection = "left";
					SETTINGS.navBarTravelling = true;
			}
			// Now update the attribute in the DOM
			scrollBlock.setAttribute("data-overflowing", determineOverflow(longTable, scrollBlock));
		});
	
		btnNext.addEventListener("click", function() {
				// If in the middle of a move return
				if (SETTINGS.navBarTravelling === true) {
						return;
				}
				// If we have content overflowing both sides or on the right
				if (determineOverflow(longTable, scrollBlock) === "right" || determineOverflow(longTable, scrollBlock) === "both") {
						// Get the right edge of the container and content
						var navBarRightEdge = longTable.getBoundingClientRect().right;
						var navBarScrollerRightEdge = scrollBlock.getBoundingClientRect().right;
						// Now we know how much space we have available to scroll
						var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
						// If the space available is less than two lots of our desired distance, just move the whole amount
						// otherwise, move by the amount in the settings
						if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
								longTable.style.transform = "translateX(-" + availableScrollRight + "px)";
						} else {
								longTable.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
						}
						// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
						longTable.classList.remove("none-transition");
						// Update our settings
						SETTINGS.navBarTravelDirection = "right";
						SETTINGS.navBarTravelling = true;
				}
				// Now update the attribute in the DOM
				scrollBlock.setAttribute("data-overflowing", determineOverflow(longTable, scrollBlock));
		});
	
		longTable.addEventListener(
				"transitionend",
				function() {
						// get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
						var styleOfTransform = window.getComputedStyle(longTable, null);
						var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
						// If there is no transition we want to default to 0 and not null
						var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
						longTable.style.transform = "none";
						longTable.classList.add("none-transition");
						// Now lets set the scroll position
						if (SETTINGS.navBarTravelDirection === "left") {
								scrollBlock.scrollLeft = scrollBlock.scrollLeft - amount;
						} else {
								scrollBlock.scrollLeft = scrollBlock.scrollLeft + amount;
						}
						SETTINGS.navBarTravelling = false;
				},
				false
		);

		function determineOverflow(content, container) {
			var containerMetrics = container.getBoundingClientRect();
			var containerMetricsRight = Math.floor(containerMetrics.right);
			var containerMetricsLeft = Math.floor(containerMetrics.left);
			var contentMetrics = content.getBoundingClientRect();
			var contentMetricsRight = Math.floor(contentMetrics.right);
			var contentMetricsLeft = Math.floor(contentMetrics.left);
			if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
					return "both";
			} else if (contentMetricsLeft < containerMetricsLeft) {
					return "left";
			} else if (contentMetricsRight > containerMetricsRight) {
					return "right";
			} else {
					return "none";
			}
		}


	}

	dragMouse() {
		let $_scrollerWrapper = $('.table-scroller-wrapper');
		let $_botScroller = $_scrollerWrapper.children('.bot-scroller');
		
		let mousePosition = null;
    let scrollPosition = null;
    let scroll = null;

		$_botScroller.mousedown(function(m){
			mousePosition = m.pageX;
			scrollPosition = $_botScroller.scrollLeft();
			scroll = true;
		});

		$_botScroller.mousemove(function(m) {
			if (scroll === true) {
				m.preventDefault();
				if (m.pageX  > mousePosition) {
						$_botScroller.scrollLeft(scrollPosition - (m.pageX - mousePosition));
				} else if (m.pageX < mousePosition) {
						$_botScroller.scrollLeft(scrollPosition + (mousePosition - m.pageX));
				}
			}
		});
		
		$_botScroller.mouseup(function(){
			scroll = false;
		});
	}
}