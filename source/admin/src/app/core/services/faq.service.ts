import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FileValidator } from '../../core/directives/required-file.directive';
import { numberValidator } from '../../core/directives/number-validator.directive';

// Services
import { HttpService } from '../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';

@Injectable()
export class FaqService {

  constructor(private builder: FormBuilder,
    private _http: HttpService,
    private appConfig: AppConfig,
  ) { }

  // initialize Plan Country
  initFaq() {
    return this.builder.group({
      id: [''],
      type: ['', Validators.required],
      CountryId: ['', Validators.required],
      langCode: ['', Validators.required],
      isSubscription: [''],
      isAlaCarte: [''],
      faqTranslate: this.builder.array([], Validators.compose([Validators.required])),
    });
  }

  // initialize Plan Translate
  initFaqTranslate() {
    return this.builder.group({
      // id: [''],
      question: ['', Validators.required],
      answer: ['', Validators.required],
    });
  }

  getFaqTypes() {
    return new Promise ((resolve, reject) => {
      this._http._getList(`${this.appConfig.config.api.faqs_types}`).subscribe(
				data => {
					resolve(data);
				},
				error => {
					resolve(error);
					console.log(error.error)
				})
    });
  }

}
