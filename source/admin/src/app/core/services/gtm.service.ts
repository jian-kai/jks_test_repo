import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class GTMService {
  dataLayer ;
  constructor(public storage: StorageService) {
    this.dataLayer = window['dataLayer'];
   }

  reverseTransactionData(orderInfo) {
    console.log('orderInfo',orderInfo);
    let products = [];
    orderInfo.orderDetail.forEach(item => {
      if(item.PlanCountry) {
        products.push({
          'name': item.PlanCountry.Plan.sku,     // Name or ID is required.
          'id': item.PlanCountry.id,
          'price': item.PlanCountry.sellPrice,
          'brand': 'Google',
          'category': item.PlanCountry.Plan.PlanType ? item.PlanCountry.Plan.PlanType.name : '',
          'quantity': (item.qty ? item.qty : 0)
        });
      }
      if(item.ProductCountry) {
        products.push({
          'name': item.ProductCountry.Product.sku,     // Name or ID is required.
          'id': item.ProductCountry.id,
          'price': item.ProductCountry.sellPrice,
          'brand': 'Google',
          'category': item.ProductCountry.Product.ProductType ? item.ProductCountry.Product.ProductType.name : '',
          'quantity': (item.qty ? item.qty : 0)
        })
      }
    })
    
    console.log('products',products);
    this.dataLayer.push({
      'event': 'transactiondata',
      'ecommerce': {
        'currencyCode': orderInfo.country ? orderInfo.country.currencyCode : 'MYR',
        'purchase': {
          'actionField': {
            'id': orderInfo.id,                         // Transaction ID. Required for purchases and refunds.
            'affiliation': 'Online Store',
            'revenue': -orderInfo.Receipt.totalPrice,                     // Total transaction value (incl. tax and shipping)
            'tax': -orderInfo.Receipt.taxAmount,
            'shipping': -orderInfo.Receipt.shippingFee,
            'coupon': orderInfo.promoCode
          },
          products
        }
      }
    });

  }
}
