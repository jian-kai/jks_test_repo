import { Injectable } from '@angular/core';
import { MdDialog } from '@angular/material';
import { DialogConfirmComponent } from '../directives/dialog-confirm/dialog-confirm.component';
import { DialogResultComponent } from '../directives/dialog-result/dialog-result.component';

@Injectable()
export class DialogService {

  constructor(public dialog: MdDialog) { }
  confirm(_content ?: string) {
    return this.dialog.open(DialogConfirmComponent, {
      data: {
        content: _content
      }
    });
  }
  result(_content ?: string) {
    return this.dialog.open(DialogResultComponent, {
      data: {
        content: _content
      }
    });
  }
}
