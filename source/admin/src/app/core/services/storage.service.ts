const STORAGE_KEY = 'admin_auth_token';
const CURRENT_USER = 'admin_current_user';
const CART_ITEMS = 'cart_items';
const PLAN_CART = 'plan_cart';
const CURRENT_COUNTRY = 'current_country';
const CURRENT_LANGUAGE = 'current_language';
const DEFAULT_COUNTRY = {
  code: 'MYS',
  name: 'Malaysia',
  currencyDisplay: 'MR',
  isDefault: true
}
const DEFAULT_LANGUAGE = {
  key: 'en',
  value: 'Malaysia',
  countryCode: 'MYS',
  isDefault: true
}

import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export class StorageService {
  getAuthToken() {
    return localStorage.getItem(STORAGE_KEY);
  }

  setAuthToken(token) {
    localStorage.setItem(STORAGE_KEY, token);
  }

  removeAuthToken() {
    localStorage.removeItem(STORAGE_KEY);
  }
  
  //set current User localstorage
  setCurrentUser(data) {
    localStorage.setItem(CURRENT_USER, JSON.stringify(data));
  }
  // remove current User localstorage
  removeCurrentUser() {
    localStorage.removeItem(CURRENT_USER);
  }
  // get localstorage item
  getCurrentUserItem(): any {
      return JSON.parse(localStorage.getItem(CURRENT_USER));
  }
  // set cart item localstorage
  setCartItem(item) {
    localStorage.setItem(CART_ITEMS, JSON.stringify(item));
  }
  // get cart item localstorage
  getCartItem(): any {
    return JSON.parse(localStorage.getItem(CART_ITEMS));
  }

  // clear cart
  clearCart() {
    localStorage.removeItem(CART_ITEMS);
  }

  // set plan item
  setPlanCart(item) {
    this.clearPlan();
    localStorage.setItem(PLAN_CART, JSON.stringify(item));
  }

  // get plan item 
  getPlanCart() : any {
    return JSON.parse(localStorage.getItem(PLAN_CART));
  }

  // get plan cart items
  getPlanCartItems() {
    let planCart = this.getPlanCart();
    return planCart.carts ? planCart.carts : [];
  }

  // set cart item localstorage
  setPlanCartItem(items) {
    let planCart = this.getPlanCart();
    planCart.carts = items;
    localStorage.setItem(PLAN_CART, JSON.stringify(planCart));
  }

  // clear plan item
  clearPlan() {
    localStorage.removeItem(PLAN_CART);
  }

  // set country code
  setCountry(country) {
    localStorage.setItem(CURRENT_COUNTRY, JSON.stringify(country));
  }

  // get country code
  getCountry() {
    if(localStorage.getItem(CURRENT_COUNTRY)) {
      return JSON.parse(localStorage.getItem(CURRENT_COUNTRY));
    } else {
      return DEFAULT_COUNTRY;
    }
  }

  // set language
  setLanguage(language) {
    localStorage.setItem(CURRENT_LANGUAGE, JSON.stringify(language));
  }

  // get language
  getLanguage() {
    if(localStorage.getItem(CURRENT_LANGUAGE)) {
      return JSON.parse(localStorage.getItem(CURRENT_LANGUAGE));
    } else {
      return DEFAULT_LANGUAGE;
    }
  }
}
