import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FileValidator } from '../../core/directives/required-file.directive';
import { numberValidator } from '../../core/directives/number-validator.directive';

@Injectable()
export class PromotionService {
constructor(private builder: FormBuilder,) { }

  // initialize Plan Translate
  initPromotionTranslate() {
    return this.builder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      description: ['', Validators.required],
      langCode: ['', Validators.required],
    });
  }

  // initialize Plan Country
  initPromotionCountry() {
    return this.builder.group({
      id: [''],
      CountryId: [''],
      CountryName: [''],
      currencyDisplay: [''],
      actualPrice: ['', Validators.required],
      sellPrice: ['', Validators.required],
      // promotionDetails: this.builder.group({
        // products: this.builder.array([]),
        // deliverPromotion: this.builder.array([])
      // })
      promotionDetails: this.builder.array([], Validators.compose([Validators.required])),
    });
  }

  // initialize Plan Details
  initPlanDetails() {
    return this.builder.group({
      plan: [null],
    });
  }

  // initialize Product Details
  initProductDetails() {
    return this.builder.group({
      product: [null],
    });
  }

  // initialize Product Details
  initUserDetails() {
    return this.builder.group({
      user: [null, Validators.required],
    });
  }

  // init deliverPromotion
  initDeliverPromotion() {
    return this.builder.group({
      times: ['', Validators.required],
      qty: ['', numberValidator()],
    });
  }
}
