import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FileValidator } from '../../core/directives/required-file.directive';

@Injectable()
export class ProductService {
  constructor(private builder: FormBuilder,) { }

  // initialize Product Translate
  initProductTranslate() {
    return this.builder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      seoTitle: [''],
      shortDescription: ['', Validators.required],
      description: ['', Validators.required],
      langCode: ['', Validators.required],
      productDetails: this.builder.array([]),
    });
  }
  
  // initialize Product Relateds
  initProductRelateds() {
    return this.builder.group({
      id: [null],
      product: [null, Validators.required],
    });
  }

  // initialize Product Country
  initProductCountry() {
    return this.builder.group({
      id: [''],
      CountryId: [''],
      CountryName: [''],
      currencyDisplay: [''],
      basedPrice: ['', Validators.required],
      sellPrice: ['', Validators.required],
      qty: ['', Validators.required],
      maxQty: ['', Validators.required],
      isOnline: [''],
      isOffline: [''],
      order: [0, Validators.required]
    });
  }

  // initialize Product Details
  initProductDetails() {
    // return this.builder.group({
    //   id: [''],
    //   type: ['', Validators.required],
    //   title: ['', Validators.required],
    //   details: ['', Validators.required],
    //   langCode: ['', Validators.required],
    //   imageUrl: ['', [FileValidator.validate]],
    // });

    return this.builder.group({
      id: [''],
      type: [''],
      title: [''],
      details: [''],
      langCode: [''],
      imageUrl: [''],
    });
  }

  // initialize product type translate
  initProductTypeTranslate() {
    return this.builder.group({
      // id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      langCode: ['', Validators.required],
    });
  }

}
