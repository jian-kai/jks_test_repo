import * as _ from 'underscore';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class ModalService {
  private modals: any[] = [];
 
  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    let modalToRemove = _.findWhere(this.modals, { id: id });
    this.modals = _.without(this.modals, modalToRemove);
  }

  open(id: string, options?: any) {
    // open modal specified by id
    let modal = _.findWhere(this.modals, { id: id });
    if (modal) {
      console.log(`modals service111::: ${id}`);
      // call init component
      console.log('model component init---- ');
      if(modal.onInit && (typeof modal.onInit === 'function')) {
        modal.onInit(options);
      }
      if(options) {
        modal.setInitData(options);
      }
      modal.open(); 
    } else {
      console.log(`modals service222::: ${id}`);
    }
  }

  close(id: string) {
    // close modal specified by id
    let modal = _.find(this.modals, { id: id });
    modal.close();
  }

  reset(id: string) {
    let modal = _.findWhere(this.modals, { id: id });
    let form = modal.el.nativeElement.querySelector("form");
    form.insertAdjacentHTML('beforeend', '<input type="reset" style="width: 0;height: 0;opacity: 0;display: none;visibility: hidden;">');
    form.querySelector("[type='reset']").click();
    form.querySelector("[type='reset']").remove();
  }
}
