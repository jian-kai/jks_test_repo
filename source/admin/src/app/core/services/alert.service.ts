import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { UsersService } from 'app/auth/services/users.service';
import { LoadingService } from 'app/core/services/loading.service';

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  constructor(private usersService: UsersService,
    private loadingService: LoadingService
  ) { }

  success(msg: any) {
    this.subject.next({ type: 'success', texts: msg });
  }

  error(msg: any) {
    if (msg && msg.status && msg.status === 401 ) {
      this.loadingService.display(false);
      this.usersService.logout();
    } else {
      let _msg = [];
      try {
        console.log('message -- ', JSON.parse(msg).message);
        _msg.push(JSON.parse(msg).message);
      } catch (e) {
          return false;
      }  
      // msg.forEach((element, index) => {
      //   try {
      //       msg[index] = JSON.parse(element).message;
      //   } catch (e) {
      //       return false;
      //   }        
      // });
      this.subject.next({ type: 'error', texts: _msg });
    }
    
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  clearMessage() {
    this.error([]);
    this.success([]);
  }

}
