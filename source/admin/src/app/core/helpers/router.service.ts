import { Injectable, Output, EventEmitter } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Router, NavigationStart } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class RouterService {
  @Output() getFooterFull: EventEmitter<Object> = new EventEmitter();
  @Output() getHeaderCommon: EventEmitter<Object> = new EventEmitter();
  @Output() getFooterAssuranceSection: EventEmitter<Object> = new EventEmitter();
  @Output() runGetdata = new EventEmitter();
  @Output() runPagination = new EventEmitter();
  constructor(private router: Router,
    private location: Location) {
      router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.getFooterFull.emit(true);
          this.getHeaderCommon.emit(true);
          this.getFooterAssuranceSection.emit(true);
        }
      });
    }

  appendRouterParams(options) {
    // get current params
    let root = this.location.path(false).split('?')[0];
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    Object.keys(options).forEach(key => {
      params.set(key, decodeURIComponent(options[key]));
    })
    // change route
    let queryParams = {};
    params.paramsMap.forEach((value, key) => {
      queryParams[key] = decodeURIComponent(value[0])
    });
    this.router.navigate([root], { queryParams });
  }


  setFooterFull(status) {
    this.getFooterFull.emit(status);
  }

  setHeaderCommon(status) {
    this.getHeaderCommon.emit(status);
  }

  setFooterAssuranceSection(status) {
    this.getFooterAssuranceSection.emit(status);
  }

  reGetdata(){
    this.runGetdata.emit(true);
  }
  paginationGetdata(data){
    this.runPagination.emit(data);
  }

}
