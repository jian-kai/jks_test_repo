export interface Item {
    id: number,
    description: string,
    price: number,
    name: string,
    image_src: string,
    quantity?: number
}