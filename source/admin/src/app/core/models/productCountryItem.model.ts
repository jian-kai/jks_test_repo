export interface ProductCountryItem {
    CountryId: number,
    CountryName: string,
    currencyDisplay: string,
    basedPrice?: number,
    sellPrice?: number,
    qty?: number,
    maxQty?: number,
    isOnline?: boolean,
    isOffline?: boolean
}