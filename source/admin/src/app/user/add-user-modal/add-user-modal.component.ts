import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';

@Component({
  selector: 'add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})
export class AddUserModalComponent extends ModalComponent {
  public userInfo: any;
  public currentUser: any;
  public userForm: FormGroup;
  public countries: any;
  public roles: any;
  public marketingOffices: any;
  public enableSelectCountry: boolean = false;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService
  ) {
    super(modalService, el);
    this.id = 'add-user-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.userForm = builder.group({
      firstName: [''],
      lastName: [''],
      email: ['', Validators.required],
      phone: [''],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      RoleId: [''],
      viewAllCountry: [''],
    });

    this.globalService.getRoleList().then((data) => {
      this.roles = data;
      try {
        this.userForm.controls["RoleId"].setValue(this.roles.filter(role => role.roleName === 'staff')[0].id);
      } catch(e) {
        // Dop nothing
      }
    });
    // get marketing offices
    this.globalService.getMarketingOfficesList().then((data) => {
      this.marketingOffices = data;
    });

    if (this.globalService.isSupperAdmin()) {
      this.userForm.addControl("CountryId", new FormControl("", Validators.required));
      this.userForm.controls["CountryId"].setValue(this.currentUser.Country.id);
      this.userForm.addControl("MarketingOfficeId", new FormControl(""));
      this.globalService.getCountryList().then((data) => {
        this.countries = data;
      });
    }

  }

  // do add user
  doAddUser() {
    let credentials = this.userForm.value;
    console.log('credentials ', credentials)
    if (!credentials.RoleId || credentials.RoleId === "null") {
      credentials.RoleId = this.roles.filter(role => role.roleName === 'staff')[0].id;
    }
    if(credentials.MarketingOfficeId === "null") {
      credentials.MarketingOfficeId = null;
    }
    console.log('credentials 222 ', credentials)
    this._http._create(`${this.appConfig.config.api.users}`, credentials).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

}