import { Component, ElementRef, OnInit, Output, EventEmitter, Input, AfterViewInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
@Injectable()
@Component({
  selector: 'edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss']
})
export class EditUserModalComponent extends ModalComponent {
  public userInfo: any;
  public currentUser: any;
  public userForm: FormGroup;
  public countries: any;
  public roles: any;
  public marketingOffices: any;
  private enableSelectCountry: boolean = false;
  public isLoading: boolean = true;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  private userID: number;
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'edit-user-modal';

    this.userForm = builder.group({
      // firstName: [''],
      // lastName: [''],
      // email: ['', Validators.required],
      // phone: [''],
      // password: ['', Validators.required],
      // confirmPassword: ['', Validators.required],
      RoleId: ['']
    });
  }

  setInitData(_userID) {
    this.userID = _userID;
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    setTimeout(_ => this.getUserInfo(this.userID));
  }

  // get user info by ID
  getUserInfo(_userID: number) {
    this.isLoading = true;
    this._http._getDetail(`${this.appConfig.config.api.users}/${_userID}`).subscribe(
      data => {
        this.userForm = this.globalService.setValueFormBuilder(this.userForm, data);
        this.userInfo = data;
        this.setForm();
      },
      error => {
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  setForm() {
    // get roles
    this.globalService.getRoleList().then((data) => {
      this.roles = data;
    });
    this.roles = this.globalService.getRoleList();

    // get marketing offices
    this.globalService.getMarketingOfficesList().then((data) => {
      this.marketingOffices = data;
    });
    this.marketingOffices = this.globalService.getMarketingOfficesList();

    if (this.globalService.isSupperAdmin()) {
      this.userForm.addControl("CountryId", new FormControl("", Validators.required));
      this.userForm.controls["CountryId"].setValue(this.userInfo.Country.id);
      this.userForm.addControl("viewAllCountry", new FormControl(""));
      this.userForm.controls["viewAllCountry"].setValue(this.userInfo.viewAllCountry);
      this.userForm.addControl("MarketingOfficeId", new FormControl(""));
      this.userForm.controls["MarketingOfficeId"].setValue(this.userInfo.MarketingOfficeId);
      this.globalService.getCountryList().then((data) => {
        this.countries = data;
      });
    }
    this.isLoading = false;
  }

  // do edit
  doEditUser() {
    let credentials = this.userForm.value;
    console.log('credentials.RoleId', credentials.RoleId);
    if (!credentials.RoleId || credentials.RoleId === "null") {
      credentials.RoleId = this.roles.filter(role => role.roleName === 'staff')[0].id;
    }
    if(credentials.MarketingOfficeId === "null") {
      credentials.MarketingOfficeId = null;
    }
    console.log('credentials.RoleId', credentials.RoleId);
    this._http._updatePut(`${this.appConfig.config.api.users}/${this.userInfo.id}`, credentials).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

}
