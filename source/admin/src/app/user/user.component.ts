import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';
import { DialogService } from '../core/services/dialog.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss', '../../assets/sass/table.scss']
})
export class UserComponent extends ListViewsComponent {
  public userList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userID: number;
  public showing: string;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public timer = null;
  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    public modalService: ModalService,
    routerService: RouterService,
    private _dialogService: DialogService,
    el: ElementRef,) {
      super(location, routerService, el);
      this.loadingService.status.subscribe((value: boolean) => {
        this.isLoading = value;
      });
    }

  // get user list
  getData() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.users}?` + this.fixedEncodeURI(this.options.toString()) ).subscribe(
      data => {
        this.userList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }
  

  // sort by order_by
  sortByName(type: string) {
    this.orderBy = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["firstName"]');
    } else {
      this.filterby('orderBy', '["firstName_' + type + '"]');
    }

  }

   // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)
  }

  // add new user
  addUser() {
    this.modalService.reset('add-user-modal');
    this.modalService.open('add-user-modal');
  }

  // edit User
  editUser(_userID: number) {
    this.userID = _userID;
    this.modalService.open('edit-user-modal', this.userID);
  }

  // delete user
  deleteUser(userID: number) {
    alert('todo');
  }

  // when user edit profile
  hasChangeUser(status: boolean) {
    if (status)
      this.getData();
  }

  // delete product 
  deleteUserCustomer(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updateProductStatus(_id, false);
      }
    }));
  }

  // active product 
  activeUserCustomer(_id: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.updateProductStatus(_id, true);
      }
    }));
  }

  // update Product status: 'removed' | 'active'
  updateProductStatus(_id: number, _status: boolean) {
    this.loadingService.display(true);
    let formData: FormData = new FormData();
    formData.append('isActive', `${_status}`);
    let _data = {isActive: `${_status}`};
    this._http._updatePut(`${this.appConfig.config.api.users}/${_id}`, _data).subscribe(
      data => {
        if (data) {
          this.loadingService.display(false);
          this.getData();
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }
  changedatalist(list){ 
    this.userList.rows =  list ;  
  }
}