import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';

import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LoadingService } from '../../../core/services/loading.service';
import { FileValidator } from '../../../core/directives/required-file.directive';
import { lengthValidator } from '../../../core/validators/length.validator';

@Component({
  selector: 'add-product-category-modal',
  templateUrl: './add-product-category.component.html',
  styleUrls: ['./add-product-category.component.scss'],
  providers: [DatePipe]
})
export class AddProductCategoryComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  private currentUser: any;
  public categoryForm: FormGroup;
  public isLoading: boolean = true;

  constructor(
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private _dialogService: DialogService,
    private location: Location,
    private loadingService: LoadingService,
    private datePipe: DatePipe) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    this.categoryForm = builder.group({
      name: ['', [Validators.required, lengthValidator({ max: 200 })]],
      url: ['', [FileValidator.validate]],
      productTypeTranslate: builder.array([], Validators.compose([Validators.required])),
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
  }

  ngOnInit() {
  }

  // do add Category
  doAddCategory() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        // let credentials = this.categoryForm.value;
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let categoryForm = this.categoryForm;
        for (var key in categoryForm.value) {
          switch (key) {
            case "productTypeTranslate":
              let i = 0;
              let indexplanDetails = 0;
              categoryForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;
                  }
                  formData.append(`productTypeTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            default:
              if (categoryForm.value[key]) {
                if (key === 'expiredAt' || (key === 'activeAt')) {
                  formData.append(key, this.datePipe.transform(categoryForm.value[key], 'yyyy-MM-dd'));
                } else {
                  formData.append(key, categoryForm.value[key]);
                }
              }
              break;
          }
        }
        this._http._createWithFormData(`${this.appConfig.config.api.product_types}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.alertService.error(error);
            this.loadingService.display(false);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }
}
