import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTypeTranslateFormComponent } from './product-type-translate-form.component';

describe('ProductTypeTranslateFormComponent', () => {
  let component: ProductTypeTranslateFormComponent;
  let fixture: ComponentFixture<ProductTypeTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
