import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';
import { empty } from 'rxjs/Observer';
import { element } from 'protractor';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss', '../../../assets/sass/table.scss']
})
export class ProductCategoryComponent extends ListViewsComponent {
  private countries: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public showing: string;
  public catID: number;
  public totalItems: number = 0;
  public options: URLSearchParams;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService, ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
  }

  // get user list
  getData() {
    this.loadingService.display(true);
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', 'name_DESC');
    }
    console.log(this.appConfig.config.api.admin_product_types);
    this._http._getList(`${this.appConfig.config.api.admin_product_types}?` + this.options.toString()).subscribe(
      data => {
        this.countries = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
        console.log(data);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // add new category
  addProductCategory() {
    this.modalService.reset('add-product-category-modal');
    this.modalService.open('add-product-category-modal');
  }

  // edit category
  editProductCategory(_catItem: any) {
    this.modalService.open('edit-product-category-modal', _catItem);
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', '["name"]');
    } else {
      this.filterby('orderBy', '["name_DESC"]');
    }
  }
  // sort by order_by
  sortByNameclick() {
    if (this.orderBy === "ASC") {
      this.orderBy = "DESC";
      this.filterby('orderBy', '["name_DESC"]');
    } else {
      this.orderBy = "ASC";
      this.filterby('orderBy', '["name"]');
    }
  }
  resetPage() {
    // this.options.page = 1;
  }

  // when user edit profile
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }

  // show number per page
  changePerPage(limit: number) {
    this.resetPage();
    // this.options.limit = limit;
    this.getData();
  }
  changestatuscategory(categoryid) {
    alert('Miss API Status category');
  }
  changedatalist(list) {
    this.countries.rows = list;
  }
  public mutiaction = false;
  checkboxchecked(event, id) {
    ///console.log(event , id );
    if (event.target.checked && this.mutiaction === false) {
      this.mutiaction = true;
    }

    let checkedcurrent = $('.actionactive.checkboxall').attr('idchecked');
    let checkedcurrentnew = $(event.target).val();
    console.log(checkedcurrentnew);
    //console.log(checkedcurrentnew);
    if (checkedcurrentnew === 'all') {
      if (event.target.checked) {
        let checkallid: any = '';
        $('.actionactive').each(function (i, e) {
          let valuenow = $(e).attr('value');
          if (valuenow != 'all') {
            $(e).prop("checked", true);
            if (checkallid == '') {
              checkallid = valuenow;
            } else {
              checkallid = checkallid + ',' + valuenow;
            }
          }
        });
        $('.actionactive.checkboxall').attr('idchecked', checkallid);
      } else {
        $('.actionactive.checkboxall').attr('idchecked', '');
        this.mutiaction = false;
        $('.actionactive').each(function (i, e) {
          $(e).prop("checked", false);
        });
      }
    } else {
      if (checkedcurrent == undefined || checkedcurrent == '') {
        if (event.target.checked) {
          $('.actionactive.checkboxall').attr('idchecked', checkedcurrentnew);
        }
      } else {
        if (event.target.checked) {
          let addnewvalue = checkedcurrent + ',' + checkedcurrentnew;
          $('.actionactive.checkboxall').attr('idchecked', addnewvalue);
        } else {
          let spl = checkedcurrent.split(',');
          let value = '';
          spl.forEach(e => {
            if (e != checkedcurrentnew) {
              if (value == '') {
                value = e;
              } else {
                value = value + ',' + e;
              }
            }
          })
          $('.actionactive.checkboxall').attr('idchecked', value).prop('checked', false);
          if (value == '') {
            this.mutiaction = false;
          };
        }
      }
    }
  }
  changeStatusProductCategory (id: number , status: string) {
    let productTypeIds = [id];
    let formData: object = { productTypeIds, status  };   
    // console.log(formData); 
     console.log(formData);
     this._http._create(`${this.appConfig.config.api.product_types}/change-status`, formData).subscribe(
       data => {
         if (data.ok) {
           this.loadingService.display(false);
           this.getData();
         }
       },
       error => {
         this.loadingService.display(false);
         this.alertService.error(error);
       }
     )
  }
  
}
