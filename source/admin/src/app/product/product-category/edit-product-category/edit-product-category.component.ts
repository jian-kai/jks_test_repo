import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LoadingService } from '../../../core/services/loading.service';
import { ProductService } from '../../../core/services/product.service';
import { FileValidator } from '../../../core/directives/required-file.directive';

@Component({
  selector: 'edit-product-category',
  templateUrl: './edit-product-category.component.html',
  styleUrls: ['./edit-product-category.component.scss'],
  providers: [DatePipe]
})
export class EditProductCategoryComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public currentUser: any;
  public categoryForm: FormGroup;
  public categoryInfo: any;
  public isLoading: boolean = true;
  public productTypeId : number;

  constructor(
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private _dialogService: DialogService,
    private location: Location,
    private loadingService: LoadingService,
    private productService: ProductService,
    private activateRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    this.categoryForm = builder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      url: ['', [FileValidator.validate]],
      productTypeTranslate: builder.array([], Validators.compose([Validators.required])),
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.activateRoute.params.subscribe(params => {
      this.productTypeId = +params['id']// (+) converts string 'id' to a number
    });
  }

  ngOnInit() {
    this.getInfo();
  }

  // get user info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.product_types}/${this.productTypeId}`).subscribe(
      data => {
        this.categoryForm.patchValue(data.data);
        data.data.productTypeTranslate.forEach(element => {
          let controlTranslate = <FormArray>this.categoryForm.controls['productTypeTranslate'];
          let prDtsCtrl = this.productService.initProductTypeTranslate();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlTranslate.push(prDtsCtrl);
        });
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  setInitData(_catItem) {
    this.categoryInfo = _catItem;
    this.categoryForm = this.globalService.setValueFormBuilder(this.categoryForm, _catItem);
  }

  // do edit Category
  doEditCategory() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        // let credentials = this.categoryForm.value;
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let categoryForm = this.categoryForm;
        for (var key in categoryForm.value) {
          switch (key) {
            case "productTypeTranslate":
              let i = 0;
              let indexplanDetails = 0;
              categoryForm.value[key].forEach((item, index) => {

                for (let key in item) {
                  if ((key === 'id' && !item[key])) {
                    continue;
                  }
                  formData.append(`productTypeTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            default:
              if (categoryForm.value[key]) {
                if (key === 'expiredAt' || (key === 'activeAt')) {
                  formData.append(key, this.datePipe.transform(categoryForm.value[key], 'yyyy-MM-dd'));
                } else {
                  formData.append(key, categoryForm.value[key]);
                }
              }
              break;
          }
        }
        this._http._updatePut(`${this.appConfig.config.api.product_types}/${this.productTypeId}`, formData, 'form-data').subscribe(
          data => {
            this.loadingService.display(false);
            this.location.back();
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }
}