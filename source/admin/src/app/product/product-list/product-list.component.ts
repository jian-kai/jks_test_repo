import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';
import { empty } from 'rxjs/Observer';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss', '../../../assets/sass/table.scss']
})
export class ProductListComponent extends ListViewsComponent {
  public productList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC"; 
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public _test;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public timer = null;

  constructor(private _http: HttpService,
    routerService: RouterService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    //private multipleChoice = '12121212',     
    public modalService: ModalService, private _dialogService: DialogService) {
    super(location, routerService, el);    
    this.multipleChoiceForTables = true; // after super()   
    // this.defaultOptions = { limit: `${this.appConfig.config.limitRows}`, page: 1 } 
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // get user list
  getData() {
    // this.options.set('orderBy', 'createdAt_DESC');
    // console.log(this.multipleChoice);
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', 'createdAt_DESC');
    }
    console.log('this.options', this.options)
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.products}?${this.options.toString()}`).subscribe(
      data => {
        this.productList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
        console.log(data);
      },
      error => {
        this.alertService.error(error);
      }
    )
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    if (!this.productTypes) {
      this.globalService.getProductTypes().then((data) => {
        this.productTypes = data;
      });
    }
  }

  // show number per page
  changePerPage(event) {
    this.onLimitChange(event);
  }

  // sort by order_by
  sortByDate(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', 'createdAt');
    } else {
      this.filterby('orderBy', 'createdAt_DESC');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)

  }

  // delete product 
  deleteProduct(_id: number) {
    // this._dialogService.confirm().afterClosed().subscribe((result => {
    //   if (result) {
    //     this.updateProductStatus(_id, 'removed');
    //   }
    // }));
    this.updateProductStatus(_id, 'removed');
  }

  // active product 
  activeProduct(_id: number) {
    // this._dialogService.confirm().afterClosed().subscribe((result => {
    //   if (result) {
    //     this.updateProductStatus(_id, 'active');
    //   }
    // }));
    this.updateProductStatus(_id, 'active');
  }

  // update Product status: 'removed' | 'active'
  updateProductStatus(_id: number, _status: string) {
    this.loadingService.display(true);
    let formData: FormData = new FormData();
    formData.append('status', _status);
    this._http._updatePut(`${this.appConfig.config.api.products}/${_id}`, formData, "form-data").subscribe(
      data => {
        if (data.ok) {
          this.loadingService.display(false);
          this.getData();
        }
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }
  changedatalist(list) {
    this.productList.rows = list;
  }
  runGetdata(){
    // nothing
  }
}