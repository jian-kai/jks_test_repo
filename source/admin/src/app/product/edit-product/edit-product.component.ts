import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { LoadingService } from '../../core/services/loading.service';
import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { ProductService } from '../../core/services/product.service';
import { ProductCountryItem } from '../../core/models/productCountryItem.model';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss', '../../../assets/sass/product.scss']
})
export class EditProductComponent implements OnInit {
  private currentUser: any;
  public productInfo: any;
  public productForm: FormGroup;
  public productTypes: any;
  public addImagesFlag: boolean = false;
  private images = [];
  private setfiles = [];
  public countries: any;
  public selectCountryForm: FormGroup;
  public productCountryForms = [];
  public productCountryItem: ProductCountryItem;
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public productId: number;
  public isLoading: boolean = true;
  private reloadProductTranslate: boolean = false;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _productService: ProductService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private productService: ProductService,
    private _dialogService: DialogService,
    private location: Location, ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    console.log('this.currentUser', this.currentUser)
    // get product detail by product id
    this.loadingService.display(true);
    this.isLoading = true;
    this.activateRoute.params.subscribe(params => {
      this.productId = +params['id']// (+) converts string 'id' to a number
    });
    this._http._getDetail(`${this.appConfig.config.api.products}/${this.productId}`).subscribe(
      data => {
        this.productInfo = data;
        this.initFormData();
        this.loadingService.display(false);
        this.isLoading = false;
      },
      error => {
        this.alertService.error(error);
        this.loadingService.display(false);
        this.isLoading = false;
        console.log(error.error);
      }
    )
    this.productForm = builder.group({
      ProductTypeId: ['', Validators.required],
      sku: ['', [Validators.required, Validators.maxLength(10)]],
      slug: ['', Validators.required],
      images: ['', [FileValidator.validate]],
      defaultImageIndex: ['', Validators.required],
      productTranslate: builder.array([]),
      order: [0],
      productCountry: builder.array([]),
      isFeatured: [''],
      isFeaturedProduct: [''],
      width: [0],
      long: [0],
      hight: [0],
      weight: [0]
    });
    this.selectCountryForm = builder.group({
      countryId: ['', Validators.required],
    });

    this.selectTranslateForm = builder.group({
      langCode: ['', Validators.required],
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });

  }

  ngOnInit() {
    this.isLoading = true;
    this.globalService.getProductTypes().then((data) => {
      this.productTypes = data;
    });

    // add default Product Translate form for CurrentUser
    this.addImagesFlag = true;

    // this.loadingService.status.subscribe((value: boolean) => {
    //   this.isLoading = value;
    // })

  }

  // set value for form data
  initFormData() {
    this.productForm.controls['ProductTypeId'].setValue(this.productInfo.ProductType.id);
    this.productForm.controls['sku'].setValue(this.productInfo.sku);
    this.productForm.controls['slug'].setValue(this.productInfo.slug ? this.productInfo.slug : '');
    this.images = this.productInfo.productImages;
    this.productForm.controls['order'].setValue(this.productInfo.order);
    this.productForm.controls['images'].setValue(this.images);
    this.productForm.controls['isFeatured'].setValue(this.productInfo.isFeatured);
    this.productForm.controls['isFeaturedProduct'].setValue(this.productInfo.isFeaturedProduct);
    this.productForm.controls['width'].setValue(this.productInfo.width);
    this.productForm.controls['long'].setValue(this.productInfo.long);
    this.productForm.controls['hight'].setValue(this.productInfo.hight);
    this.productForm.controls['weight'].setValue(this.productInfo.weight);

    //productTranslate
    this.productInfo.productTranslate.forEach(translate => {
      let productDetails: any = {};
      productDetails = this.productInfo.productDetails.filter(item => item.langCode === translate.langCode);
      if (productDetails.length > 0) {
        this.addProductTranslate(translate.langCode, translate, productDetails);
      } else {
        this.addProductTranslate(translate.langCode, translate);
      }

    });
    //productCountry
    this.globalService.getCountryList().then(data => {
      this.countries = data;
      this.productInfo.productCountry.forEach(element => {
        this.addProductCountry(element.Country.id, element);
      });
    })
  }

  // add images for products
  addImages() {
    this.addImagesFlag = true;
  }

  doEditProduct() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let productForm = this.productForm;
        // formData.append("ProductId", this.productInfo.id);
        for (var key in this.productForm.value) {
          switch (key) {
            case "productTranslate":
              let i = 0;
              let indexProductDetails = 0;
              productForm.value[key].forEach((item, index) => {

                item.productDetails.forEach((_productDetails, index) => {
                  formData.append(`productDetails[${indexProductDetails}].image`, _productDetails.imageUrl);
                  for (let key in _productDetails) {
                    if (key === 'id' && !_productDetails[key]) {
                      continue;;
                    }
                    formData.append(`productDetails[${indexProductDetails}].` + key, _productDetails[key]);
                  }
                  indexProductDetails++;
                });

                for (let key in item) {
                  if ((key === 'id' && !item[key]) || key === 'productRelateds') {
                    continue;;
                  }
                  formData.append(`productTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            case "productCountry":
              let j = 0;
              productForm.value[key].forEach((item, index) => {
                for (let key in item) {
                  if (key === 'id' && !item[key]) {
                    continue;;
                  }
                  formData.append(`productCountry[${j}].` + key, item[key]);
                }
                item.productRelateds.forEach((_productRelateds, index) => {
                  if (_productRelateds.id) {
                    formData.append(`productCountry[${j}].` + `productRelateds[${index}].` + 'id', _productRelateds.id);
                  }
                  formData.append(`productCountry[${j}].` + `productRelateds[${index}].` + 'RelatedProductCountryId', _productRelateds.product.productCountry[0].id);
                });
                j++;
              });
              break;

            case "images":
              let _imgs = [];
              productForm.value[key].forEach((image, index) => {
                if (image) {
                  _imgs[index] = {};
                  if (image.id) {
                    for (let key in image) {
                      let value = image[key];
                      if (key !== 'isDefault') {
                        _imgs[index][key] = value;
                      }
                    }
                  } else {
                    _imgs[index].image = image;
                  }
                  if (index === productForm.value["defaultImageIndex"]) {
                    image.isDefault = true;
                    _imgs[index].isDefault = image.isDefault;
                  } else {
                    _imgs[index].isDefault = 'false';
                  }
                }

              });
              let _arr = this.globalService.reindexingArray(_imgs);
              _arr.forEach((image, index) => {
                for (let key in image) {
                  let value = image[key];
                  formData.append(`images[${index}].` + `${key}`, value);
                }
              });
              break;

            default:
              formData.append(key, productForm.value[key]);
              break;
          }
        }
        this._http._updatePut(`${this.appConfig.config.api.products}/${this.productInfo.id}`, formData, "form-data").subscribe(
          data => {
            if (data.ok) {
              this.loadingService.display(false);
              this.location.back();
              // this._router.navigate(['/products'], { queryParams: {} });
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));

  }

  // add product country
  addProductCountry(_countryId: number, _setData?: any) {
    let countryItem = this.countries.filter(country => country.id == _countryId)[0];
    this.productCountryItem = {
      CountryId: countryItem.id,
      CountryName: countryItem.name,
      currencyDisplay: countryItem.currencyDisplay,
      isOffline: countryItem.isOffline,
      isOnline: countryItem.isOnline
    }
    let control = <FormArray>this.productForm.controls['productCountry'];
    let prCoCtrl = this.productService.initProductCountry();
    prCoCtrl = this.globalService.setValueFormBuilder(prCoCtrl, this.productCountryItem);
    if (_setData) {
      // prCoCtrl = this.globalService.setValueFormBuilder(prCoCtrl, _setData);

      prCoCtrl.controls.id.setValue(_setData.id);
      prCoCtrl.controls.basedPrice.setValue(_setData.basedPrice);
      prCoCtrl.controls.sellPrice.setValue(_setData.sellPrice);
      prCoCtrl.controls.isOffline.setValue(_setData.isOffline);
      prCoCtrl.controls.isOnline.setValue(_setData.isOnline);
      prCoCtrl.controls.maxQty.setValue(_setData.maxQty);
      prCoCtrl.controls.qty.setValue(_setData.qty);
      prCoCtrl.controls.order.setValue(_setData.order);

      // add product relateds
      let productRelateds = _setData.productRelateds;
      let controlProductRelateds = <FormArray>prCoCtrl.controls['productRelateds'];
      console.log('productRelateds', productRelateds);
      productRelateds.forEach(element => {
        let prDtCtrl = this.productService.initProductRelateds();
        // set product
        let _product = element.RelatedProductCountry.Product;
        _product.productCountry = [];
        _product.productCountry.push({ id: element.RelatedProductCountryId });
        prDtCtrl.patchValue({ id: element.id, product: _product });

        controlProductRelateds.push(prDtCtrl);
      });
    } else {
      prCoCtrl.controls.isOnline.setValue(true);
      prCoCtrl.controls.isOffline.setValue(true);
    }
    control.push(prCoCtrl);
  }

  removeProductCountry(_i: number) {
    let control = <FormArray>this.productForm.controls["productCountry"];
    if (control.controls[_i].value.id) {
      // remove product country on database
      this._dialogService
        .confirm()
        .afterClosed()
        .subscribe(result => {
          if (result) {
            let pathDelete = `${this.appConfig.config.api.products}/${
              this.productId
              }/product-countries/${control.controls[_i].value.id}`;
            this._http._delete(`${pathDelete}`).subscribe(
              data => {
                if (data.ok) {
                  control.removeAt(_i);
                }
              },
              error => {
                console.log("error", error);
              }
            );
          }
        });
    } else {
      control.removeAt(_i);
    }
    return false;
  }

  //add Translate product
  addProductTranslate(_langCode: string, _setDataTranslate?: any, _setDataDetails?: any) {
    let control = <FormArray>this.productForm.controls['productTranslate'];
    // add data value for product translate
    let prTrCtrl = this.productService.initProductTranslate();
    if (_setDataTranslate) {
      prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, _setDataTranslate);
    } else {
      prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, { langCode: _langCode });
    }

    let controlPRDetails = <FormArray>prTrCtrl.controls['productDetails'];
    let prDtCtrl: any;
    // set data value for product details
    if (_setDataDetails) {
      if (_setDataDetails.length) {
        _setDataDetails.forEach(details => {
          prDtCtrl = this.productService.initProductDetails();
          prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, details);
          controlPRDetails.push(prDtCtrl);
        });
      } else {
        prDtCtrl = this.productService.initProductDetails();
        prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, _setDataDetails);
        controlPRDetails.push(prDtCtrl);
      }
    } else {
      prDtCtrl = this.productService.initProductDetails();
      prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, { langCode: _langCode, type: 'center' });
      controlPRDetails.push(prDtCtrl);
    }
    control.push(prTrCtrl);
  }

  // remove product translate
  removeProductTranslate(_i: number) {
    let control = <FormArray>this.productForm.controls['productTranslate'];
    if (control.controls[_i].value.id) {
      // remove product country on database
      let pathDelete = `${this.appConfig.config.api.products}/${this.productId}/product-translates/${control.controls[_i].value.id}`;
      this._http._delete(`${pathDelete}`).subscribe(
        data => {
          if (data.ok) {
            control.removeAt(_i);
          }
        },
        error => {
          console.log('error', error);
        }
      )
    } else {
      control.removeAt(_i);
    }
    return false;
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/products']);
  }

}
