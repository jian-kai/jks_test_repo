import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'product-details-form',
  templateUrl: './product-details-form.component.html',
  styleUrls: ['./product-details-form.component.scss']
})
export class ProductDetailsFormComponent implements OnInit {
  @Input() productDetailsForm;
  @Input() parentOnActive;
  public image: string;
  constructor() { }

  ngOnInit() {
  }

}
