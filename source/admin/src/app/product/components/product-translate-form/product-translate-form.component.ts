import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';


import { GlobalService } from '../../../core/services/global.service';
import { ProductService } from '../../../core/services/product.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'product-translate-form',
  templateUrl: './product-translate-form.component.html',
  styleUrls: ['./product-translate-form.component.scss', '../../../../assets/sass/product.scss']
})
export class ProductTranslateFormComponent implements OnInit {
  @Input() productTranslateForm;
  @Input() parentOnActive;
  private productId: number;
  constructor(private productService: ProductService,
    private globalService: GlobalService,
    private _http: HttpService,
    private appConfig: AppConfig,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.productId = +params['id']// (+) converts string 'id' to a number
    });
  }

  // add a product details with translate group
  addProductDetails(): void {
    let controlPRDetails = <FormArray>this.productTranslateForm.controls['productDetails'];
    let prDtsCtrl = this.productService.initProductDetails();
    prDtsCtrl = this.globalService.setValueFormBuilder(prDtsCtrl, { langCode: this.productTranslateForm.controls.langCode.value, type: 'center' })
    controlPRDetails.push(prDtsCtrl);
  }

  // remove product details
  removeProductDetails(_j: number) {
    let controlPRDetails = <FormArray>this.productTranslateForm.controls['productDetails'];
    if (controlPRDetails.controls[_j].value.id) {
      // remove product details on database
      let pathDelete = `${this.appConfig.config.api.products}/${this.productId}/product-details/${controlPRDetails.controls[_j].value.id}`;
      this._http._delete(`${pathDelete}`).subscribe(
        data => {
          if (data.ok) {
            controlPRDetails.removeAt(_j);
          }
        },
        error => {
          console.log('error', error);
          this.alertService.error(error);
        }
      )
    } else {
      controlPRDetails.removeAt(_j);
    }
    return false;
  }

}
