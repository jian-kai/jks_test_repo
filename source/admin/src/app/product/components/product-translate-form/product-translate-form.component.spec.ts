import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTranslateFormComponent } from './product-translate-form.component';

describe('ProductTranslateFormComponent', () => {
  let component: ProductTranslateFormComponent;
  let fixture: ComponentFixture<ProductTranslateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTranslateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTranslateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
