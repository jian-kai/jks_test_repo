import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductRelatedFormComponent } from './product-related-form.component';

describe('ProductRelatedFormComponent', () => {
  let component: ProductRelatedFormComponent;
  let fixture: ComponentFixture<ProductRelatedFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductRelatedFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductRelatedFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
