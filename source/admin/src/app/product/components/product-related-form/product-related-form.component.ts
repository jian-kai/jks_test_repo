import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';

import { TranslatePipe } from '../../../core/pipes/translate.pipe';
import { ProductService } from '../../../core/services/product.service';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'product-related-form',
  templateUrl: './product-related-form.component.html',
  styleUrls: ['./product-related-form.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  providers: [TranslatePipe]
})
export class ProductRelatedFormComponent implements OnInit {
  @Input() productRelatedsForm;
  @Input() products;
  // @Input() planTypes;
  // @Input() parentOnActive;
  public filteredList: any = [];
  public query: string = '';
  public image: string;
  public options: any = {};
  public countryId: number;
  public totalDeliverTimes: number;
  public deliverTimes: any = [];
  public deliverTimesModel: any = [];
  public selectedProduct: any = [];
  public queryForm: FormGroup;
  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private elementRef: ElementRef,
    private translatePipe: TranslatePipe,
    private builder: FormBuilder,
    private productService: ProductService,
    private _dialogService: DialogService) {
      this.queryForm = builder.group({
        query: ['', Validators.required],
      });
  }

  ngOnInit() {
    // this.countryId = this.productRelatedsForm.parent.controls.CountryId.value;
    // this.totalDeliverTimes = this.planTypes.filter(item => item.id == this.planDetailsForm.parent.parent.parent.controls.PlanTypeId.value)[0].totalDeliverTimes;
    // console.log('this.totalDeliverTimes', this.totalDeliverTimes)
    // this.planDetailsForm.parent.parent.parent.controls.PlanTypeId.valueChanges.subscribe(data => {
    //   this.totalDeliverTimes = this.planTypes.filter(item => item.id == data)[0].totalDeliverTimes;
    //   if (!this.totalDeliverTimes) {
    //     this.totalDeliverTimes = 1;
    //   }
    //   console.log('this.totalDeliverTimes', this.totalDeliverTimes)
    //   this.clearPlanDetails();
    // })
  }
  // get product list
  // getproductList() {
  //   this.options = {
  //     CountryId: this.countryId,
  //     status: 'active'
  //   }
  //   let params = this.globalService._URLSearchParams(this.options);
  //   this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
  //     data => {
  //       this.products = data.rows;
  //     },
  //     error => {
  //       console.log('error', error);
  //     }
  //   )
  // }

  // clear product relateds
  clearProductRelateds() {
    let control = <FormArray>this.productRelatedsForm;
    control.controls.forEach((element, index) => {
      control.removeAt(index);
    });
  }

  filterProducts() {
    if (this.queryForm.value.query !== "") {
      this.filteredList = [];
      this.products.forEach((product, index) => {
        let has = false;
        product.productTranslate.forEach(item => {
          if (item.name.toLowerCase().indexOf(this.queryForm.value.query.toLowerCase()) > -1) {
            return has = true;
          }
        });
        if (has) {
          this.filteredList.push(product);
        }
      });
    } else {
      this.filteredList = [];
    }
  }
  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }

  select(item) {
    // set value for formName
    let _val;
    let hasExists = false;
    if (!this.productRelatedsForm.controls.length) {
      _val = [];
    } else {
      _val = this.productRelatedsForm.value;
      _val.forEach(element => {
        if (element.product.productCountry[0].id === item.productCountry[0].id) {
          return hasExists = true;
        }
      });
    }
    if (!hasExists) {
      let control = <FormArray>this.productRelatedsForm;
      let prCtrl = this.productService.initProductRelateds();
      prCtrl = this.globalService.setValueFormBuilder(prCtrl, { product: item});
      // // add deliverPlan
      // let controlDeliverPlan = <FormArray>prCtrl.controls['deliverPlan'];
      // if(!this.totalDeliverTimes) {
      //   this.totalDeliverTimes = 1;
      // }
      // for (var index = 1; index <= this.totalDeliverTimes; index++) {
      //   let deliverPlanInit = this.planService.initDeliverPlan();
      //   deliverPlanInit = this.globalService.setValueFormBuilder(deliverPlanInit, { times: index, qty: 0 })
      //   controlDeliverPlan.push(deliverPlanInit);
      // }

      control.push(prCtrl);

    }
    this.filteredList = [];
    this.queryForm.controls.query.setValue("");
  }

  setValue(_val: any) {
    this.productRelatedsForm.controls.product.setValue(_val);
  }

  removeItem(productRelatedId: any, idx: number) {
    let control = <FormArray>this.productRelatedsForm;
    if (control.controls[idx].value.id) {
      this._dialogService.confirm().afterClosed().subscribe((result => {
        if(result) {
          this._http._delete(`${this.appConfig.config.api.products}/product-relateds/${productRelatedId}`)
          .subscribe(result => {
            control.removeAt(idx);
            console.log(result)
          });
        }
      }));
    } else {
      control.removeAt(idx);
    }
  }
}
