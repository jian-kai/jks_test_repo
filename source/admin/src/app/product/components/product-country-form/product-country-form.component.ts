import { Component, ElementRef, OnInit, Output, EventEmitter, Input,  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { ProductService } from '../../../core/services/product.service';

@Component({
  selector: 'product-country-form',
  templateUrl: './product-country-form.component.html',
  styleUrls: ['./product-country-form.component.scss']
})
export class ProductCountryFormComponent implements OnInit {
  @Input() productCountryForm;
  @Input() parentOnActive;

  constructor(private _productService: ProductService,
    private builder: FormBuilder,) {
  }

  ngOnInit() {
    // this.productCountryForm.controls.isOnline.valueChanges.subscribe(data => {
    //   if (!data) {
    //     if (!this.productCountryForm.controls.isOffline.value) {
    //       this.productCountryForm.controls.isOffline.setValue(true);
    //     }
    //   }
    // });
    // this.productCountryForm.controls.isOffline.valueChanges.subscribe(data => {
    //   if (!data) {
    //     if (!this.productCountryForm.controls.isOnline.value) {
    //       this.productCountryForm.controls.isOnline.setValue(true);
    //     }
    //   }
    // });
  }

}
