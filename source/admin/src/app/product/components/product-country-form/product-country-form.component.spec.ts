import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCountryFormComponent } from './product-country-form.component';

describe('ProductCountryFormComponent', () => {
  let component: ProductCountryFormComponent;
  let fixture: ComponentFixture<ProductCountryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCountryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCountryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
