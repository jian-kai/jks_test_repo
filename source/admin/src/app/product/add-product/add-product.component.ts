import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { LoadingService } from '../../core/services/loading.service';
import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { ProductService } from '../../core/services/product.service';
import { ProductCountryItem } from '../../core/models/productCountryItem.model';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss', '../../../assets/sass/product.scss']
})
export class AddProductComponent implements OnInit {
  public currentUser: any;
  public productForm: FormGroup;
  public productTypes: any;
  public addImagesFlag: boolean = false;
  public images = [];
  public setfiles = [];
  public countries: any;
  public selectCountryForm: FormGroup;
  public productCountryForms = [];
  public productCountryItem: ProductCountryItem;
  public selectTranslateForm: FormGroup;
  public languageCodeList: any;
  public activeSubmitForm: boolean = false;

  constructor(private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _productService: ProductService,
    private _router: Router,
    private loadingService: LoadingService,
    private productService: ProductService,
    private _dialogService: DialogService,
    private location: Location, ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }

    this.productForm = builder.group({
      ProductTypeId: ['', Validators.required],
      sku: ['', [Validators.required, Validators.maxLength(10)]],
      slug: ['', Validators.required],
      images: ['', [FileValidator.validate]],
      defaultImageIndex: ['', Validators.required],
      productTranslate: builder.array([]),
      order: [0],
      productCountry: builder.array([]),
      isFeatured: [''],
      isFeaturedProduct: [''],
      width: [0],
      long: [0],
      hight: [0],
      weight: [0],
    });
    this.selectCountryForm = builder.group({
      countryId: ['', Validators.required],
    });

    this.selectTranslateForm = builder.group({
      langCode: ['', Validators.required],
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  ngOnInit() {
    this.globalService.getProductTypes().then((data) => {
      this.productTypes = data;
    });
    this.globalService.getCountryList().then(data => {
      this.countries = data;
      // add default Product Country form for CurrentUser
      this.addProductCountry(this.currentUser.Country.id);
    })

    // add default Product Translate form for CurrentUser
    this.addProductTranslate('en');
    this.addImagesFlag = true;

  }

  // add images for products
  addImages() {
    this.addImagesFlag = true;
  }

  setActiveBtn() {
    this.activeSubmitForm = true;
  }

  doAddProduct() {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let formData: FormData = new FormData();
        let productForm = this.productForm;
        for (var key in this.productForm.value) {
          switch (key) {
            case "productTranslate":
              let i = 0;
              let indexProductDetails = 0;
              productForm.value[key].forEach((item, index) => {

                item.productDetails.forEach((_productDetails, index) => {
                  // productDetails.push(_productDetails);
                  if (_productDetails.title) {
                    formData.append(`productDetails[${indexProductDetails}].image`, _productDetails.imageUrl);
                    for (let key in _productDetails) {
                      if (key === 'id' && !_productDetails[key]) {
                        continue;;
                      }
                      formData.append(`productDetails[${indexProductDetails}].` + key, _productDetails[key]);
                    }
                    indexProductDetails++;
                  }
                });

                for (let key in item) {
                  if (key === 'id' && !item[key]) {
                    continue;;
                  }
                  formData.append(`productTranslate[${i}].` + key, item[key]);
                }
                i++;
              });
              break;

            case "productCountry":
              let j = 0;
              productForm.value[key].forEach((item, index) => {
                for (let key in item) {
                  if ((key === 'id' && !item[key]) || key === 'productRelateds') {
                    continue;;
                  }
                  formData.append(`productCountry[${j}].` + key, item[key]);
                }
                item.productRelateds.forEach((_productRelateds, index) => {
                  if (_productRelateds.id) {
                    formData.append(`productCountry[${j}].` + `productRelateds[${index}].` + 'id', _productRelateds.id);
                  }
                  formData.append(`productCountry[${j}].` + `productRelateds[${index}].` + 'RelatedProductCountryId', _productRelateds.product.productCountry[0].id);
                });
                j++;
              });
              break;

            case "images":
              productForm.value[key].forEach((image, index) => {
                formData.append(`images[${index}].image`, image);
                if (index === productForm.value["defaultImageIndex"]) {
                  image.isDefault = true;
                  formData.append(`images[${index}].isDefault`, image.isDefault);
                } else {
                  formData.append(`images[${index}].isDefault`, "false");
                }
              });
              break;

            default:
              if (productForm.value[key]) {
                formData.append(key, productForm.value[key]);
              }
              break;
          }
        }
        this._http._createWithFormData(`${this.appConfig.config.api.products}`, formData, 'form-data').subscribe(
          data => {
            if (data.ok) {
              this.loadingService.display(false);
              this.location.back();
              // this._router.navigate(['/products'], { queryParams: {} });
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // add product country
  addProductCountry(_countryId: number) {
    let countryItem = this.countries.filter(country => country.id == _countryId)[0];
    this.productCountryItem = {
      CountryId: countryItem.id,
      CountryName: countryItem.name,
      currencyDisplay: countryItem.currencyDisplay,
      isOnline: true,
      isOffline: true,
    }
    let control = <FormArray>this.productForm.controls['productCountry'];
    let prCoCtrl = this.productService.initProductCountry();
    prCoCtrl = this.globalService.setValueFormBuilder(prCoCtrl, this.productCountryItem);
    control.push(prCoCtrl);
  }

  removeProductCountry(_i: number) {
    let control = <FormArray>this.productForm.controls['productCountry'];
    control.removeAt(_i);
  }

  //add Translate product
  addProductTranslate(_langCode: string) {
    let control = <FormArray>this.productForm.controls['productTranslate'];
    let prTrCtrl = this.productService.initProductTranslate();
    prTrCtrl = this.globalService.setValueFormBuilder(prTrCtrl, { langCode: _langCode });

    // add product details
    let controlPRDetails = <FormArray>prTrCtrl.controls['productDetails'];
    let prDtCtrl = this.productService.initProductDetails();
    prDtCtrl = this.globalService.setValueFormBuilder(prDtCtrl, { langCode: _langCode, type: 'center' })
    controlPRDetails.push(prDtCtrl);

    control.push(prTrCtrl);
  }

  // remove product translate
  removeProductTranslate(_i: number) {
    let control = <FormArray>this.productForm.controls['productTranslate'];
    control.removeAt(_i);
  }

  // go to the list screen
  gotoList() {
    this.location.back();
    // this._router.navigate(['/products']);
  }

}
