import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import {HttpService} from '../../core/services/http.service';
import {GlobalService} from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../core/services/storage.service';
import {AlertService} from '../../core/services/alert.service';

@Component({
  selector: 'change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent extends ModalComponent {
  private currentUser: any;
  public changePasswordForm: FormGroup;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,) {
      super(modalService, el);
      this.id = 'change-password-modal';
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser || !this.currentUser.id) {
        console.log('User is not exist');
        return;
      }

      this.changePasswordForm = builder.group({
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.required],
        confirmNewPassword: ['', Validators.required],
      });

    }

    // do change pasword
    doChangePassword() {
      let formData = this.changePasswordForm.value;
      this._http._update(`${this.appConfig.config.api.change_password}`.replace('[userID]', this.currentUser.id), formData).subscribe(
        data => {
          this.close();
        },
        error => {
          this.alertService.error(error);
        }
      )
    }


}
