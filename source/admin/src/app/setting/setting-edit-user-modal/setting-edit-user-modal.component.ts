import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import {HttpService} from '../../core/services/http.service';
import {GlobalService} from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../core/services/storage.service';
import {AlertService} from '../../core/services/alert.service';

@Component({
  selector: 'setting-edit-user-modal',
  templateUrl: './setting-edit-user-modal.component.html',
  styleUrls: ['./setting-edit-user-modal.component.scss']
})
export class SettingEditUserModalComponent extends ModalComponent {
  private currentUser: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Input() userID: any;
  private userInfo: any;
  public userForm: FormGroup;
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    
  ) {
    super(modalService, el);
    this.id = 'setting-edit-user-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
    this.userForm = builder.group({
      firstName: [''],
      lastName: [''],
      email: ['', Validators.required],
      phone: [''],
      // password: [''],
      // confirmPassword: [''],
      gender: ['']
    });
    setTimeout(_ =>
      // console.log('userID', this.userID)
      this.getUserInfo()
    );
  }

  // get user info by ID
  getUserInfo() {
    // this.userInfo = 
    this._http._getDetail(`${this.appConfig.config.api.users}/${this.currentUser.id}`).subscribe(
      data => {
        this.userForm = this.globalService.setValueFormBuilder(this.userForm, data);
      },
      error => {
        this.alertService.error(error);
        console.log(error)
      }
    )
  }

  // do edit
  doEdit() {
    let formData = this.userForm.value;
    this._http._updatePut(`${this.appConfig.config.api.users}/${this.currentUser.id}`, formData).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }


}