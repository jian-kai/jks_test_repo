import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingEditUserModalComponent } from './setting-edit-user-modal.component';

describe('SettingEditUserModalComponent', () => {
  let component: SettingEditUserModalComponent;
  let fixture: ComponentFixture<SettingEditUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingEditUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingEditUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
