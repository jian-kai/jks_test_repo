import { Component, OnInit, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ModalService } from '../core/services/modal.service';
import { LoadingService } from '../core/services/loading.service';
import { StorageService } from '../core/services/storage.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { HttpService } from '../core/services/http.service';
import { AppConfig } from '../config/app.config';
import { GlobalService } from '../core/services/global.service';
import { AlertService } from '../core/services/alert.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  public userInfo: any;
  public userID: number = 0;
  public deliveryAddressList: any;
  public cardsList: any;
  public addressItem: any;
  public isEditAddress: boolean = false;
  public isloadingPersional: boolean = true;
  public isloadingAddress: boolean = true;
  public isloadingPayment: boolean = true;

  constructor(
    el: ElementRef,
    public globalService: GlobalService,
    private loadingService: LoadingService,
    private storage: StorageService,
    public modalService: ModalService,
    private _http: HttpService,
    private appConfig: AppConfig,
    private alertService: AlertService
  ) {
    if (!this.storage.getCurrentUserItem() || !this.storage.getCurrentUserItem().id) {
      console.log('User is not exist');
      return;
    }
    // get user info
    this.getUserinfo();

    // this.deliveryAddressList = this.currentUser.deliveryAddresses[0]; 
  }

  ngOnInit() {
    // this.deliveryAddressList = this.storage.getCurrentUserItem() ? this.storage.getCurrentUserItem().deliveryAddresses : [];
  }

  // get user info 
  getUserinfo() {
    this.isloadingPersional = true;
    this._http._getDetail(`${this.appConfig.config.api.users}/${this.storage.getCurrentUserItem().id}`).subscribe(
      data => {
        this.userInfo = data;
        this.isloadingPersional = false;
      },
      error => {
        this.alertService.error(error);
        console.log(error.error);
      }
    )
  }

  showEditUserModal(userID: number) {
    this.userID = userID;
    setTimeout(_ => this.modalService.open('setting-edit-user-modal'));
  }

  // create address
  addNewAddress() {
    this.modalService.open('create-address-modal');
  }

  // edit address
  showEditAddressModal(_addressItem: any) {
    if (!_addressItem) {
      console.log("Address item is not exist!");
    }
    this.addressItem = _addressItem;
    this.isEditAddress = true;
    setTimeout(_ => this.modalService.open('edit-address-modal'));

  }

  // when user edit profile
  hasChangeUserInfo(status: boolean) {
    if (status)
      this.getUserinfo();
  }

  // create card
  addNewCard() {
    this.modalService.open('create-card-modal');
  }

  // show change password modal
  changePassword() {
    this.modalService.reset('change-password-modal');
    this.modalService.open('change-password-modal');
  }
}
