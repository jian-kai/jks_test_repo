import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule, MdNativeDateModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { routes } from './core/app.routes';

// import config
import { AppConfig } from './config/app.config';

// import service
import { StorageService } from './core/services/storage.service';
import { RequestService } from './core/services/request.service';
import { UsersService } from './auth/services/users.service';
import { ModalService } from './core/services/modal.service';
import { AuthService } from './auth/services/auth.service';
import { HttpService } from './core/services/http.service';
import { StripeService } from './core/services/stripe.service';
import { AlertService } from './core/services/alert.service';
import { LoadingService } from './core/services/loading.service';
import { CountriesService } from './core/services/countries.service';
import { PagerService } from './core/services/pager-service.service';
import { GlobalService } from './core/services/global.service';
import { ProductService } from './core/services/product.service';
import { DialogService } from './core/services/dialog.service';
import { PlanService } from './core/services/plan.service';
import { PromotionService } from './core/services/promotion.service';
import { RouterService } from './core/helpers/router.service';
import { FaqService } from './core/services/faq.service';

// import component
import { AppComponent } from './core/components/app/app.component';
import { MenuComponent } from './core/components/partials/header/menu/menu.component';
import { LoginComponent } from './auth/components/login/login.component';
import { ModalComponent } from './core/directives/modal/modal.component';
import { AlertComponent } from './core/directives/alert/alert.component';
import { LoadingComponent } from './core/directives/loading/loading.component';
import { HeaderComponent } from './core/components/partials/header/header.component';
import { FooterComponent } from './core/components/partials/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LeftMenuComponent } from './core/components/partials/left-menu/left-menu.component';
import { SettingComponent } from './setting/setting.component';
import { RouterLinkActiveDirective } from './core/directives/router-link-active.directive';
import { PaginationComponent } from './core/directives/pagination/pagination.component';
import { SettingEditUserModalComponent } from './setting/setting-edit-user-modal/setting-edit-user-modal.component';
import { UserComponent } from './user/user.component';
import { AddUserModalComponent } from './user/add-user-modal/add-user-modal.component';
import { EditUserModalComponent } from './user/edit-user-modal/edit-user-modal.component';
import { OrderPendingComponent } from './order/order-pending/order-pending.component';
import { OrderHistoryComponent } from './order/order-history/order-history.component';
import { ChangePasswordModalComponent } from './setting/change-password-modal/change-password-modal.component';
import { ResetPasswordComponent } from './auth/components/reset-password/reset-password.component';
import { OrderDetailsComponent } from './order/order-details/order-details.component';
import { TranslatePipe } from './core/pipes/translate.pipe';
import { SmsCurrencyPipe } from './core/pipes/sms-currency.pipe';
import { DisplayImageComponent } from './core/directives/display-image/display-image.component';
import { CountryComponent } from './country/country.component';
import { AddCountryComponent } from './country/add-country/add-country.component';
import { EditCountryComponent } from './country/edit-country/edit-country.component';
import { UserCustomerComponent } from './user-customer/user-customer.component';
import { UserCustomerDetailsComponent } from './user-customer/user-customer-details/user-customer-details.component';

import { AddUserCustomerModalComponent } from './user-customer/add-user-customer-modal/add-user-customer-modal.component';
import { EditUserCustomerModalComponent } from './user-customer/edit-user-customer-modal/edit-user-customer-modal.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductCategoryComponent } from './product/product-category/product-category.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { EditProductComponent } from './product/edit-product/edit-product.component';
import { MultipleFileUploadComponent } from './core/directives/multiple-file-upload/multiple-file-upload.component';
import { SingleFileUploadComponent } from './core/directives/single-file-upload/single-file-upload.component';
import { ProductCountryFormComponent } from './product/components/product-country-form/product-country-form.component';
import { ProductTranslateFormComponent } from './product/components/product-translate-form/product-translate-form.component';
import { ProductDetailsFormComponent } from './product/components/product-details-form/product-details-form.component';
import { FileValidator } from './core/directives/required-file.directive';
import { FileValueAccessorDirective } from './core/directives/file-value-accessor.directive';
import { EditProductCategoryComponent } from './product/product-category/edit-product-category/edit-product-category.component';
import { AddProductCategoryComponent } from './product/product-category/add-product-category/add-product-category.component';
import { DialogConfirmComponent } from './core/directives/dialog-confirm/dialog-confirm.component';
import { EqualValidator } from './core/directives/equal-validator.directive';
import { SubscriptionComponent } from './subscription/subscription.component';
import { EditPlanComponent } from './subscription/plans/edit-plan/edit-plan.component';
import { AddPlanComponent } from './subscription/plans/add-plan/add-plan.component';
import { PlansComponent } from './subscription/plans/plans.component';
import { PlanCountryFormComponent } from './subscription/components/plan-country-form/plan-country-form.component';
import { PlanDetailsFormComponent } from './subscription/components/plan-details-form/plan-details-form.component';
import { PlanTranslateFormComponent } from './subscription/components/plan-translate-form/plan-translate-form.component';
import { PlanTypesComponent } from './subscription/plan-types/plan-types.component';
import { EditPlanTypeComponent } from './subscription/plan-types/edit-plan-type/edit-plan-type.component';
import { AddPlanTypeComponent } from './subscription/plan-types/add-plan-type/add-plan-type.component';
import { ReportComponent } from './report/report.component';
import { GenerateReportModalComponent } from './report/generate-report-modal/generate-report-modal.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AddPromotionComponent } from './promotion/add-promotion/add-promotion.component';
import { EditPromotionComponent } from './promotion/edit-promotion/edit-promotion.component';
import { SubscribersComponent } from './subscription/subscribers/subscribers.component';
import { SubscriberDetailsComponent } from './subscription/subscribers/subscriber-details/subscriber-details.component';
import { ProductDescriptionDirective } from './core/directives/product-description.directive';
import { JsonPipePipe } from './core/pipes/json-pipe.pipe';
import { PromotionDetailsComponent } from './promotion/promotion-details/promotion-details.component';
import { PromotionTranslateFormComponent } from './promotion/components/promotion-translate-form/promotion-translate-form.component';
import { PromotionAddPlanFormComponent } from './promotion/components/promotion-add-plan-form/promotion-add-plan-form.component';
import { PromotionAddProductFormComponent } from './promotion/components/promotion-add-product-form/promotion-add-product-form.component';
import { PromotionAddUserFormComponent } from './promotion/components/promotion-add-user-form/promotion-add-user-form.component';
import { OrderNoPipe } from './core/pipes/order-no.pipe';
import { ListViewsComponent } from './core/components/list-views/list-views.component';
import { AppPaginationComponent } from './core/components/pagination/pagination.component';
import { DialogResultComponent } from './core/directives/dialog-result/dialog-result.component';
import { SlugifyPipe } from './core/pipes/slugify.pipe';
import { DatetimepickerDirective } from './core/directives/datetimepicker.directive';
import { BulkOrderComponent } from './order/bulk-order/bulk-order.component';
import { ImportFileComponent } from './core/directives/import-file/import-file.component';
import { TruncatePipe } from './core/pipes/truncate.pipe';
import { BulkCreateDetailsComponent } from './order/bulk-create-details/bulk-create-details.component';
import { PluralizeMonthPipe } from './core/pipes/pluralize-month.pipe';
import { PlanTrialProductsComponent } from './subscription/components/plan-trial-products/plan-trial-products.component';
import { UserSampleProductComponent } from './user-sample-product/user-sample-product.component';
import { SubscribersFreeTrialComponent } from './subscription/subscribers/subscribers-free-trial/subscribers-free-trial.component';
import { UserRoleComponent } from './user-role/user-role.component';
import { AddUserRoleComponent } from './user-role/add-user-role/add-user-role.component';
import { EditUserRoleComponent } from './user-role/edit-user-role/edit-user-role.component';
import { UserRoleListComponent } from './user-role/user-role-list/user-role-list.component';
import { PermissionsForRoleComponent } from './user-role/permissions-for-role/permissions-for-role.component';
import { PathComponent } from './permission/path/path.component';
import { AddPathModalComponent } from './permission/path/add-path-modal/add-path-modal.component';
import { EditPathModalComponent } from './permission/path/edit-path-modal/edit-path-modal.component';
import { OrderUploadComponent } from './order/order-upload/order-upload.component';
import { TermComponent } from './term/term.component';
import { AddTermComponent } from './term/add-term/add-term.component';
import { EditTermComponent } from './term/edit-term/edit-term.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AddPrivacyComponent } from './privacy/add-privacy/add-privacy.component';
import { EditPrivacyComponent } from './privacy/edit-privacy/edit-privacy.component';
import { FaqComponent } from './faq/faq.component';
import { AddFaqComponent } from './faq/add-faq/add-faq.component';
import { EditFaqComponent } from './faq/edit-faq/edit-faq.component';
import { TrumbowygEditorDirective } from './core/directives/trumbowyg-editor.directive';
import { AqFormComponent } from './faq/aq-form/aq-form.component';
import { StateFormComponent } from './country/state-form/state-form.component';
import { ProductTypeTranslateFormComponent } from './product/product-category/product-type-translate-form/product-type-translate-form.component';
import { PlanTypeTranslateFormComponent } from './subscription/plan-types/plan-type-translate-form/plan-type-translate-form.component';
import { SupportedLanguageFormComponent } from './country/supported-language-form/supported-language-form.component';
import { EditSubscribersModalComponent } from './subscription/subscribers/subscribers-free-trial/edit-subscribers-modal/edit-subscribers-modal.component';
import { PlanOptionFormComponent } from './subscription/components/plan-option-form/plan-option-form.component';
import { PlanOptionProductFormComponent } from './subscription/components/plan-option-product-form/plan-option-product-form.component';
import { AppcoReportComponent } from './report/appco-report/appco-report.component';
import { MoReportComponent } from './report/mo-report/mo-report.component';
import { ButtonAiComponent } from './core/components/button-ai/button-ai.component';
import { AddSubscribersModalComponent } from './subscription/subscribers/subscribers-free-trial/add-subscribers-modal/add-subscribers-modal.component';
import { ArticleComponent } from './article/article.component';
import { EditArticleComponent } from './article/edit-article/edit-article.component';
import { AddArticleComponent } from './article/add-article/add-article.component';
import { ArticleTypeComponent } from './article/article-type/article-type.component';
import { AddArticleTypeModalComponent } from './article/article-type/add-article-type-modal/add-article-type-modal.component';
import { EditArticleTypeModalComponent } from './article/article-type/edit-article-type-modal/edit-article-type-modal.component';
import { PlanGroupsComponent } from './subscription/plan-groups/plan-groups.component';
import { EditPlanGroupComponent } from './subscription/plan-groups/edit-plan-group/edit-plan-group.component';
import { AddPlanGroupComponent } from './subscription/plan-groups/add-plan-group/add-plan-group.component';
import { PlanGroupTranslateFormComponent } from './subscription/plan-groups/plan-group-translate-form/plan-group-translate-form.component';
import { PromotionAddFreeProductFormComponent } from './promotion/components/promotion-add-free-product-form/promotion-add-free-product-form.component';
import { ProductRelatedFormComponent } from './product/components/product-related-form/product-related-form.component';
import { ExportComponent } from './export/export.component';
import { PlanOptionTranslateFormComponent } from './subscription/components/plan-option-translate-form/plan-option-translate-form.component';
import { MessageComponent } from './cancellation/message/message.component';
import { CancellationComponent } from './cancellation/cancellation.component';
// Add this function
export function initConfig(config: AppConfig) {
  return () => config.load()
}

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    ModalComponent,
    AlertComponent,
    LoadingComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    LeftMenuComponent,
    SettingComponent,
    RouterLinkActiveDirective,
    PaginationComponent,
    SettingEditUserModalComponent,
    UserComponent,
    AddUserModalComponent,
    EditUserModalComponent,
    OrderPendingComponent,
    OrderHistoryComponent,
    ChangePasswordModalComponent,
    ResetPasswordComponent,
    OrderDetailsComponent,
    TranslatePipe,
    SmsCurrencyPipe,
    DisplayImageComponent,
    CountryComponent,
    AddCountryComponent,
    EditCountryComponent,
    UserCustomerComponent,
    AddUserCustomerModalComponent,
    EditUserCustomerModalComponent,
    UserCustomerDetailsComponent,
    ProductComponent,
    ProductListComponent,
    ProductCategoryComponent,
    AddProductComponent,
    EditProductComponent,
    MultipleFileUploadComponent,
    SingleFileUploadComponent,
    ProductCountryFormComponent,
    ProductTranslateFormComponent,
    ProductDetailsFormComponent,
    FileValidator,
    FileValueAccessorDirective,
    EditProductCategoryComponent,
    AddProductCategoryComponent,
    DialogConfirmComponent,
    EqualValidator,
    SubscriptionComponent,
    EditPlanComponent,
    AddPlanComponent,
    PlansComponent,
    PlanCountryFormComponent,
    PlanDetailsFormComponent,
    PlanTranslateFormComponent,
    PlanTypesComponent,
    EditPlanTypeComponent,
    AddPlanTypeComponent,
    ReportComponent,
    GenerateReportModalComponent,
    PromotionComponent,
    AddPromotionComponent,
    EditPromotionComponent,
    SubscribersComponent,
    SubscriberDetailsComponent,
    ProductDescriptionDirective,
    JsonPipePipe,
    PromotionDetailsComponent,
    PromotionTranslateFormComponent,
    PromotionAddPlanFormComponent,
    PromotionAddProductFormComponent,
    PromotionAddUserFormComponent,
    OrderNoPipe,
    ListViewsComponent,
    AppPaginationComponent,
    DialogResultComponent,
    SlugifyPipe,
    DatetimepickerDirective,
    BulkOrderComponent,
    ImportFileComponent,
    TruncatePipe,
    BulkCreateDetailsComponent,
    PluralizeMonthPipe,
    PlanTrialProductsComponent,
    UserSampleProductComponent,
    SubscribersFreeTrialComponent,
    UserRoleComponent,
    AddUserRoleComponent,
    EditUserRoleComponent,
    UserRoleListComponent,
    PermissionsForRoleComponent,
    PathComponent,
    AddPathModalComponent,
    EditPathModalComponent,
    OrderUploadComponent,
    TermComponent,
    AddTermComponent,
    EditTermComponent,
    PrivacyComponent,
    AddPrivacyComponent,
    EditPrivacyComponent,
    FaqComponent,
    AddFaqComponent,
    EditFaqComponent,
    TrumbowygEditorDirective,
    AqFormComponent,
    StateFormComponent,
    ProductTypeTranslateFormComponent,
    PlanTypeTranslateFormComponent,
    SupportedLanguageFormComponent,
    EditSubscribersModalComponent,
    PlanOptionFormComponent,
    PlanOptionProductFormComponent,
    AppcoReportComponent,
    MoReportComponent,
    ButtonAiComponent,
    AddSubscribersModalComponent,
    ArticleComponent,
    EditArticleComponent,
    AddArticleComponent,
    ArticleTypeComponent,
    AddArticleTypeModalComponent,
    EditArticleTypeModalComponent,
    PlanGroupsComponent,
    EditPlanGroupComponent,
    AddPlanGroupComponent,
    PlanGroupTranslateFormComponent,
    PlanOptionTranslateFormComponent,
	ProductRelatedFormComponent,		
    PromotionAddFreeProductFormComponent,
    ExportComponent,
    MessageComponent,
	CancellationComponent
  ],

  imports: [
    HttpModule,
    BrowserModule,
    TranslateModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    MdNativeDateModule,
    ChartsModule,
    RouterModule.forRoot(routes, {
      // useHash: true
    }),
  ],
  providers: [StorageService,
    RequestService,
    UsersService,
    ModalService,
    AuthService,
    HttpService,
    StripeService,
    AlertService,
    LoadingService,
    CountriesService,
    PagerService,
    GlobalService,
    ProductService,
    PlanService,
    PromotionService,
    DialogService,
    RouterService,
    FaqService,
    AppConfig,
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true }
  ],
  entryComponents: [DialogConfirmComponent, DialogResultComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
