import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { CountriesService } from '../../core/services/countries.service';

@Component({
  selector: 'state-form',
  templateUrl: './state-form.component.html',
  styleUrls: ['./state-form.component.scss']
})
export class StateFormComponent implements OnInit {
  @Input() stateForm;
  @Input() submitted;
constructor(
  private _http: HttpService,
  private globalService: GlobalService,
  private appConfig: AppConfig,
  private storage: StorageService,
  private builder: FormBuilder,
  private alertService: AlertService,
  private _router: Router,
  private activateRoute: ActivatedRoute,
  private loadingService: LoadingService,
  private _dialogService: DialogService,
  private location: Location,
  private countriesService: CountriesService,
) {

}

ngOnInit() {
  console.log(this.submitted);
}

setIsDefault(_event, i: number) {
  if (_event.target.checked) {// if set is true
    this.stateForm.controls.forEach((state, index) => {
      if (index !== i) {
        state.controls.isDefault.setValue(false);
      }
    });
  } else { // if set is false
    // this.stateForm.controls
  }
}

addState() {
  // let controlFAQDetails = <FormArray>this.fstateForm.controls['faqTranslate'];
  let prDtsCtrl = this.countriesService.initState();
  // prDtsCtrl.patchValue({});
  this.stateForm.push(prDtsCtrl);
}

removeState(_i: number) {
  this._dialogService.confirm().afterClosed().subscribe((result => {
    if (result) {
      if (this.stateForm.controls[_i].value.id) {
        let pathDelete = `${this.appConfig.config.api.states}/${this.stateForm.controls[_i].value.id}`;
        this._http._delete(`${pathDelete}`).subscribe(
          data => {
            if (data.ok) {
              this.stateForm.removeAt(_i);
            }
          },
          error => {
            this.alertService.error(error);
            console.log('error', error);
          }
        )
      } else {
        this.stateForm.removeAt(_i);
      }
    }
  }));
}

}
