import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../core/services/dialog.service';
import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss', '../../assets/sass/table.scss']
})
export class CountryComponent extends ListViewsComponent {
  public countryList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public countryID: number;
  public showing: string;
  public totalItems: number = 0;
  public options: URLSearchParams;

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    public modalService: ModalService,
    private _dialogService: DialogService,
    routerService: RouterService,
    el: ElementRef, ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
  }

  // get user list
  getData() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.admin_countries}?` + this.options.toString()).subscribe(
      data => {
        this.countryList = data;
        this.totalItems = data.count;
        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  // sort by order_by
  sortByName(type: string) {
    this.orderBy = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["firstName"]');
    } else {
      this.filterby('orderBy', '["firstName_' + type + '"]');
    }

  }

  // add new user
  addCountry() {
    this.modalService.reset('add-country-modal');
    this.modalService.open('add-country-modal');
  }

  // edit User
  editCountry(_countryItem: any) {
    this.countryID = _countryItem.id;
    this.modalService.open('edit-country-modal', _countryItem);
  }

  // delete user
  disableCountry(countryID: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this._http._updatePut(`${this.appConfig.config.api.countries}/${countryID}`, { isActive: false }).subscribe(
          data => {
            if (data) {
              this.loadingService.display(false);
              this.getData();
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  enableCountry(countryID: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this._http._updatePut(`${this.appConfig.config.api.countries}/${countryID}`, { isActive: true }).subscribe(
          data => {
            if (data) {
              this.loadingService.display(false);
              this.getData();
            }
          },
          error => {
            this.loadingService.display(false);
            this.alertService.error(error);
          }
        )
      }
    }));
  }

  // when user edit,add country
  hasChange(status: boolean) {
    if (status)
      this.getData();
  }
  changedatalist(list) {
    this.countryList.rows = list;
  }
}
