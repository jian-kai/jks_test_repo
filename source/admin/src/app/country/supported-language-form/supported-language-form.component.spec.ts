import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportedLanguageFormComponent } from './supported-language-form.component';

describe('SupportedLanguageFormComponent', () => {
  let component: SupportedLanguageFormComponent;
  let fixture: ComponentFixture<SupportedLanguageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportedLanguageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportedLanguageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
