import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

/* Services */
import { LoadingService } from '../../core/services/loading.service';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from '../../../app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { FileValidator } from '../../core/directives/required-file.directive';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { CountriesService } from '../../core/services/countries.service';

@Component({
  selector: 'supported-language-form',
  templateUrl: './supported-language-form.component.html',
  styleUrls: ['./supported-language-form.component.scss']
})
export class SupportedLanguageFormComponent implements OnInit {
  @Input() supportedLanguageForm;
  @Input() submitted;
  public directoryCountries: any;
  public languageCodeList: any;
  constructor(
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private _router: Router,
    private activateRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private _dialogService: DialogService,
    private location: Location,
    private countriesService: CountriesService,
  ) {
    
    this.globalService.getDirectoryCountries().then((data) => {
      this.directoryCountries = data;
      this.languageCodeList = this.directoryCountries.reduce((language, country) => {
         if (!language.includes(country.languageCode)) {
            language.push(country.languageCode)
          }
          return language;
        }, [])
    });
  }

  ngOnInit() {

  }
  
  addLanguage() {
    // let controlFAQDetails = <FormArray>this.fstateForm.controls['faqTranslate'];
    let prDtsCtrl = this.countriesService.initLanguage();
    // prDtsCtrl.patchValue({});
    this.supportedLanguageForm.push(prDtsCtrl);
  }

  removeLanguage(_i: number) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        if (this.supportedLanguageForm.controls[_i].value.id) {
          let pathDelete = `${this.appConfig.config.api.supported_languages}/${this.supportedLanguageForm.controls[_i].value.id}`;
          this._http._delete(`${pathDelete}`).subscribe(
            data => {
              if (data.ok) {
                this.supportedLanguageForm.removeAt(_i);
              }
            },
            error => {
              this.alertService.error(error);
              console.log('error', error);
            }
          )
        } else {
          this.supportedLanguageForm.removeAt(_i);
        }
      }
    }));
  }

  selectDirectoryCountry(i: any) {
    let datas = this.supportedLanguageForm.value;
    let countryObj = this.directoryCountries.filter(country => country.languageCode == datas[i].languageCode);
    if (countryObj) {
      this.supportedLanguageForm['controls'][i].controls["languageName"].setValue(countryObj[0].languageName);
    }
  }

}
