import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';


@Component({
  selector: 'add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public currentUser: any;
  public countryForm: FormGroup;
  public directoryCountries: any;
  public languageCodeList: any;
  public isLoading: boolean = true;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private location: Location, ) {
    this.countryForm = builder.group({
      country: ['', Validators.required],
      code: ['',Validators.required],
      name: ['', Validators.required],
      currencyCode: ['',Validators.required],
      callingCode: ['', Validators.required],
      defaultLang: ['', Validators.required],
      taxRate: ['', Validators.required],
      taxNumber: ['', Validators.required],
      taxName: ['', Validators.required],
      includedTaxToProduct: [],
      currencyDisplay: ['', Validators.required],
      companyName: ['', Validators.required],
      companyRegistrationNumber: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      companyAddress: ['', Validators.required],
      isBaEcommerce: [],
      isBaSubscription: [],
      isBaAlaCarte: [],
      isBaStripe: [],
      isWebEcommerce: [],
      isWebSubscription: [],
      isWebAlaCarte: [],
      isWebStripe: [],
      States: builder.array([], Validators.compose([Validators.required])),
      SupportedLanguages: builder.array([]),
      shippingMinAmount: ['', Validators.required],
      shippingFee: ['', Validators.required],
      shippingTrialFee: ['', Validators.required],
      order: ['']
    });
    this.globalService.getDirectoryCountries().then((data) => {
      this.directoryCountries = data;
      this.languageCodeList = this.directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
  }

  ngOnInit() {
  }

  // do Add User
  doAddCountry() {
    let datas = this.countryForm.value;
    this.loadingService.display(true);
    this._http._create(`${this.appConfig.config.api.countries}`, datas).subscribe(
      data => {
        this.loadingService.display(false);
        this.location.back();
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

  selectDirectoryCountry() {
    let datas = this.countryForm.value;
    let countryObj = this.directoryCountries.filter(country => country.id == datas.country);
    if (datas.country) {
      this.countryForm.controls["name"].setValue(countryObj[0].name);
      this.countryForm.controls["defaultLang"].setValue(countryObj[0].languageCode);
      this.countryForm.controls["code"].setValue(countryObj[0].code);
      this.countryForm.controls["currencyCode"].setValue(countryObj[0].currencyCode);
    }
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }
}
