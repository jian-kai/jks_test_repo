import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CountriesService } from '../../core/services/countries.service';

@Component({
  selector: 'edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.scss']
})
export class EditCountryComponent implements OnInit {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public countryItem: any;
  public currentUser: any;
  public countryForm: FormGroup;
  public directoryCountries: any;
  public languageCodeList: any;
  public isLoading: boolean = true;
  public countryId: number;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private activateRoute: ActivatedRoute,
    private location: Location,
    private countriesService: CountriesService, ) {

    this.countryForm = builder.group({
      code: [''],
      name: ['', Validators.required],
      currencyCode: [''],
      callingCode: ['', Validators.required],
      defaultLang: ['', Validators.required],
      taxRate: ['', Validators.required],
      taxNumber: ['', Validators.required],
      taxName: ['', Validators.required],
      includedTaxToProduct: [],
      currencyDisplay: ['', Validators.required],
      companyName: ['', Validators.required],
      companyRegistrationNumber: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      companyAddress: ['', Validators.required],
      isBaEcommerce: [],
      isBaSubscription: [],
      isBaAlaCarte: [],
      isBaStripe: [],
      isWebEcommerce: [],
      isWebSubscription: [],
      isWebAlaCarte: [],
      isWebStripe: [],
      States: builder.array([], Validators.compose([Validators.required])),
      SupportedLanguages: builder.array([]),
      shippingMinAmount: ['', Validators.required],
      shippingFee: ['', Validators.required],
      shippingTrialFee: ['', Validators.required],
      order: ['']
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });

    this.activateRoute.params.subscribe(params => {
      this.countryId = +params['id']// (+) converts string 'id' to a number
    });

    this.globalService.getDirectoryCountries().then((data) => {
      let directoryCountries: any = data;
      this.languageCodeList = directoryCountries.reduce((language, country) => {
        if (!language.includes(country.languageCode)) {
          language.push(country.languageCode)
        }
        return language;
      }, [])
    });
  }

  ngOnInit() {
    this.getInfo();
  }

  // get user info by ID
  getInfo() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.admin_countries}/${this.countryId}`).subscribe(
      data => {
        this.countryForm.patchValue(data.data);
        data.data.States.forEach(element => {
          let controlState = <FormArray>this.countryForm.controls['States'];
          let prDtsCtrl = this.countriesService.initState();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlState.push(prDtsCtrl);
        });
        data.data.SupportedLanguages.forEach(element => {
          let controlSupportedLanguages = <FormArray>this.countryForm.controls['SupportedLanguages'];
          let prDtsCtrl = this.countriesService.initLanguage();
          prDtsCtrl.addControl("id", new FormControl("", Validators.required));
          prDtsCtrl.patchValue(element);
          controlSupportedLanguages.push(prDtsCtrl);
        });
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error)
      }
    )
  }

  setInitData(_countryItem) {
    this.countryItem = _countryItem;
    this.countryForm = this.globalService.setValueFormBuilder(this.countryForm, this.countryItem);
    this.globalService.getDirectoryCountries().then((data) => {
      this.directoryCountries = data;
      this.languageCodeList = this.directoryCountries.reduce((language, country) => { if (!language.includes(country.languageCode)) { language.push(country.languageCode) } return language; }, [])
    });
  }

  // do Edit Country
  doEditCountry() {
    if(this.countryForm.valid) {
      let datas = this.countryForm.value;
      this.loadingService.display(true);
      this._http._updatePut(`${this.appConfig.config.api.countries}/${this.countryId}`, datas).subscribe(
        data => {
          this.loadingService.display(false);
          this.location.back();
        },
        error => {
          this.loadingService.display(false);
          this.alertService.error(error);
        }
      )
    }
  }

  // go to the list screen
  gotoList() {
    this.location.back();
  }

}
