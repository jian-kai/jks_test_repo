import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPathModalComponent } from './add-path-modal.component';

describe('AddPathModalComponent', () => {
  let component: AddPathModalComponent;
  let fixture: ComponentFixture<AddPathModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPathModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPathModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
