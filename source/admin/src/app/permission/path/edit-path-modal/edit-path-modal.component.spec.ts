import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPathModalComponent } from './edit-path-modal.component';

describe('EditPathModalComponent', () => {
  let component: EditPathModalComponent;
  let fixture: ComponentFixture<EditPathModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPathModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPathModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
