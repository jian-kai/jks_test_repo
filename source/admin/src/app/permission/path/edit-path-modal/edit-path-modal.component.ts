import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';

@Component({
  selector: 'app-edit-path-modal',
  templateUrl: './edit-path-modal.component.html',
  styleUrls: ['./edit-path-modal.component.scss']
})
export class EditPathModalComponent extends ModalComponent {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public currentUser: any;
  public pathForm: FormGroup;
  public pathItem: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'app-edit-path-modal';
    this.pathForm = builder.group({
      pathValue: ['', Validators.required],
    });
  }

  // do Add User
  doAddCountry() {
    let datas = this.pathForm.value;
    this._http._create(`${this.appConfig.config.api.countries}`, datas).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  setInitData(_pathItem) {
    this.pathItem = _pathItem;
    console.log('this.pathItem', this.pathItem)
    this.pathForm.patchValue(this.pathItem);
  }

  // do Add User
  doEditPath() {
    let datas = this.pathForm.value;
    this._http._updatePut(`${this.appConfig.config.api.path}/${this.pathItem.id}`, datas).subscribe(
      data => {
        this.hasChange.emit(true);
        this.close();
      },
      error => {
        this.alertService.error(error);
      }
    )
  }


}

