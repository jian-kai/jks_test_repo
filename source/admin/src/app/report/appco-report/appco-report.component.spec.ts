import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppcoReportComponent } from './appco-report.component';

describe('AppcoReportComponent', () => {
  let component: AppcoReportComponent;
  let fixture: ComponentFixture<AppcoReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppcoReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppcoReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
