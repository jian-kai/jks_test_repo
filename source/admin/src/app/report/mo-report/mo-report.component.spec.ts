import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoReportComponent } from './mo-report.component';

describe('MoReportComponent', () => {
  let component: MoReportComponent;
  let fixture: ComponentFixture<MoReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
