import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';

@Component({
  selector: 'generate-report-modal',
  templateUrl: './generate-report-modal.component.html',
  styleUrls: ['./generate-report-modal.component.scss']
})
export class GenerateReportModalComponent extends ModalComponent {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public dateForm: FormGroup;
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'generate-report-modal';
    this.dateForm = builder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });

  }

  doCreateReport() {
    //todo
  }

}
