import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { LoadingService } from '../core/services/loading.service';
import { PagerService } from '../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../core/services/dialog.service';
import { MdDatepickerModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ListViewsComponent } from '../core/components/list-views/list-views.component';
import { RouterService } from '../core/helpers/router.service';


@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss']
})
export class ExportComponent extends ListViewsComponent {

  public exportList: any;
  public userInfo: any;
  public isLoading: boolean = true;
  public orderBy: string = "ASC";
  public showing: string;
  public fromDate: any;
  public toDate: any;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public orderByDate: string;
  public countries: any;
  public timer = null;

  constructor(private _http: HttpService,
    routerService: RouterService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    public modalService: ModalService, private _dialogService: DialogService) {
    super(location, routerService, el);
    if (!this.storage.getCurrentUserItem() || !this.storage.getCurrentUserItem().id) {
      console.log('User is not exist');
      return;
    }
  }

  getData() {
    this.isLoading = true;
    this.userInfo = this.storage.getCurrentUserItem();
    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }
    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    } else {
      this.fromDate =  new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 1);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }

    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    } else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }
    if (this.options && !this.options.get('orderBy')) {
      this.options.set('orderBy', '["createdAt_DESC"]');
      this.orderByDate = 'DESC';
    }
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.exports}?`.replace('[adminId]', this.userInfo.id) + this.options.toString()).subscribe(
      data => {
        this.exportList = data;
        this.loadingService.display(false);
        this.isLoading = false;
        console.log(this.exportList);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
        this.isLoading = false;
      }
    )
  }
  changeFromDate(_fromDate) {
    if (_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
  }

  changeToDate(_toDate) {
    if (_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    console.log(_type);
    this.getData();
  }

  sortByDate(type: string) {
    this.orderByDate = type;
    if (type === "ASC") {
      this.filterby('orderBy', '["createdAt"]');
    } else {
      this.filterby('orderBy', '["createdAt_DESC"]');
    }
  }

  // customize sort by
  customizeFilterBy(byname: string, _event: any) {
    clearTimeout(this.timer);
    this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)
  }

  downloadFile(_id: any) {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.download_file}/${_id}`).subscribe(
      data => {
        if(data) {
          let dwldLink = document.createElement('a');
          let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
          if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute('target', '_blank');
          }
          dwldLink.setAttribute('href', data.url);
          dwldLink.style.visibility = 'hidden';
          document.body.appendChild(dwldLink);
          dwldLink.click();
          document.body.removeChild(dwldLink);
        }
  
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error(error);
      }
    )
  }

}
