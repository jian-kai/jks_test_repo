import { Component, ElementRef, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { PagerService } from '../../core/services/pager-service.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { DialogService } from '../../core/services/dialog.service';
import { ListViewsComponent } from '../../core/components/list-views/list-views.component';
import { RouterService } from '../../core/helpers/router.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss',
    '../../../assets/sass/subscription.scss',
    '../../../assets/sass/table.scss']
})
export class MessageComponent extends ListViewsComponent {
  public UserCancellationList: any;
  public MessageGetList: any;
  public MessageList: any;
  public isLoading: boolean = true;
  public defaultOptions: any = {};
  public paginationOptions: any = {};
  public orderBy: string = "ASC";
  public userID: number;
  public showing: string;
  public countries: any;
  public productTypes: any;
  public apiUrl: string = `${this.appConfig.config.api.subscribers_users}`;
  public totalItems: number = 0;
  public options: URLSearchParams;
  public fromDate: any;
  public toDate: any;
  public cancellationSub: any;
  public timer = null;
  messageList: Array<any> = [];
  orderList: Array<any> = [];
  userList: Array<any> = [];
  subcriptionList: Array<any> = [];

  constructor(private _http: HttpService,
    public globalService: GlobalService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    private pagerService: PagerService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    location: Location,
    private _router: Router,
    el: ElementRef,
    routerService: RouterService, ) {
    super(location, routerService, el);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.getInitDate();
  }

  // get user list
  getData() {
    if (this.options.get('fromDate')) {
      this.fromDate = new Date(this.options.get('fromDate'));
    } else {
      this.fromDate = new Date();
      this.fromDate.setDate(this.fromDate.getDate() - 30);
      this.options.set('fromDate', this.globalService.formatDate(this.fromDate));
    }

    if (this.options.get('toDate')) {
      this.toDate = new Date(this.options.get('toDate'));
    } else {
      this.toDate = new Date();
      this.options.set('toDate', this.globalService.formatDate(this.toDate));
    }

    if (!this.countries) {
      this.globalService.getCountryList().then(data => {
        this.countries = data;
      });
    }

    this.apiUrl = `${this.appConfig.config.api.message}`;
    let params = this.globalService._URLSearchParams(this.options);

    this.options.set('orderBy', `createdAt_DESC`);

    this.loadingService.display(true);



    this._http._getList(`${this.apiUrl}?` + this.options.toString()).subscribe(
      data => {
        this.UserCancellationList = data;

        this.loadingService.display(false);
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  getInitDate(){
    this.apiUrl = `${this.appConfig.config.api.message}`;
    this._http._getList(`${this.apiUrl}/MessageGetList`).subscribe(
      data1 => {
        this.MessageGetList = data1;
      },
      error => {
        this.alertService.error(error);
      }
    )
    this._http._getList(`${this.apiUrl}/total`).subscribe(
      data2 => {
        this.totalItems = data2.count;
        console.log('this.totalItems', this.totalItems)
    
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

  getMessageTranslates(messageid,otherreason){

    let splitted = messageid;
  let messageword="";
  if(splitted && otherreason)
  {
  splitted = splitted.split(",");
  for (var item1=0;item1<splitted.length;item1++)
  {
   this.MessageGetList.forEach((item2) => {
      if(splitted[item1]== item2.id){
      messageword +="<li>"+item2.content + "</li>";
      }
    });
  }
  messageword +="<li>Other : "+otherreason+"</li>";
}
else if(splitted)
{
  splitted = splitted.split(",");
  for (var item1=0;item1<splitted.length;item1++)
  {
   this.MessageGetList.forEach((item2) => {
      if(splitted[item1]== item2.id){
      messageword +="<li>"+item2.content + "</li>";
      }
    });
  }
}
else if(otherreason)
{

      messageword ="<li>Other : "+otherreason+"</li>";
  
}
else
{
  messageword +="-"
}

   // console.log("taestst" + messageword)
return messageword;
  }


 // customize sort by
 customizeFilterBy(byname: string, _event: any) {
  console.log("customizeFilterBy = "+ _event)
  clearTimeout(this.timer);
  this.timer = setTimeout(_ => { this.filterby(byname, _event.target.value) }, 500)
}


  // sort by order_by
  sortByName(_type: string) {
    this.orderBy = _type;
    if (_type === "ASC") {
      this.filterby('orderBy', '["firstName"]');
    } else {
      this.filterby('orderBy', '["firstName_DESC"]');
    }
  }

  changeFromDate(_fromDate) {
    console.log("changeFromDate = "+ _fromDate)
    if (_fromDate) {
      this.filterby('fromDate', this.globalService.formatDate(_fromDate));
    } else {
      this.filterby('fromDate', 'all');
    }
    this.fromDate = new Date(_fromDate);
    // this.getData();
  }

  changeToDate(_toDate) {
    if (_toDate) {
      this.filterby('toDate', this.globalService.formatDate(_toDate));
    } else {
      this.filterby('toDate', 'all');
    }
    this.toDate = new Date(_toDate);
  }

  // // delete product 
  // deleteProduct(_id: number) {
  //   this._dialogService.confirm().afterClosed().subscribe((result => {
  //     if (result) {
  //       this.updateProductStatus(_id, 'removed');
  //     }
  //   }));
  // }

  // // active product 
  // activeProduct(_id: number) {
  //   this._dialogService.confirm().afterClosed().subscribe((result => {
  //     if (result) {
  //       this.updateProductStatus(_id, 'active');
  //     }
  //   }));
  // }


  // // update Product status: 'removed' | 'active'
  // updateProductStatus(_id: number, _status: string) {
  //   this.loadingService.display(true);
  //   let formData: FormData = new FormData();
  //   formData.append('status', _status);
  //   this._http._updatePut(`${this.appConfig.config.api.products}/${_id}`, formData, "form-data").subscribe(
  //     data => {
  //       if (data.ok) {
  //         this.loadingService.display(false);
  //         this.getData();
  //       }
  //     },
  //     error => {
  //       this.loadingService.display(false);
  //       this.alertService.error(error);
  //     }
  //   )
  // }

  // // download CSV file
  // downloadCSV() {
  //   this.loadingService.display(true);
  //   this._http._getDetailCsv(`${this.apiUrl}/download?` + this.options.toString()).subscribe(
  //     data => {
  //       let blob = new Blob(['\ufeff' + data], { type: 'text/csv;charset=utf-8;' });
  //       let dwldLink = document.createElement('a');
  //       let url = URL.createObjectURL(blob);
  //       let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
  //       if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
  //         dwldLink.setAttribute('target', '_blank');
  //       }
  //       dwldLink.setAttribute('href', url);
  //       dwldLink.setAttribute('download', `Subscribers for ${this.globalService.formatDate(this.fromDate)} to ${this.globalService.formatDate(this.toDate)}.csv`);
  //       dwldLink.style.visibility = 'hidden';
  //       document.body.appendChild(dwldLink);
  //       dwldLink.click();
  //       document.body.removeChild(dwldLink);

  //       this.loadingService.display(false);
  //     },
  //     error => {
  //       this.loadingService.display(false);
  //       this.alertService.error(error);
  //     }
  //   )
  // }

  // cancel(item) {
  //   this.cancellationSub = item;
  //   this.modalService.open('edit-subscribers-modal', item.id);
  // }


  // cancel(item) {
  //   let cancleMsg = 'Are you sure?';
  //   this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
  //     if (result) {
  //       this._http._delete(`${this.apiUrl}/${item.id}`).subscribe(
  //         data => {
  //           // this.loadingService.display(false);
  //           this._dialogService.result('Please be informed your account is not eligible for Shave plan Free Trial').afterClosed().subscribe((result => {
  //             this.getData();
  //           }));
  //         },
  //         error => {
  //           // this.resError = {
  //           //   type: 'error',
  //           //   message: JSON.parse(error).message
  //           // };
  //           let message = typeof error === 'string' ? error : JSON.parse(error).message
  //           this.loadingService.display(false);
  //           // this.dialogService.result(message).afterClosed().subscribe((result => {
  //           //   this.loadingService.display(false);
  //           // }));
  //           console.log(error);
  //           this.alertService.error(error);
  //         }
  //       );
  //     }
  //   }));
  // }

  // hasChangeSubscriber(status: boolean) {
  //   if (status && this.cancellationSub.status !== 'Canceled') {
  //     let cancleMsg = 'Are you sure?';
  //     this._dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
  //       if (result) {
  //         this._http._delete(`${this.appConfig.config.api.subscribers_free_trial}/${this.cancellationSub.id}`).subscribe(
  //           data => {
  //             // this.loadingService.display(false);
  //             this._dialogService.result('Subscriber cancellation request was successful').afterClosed().subscribe((result => {
  //               this.getData();
  //             }));
  //           },
  //           error => {
  //             let message = typeof error === 'string' ? error : JSON.parse(error).message
  //             this.loadingService.display(false);
  //             this._dialogService.result(message).afterClosed().subscribe((result => {
  //               this.loadingService.display(false);
  //             }));
  //             console.log(error);
  //             this.alertService.error(error);
  //           }
  //         );
  //       }
  //     }));
  //   }
  // }

}

