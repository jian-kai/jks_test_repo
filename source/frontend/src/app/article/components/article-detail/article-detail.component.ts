import { Component, OnInit, Input } from '@angular/core';
import {URLSearchParams} from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import {LoadingService} from '../../../core/services/loading.service';
import {GlobalService} from '../../../core/services/global.service';
import $ from "jquery";

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit {
  public articleTypes: any;
  public articleId: number;
  public setArticleType: number;
  public articleInfo: any;
  public relatedArticles: any;
  public features: any;
  public isloading : boolean = true;
  public apiURL: string;
  constructor(private httpService: HttpService,
    public storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private route: ActivatedRoute,
    public appConfig: AppConfig,
  ) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.articleId = +params['id']// (+) converts string 'id' to a number
    });
    this.getArticleTypes();
    this.getArticleDetail();
    this.getFeatures();

    // set if data loading status
    this.loadingService.status.subscribe((value : boolean) => {
      this.isloading = value;
    })
  }

  // get article type list
  getArticleTypes() {
    this.httpService._getList(this.appConfig.config.api.article_types).subscribe(
      data => {
        this.articleTypes = data;
        this.setArticleType = data[0].id;
      },
      error => {
        // console.log(error.error);
      }
    )
  }

  // get article detail
  getArticleDetail() {
    this.loadingService.display(true);
    this.httpService._getDetail(`${this.appConfig.config.api.articles}/${this.articleId}`).subscribe(
      data => {
        this.articleInfo = data;
        this.getRelatedArticles();
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    );
  }

  // get related articles
  getRelatedArticles() {
    let options = {
      ArticleTypeId: this.articleInfo.ArticleType.id,
      limit: 3
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles}?` + params.toString()).subscribe(
      data => {
        this.relatedArticles = data;
        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )
  }

  // get features article
  getFeatures() {
    let options = {
      orderBy: '["views_DESC"]',
      limit: 1,
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles_featured}?` + params.toString()).subscribe(
      data => {
        this.features = data;
      },
      error => {
        // console.log(error.error);
      }
    )
  }
}
