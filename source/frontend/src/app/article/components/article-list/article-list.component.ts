import { Component, OnInit, Input } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CountriesService } from '../../../core/services/countries.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import $ from "jquery";

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {
  public setArticleType: number;
  public articleList: any;
  public articlesSlider = [];
  public articleTypes: any;
  public latestArticles: any;
  public featuredArticles: any;
  public trendingArticle : any;
  public isloading : boolean = true;
  public articleSliderOptions = {
    displayList: false,
    displayControls: true
  }
  public apiURL: string;
  constructor(private httpService: HttpService,
    public storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
		private route: ActivatedRoute
  ) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngOnInit() {

    this._init();

    // set if data loading status
    this.loadingService.status.subscribe((value : boolean) => {
      this.isloading = value;
    })

  }

  // init
  _init() {
		// console.log();
		this.route.queryParams
      .filter(params => params.type_id)
      .subscribe(params => {
				this.setArticleType = params.type_id;
				if(this.setArticleType != null) {
			    this.reloadPage();
				}
        // console.log(this.setArticleType);
      });
    // get article type list
    this.loadingService.display(true);
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    });
  }

  reloadPage() {
    this.getArticles();
    this.getLatestArticles();
    this.getTrendingArticle();
    this.getFeatures();
  }

  getArticles() {
    this.httpService._getList(this.appConfig.config.api.article_types).subscribe(
      data => {
        this.articleTypes = data;
        this.setArticleType = data[0].id;
        this.getLastestArticles();
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error);
      }
    )
  }

  // get articles latest
  getLatestArticles() {
    let options = {
      ArticleTypeId: this.setArticleType,
      limit: 3,
      offset: 2
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles}?` + params.toString()).subscribe(
      data => {
        this.latestArticles = data;
      },
      error => {
        // console.log(error.error);
      }
    )
  }

  // get articles lastest
  getLastestArticles() {
    let options = {
      ArticleTypeId: this.setArticleType,
      limit: 3,
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles}?` + params.toString()).subscribe(
      data => {
        this.articleList = data;
        data.forEach(article => {
          this.articlesSlider.push({
            id: article.id,
            title: article.title,
            imageUrl: article.bannerUrl
          });
        });
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

  // get trending article
  getTrendingArticle() {
    let options = {
      orderBy: '["views_DESC"]',
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles}?` + params.toString()).subscribe(
      data => {
        this.trendingArticle = data;
      },
      error => {
        // console.log(error.error);
      }
    )
  }

	getFeatures() {
    let options = {
      orderBy: '["views_DESC"]',
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.articles_featured}?` + params.toString()).subscribe(
      data => {
        this.featuredArticles = data;
      },
      error => {
        // console.log(error.error);
      }
    )
  }

  //change Article Type
  changeArticleType(articleType) {
    this.setArticleType = articleType;
    this._init();
  }


}
