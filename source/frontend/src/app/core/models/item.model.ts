export interface Item {
	id: number,
	product?: any,
	plan?: any,
	qty?: number,
	isRemove?: boolean,
	isFree?: boolean
	// id: number,
	// description?: string,
	// price: number,
	// name: string,
	// image_src: string,
	// quantity?: number
	// sku?: string,
	// rating?: number,
	// ProductTypeId: number,
	// productTranslate?: any,
	// productCountry?: any,
	// ProductType?: any,
	// productReviews?: any,
	// productImages?: any
}