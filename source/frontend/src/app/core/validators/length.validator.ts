import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormControl } from '@angular/forms';

export function lengthValidator (prms : any = {}): ValidatorFn {
  return (control: FormControl): {[key: string]: boolean} => {
    if(Validators.required(control)) {
      return null;
    }

    let val: string = control.value;
    let error = {
      minLength: true,
      maxLength: true,
    };

    // if(!isNaN(prms.min)) {
    //   error.minlength = false;
    // }

    // if(!isNaN(prms.max)) {
    //   error.maxlength = false;
    // }

    if ((prms.min) && val.length < prms.min) {
      error.minLength = true;
    } else {
      error.minLength = false;
    }

    if ((prms.max) && val.length > prms.max) {
      error.maxLength = true;
    } else {
      error.maxLength = false;
    }

    if (!error.minLength && !error.maxLength) {
      return null;
    }

    return error;

    // if(isNaN(val) || /\D/.test(val.toString())) {

    //   return {"validateNumber": true};
    // } else if(!isNaN(prms.min) && !isNaN(prms.max)) {

    //   return val < prms.min || val > prms.max ? {"validateNumber": true} : null;
    // } else if(!isNaN(prms.min)) {

    //   return val < prms.min ? {"validateNumber": true} : null;
    // } else if(!isNaN(prms.max)) {

    //   return val > prms.max ? {"validateNumber": true} : null;
    // } else {

    //   return null;
    // }
  };
}