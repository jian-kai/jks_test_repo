import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../core/services/storage.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { LoadingService } from '../../../core/services/loading.service';
import { CountriesService } from '../../../core/services/countries.service';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit {
  public termContent: any;

  constructor(
    public storage: StorageService,
    public _http: HttpService,
    public appConfig: AppConfig,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
  ) { }

  ngOnInit() {
    this.getTerm();
    this.countriesService.language.subscribe(country => {
      this.getTerm();
    });
  }

  getTerm() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.terms}?langCode=${this.storage.getLanguageCode()}`).subscribe(
      data => {
        if(data.data) {
          this.termContent = data.data.content;
        }
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

}
