import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { TermsConditionsComponent } from './terms-conditions.component';
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: TermsConditionsComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule
  ],
  declarations: [
    TermsConditionsComponent,
  ]
})
export class TermsConditionsModule { }
