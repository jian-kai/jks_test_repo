import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CountriesService } from '../../../core/services/countries.service'
import { CartService } from '../../../core/services/cart.service';
import { Item } from '../../../core/models/item.model';
import { ModalService } from '../../../core/services/modal.service';
import { AlertService } from '../../../core/services/alert.service';
import $ from "jquery";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss',
  ]
})
export class HomeComponent implements OnInit {
  awesomeKitPrice: any;
  public shippingFee: any;
  public featuredProducts: any;
  public articles: any;
  public hasLoadedFeaturedProducts: boolean = false;
  public apiURL: String;
  public cartRules: any;
  public LatestOrderId: any;
  stopVideo: boolean = false;
  videoRunning: boolean = false;
  constructor(private httpService: HttpService,
    public storage: StorageService,
    public modalService: ModalService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private router: Router,
    private route: ActivatedRoute,
    public appConfig: AppConfig,
    private alertService: AlertService,
    public cartService: CartService,
    private countriesService: CountriesService
  ) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngOnInit() {
    // get all products
    this.hasLoadedFeaturedProducts = false;
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    });

    let countryCode = this.router.url;
    // this.route.params.subscribe(params => {
    //   countryCode = params['countrycode'];
    // });
    if (countryCode) {
      if (countryCode === '/my') {
        countryCode = 'MYS';
      }
      if (countryCode === '/sg') {
        countryCode = 'SGP';
      }
      this.getCountries().then((res: any) => {
        let countries = res || [];
        let country = countries.find(value => value.code === countryCode);
        if (country) {
          this.countriesService.changeCountry(country);
        }
      })

    }

    //get cart rule
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
      }
    )

    let currentUser = this.storage.getCurrentUserItem();
    if (currentUser) {
      let userId = currentUser.id;
      let userEmail = currentUser.email;

      let check = this.storage.getCheckoutData();
      // let past_transaction = this.storage.getGtmTransactionID();
      console.log("================================= Redirect to Thank You  =============================");
      // console.log('Checkout Data : '+check);
      // console.log('Past Transaction : '+past_transaction);
      if (check) {

        this.storage.clearCheckoutData();
        this.storage.clearCart();
        let options = {
          userId: userId,
          userEmail: userEmail
        };
        // console.log(`Get userId === ${userId}`);
        // console.log(`Get userEmail === ${userEmail}`);
        let params = this.globalService._URLSearchParams(options);
        // this.userOrders = []; 

        this.httpService._getDetail(`${this.appConfig.config.api.user_order_list}?` + params.toString()).subscribe(
          data1 => {
            this.LatestOrderId = data1.id;


            let startTime = new Date(data1.createdAt);
            let endTime = new Date();
            let difference1 = endTime.getTime() - startTime.getTime(); // This will give difference in milliseconds
            var resultInMinutes = Math.round(difference1 / 60000);
            if (resultInMinutes <= 5) {
              setTimeout(this.router.navigate([`/thankyou/${this.LatestOrderId}`]), 3000);
            }

            //  console.log(`Get Current User Order test === ${JSON.stringify(data1)}`);
          },
          error => {
            this.alertService.error(error);
          }
        )


      }
    }
  }

  getCountries() {
    return new Promise((resolved, reject) => {
      this.httpService._getList(this.appConfig.config.api.countries)
        .subscribe(
          res => {
            resolved(res);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  emailLeadPopout() {
    if (this.storage.getLanguageCode() === 'KO' || this.storage.getCountry().code.toUpperCase() === 'KOR' || this.storage.getCountry().code.toUpperCase() === 'HKG') {
      this.modalService.open('collect-email-modal');
    }
  }

  reloadPage() {
    $("html").removeClass("video-is-visible");
    $(".wistia_embed").removeAttr("src", "");

    this.getFeaturedProducts();
    this.getAwesomeKitProduct();

    this.shippingFee = `${this.storage.getCountry().currencyDisplay}${this.appConfig.getConfig('shippingFeeConfig').trialFee}`;
  }

  getAwesomeKitProduct() {
    let options = {
      ProductTypeId: 1,
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        if (data && data.length > 0) {
          this.awesomeKitPrice = `${data[0].productCountry.Country.currencyDisplay}${data[0].productCountry.sellPrice}`;
        }
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  getFeaturedProducts() {
    let options = {
      isFeatured: true,
      limit: 3,
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.featuredProducts = data;
        this.hasLoadedFeaturedProducts = true;
      },
      error => {
        this.hasLoadedFeaturedProducts = true;
        // console.log(error.error)
      }
    )
  }

  watchVideo() {
    if (this.stopVideo) {
      this.stopVideo = false;
    } else {
      $("html").addClass("video-is-visible");
      let src = $(".wistia_embed").data('src');
      $(".wistia_embed").attr("src", src);
    }
  }
  closeVideo() {
    $("html").removeClass("video-is-visible");
    $(".wistia_embed").removeAttr("src", "");

    this.stopVideo = true;
  }

  // add item to cart
  addToCart(_item) {
    // // console.log('item', item);
    let item: Item = {
      id: _item.id,
      product: _item
    }
    item.qty = 1;
    this.cartService.applyAddItemEvent();
    this.cartService.addItem(item);
    this.cartService.checkFreeProduct(this.cartRules);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
    // this.modalService.open('add-cart-modal', this.item);
  }

}
