import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { UsersService } from '../../../auth/services/users.service';
import { LoadingService } from '../../../core/services/loading.service';
import { StorageService } from '../../../core/services/storage.service';
import { CartService} from '../../../core/services/cart.service';
import { Item } from '../../../core/models/item.model';
import { AppConfig } from 'app/config/app.config';
import { CountriesService } from '../../../core/services/countries.service';
import { HttpService } from '../../../core/services/http.service';
import { DomSanitizer } from '@angular/platform-browser'

// @Pipe({ name: 'safeHtml'})
// export class SafeHtmlPipe implements PipeTransform  {
//   constructor(private sanitized: DomSanitizer) {}
//   transform(value) {
//     return this.sanitized.bypassSecurityTrustHtml(value);
//   }
// }

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
  // providers: [SafeHtmlPipe]
})
export class PrivacyPolicyComponent implements OnInit {
  public privacyContent : any;
  constructor(
    private translate: TranslateService,
    private userService: UsersService,
    private loadingService: LoadingService,
    public storage: StorageService,
    public cartService: CartService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    private router: Router,
    private _http: HttpService,) { }

  ngOnInit() {
    this.getPrivacy();
    this.countriesService.language.subscribe(country => {
      this.getPrivacy();
    });
  }

  getPrivacy() {
    this.loadingService.display(true);
    this._http._getList(`${this.appConfig.config.api.privacy}?langCode=${this.storage.getLanguageCode()}`).subscribe(
      data => {
        // console.log('data', data);
        if(data.data) {
          this.privacyContent = data.data.content;
        }
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

}
