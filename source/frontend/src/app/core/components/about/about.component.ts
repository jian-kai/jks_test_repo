import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  private popup;
  constructor() { }

  ngOnInit() {
    window.addEventListener("message", this.receiveMessage.bind(this), false);
  }

  receiveMessage(event)
  {
    // handle message from ipay88
    if (event.origin === "http://smartshave.bjdev.net") {
      // console.log(JSON.parse(event.data));
      // this.popup.close();
    }
  }

  checkoutClick() {
    this.popup = window.open('http://smartshave.bjdev.net/payment-gateway/ipay88', "_blank")
  }
}
