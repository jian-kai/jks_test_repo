import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../../auth/services/users.service';
import { NotificationService } from '../../../../services/notification.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-notification-remind-active-email',
  templateUrl: './notification-remind-active-email.component.html',
  styleUrls: ['./notification-remind-active-email.component.scss']
})
export class NotificationRemindActiveEmailComponent implements OnInit {
  public notiMsg: string = null;
  public i18nMsg : any;
  public email: string = ""

  constructor(private notificationService: NotificationService,
  private usersService: UsersService,
    private translate: TranslateService) {
    this.translate.get('msg').subscribe(res => {
      this.i18nMsg = res.validation;
    });
  }

  ngOnInit() {

    this.notificationService.notiRemindUser.subscribe(data=> {
      if (data && data.status) {
        this.notiMsg = 'test';
        this.email = data.email;
      } else {
        this.notiMsg = null;
      }
    })
  }

  resendActiveMail() {
    this.usersService.applyResendActiveEmail(this.email);
    this.notificationService.notiRemindUser.emit();
  }

}
