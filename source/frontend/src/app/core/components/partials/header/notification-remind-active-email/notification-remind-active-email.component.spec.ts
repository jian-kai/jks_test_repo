import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationRemindActiveEmailComponent } from './notification-remind-active-email.component';

describe('NotificationRemindActiveEmailComponent', () => {
  let component: NotificationRemindActiveEmailComponent;
  let fixture: ComponentFixture<NotificationRemindActiveEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationRemindActiveEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationRemindActiveEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
