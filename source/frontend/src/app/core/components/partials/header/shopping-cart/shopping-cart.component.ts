import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { ModalService } from '../../../../services/modal.service';
import { UsersService } from '../../../../../auth/services/users.service';
import { LoadingService } from '../../../../../core/services/loading.service';
import { StorageService } from '../../../../../core/services/storage.service';
import { CartService} from '../../../../../core/services/cart.service';
import { Item } from '../../../../../core/models/item.model';
import { AppConfig } from 'app/config/app.config';
import { CountriesService } from '../../../../services/countries.service';
import { HttpService } from '../../../../services/http.service';


@Component({
  selector: '[id=app-shopping-cart]',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  isLoggedIn: boolean;
  currentCountry: Object;
  public totalItemCart: number = 0;
  public cartItems: Array<any>;
  private apiURL: string;
  public cartRules: any;
  public receipt: any;
  constructor(private modalService: ModalService, private translate: TranslateService,
    private userService: UsersService,
    private loadingService: LoadingService,
    public storage: StorageService,
    public cartService: CartService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    private router: Router,
    private httpService: HttpService,
  ) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngOnInit() {

    this.cartItems = this.cartService.getCart();
    this.cartService.hasUpdate.subscribe(hasUpdate => {
      this.cartItems = this.cartService.getCart();
      this.receipt = this.cartService.getReceipt(this.cartRules);
    });

    // get currenct country
    this.currentCountry = this.storage.getCountry();
    
    // handle country change event
    this.countriesService.country.subscribe(country => {
      this.currentCountry = country;
    });

    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        this.receipt = this.cartService.getReceipt(this.cartRules);
      }
    )
  }

  gotoCartDetail() {
    this.router.navigate(['/cart']);
  }

  removeItem(item, event) {
    event.preventDefault();
    this.cartService.removeItem(item);
    this.cartService.checkFreeProduct(this.cartRules);
  }
}
