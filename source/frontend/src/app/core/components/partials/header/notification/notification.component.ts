import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../../services/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  public notiMsg: string = null;

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.notiObject.subscribe(data=> {
      if (data && data.msg !== '') {
        this.notiMsg = data.msg;
      } else {
        this.notiMsg = null;
      }
    })
  }

}
