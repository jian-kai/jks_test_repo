import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { ModalService } from '../../../../services/modal.service';
import { UsersService } from '../../../../../auth/services/users.service';
import { LoadingService } from '../../../../../core/services/loading.service';
import { StorageService } from '../../../../../core/services/storage.service';
import { CartService } from '../../../../../core/services/cart.service';
import { AppConfig } from '../../../../../config/app.config';
import { HttpService } from '../../../../services/http.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CountriesService } from '../../../../services/countries.service';
import { NotificationService } from '../../../../services/notification.service';
import { RouterService } from '../../../../../core/helpers/router.service';
import { SmsCurrencyPipe } from '../../../../../core/pipes/sms-currency.pipe';
import { ZendeskService } from '../../../../../core/services/zendesk.service';
import { GlobalService } from '../../../../../core/services/global.service';

import $ from "jquery";
import { count } from 'rxjs/operator/count';

@Component({
  selector: 'top-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [ SmsCurrencyPipe, ZendeskService ]
})
export class MenuComponent implements OnInit {
  cartItem: Array<any>;
  isLoggedIn: boolean;
  supportedLanguage: Array<any>;
  countries: Array<any> = [];
  currentCountry: any;
  smallLogo: String;
  currentLang: String;
  nameDefaultEmail: string = null;
  public productTypes;
  public totalItemCart: number = 0;
  public userInfo: any; //BehaviorSubject<any>;
  public isCollapse: boolean = false;
  public hasNoti: boolean = false;
  public hasNotiRemind: boolean = false;
  public headerCommon: boolean = true;

  constructor(private modalService: ModalService, private translate: TranslateService,
    private userService: UsersService,
    private loadingService: LoadingService,
    public storage: StorageService,
    public cartService: CartService,
    public globalService: GlobalService,
    public appConfig: AppConfig,
    private usersService: UsersService,
    private httpService: HttpService,
    private countriesService: CountriesService,
    private router: Router,
    private notificationService: NotificationService,
    private routerService: RouterService,
    public smsCurrencyPipe: SmsCurrencyPipe,
    private zendeskService: ZendeskService
  ) {
    this.supportedLanguage = appConfig.config.supportedLanguage;
    this.cartItem = this.cartService.getCart();
    cartService.hasUpdate.subscribe(hasUpdate => {
      this.cartItem = this.cartService.getCart();
      $('.plug-item').addClass('pop').delay(100).queue(function(next) {
        $('.plug-item').removeClass("pop");
        next();
      });
    });

    cartService.justAddedItem.subscribe(res => {
      // this.router.navigateByUrl(`/cart`);
      if ($(window).width() > 950 ) {
        $('#app-shopping-cart').fadeIn();
        // $('body').css('overflow-y','hidden');
        this.disableScroll();
        $('.bg-cart-show').show();

      }

    });
    this.userService.justLogin.subscribe((value: boolean) => {
      setTimeout(() => {
        if(this.storage.getCurrentUserItem()) {
          this.nameDefaultEmail = this.storage.getCurrentUserItem().email.substring(0, this.storage.getCurrentUserItem().email.lastIndexOf("@"));
        }

        this.cartItem = this.cartService.getCart();
        this.userInfo = this.storage.getCurrentUserItem();
        // zendesk show user info
        let zendeskname = '';
        zendeskname += this.userInfo.firstName ? this.userInfo.firstName : '';
        zendeskname += this.userInfo.lastName ? (' ' + this.userInfo.lastName) : '';
        this.zendeskService.identify({
          name: zendeskname,
          email: this.userInfo.email,
          organization: 'VIP'
        })
      }, 300);
    })
    this.userService.justLogout.subscribe((value: boolean) => {
      this.userInfo = null;
      // zendesk show user info
      this.zendeskService.identify({
        name: '',
        email: '',
        organization: 'VIP'
      })
    });
    
    if(this.storage.getCurrentUserItem()) {
      this.nameDefaultEmail = this.storage.getCurrentUserItem().email.substring(0, this.storage.getCurrentUserItem().email.lastIndexOf("@"));
    }
  }

  ngOnInit() {
    // check collapse
    // get header common
    this.routerService.getHeaderCommon.subscribe( status => {
      if (status) {
        this.headerCommon = true;
      } else {
        this.headerCommon = false;
      }
    })

    this.userInfo = this.storage.getCurrentUserItem();
    this.cartItem = this.cartService.getCart();

    // get currenct country
    this.currentCountry = this.storage.getCountry();
    this.currentLang = this.storage.getLanguage();
    // get countries from host
    this.getCountries();

    // handle country change event
    this.countriesService.country.subscribe(country => {
      this.currentCountry = country;
      if(this.userInfo && !this.storage.checkFlagSyncCart()) {
        this.cartService.reloadCart().then((data: any) => {
          data.forEach(item => {
            this.cartService.addItem(item);
          });
          // sync cart item to server
          this.cartService.updateCart();
          this.storage.setFlagSyncCart();
        })
        .catch(error => {}// console.log(error))
        );
      }
    });

    // handle route change
    this.router.events.subscribe((val) => {
      $('#app-shopping-cart').hide();
      $('.bg-cart-show').hide();
      this.enableScroll();
      if(this.router.url !== '/') {
        this.smallLogo = 'small-logo';
      } else {
        this.smallLogo = '';
      }
    });
    // setTimeout(_ => this.showDropdownMenu());

    this.notificationService.notiObject.subscribe(data=> {
      if (data && data.msg) {
        this.hasNoti = true;
      } else {
        this.hasNoti = false;
      }
    })
    this.notificationService.notiRemindUser.subscribe(data=> {
      if (data && data.status) {
        this.hasNotiRemind = true;
      } else {
        this.hasNotiRemind = false;
      }

    })

    this.getProductType();
  }

  preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
  }

  preventDefaultForScrollKeys(e) {
    var keys = {37: 1, 38: 1, 39: 1, 40: 1};
    if (keys[e.keyCode]) {
        this.preventDefault(e);
        return false;
    }
  }
  disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', this.preventDefault, false);
    window.onwheel = this.preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = this.preventDefault; // older browsers, IE
    window.ontouchmove  = this.preventDefault; // mobile
    document.onkeydown  = this.preventDefaultForScrollKeys;
  }

  enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
  }
  showDropdownMenu(_event) {
    // $('.bg-cart-show').click(function(){
    //   if($('#app-shopping-cart').is(":visible")){
    //     $('#app-shopping-cart').fadeOut();
    //     $('body').css('overflow-y','auto');
    //     $('.bg-cart-show').hide();
    //   }
    // });
    if($('#app-shopping-cart').is(":visible")){
      this.enableScroll();
      $('#app-shopping-cart').hide();
      // $('body').css('overflow-y','auto');
      $('.bg-cart-show').hide();
    } else {
      // $('body').css('overflow-y','hidden');
      $('#app-shopping-cart').fadeIn();
      $('.bg-cart-show').show();
      this.disableScroll();
    }

    // else
    //   $('#app-shopping-cart').fadeOut();
    // $('ul.nav li.nav-item, ul.nav li.dropdown, li.nav-item-dropdown').hover(function() {
    //   $(this).find('.dropdown-menu').stop(true, true).delay().fadeIn();
    // }, function() {
    //   $(this).find('.dropdown-menu').stop(true, true).delay().fadeOut();
    // });
  }

  showCountryDropdown(_event) {
    if($('#country-dropdown').is(":visible")){
      $('#country-dropdown').fadeOut();
      $('.bg-cart-show').hide();
    } else {
      $('#country-dropdown').fadeIn();
      $('.bg-cart-show').show();
    }
  }

  navbarCollapse(_event) {
    this.isCollapse = !this.isCollapse;
      if (this.isCollapse) {
        $('body').prepend('<div id="cboxOverlay"></div>');
      } else {
        $('#cboxOverlay').remove();
      }
  }

  getCountries() {
    this.httpService._getList(this.appConfig.config.api.countries).subscribe(
      _data => {
        // build country menu
        let result = []
        let directoryCountries: any = [];
        let data = _data;
        this.globalService.getDirectoryCountries().then(_directoryCountries => {
          directoryCountries = _directoryCountries;
          data.forEach(country => {
            // if(country.defaultLang.toLowerCase() === 'ko') {
            //   country.displayLang = 'South Korea (English)';
            //   country.valueLang = 'en';
            //   result.push(Object.assign({}, country))
            //   country.displayLang = `${country.name} (한국어)`;
            //   country.valueLang = 'ko';
            //   result.push(Object.assign({}, country));
            //   if(this.storage.getCountry().code === country.code && !this.currentLang) {
            //     this.currentLang = `${country.name} (${country.displayLang})`;
            //     this.storage.setLanguage(this.currentLang);
            //     this.storage.setLanguageCode(country.valueLang);
            //   }
            // } else {
              let languageName = '';
              let dirCountry: any;
              // let dirCountry = directoryCountries.filter(dirCountry => dirCountry.languageCode.toUpperCase() === country.defaultLang.toUpperCase())[0];
              // if(dirCountry) {
              //   languageName = dirCountry.languageName;
              // }
              country.displayLang = `${country.name} (${country.DirectoryCountries[0].languageName})`;
              country.valueLang = country.defaultLang;
              if(country.SupportedLanguages[0]) {
                result.push(Object.assign({}, country));
              } else {
                result.push(country);
              }

              country.SupportedLanguages.forEach(supLanguage => {
                languageName = '';
                dirCountry = directoryCountries.filter(dirCountry => dirCountry.languageCode.toUpperCase() === supLanguage.languageCode.toUpperCase())[0];
                if(dirCountry) {
                  languageName = dirCountry.languageName;
                }
                country.displayLang = `${country.name} (${languageName})`;
                country.valueLang = dirCountry.languageCode;
                result.push(Object.assign({}, country));
              });

              if(!this.currentLang) {
                this.currentLang = this.storage.getLanguage();
              }
              if(this.storage.getCountry().code === country.code && !this.currentLang) {
                this.currentLang = `${country.displayLang}`;
                this.storage.setLanguage(this.currentLang);
                this.storage.setLanguageCode(country.valueLang);
              }
            // }
          })
        })

        this.countries = result;
        this.countriesService.setCountries(data);
      },
      error => {
        // console.log(`Cannot get country: ${error}`)
      }
    )
  }

  getLoggedIn() {
    return this.userService.getLoggedIn();
  }
  openModal(id: string){
    // console.log(`openModal ${id}`);
    this.modalService.open(id);
  }
  closeModal(id: string){
    this.modalService.close(id);
  }
  logout(event) {
    event.preventDefault();
    this.cartService.clearCart(true);
    this.storage.clearFlagSyncCart();
    this.userService.logout();
    this.translate.get('notification').subscribe(res => {
      this.notificationService.addNotification(res.logout);
    });
    setTimeout(() => {
       this.notificationService.addNotification(null);
    }, 2500);
  }
  ngOnDestroy() {
    this.translate.onLangChange.unsubscribe();
  }

  redirectToLoginPage() {
    this.userService.applyShowLoginStep();
    this.router.navigate(['/login']);
  }

  // displayShoppingCart() {
  //   if($(".shopping-cart").hasClass( "show" )) {
  //     $(".shopping-cart").removeClass("show");
  //   } else {
  //     $(".shopping-cart").addClass("show");
  //   }
  //   $(".shopping-cart").fadeToggle( "fast");
  // }

  changeAvatar() {
    this.openModal('change-avatar-modal');
  }

  changeCountry(event, country) {
    this.currentLang = this.storage.getLanguage();
    if(country.code !== this.storage.getCountry().code && country.displayLang !== this.currentLang) {
      this.countriesService.changeCountry(country);
      this.currentLang = country.displayLang;
      this.cartService.updateCart();
      this.translate.use(country.valueLang.toLowerCase());
      this.storage.setLanguage(country.displayLang);
      this.storage.setLanguageCode(country.valueLang);
      this.zendeskService.setLocale(country.valueLang.toLowerCase());
      this.router.navigate(['/']);
    } else if(country.displayLang !== this.currentLang) {
      this.currentLang = country.displayLang;
      this.translate.use(country.valueLang.toLowerCase());
      this.storage.setLanguage(country.displayLang);
      this.storage.setLanguageCode(country.valueLang);
      this.zendeskService.setLocale(country.valueLang.toLowerCase());
      this.countriesService.changeLanguage();

      this.router.navigate(['/']);
    }

    this.getProductType();
    
    if (!$('.btn-top-menu-collapse').hasClass('collapsed')) {
      $('.btn-top-menu-collapse').trigger('click');
    }
    $('#country-dropdown').fadeOut();
    $('.bg-cart-show').hide();
    event.preventDefault();
  }

  // go to login page
  gotoLogin() {

  }
  detechCollapse(event){
    if($('body').hasClass('remove_scroll'))
      $('body').removeClass('remove_scroll');
    else
      $('body').addClass('remove_scroll');
  }
  checkScroll(event){
    if($('body').hasClass('remove_scroll')){
      event.preventDefault();
      return true;
    }
    return false;
  }

  getProductType() {
    let productList = [];
    this.productTypes = [];

    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        data.forEach((product, index) => {
          let productImage: any;

          // push to product list
          if(productList[product.ProductType.name]) {
            productList[product.ProductType.name].push(product);
          } else {
            productList[product.ProductType.name] = [product];
            this.productTypes.push(product.ProductType);
          }
        });
        // console.log('this.productTypes', this.productTypes)
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  hideMenu() {
    if (!$('.btn-top-menu-collapse').hasClass('collapsed')) {
      $('.btn-top-menu-collapse').trigger('click');
    }
  }
}
