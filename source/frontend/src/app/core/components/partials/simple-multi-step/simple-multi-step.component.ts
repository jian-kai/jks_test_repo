import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-simple-multi-step',
  templateUrl: './simple-multi-step.component.html',
  styleUrls: ['./simple-multi-step.component.scss']
})
export class SimpleMultiStepComponent {

  stepsData: any;
  currentStep: string;

  constructor() { }

  setCurrentStep(currentStep) {
    this.currentStep = currentStep;
  }

  setStepsData(stepsData) {
    this.stepsData = stepsData;
  }
  goto(step: string) {
    let steps = Object.keys(this.stepsData);
    let currentStepComponent = this[this.stepsData[this.currentStep].step];
		// console.log(currentStepComponent);
		// // console.log(this.stepsData[this.currentStep].step);
    let stepComponent = this[this.stepsData[step].step];
    if(steps.indexOf(this.currentStep) > steps.indexOf(step)) {
      this.stepsData[this.currentStep].state = 'init';
      stepComponent.setActive();
      this.stepsData[step].state = 'active';
      this.currentStep = step;
    } else if(steps.indexOf(this.currentStep) < steps.indexOf(step)){
			if(typeof currentStepComponent !== 'undefined') {
				if(currentStepComponent.validate().allValid) {
					this.stepsData[this.currentStep].state = 'finish';
					stepComponent.setActive();
					this.stepsData[step].state = 'active';
					this.currentStep = step;
				}
			}
    }
  }
}
