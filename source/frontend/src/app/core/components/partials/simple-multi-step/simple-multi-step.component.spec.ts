import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleMultiStepComponent } from './simple-multi-step.component';

describe('SimpleMultiStepComponent', () => {
  let component: SimpleMultiStepComponent;
  let fixture: ComponentFixture<SimpleMultiStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleMultiStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleMultiStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
