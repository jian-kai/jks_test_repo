import { Component, OnInit } from '@angular/core';
import { RouterService } from '../../../helpers/router.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../services/storage.service';
import { CountriesService } from '../../../services/countries.service';
import { HttpService } from '../../../../core/services/http.service';
import { GTMService } from '../../../../core/services/gtm.service';
import { GlobalService } from '../../../../core/services/global.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  minAmount: any;
  currentCountry: any;
  public footerCommon: boolean = true;
  public hasAssuranceSection: boolean = true;
  public productTypes;
  public isLoadingProductTypes : boolean = true;

  constructor(
    private routerService: RouterService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    public storage: StorageService,
    private gtmService: GTMService,
    private globalService: GlobalService,
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.isLoadingProductTypes = true;
    this.routerService.getFooterFull.subscribe( status => {
      if (status) {
        this.footerCommon = true;
      } else {
        this.footerCommon = false;
      }
    });

    this.routerService.getFooterAssuranceSection.subscribe( status => {
      if (!status) {
        this.hasAssuranceSection = false;
      } else {
        this.hasAssuranceSection = true;
      }
    });

    this.minAmount = this.appConfig.getConfig('shippingFeeConfig').minAmount;

    this.appConfig.configEvent.subscribe(config => {
      this.minAmount = config.shippingFeeConfig.minAmount;
    });

    this.currentCountry = this.storage.getCountry();
    this.countriesService.country.subscribe(country => {
      this.currentCountry = country;
      this.getProductType();
    });

    this.getProductType();
  }


  getProductType() {
    this.isLoadingProductTypes = true;
    let productList = [];
    this.productTypes = [];

    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        // console.log('data', data)
        
        this.gtmService.itemImpressions(data);
        
        data.forEach((product, index) => {
          let productImage: any;

          // fetch to find default image
          product.productImages.forEach((imageItem, index) => {
            if (imageItem.isDefault) {
              productImage = imageItem;
            }
          });

          // push to product list
          if(productList[product.ProductType.name]) {
            productList[product.ProductType.name].push(product);
          } else {
            productList[product.ProductType.name] = [product];
            this.productTypes.push(product.ProductType);
          }
          // get productType
          // this.productTypes = Object.keys(productList);
        });
        this.isLoadingProductTypes = false;
      },
      error => {
        this.isLoadingProductTypes = false;
        // console.log(error.error)
      }
    )

    // this.httpService._getList(`${this.appConfig.config.api.product_types}/`).subscribe(
    //   data => {
    //     this.isLoadingProductTypes = false;
    //     if (data && data.rows) {
    //       this.productTypes = data.rows;
    //     }
    //     // console.log('data', data)
    //   },
    //   error => {
    //     this.isLoadingProductTypes = false;
    //     // console.log(error.error)
    //   }
    // )
  }

}
