import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleSubStepComponent } from './simple-sub-step.component';

describe('SimpleSubStepComponent', () => {
  let component: SimpleSubStepComponent;
  let fixture: ComponentFixture<SimpleSubStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleSubStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleSubStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
