import { Component, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-simple-sub-step',
  templateUrl: './simple-sub-step.component.html',
  styleUrls: ['./simple-sub-step.component.scss']
})
export class SimpleSubStepComponent {
  element: any;
  constructor(el: ElementRef) {
    this.element = $(el.nativeElement);
  }

  setActive() {
    // TODO implement
  }

  validate() {
    return {
      allValid: true
    };
  }

}
