import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { URLSearchParams, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { RouterService } from '../../helpers/router.service';

const NUM_OF_PAGES: number = 10;

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {
  // array of all items to be paged
  private totalPages: number;
  private pages: Array<number> = [];
  private start: number;
  private end: number;
  private hasNext: boolean = true;
  private hasPrev: boolean = true;

  @Input() totalItems: number = 0;
  @Input() currentPage: number = 0;
  @Input() limit: number = 10;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();

  constructor(private router: Router,
    private location: Location,
    private routerService: RouterService) { }

  ngOnInit() {}

  ngOnChanges(changes) {
    // console.log(`ngOnchange ${JSON.stringify(changes)}`);
    // reset value
    this.hasNext = true;
    this.hasPrev = true;

    // calculate total page
    this.totalPages = Math.ceil(this.totalItems / this.limit);
    this.start = (this.currentPage - 1) * this.limit + 1;
    this.end = this.totalItems;

    // calculate start and end pages
    if(this.limit < this.totalItems) {
      this.end = this.limit * this.currentPage;
      if (this.end > this.totalItems) {
        this.end = this.totalItems;
      }
    }

    // determine has next and has previous 
    if(this.end === this.totalItems) {
      this.hasNext = false;
    }
    if(this.start === 1) {
      this.hasPrev = false;
    }

    // calculate number of page displayed
    this.pages = [];
    let startPage = Math.ceil(this.currentPage/NUM_OF_PAGES);
    for(let i = startPage; (i < (startPage + NUM_OF_PAGES)) && (i <= this.totalPages) ; i ++) {
      this.pages.push(i);
    }
  }

  goto(page) {
    this.routerService.appendRouterParams({page});
    this.pageChange.emit(page);
  }

  onNext() {
    if(this.hasNext) {
      this.goto(this.currentPage + 1);
    }
  }

  onPrev() {
    if(this.hasPrev) {
      this.goto(this.currentPage - 1);
    }
  }
}
