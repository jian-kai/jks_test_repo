import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationStart } from '@angular/router';
import { Meta } from '@angular/platform-browser';

import { translator } from '../../../i18n';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { CountriesService } from '../../../core/services/countries.service';
import { NotificationService } from '../../../core/services/notification.service';
import { CartService } from '../../../core/services/cart.service';
import { HttpService } from '../../../core/services/http.service';
import { ZendeskService } from '../../../core/services/zendesk.service';
import { ModalService } from '../../../core/services/modal.service';

import { Location } from '@angular/common';
// import { setTimeout } from 'timers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers : [TranslateService, ZendeskService]
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private http: Http,
    private translate: TranslateService,
    public storage: StorageService,
    public appConfig: AppConfig,
    private router: Router,
    private countriesService: CountriesService,
    private notificationService: NotificationService,
    private _http: HttpService,
    public cartService: CartService,
    public modalService: ModalService,
    private metaService: Meta,
    private location: Location,
    public zendeskService: ZendeskService,
    ) {
      // let alertEmail;
      // if(alertEmail) {
      //   clearTimeout(alertEmail);
      // }
      // alertEmail = setTimeout(() => {
      //   if(!this.storage.getCountry().isWebEcommerce && !this.storage.getInfoForEcommerce()) {
      //     this.modalService.open('collect-email-modal');
      //   }
      // }, 30000);

      let lang = this.translate.currentLang ? this.translate.currentLang.toLowerCase() : (this.storage.getLanguageCode() ? this.storage.getLanguageCode().toLowerCase() : null);
      let currentCountry = this.countriesService.currentCountry ? this.countriesService.currentCountry : this.storage.getCountry();
      let tempLang;
      let tempCurrentCountry;
      let checkLang = setInterval(() => {
        // console.log('currentCountry', currentCountry);
        // console.log('lang', lang);
        tempLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toLowerCase() : null;
        tempCurrentCountry = this.storage.getCountry();

        if(currentCountry.code !== tempCurrentCountry.code) {
          lang = tempLang;
          currentCountry = tempCurrentCountry;
          this.countriesService.changeCountry(currentCountry);
          this.cartService.updateCart();
          this.translate.use(lang);
          this.zendeskService.setLocale(lang);
          this.router.navigate(['/']);
        } else if(lang !== tempLang) {
          lang = tempLang;
          currentCountry = tempCurrentCountry;
          this.translate.use(lang);
          this.zendeskService.setLocale(lang);
          this.countriesService.changeLanguage();
        }
      }, 1000);

      /*this.countriesService.country.subscribe(country => {
        if(alertEmail) {
          clearTimeout(alertEmail);
        }
        alertEmail = setTimeout(() => {
          if(!this.storage.getCountry().isWebEcommerce && !this.storage.getInfoForEcommerce()) {
            this.modalService.open('collect-email-modal');
          }
        }, 30000);
      });*/

      // migration multiple language
      if(!this.storage.getMigrateLanguage()) {
        // remove country data
        this.storage.clearCountry();
        this.storage.clearCountries();

        // set has migration language
        this.storage.setMigrateLanguage(true);
      }

      // init Zendesk
      if(this.storage.getCountry().isWebEcommerce) {
        this.zendeskService.show();
      } else {
        this.zendeskService.hide();
      }
      this.countriesService.country.subscribe(country => {
        if(this.storage.getCountry().isWebEcommerce) {
          this.zendeskService.show();
        } else {
          this.zendeskService.hide();
        }
      });
      if(this.storage.getCurrentUserItem()) {
        // zendesk show user info
        let _userInfo = this.storage.getCurrentUserItem();
        // zendesk show user info
        let zendeskname = '';
        zendeskname += _userInfo.firstName ? _userInfo.firstName : '';
        zendeskname += _userInfo.lastName ? (' ' + _userInfo.lastName) : '';
        this.zendeskService.identify({
          name: zendeskname,
          email: _userInfo.email,
          organization: 'VIP'
        })
      }

      // set transatation data
      translate.setTranslation('en', translator.en);
      translate.setTranslation('vn', translator.vn);
      translate.setTranslation('ko', translator.ko);

      // use english as default language
      let currentLang = this.storage.getLanguageCode()
      if(currentLang) {
        translate.setDefaultLang(currentLang.toLowerCase());
        this.zendeskService.setLocale(currentLang.toLowerCase());
      } else {
        translate.setDefaultLang('en');
        this.zendeskService.setLocale('en');
      }

      router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.http.get(this.appConfig.config.api.seo_configuration).map(res => res.json()).subscribe(
            (data: any) => {
              let ele = data.find(value => event.url.indexOf(value.url) !== -1);

              var metaName = document.getElementsByTagName('meta');
              var metaDescription;
              for(var i = 0; i < metaName.length; i++) {
                if(metaName[i].getAttribute('name') === 'description') {
                  metaDescription = metaName[i];
                }
              }

              if(ele) {
                document.title = ele.title;
                // $('meta[name=description]').remove();
                // $('head').append( '<meta name="description" content="' + ele.description + '">' );
                metaDescription.setAttribute('content', ele.description);
              } else {
                document.title = 'Shaves2U | The Perfect Shave for the Everyday Man';
                metaDescription.setAttribute('content', 'Shop for quality shavers at an affordable price and discover a new experience in men\'s grooming.');
              }
            },
            error => {
              // console.log(error.error)
            }
          )

          let expr = /\/products\?type=/;
          if (event.url.match(expr)){}
          else {
            window.scrollTo(0, 0);
          }
          var btnTopMenuCollapse = document.getElementsByClassName('btn-top-menu-collapse');
          for(var i = 0; i < btnTopMenuCollapse.length; i++) {
            if((' ' + btnTopMenuCollapse[i].className + ' ').indexOf(' collapsed ') == -1) {
              (<HTMLScriptElement><any>btnTopMenuCollapse[i]).click();
            }
          }
          // hidden notification resend active email
          this.notificationService.addNotificationRemindUser();

          // clear undo cart
          this.cartService.clearItemRemoved();

          // clear free trial storage
          let expr2 = /\/checkout/;
          if (event.url.match(expr2) && (this.storage.getFreeTrialItem() || this.storage.getCustomPlanItem())){}
          else {
            this.storage.clearFreeTrialItem();
            this.storage.clearCustomPlanItem();
          }

        }
      });

  }

  ngOnInit() {
    // var swidth=(window.innerWidth-$(window).width());
    // // console.log(swidth);
    document.getElementById('shopping-card-body').onmouseenter = function() {enableScroll()};
    document.getElementById('shopping-card-body').onmouseout = function() {disableScroll()};
    document.getElementById('shopping-card-body').onmouseover = function() {enableScroll()};

    document.getElementById('bg-cart-show').onclick = function() {
      if(document.getElementById("app-shopping-cart").style.display === 'block') {
        document.getElementById("app-shopping-cart").style.display = 'none';
        document.getElementById("bg-cart-show").style.display = 'none';
        enableScroll();
      }
      if(document.getElementById("country-dropdown").style.display === 'block') {
        document.getElementById("country-dropdown").style.display = 'none';
        document.getElementById("bg-cart-show").style.display = 'none';
        enableScroll();
      }
    };

    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
      if (window.removeEventListener)
          window.removeEventListener('DOMMouseScroll', preventDefault, false);
      window.onmousewheel = document.onmousewheel = null;
      window.onwheel = null;
      window.ontouchmove = null;
      document.onkeydown = null;
    }

    function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
      var keys = {37: 1, 38: 1, 39: 1, 40: 1};
      if (keys[e.keyCode]) {
          preventDefault(e);
          return false;
      }
    }

    // detect location to display langauge
    let currentCountry = this.storage.getCountry();
    if(!currentCountry || currentCountry.isDefault) {
      this.countriesService.countriesChanged.subscribe(countries => {
        let country = countries.find(value => value.code === currentCountry.countryCode);
        if(country) {
          this.countriesService.changeCountry(country);
        } else {
          country = countries.find(value => value.code === 'MYS');
          this.countriesService.changeCountry(country);
        }
      });

      // detect_current_location by Ip
      this.http.get(this.appConfig.config.api.detect_current_location)
      .map(res => res.json())
      .subscribe(
        (data: any) => {
          if (data.ok && data.data) {

            this.getCountries().then((res: any) => {
              let countries = res || [];
              // console.log('countries', countries)
              let country = countries.find(value => value.code === data.data.code);
              // console.log('country', country)
              if(country) {
                this.translate.use(country.defaultLang.toLowerCase());
                this.countriesService.changeCountry(country);
                this.storage.setLanguage(`${country.name} (${country.DirectoryCountries[0].languageName})`);
                this.storage.setLanguageCode(country.defaultLang);
                this.zendeskService.setLocale(country.defaultLang.toLowerCase());
              } else {
                country = countries.find(value => value.code === 'MYS');
                this.countriesService.changeCountry(country);
                this.storage.setLanguage(`${country.name} (${country.DirectoryCountries[0].languageName})`);
                this.storage.setLanguageCode(country.defaultLang);
                this.zendeskService.setLocale(country.defaultLang.toLowerCase());
              }
            })

          }
        },
        err => {
          // console.log(`can not get country code: ${err}`);
        }
      );




      // window.navigator.geolocation.getCurrentPosition(position => {
      //   this.http.get(this.appConfig.config.multipleLanguage.countryCodeService, {
      //     params: {
      //       lat: position.coords.latitude,
      //       lng: position.coords.longitude,
      //       username: this.appConfig.config.multipleLanguage.username,
      //       type: 'JSON'
      //     }
      //   })
      //     .map(res => res.json())
      //     .subscribe(
      //       data => {
      //         let countries = this.countriesService.getCountries() || [];
      //         let country = countries.find(value => value.code === data.countryCode);
      //         if(country) {
      //           this.countriesService.changeCountry(country);
      //         } else {
      //           country = countries.find(value => value.code === 'MYS');
      //           this.countriesService.changeCountry(country);
      //         }
      //       },
      //       err => {
      //         console.log(`can not get country code: ${err}`);
      //       }
      //     );
      // });

    }

    // init facebook
    setTimeout(_ => {
      window['FB'].init({
        appId      : this.appConfig.config.facebook.appId,
        cookie     : true,
        xfbml      : true,
        version    : 'v2.10'
      });
      window['FB'].AppEvents.logPageView();
    }, 1000);

    let jquery = document.createElement('script');
    jquery.src = 'https://d8pi1a7qa1dfv.cloudfront.net/assets/js/jquery.min.js';
    jquery.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(jquery);

    var scriptName = document.getElementsByTagName('script');
    let watchingInterval = setInterval(() => {
      if(window['jQuery']) {
        let node = document.createElement('script');
        node.src = 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js';
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

        node = document.createElement('script');
        node.src = 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js';
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

        node = document.createElement('script');
        node.src = '/assets/js/pgwslider.js';
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

        node = document.createElement('script');
        node.src = 'https://d8pi1a7qa1dfv.cloudfront.net/assets/js/jquery.payment.js';
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

        node = document.createElement('script');
        node.src = 'https://d8pi1a7qa1dfv.cloudfront.net/assets/js/jquery.spzoom.min.js';
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

        // node = document.createElement('script');
        // node.src = 'https://d8pi1a7qa1dfv.cloudfront.net/assets/js/zendesk/main.js';
        // node.type = 'text/javascript';
        // document.getElementsByTagName('head')[0].appendChild(node);

        // node = document.createElement('script');
        // node.src = 'https://d8pi1a7qa1dfv.cloudfront.net/assets/js/zendesk/zEmbed.js';
        // node.type = 'text/javascript';
        // document.getElementsByTagName('head')[0].appendChild(node);

        let nodeBootstrap = document.createElement('script');
        nodeBootstrap.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js';
        nodeBootstrap.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(nodeBootstrap);
        clearInterval(watchingInterval);
      }
    }, 100);

    // get utm_medium
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    let utmMedium = decodeURIComponent(params.get('utm_medium'));
    let utmSource = decodeURIComponent(params.get('utm_source'));
    let utmCampaign = decodeURIComponent(params.get('utm_campaign'));
    let utmTerm = decodeURIComponent(params.get('utm_term'));
    let utmContent = decodeURIComponent(params.get('utm_content'));
    if(utmMedium && utmMedium !== '' && utmMedium !== 'null') {
      this.storage.setGtmMedium(utmMedium);
    }
    if(utmSource && utmSource !== '' && utmSource !== 'null') {
      this.storage.setGtmSource(utmSource);
    }
    if(utmCampaign && utmCampaign !== '' && utmCampaign !== 'null') {
      this.storage.setGtmCampaign(utmCampaign);
    }
    if(utmTerm && utmTerm !== '' && utmTerm !== 'null') {
      this.storage.setGtmTerm(utmTerm);
    }
    if(utmContent && utmContent !== '' && utmContent !== 'null') {
      this.storage.setGtmContent(utmContent);
    }
  }

  ngOnDestroy() {
    this.storage.removeGtmMedium();
    this.storage.removeGtmSource();
    this.storage.removeGtmCampaign();
    this.storage.removeGtmTerm();
    this.storage.removeGtmContent();
  }

  getCountries() {
    return new Promise((resolved, reject) => {
      this._http._getList(this.appConfig.config.api.countries)
      .subscribe(
        res => {
          // console.log(`countries ${JSON.stringify(res)}`);
          resolved(res);
        },
        error => {
          reject(error);
        }
      );
    });
  }
}
