import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectEmailModalComponent } from './collect-email-modal.component';

describe('CollectEmailModalComponent', () => {
  let component: CollectEmailModalComponent;
  let fixture: ComponentFixture<CollectEmailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectEmailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectEmailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
