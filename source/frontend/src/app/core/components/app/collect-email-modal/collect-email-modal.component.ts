import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from '../../../../../app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'collect-email-modal',
  templateUrl: './collect-email-modal.component.html',
  styleUrls: ['./collect-email-modal.component.scss']
})
export class CollectEmailModalComponent extends ModalComponent {
  private currentUser: any;
  public emailForm: FormGroup;
  public resError: any;
  public i18nDialog: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private builder: FormBuilder,
    private translate: TranslateService,
    private loadingService: LoadingService,
    private alertService: AlertService, ) {
    super(modalService, el);
    this.id = 'collect-email-modal';

    this.emailForm = builder.group({
      email: ['', Validators.required],
    });

  }

  setInitData(_addresss) {
    this.emailForm.reset();
    this.resError = null;
  }

  // do save email
  doSaveEmail() {
    this.loadingService.display(true);
    let credentials = this.emailForm.value;
    credentials.email = this.emailForm.value.email;
    credentials.countryCode = this.storage.getCountry().code;

    this._http._update(`${this.appConfig.config.api.non_ecommerce}`, credentials).subscribe(
      data => {
        this.storage.setInfoForEcommerce(credentials.email);
        this.loadingService.display(false);
        this.close();
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };

        if(this.resError.message === 'This email is already used') {
          this.translate.get('msg').subscribe(res => {
            this.i18nDialog = res;
          });
          this.resError.message = this.i18nDialog.validation.email_exists;
        }
        this.alertService.error([error]);
      }
    )
  }

}
