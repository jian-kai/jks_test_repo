import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from '../../../../../app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { TranslateService } from '@ngx-translate/core';
import { CountriesService } from '../../../../core/services/countries.service';
import { ProductTranslatePipe } from '../../../../core/pipes/product-translate.pipe';
import { DialogService } from '../../../../core/services/dialog.service';
import { MatDialog } from '@angular/material';
import { DialogResultComponent } from '../../../../core/directives/dialog-result/dialog-result.component';
import * as $ from 'jquery';
@Component({
  selector: 'cancellation-journey-modal',
  templateUrl: './cancellation-journey-modal.component.html',
  styleUrls: ['./cancellation-journey-modal.component.scss'],
  providers: [ProductTranslatePipe]
})
export class CancellationJourneyModalComponent extends ModalComponent {
  private currentUser: any;
  public CancellationForm: FormGroup;
  public resError: any;
  public i18nDialog: any;
  public getmessages : any;
  public getid : any;
  public subsInfo: any;
  public showChange: boolean = false;
  currentCountry: any;
  smallLogo: String;
  currentLang: String;
  messageList: Array<any> = [];
  constructor(modalService: ModalService,
    el: ElementRef,
    public dialog: MatDialog,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private builder: FormBuilder,
    private translate: TranslateService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    private dialogService: DialogService,
    private alertService: AlertService,
    private ProductTranslatePipe: ProductTranslatePipe, ) {
    super(modalService, el);
    this.id = 'cancellation-journey-modal';

    this.CancellationForm = builder.group({
      optionReason: ['', Validators.required],
      checkboxMessage : ['', Validators.required],
    });
    this.currentCountry = this.storage.getCountry();
    this.currentLang = this.storage.getLanguage();
    // get countries from host
 

  }
  setInitData(subsInfo) {
    this.CancellationForm.reset();
    this.getMeassage();
    this.resError = null;
    this.subsInfo = subsInfo;
    
  }

 cancell_message(elmnt , id , val)
  {
    if(id != 'optionReason'){
      $('.cancell_message_c').not(elmnt).prop('checked', false);
      $('#optionReason').prop('checked', false);
    }
    else
    {
      if($('#optionReason').prop('checked')){
        $('#optionReason').prop('checked', true);
        $('.cancell_message_c').prop('checked', false);
  
      }
      else
      {  
        $('#optionReason').prop('checked', false);
      }
    } 
      this.getid = val;
  }

  getMeassage() {
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.messageList = [];

    this._http._getList(`${this.appConfig.config.api.message}?` + params.toString()).subscribe(
      data => {
        this.getmessages = data;

 // console.log('data',data);
 data.forEach((item, index) => {
  let i = 0;
  item.messagestranslates.forEach(element => {
    i ++;
    if(i <= item.messagestranslates.length) {
      this.messageList.push(element);
    }
  });
});

        console.log(`messagestest === ${JSON.stringify(data)}`);
      },
      error => {
        this.alertService.error(error);
      }
     
    )

  }

  doSaveCancelMessage(){
    let MessageResult = "";

     this.loadingService.display(true);  
    let credentials = this.CancellationForm.value;


    if(this.CancellationForm.value.optionReason)
    {
    credentials.optionReason = this.CancellationForm.value.optionReason;
    }
    else
    {
      credentials.optionReason = "";
    }
  // Access each item like this in PartyRoles


  let countmessage=1;
  let _plan_name;
  let _msg = '';
  let _msgCon = '';
  if (this.storage.getLanguageCode() === 'KO' && this.storage.getCountry().code.toUpperCase() === 'KOR') {
    _msgCon ="고객님의 정기구매 서비스가 취소되었습니다";
  }
  else
  {
    _msgCon ="Your subscription is cancelled";
  }
  // Or a filetered list 
  // let checkedMessage = this.messageList.filter(x=>x.Checked === true);
  // checkedMessage.forEach(checked=>{

  //   MessageResult += checked.id;
    
  //   if(checkedMessage.length != countmessage)
  //   {
  //     MessageResult += ",";
  //   }
  //   countmessage++;
    
  // });
  if(this.getid !='optionReason')
  {
  MessageResult = this.getid
  }


    credentials.messageid = MessageResult;
    credentials.countryCode = this.storage.getCountry().code;
    credentials.subid = this.subsInfo.id;
    credentials.userid = this.subsInfo.UserId;

    credentials.email = this.subsInfo.email;
    
     
        // readd mising param
        this._http._delete(`${this.appConfig.config.api.cancel_trial_plan}/${this.subsInfo.id}?isUserECommerce=true`).subscribe(
          data => {
           
            if(this.subsInfo.PlanCountry) {
              this.subsInfo.PlanCountry.Plan.planTranslate = this.subsInfo.PlanCountry.Plan.planTranslate[0] ? this.subsInfo.PlanCountry.Plan.planTranslate : [this.subsInfo.PlanCountry.Plan.planTranslate];
              _plan_name = this.ProductTranslatePipe.transform(this.subsInfo.PlanCountry.Plan.planTranslate, 'name');
            }
            if(this.subsInfo.CustomPlan) {
              _plan_name = this.subsInfo.CustomPlan.description;
            }

            _msg =  _msgCon;
            this.subsInfo.status = 'Canceled';
            this.showChange = false;
         
            this._http._create(`${this.appConfig.config.api.message}`, credentials).subscribe(
              data => {
                this.loadingService.display(false);
                this.close();
                 this.dialog.open(DialogResultComponent, {
                  data: {
                    content: _msgCon
                  }
                });
    
                        _msg = this.i18nDialog.confirm.trial_subs_cancelled_succ.replace('{{name}}', _msgCon);
                        this.subsInfo.status = 'Canceled';
                        this.showChange = false;
                   
                        this.dialogService.result(_msg).afterClosed().subscribe((result => {
                          
                        }));
      
              },
              error => {
                let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this.dialogService.result(message).afterClosed().subscribe((result => {
               this.loadingService.display(false);
             }));
              }
            )
          
          },
          error => {
            let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this.dialogService.result(message).afterClosed().subscribe((result => {
               this.loadingService.display(false);
             }));
           
            // console.log(error);
          }
        );

      
   
  }

}
