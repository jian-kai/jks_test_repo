import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancellationJourneyModalComponent } from './cancellation-journey-modal.component';

describe('CancellationJourneyModalComponent', () => {
  let component: CancellationJourneyModalComponent;
  let fixture: ComponentFixture<CancellationJourneyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancellationJourneyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancellationJourneyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
