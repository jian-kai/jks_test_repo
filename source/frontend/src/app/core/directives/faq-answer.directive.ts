import { Directive, Input, ElementRef, OnInit, NgModule, ModuleWithProviders  } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import showdown from 'showdown';
import { CommonModule } from '@angular/common';

@Directive({
  selector: 'appFaqAnswer'
})
export class FaqAnswerDirective implements OnInit {
  @Input() faqAnswer: any;
  converter: any;

  constructor(private el: ElementRef, private translate: TranslateService) {
    this.converter = new showdown.Converter({'simpleLineBreaks': true});
  }

  ngOnInit() {
    // let currentLang = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    // let translate = this.y.find(value => value.langCode.toUpperCase() === currentLang);
    
    this.el.nativeElement.innerHTML = this.converter.makeHtml(this.faqAnswer.replace(/\\n/g, '\n'));
    let ul = this.el.nativeElement.querySelectorAll('ul');
    if(ul) {
      // ul.forEach(element => {
      //   // element.style = 'list-style: inside;';
      //   element.className += "faq-details";
      // });
      for(let i = 0; i < ul.length; i++) {
        // element.style = 'list-style: inside;';
        ul[i].className += "faq-details";
      }
    }
  }
}

