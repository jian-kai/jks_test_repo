import { Directive, ElementRef, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import {GlobalService} from '../../../../core/services/global.service';
import {Item} from '../../../core/../models/item.model';

@Directive({
  selector: '[app-product-translate]'
})
export class ProductTranslateDirective implements OnInit {
  public curLangCode: string = 'EN';
  @Input() inputTranslate: any = [];
  constructor(private translate: TranslateService,
    private globalService: GlobalService,
  ) {
  }

  ngOnInit() {
    this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    this.orderTranslate();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.curLangCode = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
      if (!this.inputTranslate) {
        return [];
      }
      this.orderTranslate();
    });
  }

  // order translate
  public orderTranslate() {
    let product = this.inputTranslate;
    let hasExist = false;
    let productTranslate = [];
    if(product.productCountry) {
      product.productTranslate.forEach((ele) => {
        if(ele.langCode === this.curLangCode) {
          hasExist = true;
          productTranslate.splice(0, 0, ele);
          // product.productTranslate.splice(0, 0, ele);
        } else {
          productTranslate.push(ele);
          // product.productTranslate.push(ele);
        }
      });
      product.productTranslate = productTranslate;
      if (!hasExist) {
        product.productTranslate.forEach((ele) => {
          if (ele.langCode === product.productCountry.Country.defaultLang) {
            product.productTranslate.splice(0, 0, ele);
          } else {
            product.productTranslate.push(ele);
          }
        });
      }
    }
    else {
      // process for subscription
      product.planTranslate.forEach((ele) => {
        if(ele.langCode === this.curLangCode) {
          hasExist = true;
          productTranslate.splice(0, 0, ele);
        } else {
          productTranslate.push(ele);
        }
      });
      product.planTranslate = productTranslate;
      if (!hasExist) {
        product.planTranslate.forEach((ele) => {
          if (ele.langCode === product.planCountry.Country.defaultLang) {
            product.planTranslate.splice(0, 0, ele);
          } else {
            product.planTranslate.push(ele);
          }
        });
      }
    }
  }

  ngOnDestroy() {
  }

}
