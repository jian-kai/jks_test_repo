import { Directive, ElementRef, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import {GlobalService} from '../../../../core/services/global.service';
import {Item} from '../../../core/../models/item.model';

@Directive({
  selector: '[app-products-translate]'
})
export class ProductsTranslateDirective implements OnInit {
  public curLangCode: string = 'EN';
  @Input() inputTranslate: any = [];
  constructor(private translate: TranslateService,
    private globalService: GlobalService,
  ) {
  }

  ngOnInit() {
    this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
     this.orderTranslate();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.curLangCode = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
      if (!this.inputTranslate) {
        return [];
      }
      this.orderTranslate();
    });
  }

  // order translate
  public orderTranslate() {
    // let duplicateArray = this.duplicateArray(this.inputTranslate);
    this.inputTranslate.forEach((item, index) => {
      let product = item;
      if (item.product) {
        product = item.product;
      }
      // let product = item.product;
      let hasExist = false;
      let productTranslate = [];
      if(product.productCountry) {
        product.productTranslate.forEach((ele) => {
          if(ele.langCode === this.curLangCode) {
            hasExist = true;
            productTranslate.splice(0, 0, ele);
            // product.productTranslate.splice(0, 0, ele);
          } else {
            productTranslate.push(ele);
            // product.productTranslate.push(ele);
          }
        });
        product.productTranslate = productTranslate;
        if (!hasExist) {
          product.productTranslate.forEach((ele) => {
            if (ele.langCode === product.productCountry.Country.defaultLang) {
              product.productTranslate.splice(0, 0, ele);
            } else {
              product.productTranslate.push(ele);
            }
          });
        }
      }
      else {
        // process for subscription
        product.plan.planTranslate.forEach((ele) => {
          if(ele.langCode === this.curLangCode) {
            hasExist = true;
            productTranslate.splice(0, 0, ele);
          } else {
            productTranslate.push(ele);
          }
        });
        product.plan.planTranslate = productTranslate;
        if (!hasExist) {
          product.plan.planTranslate.forEach((ele) => {
            if (ele.langCode === product.planCountry.Country.defaultLang) {
              product.plan.planTranslate.splice(0, 0, ele);
            } else {
              product.plan.planTranslate.push(ele);
            }
          });
        }
      }
    });
  }

  public duplicateArray(content) {
    let arr = [];
    content.forEach((x) => {
      arr.push(x);
    })
    return arr;
  }
  ngOnDestroy() {
  }

}
