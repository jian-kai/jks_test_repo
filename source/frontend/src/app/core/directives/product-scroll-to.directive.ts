import { Directive, ElementRef, Renderer } from '@angular/core';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
declare var jQuery: any;
import $ from "jquery";

@Directive({
  selector: '[productsScrollTo]'
})
export class ProductsScrollToDirective {

  constructor(el: ElementRef, renderer: Renderer,
    private location: Location,
    private router: Router,) {
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    if (params && params.get('type')) {
      setTimeout(_ => {
        el.nativeElement.querySelector('#' + params.get('type'));
        let height_menu = $('.navbar-top').height();
        let _offset = $("#" + params.get('type'));
        if (_offset && _offset.offset()) {
          $('html,body').animate({scrollTop: _offset.offset().top - height_menu},'slow');
        } 
      }, 1000);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let expr = /\/products\?type=/;
        if (event.url.match(expr)){
          let height_menu = $('.navbar-top').height();
          let params = new URLSearchParams(event.url.split('?')[1]);
          if (params && params.get('type')) {
            let _offset = $("#" + params.get('type'));
            if (_offset && _offset.offset()) {
              $('html,body').animate({scrollTop: _offset.offset().top - height_menu},'slow');
            }            
          }
        }        
      }
    });
  }

}