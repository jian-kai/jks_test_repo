import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import $ from "jquery";

@Directive({
  selector: '[validatePassword][formControlName],[validatePassword][formControl],[validatePassword][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => PasswordValidatorDirective), multi: true }
  ]
})
export class PasswordValidatorDirective implements Validator {
  public hintsArr: any = [];
  public i18nMsg : any;
  constructor( @Attribute('idShowHints') public idShowHints: string,
    private translate: TranslateService) {
    this.translate.get('msg').subscribe(res => {
      this.i18nMsg = res.validation;
    });
  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    let v = c.value;
    if (v) {
      this.hintsArr = [];
      let re = /^(?=.*?[a-zA-Z])(?=.*?[0-9])/;
      if(!re.test(v)) {
        this.hintsArr.push({
          status: 'incorrect',
          msg: this.i18nMsg.password_format_contain,
          type: 'password_format_contain', 
        })        
      } else {
        this.hintsArr.push({
          status: 'correct',
          msg: this.i18nMsg.password_format_contain,
          type: 'password_format_contain', 
        })
      }
      
      if (this.getlength(v) < 6) {
        this.hintsArr.push({
          status: 'incorrect',
          msg: this.i18nMsg.password_format_lenght,
          type: 'password_format_lenght', 
        })   
      } else {
        this.hintsArr.push({
          status: 'correct',
          msg: this.i18nMsg.password_format_lenght,
          type: 'password_format_lenght', 
        })
      }
    }else {
      this.hintsArr = [];
    }

    let hasError: boolean;
    if (this.hintsArr && this.hintsArr.length > 0) {
      let html = '';
      $('#' + this.idShowHints).html('');
      this.hintsArr.forEach(element => {
        html += `<li class="list-group-item list-group-item-action ${element.status}">`;
        if (element.status === 'correct') {
          html += `<i class="fa fa-check" aria-hidden="true"></i>`;
        } else {
          html += `<i class="fa fa fa-circle" aria-hidden="true"></i>`;
        }
        html += `<span>${element.msg}</span></li>`;
        if (element.status === 'incorrect') {
          hasError = true;
        }
      });
      
      if (!hasError) {
        if (c.errors) {
          delete c.errors['validatePassword'];
          if (!Object.keys(c.errors).length) c.setErrors(null);
        }
        $('#' + this.idShowHints).html('');
      } else {
        $('#' + this.idShowHints).append(html);
        return {
          validatePassword: true
        }
      }
           
    } else {
      $('#' + this.idShowHints).html('');
      if (c.errors) {
        delete c.errors['validatePassword'];
        if (!Object.keys(c.errors).length) c.setErrors(null);
      }
    }
  }

  getlength(str) {
    let length = 0;
    for (var i = 0; i < str.length; i++) {
      let c : any;
      c = str.charCodeAt(i);
      if (0x0000 <= c && c <= 0x0019) {
          length += 0;
      } else if (0x0020 <= c && c <= 0x1FFF) {
          length += 1;
      } else if (0x2000 <= c && c <= 0xFF60) {
          length += 2;
      } else if (0xFF61 <= c && c <= 0xFF9F) {
          length += 1;
      } else if (0xFFA0 <= c) {
          length += 2;
      }
      
    }
    return length;
  }
}