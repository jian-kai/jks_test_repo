import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[validatePhone][formControlName],[validatePhone][formControl],[validatePhone][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => PhoneValidator), multi: true }
  ]
})
export class PhoneValidator implements Validator {
  constructor() {

  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    let v = c.value;
    /* format :
      (123) 456-7890
      123-456-7890
      123.456.7890
      1234567890
      +31636363634
      075-63546725
    */
    let pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    
    if (pattern.test(v)) {
      if (c.errors) {
        delete c.errors['validateEqual'];
        if (!Object.keys(c.errors).length) c.setErrors(null);
      }
    } else {
      return {
        validatePhone: false
      }
    }

  }
}