import { Directive, OnChanges, OnDestroy, AfterContentInit, QueryList, SimpleChanges } from '@angular/core';
import {RouterLink, RouterLinkWithHref} from '@angular/router';

@Directive({
  selector: '[routerLinkActive]',
  exportAs: 'routerLinkActive',
})
export class RouterLinkActiveDirective implements OnChanges, OnDestroy, AfterContentInit {
  links: QueryList<RouterLink>
  linksWithHrefs: QueryList<RouterLinkWithHref>
  routerLinkActiveOptions: {exact: boolean}
  get isActive(): boolean {
    return; 
  };
  ngAfterContentInit() {
    
  }
  set routerLinkActive(data: string[]|string) {

  }
  ngOnChanges(changes: SimpleChanges) {
    
  };
  ngOnDestroy() {

  }
}
