import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import showdown from 'showdown';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageService } from '../../core/services/storage.service';

@Directive({
  selector: 'appProductDescription'
})
export class ProductDescriptionDirective implements OnInit {
  @Input() productTranslate: Array<any> = [];
  @Input() displayShortDescription?: boolean = true;
  converter: any;

  constructor(
    private el: ElementRef, 
    private translate: TranslateService,
    public storage: StorageService) {
    this.converter = new showdown.Converter({'simpleLineBreaks': true});
  }

  ngOnInit() {
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate = this.productTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    if(this.displayShortDescription) {
      if(translate.shortDescription) {
        this.el.nativeElement.innerHTML = this.converter.makeHtml(translate.shortDescription.replace(/\\n/g, '\n'));
        let p = this.el.nativeElement.querySelectorAll('p');
        for(let i = 0; i < p.length; i++) {
          if(i === p.length - 1) {
            p[i].className += "mb-0";
          }
        }
      } else {
        let description = translate.description.replace(/\\n/g, '\n').slice(0, 200) + '...';
        this.el.nativeElement.innerHTML = this.converter.makeHtml(description);
      }
    } else {
      this.el.nativeElement.innerHTML = this.converter.makeHtml(translate.description.replace(/\\n/g, '\n'));
    }
    let ul = this.el.nativeElement.querySelectorAll('ul');
    // ul.forEach(element => {
    //   element.className += "description-details";
    // });
    for(let i = 0; i < ul.length; i++) {
      // element.style = 'list-style: inside;';
      ul[i].className += "description-details";
    }
  }
}