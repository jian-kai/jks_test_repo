import { Component, OnInit, ElementRef, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';

import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {
  show: boolean = false;
  @Input() id: string;
  @Input() data: any;
  @Output() modalHidden: EventEmitter<any> = new EventEmitter<any>();
  public element: any;
  private modalEle: any;

  constructor(public modalService: ModalService, private el: ElementRef) {
    this.element = $(el.nativeElement);
  }

  ngOnInit(): void {
    // document.ontouchmove = function (e) {
    //   if($('body').hasClass('remove_scroll')){
    //     e.preventDefault();
    //     return true;
    //   }
    // }
    let modal = this;
    this.element.hide();

    // console.log(`init model:: ${this.id}`);

    // ensure id attribute exists
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    // move element to bottom of page (just before </body>) so it can be displayed above everything else
    this.element.appendTo('body');

    // close modal on background click
    this.element.on('click', (e: any) => {
      var target = $(e.target);
      if (!target.closest('.modal-dialog').length) {
        modal.close();
        this.modalService.close(this.id);
      }
    });

    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  setInitData(data): void {
    this.data = data;
  }

  // open modal
  open(): void {
    $('body').addClass('remove_scroll');
    $('body').removeClass('modal-overflow-hidden');
    this.show = true;
    this.modalEle = this.element.find('.modal');
    this.element.show();
    this.element.find('.modal-backdrop').addClass('show');
    this.modalEle.show();
    this.modalEle.addClass('show');
  }

  // close modal
  close(): void {
    $('body').removeClass('remove_scroll');
    $('body').removeClass('modal-overflow-hidden');
    this.show = false;
    this.element.hide();
    this.element.find('.modal-backdrop').removeClass('show');
    this.modalEle.hide();
    this.modalEle.removeClass('show');
  }
}
