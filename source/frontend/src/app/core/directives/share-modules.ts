import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqAnswerDirective } from './faq-answer.directive';

// directives
import { EqualValidator } from './../../core/directives/equal-validator.directive';
import { numberValidator } from './../../core/directives/number-validator.directive';
import { PhoneValidator } from './../../core/directives/phone-validator.directive';
import { ModalComponent } from './../../core/directives/modal/modal.component';
import { AlertComponent } from './../../core/directives/alert/alert.component';
import { LoadingComponent } from './../../core/directives/loading/loading.component';
import { PgwSliderComponent } from './../../core/directives/pgw-slider/pgw-slider.component';
import { ProductTranslateDirective } from './../../core/directives/definition-translate/product-translate/product-translate.directive';
import { ProductsTranslateDirective } from './../../core/directives/definition-translate/products-translate/products-translate.directive';
import { DisplayImageComponent } from './../../core/directives/display-image/display-image.component';
import { RouterLinkActiveDirective } from './../../core/directives/router-link-active.directive';
import { ProductDescriptionDirective } from './../../core/directives/product-description.directive';
import { InputNumberDirective } from './../../core/directives/input-number.directive';
import { DialogConfirmComponent } from './../../core/directives/dialog-confirm/dialog-confirm.component';
import { DialogResultComponent } from './../../core/directives/dialog-result/dialog-result.component';
import { PasswordValidatorDirective } from './../../core/directives/password-validator.directive';
import { ScrollbarDirective } from './../../core/directives/scrollbar.directive';
import { ProductsScrollToDirective } from './../../core/directives/product-scroll-to.directive';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material';

//pipes
import { ProductTranslatePipe } from './../../core/pipes/product-translate.pipe';
import { ProductTypeTranslatePipe } from './../../core/pipes/product-type-translate.pipe';
import { JsonPipePipe } from './../../core/pipes/json-pipe.pipe';
import { TranslatePipe } from './../../core/pipes/translate.pipe';
import { SlugifyPipe } from './../../core/pipes/slugify.pipe';
import { OrderNoPipe } from './../../core/pipes/order-no.pipe';
import { SmsCurrencyPipe } from "./../../core/pipes/sms-currency.pipe";
import { SafeHtmlPipe } from './../../core/pipes/safe-html.pipe';
import { LocaleDateStringPipe } from './../../core/pipes/locale-date-string.pipe';

// components
// import { CartTemplateComponent } from '../../product/components/cart-template/cart-template.component';
import { DeliveryAddressModalFormComponent } from './../../commons/delivery-address/delivery-address-modal-form/delivery-address-modal-form.component';
import { DeliveryAddressesModalComponent } from './../../commons/delivery-address/delivery-addresses-modal/delivery-addresses-modal.component';
import { CardListModalComponent } from './../../commons/card/card-list-modal/card-list-modal.component';
import { CardModalFormComponent } from './../../commons/card/card-modal-form/card-modal-form.component';

import { ChangePlanComponent } from './../../product/components/change-plan/change-plan.component';
import { CollectEmailModalComponent } from './../../core/components/app/collect-email-modal/collect-email-modal.component';
import { CancellationJourneyModalComponent } from './../../core/components/app/cancellation-journey-modal/cancellation-journey-modal.component';
import { FreeTrialModalComponent } from './../../core/components/app/free-trial-modal/free-trial-modal.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';


@NgModule({
    imports: [
      CommonModule,
      TranslateModule,
      MatDialogModule,
      FormsModule,
      ReactiveFormsModule
    ],
    declarations: [
      FaqAnswerDirective, 
      ModalComponent,
      AlertComponent,
      LoadingComponent,
      PgwSliderComponent,
      ProductTranslateDirective,
      ProductsTranslateDirective,
      DisplayImageComponent,
      RouterLinkActiveDirective,
      ProductDescriptionDirective,
      InputNumberDirective,
      PasswordValidatorDirective,
      ScrollbarDirective,
      ProductsScrollToDirective,
      EqualValidator,
      PhoneValidator,
      DialogConfirmComponent,
      DialogResultComponent,
      ProductTranslatePipe,
      ProductTypeTranslatePipe,
      JsonPipePipe,
      TranslatePipe,
      SlugifyPipe,
      OrderNoPipe,
      SafeHtmlPipe,
      SmsCurrencyPipe,
      LocaleDateStringPipe,
      ChangePlanComponent,
      DeliveryAddressModalFormComponent,
      DeliveryAddressesModalComponent,
      CardListModalComponent,
      CardModalFormComponent,
      CollectEmailModalComponent,
      CancellationJourneyModalComponent,
      FreeTrialModalComponent
    ],
    exports: [
      FaqAnswerDirective, 
      ModalComponent,
      AlertComponent,
      LoadingComponent,
      PgwSliderComponent,
      ProductTranslateDirective,
      ProductsTranslateDirective,
      DisplayImageComponent,
      RouterLinkActiveDirective,
      ProductDescriptionDirective,
      InputNumberDirective,
      PasswordValidatorDirective,
      ScrollbarDirective,
      ProductsScrollToDirective,
      EqualValidator,
      PhoneValidator,
      DialogConfirmComponent,
      DialogResultComponent,
      ProductTranslatePipe,
      ProductTypeTranslatePipe,
      JsonPipePipe,
      TranslatePipe,
      SlugifyPipe,
      OrderNoPipe,
      SafeHtmlPipe,
      SmsCurrencyPipe,
      LocaleDateStringPipe,
      ChangePlanComponent,
      DeliveryAddressModalFormComponent,
      DeliveryAddressesModalComponent,
      CardListModalComponent,
      CardModalFormComponent,
      CollectEmailModalComponent,
      CancellationJourneyModalComponent,
      FreeTrialModalComponent
    ],
    entryComponents: [DialogConfirmComponent, DialogResultComponent],
  })
  export class SharedModule {
    static forRoot() {
      return {
        ngModule: SharedModule,
        providers: []
      }
    }
  }