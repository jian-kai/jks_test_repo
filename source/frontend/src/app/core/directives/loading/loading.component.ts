import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  public isLoading: boolean;
  public isLoading1: boolean;
  constructor(private loadingService: LoadingService) { }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
      this.isLoading1 = false;
    })
    this.loadingService.status1.subscribe((value1: boolean) => {
      this.isLoading1 = value1;
    })
  }

}
