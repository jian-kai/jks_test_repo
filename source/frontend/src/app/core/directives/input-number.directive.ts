import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import $ from "jquery";

@Directive({
  selector: '[inputNumber]'
})

export class InputNumberDirective {

  constructor(private el: ElementRef,) {
    $(el.nativeElement).on('change', (e) => {
      if (e.target.value && !/^[0-9 ]+$/.test(e.target.value)) {
        $(el.nativeElement).val('');
      }
    });
  }
  @HostListener('keypress', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    const pattern = /.*[^0-9].*/;
    // let inputChar = String.fromCharCode(event.keyCode);
    let inputChar = String.fromCharCode(event.which);

    if (pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
}
