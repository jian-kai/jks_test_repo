import { Component, OnInit, Input, AfterViewInit, ViewChild, NgModule } from '@angular/core';
import { AppConfig } from 'app/config/app.config';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'display-image',
  templateUrl: './display-image.component.html',
  styleUrls: ['./display-image.component.scss']
})
export class DisplayImageComponent implements AfterViewInit {
  @Input() images: any = [];
  @Input() id: string = "";
  @Input() class: string = "";
  @Input() alt: string = "";
  public urlImage: string = "";
  private apiURL: string = "";
  constructor(public appConfig: AppConfig) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngAfterViewInit() {
    
    if(this.images.length > 0) {
      let i = 0;
      this.images.forEach((element, index) => {
        if(element.isDefault) {
          i = index;
          return;
        }
      });
      setTimeout(_ => this.urlImage = this.images[i].url);
    }
  }

}
