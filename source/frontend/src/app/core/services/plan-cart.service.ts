import { Injectable } from '@angular/core';
import {Item} from '../../core/models/item.model';
import {StorageService} from '../../core/services/storage.service';
import { HttpService } from '../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';

@Injectable()
export class PlanCartService {
  public cart: Item[] = [];
  constructor(public storage: StorageService,
    private httpService: HttpService,
    public appConfig: AppConfig
  ) { }

  // add product item to cart
  addItem(item: Item) {
    let isNull = false;
    if (!this.getPlanCartItems() || this.getPlanCartItems().length <= 0) {
      isNull = true;
    }
    //productImageURL
    if (!item.product.productImageURL && item.product.productImages.length > 0) {
      item.product.productImageURL = '';
      item.product.productImages.forEach(element => {
        if (element.isDefault) {
          item.product.productImageURL = element.url;
        }
      });
      if (item.product.productImageURL === '') {
        item.product.productImageURL = item.product.productImages[0].url;
      }
    }
    if (isNull) {
      this.cart.push(item);
      this.storage.setPlanCartItem(this.cart);
      return;
    }
    this.cart = this.getPlanCartItems();
    let isExists = false;
    this.cart.forEach( (cartItem, index ) => {
      if (cartItem.product.id === item.product.id) {
        isExists = true;
        if (typeof item.qty === 'string') {
          item.qty = parseInt(item.qty);
        }
        cartItem.qty += item.qty;
        this.cart[index] = cartItem;
      }
    });
    if (!isExists) {
      this.cart.push(item);
    }
    
    this.storage.setPlanCartItem(this.cart);
  }

  // get plan cart items
  getPlanCartItems(): Item[] {
    return this.storage.getPlanCartItems();
  }

  // get plan cart
  getPlanCart() {
    return this.storage.getPlanCart();
  }

  //remove item
  removeItem(item: Item) {
    this.cart = this.getPlanCartItems();
    this.cart = this.cart.filter(cartItem => cartItem.product.id !== item.product.id);
    this.storage.setPlanCartItem(this.cart);
    return this.getPlanCart();
  }

  // refresh item
  refreshItem(item: Item): Item[] {
    this.cart = this.getPlanCartItems();
    let tmpItem = this.cart.reduce((getItem, cartItem)=> {
      if (cartItem.id === item.id) {
        if (typeof item.qty === 'string') {
          item.qty = parseInt(item.qty);
        }
        cartItem.qty = item.qty, getItem;
      }
      return getItem.push(cartItem), getItem;
    }, [])
    this.cart = tmpItem;
    this.storage.setPlanCartItem(this.cart);
    return this.getPlanCart();
  }

  // getTotalPrice
  getSubTotalPrice() {
    
    if (!this.getPlanCartItems()) {
      return 0;
    } else {
      //return 0;
    }
    this.cart = this.getPlanCartItems();
    // let totalPrice = this.cart.reduce((sum, cartItem) => {
    //   return sum += cartItem.product.productCountry.sellPrice, sum;
    // }, 0)

    let totalPrice = 0;

    this.cart.forEach( (cartItem, index ) => {
      if (cartItem.product.productCountry)
        totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty; 
    });

    return totalPrice;
  }
  // shipping free
  getShippingFree() {
    return 0;
  }

  // getPromoCode
  getPromoCode() {
    return {code: 'abcxyz', price: 0};
  }
  getTotalPrice() {
    if (!this.getPlanCartItems()) {
      return 0;
    }
    let getSubTotalPrice = this.getSubTotalPrice();
    return getSubTotalPrice + this.getShippingFree() + this.getPromoCode().price;
  }

  // clear cart
  clearCart() {
    this.storage.clearPlan();
  }

}