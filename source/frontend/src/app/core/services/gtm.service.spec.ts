import { TestBed, inject } from '@angular/core/testing';

import { GTMService } from './gtm.service';

describe('GTMService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GTMService]
    });
  });

  it('should be created', inject([GTMService], (service: GTMService) => {
    expect(service).toBeTruthy();
  }));
});
