import { Injectable, Output, EventEmitter } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as $ from 'jquery';

@Injectable()
export class ModalService {
  @Output() modelChange: EventEmitter<Object> = new EventEmitter();
  private modals: any[] = [];
  public status = new BehaviorSubject<Boolean>(false); // status : show: true|hide: false

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    let tmp = [];
    this.modals.forEach(model => {
      if(model.id !== id) {
        tmp.push(model);
      }
    });
    this.modals = tmp;
  }

  open(id: string, options?: any) {
    // open modal specified by id
    let modal = this.modals.find(modal => modal.id === id);
    if (modal) {
      // console.log(`modals service111::: ${id}`);
      // call init component
      // console.log('model component init---- ');
      if(modal.onInit && (typeof modal.onInit === 'function')) {
        modal.onInit(options);
      }
      if(options) {
        modal.setInitData(options);
      }
      modal.open();
      this.status.next(true);
      $('body').addClass('modal-overflow-hidden');

      // trigger event change
      this.modelChange.emit({id: id, stage: 'open'});
    } else {
      // console.log(`modals service222::: ${id}`);
    }
  }

  close(id: string) {
    // close modal specified by id
    $('body').removeClass('modal-overflow-hidden');
    let modal = this.modals.find(modal => modal.id === id);
    modal.close();
    // trigger event change
    this.modelChange.emit({id: id, stage: 'close'});
  }

  reset(id: string) {
    let modal = this.modals.find(modal => modal.id === id);
    modal.resError = null;
    let form = modal.el.nativeElement.querySelector("form");
    form.insertAdjacentHTML('beforeend', '<input type="reset" style="width: 0;height: 0;opacity: 0;display: none;visibility: hidden;">');
    form.querySelector("[type='reset']").click();
    form.querySelector("[type='reset']").remove();
  }

}
