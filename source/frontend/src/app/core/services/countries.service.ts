import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { StorageService } from './storage.service';

@Injectable()
export class CountriesService {
  @Output() country: EventEmitter<Object> = new EventEmitter();
  @Output() countriesChanged: EventEmitter<Object> = new EventEmitter();
  @Output() language: EventEmitter<Object> = new EventEmitter();
  countries: Array<any>;
  currentCountry: any;

  constructor(
    private http: Http,
    public storageService: StorageService
  ) { }

  changeCountry(country) {
    // store country into local store
    this.storageService.setCountry(country);
    this.currentCountry = country;
    // emit country change
    this.country.emit(country);
  }

  setCountries(countries) {
    this.countries = countries;
    this.countriesChanged.emit(countries);
    this.storageService.setCountries(countries);
  }

  getCountries() {
    return this.countries;
  }

  changeLanguage() {
    this.language.emit(true);
  }
}
