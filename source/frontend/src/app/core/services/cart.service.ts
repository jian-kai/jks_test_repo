import { Injectable, Output, EventEmitter } from '@angular/core';
import { Item } from '../../core/models/item.model';
import { StorageService } from '../../core/services/storage.service';
import { HttpService } from '../../core/services/http.service';
import { GTMService } from '../../core/services/gtm.service';
import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../core/services/global.service';

@Injectable()
export class CartService {
  promotion: any = {};
  @Output() hasUpdate: EventEmitter<boolean> = new EventEmitter(true);
  // @Output() hasUpdateShippingFee: EventEmitter<boolean> = new EventEmitter(true);
  @Output() justAddedItem: EventEmitter<Object> = new EventEmitter();
  public cart: Item[] = [];
  public cartRules: any;
  // public receiptForCustom: any;
  // public receipt: any;
  constructor(public storage: StorageService,
    private httpService: HttpService,
    public appConfig: AppConfig,
    private globalService: GlobalService,
    private gtmService: GTMService
  ) {
    
    // console.log(``)
    // this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
    //   data => {
    //     this.cartRules = data;
    //   }
    // )
  }

  applyAddItemEvent() {
    this.justAddedItem.emit(true);
  }

  reloadSubscriptions() {
    let currentUser = this.storage.getCurrentUserItem();
    if (currentUser) {
      this.httpService._getList(`${this.appConfig.config.api.subscription}`.replace('[userID]', currentUser.id)).subscribe(
        data => {
          currentUser.subscriptions = data;
          this.storage.setCurrentUser(currentUser);
        },
        error => {
          // console.log(error.error)
        }
      )
    }
  }

  getSubscriptionsByUser(currentUser) {
      return new Promise((resolve, reject) => {
        this.httpService._getList(`${this.appConfig.config.api.subscription}`.replace('[userID]', currentUser.id)).subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        )
      });
  }
  getSubscriptionsByGuest(_email: string) {
    // check email is exist or not
    return new Promise((resolve, reject) => {
      this.httpService._create(this.appConfig.config.api.checkEmail, { email: _email }).subscribe(
        data => {
          resolve(data);
        },
        error => {
          reject(error);
        }
      )
    });
  }

  // get shipping fee configuration
  getShippingFeeConfig() {

  }

  updateCart() {
    this.hasUpdate.emit(true);
    return this.saveCartToDB()
      .then(() => {}) // console.log('succeed'))
      .catch(error => {} // console.log(error));
  );
  }

  // add product item to cart
  addItem(item: Item) {
    let isNull = false;
    this.cart = this.getCart();
    if (!this.cart) {
      this.cart = [];
      isNull = true;
    }
    //productImageURL
    if(item.product) {
      // process for product
      if (!item.product.productImageURL && item.product.productImages.length > 0) {
        item.product.productImageURL = '';
        item.product.productImages.forEach(element => {
          if (element.isDefault) {
            item.product.productImageURL = element.url;
          }
        });
        if (item.product.productImageURL === '') {
          item.product.productImageURL = item.product.productImages[0].url;
        }
      }
    }
    if(item.plan) {
      // process for subscription
      if (!item.plan.planImageURL && item.plan.planImages.length > 0) {
        item.plan.planImageURL = '';
        item.plan.planImages.forEach(element => {
          if (element.isDefault) {
            item.plan.planImageURL = element.url;
          }
        });
        if (item.plan.planImageURL === '') {
          item.plan.planImageURL = item.plan.planImages[0].url;
        }
      }
    }

    if (isNull) {
      this.cart.push(item);
      this.storage.setCartItem(this.cart);
      return;
    }
    this.cart = this.getCart();
    let isExists = false;
    this.cart.forEach( (cartItem, index ) => {
      if(cartItem.product && item.product) {
        if (cartItem.product.id === item.product.id && cartItem.isFree === item.isFree) {
          isExists = true;
          if (typeof item.qty === 'string') {
            item.qty = parseInt(item.qty);
          }
          if (typeof cartItem.qty === 'string') {
            cartItem.qty = parseInt(cartItem.qty);
          }
          cartItem.qty += item.qty;
          this.cart[index] = cartItem;
        }
      }
      if(cartItem.plan && item.plan) {
        if (cartItem.plan.id === item.plan.id) {
          isExists = true;
          if (typeof item.qty === 'string') {
            item.qty = parseInt(item.qty);
          }
          if (typeof cartItem.qty === 'string') {
            cartItem.qty = parseInt(cartItem.qty);
          }
          cartItem.qty += item.qty;
          this.cart[index] = cartItem;
        }
      }
    });
    if (!isExists) {
      this.cart.push(item);
    }
    this.storage.setCartItem(this.cart);
    this.gtmService.addCart(item);
  }

  //save cart to DB
  saveCartToDB() {
    return new Promise((resolve, reject) => {
      let curUser = this.storage.getCurrentUserItem();
      if (curUser) {
        this.cart = this.getCart();
        let _carts = {carts:[]};
        _carts.carts = [];
        if(this.cart) {
          this.cart.forEach(ele => {
            if(ele.product) {
              _carts.carts.push({ProductCountryId: ele.product.productCountry.id, qty: ele.qty})
            }
            if(ele.plan) {
              _carts.carts.push({PlanCountryId: ele.plan.planCountry.id, qty: ele.qty})
            }
          });
        }
        this.httpService._create(`${this.appConfig.config.api.carts}`.replace('[userID]', curUser.id), _carts)
        .subscribe(
          data => {
            resolve();
          },
          error => {
            reject(error);
          }
        )
      }
    });
  }

  // get cart item
  getCart(): Item[] {
    let country = this.storage.getCountry();
    let cartItems = this.storage.getCartItem();
    let countryCart = [];
    if (cartItems && cartItems[country.code] && country.isWebEcommerce) {
      countryCart = cartItems[country.code];
    }
    return countryCart;
  }

  //remove item
  removeItem(item: Item, removeForUndo?: boolean) {
    this.cart = this.getCart();
    let cartTemp: Item[] = [];

    if(item.product) {
      // this.cart = this.cart.filter(cartItem => cartItem.product.id !== item.product.id); 
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.product) {
          if(cartItem.product.id !== item.product.id || cartItem.isFree !== item.isFree) {
            cartTemp.push(cartItem);
          } else if(removeForUndo) {
            cartItem['isRemove'] = true;
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    if(item.plan) {
      // this.cart = this.cart.filter(cartItem => cartItem.plan.id !== item.plan.id); 
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.plan) {
          if(cartItem.plan.id !== item.plan.id) {
            cartTemp.push(cartItem);
          } else if(removeForUndo) {
            cartItem['isRemove'] = true;
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    this.cart = cartTemp;
    this.storage.setCartItem(this.cart);
    // if(changePlan) {
    // } else {
    //   this.storage.setProductsRemoved(item);
    // }
    this.gtmService.removeCart(item);
    return this.updateCart()
      .then(() => this.cart)
      .catch(error => {} // console.log(error));
    );
  }

  // refresh item
  refreshItem(_cartItems: any): Item[] {
    this.cart = this.getCart();
    let tmpItem: any;
    _cartItems.forEach(item => {
      tmpItem = this.cart.reduce((getItem, cartItem)=> {
        if(cartItem.product && item.product) {
          if (cartItem.product.id === item.product.id && cartItem.isFree === item.isFree) {
            if (typeof item.qty === 'string') {
              item.qty = parseInt(item.qty);
            }
            cartItem.qty = item.qty, getItem;
          }
        }
        if(cartItem.plan && item.plan) {
          if (cartItem.plan.id === item.plan.id) {
            if (typeof item.qty === 'string') {
              item.qty = parseInt(item.qty);
            }
            cartItem.qty = item.qty, getItem;
          }
        }
        return getItem.push(cartItem), getItem;

      }, [])
    });
    this.cart = tmpItem;
    this.storage.setCartItem(this.cart);
    this.updateCart();
    return this.getCart();
  }

  // changeShipping(shippingFee) {
  //   this.receipt.shippingFee = shippingFee;
  //   this.receipt.totalIclTax = this.receipt.subTotal + +this.receipt.shippingFee;
  //   this.hasUpdateShippingFee.emit(true);
  // }

  // calculate receipt amount
  getReceipt(cartRules?: any) {
    // if(!cartRules) {
    //   return this.receipt;
    // } else {
      let receipt = {
        totalPrice: 0,
        totalPriceNotDivided: 0,
        specialDiscount: 0,
        discount: 0,
        subTotal: 0,
        shippingFee: 0,
        // customOneDayShippingFee: 0,
        // customTwoDayShippingFee: 0,
        // customGroundShippingFee: 0,
        totalIclTax: 0,
        taxRate: 0,
        applyDiscount: 0,
        taxName: ''
      }

      let currentUser = this.storage.getCurrentUserItem();

      this.cart = this.getCart();
      if (!this.cart || !(Array.isArray(this.cart))) {
        return receipt;
      }

      // check cart has subscription
      let hasSubscription: boolean = false;
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.plan) {
          hasSubscription = true;
        }

      });


      if(cartRules) {
        receipt = this.caculationDiscountPrice(receipt, currentUser, hasSubscription, cartRules);
      }
  
      // // calculate discount price and total price
      // this.cart.forEach( (cartItem, index ) => {
      //   if(cartItem.product) {
      //     // process for product
      //     receipt.totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty;
      //     receipt.totalPriceNotDivided += +cartItem.product.productCountry.sellPrice * cartItem.qty;

      //     this.cartRules.forEach( (cartRuleItem, index ) => {
      //       if(cartRuleItem.productSKU) {
      //         if(currentUser && currentUser.subscriptions.length > 0
      //           && cartItem.product.sku === cartRuleItem.productSKU && cartRuleItem.isActiveWeb) {
      //           receipt.specialDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (parseInt(cartRuleItem.discount) / 100);
      //         }
      //       } else if(hasSubscription && cartRuleItem.isActiveWeb) {
      //         receipt.specialDiscount += (cartItem.product.productCountry.sellPrice * cartItem.qty) * (parseInt(cartRuleItem.discount) / 100);
      //       } else if(this.promotion && this.promotion.productCountryIds && 
      //                 (this.promotion.productCountryIds.indexOf(cartItem.product.productCountry.id) > -1 || this.promotion.productCountryIds === 'all')) {
      //         receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
      //       }
      //     });
      //   }
      //   if(cartItem.plan) {
      //     if(cartItem.plan.planCountry[0]) {
      //       cartItem.plan.planCountry = cartItem.plan.planCountry[0];
      //     }
      //     // process for subscription
      //     receipt.totalPrice += +(cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes;
      //     receipt.totalPriceNotDivided += +cartItem.plan.planCountry.sellPrice * cartItem.qty;
          
      //     // calculate discount
      //     if(this.promotion && this.promotion.planCountryIds && 
      //               (this.promotion.planCountryIds.indexOf(cartItem.plan.planCountry.id) > -1 || this.promotion.planCountryIds === 'all')) {
      //       receipt.discount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes) * (this.promotion.discount / 100);
      //     }
      //   }
      // });
      // // // console.log('this.promotion',this.promotion);
      // // check discount for promo: WELCOME10
      // if(receipt.specialDiscount > 0) {
      //   receipt.discount = receipt.specialDiscount;
      // } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
      //   receipt.discount = this.promotion.maxDiscount;
      // }

      // // calculate subTotal
      // receipt.subTotal = receipt.totalPrice - receipt.discount;

      // calculate shipping fee
      if(receipt.totalPriceNotDivided < this.appConfig.getConfig('shippingFeeConfig').minAmount && this.getTotalQty() > 0) {
        receipt.shippingFee = this.appConfig.getConfig('shippingFeeConfig').fee;
        // this.receipt.customOneDayShippingFee = this.appConfig.getConfig('shippingFeeConfig').customOneDayShippingFee;
        // this.receipt.customTwoDayShippingFee = this.appConfig.getConfig('shippingFeeConfig').customTwoDayShippingFee;
        // this.receipt.customGroundShippingFee = this.appConfig.getConfig('shippingFeeConfig').customGroundShippingFee;
      }
      // if(hasSubscription || (this.promotion && this.promotion.isFreeShipping)) {
      if(this.promotion && this.promotion.isFreeShipping) {
        receipt.shippingFee = 0;
      }

      // get tax
      let currentCountry = this.storage.getCountry();
      receipt.taxRate = currentCountry.taxRate;
      receipt.taxName = currentCountry.taxName;

      // calulate total include tax
      receipt.totalIclTax = receipt.subTotal + +receipt.shippingFee;
      if(!currentCountry.includedTaxToProduct) {
        // receipt.totalIclTax = receipt.totalIclTax + (receipt.totalIclTax * (receipt.taxRate / 100));
      }

      return receipt;
    // }
  }

  caculationDiscountPrice(receipt, currentUser, hasSubscription, cartRules) {
    // calculate special discount price and total price
    this.cart.forEach( (cartItem, index ) => {
      if(cartItem.product && !cartItem.isFree && !cartItem.isRemove) {
        // process for product
        receipt.totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty;
        receipt.totalPriceNotDivided += +cartItem.product.productCountry.sellPrice * cartItem.qty;

        cartRules.forEach( (cartRuleItem, index ) => {
          if(cartRuleItem.productSKU) {
            if(currentUser && currentUser.subscriptions.length > 0
              && cartItem.product.sku === cartRuleItem.productSKU && cartRuleItem.isActiveWeb) {
              receipt.specialDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (parseInt(cartRuleItem.discount) / 100);
            }
          } else if(hasSubscription && cartRuleItem.isActiveWeb) {
            receipt.specialDiscount += (cartItem.product.productCountry.sellPrice * cartItem.qty) * (parseInt(cartRuleItem.discount) / 100);
          }
        });
      }
      if(cartItem.plan && !cartItem.isRemove) {
        if(cartItem.plan.planCountry[0]) {
          cartItem.plan.planCountry = cartItem.plan.planCountry[0];
        }
        // process for subscription
        receipt.totalPrice += +(cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes;
        receipt.totalPriceNotDivided += +cartItem.plan.planCountry.sellPrice * cartItem.qty;
      }
    });

    // calculate discount price
    // if(this.promotion.minSpend && receipt.totalPriceNotDivided < this.promotion.minSpend) {
    if(this.promotion.minSpend && receipt.totalPrice < this.promotion.minSpend) {
    } else {
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.product && !cartItem.isFree && !cartItem.isRemove) {
          // cartRules.forEach( (cartRuleItem, index ) => {
          //   if(cartRuleItem.productSKU) {
          //   } else if(hasSubscription && cartRuleItem.isActiveWeb) {
            // } else 
            if(this.promotion && this.promotion.productCountryIds && 
                      (this.promotion.productCountryIds.indexOf(`${cartItem.product.productCountry.id}`) > -1 || this.promotion.productCountryIds === 'all')) {
              receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
              receipt.applyDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty;
            }
          // });
        }
        if(cartItem.plan && !cartItem.isRemove) {
          if(cartItem.plan.planCountry[0]) {
            cartItem.plan.planCountry = cartItem.plan.planCountry[0];
          }
          // calculate discount
          if(this.promotion && this.promotion.planCountryIds && 
                    (this.promotion.planCountryIds.indexOf(`${cartItem.plan.planCountry.id}`) > -1 || this.promotion.planCountryIds === 'all')) {
            receipt.discount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes) * (this.promotion.discount / 100);
            receipt.applyDiscount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes);
          }
        }
      });
      
      // check discount for promo: WELCOME10
      if(receipt.specialDiscount > 0) {
        receipt.discount = receipt.specialDiscount;
      } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
        receipt.discount = this.promotion.maxDiscount;
      }
    }

    // calculate subTotal
    receipt.subTotal = receipt.totalPrice - receipt.discount;

    return receipt;
  }

  setPromotion(promotion?) {
    if(promotion) {
      this.promotion = promotion;
    } else {
      this.promotion = {};
    }
    this.hasUpdate.emit(true);
  }

  getTotalQty() {
    if (!this.getCart()) {
      return 0;
    }
    this.cart = this.getCart();
    let totalQty = 0;

    this.cart.forEach( (cartItem, index ) => {
      if(!cartItem.isRemove) {
        if (cartItem.product) {
          totalQty += cartItem.qty;
        }
        if (cartItem.plan) {
          totalQty += cartItem.qty;
        }
      }
    });

    return totalQty;
  }

  // getPromoCode
  getPromoCode() {
    // return {code: 'abcxyz', price: 0};
    return this.promotion;
  }

  // clear cart
  clearCart(removeAll ?: any) {
    this.storage.clearCart(removeAll);
    this.cart = [];
    this.setPromotion();
    // emit cart change event
    this.hasUpdate.emit(true);
  }

  //sync Cart
  syncCart() {
    // this.cart = this.getCart();
    let curUser = this.storage.getCurrentUserItem();
    if (curUser && curUser.Cart && curUser.Cart.cartDetails.length > 0) {
      // for case: history of a shopping cart of a user is not null
      curUser.Cart.cartDetails.forEach(item => {
        this.addItem(item);
      })
    } else {
      // for case: history of a shopping cart of a user is null and localstorage cart is not null
      this.saveCartToDB()
        .catch(error => {}// console.log(error));
      );
    }
  }
  // reload cart by country
  reloadCart() {
    return new Promise((resolve, reject) => {
      let curUser = this.storage.getCurrentUserItem();
      this.httpService._getList(`${this.appConfig.config.api.carts}`.replace('[userID]', curUser.id))
      .subscribe(
        data => {
          resolve(data);
        },
        error => {
          reject(error);
        }
      )
    })
  }
  
  //undo item
  undoItem(item: Item) {
    this.cart = this.getCart();
    let cartTemp: Item[] = [];

    if(item.product) {
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.product) {
          if(cartItem.product.id !== item.product.id) {
            cartTemp.push(cartItem);
          } else {
            cartItem['isRemove'] = false;
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    if(item.plan) {
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.plan) {
          if(cartItem.plan.id !== item.plan.id) {
            cartTemp.push(cartItem);
          } else {
            cartItem['isRemove'] = false;
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    this.cart = cartTemp;
    this.storage.setCartItem(this.cart);
    return this.updateCart()
      .then(() => this.cart)
      .catch(error => {} // console.log(error));
    );
  }

  //clear item removed
  clearItemRemoved() {
    this.cart = this.getCart();
    let cartTemp: Item[] = [];

    this.cart.forEach( (cartItem, index ) => {
      if(!cartItem.isRemove) {
        cartTemp.push(cartItem);
      }
    });
    
    this.cart = cartTemp;
    this.storage.setCartItem(this.cart);
    return this.saveCartToDB()
      .then(() => {})// console.log('succeed'))
      .catch(error => {}// console.log(error));
    );
  }

  //check free product with cart rule
  checkFreeProduct(cartRules: any) {
    this.cart = this.getCart();

    cartRules.forEach( (cartRuleItem, index ) => {
      if(cartRuleItem.freeProductIds && cartRuleItem.isActiveWeb) {
        let hasProductForFree = false;
        let hasFreeProduct = false;

        this.cart.forEach( (cartItem, index ) => {
          if(cartItem.product) {
            if(JSON.parse(cartRuleItem.productIds).find(id => id === cartItem.product.productCountry.id) && !cartItem.isRemove && !cartItem.isFree) {
              hasProductForFree = true;
            }
            if(JSON.parse(cartRuleItem.freeProductIds).find(id => id === cartItem.product.productCountry.id) && cartItem.isFree) {
              hasFreeProduct = true;
            }
          }
        });

        if((!hasProductForFree && hasFreeProduct)) {
          cartRuleItem.freeProductSlugs.forEach(freeProductSlug => {
            this.doAddOrRemoveFreeItem('remove', freeProductSlug);
          });
        } else if(hasProductForFree && !hasFreeProduct) {
          cartRuleItem.freeProductSlugs.forEach(freeProductSlug => {
          this.doAddOrRemoveFreeItem('add', freeProductSlug);
        });
        }
      }
    })
  }

  //get product detail
  doAddOrRemoveFreeItem(_param, _slug) {
    let options = {
      countryCode: 'mys',
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getDetail(`${this.appConfig.config.api.products}/${_slug}?` + params.toString()).subscribe(
      data => {
        let item = {
          id: data.id,
          product: data,
          qty: 1,
          isFree: true
        }
        if(_param === 'add') {
          this.addItem(item);
          this.updateCart()
            .then(() => this.cart)
            .catch(error => {}// console.log(error));
          );
        }
        if(_param === 'remove') {
          this.removeItem(item);
        }
      },
      error => {
        // console.log(error.error);
      }
    )
  }

  // changeCustomShipping(shippingFee) {
  //   this.receiptForCustom.shippingFee = shippingFee;
  //   this.receiptForCustom.totalIclTax = this.receiptForCustom.subTotal + +this.receiptForCustom.shippingFee;
  //   this.hasUpdateShippingFee.emit(true);
  // }

  // calculate receipt amount
  getReceiptForCustomPlan(cartRules?: any, cart?: any) {
    // if(!cartRules && !cart) {
    //   return this.receiptForCustom;
    // } else {
      let receiptForCustom = {
        totalPrice: 0,
        totalPriceNotDivided: 0,
        specialDiscount: 0,
        discount: 0,
        subTotal: 0,
        shippingFee: 0,
        // customOneDayShippingFee: 0,
        // customTwoDayShippingFee: 0,
        // customGroundShippingFee: 0,
        totalIclTax: 0,
        taxRate: 0,
        applyDiscount: 0,
        taxName: ''
      }
  
      let currentUser = this.storage.getCurrentUserItem();
  
      // this.cart = this.getCart();
      if (!cart || !(Array.isArray(cart))) {
        return receiptForCustom;
      }
  
      // check cart has subscription
      let hasSubscription: boolean = false;
      cart.forEach( (cartItem, index ) => {
        if(cartItem.plan) {
          hasSubscription = true;
        }
  
      });
  
  
      if(cartRules) {
        receiptForCustom = this.caculationDiscountPriceForCustomPlan(receiptForCustom, currentUser, hasSubscription, cartRules, cart);
      }
  
      // calculate shipping fee
      // if(receiptForCustom.totalPriceNotDivided < this.appConfig.getConfig('shippingFeeConfig').minAmount && this.getTotalQty() > 0) {
      //   receiptForCustom.shippingFee = this.appConfig.getConfig('shippingFeeConfig').fee;
      //   this.receiptForCustom.customOneDayShippingFee = this.appConfig.getConfig('shippingFeeConfig').customOneDayShippingFee;
      //   this.receiptForCustom.customTwoDayShippingFee = this.appConfig.getConfig('shippingFeeConfig').customTwoDayShippingFee;
      //   this.receiptForCustom.customGroundShippingFee = this.appConfig.getConfig('shippingFeeConfig').customGroundShippingFee;
      // }
      // if(hasSubscription || (this.promotion && this.promotion.isFreeShipping)) {
      if(this.promotion && this.promotion.isFreeShipping) {
        receiptForCustom.shippingFee = 0;
        // this.receiptForCustom.customOneDayShippingFee = 0;
        // this.receiptForCustom.customTwoDayShippingFee = 0;
        // this.receiptForCustom.customGroundShippingFee = 0;
      }
  
      // get tax
      let currentCountry = this.storage.getCountry();
      receiptForCustom.taxRate = currentCountry.taxRate;
      receiptForCustom.taxName = currentCountry.taxName;
  
      // calulate total include tax
      receiptForCustom.totalIclTax = receiptForCustom.subTotal + +receiptForCustom.shippingFee;
      // this.receiptForCustom.totalIclTax = this.receiptForCustom.subTotal;
      if(!currentCountry.includedTaxToProduct) {
        // receipt.totalIclTax = receipt.totalIclTax + (receipt.totalIclTax * (receipt.taxRate / 100));
      }
  
      return receiptForCustom;
    // }
  }

  caculationDiscountPriceForCustomPlan(receipt, currentUser, hasSubscription, cartRules, cart) {
    // calculate special discount price and total price
    cart.forEach( (cartItem, index ) => {
      if(cartItem.product && !cartItem.isFree && !cartItem.isRemove) {
        // process for product
        receipt.totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty;
        receipt.totalPriceNotDivided += +cartItem.product.productCountry.sellPrice * cartItem.qty;

        cartRules.forEach( (cartRuleItem, index ) => {
          if(cartRuleItem.productSKU) {
            if(currentUser && currentUser.subscriptions.length > 0
              && cartItem.product.sku === cartRuleItem.productSKU && cartRuleItem.isActiveWeb) {
              receipt.specialDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (parseInt(cartRuleItem.discount) / 100);
            }
          } else if(hasSubscription && cartRuleItem.isActiveWeb) {
            receipt.specialDiscount += (cartItem.product.productCountry.sellPrice * cartItem.qty) * (parseInt(cartRuleItem.discount) / 100);
          }
        });
      }
      if(cartItem.plan && !cartItem.isRemove) {
        if(cartItem.plan.planCountry[0]) {
          cartItem.plan.planCountry = cartItem.plan.planCountry[0];
        }
        // process for subscription
        receipt.totalPrice += +(cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes;
        receipt.totalPriceNotDivided += +cartItem.plan.planCountry.sellPrice * cartItem.qty;
      }
    });

    // calculate discount price
    // if(this.promotion.minSpend && receipt.totalPriceNotDivided < this.promotion.minSpend) {
    if(this.promotion.minSpend && receipt.totalPrice < this.promotion.minSpend) {
    } else {
      cart.forEach( (cartItem, index ) => {
        if(cartItem.product && !cartItem.isFree && !cartItem.isRemove) {
          // cartRules.forEach( (cartRuleItem, index ) => {
          //   if(cartRuleItem.productSKU) {
          //   } else if(hasSubscription && cartRuleItem.isActiveWeb) {
            // } else 
            if(this.promotion && this.promotion.productCountryIds && 
                      (this.promotion.productCountryIds.indexOf(`${cartItem.product.productCountry.id}`) > -1 || this.promotion.productCountryIds === 'all')) {
              receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
              receipt.applyDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty;
            }
          // });
        }
        if(cartItem.plan && !cartItem.isRemove) {
          if(cartItem.plan.planCountry[0]) {
            cartItem.plan.planCountry = cartItem.plan.planCountry[0];
          }
          // calculate discount
          if(this.promotion && this.promotion.planCountryIds && 
                    (this.promotion.planCountryIds.indexOf(`${cartItem.plan.planCountry.id}`) > -1 || this.promotion.planCountryIds === 'all')) {
            receipt.discount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes) * (this.promotion.discount / 100);
            receipt.applyDiscount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes);
          }
        }
      });
      
      // check discount for promo: WELCOME10
      if(receipt.specialDiscount > 0) {
        receipt.discount = receipt.specialDiscount;
      } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
        receipt.discount = this.promotion.maxDiscount;
      }
    }

    // calculate subTotal
    receipt.subTotal = receipt.totalPrice - receipt.discount;

    return receipt;
  }
}
