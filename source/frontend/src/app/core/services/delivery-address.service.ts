import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { lengthValidator } from '../../core/validators/length.validator';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class DeliveryAddressService {
  @Output() deliveryAddressAction: EventEmitter<any> = new EventEmitter<any>();
  constructor(private builder: FormBuilder,
    public storage: StorageService,) { }

  setDeliveryAddressAction(_data) {
    this.deliveryAddressAction.emit(_data);
  }

  // initialize Product Translate
  initAddress() {
    if(this.storage.getCountry().code !== 'SGP') {
      return this.builder.group({
        id: [undefined],
        // email: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        callingCode: [''],
        contactNumber: ['', [Validators.required, lengthValidator({min: 8})]],
        address: ['', [Validators.required, lengthValidator({max: 150})]],
        state: ['', Validators.required],
        city: ['', Validators.required],
        portalCode: ['', [Validators.required, lengthValidator({min: 5})]],
      });
    } else {
      return this.builder.group({
        id: [undefined],
        // email: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        callingCode: [''],
        contactNumber: ['', [Validators.required, lengthValidator({min: 8,max: 8})]],
        address: ['', [Validators.required, lengthValidator({max: 150})]],
        state: ['', Validators.required],
        city: ['', Validators.required],
        portalCode: ['', [Validators.required, lengthValidator({min: 6})]],
      });
    }
  }

  initContact() {
    return this.builder.group({
      email: ['', Validators.required],
    });
  }

}
