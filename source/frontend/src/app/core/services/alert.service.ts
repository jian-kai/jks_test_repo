import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  constructor() { }

  success(msg: any) {
    this.subject.next({type: 'success', texts: msg});
  }

  error(msg: any) {
    msg.forEach((element, index) => {
      msg[index] = JSON.parse(element).message;
    });
    this.subject.next({type: 'error', texts: msg});
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  clearMessage() {
    this.error([]);
    this.success([]);
  }

}
