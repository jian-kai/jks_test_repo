import { Injectable } from '@angular/core';
import { CartService } from '../../core/services/cart.service';
import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../core/services/global.service';
import { HttpService } from '../../core/services/http.service';
import { StorageService } from '../../core/services/storage.service';
import { LoadingService } from '../../core/services/loading.service';

declare var jQuery: any;
import * as $ from 'jquery';

@Injectable()
export class PlanService {

  constructor(private globalService: GlobalService,
    public appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    public storage: StorageService,) { }

  // groupPlan(_data: any) {
  //   // console.log('_data', _data)
  //   // let planPrefix: any = ['Y-Sub-', 'Sub-', 'M-Sub-'];
  //   let planPrefix: any = [];
  //   _data.forEach(plan => {
  //     if (!planPrefix.includes(plan.PlanType.prefix)) {
  //       planPrefix.push(plan.PlanType.prefix);
  //     }
  //   });
  //   let groupName: any = {};
  //   _data.forEach(plan => {
  //     let sku = plan.sku;
  //     planPrefix.forEach(value => {
  //       if (sku.startsWith(value)) {
  //         let regex = new RegExp('^' + value,"g");
  //         let replaced = sku.replace(regex, "");
  //         groupName[replaced] = replaced;
  //       }
  //     });
  //   });
  //   let groupPlan : any = [];
  //   let i = 0;
  //   for (var key in groupName) {
  //     if (groupName.hasOwnProperty(key)) {
  //         // console.log(key + " -> " + groupName[key]);
  //         groupPlan[i] = {};
  //         groupPlan[i]['name'] = null;
  //         groupPlan[i]['item'] = null;
  //         groupPlan[i]['name'] = groupName[key];
  //         let tmp : any = [];
  //         planPrefix.forEach(value => {
  //           let fullName = value + groupName[key];
  //           _data.forEach(plan => {
  //             let sku = plan.sku;
  //             if (sku === fullName) {
  //               plan.groupName = groupName[key];
  //               tmp.push(plan);
  //             }
  //           })
  //         });
  //         groupPlan[i]['item'] = tmp;
  //     }
  //     i++;
  //   }
  //   return groupPlan;
  // }

  groupPlan(_data: any) {
    // console.log('_data', _data)
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let groupPlan : any = [];
    let paymentTypeList = this.appConfig.config.trial_payment_types;

    _data.forEach(planGroup => {
      let group: any = [];
      group['id'] = planGroup.id;
      group['name'] = planGroup.planGroupTranslate.find(value => value.langCode.toUpperCase() === currentLang) ? planGroup.planGroupTranslate.find(value => value.langCode.toUpperCase() === currentLang).name : planGroup.planGroupTranslate[0].name;
      group['image'] = planGroup.url;
      
      planGroup.Plans.forEach(plan => {
        plan.planCountry.forEach(planCountry => {
          planCountry.planOptions.forEach(planOption => {
            planOption.description = planOption.planOptionTranslate.find(value => value.langCode.toUpperCase() === currentLang) ?
                                     planOption.planOptionTranslate.find(value => value.langCode.toUpperCase() === currentLang).name :
                                     planOption.planOptionTranslate[0] ? planOption.planOptionTranslate[0].name : '';
          });
        });
      });

      paymentTypeList.forEach(paymentType => {
        if(paymentType === 'Annual') {
          group[paymentType] = planGroup.Plans.filter(plan => (plan.sku.indexOf('Y-') !== -1));
        } else {
          group[paymentType] = planGroup.Plans.filter(plan => (plan.sku.indexOf('Y-') === -1));
        }
        group[paymentType].forEach(plan => {
          plan.planCountry = plan.planCountry[0];
          if(plan.planTranslate[0]) {
            plan.planTranslate = plan.planTranslate.find(value => value.langCode.toUpperCase() === currentLang) ? plan.planTranslate.find(value => value.langCode.toUpperCase() === currentLang) : plan.planTranslate[0];
          }
        });
      });
      groupPlan.push(group);
    });
    // console.log('groupPlan', groupPlan);
    return groupPlan;
  }

  getGroupName(_planGroups: any, _data: any) {
    let groupName = '';
    _planGroups.forEach(group => {
      group.item.forEach(_item => {
        if (_item.id === _data.plan.id) {
          return groupName = group.name;
        }
      });
    });

    return groupName;
  }


   // get all plans
  getAllPlans() {
    return new Promise((resolved, reject) => {
      let options = {
        userTypeId: 1
      };
      let params = this.globalService._URLSearchParams(options);
      this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
        data => {
          if (data) {
            resolved(data)
          }
        },
        error => {
          // console.log(error.error);
          reject(error);
        }
      )
    })
  } 

}