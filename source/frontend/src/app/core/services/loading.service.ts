import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class LoadingService {
  public status = new BehaviorSubject<Boolean>(false);
  public status1 = new BehaviorSubject<boolean>(false);
  constructor() { }

  public display(value: Boolean) {
    this.status.next(value);
  }
  public display1(value1: boolean) {
    this.status1.next(value1);
  }

}