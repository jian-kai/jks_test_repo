import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class NotificationService {
  @Output() notiObject: EventEmitter<Object> = new EventEmitter();
  @Output() notiRemindUser: EventEmitter<Object> = new EventEmitter();
  constructor() {
  }
  addNotification(_msg: string) {
    this.notiObject.emit({msg: _msg});
  }

  addNotificationRemindUser(_email?: string ,_msg?: string) {
    if (_email)
      this.notiRemindUser.emit({status: true, email: _email});
    else
      this.notiRemindUser.emit({status: false});
  }
}
