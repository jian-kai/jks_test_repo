const STORAGE_KEY = 'auth_token';
const CURRENT_USER = 'current_user';
const CART_ITEMS = 'cart_items';
const FREE_TRIAL_ITEM = 'free_trial_item';
const ASK_ITEM = 'ask_item';
const CUSTOM_PLAN_ITEM = 'custom_plan_item';
const CHECKOUT_DATA = 'checkout_data';
const NOT_LOGIN_FOR_CUSTOM_PLAN = 'is_not_login_custom_plan';
const FLAG_SYNC_CART = 'flag_sync_cart';
const PLAN_CART = 'plan_cart';
const CURRENT_COUNTRY = 'current_country';
const COUNTRIES = 'countries';
const CURRENT_LANGUAGE = 'current_language';
const CURRENT_LANGUAGE_CODE = 'current_language_code';
const DELIVERY_ADDRESS_BY_GUEST = 'delivery_address_by_guest';
const CARD_BY_GUEST = 'card_by_guest';
const PRODUCTS_REMOVED = 'products_removed';
const GTM_TRANSACTION_ID_KEY = 'gtm_transaction_id';
const PURCHASE_ID = 'purchase_id';
const GTM_MEDIUM = 'gtm_medium';
const GTM_SOURCE = 'gtm_source';
const GTM_CAMPAIGN = 'gtm_campaign';
const GTM_TERM = 'gtm_term';
const GTM_CONTENT = 'gtm_content';
const INFO_FOR_ECOMMERCE = 'info_for_ecommerce';
const HAS_MIGRATE_LANGUAGE = 'has_migrate_language';
const DEFAULT_COUNTRY = {
  code: 'MYS',
  name: 'Malaysia',
  currencyDisplay: 'RM',
  taxName: 'GST',
  taxRate: 0,
  includedTaxToProduct: 1,
  callingCode: '60',
  defaultLang: 'en',
  isDefault: true,
  isBaEcommerce: true,
  isBaSubscription: true,
  isBaAlaCarte: true,
  isBaStripe: true,
  isWebEcommerce: true,
  isWebSubscription: true,
  isWebAlaCarte: true,
  isWebStripe: true,
  DirectoryCountries: {
    flag: 'https://restcountries.eu/data/mys.svg'
  }
}
const DEFAULT_LANGUAGE = {
  key: 'en',
  value: 'Malaysia',
  countryCode: 'MYS',
  isDefault: true
}

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class StorageService {
  getAuthToken() {
    return localStorage.getItem(STORAGE_KEY);
  }

  setAuthToken(token) {
    localStorage.setItem(STORAGE_KEY, token);
  }

  removeAuthToken() {
    localStorage.removeItem(STORAGE_KEY);
  }

  // set current User localstorage
  setCurrentUser(data) {
    localStorage.setItem(CURRENT_USER, JSON.stringify(data));
  }
  // remove current User localstorage
  removeCurrentUser() {
    localStorage.removeItem(CURRENT_USER);
  }
  // get localstorage item
  getCurrentUser(): any {
    return JSON.parse(localStorage.getItem(CURRENT_USER));
  }
  // get localstorage item
  getCurrentUserItem(): any {
    let user = JSON.parse(localStorage.getItem(CURRENT_USER));
    if (user && user.isActive) {
      return user;
    }
  }

  // reset defaultShipping | defaultBilling
  resetDefaultShipping(_value): any {
    let user = this.getCurrentUser();
    user.defaultShipping = _value;
    this.setCurrentUser(user);
  }

  // reset defaultBilling
  resetDefaultBilling(_value): any {
    let user = this.getCurrentUser();
    user.defaultBilling = _value;
    this.setCurrentUser(user);
  }

  // set cart item localstorage
  setCartItem(item) {
    let country = this.getCountry();
    let _item = this.getCartItem();
    if (!_item) {
      _item = {};
    }
    _item[country.code] = item;

    localStorage.setItem(CART_ITEMS, JSON.stringify(_item));
  }
  // get cart item localstorage
  getCartItem(): any {
    return JSON.parse(localStorage.getItem(CART_ITEMS));
  }

  // clear cart
  clearCart(removeAll?: any) {
    if (removeAll) {// remove cart items of all countries
      localStorage.removeItem(CART_ITEMS);
    } else { // remove cart items of current country
      let country = this.getCountry();
      let getCartItems = JSON.parse(localStorage.getItem(CART_ITEMS));
      delete getCartItems[country.code];
      localStorage.setItem(CART_ITEMS, JSON.stringify(getCartItems));
    }
  }

  // set free trial item localstorage
  setFreeTrialItem(item) {
    this.clearFreeTrialItem();
    localStorage.setItem(FREE_TRIAL_ITEM, JSON.stringify(item));
  }
  // get cart item localstorage
  getFreeTrialItem(): any {
    return JSON.parse(localStorage.getItem(FREE_TRIAL_ITEM));
  }

  // clear cart
  clearFreeTrialItem() {
    localStorage.removeItem(FREE_TRIAL_ITEM);
  }

  // set awesome-shave-kit item localstorage
  setASKItem(item) {
    this.clearFreeTrialItem();
    localStorage.setItem(ASK_ITEM, JSON.stringify(item));
  }
  // get cart item localstorage
  getASKItem(): any {
    return JSON.parse(localStorage.getItem(ASK_ITEM));
  }

  // clear cart
  clearASKItem() {
    localStorage.removeItem(ASK_ITEM);
  }
  // set custom plan item localstorage
  setCustomPlanItem(item) {
    this.clearFreeTrialItem();
    localStorage.setItem(CUSTOM_PLAN_ITEM, JSON.stringify(item));
  }
  // get cart item localstorage
  getCustomPlanItem(): any {
    return JSON.parse(localStorage.getItem(CUSTOM_PLAN_ITEM));
  }

  // clear cart
  clearCustomPlanItem() {
    localStorage.removeItem(CUSTOM_PLAN_ITEM);
  }

  //set checkout data to prevent double payment
  setCheckoutData(item) {
    localStorage.setItem(CHECKOUT_DATA, JSON.stringify(item));
  }
  //get checkout data to prevent double payment
  getCheckoutData(): any {
    return JSON.parse(localStorage.getItem(CHECKOUT_DATA));
  }
  //clear checkout data to prevent double payment
  clearCheckoutData() {
    localStorage.removeItem(CHECKOUT_DATA);
  }

  // set check
  setCheckLoginForCustomPlan(value) {
    localStorage.setItem(NOT_LOGIN_FOR_CUSTOM_PLAN, JSON.stringify(value));
  }
  // get check
  getCheckLoginForCustomPlan(): any {
    return JSON.parse(localStorage.getItem(NOT_LOGIN_FOR_CUSTOM_PLAN));
  }

  // clear check
  clearCheckLoginForCustomPlan() {
    localStorage.removeItem(NOT_LOGIN_FOR_CUSTOM_PLAN);
  }

  // set plan item
  setPlanCart(item) {
    this.clearPlan();
    localStorage.setItem(PLAN_CART, JSON.stringify(item));
  }

  // get plan item 
  getPlanCart(): any {
    return JSON.parse(localStorage.getItem(PLAN_CART));
  }

  // get plan cart items
  getPlanCartItems() {
    let planCart = this.getPlanCart();
    return planCart.carts ? planCart.carts : [];
  }

  // set cart item localstorage
  setPlanCartItem(items) {
    let planCart = this.getPlanCart();
    planCart.carts = items;
    localStorage.setItem(PLAN_CART, JSON.stringify(planCart));
  }

  // clear plan item
  clearPlan() {
    localStorage.removeItem(PLAN_CART);
  }

  // set country code
  setCountry(country) {
    localStorage.setItem(CURRENT_COUNTRY, JSON.stringify(country));
  }

  // get country code
  getCountry() {
    if (localStorage.getItem(CURRENT_COUNTRY)) {
      return JSON.parse(localStorage.getItem(CURRENT_COUNTRY));
    } else {
      return DEFAULT_COUNTRY;
    }
  }

  // clear flag sync cart
  clearCountry() {
    localStorage.removeItem(CURRENT_COUNTRY);
  }

  // set contries
  setCountries(countries) {
    // update current country
    if (this.getCountry()) {
      let country = this.getCountry();
      let newCountry = countries.find(value => value.code === country.code);
      if (JSON.stringify(country) !== JSON.stringify(newCountry)) {
        this.setCountry(newCountry);
      }
    }
    localStorage.setItem(COUNTRIES, JSON.stringify(countries));
  }

  // get country code
  getCountries() {
    if (localStorage.getItem(COUNTRIES)) {
      return JSON.parse(localStorage.getItem(COUNTRIES));
    } else {
      return null;
    }
  }

  // clear flag sync cart
  clearCountries() {
    localStorage.removeItem(COUNTRIES);
  }

  // set language
  setLanguage(language) {
    localStorage.setItem(CURRENT_LANGUAGE, JSON.stringify(language));
  }

  // set flag for country when the cart has been synced
  setFlagSyncCart() {
    let country = this.getCountry();
    let flagSync = {};
    if (localStorage.getItem(FLAG_SYNC_CART)) {
      flagSync = JSON.parse(localStorage.getItem(FLAG_SYNC_CART));
    }
    flagSync[country.code] = true;

    localStorage.setItem(FLAG_SYNC_CART, JSON.stringify(flagSync));
  }

  // check flag sync cart
  checkFlagSyncCart() {
    let country = this.getCountry();
    let flagSync = JSON.parse(localStorage.getItem(FLAG_SYNC_CART));
    if (flagSync && flagSync[country.code]) {
      return true;
    } else {
      return false;
    }
  }

  // clear flag sync cart
  clearFlagSyncCart() {
    localStorage.removeItem(FLAG_SYNC_CART);
  }

  // get language
  getLanguage() {
    if (localStorage.getItem(CURRENT_LANGUAGE)) {
      return JSON.parse(localStorage.getItem(CURRENT_LANGUAGE));
    }
  }

  // set language
  setLanguageCode(language) {
    localStorage.setItem(CURRENT_LANGUAGE_CODE, JSON.stringify(language));
  }

  // get language
  getLanguageCode() {
    if (localStorage.getItem(CURRENT_LANGUAGE_CODE)) {
      return JSON.parse(localStorage.getItem(CURRENT_LANGUAGE_CODE));
    }
  }

  // set delivery address by guest
  setDeliveryAddressByGuest(_address) {
    let addressStorage = localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST);
    let arrTmp = [];
    let has = false;
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      _tmp.forEach(address => {
        if (address.id === _address.id) {
          arrTmp.push(_address);
          has = true;
        } else {
          arrTmp.push(address);
        }
      });
      if (!has) {
        arrTmp.push(_address);
      }
    } else {
      arrTmp.push(_address);
    }
    localStorage.setItem(DELIVERY_ADDRESS_BY_GUEST, JSON.stringify(arrTmp));
  }

  // get delivery address by guest
  getDeliveryAddressByGuest() {
    return JSON.parse(localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST));
  }

  // remove delivery address by guest
  removeDeliveryAddressByGuest(_address) {
    let addressStorage = localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST);
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      let _tmp2 = _tmp.filter(address => address.id !== _address.id);
      localStorage.setItem(DELIVERY_ADDRESS_BY_GUEST, JSON.stringify(_tmp2));
    }
  }

  clearDeliveryAddressByGuest() {
    localStorage.removeItem(DELIVERY_ADDRESS_BY_GUEST);
  }

  // set delivery address by guest
  setCardByGuest(_card) {
    let addressStorage = localStorage.getItem(CARD_BY_GUEST);
    let arrTmp = [];
    let has = false;
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      _tmp.forEach(card => {
        if (card.id === parseInt(_card.id)) {
          arrTmp.push(_card);
          has = true;
        } else {
          arrTmp.push(card);
        }
      });
      if (!has) {
        arrTmp.push(_card);
      }
    } else {
      arrTmp.push(_card);
    }
    localStorage.setItem(CARD_BY_GUEST, JSON.stringify(arrTmp));
  }

  // get delivery address by guest
  getCardByGuest() {
    return JSON.parse(localStorage.getItem(CARD_BY_GUEST));
  }

  // remove delivery address by guest
  removeCardByGuest(_card) {
    let addressStorage = localStorage.getItem(CARD_BY_GUEST);
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      let _tmp2 = _tmp.filter(address => address.id !== _card.id);
      localStorage.setItem(CARD_BY_GUEST, JSON.stringify(_tmp2));
    }
  }

  clearCardByGuest() {
    localStorage.removeItem(CARD_BY_GUEST);
  }

  // set products removed
  setProductsRemoved(_item) {
    let productsRemovedStorage = localStorage.getItem(PRODUCTS_REMOVED);
    let arrTmp = [];
    if (productsRemovedStorage) {
      let _tmp = JSON.parse(productsRemovedStorage);
      _tmp.forEach(item => {
        arrTmp.push(item);
      });
    }
    arrTmp.push(_item);
    localStorage.setItem(PRODUCTS_REMOVED, JSON.stringify(arrTmp));
  }

  // get products removed
  getLatestProductsRemoved() {
    let productsRemovedStorage = JSON.parse(localStorage.getItem(PRODUCTS_REMOVED));
    if (productsRemovedStorage) {
      return productsRemovedStorage[productsRemovedStorage.length - 1];
    } else {
      return null;
    }
  }

  // remove products removed
  removeProductsRemoved(_item) {
    let productsRemovedStorage = localStorage.getItem(PRODUCTS_REMOVED);
    if (productsRemovedStorage) {
      let _tmp = JSON.parse(productsRemovedStorage);
      let _tmp2 = _tmp.filter(item => item.id !== _item.id);
      localStorage.setItem(PRODUCTS_REMOVED, JSON.stringify(_tmp2));
    }
  }

  // Purchase/Transaction ID
  getPurchaseID() {
    return localStorage.getItem(PURCHASE_ID);
  }

  setPurchaseID(_id) {
    localStorage.setItem(PURCHASE_ID, _id);
  }

  removePurchaseID() {
    localStorage.removeItem(PURCHASE_ID);
  }

  // clear products removed
  clearProductsRemoved() {
    localStorage.removeItem(PRODUCTS_REMOVED);
  }

  // GTM Transaction ID
  getGtmTransactionID() {
    return localStorage.getItem(GTM_TRANSACTION_ID_KEY);
  }

  setGtmTransactionID(_id) {
    localStorage.setItem(GTM_TRANSACTION_ID_KEY, _id);
  }

  removeGtmTransactionID() {
    localStorage.removeItem(GTM_TRANSACTION_ID_KEY);
  }

  setGtmMedium(string) {
    localStorage.setItem(GTM_MEDIUM, string);
  }

  getGtmMedium() {
    return localStorage.getItem(GTM_MEDIUM);
  }

  removeGtmMedium() {
    localStorage.removeItem(GTM_MEDIUM);
  }

  setGtmSource(string) {
    localStorage.setItem(GTM_SOURCE, string);
  }

  getGtmSource() {
    return localStorage.getItem(GTM_SOURCE);
  }

  removeGtmSource() {
    localStorage.removeItem(GTM_SOURCE);
  }

  setGtmCampaign(string) {
    localStorage.setItem(GTM_CAMPAIGN, string);
  }

  getGtmCampaign() {
    return localStorage.getItem(GTM_CAMPAIGN);
  }

  removeGtmCampaign() {
    localStorage.removeItem(GTM_CAMPAIGN);
  }

  setGtmTerm(string) {
    localStorage.setItem(GTM_TERM, string);
  }

  getGtmTerm() {
    return localStorage.getItem(GTM_TERM);
  }

  removeGtmTerm() {
    localStorage.removeItem(GTM_TERM);
  }

  setGtmContent(string) {
    localStorage.setItem(GTM_CONTENT, string);
  }

  getGtmContent() {
    return localStorage.getItem(GTM_CONTENT);
  }

  removeGtmContent() {
    localStorage.removeItem(GTM_CONTENT);
  }

  setInfoForEcommerce(string) {
    localStorage.setItem(INFO_FOR_ECOMMERCE, string);
  }

  getInfoForEcommerce() {
    return localStorage.getItem(INFO_FOR_ECOMMERCE);
  }

  setMigrateLanguage(string) {
    localStorage.setItem(HAS_MIGRATE_LANGUAGE, string);
  }

  getMigrateLanguage() {
    return localStorage.getItem(HAS_MIGRATE_LANGUAGE);
  }

}
