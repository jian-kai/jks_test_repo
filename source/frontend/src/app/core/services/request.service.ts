import { Injectable } from '@angular/core';

import { StorageService } from './storage.service';
import { Headers } from '@angular/http';

@Injectable()
export class RequestService {
  constructor(public storage: StorageService) {}

  getAuthHeaders(contentType?: string) {
    let _headers = this.getHeaders(contentType);
    let authToken = this.storage.getAuthToken();

    _headers.append('x-access-token', authToken);
    return {headers: _headers};
  }

  getHeaders(contentType: string) {
    let country = this.storage.getCountry();
    let headers = new Headers();
    let _ctype = "";
    switch (contentType) {
      case "json":
        _ctype = "application/json";
        break;
      case "form-data":
        _ctype = "";
        break;
      default:
        _ctype = "application/json";
        break;
    }
    if (_ctype) {
      headers.append('Content-Type', _ctype);
    }

    headers.append('country-code', country.code);
    headers.append('lang-code', this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN');
    // set basic authentication
    headers.append('authorization', `basic ${btoa('shaves2u:shaves2u')}`);
    return headers;
  }

}
