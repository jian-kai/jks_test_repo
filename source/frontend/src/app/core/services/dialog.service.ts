import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogConfirmComponent } from '../directives/dialog-confirm/dialog-confirm.component';
import { DialogResultComponent } from '../directives/dialog-result/dialog-result.component';

@Injectable()
export class DialogService {

  constructor(public dialog: MatDialog) { }
  confirm(_content ?: string, _submit_name ?: string) {
    return this.dialog.open(DialogConfirmComponent, {
      data: {
        content: _content,
        submit_name: _submit_name
      }
    });
  }
  result(_content ?: string) {
    // console.log('_content', _content)
    return this.dialog.open(DialogResultComponent, {
      data: {
        content: _content
      }
    });
  }
}
