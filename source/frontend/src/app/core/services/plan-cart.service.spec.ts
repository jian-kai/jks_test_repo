import { TestBed, inject } from '@angular/core/testing';

import { PlanCartService } from './plan-cart.service';

describe('PlanCartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlanCartService]
    });
  });

  it('should be created', inject([PlanCartService], (service: PlanCartService) => {
    expect(service).toBeTruthy();
  }));
});
