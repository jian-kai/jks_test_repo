import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class GTMService {
  dataLayer ;
  constructor(public storage: StorageService) {
    this.dataLayer = window['dataLayer'];
   }

   clickProduct(item) {
    return new Promise((resolve, reject) => {
      if(item.product.productCountry.Country) {
        // console.log(item);
        this.dataLayer.push({
          'event': 'productClick',
          'ecommerce': {
            'currencyCode': item.product.productCountry.Country.currencyCode,
            'click': {
              'products': [{
                'name': item.product.productTranslate[0].name,                      // Name or ID is required.
                'id': item.product.id,
                'price': item.product.productCountry.sellPrice,
                'brand': 'Google',
                'category': 'A La Carte'
               }]
             }
           }
        });
      }
      resolve(null);
    });
  }

  // clickProduct(item) {
  //   // console.log(item);
  //   this.dataLayer.push({
  //     'event': 'productClick',
  //     'ecommerce': {
  //       'currencyCode': item.product.productCountry.Country ? item.product.productCountry.Country.currencyCode : 'MYR',
  //       'click': {
  //         'products': [{
  //           'name': item.product.productTranslate[0].name,                      // Name or ID is required.
  //           'id': item.product.id,
  //           'price': item.product.productCountry.sellPrice,
  //           'brand': 'Google',
  //           'category': 'A La Carte'
  //          }]
  //        }
  //      }
  //   });
  // }

   productDetail(item) {
    // console.log('item',item);
    return new Promise((resolve, reject) => {
      if(item.product && item.product.productCountry.Country) {
        this.dataLayer.push({
          'event': 'viewProductDetail',
          'ecommerce': {
            'currencyCode': item.product.productCountry.Country.currencyCode,
            'detail': {
              'products': [{
                'name': item.product.productTranslate[0].name,                      // Name or ID is required.
                'id': item.product.id,
                'price': item.product.productCountry.sellPrice,
                'brand': 'Google',
                'category': 'A La Carte'
               }]
             }
           }
        });
      }
      if(item.planCountry && item.planCountry[0] && item.planCountry[0].Country) {
        let planCountry = item.planCountry[0] ? item.planCountry[0] : item.planCountry;
        this.dataLayer.push({
          'event': 'viewProductDetail',
          'ecommerce': {
            'currencyCode': planCountry.Country.currencyCode,
            'detail': {
              'products': [{
                'name': item.sku,                      // Name or ID is required.
                'id': item.id,
                'price': planCountry.sellPrice,
                'brand': 'Google',
                'category': item.PlanType.name
               }]
             }
           }
        });
      }
      // console.log('this.dataLayer',this.dataLayer);
      resolve(null);
    });
  }

  itemImpressions(items) {
    // // console.log('itemImpressions',items);
    let products = [];
    let positionCtn = 0;
    let currencyCode = '';
    items.forEach(item => {
      if(item.productCountry) {
        positionCtn += 1;
        currencyCode = item.productCountry.Country ? item.productCountry.Country.currencyCode : 'MYR';
        products.push({
          'name': item.productTranslate[0].name,     // Name or ID is required.
          'id': item.id,
          'price': item.productCountry.sellPrice,
          'brand': 'Google',
          'category': 'A La Carte',
          'position': positionCtn
        });
      }
      if(item.item) {
        item.item.forEach(plan => {
          if(plan.planCountry) {
            positionCtn += 1;
            let planCountry = plan.planCountry[0] ? plan.planCountry[0] : plan.planCountry;
            currencyCode = planCountry.Country.currencyCode;
            products.push({
              'name': plan.sku,     // Name or ID is required.
              'id': plan.id,
              'price': planCountry.sellPrice,
              'brand': 'Google',
              'category': plan.PlanType ? plan.PlanType.name : '',
              'position': positionCtn
            })
          }
        })
      }
    })
    
    this.dataLayer.push({
      'event': 'impressions',
      'ecommerce': {
        'currencyCode': currencyCode,
        'impressions': {
          products
        }
      }
    });
  }

  addCart(item) {
    // // console.log(item);
    return new Promise((resolve, reject) => {
      if(item.product && item.product.productCountry.Country) {
        this.dataLayer.push({
          'event': 'addToCart',
          'ecommerce': {
            'currencyCode': item.product.productCountry.Country ? item.product.productCountry.Country.currencyCode : 'MYR',
            'add': {
              'products': [{
                'name': item.product.productTranslate[0].name,
                'id': item.product.id,
                'price': item.product.productCountry.sellPrice,
                'brand': 'Google',
                'quantity': item.qty,
                'sku': item.product.sku,
                'category': 'A La Carte'
              }]
            }
          }
        });
      }
      if(item.plan) {
        let planCountry = item.plan.planCountry[0] ? item.plan.planCountry[0] : item.plan.planCountry;
        this.dataLayer.push({
          'event': 'addToCart',
          'ecommerce': {
            'currencyCode': planCountry.Country.currencyCode,
            'add': {
              'products': [{
                'name': item.plan.sku,
                'id': item.id,
                'price': planCountry.sellPrice,
                'brand': 'Google',
                'quantity': item.qty,
                'category': item.plan.PlanType.name
              }]
            }
          }
        });
      }
      resolve(null);
    });
  }

  removeCart(item) {
    // // console.log(item);
    return new Promise((resolve, reject) => {
      if(item.product && item.product.productCountry.Country) {
        this.dataLayer.push({
          'event': 'removeFromCart',
          'ecommerce': {
            'currencyCode': item.product.productCountry.Country ? item.product.productCountry.Country.currencyCode : 'MYR',
            'remove': {                               // 'remove' actionFieldObject measures.
              'products': [{                          //  removing a product to a shopping cart.
                'name': item.product.productTranslate[0].name,
                'id': item.product.id,
                'price': item.product.productCountry.sellPrice,
                'brand': 'Google',
                'sku': item.product.sku,
                'quantity': item.qty,
                'category': 'A La Carte'
              }]
            }
          }
        });
      }

      if(item.plan){
        let planCountry = item.plan.planCountry[0] ? item.plan.planCountry[0] : item.plan.planCountry;
        this.dataLayer.push({
          'event': 'removeFromCart',
          'ecommerce': {
            'currencyCode': planCountry.Country.currencyCode,
            'remove': {                               // 'remove' actionFieldObject measures.
              'products': [{                          //  removing a product to a shopping cart.
                'name': item.plan.sku,
                'id': item.id,
                'price': planCountry.sellPrice,
                'brand': 'Google',
                'quantity': item.qty,
                'category': item.plan.PlanType.name
              }]
            }
          }
        });
      }
      resolve(null);
    });
  }
  
  checkout(actions) {
  
    // build product
    let country = this.storage.getCountry();
    let cartItems = this.storage.getCartItem();
    let countryCart = [];
    if (cartItems && cartItems[country.code]) {
      countryCart = cartItems[country.code];
    }
    
    let products = [];
    let currencyCode;
    countryCart.forEach(obj => {
      if(obj.plan) {
        let planCountry = obj.plan.planCountry[0] ? obj.plan.planCountry[0] : obj.plan.planCountry;
        currencyCode = planCountry.Country.currencyCode;
        products.push({
          'name': obj.plan.sku,
          'id': obj.id,
          'price': planCountry.sellPrice,
          'brand': 'Google',
          'quantity': obj.qty,
          'category': obj.plan.PlanType ? obj.plan.PlanType.name : ''
        });
      } else {
        currencyCode = obj.product.productCountry && obj.product.productCountry.Country ? obj.product.productCountry.Country.currencyCode : "MYR";
        
        products.push({
          'name': obj.product.sku,
          'id': obj.product.id,
          'price': obj.product.productCountry.sellPrice,
          'brand': 'Google',
          'quantity': obj.qty,
          'category': 'A La Carte'
       });
      }
    })

    this.dataLayer.push({
      'event': 'checkout',
      'ecommerce': {
        'currencyCode': currencyCode,
        'checkout': {
          'actionField': actions,
          products
       }
      }
    });
  }

  purchaseCart(item) {
    // console.log(item);
    let products = [];
    if(item.planCountries) {
      item.planCountries.forEach(planCountry => {
        let subsItem = item.params.subsItems.find(obj => obj.PlanCountryId === planCountry.id);
        products.push({
          'name': planCountry.Plan.sku,     // Name or ID is required.
          'id': planCountry.id,
          'price': planCountry.sellPrice,
          'brand': 'Google',
          'category': planCountry.Plan.PlanType.name,
          'quantity': (subsItem ? subsItem.qty : 0)
        });
      });
    }
    if(item.productCountries) {
      item.productCountries.forEach(productCountry => {
        let alaItems = item.params.alaItems.find(obj => obj.ProductCountryId === productCountry.id);
        products.push({
          'name': productCountry.Product.sku,     // Name or ID is required.
          'id': productCountry.id,
          'price': productCountry.sellPrice,
          'brand': 'Google',
          'category': 'A La Carte',
          'quantity': (alaItems ? alaItems.qty : 0)
        })
      })
    }
    this.dataLayer.push({
      'ecommerce': {
        'currencyCode': item.country ? item.country.currencyCode : 'MYR',
        'purchase': {
          'actionField': {
            'id': item.order.id,                         // Transaction ID. Required for purchases and refunds.
            'affiliation': 'Online Store',
            'revenue': item.receiptObject.totalPrice,                     // Total transaction value (incl. tax and shipping)
            'tax':item.receiptObject.taxAmount,
            'shipping': item.receiptObject.shippingFee,
            'coupon': item.order.promoCode
          },
          products
        }
      }
    });
  }
  
  transactionData(orderInfo) {
    // console.log('orderInfo',orderInfo);
    let products = [];
    orderInfo.orderDetail.forEach(item => {
      if(item.PlanCountry) {
        products.push({
          'name': item.PlanCountry.Plan.sku,     // Name or ID is required.
          'id': item.PlanCountry.id,
          'price': item.PlanCountry.sellPrice,
          'brand': 'Google',
          'category': item.PlanCountry.Plan.PlanType ? item.PlanCountry.Plan.PlanType.name : '',
          'quantity': (item.qty ? item.qty : 0)
        });
      }
      if(item.ProductCountry) {
        products.push({
          'name': item.ProductCountry.Product.sku,     // Name or ID is required.
          'id': item.ProductCountry.id,
          'price': item.ProductCountry.sellPrice,
          'brand': 'Google',
          'category': 'A La Carte',
          'quantity': (item.qty ? item.qty : 0)
        })
      }
    })
    
    // console.log('products',products);
    this.dataLayer.push({
      'event': 'transactiondata',
      'ecommerce': {
        'currencyCode': orderInfo.Country ? orderInfo.Country.currencyCode : 'MYR',
        'purchase': {
          'actionField': {
            'id': orderInfo.id,                         // Transaction ID. Required for purchases and refunds.
            'affiliation': 'Online Store',
            'revenue': orderInfo.Receipt.totalPrice,                     // Total transaction value (incl. tax and shipping)
            'tax':orderInfo.Receipt.taxAmount,
            'shipping': orderInfo.Receipt.shippingFee,
            'coupon': orderInfo.promoCode
          },
          products
        }
      }
    });
    // remove GTM Transaction ID
    this.storage.removeGtmTransactionID();

  }
}
