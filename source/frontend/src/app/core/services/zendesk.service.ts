import { Injectable } from '@angular/core';
import { AppConfig } from '../../config/app.config';

@Injectable()
// I wrap the zEmbed object, providing Promise-based method calls so that the calling
// context doesn't have to worry about whether or not the underlying zEmbed object has
// been loaded.
export class ZendeskService {

	public window: any;


	// I initialize the service.
	constructor(public appConfig: AppConfig,) {

    this.ngxZendeskWebwidgetService();

	}

	/**
 * @return {?}
 */
	public getWindow() {
			return window;
	}

	public ngxZendeskWebwidgetService() {
		var _this = this;
		var _ngxZendeskWebwidgetConfig : any = {};
		_ngxZendeskWebwidgetConfig.accountUrl = this.appConfig.config.zendeskHost;
		if (!_ngxZendeskWebwidgetConfig.accountUrl) {
			throw new Error('Missing accountUrl. Please set in app config via ZendeskWidgetProvider');
		}
		this.window = this.getWindow();
		var window = this.window;
		// Following is essentially a copy paste of JS portion of the Zendesk embed code
		// with our settings subbed in. For more info, see:
		// https://support.zendesk.com/hc/en-us/articles/203908456-Using-Web-Widget-to-embed-customer-service-in-your-website
		window.zEmbed || function (e, t) {
			var n, o, d, i, s, a = [], r = document.createElement("iframe");
			window.zEmbed = function () {
				a.push(arguments);
			}, window.zE = window.zE || window.zEmbed,
				r.src = "javascript:false",
				r.title = "",
				r.style.cssText = "display: none",
				d = document.getElementsByTagName("script"),
				d = d[d.length - 1],
				d.parentNode.insertBefore(r, d),
				i = r.contentWindow,
				s = i.document;
			try {
				o = s;
			}
			catch (e) {
				n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);',
					o = s;
			}
			o.open()._l = function () {
				var e = this.createElement("script");
				n && (this.domain = n),
					e.id = "js-iframe-async",
					e.src = "https://assets.zendesk.com/embeddable_framework/main.js",
					this.t = +new Date,
					this.zendeskHost = _ngxZendeskWebwidgetConfig.accountUrl,
					this.zEQueue = a,
					this.body.appendChild(e);
			},
				o.write('<body onload="document._l();">'),
				o.close();
		}();
		this.window.zE(function () {
			// _ngxZendeskWebwidgetConfig.beforePageLoad(_this.window.zE);
		});
	}

	/**
     * @param {?} locale
     * @return {?}
     */
    public setLocale = function (locale) {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.setLocale(locale);
        });
    };
    /**
     * @param {?} userObj
     * @return {?}
     */
    public identify = function (userObj) {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.identify(userObj);
        });
    };
    /**
     * @return {?}
     */
    public hide = function () {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.hide();
        });
    };
    /**
     * @return {?}
     */
    public show = function () {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.show();
        });
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    public activate = function (options) {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.activate(options);
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    public setHelpCenterSuggestions = function (options) {
        var _this = this;
        this.window.zE(function () {
            _this.window.zE.setHelpCenterSuggestions(options);
        });
    };
    /**
     * @param {?} settings
     * @return {?}
     */
    public setSettings = function (settings) {
        this.window.zESettings = settings;
    };

	

}