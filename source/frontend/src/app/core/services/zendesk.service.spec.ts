import { TestBed, inject } from '@angular/core/testing';

import { ZendeskService } from './zendesk.service';

describe('ZendeskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ZendeskService]
    });
  });

  it('should be created', inject([ZendeskService], (service: ZendeskService) => {
    expect(service).toBeTruthy();
  }));
});
