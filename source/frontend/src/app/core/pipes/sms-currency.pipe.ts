import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { CommonModule, CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'smsCurrency',
  pure: false
})
export class SmsCurrencyPipe implements PipeTransform {

  constructor(public storage: StorageService,
    private currencyPipe: CurrencyPipe) { }

  transform(value: any, args?: any): any {
    let country = this.storage.getCountry();
    let lang = this.storage.getLanguageCode();
    if (country.code.toUpperCase() === 'KOR' && lang.toUpperCase() === 'KO') {
      value = typeof value === 'number' ? value.toFixed(2) : value;
      if (args && args === 'html') {
        return `<span class="price">${this.currencyPipe.transform(value).replace('USD', '').replace('.00', '')}</span><span class="currency-code">원</span>`;
      }
      return `${this.currencyPipe.transform(value).replace('USD', '').replace('.00', '')}원`;
    }
    else {
      let country = this.storage.getCountry();
      value = typeof value === 'number' ? value.toFixed(2) : value;
      if (args && args === 'html') {
        return `<span class="currency-code">${country.currencyDisplay}</span>
              <span class="price">${this.currencyPipe.transform(value).replace('USD', '')}</span>`;
      }
      return `${country.currencyDisplay}${this.currencyPipe.transform(value).replace('USD', '')}`;
    }
  }

}