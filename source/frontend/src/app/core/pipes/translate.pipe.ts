import { TranslateService } from '@ngx-translate/core';
import { Pipe, PipeTransform } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';

@Pipe({
  name: 'translatePipe',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslateService,
    public storage: StorageService) {}

  transform(arrayTranslate: any, args?: any): any {
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate = arrayTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    translate = translate || arrayTranslate.find(value => value.isDefault === true);
    if (!translate) {
      translate = arrayTranslate[0];
    }
    return translate[args];
  }

}