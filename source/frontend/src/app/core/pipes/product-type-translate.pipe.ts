import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService, } from '@ngx-translate/core';
import { StorageService } from '../../core/services/storage.service';

@Pipe({
  name: 'productTypeTranslate',
  pure: false
})
export class ProductTypeTranslatePipe implements PipeTransform {
  public productTypeTranslate;
  public args;
  constructor(
    private translate: TranslateService,
    public storage: StorageService) {
    }

  transform(productTypeTranslate: any, args?: any): any {
    this.productTypeTranslate = productTypeTranslate;
    this.args = args;
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate = productTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    return translate ? translate[args]: '';
  }

}
