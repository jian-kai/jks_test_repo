import { Pipe, PipeTransform } from '@angular/core';

const countries = {
  'MYS': 'MY',
  'SGP': 'SG',
  'KOR': 'KR'
}

@Pipe({
  name: 'orderNo'
})
export class OrderNoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const BASE_ORDER = '000000000';
    let countryCode = countries[args] || args;
    value = `${value}`;
    return `#${countryCode}${BASE_ORDER.substr(0, BASE_ORDER.length - value.length)}${value}`;
  }

}
