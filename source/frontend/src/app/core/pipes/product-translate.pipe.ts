import { TranslateService, } from '@ngx-translate/core';
import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageService } from '../../core/services/storage.service';

@Pipe({
  name: 'productTranslate',
  pure: false
})
export class ProductTranslatePipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    public storage: StorageService) {}

  transform(productTranslate: any, args?: any): any {
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate;
    if(Array.isArray(productTranslate)) {
      translate = productTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    } else {
      translate = productTranslate;
    }
    return translate ? (translate[args] ? translate[args] : translate) : '';
  }

}