// import { AboutComponent } from './components/about/about.component';
// import { ThankyouComponent } from '../product/components/checkout/thankyou/thankyou.component';
 //import { ProductListComponent } from '../product/components/product-list/product-list.component';
import { LoggedInGuard } from './guards/logged-in.guard';
// import { SocialLoginComponent } from '../auth/components/social-login/social-login.component';
import { AuthService } from '../auth/services/auth.service';
// import { ProductDetailComponent } from '../product/components/product-detail/product-detail.component';
// import { ArticleListComponent } from '../article/components/article-list/article-list.component';
// import { ArticleDetailComponent } from '../article/components/article-detail/article-detail.component';
import {  } from './components/privacy-policy/privacy-policy.component';
import { AskComponent } from '../ask/ask.component';
import { ProductListComponent } from '../product/components/product-list/product-list.component';

// import { SupportComponent } from '../support/support.component';
// import { UserSubscriptionComponent } from '../user/components/subscription/user-subscription.component';
// import { UserOrderListComponent } from '../user/components/order/user-order-list/user-order-list.component';
// import { UserOrderDetailComponent } from '../user/components/order/user-order-detail/user-order-detail.component';
// import { SettingComponent } from '../user/components/setting/setting.component';
import { LoginComponent } from '../auth/components/login/login.component';
// import { LogoutComponent } from '../auth/components/logout/logout.component';

export const routes = [
  { path: '', loadChildren: 'app/core/components/home/home.module#HomeModule' },
  { path: 'sg', loadChildren: 'app/core/components/home/home.module#HomeModule' },
  { path: 'my', loadChildren: 'app/core/components/home/home.module#HomeModule' },
  // { path: 'about', component: AboutComponent, pathMatch: 'full' },
  // { path: 'setting', component: SettingComponent, canActivate: [AuthService] },
  { path: 'checkout', loadChildren: 'app/product/components/checkout/checkout.module#CheckoutModule' },
  { path: 'checkout-trial', loadChildren: 'app/product/components/checkout-free-trial/checkout-free-trial.module#CheckoutFreeTrialModule' },
  { path: 'checkout-ask', loadChildren: 'app/product/components/checkout-ask/checkout-ask.module#CheckoutAskModule' },
  { path: 'ipay88/submit/:orderId', loadChildren: 'app/product/components/checkout/submit-ipay88/submit-ipay88.module#SubmitIpay88Module' },
  { path: 'ipay88/error/:orderId/:message', loadChildren: 'app/product/components/checkout/ipay88-error/ipay88-error.module#Ipay88ErrorModule' },
  { path: 'thankyou/:orderId', loadChildren: 'app/product/components/checkout/thankyou/thankyou.module#ThankyouModule', pathMatch: 'full' },
  // { path: 'auth', component: SocialLoginComponent, canActivate: [AuthService] },
  { path: 'cart', loadChildren: 'app/product/components/cart/cart.module#CartModule' },
  { path: 'products',   loadChildren: 'app/product/components/product-list/product-list.module#ProductListModule'},
  //{ path: 'products/:slug', loadChildren: 'app/product/components/product-detail/product-detail.module#ProductDetailModule' },
  { path: 'shave-plans', loadChildren: 'app/subscription/subscription.module#SubscriptionModule' },

  // { path: 'subscriptions/:id', component: SubscriptionDetailComponent },
  // { path: 'articles', component: ArticleListComponent },
  // { path: 'article/:id', component: ArticleDetailComponent },
  { path: 'privacy-policy', loadChildren: 'app/core/components/privacy-policy/privacy-policy.module#PrivacyPolicyModule' },
  // { path: 'support', component: SupportComponent },
  // { path: 'help', component: HelpComponent },
  { path: 'help', loadChildren: 'app/support/help/help.module#HelpModule' },

  { path: 'faq', loadChildren: 'app/support/faq/faq.module#FaqModule' },
  { path: 'terms-conditions', loadChildren: 'app/core/components/terms-conditions/terms-conditions.module#TermsConditionsModule' },
  { path: 'login', loadChildren: 'app/auth/components/login/login.module#LoginModule', canActivate: [LoggedInGuard] },
  { path: 'login/:token', loadChildren: 'app/auth/components/login/login.module#LoginModule', canActivate: [LoggedInGuard] },
  // { path: 'logout', component: LogoutComponent },
  { path: 'users/reset-password', loadChildren: 'app/auth/components/reset-password/reset-password.module#ResetPasswordModule' },
  // { path: 'plans', loadChildren: 'app/subscription/plan/plan.module#PlanModule' },
   //  Subscibers trial
  { path: 'shave-plans/free-trial', loadChildren: 'app/subscription/free-trial/free-trial.module#FreeTrialModule' },
  { path: 'shave-plans/custom-plan', loadChildren: 'app/subscription/custom-plan/custom-plan.module#CustomPlanModule' },
  { path: 'ask', component:AskComponent },
  {
    path: 'user', loadChildren: 'app/user/components/user/user.module#UserModule', canActivate: [AuthService],
    // children: [
    //   { path: '', redirectTo: 'settings', pathMatch: 'full' },
    //   { path: 'orders', component: UserOrderListComponent },
    //   { path: 'orders/:orderId', loadChildren: '../user/components/order/user-order-detail/user-order-detail.module#UserOrderDetailModule'},
    //   { path: 'shave-plans', component: UserSubscriptionComponent },
    //   { path: 'settings', component: SettingComponent }
    // ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];
