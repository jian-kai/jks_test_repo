import {
    trigger, transition, style, animate, state
} from '@angular/core';

export const fadeOutAnimation =
    trigger("fadeOutAnimation", [
        transition(':leave', [
            animate('900ms', style({ opacity: 0, transform: 'scale(0.0)', }))
        ]),
    ])