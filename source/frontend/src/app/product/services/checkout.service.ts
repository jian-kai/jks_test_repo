import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AppConfig } from 'app/config/app.config';
import { RequestService } from '../../core/services/request.service';

@Injectable()
export class CheckoutService {

  constructor(private http: Http, private request: RequestService, public appConfig: AppConfig) { }

  postCheckout(stripeToken) {
    return this.http
      .post(`${this.appConfig.config.api.checkout}`, { stripeToken }, this.request.getAuthHeaders())
      .map(res => res.json())
      .map((res) => {
        // console.log(`res: ${res}`);
        return res;
      });
  }

}
