import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { Item } from '../../../core/models/item.model';
import { CartService } from '../../../core/services/cart.service';
import { GlobalService } from '../../../core/services/global.service';
import { ModalService } from '../../../core/services/modal.service';
import { GTMService } from '../../../core/services/gtm.service';
import { CountriesService } from '../../../core/services/countries.service';

import $ from "jquery";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  apiURL: string;
  public productInfo: any;
  public slug: string;
  public productsSlider = [];
  public item: Item;
  public isLoading: boolean = true;
  public cartRules: any;
  public currentLang: any;
  public productsSliderOptions = {
    listPosition: 'left',
    displayControls: true,
    autoSlide: false,
    intervalDuration: 600000
  }
  constructor(private httpService: HttpService,
    public storage: StorageService,
    private loadingService: LoadingService,
    public modalService: ModalService,
    private route: ActivatedRoute,
    private router: Router,
    public cartService: CartService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private gtmService: GTMService,
    public countriesService: CountriesService
  ) {
    this.apiURL = appConfig.config.apiURL;
    let curCountry = this.storage.getCountry();
    if (!curCountry.isWebAlaCarte) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    // get product detail by product id
    // console.log(this.storage.getCountry().currencyDisplay);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
    this.getDetailProduct();
    this.countriesService.language.subscribe(
      data => {
        this.productsSlider = [];
        this.getDetailProduct();
      }
    )
    //get cart rule
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
      }
    )
  }
  getDetailProduct() {
    this.loadingService.display(true);
    this.currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    this.route.params.subscribe(params => {
      this.slug = params['slug'];
    });
    let options = {
      countryCode: this.storage.getCountry().code,
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getDetail(`${this.appConfig.config.api.products}/${this.slug}?` + params.toString()).subscribe(
      data => {
        this.productInfo = data;
        this.item = {
          id: data.id,
          product: data
        }
        this.item.qty = 1;
        data.productImages.forEach((item, index) => {
          this.productsSlider.push({
            id: item.ProductId,
            imageUrl: item.url
          })
        });
        this.gtmService.clickProduct(this.item);
        this.gtmService.productDetail(this.item);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error);
      }
    )
  }

  // add item to cart
  addToCart() {
    if (this.storage.getLanguageCode() === 'KO' || this.storage.getCountry().code.toUpperCase() === 'KOR' || this.storage.getCountry().code.toUpperCase() === 'HKG') {
      this.modalService.open('collect-email-modal');
    }
    else {
      this.cartService.applyAddItemEvent();
      this.cartService.addItem(this.item);
      this.cartService.checkFreeProduct(this.cartRules);
      // sync cart item to server
      this.cartService.updateCart();
      this.cartService.getCart();
      // this.modalService.open('add-cart-modal', this.item);
    }
  }

  //minus quatity of the item
  minusQuatity() {
    if (this.item.qty > 1) {
      this.item.qty -= 1;
    }
  }

  //plus quatity of the item
  plusQuatity() {
    this.item.qty += 1;
  }

  //change quatity of the item
  changeQuatity() {
    if (this.item.qty < 1) {
      this.item.qty = 1;
    }
  }

}
