import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

import { ProductDetailComponent } from './product-detail.component';

// Modules
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: ProductDetailComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProductDetailComponent,
  ]
})

export class ProductDetailModule { }
