import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// component
import { CheckoutAskComponent } from './checkout-ask.component';
import { CartTemplateAskComponent } from './cart-template-ask/cart-template-ask.component';
import { DeliveryAddressAskComponent } from './delivery-address-ask/delivery-address-ask.component';
import { PaymentMethodAskComponent } from './payment-method-ask/payment-method-ask.component';
import { SignInAskComponent } from './sign-in-ask/sign-in-ask.component';
import { SummaryPlaceOrderAskComponent } from './summary-place-order-ask/summary-place-order-ask.component';

// module
import { SharedModule } from './../../../core/directives/share-modules';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// routes
export const ROUTES: Routes = [
  { path: '', component: CheckoutAskComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CheckoutAskComponent, CartTemplateAskComponent,
    DeliveryAddressAskComponent, PaymentMethodAskComponent, SignInAskComponent, SummaryPlaceOrderAskComponent
  ]
})
export class CheckoutAskModule { }
