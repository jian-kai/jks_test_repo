import { Component, OnInit, ElementRef, ViewChild, QueryList } from '@angular/core';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { AppConfig } from '../../../../app/config/app.config';
import { HttpService } from '../../../core/services/http.service';
import { StripeService } from '../../../core/services/stripe.service';
import { CheckoutService } from '../../services/checkout.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { CartService } from '../../../core/services/cart.service';
import { SimpleMultiStepComponent } from '../../../core/components/partials/simple-multi-step/simple-multi-step.component';
import { CountriesService } from '../../../core/services/countries.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { CardService } from '../../../core/services/card.service';
import { GTMService } from '../../../core/services/gtm.service';
import { DialogService } from '../../../core/services/dialog.service';

// import view child
import { DeliveryAddressAskComponent } from './delivery-address-ask/delivery-address-ask.component';
import { PaymentMethodAskComponent } from './payment-method-ask/payment-method-ask.component';
import { SignInAskComponent } from './sign-in-ask/sign-in-ask.component';
import { SummaryPlaceOrderAskComponent } from './summary-place-order-ask/summary-place-order-ask.component';
import { RouterService } from '../../../core/helpers/router.service';
@Component({
  selector: 'app-checkout-ask',
  templateUrl: './checkout-ask.component.html',
  styleUrls: ['./checkout-ask.component.scss']
})
export class CheckoutAskComponent extends SimpleMultiStepComponent implements OnInit {
  @ViewChild(SignInAskComponent)
  private SignInAskComponent: SignInAskComponent;
  @ViewChild(DeliveryAddressAskComponent)
  private DeliveryAddressAskComponent: DeliveryAddressAskComponent
  @ViewChild(PaymentMethodAskComponent)
  private PaymentMethodAskComponent: PaymentMethodAskComponent
  @ViewChild(SummaryPlaceOrderAskComponent)
  private SummaryPlaceOrderAskComponent: SummaryPlaceOrderAskComponent

  @ViewChild('account') account: ElementRef;
  @ViewChild('address') address: ElementRef;
  @ViewChild('payment') payment: ElementRef;
  @ViewChild('review') review: ElementRef;

  element: any;
  Stripe: any;
  currentCountry: any;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  cardForm: FormGroup;
  email: AbstractControl;
  stepsData: any;
  // currentStep: string = 'login';

  // Data select from free trial
  dataSelect: any = [];
  dataImage: string;
  dataProduct: any = [];
  dataProductOption: any = [];
  refillships_date: any;
  refillships_date_2: any;
  refillships_date_next: any;
  refillships_date_next_2: any;
  _time: any;
  listMonth: any = [];

  // Promo
  promoCode: string;
  checkPromoCodeApplied: boolean = false;
  promoCodeError: any;
  notAllowPromoCode: boolean = false;
  promotion: any;
  public promoCodeForm: FormGroup;

  public currentUser;
  public isLoading: boolean = true;

  public workflowStatus: any;
  public activeStepStatus: any;
  resError: any;
  public i18nDialog: any;
  public receipt: any;
  public cartRules: any;
  public cartItems: any;
  public hasFreeTrial: boolean = false;
  public ASKItem: any;
  public hasASK: boolean = false;
  public _dataProductStorage: any;
  public _dataProductCountryActive: any;

  constructor(stripeService: StripeService,
    private builder: FormBuilder,
    private el: ElementRef,
    private checkoutService: CheckoutService,
    public storage: StorageService,
    private usersService: UsersService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private httpService: HttpService,
    private router: Router,
    public appConfig: AppConfig,
    public cartService: CartService,
    private countriesService: CountriesService,
    private routerService: RouterService,
    private deliveryAddressService: DeliveryAddressService,
    private cardService: CardService,
    private gtmService: GTMService,
    private dialogService: DialogService,
  ) {
    super();
    this.cardForm = builder.group({
      cardNumber: ['', Validators.required],
      expiryMonth: ['', Validators.required],
      expiryYear: ['', Validators.required],
      cvc: ['', Validators.required]
    });

    // Validate promocode
    this.promoCodeForm = builder.group({
      promoCode: ['', Validators.required],
    });

    this.Stripe = stripeService.getInstance();
    this.element = $(el.nativeElement);

    this.cardForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
    this.getCheckoutPreviousData();
    cartService.hasUpdate.subscribe(hasUpdate => {
      if (this.getReceiptForAsk()) {
        return;
      }
    });
  }

  checkDelete(data) {

  }

  ngOnInit(): void {
    // set footer short
    this.routerService.setFooterFull(false);
    this.routerService.setHeaderCommon(false);

    this.resError = null;
    // check if cart empty then redirect to homePage
    this.checkCartEmpty();
    // initialize card info
    this.cardNumber = '';
    this.expiryMonth = '';
    this.expiryYear = '';
    this.cvc = '';

    this.setCurrentStep('login');
    this.setStepsData({
      login: { state: 'active', data: {}, step: 'SignInAskComponent' },
      deliveryAddress: { state: 'init', data: {}, step: 'DeliveryAddressAskComponent' },
      payment: { state: 'init', data: {}, step: 'PaymentMethodAskComponent' },
      // summary: {state: 'init', data: {}, step: 'summaryPlaceOrderTrialComponent'},
      hasChangeAccount: { status: false },
    })

    // step 1 => sign up
    this.currentUser = this.storage.getCurrentUserItem();
    // // console.log(this.currentUser);
    if (this.currentUser) {
      this.stepsData.login.data = this.currentUser;
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
    }
    // handle country change event
    this.currentCountry = this.storage.getCountry();
    this.countriesService.country.subscribe(country => {
      this.checkCartEmpty();
      this.currentCountry = country;
    });

    // subscribe card action
    this.cardService.cardAction.subscribe(data => {
      if (data) {
        // action: select change | remove
        if (data.action === 'reset') {
          if (!data.dataReset) {// console.log('data.dataReset 1', data.dataReset);
            this.stepsData.payment.data = {};
            this.stepsData.loadedReview = false;
          } else {
            // console.log('data.dataReset 2', data.dataReset);
          }
        }
        // action: add
        if (data.action === 'add') {

        }
      }
    })

    if (this.getReceiptForAsk() == false) {
      // this.router.navigate(['/']);
    }

    // Promocode
    this.promotion = this.cartService.getPromoCode();
    if (this.promotion.id) {
      this.promoCode = this.promotion.promotionCodes[0].code;
    }

    // Get data storage plan select
    if (this.storage.getASKItem()) {
      let _dataProductStorage = this.storage.getASKItem();
      let _dataProductCountryActive = _dataProductStorage.productCountry;
      let _filterImageDefault = _dataProductStorage.productImages.filter(value => (value.isDefault === true));
      // Get image plan
      this.dataImage = _filterImageDefault[0].url;
      let currDate = new Date();
      this.refillships_date = currDate.setDate(currDate.getDate() + 13);
      this.refillships_date_2 = currDate.setDate(currDate.getDate() + 3);
      this.refillships_date_next = new Date(this.refillships_date);
      this.refillships_date_next_2 = new Date(this.refillships_date_2);
      let _dataProduct = this._dataProductCountryActive.filter(value => (value.id === this._dataProductStorage.id))
      this.dataProduct = _dataProduct[0];
      if (this.promotion.id) {
        this.dataProduct.sellPriceDiscount = this.dataProduct.sellPrice - (this.dataProduct.sellPrice * (this.promotion.discount / 100));
      } else {
        this.dataProduct.sellPriceDiscount = 0;
      }
      let _productOption = this._dataProductStorage.productCountry.productRelateds;
      _productOption.forEach(value => {
        //API to get each related product -> pending API
        this.dataProductOption.push({ productTranslate: value.ProductCountry.Product.productTranslate, qty: value.qty });
      });
    }

    // Promo handle
    this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
      if (value === '') {
        this.cartService.setPromotion();
      }
    })
  }


  checkCartEmpty() {
    if (!this.storage.getASKItem()) {
      this.router.navigate(['/']);
      this.routerService.setFooterFull(true);
      this.routerService.setHeaderCommon(true);
    } else {

    }
  }

  /**
   * step 1 => Sign in
   */
  getNotifyStep1Status(data) {
    if (data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.gtmService.checkout({ step: 1, options: data.email ? 'logged in' : 'guest' });
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }

  goToLogin() {
    if (Object.keys(this.stepsData.login.data).length === 0 || !this.storage.getCurrentUserItem()) {
      this.goto('login');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  hasChangeAccount(data) {
    if (data) {
      this.stepsData.login.data = {};
      this.PaymentMethodAskComponent.selectedCard = null;
      this.stepsData.hasChangeAccount.status = true;
      $(this.account.nativeElement).trigger('click');
    }
  }

  /**
   * step 2 => delivery address
   */
  getNotifyStep2Status(data) {
    this.gtmService.checkout({ step: 2, options: this.stepsData.login.data.email ? 'logged in' : 'guest' });
    if (data.state === 'login') {
      this.stepsData.login.data.email = data.email;
      this.goto('login');
      window.scrollTo(0, 0);
      this.resError = null;
    } else {
      this.stepsData.deliveryAddress.data = data;
      if (Object.keys(this.stepsData.login.data).length == 0 && data.emailByGuest) {
        this.stepsData.login.data = { email: data.emailByGuest }
      }
      this.goto('payment');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  /* Step 2 => delivery address edit */
  getNotifyStep2Edit(edit: boolean) {
    if (edit) {
      this.goto('deliveryAddress');
    }
  }

  goToDeliveryAddress() {
    if (Object.keys(this.stepsData.login.data).length !== 0) {
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  /**
   * step 3 => payment method
   */
  getNotifyStep3Status(data) {
    this.stepsData.payment.data = data;
    this.stepsData.loadedReview = true;
    this.payNow();
    window.scrollTo(0, 0);
    this.resError = null;
  }

  payNow() {
    console.log("ASK == | payNow() start");
    this.loadingService.display(true);
    if (this.storage.getASKItem()) {
      console.log('If ASKItem in storage -> process to payment handler')
      let currentUser = this.storage.getCurrentUserItem();
      let _email = this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest;
      this.handlePaynowWithASK();
    }
  }

  handlePaynowWithASK() {
    console.log("ASK == | handlePaynowWithASK() start");
    let order;
    let _orderData = {};
    let _awesome_shave_kit_item = this.storage.getASKItem();
    let promoCode = '';
    if (this.cartService.getPromoCode().id) {
      promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
    }
    _orderData = {
      userTypeId: 1,
      productCountryId: _awesome_shave_kit_item.productCountry.id,
      email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
      CardId: this.stepsData.payment.data.card.id,
      DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
      BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
      isWeb: true,
      ProductOptionId: _awesome_shave_kit_item.id,
      promotionCode: promoCode
    }

    this.createASKOrder(_orderData)
      .then(result => {
        // console.log(`payment trial result ---  ${result}`);
        // submit to GTM
        this.gtmService.purchaseCart(result['result']);

        this.cartService.reloadSubscriptions();
        if (this.stepsData.payment.data.paymentMethod === 'stripe') {

          order = result['result']['order'];
          if (!this.storage.getCurrentUserItem()) {
            localStorage.setItem('orderId', order.id);
          }

          // clear free trial storage
          this.storage.clearASKItem();

          // clear card, address by guest
          this.storage.clearCardByGuest();
          this.storage.clearDeliveryAddressByGuest();
          // set gtm transaction id
          this.storage.setGtmTransactionID(order.id);

          // redirect to orders page
          this.router.navigate([`/thankyou/${order.id}`]);

          this.loadingService.display(false);
        }
      })
      .catch(error => {
        this.loadingService.display(false);
        // console.log(`payment trial error ${error}`);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.PaymentMethodAskComponent.resError = this.resError;
        // this.summaryPlaceOrderTrialComponent.resError = this.resError;
      });
  }
  setResetCardSelected(data) {
    if (data) {
      this.stepsData.payment.data = data;
    } else {
      this.stepsData.payment.data = {};
    }
  }

  goToPayment() {
    if (this.DeliveryAddressAskComponent.getSelectedAddress() && Object.keys(this.stepsData.login.data).length !== 0) {
      this.DeliveryAddressAskComponent.goToStep3();
    }
  }

  getCheckoutPreviousData() {
    let check = this.storage.getCheckoutData();
    let past_transaction = this.storage.getGtmTransactionID();

    if (check) {
      console.log("================================= getCheckoutData is NOT EMPTY =============================");
      console.log('Checkout Data : ' + check);
      console.log('Past Transaction : ' + past_transaction);
      // this.router.navigate([`/`]);
    } else {
      console.log("================================= getCheckoutData is EMPTY =================================");
      let test_variable = 'teststorage';
      let current_date = new Date();
      let checkoutItems = { "Test": test_variable, "Date": current_date };
      this.storage.setCheckoutData(checkoutItems);
    }
  }

  createASKOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if (utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if (this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order_awesome_shave_kit}/stripe`, orderData)
          .subscribe(
            res => {
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              reject(error);
            }
          );
      }
    });
  }

  getNotifyJustLogin(data) {
    if (data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }
  submitcheck() {
    if (this.stepsData.login.state === 'active') {
      $('#submitStep1').trigger('click');
    } else {
      $('#submit').trigger('click');
    }
  }

  getReceiptForAsk() {
    if (this.storage.getASKItem()) {
      this.hasASK = true;
      this.ASKItem = this.storage.getASKItem();
      let _receipt = {
        totalPrice: 0,
        totalPriceNotDivided: 0,
        specialDiscount: 0,
        discount: 0,
        subTotal: 0,
        shippingFee: 0,
        totalIclTax: 0,
        taxRate: 0,
        applyDiscount: 0,
        taxName: ''
      }
      let currentCountry = this.storage.getCountry();
      _receipt.taxRate = currentCountry.taxRate;
      _receipt.taxName = currentCountry.taxName;
      _receipt.subTotal = this.ASKItem.productCountry.sellPrice;
      _receipt.totalPrice = this.ASKItem.productCountry.sellPrice;
      _receipt.shippingFee = 0;
      this.receipt = _receipt;
      this.receipt.totalIclTax = (parseFloat(this.receipt.totalPrice) + parseFloat(this.receipt.shippingFee)) - parseFloat(this.receipt.discount);
      return true;
    }
    return false;
  }

  // Apply promo code
  applyPromoCode() {
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    let productCountrys = [this.ASKItem.productCountry.id];
    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: this.promoCode,
      productCountryIds,
      productCountrys,
      userTypeId: '1'
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);
        this.dataProduct.sellPriceDiscount = this.dataProduct.sellPrice - (this.dataProduct.sellPrice * (data.discount / 100));
        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
      }
    )
  }

}
