import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPlaceOrderAskComponent } from './summary-place-order-ask.component';

describe('SummaryPlaceOrderAskComponent', () => {
  let component: SummaryPlaceOrderAskComponent;
  let fixture: ComponentFixture<SummaryPlaceOrderAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPlaceOrderAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPlaceOrderAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
