import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HttpService } from '../../../../core/services/http.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { Item } from '../../../../core/models/item.model';
import { CartService } from '../../../../core/services/cart.service';
import { slideInOutCheckoutAnimation } from '../../../../core/animations/slide-in-out-checkout.animation';
import { SimpleSubStepComponent } from '../../../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { NotificationService } from '../../../../core/services/notification.service';

import $ from "jquery";

@Component({
  selector: 'app-summary-place-order-ask',
  templateUrl: './summary-place-order-ask.component.html',
  styleUrls: ['./summary-place-order-ask.component.scss'],
  animations: [slideInOutCheckoutAnimation]
})
export class SummaryPlaceOrderAskComponent extends SimpleSubStepComponent implements OnInit {
  promoCode: string;
  promotion: any;
  @Output() notifyStep4Status: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeAccountStatus: EventEmitter<any> = new EventEmitter<any>();
  @Input() stepsData: any;
  @Input() receipt: any;
  allValid: boolean = true;
  public cartItems: Item[] = [];
  public cartTemplate: string = 'template-2';
  resError: any;
  public hasFreeTrial: boolean = false;
  public hasCustomPlan: boolean = false;
  constructor(el: ElementRef, public cartService: CartService,
    public storage: StorageService,
    private userService: UsersService,
    private loadingService: LoadingService,
    public appConfig: AppConfig,
    private usersService: UsersService,
    private httpService: HttpService,
    private notificationService: NotificationService,
    private translate: TranslateService, ) {
    super(el);
  }

  setActive() {
    // console.log('stepsData', this.stepsData)
  }

  ngOnInit() {
    if (this.storage.getFreeTrialItem()) {
      this.hasFreeTrial = true;
      return;
    }
    // console.log('this.storage.getFreeTrialItem --- ', this.storage.getFreeTrialItem());
    if (this.storage.getCustomPlanItem()) {
      this.cartItems = this.storage.getCustomPlanItem();
      this.hasCustomPlan = true;
    } else {
      this.cartItems = this.cartService.getCart();
    }
    this.promotion = this.cartService.getPromoCode();
    if (this.promotion.id) {
      this.promoCode = this.promotion.promotionCodes[0].code;
    }
  }

  payNow() {
    if (this.storage.getFreeTrialItem()) {
      let _odata = this.storage.getFreeTrialItem();
      _odata.isTrial = true;
      this.notifyStep4Status.emit(_odata);
      return;
    }
    let cartDetails;
    if (this.storage.getCustomPlanItem()) {
      cartDetails = this.storage.getCustomPlanItem().customPlanDetail;
    } else {
      cartDetails = this.cartService.getCart();
    }
    interface orderDetails {
      alaItems: any,
      subsItems: any
    }
    // let orderDetails = [];
    let alaItems = [];
    let subsItems = [];

    // build order detail here
    // console.log('cartDetails', cartDetails);
    cartDetails.forEach(item => {
      if (item.product) {
        alaItems.push({
          ProductCountryId: item.product.productCountry.id,
          qty: item.qty
        });
      }
      if (item.plan) {
        if (!item.plan.planCountry[0]) {
          subsItems.push({
            PlanCountryId: item.plan.planCountry.id,
            qty: item.qty
          });
        }
        if (item.plan.planCountry[0]) {
          subsItems.push({
            PlanCountryId: item.plan.planCountry[0].id,
            qty: item.qty
          });
        }
      }
    });

    let orderDetails = {
      alaItems: alaItems,
      subsItems: subsItems
    }

    // console.log(`orderDetails ===== ${JSON.stringify(orderDetails)}`);
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        let receipt;
        if (this.storage.getCustomPlanItem()) {
          this.cartItems = [];
          let customPlan = this.storage.getCustomPlanItem();
          customPlan.customPlanDetail.forEach(element => {
            this.cartItems.push({
              id: element.product.id,
              product: element.product,
              qty: element.qty
            });
          });
          receipt = this.cartService.getReceiptForCustomPlan(data, this.cartItems);
        } else {
          receipt = this.cartService.getReceipt(data);
        }
        if (this.promotion.id) {
          if (this.promotion.minSpend && receipt.totalPrice < this.promotion.minSpend) {
            this.cartService.setPromotion();
          } else if (receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && this.promotion.isFreeShipping) {
            this.cartService.setPromotion();
          }
        }
        this.notifyStep4Status.emit(orderDetails);
      }
    )
  }

  backToAddressStep() {
    $('.checkout-bar .address').trigger('click');
  }

  backToPaymentStep() {
    $('.checkout-bar .payment').trigger('click');
  }

  changeAccount() {
    // this.cartService.clearCart(true);
    // this.storage.clearFlagSyncCart();
    this.changeAccountStatus.emit(true);
    this.userService.logout();
    this.translate.get('notification').subscribe(res => {
      this.notificationService.addNotification(res.logout);
    });
    setTimeout(() => {
      this.notificationService.addNotification(null);
    }, 2500);
  }
}
