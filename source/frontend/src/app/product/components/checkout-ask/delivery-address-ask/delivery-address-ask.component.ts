import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { HttpService } from '../../../../core/services/http.service';

import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { SimpleMultiStepComponent } from '../../../../core/components/partials/simple-multi-step/simple-multi-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../../../../../app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../../../core/animations/slide-in-out-checkout.animation';
import { ModalService } from '../../../../core/services/modal.service';
import { GlobalService } from '../../../../core/services/global.service';
import { CountriesService } from '../../../../core/services/countries.service';
import { DeliveryAddressService } from '../../../../core/services/delivery-address.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { CartService } from '../../../../core/services/cart.service';

declare var $: any;

@Component({
  selector: 'checkout-delivery-address-ask',
  templateUrl: './delivery-address-ask.component.html',
  styleUrls: ['./delivery-address-ask.component.scss'],
  animations: [slideInOutCheckoutAnimation]
})
export class DeliveryAddressAskComponent extends SimpleMultiStepComponent implements OnInit {
  public selectedShippingAddress: any;
  public selectedBillingAddress: any;
  allValid: boolean = false
  countryStates: Array<any>;
  shippingAddressList: Array<any> = [];
  billingAddressList: Array<any> = [];
  public deliveryAddressList: any = [];
  stage: string = 'list';
  public makeBillingAsShipping: boolean = false;
  public isAddNewAddress: boolean = false;
  public isEditAddress: boolean = false;
  public deliveryAddressForm;
  public isLoading: boolean = true;
  public resError: any;
  public directoryCountries: any = [];
  public directoryConntry: any;
  public emailByGuest: string = '';
  public callingCodes: any;
  public contactForm;
  public isExisted: boolean = false;
  public countryStateDefault: any;
  public countries: any;
  public i18nDialog: any;
  public country: any;
  public economyDateTo: any;
  public economyDateFrom: any;
  public groundDate: any;
  public twoDate: any;
  public oneDate: any;
  public startDelivery: any;
  public endDelivery: any;

  @Output() notifyStep2Status: EventEmitter<any> = new EventEmitter<any>();
  @Output() notifyStep2Edit: EventEmitter<boolean> = new EventEmitter<any>(false);
  @Output() changeAccountStatus: EventEmitter<any> = new EventEmitter<any>();
  @Output() notifyJustLogin: EventEmitter<any> = new EventEmitter<boolean>();
  @Input() stepsData: any;
  @Input() receipt: any;

  constructor(
    el: ElementRef,
    public storage: StorageService,
    public usersService: UsersService,
    public modalService: ModalService,
    public alertService: AlertService,
    public loadingService: LoadingService,
    public translate: TranslateService,
    public _http: HttpService,
    public builder: FormBuilder,
    public appConfig: AppConfig,
    public globalService: GlobalService,
    public countriesService: CountriesService,
    public cartService: CartService,
    public deliveryAddressService: DeliveryAddressService,
    public _dialogService: DialogService,
    public userService: UsersService,
    public httpService: HttpService,
    public notificationService: NotificationService,
  ) {
    super();
    this.deliveryAddressForm = this.builder.array([]);
    this.contactForm = this.deliveryAddressService.initContact();
  }

  setActive() {
    // // console.log('receipt', this.receipt);
    this.translate.get('msg').subscribe(res => {
      this.i18nDialog = res;
    });
    this.isExisted = false;
    // this.callingCodes = this.globalService.getCountryCodes();
    this.getCallingCode();

    this.getDeliveryAddressList();
    this.getCountryStates();
    if (this.storage.getCurrentUser()) {
      this.contactForm.controls.email.setValue(this.storage.getCurrentUser().email);
    } else if (this.storage.getDeliveryAddressByGuest() && this.storage.getDeliveryAddressByGuest()[0]) {
      this.contactForm.controls.email.setValue(this.storage.getDeliveryAddressByGuest()[0].email);
    } else {
      this.contactForm.controls.email.setValue('');
    }
    this.contactForm.controls.email.valueChanges.subscribe(value => {
      let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
      if (re.test(value)) {
        this.checkEmail(value)
      } else {
        this.isExisted = false;
      }
    })

    this.country = this.storage.getCountry();

    this.economyDateTo = this.globalService.calculateWorkingDay(new Date(), 4);
    this.economyDateFrom = this.globalService.calculateWorkingDay(new Date(), 8);
    this.groundDate = this.globalService.calculateWorkingDay(new Date(), 4);
    this.twoDate = this.globalService.calculateWorkingDay(new Date(), 2);
    this.oneDate = this.globalService.calculateWorkingDay(new Date(), 1);
  }

  getCallingCode() {
    this.countries = this.countriesService.getCountries();
    this.callingCodes = [];
    if (this.countries && this.countries.length > 0) {
      this.setCallingCodes(this.countries)
    } else {
      this.getCountries().then((data: any = []) => {
        // console.log('data', data)
        if (data && data.length > 0) {
          this.setCallingCodes(this.countries);
        }
      })
    }
  }

  setCallingCodes(data) {
    data.forEach(country => {
      if (country.callingCode) {
        this.callingCodes.push(
          {
            'code': country.code,
            'dial_code': '+' + country.callingCode,
            'name': country.name
          }
        );
      }
    });
  }

  getCountries() {
    return new Promise((resolved, reject) => {
      this.httpService._getList(this.appConfig.config.api.countries).subscribe(
        data => {
          this.countries = data;
          resolved(data)
          // this.countriesService.setCountries(data);
        },
        error => {
          reject(error)
          // console.log(`Cannot get country: ${error}`)
        }
      )
    });
  }

  checkEmail(_email: string) {

    // check email is exist or not
    this._http._create(this.appConfig.config.api.checkEmail, { email: _email })
      .subscribe(res => {
        if (res && res.user) {
          if (res.user.isActive) {
            this.isExisted = true;
          } else {
            this.isExisted = false;
          }
        } else {
          this.isExisted = false;
        }
      });

  }

  validate() {
    return {
      allValid: this.allValid,
      data: this.getSelectedAddress()
    }
  }

  ngOnInit() {
    this.makeBillingAsShipping = false;

    this.initDeliveryAddress();
    this.getCountryStates();

    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });


    // Time
    this.startDelivery = new Date();
    let currDate = new Date();
    this.endDelivery = currDate.setDate(currDate.getDate() + 6);
    if (this.storage.getCountry().code.toUpperCase() === 'KOR') {
      this.loadingService.display(true);
      this.httpService._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
        data => {
          if (data) {
            this.startDelivery = new Date(data);
            currDate = new Date(data);
            this.endDelivery = currDate.setDate(currDate.getDate() + 6);
          }
          this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
        }
      )
    }
  }

  initDeliveryAddress() {
    let control = <FormArray>this.deliveryAddressForm;
    let initAddressCtrl = this.deliveryAddressService.initAddress();
    initAddressCtrl.removeControl("firstName");
    initAddressCtrl.removeControl("lastName");
    initAddressCtrl.removeControl("callingCode");
    initAddressCtrl.addControl("fullName", new FormControl("", Validators.required));

    if (this.selectedShippingAddress) {
      // let contactNumber = this.selectedShippingAddress.contactNumber.replace('+' + this.storage.getCurrentUser().Country.callingCode, '');
      // if(contactNumber.indexOf(' ') === 0) {
      //   contactNumber = contactNumber.substr(1, contactNumber.length);
      // }
      initAddressCtrl.patchValue({
        fullName: this.selectedShippingAddress.fullName ? this.selectedShippingAddress.fullName : this.selectedShippingAddress.firstName + ' ' + this.selectedShippingAddress.lastName,
        address: this.selectedShippingAddress.address,
        city: this.selectedShippingAddress.city,
        state: this.selectedShippingAddress.state,
        portalCode: this.selectedShippingAddress.portalCode,
        // callingCode: '+' + this.storage.getCurrentUser().Country.callingCode,
        contactNumber: this.selectedShippingAddress.contactNumber
      });
    } else {
      // initAddressCtrl.patchValue({ callingCode: '+' + this.storage.getCountry().callingCode });
      if (this.storage.getCurrentUser()) {
        initAddressCtrl.patchValue({ firstName: this.storage.getCurrentUser().firstName, lastName: this.storage.getCurrentUser().lastName });
      }
      if (this.countryStates && this.countryStates.length > 0) {
        let state = this.countryStates.filter(state => state.isDefault)[0];
        state = state ? state : this.countryStates[0];
        initAddressCtrl.patchValue({ state: state.name });
        // if (this.storage.getCountry().code === 'MYS') {
        //   initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name });
        // }
        // if (this.storage.getCountry().code === 'SGP') {
        //   initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Singapore')[0].name });
        // }
      }
    }


    // setTimeout(() => {
    //   // console.log('this.countryStates', this.countryStates);
    // if (this.countryStates) {
    //   if (this.storage.getCountry().code === 'MYS') {
    //     initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name });
    //   }
    //   if (this.storage.getCountry().code === 'SGP') {
    //     initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Singapore')[0].name });
    //   }
    // }
    // }, 300);

    control.push(initAddressCtrl);
  }

  /**
   * step 2 => delivery address
   */
  // show form add new addresss
  addNewAddress(addressType) {
    this.modalService.open('create-address-modal', addressType);

    this.initDeliveryAddress();

    // this.deliveryAddressForm.reset();
    this.stage = 'add';
    // this.deliveryAddressForm.value.addressType = addressType ? [addressType] : [];
    this.resError = null;
    this.getDirectoryCountry();
  }

  // open address list popup
  addressList(_addressType: string) {
    this.modalService.open('delivery-addresses-modal', { reGetDB: true, addressType: _addressType });
  }

  editAddress(address: any) {
    this.deliveryAddressForm = this.builder.array([]);
    let control = <FormArray>this.deliveryAddressForm;
    let initAddressCtrl = this.deliveryAddressService.initAddress();
    initAddressCtrl.addControl("fullName", new FormControl("", Validators.required));
    initAddressCtrl.patchValue(address);
    control.push(initAddressCtrl);

    // this.deliveryAddressForm.patchValue(address);
    this.stage = 'edit';
    this.resError = null;
  }

  getDirectoryCountry() {
    if (this.directoryCountries.length < 1) {
      this.globalService.getDirectoryCountries().then(data => {
        this.directoryCountries = data;
        this.directoryConntry = this.globalService.getDirectoryCountry(this.directoryCountries);
      })
    } else {
      this.directoryConntry = this.globalService.getDirectoryCountry(this.directoryCountries);
    }
    // console.log('this.directoryConntry', this.directoryConntry);
  }

  // get delivery address list
  getDeliveryAddressList() {
    this.isLoading = true;
    // if (!(this.stepsData &&
    //   this.stepsData.deliveryAddress &&
    //   this.stepsData.deliveryAddress.data &&
    //   this.stepsData.deliveryAddress.data.billing) || !this.selectedShippingAddress || !this.selectedBillingAddress) { // check if has no set delivery Address
    if (this.storage.getCurrentUser()) {
      this.shippingAddressList = [];
      this.billingAddressList = [];
      // this.selectedShippingAddress = null;
      // this.selectedBillingAddress = null;
      this._http._getList(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
        data => {

          // build billing address and delivery address
          if (data && data.length > 0) {
            this.deliveryAddressList = data;
            let defaultShipping = this.storage.getCurrentUser().defaultShipping;
            let defaultBilling = this.storage.getCurrentUser().defaultBilling;


            if (!defaultShipping) {
              // data[0].checked = true;
              this.selectedShippingAddress = data[0];
              this.deliveryAddressForm = this.builder.array([]);
            }

            if (!defaultBilling) {
              // data[0].checked = true;
              this.selectedBillingAddress = data[0];
            }

            data.forEach((address, index) => {
              if (!this.selectedShippingAddress && address.id === defaultShipping) {
                address.checked = true;
                this.selectedShippingAddress = address;
                this.selectedShippingAddress.index = index + 1;
              }
              if (!this.selectedBillingAddress && address.id === defaultBilling) {
                address.checked = true;
                this.selectedBillingAddress = address;
                this.selectedBillingAddress.index = index + 1;
              }
            });

            if (!this.selectedShippingAddress) {
              this.selectedShippingAddress = data[0];
              this.selectedShippingAddress.index = 1;
            }

            if (!this.selectedBillingAddress) {
              this.selectedBillingAddress = data[0];
              this.selectedBillingAddress.index = 1;
            }

            if (this.selectedShippingAddress) {
              this.deliveryAddressForm = this.builder.array([]);
              this.initDeliveryAddress();
            }
          } else {
            this.deliveryAddressList = [];
            this.makeBillingAsShipping = false;
            this.deliveryAddressForm = this.builder.array([]);
            this.initDeliveryAddress();
          }
          this.isLoading = false;
        },
        error => {
          this.isLoading = false;
          // console.log(error.error)
        }
      )
    } else {
      this.isLoading = false;
      this.deliveryAddressList = this.storage.getDeliveryAddressByGuest() ? this.storage.getDeliveryAddressByGuest() : [];
      if (this.deliveryAddressList && this.deliveryAddressList.length > 0) {
        // check for case address has been removed
        let hasRemove = true;
        if (this.selectedShippingAddress && this.selectedBillingAddress) {
          this.deliveryAddressList.forEach(address => {
            if (this.selectedShippingAddress.id === address.id) {
              this.selectedShippingAddress = address;
              hasRemove = false;
            }
            if (this.selectedBillingAddress.id === address.id) {
              this.selectedBillingAddress = address;
              hasRemove = false;
            }
          });
        } else {
          hasRemove = false;
        }

        if (hasRemove) {
          this.selectedShippingAddress = null;
          this.selectedBillingAddress = null;
        }

        if (!this.selectedShippingAddress) {
          if (this.deliveryAddressList && this.deliveryAddressList.length > 0) {
            this.selectedShippingAddress = this.deliveryAddressList[0];
            this.selectedShippingAddress.index = 1;
          }
        }
        if (!this.selectedBillingAddress) {
          if (this.deliveryAddressList && this.deliveryAddressList.length > 0) {
            this.selectedBillingAddress = this.deliveryAddressList[0];
            this.selectedBillingAddress.index = 1;
          }
        }

      } else {
        this.deliveryAddressList = [];
        this.makeBillingAsShipping = false;
        this.deliveryAddressForm = this.builder.array([]);
        this.initDeliveryAddress();
      }
    }
    // } else {
    //   this.isLoading = false;
    // }


  }

  // get country states
  getCountryStates() {
    if (!this.countryStates || !this.storage.getCurrentUser()) {
      this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
        data => {
          this.countryStates = data;
          if (this.countryStates && this.countryStates.length > 0) {
            // if (this.storage.getCountry().code === 'MYS') {
            //   this.deliveryAddressForm.controls[0].controls.state.setValue(this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name);
            // }
            // if (this.storage.getCountry().code === 'SGP') {
            //   this.deliveryAddressForm.controls[0].controls.state.setValue(this.countryStates.filter(country => country.name == 'Singapore')[0].name);
            // }
            let state = this.countryStates.filter(state => state.isDefault)[0];
            state = state ? state : this.countryStates[0];
            if (this.deliveryAddressForm.controls[0].controls.state.value.length <= 0) {
              this.deliveryAddressForm.controls[0].controls.state.setValue(state.name);
            }
          }
        },
        error => {
          // console.log(error.error)
        }
      )
    }
  }

  addressChange(event) {
    if (!event.target.checked) {
      this.makeBillingAsShipping = false;
      let control = <FormArray>this.deliveryAddressForm;
      control.removeAt(1);
      // this.initDeliveryAddress();
    } else {
      this.makeBillingAsShipping = true;
      // let control = <FormArray>this.deliveryAddressForm;
      // control.removeAt(1);
      this.initDeliveryAddress();
    }
  }

  // do add new address
  doAddDeliveryAddress(data) {
    this.resError = null;
    let curUser = this.storage.getCurrentUser();
    this.loadingService.display(true);
    if (data.length) {
      data[0].CountryId = this.storage.getCountry().id;
      // data[0].contactNumber = data[0].callingCode + ' ' + data[0].contactNumber;
      data[0].firstName = (data[0].fullName.indexOf(' ') > -1) ? data[0].fullName.substr(0, data[0].fullName.indexOf(' ')) : data[0].fullName;
      data[0].lastName = (data[0].fullName.indexOf(' ') > -1) ? data[0].fullName.substr(data[0].fullName.indexOf(' ') + 1) : '';
      if (data.length > 1) {
        data[1].CountryId = this.storage.getCountry().id;
        // data[1].contactNumber = data[1].callingCode + ' ' + data[1].contactNumber;
        data[1].firstName = (data[0].fullName.indexOf(' ') > -1) ? data[0].fullName.substr(0, data[0].fullName.indexOf(' ')) : data[0].fullName;
        data[1].lastName = (data[0].fullName.indexOf(' ') > -1) ? data[0].fullName.substr(data[0].fullName.indexOf(' ') + 1) : '';
      }
    }

    if (data.length > 1) {
      data[0].defaultShipping = true;
      data[1].defaultBilling = true;
    }

    if (this.deliveryAddressList.length > 0 && this.selectedShippingAddress) {
      data[0].id = this.selectedShippingAddress.id;
      this._http._update(`${this.appConfig.config.api.delivery_address}/${this.selectedShippingAddress.id}`.replace('[userID]', curUser.id), data[0])
        .subscribe(
          res => {
            if (res.ok) {
              this.selectedShippingAddress = res.data;
              this.selectedBillingAddress = res.data;
              this.getDeliveryAddressList();
              // goto step3
              this.goToStep3();
            }
            this.stage = 'list';
            this.loadingService.display(false);
            this.resError = null;
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
          }
        )
    } else {
      if (curUser) {
        data.CountryId = this.storage.getCountry().id;
        // data.contactNumber = data.callingCode + ' ' + data.contactNumber;
        this._http._create(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', curUser.id), data).subscribe(
          res => {
            if (res.ok) {
              if (res.data.length > 1) {
                this.storage.resetDefaultShipping(res.data[0].id);
                this.storage.resetDefaultBilling(res.data[1].id);
                this.selectedShippingAddress = res.data[0];
                this.selectedBillingAddress = res.data[1];
              }
              if (res.data.length == 1) {
                this.selectedShippingAddress = res.data[0];
                this.selectedBillingAddress = res.data[0];
              }
              this.getDeliveryAddressList();
              // goto step3
              this.goToStep3();
            }
            this.stage = 'list';
            this.loadingService.display(false);
            this.resError = null;
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
          }
        )
      } else {
        this._http._create(`${this.appConfig.config.api.users}/deliveries`, data)
          .subscribe(
            res => {
              if (res.ok) {

                // console.log('res', res)

                if (res.data.length > 1) {
                  this.selectedShippingAddress = res.data[0];
                  this.selectedBillingAddress = res.data[1];
                }
                if (res.data.length == 1) {
                  this.selectedShippingAddress = res.data[0];
                  this.selectedBillingAddress = res.data[0];
                }

                res.data.forEach(address => {
                  address.email = this.contactForm.value.email;
                  this.storage.setDeliveryAddressByGuest(address);
                });

                this.getDeliveryAddressList();
                // goto step3
                this.goToStep3();
              }
              this.stage = 'list';
              this.loadingService.display(false);
              this.resError = null;
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          )
      }
    }
  }

  resetErrorsForBillingAddressForm(_value: any) {
    let value: any;
    if (_value) {
      value = { 'required': true };
    } else {
      value = null;
    }
    this.deliveryAddressForm.controls['firstNameBill'].setErrors(value);
    this.deliveryAddressForm.controls['lastNameBill'].setErrors(value);
    this.deliveryAddressForm.controls['contactNumberBill'].setErrors(value);
    this.deliveryAddressForm.controls['addressBill'].setErrors(value);
    this.deliveryAddressForm.controls['stateBill'].setErrors(value);
    this.deliveryAddressForm.controls['cityBill'].setErrors(value);
    this.deliveryAddressForm.controls['portalCodeBill'].setErrors(value);
  }


  // do add new address
  doEditDeliveryAddress(data) {
    let curUser = this.storage.getCurrentUser();
    if (curUser && this.deliveryAddressForm.valid) {
      this.loadingService.display(true);
      data.CountryId = 1;
      this._http._update(`${this.appConfig.config.api.delivery_address}/${data.id}`.replace('[userID]', curUser.id), data)
        .subscribe(
          res => {
            if (res.ok) {
              this.getDeliveryAddressList();
              this.stage = 'list';
              this.loadingService.display(false);
              this.resError = null;
            }
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
          }
        )
    }

    // this.loadingService.display(false);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getSelectedAddress() {
    if (this.selectedShippingAddress && this.selectedBillingAddress) {
      return {
        shipping: this.selectedShippingAddress,
        billing: this.selectedBillingAddress,
        emailByGuest: !this.storage.getCurrentUser() ? this.contactForm.value.email : '',
      };
    } else {
      return false;
    }
  }

  // goToStep3
  goToStep3() {
    this.allValid = true;
    this.notifyStep2Status.emit(this.getSelectedAddress());
    this.resError = null;
  }

  // Confirm Deletion modal
  showConfirmDeletionModal(item: any, event: any) {
    // this.modalService.open('confirm-deletion-modal', { item: item, event: event });
  }

  // delete address
  deleteAddress(address, event) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        event.stopPropagation();
        if (this.storage.getCurrentUser()) {
          this._http._delete(`${this.appConfig.config.api.delivery_address}/${address.id}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
            data => {
              if (address.addressType === 'billing') {
                this.selectedBillingAddress = null;
              } else {
                this.selectedShippingAddress = null;
              }
              this.getDeliveryAddressList();
              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          )
        } else {
          this._http._delete(`${this.appConfig.config.api.users}/deliveries/${address.id}`).subscribe(
            data => {
              // reset billing address list with checked
              this.billingAddressList = this.billingAddressList.filter(value => value !== address);
              if (address.id === this.selectedBillingAddress.id && this.billingAddressList[0]) {
                this.billingAddressList[0].checked = true;
                this.selectedBillingAddress = this.billingAddressList[0];
              }

              // reset billing address list with checked
              this.shippingAddressList = this.shippingAddressList.filter(value => value !== address);
              if (address.id === this.selectedShippingAddress.id && this.shippingAddressList[0]) {
                this.shippingAddressList[0].checked = true;
                this.selectedShippingAddress = this.shippingAddressList[0];
              }

              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          )
        }
      }
    }));

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  // change default address
  changeShippingAddress(addressId) {
    // console.log('this.selectedShippingAddress', this.selectedShippingAddress);
    // console.log('addressId', addressId);
    this.deliveryAddressList.forEach((deliveryAddress, index) => {
      deliveryAddress.isDefault = false;
      deliveryAddress.checked = false;
      if (addressId === deliveryAddress.id) {
        deliveryAddress.checked = true;
        this.selectedShippingAddress = deliveryAddress;
        this.selectedShippingAddress.index = index + 1;
      }
    });
  }
  changeBillingAddress(addressId) {
    this.deliveryAddressList.forEach((deliveryAddress, index) => {
      deliveryAddress.isDefault = false;
      deliveryAddress.checked = false;
      if (addressId === deliveryAddress.id) {
        deliveryAddress.checked = true;
        this.selectedBillingAddress = deliveryAddress;
        this.selectedBillingAddress.index = index + 1;
      }
    });

  }

  hasResetDefault(_data) {
    if (_data) {
      // console.log('_data', _data)
      if (_data.addressType === 'shipping') {
        this.changeShippingAddress(_data.id);
      }
      if (_data.addressType === 'billing') {
        this.changeBillingAddress(_data.id);
      }
    }
  }

  hasChangeAddress(_res) {
    if (_res) {
      this.getDeliveryAddressList();
    }
  }

  changeAccount() {
    // this.cartService.clearCart(true);
    // this.storage.clearFlagSyncCart();
    this.changeAccountStatus.emit(true);
    this.userService.logout();
    this.translate.get('notification').subscribe(res => {
      this.notificationService.addNotification(res.logout);
    });
    setTimeout(() => {
      this.notificationService.addNotification(null);
    }, 2500);
  }

  hasRemove(_res) {
    if (_res && _res.id) {
      // if (_res.addressType === 'shipping') {
      //   this.selectedShippingAddress = null;
      // }
      // if (_res.addressType === 'billing') {
      //   this.selectedBillingAddress = null;
      // }
      if (_res.id === this.selectedShippingAddress.id) {
        this.selectedShippingAddress = null;
      }
      if (_res.id === this.selectedBillingAddress.id) {
        this.selectedBillingAddress = null;
      }
    }
    this.getDeliveryAddressList();
  }

  loginModal(_email?) {
    this.modalService.open('app-login-modal', { step: 'delivery-address', email: _email });
  }

  hasLogin(data) {
    if (data) {
      this.notifyJustLogin.emit(data.user);
      this.getDeliveryAddressList();
    }
  }

  backToLogin() {
    // console.log('this.contactForm.value.email', this.contactForm.value.email);
    this.allValid = true;
    this.notifyStep2Status.emit({ state: 'login', email: this.contactForm.value.email });
    this.resError = null;
  }

  editDeliveryAddress(edit: boolean) {
    this.notifyStep2Edit.emit(edit);
  }

  // changeShippingFee(value) {
  //   if(this.storage.getCustomPlanItem()) {
  //     this.cartService.changeCustomShipping(value);
  //   } else {
  //     this.cartService.changeShipping(value);
  //   }
  // }
  togglerTooltip() {
    $('.tooltip_info').toggleClass('hidden');
  }
}

