import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAddressAskComponent } from './delivery-address-ask.component';

describe('DeliveryAddressAskComponent', () => {
  let component: DeliveryAddressAskComponent;
  let fixture: ComponentFixture<DeliveryAddressAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryAddressAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryAddressAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
