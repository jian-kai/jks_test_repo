import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodAskComponent } from './payment-method-ask.component';

describe('PaymentMethodAskComponent', () => {
  let component: PaymentMethodAskComponent;
  let fixture: ComponentFixture<PaymentMethodAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
