import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartTemplateAskComponent } from './cart-template-ask.component';

describe('CartTemplateAskComponent', () => {
  let component: CartTemplateAskComponent;
  let fixture: ComponentFixture<CartTemplateAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartTemplateAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartTemplateAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
