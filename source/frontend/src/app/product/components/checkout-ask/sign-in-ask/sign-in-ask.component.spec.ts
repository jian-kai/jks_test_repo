import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInAskComponent } from './sign-in-ask.component';

describe('SignInAskComponent', () => {
  let component: SignInAskComponent;
  let fixture: ComponentFixture<SignInAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
