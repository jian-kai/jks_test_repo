import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutAskComponent } from './checkout-ask.component';

describe('CheckoutAskComponent', () => {
  let component: CheckoutAskComponent;
  let fixture: ComponentFixture<CheckoutAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

