import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AlertService } from '../../../core/services/alert.service';
import { GTMService } from '../../../core/services/gtm.service';
import { Item } from '../../../core/models/item.model';
import { CartService } from '../../../core/services/cart.service';

@Component({
  selector: 'add-to-cart-modal',
  templateUrl: './add-to-cart-modal.component.html',
  styleUrls: ['./add-to-cart-modal.component.scss']
})
export class AddToCartModalComponent extends ModalComponent {
  private currentUser: any;
  public changePasswordForm: FormGroup;
  public resError: any;
  public isLoading: boolean = true;
  public productInfo: any;
  public item: Item;
  public cartRules: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private builder: FormBuilder,
    private loadingService: LoadingService,
    private gtmService: GTMService,
    public cartService: CartService,
    private alertService: AlertService, ) {
    super(modalService, el);
    this.id = 'add-to-cart-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }

    this.changePasswordForm = builder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required],
    });

  }

  setInitData(_slug) {
    this.loadingService.display(true);
    this.productInfo = null;
    let options = {
      countryCode: 'mys',
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this._http._getDetail(`${this.appConfig.config.api.products}/${_slug}?` + params.toString()).subscribe(
      data => {
        this.productInfo = data;
        // console.log('this.productInfo', this.productInfo);
        this.item = {
          id: data.id,
          product: data
        }
        this.item.qty = 1;
        this.gtmService.clickProduct(this.item);
        this.gtmService.productDetail(this.item);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error);
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
    
    //get cart rule
    this._http._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
      }
    )
  }

  // add item to cart
  addToCart() {
    // this.cartService.applyAddItemEvent();
    this.cartService.addItem(this.item);
    this.cartService.checkFreeProduct(this.cartRules);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
    // this.modalService.open('add-cart-modal', this.item);
    this.hasChange.emit(true);
    this.close();
  }

  //minus quatity of the item
  minusQuatity() {
    if (this.item.qty > 1) {
      this.item.qty -= 1;
    }
  }

  //plus quatity of the item
  plusQuatity() {
    this.item.qty += 1;
  }

  //change quatity of the item
  changeQuatity() {
    if (this.item.qty < 1) {
      this.item.qty = 1;
    }
  }
}
