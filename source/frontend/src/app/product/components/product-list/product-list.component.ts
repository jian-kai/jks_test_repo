import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CountriesService } from '../../../core/services/countries.service';
import { CartService } from '../../../core/services/cart.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { GTMService } from '../../../core/services/gtm.service';
import { ModalService } from '../../../core/services/modal.service';
import $ from "jquery";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productList: Object = {};
  productid: any;
  productTypes: any = [];
  productsSlider = [];
  productsShaveKit: any;
  featuredProducts: any;
  public focusTypeBlock: string = '';
  public productsShavesKit: any;
  public isloading: boolean = true;
  public cartRules: any;
  private id: any;
  private prodId: any;
  public productSliderOptions = {
    displayList: false,
    displayControls: true
  }
  public apiURL: String;
  constructor(private httpService: HttpService,
    public modalService: ModalService,
    public storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    private router: Router,
    public cartService: CartService,
    private gtmService: GTMService,
    private location: Location,
  ) {
    this.apiURL = appConfig.config.apiURL;
    let curCountry = this.storage.getCountry();
    if (!curCountry.isWebAlaCarte) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    var anchor = window.location.hash;
    $(window).on("load", function () {
      if ($(anchor).length > 0) {
        //alert(anchor);
        var box = $(''+anchor);
        //alert(box);
        box.addClass('ScrollPad');
        $('html, body').animate({
          scrollTop: $(anchor).offset().top
        }, 2000);
        box.removeClass('ScrollPad')
      }
    })
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    })
    this.loadingService.status.subscribe((value: boolean) => {
      this.isloading = value;
    })

    // get cart rule
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
      }
    )
  }

  reloadPage() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.productList = [];
    this.productsSlider = [];
    // get all products
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        console.log('data', data);
        this.gtmService.itemImpressions(data);
        data.forEach((product, index) => {

          let productImage: any;

          // fetch to find default image
          // tslint:disable-next-line:no-shadowed-variable
          product.productImages.forEach((imageItem, index) => {
            if (imageItem.isDefault) {
              productImage = imageItem;
            }
          });


          // if (product.ProductType.id === 1) {
          //   if (this.productList[product.ProductType.name]) {
          //     this.productList[product.ProductType.name].push(product);
          //   } else {
          //     this.productTypes.push(product.ProductType);
          //     this.productList[product.ProductType.name] = [product];

          if (product.isFeaturedProduct === true) {

            if (this.productList[product.ProductType.name]) {
              this.productList[product.ProductType.name].push(product);
              // this.productList[product.ProductType.name].sort(function (a, b) {
              //   return b.productList - a.productList;
              // });
              console.log('A ----- ' + JSON.stringify(product.ProductType.id) + ' Product Name ' + JSON.stringify(product.ProductType.name) + 'Product Order ' + JSON.stringify(product.ProductType.order));
            }
            if (!this.productList[product.ProductType.name]) {
              this.productTypes.push(product.ProductType);
              this.productList[product.ProductType.name] = [product];
              // this.productList[product.ProductType.name].sort(function (a, b) {
              //   return a.productList - b.productList;
              // });
              console.log('B ----- ' + JSON.stringify(product.ProductType.id) + ' Product Name ' + JSON.stringify(product.ProductType.name) + 'Product Order ' + JSON.stringify(product.ProductType.order));
            }

          }


          // if (product.ProductType.id !== 1) {
          // if(this.productList[product.ProductType.name]) {
          //   this.productList[product.ProductType.name].push(product);
          // } else {
          //   this.productList[product.ProductType.name] = [product];
          // }
          // } else { // if product type is 'shaves kit'
          //   if(this.productList[product.ProductType.name]) {
          //     this.productsShavesKit.push(product);
          //   } else {
          //     this.productsShavesKit = [product];
          //   }
          // }
          // get productType
          // this.productTypes = Object.keys(this.productList);
          // push to product Slider
          this.productsSlider.push({
            id: product.id,
            title: product.productTranslate.name,
            description: product.productTranslate.description,
            imageUrl: productImage ? productImage.url : product.productImages[0].url
          });
        });
        this.loadingService.display(false);
        // console.log('productsShavesKit', this.productsShavesKit)
      },
      error => {
        // console.log(error.error)
      }
    )


  }

  emailLeadPopout() {
    if (this.storage.getLanguageCode() === 'KO' || this.storage.getCountry().code.toUpperCase() === 'KOR' || this.storage.getCountry().code.toUpperCase() === 'HKG') {
      this.modalService.open('collect-email-modal');
    }
  }

  goProductDetail(Id) {

    switch (Id) {
      case 1010:
        this.prodId = "1";
        this.router.navigateByUrl(`/shave-plans/free-trial?prodId=${this.prodId}`);
        break;
      case 1011:
        this.prodId = "2";
        this.router.navigateByUrl(`/shave-plans/free-trial?prodId=${this.prodId}`);
        break;
      case 1012:
        this.prodId = "3";
        this.router.navigateByUrl(`/shave-plans/free-trial?prodId=${this.prodId}`);
        break;
      default:
        this.prodId = "1";
        this.router.navigateByUrl(`/shave-plans/free-trial?prodId=${this.prodId}`);
    }

    // this.router.navigateByUrl('');
    // this.router.navigateByUrl(`/shave-plans/free-trial?prodId=${prodId}`);
    // this.router.navigateByUrl(`/shave-plans/free-trial`);
    // this.router.navigateByUrl(`/shave-plans/custom-plan?${prodId}`);    
    // console.log('id ----- ' + (prodId));



  }

  // add item to cart
  addToCart(_item) {
    this.cartService.applyAddItemEvent();
    let item = {
      id: _item.id,
      product: _item,
      qty: 1
    }
    this.cartService.addItem(item);
    this.cartService.checkFreeProduct(this.cartRules);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
  }
}


// $(document).ready(function () {
//   if ($(anchor).length > 0)
//     $('html, body').animate({
//       scrollTop: $(anchor).offset().top
//     }, 2000);
// })

// $(window).load(function() {
// page is fully loaded, including all frames, objects and images
// alert("window is loaded");
// });

// $(document).ready(function() {
//   // document is loaded and DOM is ready
//   alert("document is ready");
// })


