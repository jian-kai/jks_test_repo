
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { ProductListComponent } from './product-list.component';

// Modules
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
{ path: '', component: ProductListComponent }
];

@NgModule({
imports: [
TranslateModule,
CommonModule,
SharedModule,
RouterModule.forChild(ROUTES),
],
declarations: [
ProductListComponent,
]
})
export class ProductListModule { }