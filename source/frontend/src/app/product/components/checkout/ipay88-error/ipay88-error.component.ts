import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-ipay88-error',
  templateUrl: './ipay88-error.component.html',
  styleUrls: ['./ipay88-error.component.scss']
})
export class Ipay88ErrorComponent implements OnInit {
  orderId: string;
  message: string;
  constructor(private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orderId = params.orderId;
      this.message = decodeURIComponent(params.message);
    });
  }

}
