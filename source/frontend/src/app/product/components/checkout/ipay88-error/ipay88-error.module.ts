import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { Ipay88ErrorComponent } from './ipay88-error.component';
import { SharedModule } from './../../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: Ipay88ErrorComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [
    Ipay88ErrorComponent,
  ]
})
export class Ipay88ErrorModule { }
