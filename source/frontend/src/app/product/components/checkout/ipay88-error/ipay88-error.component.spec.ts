import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ipay88ErrorComponent } from './ipay88-error.component';

describe('Ipay88ErrorComponent', () => {
  let component: Ipay88ErrorComponent;
  let fixture: ComponentFixture<Ipay88ErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ipay88ErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ipay88ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
