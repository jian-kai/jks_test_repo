import { Component, OnInit, Input , Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import {HttpService} from '../../../../core/services/http.service';

import { StorageService } from '../../../../core/services/storage.service';
import {UsersService} from '../../../../auth/services/users.service';
import {AlertService} from '../../../../core/services/alert.service';
import {LoadingService} from '../../../../core/services/loading.service';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
@Component({
  selector: 'checkout-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  // @Input() signInForm1: FormGroup;
  // @Input() isComfirmPassword: Boolean;
  @Output() notify: EventEmitter<boolean> = new EventEmitter<boolean>(false);
  @Output() isSignIn: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  public isSignUp: Boolean = true;
  public isSendEmail1: Boolean = false;
  public isSendEmail2: Boolean = false;
  private signUpForm: FormGroup;
  public isLoading: boolean = true;
  resError: any;
  public promoCode_param: any;
  public promoCode: any;

  constructor(private builder: FormBuilder,
   public storage: StorageService,
    private usersService: UsersService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private location: Location,
    private httpService: HttpService) {
    /**
     * step 1 => sign up
     */
    this.signUpForm = builder.group({
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.signUpForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
   }

  ngOnInit() {
    const promoCode_param = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.promoCode = promoCode_param.get('promo');
  }

  /**
   * step 1 => Sign up
   */


  // go to the sign up form
  goToSignUpForm() {
    this.isSignUp = true;
  }

  // handle sign up
  doSignUp(credentials) {
    this.loadingService.display(true);
    credentials.defaultLanguage = this.translate.currentLang ? this.translate.currentLang : this.translate.defaultLang;
    credentials.utmData = {
      source: this.storage.getGtmSource(),
      medium: this.storage.getGtmMedium(),
      campaign: this.storage.getGtmCampaign(),
      content: this.storage.getGtmContent(),
      term: this.storage.getGtmTerm()
    };
    this.usersService._register(credentials).subscribe(
      data=> {
        this.alertService.clearMessage();
        this.storage.setAuthToken(data.token);
        this.storage.setCurrentUser(data.user);
        this.usersService.setLogedin();
        this.isSignUp = false;
        this.isSendEmail1 = true;
        this.loadingService.display(false);
      },
      error=> {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // back to sign in form
  backToSignIn() {
    this.isSignUp = false;
    this.isSignIn.emit(true)
  }

  // handle send email again
  sendEmailAgain() {
    this.isSendEmail1 = false;
    this.isSendEmail2 = true;
  }

  // go to step 2 => delivery address
  goToStep2() {
    this.notify.emit(true);
    // get delivery addresses
  }
}
