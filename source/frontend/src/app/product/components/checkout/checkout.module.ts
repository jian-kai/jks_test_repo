import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// component
import { CheckoutComponent } from './checkout.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SummaryPlaceOrderComponent } from './summary-place-order/summary-place-order.component';

import { CartTemplateComponent } from './../../../product/components/cart-template/cart-template.component';

// module
import { SharedModule } from './../../../core/directives/share-modules';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
// routes
export const ROUTES: Routes = [
  { path: '', component: CheckoutComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ CheckoutComponent, DeliveryAddressComponent,
    PaymentMethodComponent, SignInComponent, SummaryPlaceOrderComponent, CartTemplateComponent
  ]
})
export class CheckoutModule { }
