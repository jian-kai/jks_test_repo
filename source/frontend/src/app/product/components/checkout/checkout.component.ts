import { Component, OnInit, ElementRef, ViewChild, QueryList } from '@angular/core';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { AppConfig } from 'app/config/app.config';
import { HttpService } from '../../../core/services/http.service';
import { StripeService } from '../../../core/services/stripe.service';
import { CheckoutService } from '../../services/checkout.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { CartService } from '../../../core/services/cart.service';
import { SimpleMultiStepComponent } from '../../../core/components/partials/simple-multi-step/simple-multi-step.component';
import { CountriesService } from '../../../core/services/countries.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { CardService } from '../../../core/services/card.service';
import { GTMService } from '../../../core/services/gtm.service';
import { DialogService } from '../../../core/services/dialog.service';

// import view child
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { SummaryPlaceOrderComponent } from './summary-place-order/summary-place-order.component';
import { RouterService } from '../../../core/helpers/router.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent extends SimpleMultiStepComponent implements OnInit {

  @ViewChild(SignInComponent)
  private signInComponent: SignInComponent;
  @ViewChild(DeliveryAddressComponent)
  private deliveryAddressComponent: DeliveryAddressComponent
  @ViewChild(PaymentMethodComponent)
  private paymentMethodComponent: PaymentMethodComponent
  @ViewChild(SummaryPlaceOrderComponent)
  private summaryPlaceOrderComponent: SummaryPlaceOrderComponent

  @ViewChild('account') account: ElementRef;
  @ViewChild('address') address: ElementRef;
  @ViewChild('payment') payment: ElementRef;
  @ViewChild('review') review: ElementRef;

  element: any;
  Stripe: any;
  currentCountry: any;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  cardForm: FormGroup;
  promoCodeForm: FormGroup;
  email: AbstractControl;
  stepsData: any;
  // currentStep: string = 'login';
  public currentUser;
  public isLoading: boolean = true;

  public workflowStatus: any;
  public activeStepStatus: any;
  resError: any;
  public i18nDialog: any;
  public receipt: any;
  public cartRules: any;
  public cartItems: any;
  checkPromoCodeApplied: boolean = false;
  promoCode: string;
  isCustomPlanPresent: any;
  public hasCustomPlan: boolean = false;
  promoCodeError: any;
  public cart_promoCode_param: any;
  public cart_promoCode: any;

  constructor(stripeService: StripeService,
    private builder: FormBuilder,
    private el: ElementRef,
    private checkoutService: CheckoutService,
    public storage: StorageService,
    private usersService: UsersService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private httpService: HttpService,
    private router: Router,
    public appConfig: AppConfig,
    public cartService: CartService,
    private countriesService: CountriesService,
    private routerService: RouterService,
    private deliveryAddressService: DeliveryAddressService,
    private cardService: CardService,
    private gtmService: GTMService,
    private dialogService: DialogService,
    private location: Location
  ) {
    super();
    this.cardForm = builder.group({
      cardNumber: ['', Validators.required],
      expiryMonth: ['', Validators.required],
      expiryYear: ['', Validators.required],
      cvc: ['', Validators.required]
    });

    this.promoCodeForm = builder.group({
      promoCode: ['', Validators.required],
    });

    this.Stripe = stripeService.getInstance();
    this.element = $(el.nativeElement);

    this.cardForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });

    // Check if Custom Plan or Ala Carte Journey
    if (this.storage.getCustomPlanItem()) {
      this.isCustomPlanPresent = this.storage.getCustomPlanItem();
      this.isCustomPlanPresent = this.isCustomPlanPresent.customPlanDetail;
    } else {
      this.isCustomPlanPresent = '';
    }

    cartService.hasUpdate.subscribe(hasUpdate => {
      if (this.storage.getCustomPlanItem()) {
        this.cartItems = [];
        let customPlan = this.storage.getCustomPlanItem();
        customPlan.customPlanDetail.forEach(element => {
          this.cartItems.push({
            id: element.product.id,
            product: element.product,
            qty: element.qty
          });
        });
        this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
      } else {
        this.cartItems = this.cartService.getCart();
        this.receipt = this.cartService.getReceipt(this.cartRules);
      }

      let promoCode = this.cartService.getPromoCode();
      this.checkPromoCodeApplied = false;
      if (promoCode.id) {
        if (promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend) {
        } else if (this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
        } else {
          this.promoCode = promoCode.promotionCodes[0].code;
          this.checkPromoCodeApplied = true;
        }
      }
    });
    this.getCheckoutPreviousData();
    // check if is custom plan
    if (this.storage.getCustomPlanItem()) {
      this.hasCustomPlan = true;
    }
  }

  checkDelete(data) {

  }

  ngOnInit(): void {
    // set footer short
    this.routerService.setFooterFull(false);
    this.routerService.setHeaderCommon(false);

    this.resError = null;
    // check if cart empty then redirect to homePage
    this.checkCartEmpty();
    // initialize card info
    this.cardNumber = '';
    this.expiryMonth = '';
    this.expiryYear = '';
    this.cvc = '';

    this.setCurrentStep('login');
    this.setStepsData({
      login: { state: 'active', data: {}, step: 'signInComponent' },
      deliveryAddress: { state: 'init', data: {}, step: 'deliveryAddressComponent' },
      payment: { state: 'init', data: {}, step: 'paymentMethodComponent' },
      summary: { state: 'init', data: {}, step: 'summaryPlaceOrderComponent' },
      hasChangeAccount: { status: false },
    })
    // this.getNotifyStep1Status({});

    // step 1 => sign up
    this.currentUser = this.storage.getCurrentUserItem();
    // console.log(this.currentUser);
    if (this.currentUser) {
      this.stepsData.login.data = this.currentUser;
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
    }
    // handle country change event
    this.currentCountry = this.storage.getCountry();
    this.countriesService.country.subscribe(country => {
      this.checkCartEmpty();
      this.currentCountry = country;
    });

    // apply promo code to input field
    const cart_promoCode_param = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.cart_promoCode = cart_promoCode_param.get('promo');
    console.log(this.cart_promoCode);
    if (this.cart_promoCode) {
      this.promoCode = this.cart_promoCode;
      setTimeout(this.triggerPromoCheck, 3000);
    }

    // subscribe deliver address action
    // this.deliveryAddressService.deliveryAddressAction.subscribe(data => {
    //   if( data ) {
    //     // // console.log('deliveryAddressService', data)
    //     // action: edit
    //     if (data.action === 'edit') {
    //       // // console.log('deliveryAddressService', data)
    //     }
    //     // action: add
    //     if (data.action === 'add') {

    //     }
    //     // action: remove
    //     if (data.action === 'remove') {

    //     }
    //   }
    // })

    // subscribe card action
    this.cardService.cardAction.subscribe(data => {
      if (data) {
        // console.log('cardAction', data)
        // action: select change | remove
        if (data.action === 'reset') {
          if (!data.dataReset) {// console.log('data.dataReset 1', data.dataReset);
            this.stepsData.payment.data = {};
            this.stepsData.loadedReview = false;
          } else {
            // console.log('data.dataReset 2', data.dataReset);
          }
        }
        // action: add
        if (data.action === 'add') {

        }
      }
    })
    // $( window ).scroll(function() {
    //   if($( window ).width()<=767){
    //     if(53 - $(window).scrollTop() >=0)
    //       $('.progress-tabs').css("top",(55 - $(window).scrollTop())+"px");
    //     if($(window).scrollTop() >= 55)
    //       $('.progress-tabs').css("top","0px");
    //   }
    // });

    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        if (this.storage.getCustomPlanItem()) {
          this.cartItems = [];
          let customPlan = this.storage.getCustomPlanItem();
          customPlan.customPlanDetail.forEach(element => {
            this.cartItems.push({
              id: element.product.id,
              product: element.product,
              qty: element.qty
            });
          });
          this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
        } else {
          this.cartItems = this.cartService.getCart();
          this.receipt = this.cartService.getReceipt(this.cartRules);
        }

        let promoCode = this.cartService.getPromoCode();
        this.checkPromoCodeApplied = false;
        if (promoCode.id) {
          if (promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend) {
          } else if (this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
          } else {
            this.promoCode = promoCode.promotionCodes[0].code;
            this.checkPromoCodeApplied = true;
          }
        }

        this.isLoading = false;
      }
    )

    this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
      if (value === '') {
        this.cartService.setPromotion();
      }
    })
  }

  checkCartEmpty() {
    if (((!this.cartService.getCart() || this.cartService.getCart().length <= 0) && !this.storage.getFreeTrialItem() && !this.storage.getCustomPlanItem())) {
      this.router.navigate(['/']);
      this.routerService.setFooterFull(true);
      this.routerService.setHeaderCommon(true);
    } else {

    }
  }


  triggerPromoCheck() {
    let triggerPromoClick : HTMLElement = document.getElementById('applyPromoBtn_checkout') as HTMLElement;
    triggerPromoClick.click();
  }
  /**
   * step 1 => Sign in
   */
  getNotifyStep1Status(data) {
    if (data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.gtmService.checkout({ step: 1, options: data.email ? 'logged in' : 'guest' });
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }

  goToLogin() {
    if (Object.keys(this.stepsData.login.data).length === 0 || !this.storage.getCurrentUserItem()) {
      this.goto('login');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  hasChangeAccount(data) {
    if (data) {
      this.stepsData.login.data = {};
      this.paymentMethodComponent.selectedCard = null;
      this.stepsData.hasChangeAccount.status = true;
      $(this.account.nativeElement).trigger('click');
    }
  }

  /**
   * step 2 => delivery address
   */
  getNotifyStep2Status(data) {
    this.gtmService.checkout({ step: 2, options: this.stepsData.login.data.email ? 'logged in' : 'guest' });
    if (data.state === 'login') {
      this.stepsData.login.data.email = data.email;
      this.goto('login');
      window.scrollTo(0, 0);

      // console.log('this.stepsData', this.stepsData)
      this.resError = null;
    } else {
      // console.log(`data ==== ${JSON.stringify(data)}`);
      this.stepsData.deliveryAddress.data = data;
      if (Object.keys(this.stepsData.login.data).length == 0 && data.emailByGuest) {
        this.stepsData.login.data = { email: data.emailByGuest }
      }
      this.goto('payment');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  goToDeliveryAddress() {
    if (Object.keys(this.stepsData.login.data).length !== 0) {
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  /**
   * step 3 => payment method
   */
  getNotifyStep3Status(data) {
    this.stepsData.payment.data = data;
    this.stepsData.loadedReview = true;
    this.goto('summary');
    window.scrollTo(0, 0);
    this.summaryPlaceOrderComponent.resError = null;
    this.resError = null;
    this.gtmService.checkout({ step: 3, options: this.stepsData.payment.data.paymentMethod });
  }

  setResetCardSelected(data) {
    if (data) {
      this.stepsData.payment.data = data;
    } else {
      this.stepsData.payment.data = {};
    }
  }

  goToPayment() {
    if (this.deliveryAddressComponent.getSelectedAddress() && Object.keys(this.stepsData.login.data).length !== 0) {
      this.deliveryAddressComponent.goToStep3();
    }
  }

  goToSummary() {
    if (this.paymentMethodComponent.paymentMethod === 'ipay88' ||
      (this.paymentMethodComponent.paymentMethod === 'stripe' && this.paymentMethodComponent.selectedCard)) {
      this.paymentMethodComponent.gotoConfirmStep();
      this.goto('summary');
      window.scrollTo(0, 0);
    }
  }

  /**
   * step 4 => summary method
   */
  getNotifyStep4Status(data) {
    this.loadingService.display1(true);
    // paynow for free trial
    if (this.storage.getFreeTrialItem()) {
      // console.log('this.storage.getFreeTrialItem() --- ', this.storage.getFreeTrialItem())

      let currentUser = this.storage.getCurrentUserItem();
      if (currentUser) {
        this.cartService.getSubscriptionsByUser(currentUser).then(
          (data: any) => {
            currentUser.subscriptions = data;
            this.storage.setCurrentUser(currentUser);
            if (data && data.length > 0) {
              let checkIsTrialSub: boolean = false;
              data.forEach(item => {
                if (item.isTrial && item.status !== "Canceled") {
                  checkIsTrialSub = true;
                }
              });
              // console.log('checkIsTrialSub --- ', checkIsTrialSub);
              // if user has exists subs
              if (checkIsTrialSub) {
                this.loadingService.display1(false);
                this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
                  if (result) {
                    //
                  }
                }));
              } else {
                this.handlePaynowWithTrial();
              }
            } else {
              this.handlePaynowWithTrial();
            }
          },
          error => {
            // console.log(error.error)
          }
        )

      } else {
        let _email = this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest;
        this.cartService.getSubscriptionsByGuest(_email).then(
          (data: any) => {
            let checkIsTrialSub: boolean = false;
            if (data && data.user && data.user.subscriptions && data.user.subscriptions.length > 0) {
              data.user.subscriptions.forEach(item => {
                if (item.isTrial) {
                  checkIsTrialSub = true;
                }
              });
              if (checkIsTrialSub) {
                this.loadingService.display1(false);
                this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
                  if (result) {
                    //
                  }
                }));
              } else {
                this.handlePaynowWithTrial();
              }
            } else {
              this.handlePaynowWithTrial();
            }
          },
          (error: any) => {
            // console.log(error.error)
          }

        )
      }
    } else if (this.storage.getCustomPlanItem()) {
      // console.log('this.storage.getCustomPlanItem() --- ', this.storage.getCustomPlanItem())

      let currentUser = this.storage.getCurrentUser();
      if (currentUser) {
        this.cartService.getSubscriptionsByUser(currentUser).then(
          (data: any) => {
            currentUser.subscriptions = data;
            this.storage.setCurrentUser(currentUser);
            this.handlePaynowWithCustomPlan();
            // if (data && data.length > 0) {
            //     let checkIsTrialSub : boolean = false;
            //     data.forEach(item => {
            //       if (item.isTrial && item.status !== "Canceled") {
            //         checkIsTrialSub = true;
            //       }
            //     });
            //     // console.log('checkIsTrialSub --- ', checkIsTrialSub);
            //     // if user has exists subs
            //     if (checkIsTrialSub) {
            //       this.loadingService.display(false);
            //       this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
            //         if (result) {
            //           //
            //         }
            //       }));
            //     } else {
            //       this.handlePaynowWithCustomPlan();
            //     }
            //   } else {
            //     this.handlePaynowWithCustomPlan();
            //   }
          },
          error => {
            // console.log(error.error)
          }
        )

      } else {
        this.loadingService.display1(false);
      }
    } else {
      // paynow for not free trial
      let order;
      this.stepsData.summary.valid = true;
      this.stepsData.summary.data = data;

      // submit data
      // build data to create order
      let orderData;
      let promoCode = '';
      if (this.cartService.getPromoCode().id) {
        promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
      }

      if (this.stepsData.payment.data.paymentMethod === 'stripe') {
        orderData = {
          CardId: this.stepsData.payment.data.card.id,
          DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
          BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
          promotionCode: promoCode,
          userTypeId: 1,
          alaItems: data.alaItems,
          subsItems: data.subsItems,
          email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
          // shippingFee: this.receipt.shippingFee
        }
      } else {
        orderData = {
          DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
          BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
          promotionCode: promoCode,
          userTypeId: 1,
          alaItems: data.alaItems,
          email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
          // shippingFee: this.receipt.shippingFee
        }
      }

      // console.log('orderData',orderData);
      this.createOrder(orderData)
        .then(result => {
          // submit to GTM
          this.gtmService.purchaseCart(result['result']);

          if (!this.storage.getCurrentUserItem()) {
            localStorage.setItem('orderId', order.id);
          }
          // reset promo code
          this.cartService.setPromotion();
          this.cartService.reloadSubscriptions();
          if (this.stepsData.payment.data.paymentMethod === 'stripe') {
            order = result['result']['order'];
            // clear cart
            this.cartService.clearCart();

            // clear card, address by guest
            this.storage.clearCardByGuest();
            this.storage.clearDeliveryAddressByGuest();
            // clear products removed
            this.cartService.clearItemRemoved();
            // set gtm transaction id
            this.storage.setGtmTransactionID(order.id);

            // redirect to orders page
            this.router.navigate([`/thankyou/${order.id}`]);

            this.loadingService.display1(false);
          } else {
            order = result['result']['order'];
            // set gtm transaction id
            this.storage.setGtmTransactionID(order.id);

            this.router.navigate([`ipay88/submit/${order.id}`]);
            return;
          }
        })
        .catch(error => {
          this.loadingService.display1(false);
          // console.log(`payment error ${error}`);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
          this.summaryPlaceOrderComponent.resError = this.resError;
        });
    }

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getCheckoutPreviousData(){
    let check = this.storage.getCheckoutData();
    let past_transaction = this.storage.getGtmTransactionID();

    if(check){
      console.log("================================= getCheckoutData is NOT EMPTY =============================");
      console.log('Checkout Data : '+check);
      console.log('Past Transaction : '+past_transaction);
      this.router.navigate([`/`]);     
    }else{
      console.log("================================= getCheckoutData is EMPTY =================================");
      let test_variable = 'teststorage';
      let current_date = new Date();
      let checkoutItems = {"Test": test_variable,"Date" : current_date};
      this.storage.setCheckoutData(checkoutItems);
    }
  }

  handlePaynowWithTrial() {
    let order;
    let _orderData = {};
    let _trial_item = this.storage.getFreeTrialItem();
    _orderData = {
      userTypeId: 1,
      planCountryId: _trial_item.plan.planCountry[0].id,
      email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
      CardId: this.stepsData.payment.data.card.id,
      DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
      BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
      isWeb: true
    }

    this.createFreeTrialOrder(_orderData)
      .then(result => {
        // console.log(`payment trial result ---  ${result}`);
        // submit to GTM
        this.gtmService.purchaseCart(result['result']);

        this.cartService.reloadSubscriptions();
        if (this.stepsData.payment.data.paymentMethod === 'stripe') {

          order = result['result']['order'];
          if (!this.storage.getCurrentUserItem()) {
            localStorage.setItem('orderId', order.id);
          }

          // clear free trial storage
          this.storage.clearFreeTrialItem();

          // clear card, address by guest
          this.storage.clearCardByGuest();
          this.storage.clearDeliveryAddressByGuest();
          // set gtm transaction id
          this.storage.setGtmTransactionID(order.id);

          // redirect to orders page
          this.router.navigate([`/thankyou/${order.id}`]);

          this.loadingService.display(false);
        }
      })
      .catch(error => {
        this.loadingService.display(false);
        // console.log(`payment trial error ${error}`);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.summaryPlaceOrderComponent.resError = this.resError;
      });
  }

  handlePaynowWithCustomPlan() {
    let order;
    let _orderData;
    let _custom_item = this.storage.getCustomPlanItem();
    let promoCode = '';
    if (this.cartService.getPromoCode().id) {
      promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
    }
    _orderData = {
      userTypeId: 1,
      PlanTypeId: _custom_item.PlanTypeId,
      promotionCode: promoCode,
      email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
      CardId: this.stepsData.payment.data.card.id,
      DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
      BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
      isWeb: true,
      customPlanDetail: [],
      // shippingFee: this.receipt.shippingFee
    }
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    _custom_item.customPlanDetail.forEach(element => {
      _orderData.customPlanDetail.push({
        qty: element.qty,
        ProductCountryId: element.product.productCountry.id,
        productName: element.product.productTranslate.find(value => value.langCode.toUpperCase() === currentLang) ? element.product.productTranslate.find(value => value.langCode.toUpperCase() === currentLang).name : element.product.productTranslate[0].name,
        duration: element.duration
      });
    });

    this.createCustomPlanOrder(_orderData)
      .then(result => {
        // console.log(`payment custom plan result ---  ${result}`);
        // submit to GTM
        this.gtmService.purchaseCart(result['result']);

        this.cartService.reloadSubscriptions();
        if (this.stepsData.payment.data.paymentMethod === 'stripe') {

          order = result['result']['order'];
          if (!this.storage.getCurrentUserItem()) {
            localStorage.setItem('orderId', order.id);
          }

          // clear custom plan storage
          this.storage.clearCustomPlanItem();

          // clear card, address by guest
          this.storage.clearCardByGuest();
          this.storage.clearDeliveryAddressByGuest();
          // set gtm transaction id
          this.storage.setGtmTransactionID(order.id);

          // redirect to orders page
          this.router.navigate([`/thankyou/${order.id}`]);

          this.loadingService.display(false);
        }
      })
      .catch(error => {
        this.loadingService.display(false);
        // console.log(`payment trial error ${error}`);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.summaryPlaceOrderComponent.resError = this.resError;
      });
  }

  createOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if (utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if (this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.summaryPlaceOrderComponent.resError = this.resError;
              reject(error);
            }
          );
      } else {
        this.httpService._create(`${this.appConfig.config.api.place_order}/ipay88`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.summaryPlaceOrderComponent.resError = this.resError;
              reject(error);
            }
          );
      }
    });
  }

  createFreeTrialOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if (utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if (this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order_trial}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.summaryPlaceOrderComponent.resError = this.resError;
              reject(error);
            }
          );
      }
    });
  }

  createCustomPlanOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if (utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if (this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order_custom_plan}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.summaryPlaceOrderComponent.resError = this.resError;
              reject(error);
            }
          );
      }
    });
  }

  getNotifyJustLogin(data) {
    if (data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }

  // checkoutStripe(data) {
  //   return new Promise((resolved, reject) => {
  //     this.httpService._create(this.appConfig.config.api.stripe_checkout, data)
  //       .subscribe(
  //         res => {
  //           // console.log(`checkoput stripe ${JSON.stringify(res)}`);
  //           resolved(res);
  //         },
  //         error => {
  //           this.resError = {
  //             type: 'error',
  //             message: JSON.parse(error).message
  //           };
  //           this.summaryPlaceOrderComponent.resError = this.resError;
  //           reject(error);
  //         }
  //       );
  //   });
  // }

  applyPromoCode() {
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    let planCountryIds = [];
    this.cartItems.forEach(cartItem => {
      if (cartItem.product) {
        productCountryIds.push(cartItem.product.productCountry.id);
      }
      if (cartItem.plan) {
        if (!cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry.id);
        }
        if (cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry[0].id);
        }
      }
    })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: this.promoCode,
      getCustomPlan: this.isCustomPlanPresent,
      productCountryIds,
      planCountryIds,
      userTypeId: '1'
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);

        this.translate.get('msg').subscribe(res => {
          this.i18nDialog = res;
        });
        if (data.minSpend && this.receipt.totalPrice < data.minSpend) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + data.minSpend)
          };
        }

        if (this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && data.isFreeShipping) {
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_not_apply
          };
        }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        // console.log(error)
      }
    )
  }
}
