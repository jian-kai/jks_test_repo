import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { HttpService } from '../../../../core/services/http.service';
import { StorageService } from '../../../../core/services/storage.service';
import { GlobalService } from '../../../../core/services/global.service';
import { UsersService } from '../../../../auth/services/users.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { SimpleSubStepComponent } from '../../../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../../../core/animations/slide-in-out-checkout.animation';
import { CountriesService } from '../../../../core/services/countries.service';
import { CartService } from '../../../../core/services/cart.service';
import { numberValidator } from '../../../../core/directives/number-validator.directive';
import { NotificationService } from '../../../../core/services/notification.service';
import { Location } from '@angular/common';
@Component({
  selector: 'checkout-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  animations: [slideInOutCheckoutAnimation]
})
export class SignInComponent extends SimpleSubStepComponent implements OnInit {
  // @Input() loginForm: FormGroup;
  // @Input() isComfirmPassword: boolean;
  @ViewChild('nativeForm') nativeForm: ElementRef;
  @ViewChild('nativeSignupForm') nativeSignupForm: ElementRef;
  @ViewChild('signinRadio') signinRadio: ElementRef;
  @Output() notifyStep1Status: EventEmitter<any> = new EventEmitter<boolean>();
  @Input() stepsData: any;
  resError: any;
  fbResError: any;
  allValid: boolean = false;
  stage: string; // signin | password | signup | sendEmail | emailActive
  userName: string;
  activeEmail: string;
  public isComfirmPassword: boolean = false;
  public isLoading: boolean = true;
  public loginForm: FormGroup;
  public passwordForm: FormGroup;
  public signUpForm: FormGroup;
  public forgotPasswordForm: FormGroup;
  public hasSubscription: boolean = false;
  public promoCode_param: any;
  public promoCode: any;

  constructor(
    el: ElementRef,
    public builder: FormBuilder,
    public storage: StorageService,
    public usersService: UsersService,
    public alertService: AlertService,
    public loadingService: LoadingService,
    public translate: TranslateService,
    public router: Router,
    public _http: HttpService,
    public appConfig: AppConfig,
    public countriesService: CountriesService,
    public cartService: CartService,
    public notificationService: NotificationService,
    public globalService: GlobalService,
    private location: Location
  ) {
    super(el);
    this.loginForm = builder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.passwordForm = builder.group({
      password: ['', Validators.required]
    });
    this.signUpForm = builder.group({
      email: ['', Validators.required],
      // firstName: ['', Validators.required],
      // lastName: ['', Validators.required],
      password: ['', Validators.required],
      // confirmPassword: ['', Validators.required],
      // day: ['', numberValidator({ min: 1, max: 31 })],
      // month: ['', numberValidator({ min: 1, max: 12 })],
      // year: ['', numberValidator({ min: new Date().getFullYear() - 100, max: new Date().getFullYear() })],
      // gender: ['', Validators.required],
      hasReceiveOffer: [true]
    });
    this.forgotPasswordForm = builder.group({
      email: ['', Validators.required],
    });
    this.element = $(el.nativeElement);

    this.loginForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.passwordForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.signUpForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    let cartItems = this.cartService.getCart();
    this.hasSubscription = false;
    if(cartItems) {
      cartItems.forEach(item => {
        if (item.plan) {
          this.hasSubscription = true;
        }
      });
    }

    cartService.hasUpdate.subscribe(hasUpdate => {
      let cartItems = this.cartService.getCart();
      this.hasSubscription = false;
      if(cartItems) {
        cartItems.forEach(item => {
          if (item.plan) {
            this.hasSubscription = true;
          }
        });
      }
    });

    this.usersService.resendActiveEmail.subscribe(email => {
      if (email && email !== '') {
        this.router.navigate(['/login'], { queryParams: {activeEmail: email}});
      }
    });
  }

  setActive() {
    // reset form
    this.loginForm.reset();
    this.passwordForm.reset();
    this.signUpForm.reset();
    // remove authen info
    this.storage.removeAuthToken();
    this.storage.removeCurrentUser();

    this.stage = 'signup';

    // reset form
    this.globalService.resetForm(this.nativeForm);
    if (this.signinRadio && this.signinRadio.nativeElement) {
      $(this.signinRadio.nativeElement).trigger('click')
    }

    if(this.stepsData.login.data.email) {
      this.loginForm.patchValue({email: this.stepsData.login.data.email});
    }
  }

  validate() {
    if (this.usersService._loggedIn) {
      return {
        allValid: true
      }
    } else {
      return {
        allValid: this.allValid
      }
    }
  }

  ngOnInit() {
    this.stage = 'signup'
    const promoCode_param = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.promoCode = promoCode_param.get('promo');
  }

  watchingActiveStatus(email) {
    // email = email ? email : this.loginForm.value.email;
    // let watchingInterval = setInterval(() => {
    //   this._http._create(this.appConfig.config.api.checkEmail, { email })
    //     .subscribe(res => {
    //       if (res && res.user.isActive) {
    //         this.storage.setAuthToken(res.token);
    //         this.storage.setCurrentUser(res.user);
    //         this.usersService._loggedIn.next(true);
    //         this.allValid = true;
    //         this.stage = 'emailActive'
    //         this.resError = null;
    //         clearInterval(watchingInterval);
    //       }
    //     });
    // }, 10 * 1000);
    let watchingInterval = setInterval(() => {
      if(this.storage.getCurrentUserItem() && this.router.url === '/checkout') {
        clearInterval(watchingInterval);
        this.goToStep2();
      }
    }, 1000);
  }

  checkEmail() {
    this.loadingService.display(true);
    if (this.loginForm.valid) {
      // check email is exist or not
      this._http._create(this.appConfig.config.api.checkEmail, { email: this.loginForm.value.email })
        .subscribe(res => {
          if (!res || res.isExisted === false) {
            // change step to sign up
            this.stage = 'signup';
            this.loadingService.display(false);
            this.resError = null;
          } else if (res && res.user.isActive) {
            // change step to input password
            this.userName = res.user.firstName;
            this.stage = 'password';
            this.loadingService.display(false);
            this.resError = null;
          } else {
            this.loadingService.display(false);
            this.resError = null;
            // change step to send email
            this.watchingActiveStatus(null);
            this.stage = 'sendEmail';
            this.activeEmail = this.loginForm.value.email;
          }
        });
    }

    this.loadingService.display(false);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  doSignup() {
    this.loadingService.display(true);
    let credentials = this.signUpForm.value;
    credentials.email = this.signUpForm.value.email;
    credentials.isCheckout = true;
    credentials.defaultLanguage = this.translate.currentLang ? this.translate.currentLang : this.translate.defaultLang;
    credentials.countryCode = this.storage.getCountry().code;
    // if (credentials.day) {
    //   credentials.birthday = `${credentials.year}/${credentials.month}/${credentials.day}`;
    // }
    let currentUser = this.storage.getCurrentUser();
    // console.log('currentUser', currentUser);
    if (currentUser && currentUser.email === credentials.email) {
      this.loadingService.display(false);
      this.notifyStep1Status.emit(currentUser);
    } else {
      credentials.utmData = {
        source: this.storage.getGtmSource(),
        medium: this.storage.getGtmMedium(),
        campaign: this.storage.getGtmCampaign(),
        content: this.storage.getGtmContent(),
        term: this.storage.getGtmTerm()
      };
      this._http._create(this.appConfig.config.api.register, credentials)
      .subscribe(data => {
        this.loadingService.display(false);
        // change step to send email
        // this.watchingActiveStatus(null);
        // this.stage = 'sendEmail';
        // this.resError = null;

        // store user into local store
        this.storage.setAuthToken(data.token);
        this.storage.setCurrentUser(data.user);
        this.notifyStep1Status.emit(data.user);
        this.allValid = true;
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
    });
    }

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // handle confirm password to the database
  doLogin() {
    // console.log(this.stepsData);
    this.loadingService.display(true);
    let credentials = { email: this.loginForm.value.email, password: this.loginForm.value.password };
    this._http._create(this.appConfig.config.api.login, credentials)
      .subscribe(data => {
        if(data.token) {
          // store token into localstore
          this.storage.setAuthToken(data.token);
          this.storage.setCurrentUser(data.user);
          this.usersService.setLogedin();
          this.allValid = true;

          // console.log(this.stepsData);
          if ((this.stepsData.hasChangeAccount && !this.stepsData.hasChangeAccount.status)) {
            // add item into cart
            data.user.Cart.cartDetails.forEach(item => {
              this.cartService.addItem(item);
            });
            // sync cart item to server
            this.cartService.updateCart();
          }

          this.notifyStep1Status.emit(data.user);
          this.notificationService.addNotificationRemindUser(null);
        } else if(!data.ok && data.code === 'inactive') {
          this.notificationService.addNotificationRemindUser(this.loginForm.value.email);
          setTimeout(() => this.notificationService.addNotificationRemindUser(null), 30000);
        }
        this.loadingService.display(false);
        this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
      );

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  goToStep2(checkoutByGuest?: boolean) {
    if(checkoutByGuest) {
      this.storage.removeCurrentUser();
      this.storage.clearCardByGuest();
      this.storage.clearDeliveryAddressByGuest();
    }
    if(this.storage.getCurrentUserItem()) {
      this.notifyStep1Status.emit(this.storage.getCurrentUserItem());
    } else {
      this.notifyStep1Status.emit(
        {
          // email: this.loginForm.value['email']
        }
      );
    }
    this.allValid = true;
  }

  // send email again
  sendEmailAgain() {
    this.loadingService.display(true);
    // console.log('this.signUpForm',this.signUpForm);
    this._http._create(this.appConfig.config.api.resendActiveEmail, { email: this.activeEmail })
      .subscribe(res => {
        // TODO update UI
      });
    this.loadingService.display(false);
    this.watchingActiveStatus(null);

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  changeEmail() {
    this.stage = 'signin';
    this.passwordForm.reset();
  }

  // forgot password
  forgotPassword() {
    this.loadingService.display(true);
    let credentials = { email: this.forgotPasswordForm.value.email };
    if (credentials) {
      this._http._create(`${this.appConfig.config.api.reset_password}`, credentials).subscribe(
        data => {
          this.stage = 'forgotPassword_2';
          this.resError = null;
          // console.log('data', data);
          this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
      );
    }

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getFBLoginStatus() {
    return new Promise((resolve, reject) => {
      window['FB'].getLoginStatus(response => {
        resolve(response);
      });
    });
  }

  doLoginWithFB() {
    return new Promise((resolve, reject) => {
      window['FB'].login(function(response) {
        if (response.authResponse) {
          resolve(response);
        } else {
          reject(response);
        }
      }, {scope: 'email'});
    });
  }

  getFBUserInfo(res) {
    return new Promise((resole, reject) => {
      window['FB'].api(`/${res.authResponse.userID}`, { locale: 'en_US', fields: 'name, email, gender, birthday' }, function(response) {
        if(!response || response.error) {
          reject(response);
        } else {
          resole(response);
        }
      });
    });
  }

  facebookLogin() {
    this.resError = null;
    this.fbResError = null;
    this.getFBLoginStatus()
      .then(res => {
        if(res['status'] === 'connected' && res['email']) {
          return res;
        } else {
          return this.doLoginWithFB();
        }
      })
      .then(res => this.getFBUserInfo(res))
      .then(res => {
        let firstName = (res['name'].indexOf(' ') > -1) ? res['name'].split(' ')[0] : res['name'];
        let lastName = (res['name'].indexOf(' ') > -1) ? res['name'].split(' ')[1] : '';
        let user = {
          email: res['email'],
          firstName,
          lastName,
          socialId: res['id'],
          gender: res['gender'],
          birthday: res['birthday'],
          utmData: {
            source: this.storage.getGtmSource(),
            medium: this.storage.getGtmMedium(),
            campaign: this.storage.getGtmCampaign(),
            content: this.storage.getGtmContent(),
            term: this.storage.getGtmTerm()
          }
        }

        if(user.email && user.email != '') {
          // submit user object
          this._http._create(this.appConfig.config.api.signinFB, user).subscribe(
            data => {
              // store token into localstore
              this.storage.setAuthToken(data.token);
              this.storage.setCurrentUser(data.user);
              this.usersService.setLogedin();
              this.notifyStep1Status.emit(data.user);
              this.allValid = true;

              // add item into cart
              data.user.Cart.cartDetails.forEach(item => {
                this.cartService.addItem(item);
              });

              // sync cart item to server
              this.cartService.updateCart();

              this.loadingService.display(false);
              this.resError = null;
              this.fbResError = null;
            },
            error => {
              this.loadingService.display(false);
              // console.log(error)
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          );
        } else {
          this.loadingService.display(false);
          this.fbResError = {
            type: 'error',
            message: 'Your email is required to register an account with Shaves2U'
          };
        }
        // console.log(res);
      })
      .catch(err => {}// console.log(err));
      );
  }

  changeSignInUp(value) {
    // this.emailFormType = value;
    this.stage = value;
    this.resError = null;
    if (this.stage === 'signin') {
      this.loginForm = this.builder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      });
    }
    if (this.stage === 'signup') {
      this.signUpForm = this.builder.group({
        email: ['', Validators.required],
        // firstName: ['', Validators.required],
        // lastName: ['', Validators.required],
        password: ['', Validators.required],
        // confirmPassword: ['', Validators.required],
        hasReceiveOffer: [true]
      });
    }
  }

  referForgotPassword() {
    this.stage = 'forgotPassword_1';
    this.resError = null;
  }

  referSignIn() {
    this.stage = 'signin';
    this.resError = null;
    this.loginForm.reset();
  }

}
