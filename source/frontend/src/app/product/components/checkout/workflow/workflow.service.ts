import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class WorkflowService {
  public workflowObj = [
    { step: 'sign_in', valid: false },
    { step: 'delivery_address', valid: false },
    { step: 'payment_method', valid: false },
    { step: 'summary_place_order', valid: false }
  ];
  public workflow = new BehaviorSubject(this.workflowObj);
  public activeStepObj = [
    { step: 'sign_in', valid: false },
    { step: 'delivery_address', valid: false },
    { step: 'payment_method', valid: false },
    { step: 'summary_place_order', valid: false }
  ];
  public activeStep = new BehaviorSubject(this.activeStepObj);
  constructor() { 
  }
  public setStep(step: string, valid: boolean) {
    // If the state is found, set the valid field to true 
    for (var i = 0; i < this.workflowObj.length; i++) {
        if (this.workflowObj[i].step === step) {
          this.workflowObj[i].valid = valid;
        }
    }
    this.workflow.next(this.workflowObj);
  }
  public setActiveStep(step: string, valid: boolean) {
    // If the state is found, set the valid field to true 
    for (var i = 0; i < this.activeStepObj.length; i++) {
        if (this.activeStepObj[i].step === step) {
          this.activeStepObj[i].valid = valid;
        }
    }
    this.activeStep.next(this.activeStepObj);
  }

}
