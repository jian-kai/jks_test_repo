import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPlaceOrderComponent } from './summary-place-order.component';

describe('SummaryPlaceOrderComponent', () => {
  let component: SummaryPlaceOrderComponent;
  let fixture: ComponentFixture<SummaryPlaceOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPlaceOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPlaceOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
