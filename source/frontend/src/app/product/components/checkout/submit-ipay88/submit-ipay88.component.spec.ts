import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitIpay88Component } from './submit-ipay88.component';

describe('SubmitIpay88Component', () => {
  let component: SubmitIpay88Component;
  let fixture: ComponentFixture<SubmitIpay88Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitIpay88Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitIpay88Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
