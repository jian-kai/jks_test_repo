import { Component, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from 'app/config/app.config';

@Component({
  selector: 'app-submit-ipay88',
  templateUrl: './submit-ipay88.component.html',
  styleUrls: ['./submit-ipay88.component.scss']
})
export class SubmitIpay88Component implements AfterViewInit {
  orderId: string;
  element: any;
  constructor(private route: ActivatedRoute,
    private el: ElementRef,
    public appConfig: AppConfig) {
      this.element = this.el.nativeElement;
    }

  ngAfterViewInit() {
    this.route.params.subscribe(params => {
      this.orderId = params['orderId'];
      let form = this.el.nativeElement.querySelector('form');
      form.querySelector('input').value = this.orderId;
      form.submit();
    });
  }

}
