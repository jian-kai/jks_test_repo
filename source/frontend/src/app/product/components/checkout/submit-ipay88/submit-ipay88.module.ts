import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { SubmitIpay88Component } from './submit-ipay88.component';

// routes
export const ROUTES: Routes = [
  { path: '', component: SubmitIpay88Component }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [
    SubmitIpay88Component,
  ]
})
export class SubmitIpay88Module { }
