import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// component
import { ThankyouComponent } from './thankyou.component';

// Modules
import { SharedModule } from './../../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: ThankyouComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    ThankyouComponent,
  ]
})
export class ThankyouModule { }
