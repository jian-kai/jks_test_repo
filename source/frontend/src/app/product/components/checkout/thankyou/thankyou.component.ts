import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { CartService} from '../../../../core/services/cart.service';
import { CountriesService } from '../../../../core/services/countries.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { GTMService } from '../../../../core/services/gtm.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {
  orderId: string;
  isloading: boolean;
  orderInfo: any;
  public currentUser: any;
  orderIdGuest: string;
  public isLoading: boolean = true;
  public deliveryDateFrom: any;
  public deliveryDateTo: any;
  constructor(private route: ActivatedRoute,
    private httpService: HttpService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private router: Router,
    public cartService: CartService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    private location: Location,
    private gtmService: GTMService
  ) { }

  ngOnInit() {
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    })
    this.storage.clearCheckoutData();
  }

  reloadPage() {

    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    let ipay88 = params.get('ipay88');
    if(ipay88) {
      this.cartService.clearCart();
    }

    this.loadingService.display(true);
    this.currentUser = this.storage.getCurrentUserItem();
    this.orderIdGuest = localStorage.getItem('orderId');

    // reset cart items
    this.cartService.setPromotion();
    if(!this.currentUser && !this.orderIdGuest) {
      this.loadingService.display(false);
      this.router.navigate(['/']);
    } else {
      this.route.params.subscribe(params => {
        this.orderId = params['orderId'];
        this.getOrderDetail();
      });
    }
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getOrderDetail() {
    this.isloading = true;
    this.deliveryDateFrom = new Date();
    this.deliveryDateTo = new Date(new Date().setDate(new Date().getDate() + 6));
    this.loadingService.display(true);
    if(this.currentUser) {
      this.httpService._getDetail(`${this.appConfig.config.api.order}/${this.orderId}`.replace('[userID]', this.currentUser.id)).subscribe(
        data => {
          if(data && Object.keys(data).length > 0) {
            this.orderInfo = data;
            // console.log(' this.orderInfo', this.orderInfo);

            // Check if is trial
            if (this.orderInfo) {
              this.orderInfo.orderDetail.forEach(order => {
                if (order.PlanCountry && order.PlanCountry.Plan && order.PlanCountry.Plan.isTrial) {
                  this.orderInfo.isTrial = true;
                }
              });
            }

            // submit to GTM
            if (this.storage.getGtmTransactionID() && this.orderInfo.id == this.storage.getGtmTransactionID()) {
              this.gtmService.transactionData(this.orderInfo);
            }

            if(this.storage.getCountry().code.toUpperCase() === 'KOR') {
              this.httpService._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
                data => {
                  if (data) {
                    this.deliveryDateFrom = new Date(data);
                    this.deliveryDateTo = new Date(new Date(data).setDate(new Date(data).getDate() + 6));
                  }
                  this.loadingService.display(false);
                },
                error => {
                  this.loadingService.display(false);
                }
              )
            } else {
              this.loadingService.display(false);
            }
          } else {
            this.loadingService.display(false);
            this.router.navigate(['/']);
          }
        },
        error => {
          this.loadingService.display(false);
          // console.log(error.error);
        }
      )
    } else {
      this.httpService._getDetail(`${this.appConfig.config.api.users}/orders/${this.orderIdGuest}`).subscribe(
        data => {
          if(data && Object.keys(data).length > 0) {
            this.orderInfo = data;

            // console.log(' this.orderInfo', this.orderInfo);

            // Check if is trial
            if (this.orderInfo) {
              this.orderInfo.orderDetail.forEach(order => {
                if (order.PlanCountry && order.PlanCountry.Plan && order.PlanCountry.Plan.isTrial) {
                  this.orderInfo.isTrial = true;
                }
              });
            }

            // submit to GTM
            if (this.storage.getGtmTransactionID() && this.orderInfo.id == this.storage.getGtmTransactionID()) {
              this.gtmService.transactionData(this.orderInfo);
            }
            // this.router.navigate([`/thankyou/${this.orderIdGuest}`]);
            if(this.storage.getCountry().code.toUpperCase() === 'KOR') {
              this.httpService._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
                data => {
                  if (data) {
                    this.deliveryDateFrom = new Date(data);
                    this.deliveryDateTo = new Date(new Date(data).setDate(new Date(data).getDate() + 6));
                  }
                  this.loadingService.display(false);
                },
                error => {
                  this.loadingService.display(false);
                }
              )
            } else {
              this.loadingService.display(false);
            }
          } else {
            this.loadingService.display(false);
            this.router.navigate(['/']);
          }
        },
        error => {
          this.loadingService.display(false);
          // console.log(error.error);
        }
      )
    }
  }

  trackOrder(event) {
    event.preventDefault();
    // redirect to orders detail page
    this.router.navigate([`/user/orders/${this.orderInfo.id}`]);
  }

  completeSignup() {
    this.router.navigate(['/login'], { queryParams: {email: this.orderInfo.email} });
  }
  
  goToHelp() {
    this.router.navigate(['/help'], { queryParams: {id: 3}});
  }
}
