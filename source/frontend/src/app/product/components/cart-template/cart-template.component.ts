import { Component, OnInit, Input, Injectable, AfterViewInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { CartService } from '../../../core/services/cart.service';
import { Item } from '../../../core/models/item.model';
import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../core/services/global.service';
import { HttpService } from '../../../core/services/http.service';
import { CountriesService } from '../../../core/services/countries.service';
import { Router } from '@angular/router';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';
import { UsersService } from '../../../auth/services/users.service';
declare var jQuery: any;

@Component({
  selector: 'app-cart-template',
  templateUrl: './cart-template.component.html',
  styleUrls: ['./cart-template.component.scss']
})
export class CartTemplateComponent implements OnInit {
  productList = [];
  nativeWindow: any;
  receipt = {
    totalPrice: 0,
    totalPriceNotDivided: 0,
    specialDiscount: 0,
    discount: 0,
    subTotal: 0,
    shippingFee: 0,
    totalIclTax: 0,
    applyDiscount: 0
  };
  promoCode: string;
  checkPromoCodeApplied: boolean = false;
  promoCodeError: any;
  notAllowPromoCode: boolean = false;
  private apiURL: string;
  @Input() cartItems: Item[];
  @Input() template: string = 'template-1';
  public cartItemsByLangCode: Item[];
  public isLoading: boolean = true;
  public item: Item;
  public planGroups: any = [];
  public activeGroup: any = {};
  public activePayment: any = {};
  public isLoadingAllPlans: boolean = true;
  public cartRules: any;
  public latestProductsRemoved: any;
  public promoCodeForm: FormGroup;
  public i18nDialog: any;
  public hasFreeTrial : boolean = false;
  public freeTrialItem: any;
  public hasCustomPlan: boolean = false;

  // public cartItems : Item[];
  constructor(public cartService: CartService,
    private globalService: GlobalService,
    private translate: TranslateService,
    public appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    public storage: StorageService,
    private router: Router,
    public planService: PlanService,
    private _dialogService: DialogService,
    private usersService: UsersService,
    private builder: FormBuilder,
  ) {
    this.apiURL = appConfig.config.apiURL;
    this.promoCodeForm = builder.group({
      promoCode: ['', Validators.required],
    });

    cartService.hasUpdate.subscribe(hasUpdate => {
      if(this.getReceiptForFreeTrial()) {
        return;
      }
      if(this.storage.getCustomPlanItem()) {
        this.cartItems = [];
        let customPlan = this.storage.getCustomPlanItem();
        customPlan.customPlanDetail.forEach(element => {
          this.cartItems.push({
            id: element.product.id,
            product: element.product,
            qty: element.qty
          });
        });
        this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
      } else {
        this.cartItems = this.cartService.getCart();
        this.receipt = this.cartService.getReceipt(this.cartRules);
      }
      
      this.notAllowPromoCode = false;
      if(this.receipt.specialDiscount) {
        this.notAllowPromoCode = true;
        this.promoCode = null;
      }
      
      let promoCode = this.cartService.getPromoCode();
      if(promoCode.id) {
        this.translate.get('msg').subscribe(res => {
          this.i18nDialog = res;
        });
        if ((promoCode.minSpend && this.receipt.applyDiscount < promoCode.minSpend && !promoCode.allowMinSpendForTotalAmount)
          || (promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend && promoCode.allowMinSpendForTotalAmount)) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + promoCode.minSpend)
            
          };
        } else if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_not_apply
          };
        } else {
          this.promoCodeError = null;
        }
      }
      
      // get latest products removed list
      this.latestProductsRemoved = this.storage.getLatestProductsRemoved();

      this.checkPromoCodeApplied = false;
      if(promoCode.id) {
        if(promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend) {
        } else if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
        } else {
          this.promoCode = promoCode.promotionCodes[0].code;
          this.checkPromoCodeApplied = true;
        }
      }
    });

    // check if is custom plan
    if (this.storage.getCustomPlanItem()) {
      this.hasCustomPlan = true;
    }
  }

  ngOnInit() {
    // if (this.storage.getFreeTrialItem()) {
    //   this.hasFreeTrial = true;
    //   this.freeTrialItem = this.storage.getFreeTrialItem();
    //   this.cartItems[0] = this.freeTrialItem;
    //   let currDate = new Date();
    //   this.cartItems[0]['first_delivery'] = currDate.setDate(currDate.getDate() + 20);
    //   let _receipt = {
    //     totalPrice: 0,
    //     totalPriceNotDivided: 0,
    //     specialDiscount: 0,
    //     discount: 0,
    //     subTotal: 0,
    //     shippingFee: 0,
    //     totalIclTax: 0,
    //     taxRate: 0,
    //     applyDiscount: 0,
    //     taxName: ''
    //   }
    //   let currentCountry = this.storage.getCountry();
    //   _receipt.taxRate = currentCountry.taxRate;
    //   _receipt.taxName = currentCountry.taxName;
    //   this.receipt = _receipt;
    //   this.receipt.shippingFee = this.appConfig.getConfig('shippingFeeConfig').trialFee;
    //   this.receipt.totalIclTax = this.receipt.totalPrice + this.receipt.shippingFee;
    //   return;
    // }
    if(this.getReceiptForFreeTrial()) {
      return;
    }
    this.usersService.justLogout.subscribe((value: boolean) => {
      if(!this.cartItems) {
        if(this.storage.getCustomPlanItem()) {
          this.cartItems = [];
          let customPlan = this.storage.getCustomPlanItem();
          customPlan.customPlanDetail.forEach(element => {
            this.cartItems.push({
              id: element.product.id,
              product: element.product,
              qty: element.qty
            });
          });
        } else {
          this.cartItems = this.cartService.getCart();
        }
        if(this.cartItems) {
          if(this.storage.getCustomPlanItem()) {
            this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
          } else {
            this.receipt = this.cartService.getReceipt(this.cartRules);
          }
    
          this.notAllowPromoCode = false;
          if(this.receipt.specialDiscount) {
            this.notAllowPromoCode = true;
            this.promoCode = null;
          }
        }
      }
    });

    this.isLoading = true;
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        if(this.storage.getCustomPlanItem()) {
          this.cartItems = [];
          let customPlan = this.storage.getCustomPlanItem();
          customPlan.customPlanDetail.forEach(element => {
            this.cartItems.push({
              id: element.product.id,
              product: element.product,
              qty: element.qty
            });
          });
          this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
        } else {
          this.cartItems = this.cartService.getCart();
          this.receipt = this.cartService.getReceipt(this.cartRules);
        }
        
        let promoCode = this.cartService.getPromoCode();
        this.checkPromoCodeApplied = false;
        if(promoCode.id) {
          if(promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend) {
          } else if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
          } else {
            this.promoCode = promoCode.promotionCodes[0].code;
            this.checkPromoCodeApplied = true;
          }
        }

        this.isLoading = false;
      }
    )
    
    this.notAllowPromoCode = false;
    if(this.receipt.specialDiscount) {
      this.notAllowPromoCode = true;
      this.promoCode = null;
    }

    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });

    this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
      if(value === '') {
        this.cartService.setPromotion();
      }
    })
    

  }

  reloadPage() {
    this.loadingService.display(true);
    // get Cart
    if(this.storage.getCustomPlanItem()) {
      this.cartItems = [];
      let customPlan = this.storage.getCustomPlanItem();
      customPlan.customPlanDetail.forEach(element => {
        this.cartItems.push({
          id: element.product.id,
          product: element.product,
          qty: element.qty
        });
      });
    } else {
      this.cartItems = this.cartService.getCart();
    }

    if (this.cartItems) {
      let hasPlanItem = false;
      this.cartItems.forEach(item => {
        if(item.plan) {
          hasPlanItem = true;
        }
      });

      if(hasPlanItem && this.planGroups.length < 1) {
        this.getAllPlans();
      }
    }

    // get product for Releated product
    this.getReleatedProduct();
    
    if(this.storage.getCustomPlanItem()) {
      this.cartItems = [];
      let customPlan = this.storage.getCustomPlanItem();
      customPlan.customPlanDetail.forEach(element => {
        this.cartItems.push({
          id: element.product.id,
          product: element.product,
          qty: element.qty
        });
      });
      this.receipt = this.cartService.getReceiptForCustomPlan(this.cartRules, this.cartItems);
    } else {
      this.cartItems = this.cartService.getCart();
      this.receipt = this.cartService.getReceipt(this.cartRules);
    }

    // get latest products removed list
    this.latestProductsRemoved = this.storage.getLatestProductsRemoved();

    this.loadingService.display(false);
  }

  getReceiptForFreeTrial() {
    if (this.storage.getFreeTrialItem()) {
      this.hasFreeTrial = true;
      this.freeTrialItem = this.storage.getFreeTrialItem();
      this.cartItems[0] = this.freeTrialItem;
      let currDate = new Date();
      this.cartItems[0]['first_delivery'] = currDate.setDate(currDate.getDate() + 20);
      let _receipt = {
        totalPrice: 0,
        totalPriceNotDivided: 0,
        specialDiscount: 0,
        discount: 0,
        subTotal: 0,
        shippingFee: 0,
        totalIclTax: 0,
        taxRate: 0,
        applyDiscount: 0,
        taxName: ''
      }
      let currentCountry = this.storage.getCountry();
      _receipt.taxRate = currentCountry.taxRate;
      _receipt.taxName = currentCountry.taxName;
      this.receipt = _receipt;
      this.receipt.shippingFee = this.appConfig.getConfig('shippingFeeConfig').trialFee;
      this.receipt.totalIclTax = this.receipt.totalPrice + this.receipt.shippingFee;
      return true;
    }
    return false;
  }

  // get all plans
  getAllPlans() {
    let cur = this.storage.getCurrentUserItem();
    this.isLoadingAllPlans =  true;
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
      data => {
        if (data) {
          this.planGroups = this.planService.groupPlan(data);
          // if (this.planGroups.length > 0) {
          //   this.activeGroup = this.planGroups[0];
          //   this.activePayment = this.activeGroup.item[0];
          // }
          // // console.log('this.planGroups', this.planGroups);
        }
        this.isLoadingAllPlans =  false;
      },
      error => {
        // console.log(error.error)
        this.isLoadingAllPlans =  false;
      }
    )
  }

  getReleatedProduct() {
    // get product for Releated product
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.productList = [];
        data.forEach((product, i) => {
          let index = null;
          if(this.cartItems) {
            this.cartItems.forEach(item => {
              if (product.id === item.id && item.product) {
                index = i;
              }
            });
          }
          if (index === null && this.productList.length < 3)
            this.productList.push(product);
        });

        // get all plans
        // this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
        //   data => {
        //     data.forEach((plan, i) => {
        //       let index = null;
        //       if(this.cartItems) {
        //         this.cartItems.forEach(item => {
        //           if (plan.id === item.id && item.plan) {
        //             index = i;
        //           }
        //         });
        //       }
        //       if (index === null && this.productList.length < 3)
        //         this.productList.push(plan);
        //     });
        //   },
        //   error => {
        //     // console.log(error.error)
        //     this.loadingService.display(false);
        //   }
        // )
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item, true)
      .then(card => this.cartItems = this.cartService.getCart());
    
    // if(this.cartService.getCart().length < 1) {
    //   this.router.navigate(['/products']);
    // }

    // get latest products removed list
    this.latestProductsRemoved = this.storage.getLatestProductsRemoved();
  }

  //handle click refresh item
  // refreshItem(item) {
  //   this.cartItems = this.cartService.refreshItem(item);
  // }

  //change quatity of the item
  changeQuatity(index) {
    if (this.cartItems[index].qty < 1) {
      this.cartItems[index].qty = 1;
    }
    if (this.cartItems[index].qty > 10000) {
      this.cartItems[index].qty = 10000;
    }
    // this.cartItems.forEach(element => {
      this.cartService.refreshItem(this.cartItems);
    // });
  }

  //minus quatity of the item
  minusQuatity(index) {
    if (this.cartItems[index].qty > 1) {
      this.cartItems[index].qty -= 1;
    }
    this.changeQuatity(index);
  }

  //plus quatity of the item
  plusQuatity(index) {
    if (this.cartItems[index].qty < 10000) {
      this.cartItems[index].qty += 1;
    }
    this.changeQuatity(index);
  }

  // pay
  payNow() {
    // console.log('todo...')
  }

  goProductDetail(product) {
    if(product.productCountry) {
      this.router.navigateByUrl(`/products/${product.slug}`);
    }
    if(product.planCountry) {
      this.router.navigateByUrl(`/shave-plans/${product.slug}`);
    }
  }
  
  // add item to cart
  addToCart(product: any, notSetItem?: boolean) {
    this.loadingService.display(true);
    let item:any;
    if(notSetItem) {
      item = product;
    } else {
      if(product.productCountry) {
        item = {
          id: product.id,
          product: product,
          qty: 1
        }
      }
      if(product.planCountry) {
        item = {
          id: product.id,
          plan: product,
          qty: 1,
        }
      }
    }
    
    this.cartService.addItem(item);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
    
    // get product for Releated product
    this.getReleatedProduct();

    this.loadingService.display(false);
  }

  applyPromoCode() {
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    let planCountryIds = [];
    this.cartItems.forEach(cartItem => {
      if(cartItem.product) {
        productCountryIds.push(cartItem.product.productCountry.id);
      }
      if(cartItem.plan) {
        if(!cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry.id);
        }
        if(cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry[0].id);
        }
      }
    })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: this.promoCode,
      productCountryIds,
      planCountryIds,
      userTypeId: '1'
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);

        this.translate.get('msg').subscribe(res => {
          this.i18nDialog = res;
        });
        if(data.minSpend && this.receipt.totalPrice < data.minSpend) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + data.minSpend)
          };
        }

        if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && data.isFreeShipping) {
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_not_apply
          };
        }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        // console.log(error)
      }
    )
  }

  newWindowDetail(_item) {
    if(_item.product) {
      window.open(`/products/${_item.product.slug}`);
    }
    if(_item.plan) {
      window.open(`/shave-plans/${_item.plan.slug}`);
    }
  }

  undoProductRemoved(_item) {
    this.cartService.undoItem(_item);
    // this.addToCart(this.latestProductsRemoved, true);
    if(_item.plan) {
      this.getAllPlans();
    }
    // this.latestProductsRemoved = this.storage.removeProductsRemoved(this.latestProductsRemoved);

    // // get latest products removed
    // this.latestProductsRemoved = this.storage.getLatestProductsRemoved();
  }

  goToHelp() {
    this.router.navigate(['/help'], { queryParams: {id: 3}});
  }

}
