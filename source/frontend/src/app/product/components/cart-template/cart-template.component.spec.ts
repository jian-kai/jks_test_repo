import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartTemplateComponent } from './cart-template.component';

describe('CartTemplateComponent', () => {
  let component: CartTemplateComponent;
  let fixture: ComponentFixture<CartTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
