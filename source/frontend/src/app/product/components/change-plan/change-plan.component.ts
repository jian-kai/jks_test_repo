import { Component, OnInit, Output, Input } from '@angular/core';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';
import { CartService } from '../../../core/services/cart.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'cart-change-plan',
  templateUrl: './change-plan.component.html',
  styleUrls: ['./change-plan.component.scss']
})
export class ChangePlanComponent implements OnInit {
  @Input() planGroups: any = [];
  @Input() item: any = [];
  public activeGroup: any = {};
  public activePayment: any = {};
  public isLoading: boolean = true;
  public resetPayment : boolean = false;
  public deliveryDate: any;
  public i18nDialog: any;

  constructor(public storage: StorageService,
    public planService: PlanService,
    private _dialogService: DialogService,
    private loadingService: LoadingService,
    public cartService: CartService,
    private translate: TranslateService,) { }

  ngOnInit() {
    this.activeGroup = this.planGroups.filter((group)=> group.name === this.item.groupName)[0];
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });
    this.deliveryDate = new Date();
    // this.deliveryDate.setMonth(this.deliveryDate.getMonth() + 3);
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
  }

  changeGroup(_group: any, _item: any) {
    // _group = JSON.parse(_group);
    if (_group.name === this.activeGroup.name) {
      return false;
    }
    let msgConfirm = (this.i18nDialog.confirm.change_plan).replace('{{from}}', this.activeGroup.name).replace('{{to}}', _group.name);
    this._dialogService.confirm(msgConfirm).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        // this.resetPayment = true;
        this.activeGroup = _group;
        this.activePayment = _group.item[0];
        let newItem = {
          id: this.activePayment.id,
          plan: this.activePayment,
          qty : 1,
          groupName: this.activeGroup.name
        }
        this.handleAddToCart(_item, newItem);
        setTimeout(() => {  
          this.loadingService.display(false);
        }, 100);
      }
    }));
  }

  changePayment(_paymentType: any, _item: any) {
    if (_paymentType == 0) {
      return false;
    }
    // _paymentType = JSON.parse(_paymentType);

    if (_paymentType.id === _item.id) { return false;}
    let msgConfirm = (this.i18nDialog.confirm.change_payment).replace('{{from}}', this.item.plan.PlanType.name).replace('{{to}}', _paymentType.PlanType.name);
    this._dialogService.confirm(msgConfirm).afterClosed().subscribe((result => {
      if (result) {
        // this.resetPayment = false;
        // this.removeItem(_item);

        this.loadingService.display(true);
        this.activePayment = _paymentType;

        let newItem = {
          id: this.activePayment.id,
          plan: this.activePayment,
          qty : 1,
          groupName: this.activeGroup.name
        }
        this.handleAddToCart(_item, newItem);
        
        setTimeout(() => {  
          this.loadingService.display(false);
        }, 100);
      }
    }));
  }

  handleAddToCart(oldItem: any, newItem: any) {
    this.removeItem(oldItem);
    this.cartService.addItem(newItem);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item);
    
    // if(this.cartService.getCart().length < 1) {
    //   this.router.navigate(['/products']);
    // }
  }

}
