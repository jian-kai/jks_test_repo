import { Component, OnInit, Input, Injectable, AfterViewInit, trigger, transition, style, animate, sequence } from '@angular/core';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { CartService } from '../../../core/services/cart.service';
import { Item } from '../../../core/models/item.model';
import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../core/services/global.service';
import { HttpService } from '../../../core/services/http.service';
import { CountriesService } from '../../../core/services/countries.service';
import { Router } from '@angular/router';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PlanService } from '../../../core/services/plan.service';
import { DialogService } from '../../../core/services/dialog.service';
import { UsersService } from '../../../auth/services/users.service';
import { ModalService } from '../../../core/services/modal.service';
declare var jQuery: any;
export const slideUndoCartAnimation =
  trigger('slideUndoCartAnimation', [
    transition('* => void', [
      style({ height: '*', opacity: '1', transform: 'translateX(0)' }),
      sequence([
        animate(".45s ease", style({ height: '*', opacity: '.2' })),
        animate(".2s ease", style({ height: '0', opacity: 0 }))
      ])
    ]),
    transition('void => true', [
      style({ height: '0', opacity: '0', transform: 'translateX(200px)', 'box-shadow': 'none' }),
      sequence([
        animate(".2s ease", style({ height: '*', opacity: '.2' })),
        animate(".55s ease", style({ height: '*', opacity: 1 }))
      ])
    ])
  ])

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  animations: [slideUndoCartAnimation]
})

export class CartComponent implements OnInit {
  productList = [];
  nativeWindow: any;
  receipt = {
    totalPrice: 0,
    totalPriceNotDivided: 0,
    specialDiscount: 0,
    discount: 0,
    subTotal: 0,
    shippingFee: 0,
    totalIclTax: 0,
    applyDiscount: 0
  };
  promoCode: string;
  checkPromoCodeApplied: boolean = false;
  promoCodeError: any;
  notAllowPromoCode: boolean = false;
  private apiURL: string;
  @Input() cartItems: Item[];
  @Input() template: string = 'template-1';
  public cartItemsByLangCode: Item[];
  public isLoading: boolean = true;
  public item: Item;
  public planGroups: any = [];
  public activeGroup: any = {};
  public activePayment: any = {};
  public isLoadingAllPlans: boolean = true;
  public cartRules: any;
  public latestProductsRemoved: any;
  private promoCodeForm: FormGroup;
  public i18nDialog: any;

  // public cartItems : Item[];
  constructor(public cartService: CartService,
    private globalService: GlobalService,
    private translate: TranslateService,
    public appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    public storage: StorageService,
    private router: Router,
    public planService: PlanService,
    public modalService: ModalService,
    private _dialogService: DialogService,
    private usersService: UsersService,
    private builder: FormBuilder,
  ) {
    if(!this.storage.getCountry().isWebEcommerce) {
      this.router.navigate(['/']);
    }

    this.apiURL = appConfig.config.apiURL;
    this.promoCodeForm = builder.group({
      promoCode: ['', Validators.required],
    });

    cartService.hasUpdate.subscribe(hasUpdate => {
      this.cartItems = this.cartService.getCart();
      this.receipt = this.cartService.getReceipt(this.cartRules);

      this.notAllowPromoCode = false;
      if (this.receipt.specialDiscount) {
        this.notAllowPromoCode = true;
        this.promoCode = null;
      }

      let promoCode = this.cartService.getPromoCode();
      if (promoCode.id) {
        this.translate.get('msg').subscribe(res => {
          this.i18nDialog = res;
        });
        if ((promoCode.minSpend && this.receipt.applyDiscount < promoCode.minSpend && !promoCode.allowMinSpendForTotalAmount)
          || (promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend && promoCode.allowMinSpendForTotalAmount)) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + promoCode.minSpend)

          };
        } else if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && promoCode.isFreeShipping) {
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_not_apply
          };
        } else {
          this.promoCodeError = null;
        }
      }

      // get latest products removed list
      this.latestProductsRemoved = this.storage.getLatestProductsRemoved();
      
      // clear promotion Code if Cart is empty
      if(this.cartService.getTotalQty() === 0) {
        this.promoCodeError = null;
        this.promoCode = '';
        this.cartService.setPromotion();
      }
    });

    this.checkPromoCodeApplied = false;
    if (this.cartService.getPromoCode().id) {
      this.promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
      this.checkPromoCodeApplied = true;
    }
  }

  ngOnInit() {
    this.usersService.justLogout.subscribe((value: boolean) => {
      if (!this.cartItems) {
        this.cartItems = this.cartService.getCart();
        if (this.cartItems) {
          this.receipt = this.cartService.getReceipt(this.cartRules);

          this.notAllowPromoCode = false;
          if (this.receipt.specialDiscount) {
            this.notAllowPromoCode = true;
            this.promoCode = null;
          }
        }
      }
    });

    this.isLoading = true;
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        this.cartItems = this.cartService.getCart();
        if(this.promoCode) {
          this.applyPromoCode();
        }
        this.receipt = this.cartService.getReceipt(this.cartRules);
        this.isLoading = false;
      }
    )

    this.notAllowPromoCode = false;
    if (this.receipt.specialDiscount) {
      this.notAllowPromoCode = true;
      this.promoCode = null;
    }

    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    });
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    });

    this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
      if (value === '') {
        this.cartService.setPromotion();
      }
    })


  }

  reloadPage() {
    this.loadingService.display(true);
    // get Cart
    this.cartItems = this.cartService.getCart();

    if (this.cartItems) {
      let hasPlanItem = false;
      this.cartItems.forEach(item => {
        if (item.plan) {
          hasPlanItem = true;
        }
      });

      if (hasPlanItem && this.planGroups.length < 1) {
        this.getAllPlans();
      }
    }

    // get product for Releated product
    this.getReleatedProduct();

    this.receipt = this.cartService.getReceipt(this.cartRules);

    // get latest products removed list
    this.latestProductsRemoved = this.storage.getLatestProductsRemoved();

    this.loadingService.display(false);
  }

  // get all plans
  getAllPlans() {
    let cur = this.storage.getCurrentUserItem();
    this.isLoadingAllPlans = true;
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
      data => {
        if (data) {
          this.planGroups = this.planService.groupPlan(data);
          // if (this.planGroups.length > 0) {
          //   this.activeGroup = this.planGroups[0];
          //   this.activePayment = this.activeGroup.item[0];
          // }
          // // console.log('this.planGroups', this.planGroups);
        }
        this.isLoadingAllPlans = false;
      },
      error => {
        // console.log(error.error)
        this.isLoadingAllPlans = false;
      }
    )
  }

  getReleatedProduct() {
    // get product for Releated product
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.productList = [];
        data.forEach((product, i) => {
          let index = null;
          if (this.cartItems) {
            this.cartItems.forEach(item => {
              if (product.id === item.id && item.product) {
                index = i;
              }
            });
          }
          if (index === null && this.productList.length < 3)
            this.productList.push(product);
        });

        // get all plans
        // this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
        //   data => {
        //     data.forEach((plan, i) => {
        //       let index = null;
        //       if(this.cartItems) {
        //         this.cartItems.forEach(item => {
        //           if (plan.id === item.id && item.plan) {
        //             index = i;
        //           }
        //         });
        //       }
        //       if (index === null && this.productList.length < 3)
        //         this.productList.push(plan);
        //     });
        //   },
        //   error => {
        //     // console.log(error.error)
        //     this.loadingService.display(false);
        //   }
        // )
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item, true)
      .then(card => this.cartItems = this.cartService.getCart());
    this.cartService.checkFreeProduct(this.cartRules);
    this.cartItems = this.cartService.getCart()

    if(this.promoCode) {
      this.applyPromoCode();
    }
    // if(this.cartService.getCart().length < 1) {
    //   this.router.navigate(['/products']);
    // }

    // get latest products removed list
    // this.latestProductsRemoved = this.storage.getLatestProductsRemoved();
  }

  //handle click refresh item
  // refreshItem(item) {
  //   this.cartItems = this.cartService.refreshItem(item);
  // }

  //change quatity of the item
  changeQuatity(index) {
    if (this.cartItems[index].qty < 1) {
      this.cartItems[index].qty = 1;
    }
    if (this.cartItems[index].qty > 10000) {
      this.cartItems[index].qty = 10000;
    }
    // this.cartItems.forEach(element => {
    this.cartService.refreshItem(this.cartItems);
    // });
  }

  //minus quatity of the item
  minusQuatity(index) {
    if (this.cartItems[index].qty > 1) {
      this.cartItems[index].qty -= 1;
    }
    this.changeQuatity(index);
  }

  //plus quatity of the item
  plusQuatity(index) {
    if (this.cartItems[index].qty < 10000) {
      this.cartItems[index].qty += 1;
    }
    this.changeQuatity(index);
  }

  // pay
  payNow() {
    // console.log('todo...')
  }

  goProductDetail(product) {
    if (product.productCountry) {
      this.router.navigateByUrl(`/products/${product.slug}`);
    }
    if (product.planCountry) {
      this.router.navigateByUrl(`/shave-plans/${product.slug}`);
    }
  }

  // add item to cart
  addToCart(product: any, notSetItem?: boolean) {
    this.loadingService.display(true);
    let item: any;
    if (notSetItem) {
      item = product;
    } else {
      if (product.productCountry) {
        item = {
          id: product.id,
          product: product,
          qty: 1
        }
      }
      if (product.planCountry) {
        item = {
          id: product.id,
          plan: product,
          qty: 1,
        }
      }
    }

    this.cartService.addItem(item);
    this.cartService.checkFreeProduct(this.cartRules);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();

    // get product for Releated product
    this.getReleatedProduct();
    this.cartItems = this.cartService.getCart();
    if(this.promoCode) {
        this.applyPromoCode();
    }
    this.loadingService.display(false);
  }

  applyPromoCode() {
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    let planCountryIds = [];
    this.cartItems.forEach(cartItem => {
      if (cartItem.product && !cartItem.isRemove) {
        productCountryIds.push(cartItem.product.productCountry.id);
      }
      if (cartItem.plan && !cartItem.isRemove) {
        if (!cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry.id);
        }
        if (cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry[0].id);
        }
      }
    })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: this.promoCode,
      productCountryIds,
      planCountryIds,
      userTypeId: '1'
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);

        this.translate.get('msg').subscribe(res => {
          this.i18nDialog = res;
        });
        if (data.minSpend && this.receipt.totalPrice < data.minSpend) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + data.minSpend)
          };
        }

        if (this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && data.isFreeShipping) {
          this.promoCodeError = {
            type: 'error',
            message: this.i18nDialog.validation.promocode_not_apply
          };
        }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        // console.log(error)
      }
      )
  }

  newWindowDetail(_item) {
    if (_item.product) {
      window.open(`/products/${_item.product.slug}`);
    }
    if (_item.plan) {
      window.open(`/shave-plans/${_item.plan.slug}`);
    }
  }

  undoProductRemoved(_item) {
    this.cartService.undoItem(_item);
    // this.addToCart(this.latestProductsRemoved, true);
    if (_item.plan) {
      this.getAllPlans();
    }
    this.cartService.checkFreeProduct(this.cartRules);
    
    this.cartItems = this.cartService.getCart();
    if(this.promoCode) {
      this.applyPromoCode();
    }
    // this.latestProductsRemoved = this.storage.removeProductsRemoved(this.latestProductsRemoved);

    // // get latest products removed
    // this.latestProductsRemoved = this.storage.getLatestProductsRemoved();
  }

  goToHelp() {
    this.router.navigate(['/help'], { queryParams: { id: 3 } });
  }

  trackByFn(index, item) {
    return index; // or item.id
  }

  // edit address
  addProductToCart(_item: any) {
    // if (_item) {
    //   _address.type = _addressType;
    // }
    // _address.enableSetDefault = true;
    // this.modalService.reset('add-to-cart-modal');
    this.modalService.open('add-to-cart-modal', _item.slug);
  }

  hasChangeCart(status: boolean) {
    this.loadingService.display(true);

    // get product for Releated product
    this.getReleatedProduct();
    this.cartItems = this.cartService.getCart();
    if(this.promoCode) {
        this.applyPromoCode();
    }
    this.loadingService.display(false);
  }

}
