import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { CartComponent } from './cart.component';
import { AddToCartModalComponent } from '../add-to-cart-modal/add-to-cart-modal.component';

// Modules
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: CartComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    CartComponent,
    AddToCartModalComponent,
  ]
})
export class CartModule { }
