import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// component
import { CheckoutFreeTrialComponent } from './checkout-free-trial.component';
import { DeliveryAddressTrialComponent } from './delivery-address-trial/delivery-address-trial.component';
import { PaymentMethodTrialComponent } from './payment-method-trial/payment-method-trial.component';
import { SignInTrialComponent } from './sign-in-trial/sign-in-trial.component';
import { SummaryPlaceOrderTrialComponent } from './summary-place-order-trial/summary-place-order-trial.component';
import { CartTemplateTrialComponent } from './cart-template-trial/cart-template-trial.component';

// module
import { SharedModule } from './../../../core/directives/share-modules';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
// routes
export const ROUTES: Routes = [
  { path: '', component: CheckoutFreeTrialComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ CheckoutFreeTrialComponent, DeliveryAddressTrialComponent,
    PaymentMethodTrialComponent, SignInTrialComponent, SummaryPlaceOrderTrialComponent, CartTemplateTrialComponent
  ]
})
export class CheckoutFreeTrialModule { }
