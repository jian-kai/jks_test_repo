import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutFreeTrialComponent } from './checkout-free-trial.component';

describe('CheckoutFreeTrialComponent', () => {
  let component: CheckoutFreeTrialComponent;
  let fixture: ComponentFixture<CheckoutFreeTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutFreeTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutFreeTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
