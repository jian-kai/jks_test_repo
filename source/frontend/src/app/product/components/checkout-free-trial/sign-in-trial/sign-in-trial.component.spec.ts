import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInTrialComponent } from './sign-in-trial.component';

describe('SignInTrialComponent', () => {
  let component: SignInTrialComponent;
  let fixture: ComponentFixture<SignInTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
