import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAddressTrialComponent } from './delivery-address-trial.component';

describe('DeliveryAddressTrialComponent', () => {
  let component: DeliveryAddressTrialComponent;
  let fixture: ComponentFixture<DeliveryAddressTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryAddressTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryAddressTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
