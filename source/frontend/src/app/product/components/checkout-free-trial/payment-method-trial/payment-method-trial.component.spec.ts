import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodTrialComponent } from './payment-method-trial.component';

describe('PaymentMethodTrialComponent', () => {
  let component: PaymentMethodTrialComponent;
  let fixture: ComponentFixture<PaymentMethodTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
