import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HttpService } from '../../../../core/services/http.service';
declare let  jQuery: any;
import * as $ from 'jquery';

import { CheckoutService } from '../../../services/checkout.service';
import { StripeService } from '../../../../core/services/stripe.service';
import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { GlobalService } from '../../../../core/services/global.service';
import { SimpleSubStepComponent } from '../../../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../../../core/animations/slide-in-out-checkout.animation';
import { CartService } from '../../../../core/services/cart.service';
import { ModalService } from '../../../../core/services/modal.service';
import { CardService } from '../../../../core/services/card.service';
import { CountriesService } from '../../../../core/services/countries.service';

@Component({
  selector: 'checkout-payment-method-trial',
  templateUrl: './payment-method-trial.component.html',
  styleUrls: ['./payment-method-trial.component.scss'],
  animations: [slideInOutCheckoutAnimation]
})
export class PaymentMethodTrialComponent extends SimpleSubStepComponent implements OnInit {
  @Output() notifyStep3Status: EventEmitter<any> = new EventEmitter<any>();
  @Input() show: boolean = false;
  @ViewChild('tabs') tabs: ElementRef;
  @ViewChild('cardNumber') cardNumber: ElementRef;
  @Input() stepsData: any;
  @Input() priceTotal: any;
  @Output() resetCardSelected: EventEmitter<any> = new EventEmitter<any>();
  currentCountry: any;
  allValid: boolean = false;
  stripeToken: string;
  public ipay88Status: boolean = false;
  public paymentMethodForm: FormGroup;
  public isLoading: boolean = true;
  public Stripe: any;
  public card: any;
  public message: string;
  public cardForm: FormGroup;
  public cardType: string;
  public yearList: Array<string> = [];
  public monthList: Array<string> = [];
  public selectedCard: any;
  public cardList: Array<any> = [];
  public stage: string = 'list';
  public paymentMethod: string = 'stripe';
  public resError: any;
  public hasSubscription: boolean = false;
  public hasSubscriptionTrial: boolean = false;
  public hasCustomPlan: boolean = false;
  constructor(
    el: ElementRef,
    public appConfig: AppConfig,
    stripeService: StripeService,
    public storage: StorageService,
    private loadingService: LoadingService,
    public modalService: ModalService,
    private builder: FormBuilder,
    private _http: HttpService,
    private checkoutService: CheckoutService,
    private alertService: AlertService,
    public globalService: GlobalService,
    public cartService: CartService,
    private cardService: CardService,
    private countriesService: CountriesService
  ) {
    super(el);
    this.paymentMethodForm = builder.group({
      cardnumber: ['', Validators.required],
      securityCode: ['', Validators.required]
    })
    this.cardForm = this.cardService.initCard();
    this.cardForm.removeControl("cardName");

    this.paymentMethodForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.cardForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.Stripe = stripeService.getInstance();

    // update card valid
    this.cardForm.valueChanges.subscribe(() => {
      this.allValid = false;
    });

    // init data
    for(let num = 2017; num <= 2500; num++) {
      this.yearList.push(`${num}`);
    }
    for(let num = 1; num <= 12; num++) {
      this.monthList.push(`${num}`);
    }

    // check if is free trial
    if (this.storage.getFreeTrialItem()) {
      // console.log('getFreeTrialItem --- ', this.storage.getFreeTrialItem())
      this.hasSubscriptionTrial = true;
    }

    // check if is custom plan
    if (this.storage.getCustomPlanItem()) {
      // console.log('getCustomPlanItem --- ', this.storage.getCustomPlanItem())
      this.hasCustomPlan = true;
    }

    this.resError = null;

    let cartItems = this.cartService.getCart();
    this.hasSubscription = false;
    if(cartItems) {
      cartItems.forEach(item => {
        if (item.plan) {
          this.hasSubscription = true;
        }
      });
    }

    cartService.hasUpdate.subscribe(hasUpdate => {
      let cartItems = this.cartService.getCart();
      this.hasSubscription = false;
      if(cartItems) {
        cartItems.forEach(item => {
          if (item.plan) {
            this.hasSubscription = true;
          }
        });
      }
    });

    // get currenct country
    this.currentCountry = this.storage.getCountry();

    // handle country change event
    this.countriesService.country.subscribe(country => {
      this.currentCountry = country;
    });

    if(!this.storage.getCountry().isWebStripe) {
      this.paymentMethod = 'ipay88';
    }
  }

  ngOnInit() {

  }
  // show form add new addresss
  addNewCard() {
    this.cardForm.reset();
    this.stage = 'add';
    this.resError = null;
  }

  validate() {
    if(this.paymentMethod === 'stripe') {
      return {
        allValid: this.selectedCard,
        stripeToken: this.stripeToken
      }
    } else {
      return {
        allValid: true
      }
    }
  }

  setActive() {

    this.getCardList();

    // this.loadingService.status.subscribe((value: boolean) => {
    //   this.isLoading = value;
    // })
    jQuery(function($) {
      jQuery('[data-stripe="number"]').payment('formatCardNumber');
      jQuery('[data-stripe="exp"]').payment('formatCardExpiry');
      jQuery('[data-stripe="cvc"]').payment('formatCardCVC');
    });

    this.cardForm.controls.cardNumber.valueChanges.subscribe(data => {
      if (data) {
        this.changeCardNumber();
      } else {
        this.cardType = '';
      }
    })
  }

  ngAfterViewInit() {
    // jQuery(this.tabs.nativeElement).smartTab({autoProgress: false,stopOnFocus:true,transitionEffect:'vSlide'});
  }

  getCardList(gotoNextStep?: boolean, selectedCardChangeId? : any) {
		// console.log(this.stepsData)
    this.isLoading = true;
    // if (!(this.stepsData &&
    //   this.stepsData.payment &&
    //   this.stepsData.payment.data &&
    //   this.stepsData.payment.data.paymentMethod) || !this.selectedCard) { // check if has no set payment
      if(this.storage.getCurrentUser()) {
        this.selectedCard = null;
        this._http._getList(`${this.appConfig.config.api.cards}?CountryId=${this.storage.getCountry().id}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
          data => {
            // build billing address and delivery address
            if(data && data.length > 0) {
              let hasRemove = true;
              this.cardList = data;
              // set selected card
              data.forEach((card, index) => {
                if(selectedCardChangeId) {
                  if(selectedCardChangeId === card.id) {
                    this.selectedCard = card;
                    this.selectedCard.index = (index + 1);
                  }
                } else {
                  if (card.isDefault) {
                    card.checked = card.isDefault;
                    this.selectedCard = card;
                    this.selectedCard.index = (index + 1);
                  }
                }
              });
              if (!this.selectedCard) {
                this.selectedCard = this.cardList[0];
                this.selectedCard.index = 1;
              }

              this.cardForm.patchValue({
                cardNumber: 'xxxx xxxx xxxx ' + this.selectedCard.cardNumber,
                expiry: this.selectedCard.expiredMonth + ' / ' + this.selectedCard.expiredYear.substr(2,2),
                cvc: '***'
              });
              this.cardType = this.selectedCard.branchName.toUpperCase();
              // this.setResetCardSelected(true);
            } else {
              this.cardList = [];
              this.globalService.resetForm(this.cardNumber);
              // this.setResetCardSelected(false);
            }
            this.isLoading = false;
            if (gotoNextStep) {
              this.goToNextStep();
            }
          },
          error => {
            this.isLoading = false;
            // console.log(error);
          }
        );
      } else { // checkout by guest
        this.isLoading = false;
        this.cardList = this.storage.getCardByGuest() ? this.storage.getCardByGuest() : [];
        // check for case address has been removed
        let hasRemove = true;
        if (this.cardList && this.cardList.length > 0) {
          if (this.selectedCard) {
            this.cardList.forEach(card => {
              card.expiredYear = "" + card.expiredYear;
              if (this.selectedCard.id === card.id) {
                hasRemove = false;
              }
            });
          }

          if (hasRemove) {
            this.selectedCard = this.cardList[0];
            this.selectedCard.index = 1;
            this.selectedCard.expiredYear = "" + this.selectedCard.expiredYear;
          }
          this.setResetCardSelected(true);
          if (gotoNextStep) {
            this.goToNextStep();
          }
        } else {
          this.cardList = [];
          this.setResetCardSelected(false);
        }
      }
    // } else {
    //   this.isLoading = false;
    // }
  }

  // create stripe token
  createToken() {
    let $form = this.element.find('#cardForm');
    return new Promise((resolve, reject) => {
      this.Stripe.card.createToken($form, (status, response) => {
        if(response.error) {
          this.resError = {
            type: 'error',
            message: response.error.message
          }
          this.loadingService.display(false);
          // this.alertService.error([{'ok':false, 'message': response.error.message}]);
          reject(response.error.message);
        } else {
          this.resError = null;
          resolve(response.id);
        }
      });
    });
  }

  // create stripe customer
  createCustomer(stripeToken) {
    if(this.storage.getCurrentUser()) { // For case signed in
      return new Promise((resolve, reject) => {
        this._http._create(`${this.appConfig.config.api.cards}`.replace('[userID]', this.storage.getCurrentUser().id), {stripeToken, CountryId: this.storage.getCountry().id}).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            this.loadingService.display(false);
            reject(error);
          }
        );
      });
    } else { // For case checkout by guest
      return new Promise((resolve, reject) => {
        this._http._create(`${this.appConfig.config.api.users}/cards`, {stripeToken, email: this.stepsData.login.data.email, type: 'website'}).subscribe(
          data => {
            resolve(data);
            if (data) {
              data.expiredYear = `${data.expiredYear}`;
              this.storage.setCardByGuest(data);
            }
          },
          error => {
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            this.loadingService.display(false);
            reject(error);
          }
        );
      });
    }
  }

  // submit Dredit/Debit card
  submitCard(cardInfo) {
    this.loadingService.display(true);
    if(this.cardList.length > 0 && this.selectedCard) {
      if(this.cardForm.valid &&
        (this.cardForm.controls['cardNumber'].value !== 'xxxx xxxx xxxx ' + this.selectedCard.cardNumber ||
        this.cardForm.controls['expiry'].value !== this.selectedCard.expiredMonth + ' / ' + this.selectedCard.expiredYear.substr(2,2) ||
        this.cardForm.controls['cvc'].value !== '***'
      )) {
        this.createToken()
          .then(stripeToken => this.createCustomerEdit(stripeToken, cardInfo))
          .then(customer => {
            // update stage
            this.stage = 'list';
            // reload list
            this.getCardList(true);
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: typeof error === 'string' ? error : JSON.parse(error).message
            };
          });
      } else {
        this.gotoConfirmStep();
        // this.loadingService.display(false);
      }
    } else {
      if(this.cardForm.valid) {
        this.createToken()
          .then(stripeToken => this.createCustomer(stripeToken))
          .then(customer => {
            // update stage
            this.stage = 'list';
            // reload list
            this.getCardList(true);
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
          });
      } else {
        this.loadingService.display(false);
      }
    }

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // create stripe customer for edit
  createCustomerEdit(stripeToken, data) {
    if(this.storage.getCurrentUser()) { // For case signed in
      return new Promise((resolve, reject) => {
        let valueDefault: boolean = true;
        this._http._updatePut(`${this.appConfig.config.api.cards}/${this.selectedCard.id}`.replace('[userID]', this.storage.getCurrentUser().id), {stripeToken, isDefault: valueDefault, CountryId: this.storage.getCountry().id}).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            reject(error);
          }
        );
      });
    }
  }

  // goToStep3
  goToNextStep() {
    this.allValid = true;
    this.notifyStep3Status.emit({
      paymentMethod: this.paymentMethod,
      card: this.selectedCard
    });
    this.resError = null;
  }

  // Confirm Deletion modal
  showConfirmDeletionModal(item: any, event: any) {
    // this.modalService.open('confirm-deletion-modal', {item: item, event: event});
  }

  gotoConfirmStep() {
    if (this.selectedCard && this.paymentMethod === 'stripe') {
      this.notifyStep3Status.emit({
        paymentMethod: this.paymentMethod,
        card: this.selectedCard
      });
    } else if(this.paymentMethod === 'ipay88') {
      this.notifyStep3Status.emit({
        paymentMethod: this.paymentMethod
      });
    }
  }

  // onchange card number
  changeCardNumber() {
    this.cardType = this.globalService.getCardType(this.cardForm.controls['cardNumber'].value).toUpperCase();
  }

  // change default address
  changeSelectedCard(card) {
    this.cardList.forEach(card => {
      card.isDefault = false;
      card.checked = false;
    });
    card.checked = true;
    this.selectedCard = card;
  }

  // auto spacing for card number
  autoSpacing() {
    let  value = $('#cardNumber').val().replace(/\s/g,'');
    let  v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    let  matches = v.match(/\d{4,16}/g);
    let  match = matches && matches[0] || ''
    let  parts = []

    for (let i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }
    let output: any;
    if (parts.length) {
        output = parts.join(' ');
    } else {
        output = value;
    }

    $('#cardNumber').val(output);
  }

  // open address list popup
  showCardList() {
    this.modalService.open('card-list-modal', {reGetDB: true});
  }

  // reset card default to checkout
  hasResetDefault(_data) {
    if(!this.storage.getCurrentUser()) {
      this.cardList = this.storage.getCardByGuest();
    }
    this.selectedCard = this.cardList.filter(card => card.id === _data.id) ? this.cardList.filter(card => card.id === _data.id)[0] : [];
    this.selectedCard.expiredYear = this.selectedCard.expiredYear + '';
    this.cardList.forEach((card, index) => {
      if (card.id === _data.id) {
        // this.selectedCard = card;
        this.selectedCard.index = index + 1;
      }
    });
  }

  // subscriber if address has change from card list modal
  hasChangeCard(_res) {
    if (_res) {
      // this.getCardList();
      if(_res.data && _res.data.id === this.selectedCard.id) {
        this.getCardList(null, _res.data.id);
      } else {
        this.getCardList(null, this.selectedCard.id);
      }
    }
  }

  hasRemove(_res) {
    if (_res && _res.id) {
      let idRemove = this.selectedCard.id === _res.id ?  null : this.selectedCard;
      if (!idRemove) {
        this.selectedCard = null;
        this.getCardList();
      }
    } else {
      this.getCardList();
    }
  }

  setResetCardSelected(status: boolean){
    if (status) {
      this.resetCardSelected.emit({
        card: this.selectedCard
      });
    } else {
      this.resetCardSelected.emit({
        card: null
      });
    }

  }

	togglerTooltip() {
		$('.tooltip_info').toggleClass('hidden');
	}
}
