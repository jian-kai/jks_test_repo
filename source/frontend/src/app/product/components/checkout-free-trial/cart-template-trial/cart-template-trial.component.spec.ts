import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartTemplateTrialComponent } from './cart-template-trial.component';

describe('CartTemplateTrialComponent', () => {
  let component: CartTemplateTrialComponent;
  let fixture: ComponentFixture<CartTemplateTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartTemplateTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartTemplateTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
