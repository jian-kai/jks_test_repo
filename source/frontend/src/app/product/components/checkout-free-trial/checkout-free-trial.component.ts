import { Component, OnInit, ElementRef, ViewChild, QueryList } from '@angular/core';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { AppConfig } from '../../../../app/config/app.config';
import { HttpService } from '../../../core/services/http.service';
import { StripeService } from '../../../core/services/stripe.service';
import { CheckoutService } from '../../services/checkout.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { CartService } from '../../../core/services/cart.service';
import { SimpleMultiStepComponent } from '../../../core/components/partials/simple-multi-step/simple-multi-step.component';
import { CountriesService } from '../../../core/services/countries.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { CardService } from '../../../core/services/card.service';
import { GTMService } from '../../../core/services/gtm.service';
import { DialogService } from '../../../core/services/dialog.service';

// import view child
import { SignInTrialComponent } from './sign-in-trial/sign-in-trial.component';
import { DeliveryAddressTrialComponent } from './delivery-address-trial/delivery-address-trial.component';
import { PaymentMethodTrialComponent } from './payment-method-trial/payment-method-trial.component';
import { SummaryPlaceOrderTrialComponent } from './summary-place-order-trial/summary-place-order-trial.component';
import { RouterService } from '../../../core/helpers/router.service';

@Component({
  selector: 'app-checkout-free-trial',
  templateUrl: './checkout-free-trial.component.html',
  styleUrls: ['./checkout-free-trial.component.scss']
})
export class CheckoutFreeTrialComponent extends SimpleMultiStepComponent implements OnInit {

  @ViewChild(SignInTrialComponent)
  private signInTrialComponent: SignInTrialComponent;
  @ViewChild(DeliveryAddressTrialComponent)
  private deliveryAddressTrialComponent: DeliveryAddressTrialComponent
  @ViewChild(PaymentMethodTrialComponent)
  private paymentMethodTrialComponent: PaymentMethodTrialComponent
  @ViewChild(SummaryPlaceOrderTrialComponent)
  private summaryPlaceOrderTrialComponent: SummaryPlaceOrderTrialComponent

  @ViewChild('account') account: ElementRef;
  @ViewChild('address')  address: ElementRef;
  @ViewChild('payment')  payment: ElementRef;
  @ViewChild('review')  review: ElementRef;

  element: any;
  Stripe: any;
  currentCountry: any;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  cardForm: FormGroup;
  email: AbstractControl;
  stepsData: any;
  // currentStep: string = 'login';

	// Data select from free trial
	dataSelect: any = [];
	dataImage: string;
	dataProduct: any = [];
	dataProductOption: any = [];
  refillships_date: any;
  refillships_date_2: any;
  refillships_date_next: any;
  refillships_date_next_2: any;
  _time: any;
  listMonth: any = [];

	// Promo
  promoCode: string;
  checkPromoCodeApplied: boolean = false;
  promoCodeError: any;
  notAllowPromoCode: boolean = false;
  promotion: any;
  public promoCodeForm: FormGroup;

  public currentUser;
  public isLoading: boolean = true;

  public workflowStatus: any;
  public activeStepStatus: any;
  resError: any;
  public i18nDialog: any;
  public receipt: any;
  public cartRules: any;
  public cartItems: any;
  public hasFreeTrial : boolean = false;
  public freeTrialItem: any;

  constructor(stripeService: StripeService,
    private builder: FormBuilder,
    private el: ElementRef,
    private checkoutService: CheckoutService,
    public storage: StorageService,
    private usersService: UsersService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private httpService: HttpService,
    private router: Router,
    public appConfig: AppConfig,
    public cartService: CartService,
    private countriesService: CountriesService,
    private routerService: RouterService,
    private deliveryAddressService: DeliveryAddressService,
    private cardService: CardService,
    private gtmService: GTMService,
    private dialogService: DialogService,
  ) {
    super();
    this.cardForm = builder.group({
      cardNumber: ['', Validators.required],
      expiryMonth: ['', Validators.required],
      expiryYear: ['', Validators.required],
      cvc: ['', Validators.required]
    });

		// Validate promocode
    this.promoCodeForm = builder.group({
      promoCode: ['', Validators.required],
    });

    this.Stripe = stripeService.getInstance();
    this.element = $(el.nativeElement);

    this.cardForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
    this.getCheckoutPreviousData();
    cartService.hasUpdate.subscribe(hasUpdate => {
			if(this.getReceiptForFreeTrial()) {
        return;
      }
    });
  }

  checkDelete(data) {

  }

  ngOnInit(): void {
    // set footer short
    this.routerService.setFooterFull(false);
    this.routerService.setHeaderCommon(false);

    this.resError = null;
    // check if cart empty then redirect to homePage
    this.checkCartEmpty();
    // initialize card info
    this.cardNumber = '';
    this.expiryMonth = '';
    this.expiryYear = '';
    this.cvc = '';
		// // Time
		// let currDate = new Date();
		// this.refillships_date = currDate.setDate(currDate.getDate() + 20);

    this.setCurrentStep('login');
    this.setStepsData({
      login: {state: 'active', data: {}, step: 'signInTrialComponent'},
      deliveryAddress: {state: 'init', data: {}, step: 'deliveryAddressTrialComponent'},
      payment: {state: 'init', data: {}, step: 'paymentMethodTrialComponent'},
      // summary: {state: 'init', data: {}, step: 'summaryPlaceOrderTrialComponent'},
      hasChangeAccount: {status: false},
    })
    // this.getNotifyStep1Status({});

    // step 1 => sign up
    this.currentUser = this.storage.getCurrentUserItem();
    // // console.log(this.currentUser);
    if (this.currentUser) {
      this.stepsData.login.data = this.currentUser;
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
    }
    // handle country change event
    this.currentCountry = this.storage.getCountry();
    this.countriesService.country.subscribe(country => {
      this.checkCartEmpty();
      this.currentCountry = country;
    });

    // subscribe card action
    this.cardService.cardAction.subscribe(data => {
      if( data ) {
        // action: select change | remove
        if (data.action === 'reset') {
          if(!data.dataReset) {// console.log('data.dataReset 1', data.dataReset);
            this.stepsData.payment.data = {};
            this.stepsData.loadedReview = false;
          } else {
            // console.log('data.dataReset 2', data.dataReset);
          }
        }
        // action: add
        if (data.action === 'add') {

        }
      }
    })
    // $( window ).scroll(function() {
    //   if($( window ).width()<=767){
    //     if(53 - $(window).scrollTop() >=0)
    //       $('.progress-tabs').css("top",(55 - $(window).scrollTop())+"px");
    //     if($(window).scrollTop() >= 55)
    //       $('.progress-tabs').css("top","0px");
    //   }
    // });

		if(this.getReceiptForFreeTrial() == false) {
			this.router.navigate(['/']);
		}

		// Promocode
    this.promotion = this.cartService.getPromoCode();
    if(this.promotion.id) {
      this.promoCode = this.promotion.promotionCodes[0].code;
    }

		// Get data storage plan select
		if (this.storage.getFreeTrialItem()) {
			let _dataPlanStorage = this.storage.getFreeTrialItem();
			let _dataPlanStoragePlanActive = _dataPlanStorage.planActive;
			let _filterImageDefault = _dataPlanStoragePlanActive.planImages.filter(value => (value.isDefault === true));
			// Get image plan
			this.dataImage = _filterImageDefault[0].url;
			// Get month (Every ...)
      this._time = _dataPlanStoragePlanActive.PlanType.name;
      this.listMonth.push({name: _dataPlanStoragePlanActive.PlanType.planTypeTranslate});
      // caculate next refill shipping date
      let nextMonth = parseInt(this._time.substring(0, this._time.indexOf(' ')));
      if(this.storage.getCountry().code.toUpperCase() === 'KOR') {
        this.loadingService.display(true);
        this.httpService._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
          data => {
            if (data) {
              let currDate = new Date(data);
              this.refillships_date = currDate.setDate(currDate.getDate() + 13);
              this.refillships_date_2 = currDate.setDate(currDate.getDate() + 3);
              this.refillships_date_next = new Date(this.refillships_date);
              this.refillships_date_next_2 = new Date(this.refillships_date_2);
              this.refillships_date_next = this.refillships_date_next.setMonth(this.refillships_date_next.getMonth() + nextMonth);
              this.refillships_date_next_2 = this.refillships_date_next_2.setMonth(this.refillships_date_next_2.getMonth() + nextMonth);
            }
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
          }
        )
      } else {
        let currDate = new Date();
        this.refillships_date = currDate.setDate(currDate.getDate() + 13);
        this.refillships_date_2 = currDate.setDate(currDate.getDate() + 3);
        this.refillships_date_next = new Date(this.refillships_date);
        this.refillships_date_next_2 = new Date(this.refillships_date_2);
        this.refillships_date_next = this.refillships_date_next.setMonth(this.refillships_date_next.getMonth() + nextMonth);
        this.refillships_date_next_2 = this.refillships_date_next_2.setMonth(this.refillships_date_next_2.getMonth() + nextMonth);
      }

			let _dataProduct = _dataPlanStoragePlanActive.planCountry.planOptions.filter(value => (value.id === _dataPlanStorage.productSelected))
			this.dataProduct = _dataProduct[0];
      if(this.promotion.id) {
        this.dataProduct.sellPriceDiscount = this.dataProduct.sellPrice - (this.dataProduct.sellPrice * (this.promotion.discount / 100));
      } else {
        this.dataProduct.sellPriceDiscount = 0;
      }
			let _productOption = this.dataProduct.planOptionProducts;
			_productOption.forEach(value => {
				this.dataProductOption.push({productTranslate: value.ProductCountry.Product.productTranslate, qty: value.qty});
			});
		}

		// Promo handle
    this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
      if(value === '') {
        this.cartService.setPromotion();
      }
    })
	}

  checkCartEmpty() {
    if( ( (!this.cartService.getCart() || this.cartService.getCart().length <= 0) && !this.storage.getFreeTrialItem() && !this.storage.getCustomPlanItem() )) {
      this.router.navigate(['/']);
      this.routerService.setFooterFull(true);
      this.routerService.setHeaderCommon(true);
    } else {

    }
  }

  /**
   * step 1 => Sign in
   */
  getNotifyStep1Status(data) {
    if(data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.gtmService.checkout({step: 1, options: data.email ? 'logged in' : 'guest'});
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }

  goToLogin() {
    if(Object.keys(this.stepsData.login.data).length === 0 || !this.storage.getCurrentUserItem()) {
      this.goto('login');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  hasChangeAccount(data) {
    if (data) {
      this.stepsData.login.data = {};
      this.paymentMethodTrialComponent.selectedCard = null;
      this.stepsData.hasChangeAccount.status = true;
      $(this.account.nativeElement).trigger('click');
    }
  }

  /**
   * step 2 => delivery address
   */
  getNotifyStep2Status(data) {
    this.gtmService.checkout({step: 2, options: this.stepsData.login.data.email ? 'logged in' : 'guest'});
    if(data.state === 'login') {
      this.stepsData.login.data.email = data.email;
      this.goto('login');
      window.scrollTo(0, 0);

      // console.log('this.stepsData', this.stepsData)
      this.resError = null;
    } else {
      // console.log(`data ==== ${JSON.stringify(data)}`);
      this.stepsData.deliveryAddress.data = data;
      if (Object.keys(this.stepsData.login.data).length == 0 && data.emailByGuest) {
        this.stepsData.login.data = { email: data.emailByGuest }
      }
      this.goto('payment');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

	/* Step 2 => delivery address edit */
	getNotifyStep2Edit(edit: boolean) {
		if(edit) {
			this.goto('deliveryAddress');
		}
	}

  goToDeliveryAddress() {
    if(Object.keys(this.stepsData.login.data).length !== 0) {
      this.goto('deliveryAddress');
      window.scrollTo(0, 0);
      this.resError = null;
    }
  }

  /**
   * step 3 => payment method
   */
  getNotifyStep3Status(data) {
    this.stepsData.payment.data = data;
    this.stepsData.loadedReview = true;
    this.payNow();
    window.scrollTo(0, 0);
    this.resError = null;
  }

	payNow() {
    this.loadingService.display(true);
    if (this.storage.getFreeTrialItem()) {
			if (this.storage.getFreeTrialItem()) {
	      // console.log('this.storage.getFreeTrialItem() --- ', this.storage.getFreeTrialItem())

	      let currentUser = this.storage.getCurrentUserItem();
	      if (currentUser) {
	        this.cartService.getSubscriptionsByUser(currentUser).then(
	          (data: any) => {
	            currentUser.subscriptions = data;
	            this.storage.setCurrentUser(currentUser);
	            if (data && data.length > 0) {
	                let checkIsTrialSub : boolean = false;
	                data.forEach(item => {
	                  if (item.isTrial && item.status !== "Canceled") {
	                    checkIsTrialSub = true;
	                  }
	                });
	                // console.log('checkIsTrialSub --- ', checkIsTrialSub);
	                // if user has exists subs
	                if (checkIsTrialSub) {
	                  this.loadingService.display(false);
	                  this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
	                    if (result) {}
	                  }));
	                } else {
	                  this.handlePaynowWithTrial();
	                }
	              } else {
	                this.handlePaynowWithTrial();
	              }
	          },
	          error => {
	            // console.log(error.error)
	          }
	        )

	      } else {
	        let _email = this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest;
	        this.cartService.getSubscriptionsByGuest(_email).then(
	          (data: any) => {
	            let checkIsTrialSub : boolean = false;
	            if (data && data.user && data.user.subscriptions && data.user.subscriptions.length > 0) {
	              data.user.subscriptions.forEach(item => {
	                if (item.isTrial) {
	                  checkIsTrialSub = true;
	                }
	              });
	              if (checkIsTrialSub) {
	                this.loadingService.display(false);
	                this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
	                  if (result) {
	                    //
	                  }
	                }));
	              } else {
	                this.handlePaynowWithTrial();
	              }
	            } else {
	              this.handlePaynowWithTrial();
	            }
	          },
	          (error: any) => {
	            // console.log(error.error)
	          }

	        )
	      }
	    }
    }
  }


  setResetCardSelected(data) {
    if (data) {
      this.stepsData.payment.data = data;
    } else {
      this.stepsData.payment.data = {};
    }
  }

  goToPayment() {
    if(this.deliveryAddressTrialComponent.getSelectedAddress() && Object.keys(this.stepsData.login.data).length !== 0) {
      this.deliveryAddressTrialComponent.goToStep3();
    }
  }

  // goToSummary() {
  //   if(this.paymentMethodTrialComponent.paymentMethod === 'ipay88' ||
  //     (this.paymentMethodTrialComponent.paymentMethod === 'stripe' && this.paymentMethodTrialComponent.selectedCard)) {
  //     this.paymentMethodTrialComponent.gotoConfirmStep();
  //     this.goto('summary');
  //     window.scrollTo(0, 0);
  //   }
  // }
  getCheckoutPreviousData(){
    let check = this.storage.getCheckoutData();
    let past_transaction = this.storage.getGtmTransactionID();

    if(check){
      console.log("================================= getCheckoutData is NOT EMPTY =============================");
      console.log('Checkout Data : '+check);
      console.log('Past Transaction : '+past_transaction);
      this.router.navigate([`/`]);     
    }else{
      console.log("================================= getCheckoutData is EMPTY =================================");
      let test_variable = 'teststorage';
      let current_date = new Date();
      let checkoutItems = {"Test": test_variable,"Date" : current_date};
      this.storage.setCheckoutData(checkoutItems);
    }
  }
  handlePaynowWithTrial() {
    console.log("start trial payment handler");
    let order;
    let _orderData = {};
    let _trial_item = this.storage.getFreeTrialItem();
    let promoCode = '';
    if(this.cartService.getPromoCode().id) {
      promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
    }
    _orderData = {
      userTypeId: 1,
      planCountryId: _trial_item.planActive.planCountry.id,
      email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
      CardId: this.stepsData.payment.data.card.id,
      DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
      BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
      isWeb: true,
			PlanOptionId: _trial_item.productSelected,
      promotionCode: promoCode
    }
    console.log("trial orderdata"+JSON.stringify(_orderData));

    this.createFreeTrialOrder(_orderData)
    .then(result => {
      // console.log(`payment trial result ---  ${result}`);
      // submit to GTM
      this.gtmService.purchaseCart(result['result']);

      this.cartService.reloadSubscriptions();
      if(this.stepsData.payment.data.paymentMethod === 'stripe') {

        order = result['result']['order'];
        if(!this.storage.getCurrentUserItem()) {
          localStorage.setItem('orderId', order.id);
        }

        // clear free trial storage
        this.storage.clearFreeTrialItem();

        // clear card, address by guest
        this.storage.clearCardByGuest();
        this.storage.clearDeliveryAddressByGuest();
        // set gtm transaction id
        this.storage.setGtmTransactionID(order.id);

        // redirect to orders page
        this.router.navigate([`/thankyou/${order.id}`]);

        this.loadingService.display(false);
      }
    })
    .catch(error => {
      this.loadingService.display(false);
      // console.log(`payment trial error ${error}`);
      this.resError = {
        type: 'error',
        message: JSON.parse(error).message
      };
      this.paymentMethodTrialComponent.resError = this.resError;
      // this.summaryPlaceOrderTrialComponent.resError = this.resError;
    });
  }

  handlePaynowWithCustomPlan() {
    let order;
    let _orderData;
    let _custom_item = this.storage.getCustomPlanItem();
    _orderData = {
      userTypeId: 1,
      PlanTypeId: _custom_item.PlanTypeId,
      email: this.stepsData.login.data.email ? this.stepsData.login.data.email : this.stepsData.deliveryAddress.data.emailByGuest,
      CardId: this.stepsData.payment.data.card.id,
      DeliveryAddressId: this.stepsData.deliveryAddress.data.shipping.id,
      BillingAddressId: this.stepsData.deliveryAddress.data.billing.id,
      isWeb: true,
      customPlanDetail: [],
      shippingFee: this.receipt.shippingFee
    }
    _custom_item.customPlanDetail.forEach(element => {
      _orderData.customPlanDetail.push({
        qty: element.qty,
        ProductCountryId: element.product.productCountry.id,
        productName: element.product.productTranslate[0].name,
        duration: element.duration
      });
    });

    this.createCustomPlanOrder(_orderData)
    .then(result => {
      // console.log(`payment custom plan result ---  ${result}`);
      // submit to GTM
      this.gtmService.purchaseCart(result['result']);

      this.cartService.reloadSubscriptions();
      if(this.stepsData.payment.data.paymentMethod === 'stripe') {

        order = result['result']['order'];
        if(!this.storage.getCurrentUserItem()) {
          localStorage.setItem('orderId', order.id);
        }

        // clear custom plan storage
        this.storage.clearCustomPlanItem();

        // clear card, address by guest
        this.storage.clearCardByGuest();
        this.storage.clearDeliveryAddressByGuest();
        // set gtm transaction id
        this.storage.setGtmTransactionID(order.id);

        // redirect to orders page
        this.router.navigate([`/thankyou/${order.id}`]);

        this.loadingService.display(false);
      }
    })
    .catch(error => {
      this.loadingService.display(false);
      // console.log(`payment trial error ${error}`);
      this.resError = {
        type: 'error',
        message: JSON.parse(error).message
      };
      this.paymentMethodTrialComponent.resError = this.resError;
    });
  }

  createOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if(utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if(this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              // this.summaryPlaceOrderTrialComponent.resError = this.resError;
              reject(error);
            }
          );
      } else {
        this.httpService._create(`${this.appConfig.config.api.place_order}/ipay88`, orderData)
        .subscribe(
          res => {
            // console.log(`create order ${JSON.stringify(res)}`);
            resolved(res);
          },
          error => {
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            // this.summaryPlaceOrderTrialComponent.resError = this.resError;
            reject(error);
          }
        );
      }
    });
  }

  
  createFreeTrialOrder(orderData) {
    console.log("start createFreeTrialOrder ");
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if(utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }
    console.log("start freetrial paymentAPI");
    return new Promise((resolved, reject) => {
      if(this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order_trial}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              // this.summaryPlaceOrderTrialComponent.resError = this.resError;
              reject(error);
            }
          );
      }
    });
  }

  createCustomPlanOrder(orderData) {
    // get utm_source
    let utmSource = this.storage.getGtmSource();
    utmSource = utmSource ? utmSource : $('#gtm-referrer').html();
    if(utmSource && utmSource !== '') {
      orderData.utmSource = utmSource;
      orderData.utmMedium = this.storage.getGtmMedium();
      orderData.utmCampaign = this.storage.getGtmCampaign();
      orderData.utmTerm = this.storage.getGtmTerm();
      orderData.utmContent = this.storage.getGtmContent();
    }

    return new Promise((resolved, reject) => {
      if(this.stepsData.payment.data.paymentMethod === 'stripe') {
        this.httpService._create(`${this.appConfig.config.api.place_order_custom_plan}/stripe`, orderData)
          .subscribe(
            res => {
              // console.log(`create order ${JSON.stringify(res)}`);
              resolved(res);
            },
            error => {
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              // this.summaryPlaceOrderTrialComponent.resError = this.resError;
              reject(error);
            }
          );
      }
    });
  }

  getNotifyJustLogin(data) {
    if(data) {
      this.stepsData.login.data = data;
      this.stepsData.hasChangeAccount.status = false;
    }
    this.goto('deliveryAddress');
    window.scrollTo(0, 0);
    this.resError = null;
  }
	submitcheck() {
    if(this.stepsData.login.state === 'active') {
      $('#submitStep1').trigger('click');
    } else {
      $('#submit').trigger('click');
    }
	}


	// Get Receipt
	getReceiptForFreeTrial() {
    if (this.storage.getFreeTrialItem()) {
      this.hasFreeTrial = true;
      this.freeTrialItem = this.storage.getFreeTrialItem();
      let currentCountry = this.storage.getCountry();
      let price = 0;

      if(currentCountry.id === 1){
        price = 6.50;
      }else if(currentCountry.id === 7){
        price = 5.00;
      }else {
        price = 0;
      }
      // this.cartItems[0] = this.freeTrialItem;
      let _receipt = {
        totalPrice: price,
        totalPriceNotDivided: 0,
        specialDiscount: 0,
        discount: 0,
        subTotal: price,
        shippingFee: 0,
        totalIclTax: 0,
        taxRate: 0,
        applyDiscount: 0,
        taxName: ''
      }
      
      _receipt.taxRate = currentCountry.taxRate;
      _receipt.taxName = currentCountry.taxName;
      this.receipt = _receipt;
      this.receipt.shippingFee = 0;
      this.receipt.totalIclTax = this.receipt.totalPrice + this.receipt.shippingFee;
      return true;
    }
    return false;
  }

	// Apply promo code
  applyPromoCode() {
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    let planCountryIds = [this.freeTrialItem.planActive.planCountry.id];
    // this.cartItems.forEach(cartItem => {
    //   if(cartItem.product) {
    //     productCountryIds.push(cartItem.product.productCountry.id);
    //   }
    //   if(cartItem.plan) {
    //     if(!cartItem.plan.planCountry[0]) {
    //       planCountryIds.push(cartItem.plan.planCountry.id);
    //     }
    //     if(cartItem.plan.planCountry[0]) {
    //       planCountryIds.push(cartItem.plan.planCountry[0].id);
    //     }
    //   }
    // })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: this.promoCode,
      productCountryIds,
      planCountryIds,
      userTypeId: '1'
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);
        
        this.dataProduct.sellPriceDiscount = this.dataProduct.sellPrice - (this.dataProduct.sellPrice * (data.discount / 100));

        // this.translate.get('msg').subscribe(res => {
        //   this.i18nDialog = res;
        // });
        // if(data.minSpend && this.receipt.totalPrice < data.minSpend) {
        //   // this.cartService.setPromotion();
        //   this.promoCodeError = {
        //     type: 'error',
        //     message: this.i18nDialog.validation.promocode_minprice.replace('{{value}}', this.storage.getCountry().currencyDisplay + data.minSpend)
        //   };
        // }

        // if(this.receipt.totalPriceNotDivided > this.appConfig.getConfig('shippingFeeConfig').minAmount && data.isFreeShipping) {
        //   this.promoCodeError = {
        //     type: 'error',
        //     message: this.i18nDialog.validation.promocode_not_apply
        //   };
        // }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        // console.log(error)
      }
    )
  }
}
