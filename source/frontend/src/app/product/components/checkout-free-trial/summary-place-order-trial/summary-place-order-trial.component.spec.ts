import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPlaceOrderTrialComponent } from './summary-place-order-trial.component';

describe('SummaryPlaceOrderTrialComponent', () => {
  let component: SummaryPlaceOrderTrialComponent;
  let fixture: ComponentFixture<SummaryPlaceOrderTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPlaceOrderTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPlaceOrderTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
