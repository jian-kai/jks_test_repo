import { Component, ElementRef, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import {HttpService} from '../../../core/services/http.service';
import {GlobalService} from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import {AlertService} from '../../../core/services/alert.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { DialogService } from '../../../core/services/dialog.service';
import { CardService } from '../../../core/services/card.service';


@Component({
  selector: 'card-list-modal',
  templateUrl: './card-list-modal.component.html',
  styleUrls: ['./card-list-modal.component.scss']
})
export class CardListModalComponent extends ModalComponent {
  countryStates: Array<any>;
  public deliveryAddressForm;
  public isLoading: boolean = true;
  private currentUser: any;
  resError: any;
  errorCardId: any;
  public cardList: any;
  public i18nDialog: any;
  public strPayment;
  private currentCountryId: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Output() hasResetDefault: EventEmitter<object> = new EventEmitter<any>();
  @Output() hasRemove: EventEmitter<object> = new EventEmitter<any>();
  @Output() hasClicked = new EventEmitter<boolean>();
  @Input() selectedcard: any;
  @Input() email: any;
  @Input() currentSubId: any;
  //@Input() currentCountryId: any;
  
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    private loadingService: LoadingService,
    private deliveryAddressService: DeliveryAddressService,
    private _dialogService: DialogService,
    private alertService: AlertService,
    private cardService: CardService,
    private translate: TranslateService,
  ) {

    super(modalService, el);
    this.id = 'card-list-modal';
    this.deliveryAddressForm = this.deliveryAddressService.initAddress();
    this.deliveryAddressForm.patchValue({callingCode: '+' + this.storage.getCountry().callingCode});
    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  setInitData(_data: any, _card?: any) {
    this.currentCountryId = _data.CountryId ? _data.CountryId : this.storage.getCountry().id;    
    this.getCountryStates();
    this.getCardList();
    if(_card) {
      this.selectedcard = _card;
    }
    // this.deliveryAddressForm.reset();
    this.resError = null;
  }

  // get country states
  getCountryStates() {
    this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
      data => {
        this.countryStates = data;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // get delivery address list
  getCardList(fromAction?: string, dataChange?: any) {
    if(this.storage.getCurrentUser()) {
      this._http._getList(`${this.appConfig.config.api.cards}?CountryId=${this.currentCountryId}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
        data => {
          if(data) {
            this.cardList = data;
            let _dataRest = this.cardList.length > 0 ? this.cardList[0] : null;
            if (fromAction && (fromAction === 'remove')) {
              this.cardService.setCardAction({action: 'reset', dataReset: _dataRest})
            }
            if (fromAction && fromAction === 'add') {}
            if (fromAction && fromAction === 'change') {
              _dataRest = null;
              if (this.cardList.length > 0 && dataChange) {
                let _dataRest = this.cardList.filter(card => card.id === dataChange.id)[0];
              }              
              this.cardService.setCardAction({action: 'reset', dataReset: _dataRest})
            }
          }
          // if (data && this.cardList.length <= 0) {
          //   this.modalService.close(this.id);
          // }
        },
        error => {
          // console.log(error);
        }
      );
    } else { // checkout by guest
      this.cardList = this.storage.getCardByGuest() ? this.storage.getCardByGuest() : [];
      if (this.cardList.length > 0) {
        this.cardList.forEach(card => {
          card.expiredYear = "" + card.expiredYear;
        });
      }
      let _dataRest = this.cardList.length > 0 ? this.cardList[0] : null;
      if (fromAction && (fromAction === 'remove')) {
        this.cardService.setCardAction({action: 'reset', dataReset: _dataRest})
      }

      if (fromAction && fromAction === 'change') {
        _dataRest = null;
        if (this.cardList.length > 0 && dataChange) {
          let _dataRest = this.cardList.filter(card => card.id === dataChange.id)[0];
        }              
        this.cardService.setCardAction({action: 'reset', dataReset: _dataRest})
      }
    }
  }

  // show form edit card
  editCard(_card: any) {
    this.modalService.open('card-modal-form', _card);
    this.resError = null;
  }

  // show form add new card
  addNewCard() {
    this.modalService.open('card-modal-form', {reGetDB: true, CountryId: this.currentCountryId});
  }

  // delete card
  deleteCard(card, event) {
    this.translate.get('lbl').subscribe(res => {
      this.i18nDialog = res;
    });
    let strDeletePayment = this.i18nDialog.user.subscription.delete_payment;
    this._dialogService.confirm(strDeletePayment.replace('{{value}}', card.cardNumber)).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        event.stopPropagation();
        if(this.storage.getCurrentUser()) {
          this._http._delete(`${this.appConfig.config.api.cards}/${card.id}?CountryId=${this.currentCountryId}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
            data => {
              this.hasChange.emit(true);
              // this.hasRemove.emit({status: true, id: card.id});
              if (this.selectedcard.id === card.id) {
                this.getCardList('remove');
                this.hasRemove.emit({id: card.id});
              } else {
                this.getCardList();
              }
              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              // console.log(error);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.errorCardId = card.id;
            }
          );
        } else {
          this._http._delete(`${this.appConfig.config.api.users}/cards/${card.id}`).subscribe(
            data => {
              this.hasChange.emit(true);
              this.hasRemove.emit({status: true, id: card.id});
              this.storage.removeCardByGuest(card);
              if (this.selectedcard.id === card.id) {
                this.getCardList('remove');
              } else {
                this.getCardList();
              }
              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              // console.log(error);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              this.errorCardId = card.id;
            }
          );
        }
      }
    }));
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  

    // when user edited address
  hasChangeCard(data: any) {
    this.hasChange.emit(data);
    this.modalService.close('card-modal-form')
    this.modalService.open(this.id);
    this.getCardList();
  }

  // select card
  selectCard(_card) {
    this.hasClicked.emit(true);
    this.hasResetDefault.emit({id: _card.id});
    this.modalService.close(this.id);
  }

  updateCard(_card) {
    this.loadingService.display(true);
    let _data = {cardId : _card.id}
    this._http._updatePut(`${this.appConfig.config.api.subscription}/${this.currentSubId}`.replace('[userID]', this.currentUser.id), _data).subscribe(
      data => {
        this.selectedcard = _card;
        this.hasResetDefault.emit({card: _card});
        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )
  }

  
}