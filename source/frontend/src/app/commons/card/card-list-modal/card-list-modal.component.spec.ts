import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardListModalComponent } from './card-list-modal.component';

describe('CardListModalComponent', () => {
  let component: CardListModalComponent;
  let fixture: ComponentFixture<CardListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
