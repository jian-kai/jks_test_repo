import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardModalFormComponent } from './card-modal-form.component';

describe('CardModalFormComponent', () => {
  let component: CardModalFormComponent;
  let fixture: ComponentFixture<CardModalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardModalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardModalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
