import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import {HttpService} from '../../../core/services/http.service';
import {GlobalService} from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import {AlertService} from '../../../core/services/alert.service';
import { CardService } from '../../../core/services/card.service';
import { StripeService } from '../../../core/services/stripe.service';
declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'card-modal-form',
  templateUrl: './card-modal-form.component.html',
  styleUrls: ['./card-modal-form.component.scss']
})
export class CardModalFormComponent extends ModalComponent {
  countryStates: Array<any>;
  public cardForm;
  public isLoading: boolean = true;
  private currentUser: any;
  resError: any;
  public action: string = 'add';
  public cardType: string;
  public Stripe: any;
  public isDefault: boolean = true;
  public enableSetDefault: boolean;
  private card: any;
  private currentCountryId: any;
  @Output() hasChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('cardNumberOnModal') cardNumberOnModal: ElementRef;
  @Input() email: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    private loadingService: LoadingService,
    private cardService: CardService,
    public stripeService: StripeService,
    private alertService: AlertService) {

    super(modalService, el);
    this.id = 'card-modal-form';
    this.cardForm = this.cardService.initCard();
    this.cardForm.removeControl("cardName");
    // this.cardForm.patchValue({callingCode: '+' + this.storage.getCountry().callingCode});
    this.cardForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  setInitData(options) {
    // get current country
    this.currentCountryId = options.CountryId ? options.CountryId : this.storage.getCountry().id;

    this.isDefault = false;
    if (options.enableSetDefault) {
      this.enableSetDefault = options.enableSetDefault;
    }
    this.modalService.reset(this.id);
    if (options && options.id) {
      this.action = 'edit';
      this.cardForm.patchValue(options);
      if(options.expiredMonth.length < 2) {
        options.expiredMonth = '0' + options.expiredMonth;
      }
      this.cardForm.controls.expiry.setValue(options.expiredMonth + ' / ' + options.expiredYear.substr(2,2));
      this.card = options;
    } else {
      this.action = 'add';
    }
    
    this.Stripe = this.stripeService.getInstance();
    this.resError = null;

    jQuery(function($) {
      jQuery('[data-stripe="number"]').payment('formatCardNumber');
      jQuery('[data-stripe="exp"]').payment('formatCardExpiry');
      jQuery('[data-stripe="cvc"]').payment('formatCardCVC');
    });

    this.cardForm.controls.cardNumber.valueChanges.subscribe(data => {
      if (data) {
        this.changeCardNumber();
      } else {
        this.cardType = '';
      }
    })
  }

  actionSubmit(_data: any, _action: string){
    if (_action === 'edit') {
      this.doEditCard(_data);
    } else {
      this.doAddCard(_data);
    }
  }

  // do add new card
  doAddCard(data) {
    this.loadingService.display(true);
    this.createToken()
      .then(stripeToken => this.createCustomer(stripeToken))
      .then(customer => {
        this.hasChange.emit({status: true});
        this.close();
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      });
  }

  // do add new address
  doEditCard(data) {
    this.loadingService.display(true);
    this.createToken()
      .then(stripeToken => this.createCustomerEdit(stripeToken, data))
      .then(customer => {
        data.id = this.card.id;
        this.hasChange.emit({status: true, data: data});
        this.close();
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      });
    // let curUser = this.storage.getCurrentUserItem();
    // if (curUser && this.cardForm.valid) {
    //   this.loadingService.display(true);
    //   data.CountryId = this.storage.getCountry().id;
    //   this._http._update(`${this.appConfig.config.api.delivery_address}/${data.id}`.replace('[userID]', curUser.id), data)
    //   .subscribe(
    //     res => {
    //       if (res.ok) {
    //         this.hasChange.emit(true);
    //         this.loadingService.display(false);
    //         this.resError = null;
    //       }
    //     },
    //     error => {
    //       this.loadingService.display(false);
    //       this.resError = {
    //         type: 'error',
    //         message: JSON.parse(error).message
    //       };
    //     }
    //   )
    // }
  }

   // create stripe token
  createToken() {
    let $form = this.element.find('.cardForm');
    return new Promise((resolve, reject) => {
      this.Stripe.card.createToken($form, (status, response) => {
        if(response.error) {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: response.error.message
          }
          // this.alertService.error([{'ok':false, 'message': response.error.message}]);
          reject(response.error.message);
        } else {
          this.resError = null;
          resolve(response.id);
        }
      });
    });
  }

  // create stripe customer
  createCustomer(stripeToken) {
    if(this.storage.getCurrentUser()) { // For case signed in
      return new Promise((resolve, reject) => {
        let valueDefault: boolean = this.isDefault ? this.isDefault : false;
        this._http._create(`${this.appConfig.config.api.cards}?CountryId=${this.currentCountryId}`.replace('[userID]', this.storage.getCurrentUser().id), {stripeToken, isDefault: valueDefault, CountryId: this.currentCountryId}).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            reject(error);
          }
        );
      });
    } else { // For case checkout by guest
      return new Promise((resolve, reject) => {
        this._http._create(`${this.appConfig.config.api.users}/cards`, {stripeToken, type: 'website'}).subscribe(
          data => {
            resolve(data);
            if (data) {
              this.storage.setCardByGuest(data);
            }
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            reject(error);
          }
        );
      });
    }
  }

  // create stripe customer for edit
  createCustomerEdit(stripeToken, data) {
    if(this.storage.getCurrentUser()) { // For case signed in
      return new Promise((resolve, reject) => {
        let valueDefault: boolean = this.isDefault ? this.isDefault : false;
        this._http._updatePut(`${this.appConfig.config.api.cards}/${this.card.id}`.replace('[userID]', this.storage.getCurrentUser().id), {stripeToken, isDefault: valueDefault, CountryId: this.currentCountryId}).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: JSON.parse(error).message
            };
            reject(error);
          }
        );
      });
    } else { // For case checkout by guest
      if (this.email) {
        return new Promise((resolve, reject) => {
          this._http._updatePut(`${this.appConfig.config.api.users}/cards/${this.card.id}`, {stripeToken, email: this.email, type: 'website'}).subscribe(
            res => {
              resolve(res);
              if (res.ok) {
                this.storage.setCardByGuest(res.data);
              }
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
              reject(error);
            }
          );
        });
      }
    }
  }

  backToList() {
    this.modalService.close(this.id)
    this.modalService.open('card-list-modal');
  }

  // onchange card number
  changeCardNumber() {
    this.cardType = this.globalService.getCardType(this.cardForm.controls['cardNumber'].value).toUpperCase();
  }

    // auto spacing for card number
  autoSpacing() {
    let  value = $('#cardNumberOnModal').val().replace(/\s/g,'');
    let  v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    let  matches = v.match(/\d{4,16}/g);
    let  match = matches && matches[0] || ''
    let  parts = []

    for (let i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }
    let output: any;
    if (parts.length) {
        output = parts.join(' ');
    } else {
        output = value;
    }

    $('#cardNumberOnModal').val(output);
  }


  changeDefault(event) {
    if(!event.target.checked) {
      this.isDefault = false;
    } else {
      this.isDefault = true;
    }
  }

}