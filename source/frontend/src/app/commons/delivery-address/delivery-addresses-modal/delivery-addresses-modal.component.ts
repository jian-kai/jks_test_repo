import { Component, OnInit, Input, Output, ElementRef, Inject, AfterViewInit, ViewChild, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import {HttpService} from '../../../core/services/http.service';
import {GlobalService} from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import {AlertService} from '../../../core/services/alert.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { DialogService } from '../../../core/services/dialog.service';

declare var jQuery: any;
import $ from "jquery";

@Component({
  selector: 'delivery-addresses-modal',
  templateUrl: './delivery-addresses-modal.component.html',
  styleUrls: ['./delivery-addresses-modal.component.scss']
})
export class DeliveryAddressesModalComponent extends ModalComponent {
  countryStates: Array<any>;
  public deliveryAddressForm;
  public isLoading: boolean = true;
  private currentUser: any;
  addressType: string;
  resError: any;
  public deliveryAddressList: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Output() hasResetDefault: EventEmitter<object> = new EventEmitter<any>();
  @Output() hasRemove: EventEmitter<object> = new EventEmitter<any>();
  @Input() selectedShippingAddress: any;
  @Input() selectedBillingAddress: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    private loadingService: LoadingService,
    private deliveryAddressService: DeliveryAddressService,
    private _dialogService: DialogService,
    private alertService: AlertService) {

    super(modalService, el);
    this.id = 'delivery-addresses-modal';
    this.deliveryAddressForm = this.deliveryAddressService.initAddress();
    this.deliveryAddressForm.patchValue({callingCode: '+' + this.storage.getCountry().callingCode});
    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  setInitData(_data: any) {
    this.getDeliveryAddressList();
    this.getCountryStates()
    // this.deliveryAddressForm.reset();
    this.addressType = _data.addressType ? _data.addressType : '';
    this.resError = null;
    // jQuery(".delivery-address-modal").scrollbar();
    // $(window).resize(function() {
    //     jQuery(".delivery-address-modal").scrollbar();
    // });
  }

  // get country states
  getCountryStates() {
    this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
      data => {
        this.countryStates = data;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // get delivery address list
  getDeliveryAddressList(fromAction?: string, _addressType?: string , dataChange?: any) {
    if(this.storage.getCurrentUser()) {
      this.deliveryAddressList = [];
      this._http._getList(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
        data => {
          if(data) {
            this.deliveryAddressList = data;
            let _dataRest = this.deliveryAddressList.length > 0 ? this.deliveryAddressList[0] : null;
            if (fromAction && (fromAction === 'remove')) {
              this.deliveryAddressService.setDeliveryAddressAction({action: 'remove', dataReset: _dataRest, addressType: _addressType})
            }
            if (fromAction && fromAction === 'add') {}
            if (fromAction && fromAction === 'change') {
              _dataRest = null;
              if (this.deliveryAddressList.length > 0 && dataChange) {
                let _dataRest = this.deliveryAddressList.filter(address => address.id === dataChange.id)[0];
              }        
              this.deliveryAddressService.setDeliveryAddressAction({action: 'reset', dataReset: _dataRest, addressType: _addressType})
            }
          }
          if (data && this.deliveryAddressList.length <= 0) {
            this.modalService.close(this.id);
          }
        },
        error => {
          // console.log(error.error)
        }
      )
    } else {
      this.deliveryAddressList = this.storage.getDeliveryAddressByGuest();
    }
  }

  editAddress(_address: any) {
    _address.type = this.addressType;
    this.modalService.reset('delivery-address-modal-form');
    this.modalService.open('delivery-address-modal-form', _address);
    // this.addressType = '';
    this.resError = null;
  }

  // show form add new addresss
  addNewAddress() {
    this.modalService.reset('delivery-address-modal-form');
    this.modalService.open('delivery-address-modal-form', {reGetDB: true, type: this.addressType});
  }

  // delete address
  deleteAddress(address, event) {
    this._dialogService.confirm().afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        event.stopPropagation();
        if(this.storage.getCurrentUser()) {
          this._http._delete(`${this.appConfig.config.api.delivery_address}/${address.id}`.replace('[userID]', this.storage.getCurrentUser().id)).subscribe(
            data => {
              this.hasChange.emit(true);
              this.getDeliveryAddressList();
              this.deliveryAddressService.setDeliveryAddressAction({action: 'remove', data: address, addressType: this.addressType});
              let selectedAddress: any;
              if (this.addressType === 'shipping') {
                selectedAddress = this.selectedShippingAddress;
              }
              if (this.addressType === 'billing') {
                selectedAddress = this.selectedBillingAddress;
              }
              if (selectedAddress.id === address.id) {
                this.getDeliveryAddressList('remove', this.addressType);
                // this.hasRemove.emit({id: address.id, addressType: this.addressType});
              } else {
                this.getDeliveryAddressList();
              }
              
              this.hasRemove.emit({id: address.id, addressType: this.addressType});
              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          )
        } else {
          this._http._delete(`${this.appConfig.config.api.users}/deliveries/${address.id}`).subscribe(
            data => {
              this.hasChange.emit(true);
              this.storage.removeDeliveryAddressByGuest(address);
              this.getDeliveryAddressList();
              this.deliveryAddressService.setDeliveryAddressAction({action: 'remove', data: address, addressType: this.addressType});
              this.loadingService.display(false);
            },
            error => {
              this.loadingService.display(false);
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          )
        }
      }
    }));
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

    // when user edited address
  hasChangeAddress(status: boolean) {
    this.hasChange.emit(true);
    this.modalService.close('delivery-address-modal-form')
    this.modalService.open(this.id);
    this.getDeliveryAddressList();
  }

  // select address as shipping|billing
  selectAddress(_address, _addressType: string) {
    this.hasResetDefault.emit({id: _address.id, addressType: _addressType});
    this.modalService.close(this.id);
  }

}