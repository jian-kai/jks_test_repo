import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAddressesModalComponent } from './delivery-addresses-modal.component';

describe('DeliveryAddressesModalComponent', () => {
  let component: DeliveryAddressesModalComponent;
  let fixture: ComponentFixture<DeliveryAddressesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryAddressesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryAddressesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
