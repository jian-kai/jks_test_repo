import { Component, ElementRef, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { HttpService } from '../../../core/services/http.service';
import { GlobalService } from '../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AlertService } from '../../../core/services/alert.service';
import { DeliveryAddressService } from '../../../core/services/delivery-address.service';
import { CountriesService } from '../../../core/services/countries.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'delivery-address-modal-form',
  templateUrl: './delivery-address-modal-form.component.html',
  styleUrls: ['./delivery-address-modal-form.component.scss']
})
export class DeliveryAddressModalFormComponent extends ModalComponent {
  countryStates: Array<any>;
  public deliveryAddressForm;
  public isLoading: boolean = true;
  private currentUser: any;
  public addressType: string = 'shipping';
  resError: any;
  public action: string = 'add';
  public isDefault: boolean = true;
  public enableSetDefault: boolean;
  public callingCodes: any;
  public i18nDialog: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    public translate: TranslateService,
    private loadingService: LoadingService,
    private deliveryAddressService: DeliveryAddressService,
    private countriesService: CountriesService,
    private alertService: AlertService) {

    super(modalService, el);
    this.id = 'delivery-address-modal-form';
    this.deliveryAddressForm = this.deliveryAddressService.initAddress();
    // this.deliveryAddressForm.patchValue({callingCode: '+' + this.storage.getCountry().callingCode});
    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
  }

  setInitData(_addresss) {
    this.translate.get('msg').subscribe(res => {
      this.i18nDialog = res;
    });
    // this.callingCodes = this.globalService.getCountryCodes();

    let countries = this.countriesService.getCountries();
    this.callingCodes = [];
    countries.forEach(country => {
      this.callingCodes.push(
        {
          'code': country.code,
          'dial_code': '+' + country.callingCode,
          'name': country.name
        }
      );
    });

    this.deliveryAddressForm = this.deliveryAddressService.initAddress();
    this.isDefault = false;
    this.addressType = _addresss.type ? _addresss.type : 'shipping';
    if (_addresss.enableSetDefault) {
      this.enableSetDefault = _addresss.enableSetDefault;
    }
    // this.modalService.reset(this.id);
    this.deliveryAddressForm.patchValue({ callingCode: '+' + this.storage.getCountry().callingCode });
    this.deliveryAddressForm.addControl('isDefault', new FormControl());
    if (_addresss && _addresss.id) {
      this.action = 'edit';
      this.deliveryAddressForm.patchValue(_addresss);
      let curUser = this.storage.getCurrentUserItem();
      if (curUser) {
        if (this.addressType === 'shipping' && _addresss.id == curUser.defaultShipping) {
          this.isDefault = true;
        }
        if (this.addressType === 'billing' && _addresss.id == curUser.defaultBilling) {
          this.isDefault = true;
        }
      }
      this.deliveryAddressForm.controls.isDefault.setValue(this.isDefault);
    } else {
      this.action = 'add';
      if(this.storage.getCurrentUserItem()) {
        this.deliveryAddressForm.patchValue({ firstName: this.storage.getCurrentUserItem().firstName, lastName: this.storage.getCurrentUserItem().lastName });
      }
    }

    this.getCountryStates()
    
    // setTimeout(()=>{
    //   if(this.countryStates) {
    //     if(this.storage.getCountry().code === 'MYS') {
    //       this.deliveryAddressForm.patchValue({state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name});
    //     }
    //     if(this.storage.getCountry().code === 'SGP') {
    //       this.deliveryAddressForm.patchValue({state: this.countryStates.filter(country => country.name == 'Singapore')[0].name});
    //     }
    //   }
    // },300);

    this.resError = null;
  }

  // get country states
  getCountryStates() {
    this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
      data => {
        this.countryStates = data;
        // console.log('this.countryStates', this.countryStates);
        if(this.countryStates) {
          // if(this.storage.getCountry().code === 'MYS') {
          //   this.deliveryAddressForm.patchValue({state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name});
          // }
          // if(this.storage.getCountry().code === 'SGP') {
          //   this.deliveryAddressForm.patchValue({state: this.countryStates.filter(country => country.name == 'Singapore')[0].name});
          // }
          let state = this.countryStates.filter(state => state.isDefault)[0];
          state = state ? state : this.countryStates[0];
          this.deliveryAddressForm.patchValue({state: state.name});
        }
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  actionSubmit(_data: any, _action: string) {
    if (_action === 'edit') {
      this.doEditDeliveryAddress(_data);
    } else {
      this.doAddDeliveryAddress(_data);
    }
  }

  // do add new address
  doAddDeliveryAddress(data) {
    this.resError = null;
    this.loadingService.display(true);
    let curUser = this.storage.getCurrentUser();

    if (curUser) {
      if (this.enableSetDefault) {
        if (this.addressType === 'shipping' && this.isDefault) {
          data.defaultShipping = true;
        }
        if (this.addressType === 'billing' && this.isDefault) {
          data.defaultBilling = true;
        }
        data.addressType = this.addressType;
      }
      data.CountryId = this.storage.getCountry().id;
      data.contactNumber = data.callingCode + ' ' + data.contactNumber;
      this._http._create(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', curUser.id), data)
        .subscribe(
        res => {
          if (res.ok) {
            this.hasChange.emit(true);
            this.close();
            this.loadingService.display(false);
            if (this.enableSetDefault) {
              if (data.addressType === 'shipping' && data.defaultShipping) {
                this.storage.resetDefaultShipping(res.data[0].id);
              }
              if (data.addressType === 'billing' && data.defaultBilling) {
                this.storage.resetDefaultBilling(res.data[0].id);
              }
            }
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
        )
    } else { // For case checkout by guest
      this._http._create(`${this.appConfig.config.api.users}/deliveries`, data)
        .subscribe(
        res => {
          if (res.ok) {
            res.data.forEach(address => {
              this.storage.setDeliveryAddressByGuest(address);
            });
            this.hasChange.emit(true);
            this.close();
            this.loadingService.display(false);
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
        )
    }
  }

  // do add new address
  doEditDeliveryAddress(data) {
    let curUser = this.storage.getCurrentUser();
    if (curUser && this.deliveryAddressForm.valid) {
      this.loadingService.display(true);
      data.CountryId = this.storage.getCountry().id;
      if (this.enableSetDefault) {
        if (this.addressType === 'shipping') {
          if (this.isDefault) {
            data.defaultShipping = true;
          } else {
            data.defaultShipping = false;
          }
        }
        if (this.addressType === 'billing') {
          if (this.isDefault) {
            data.defaultBilling = true;
          } else {
            data.defaultBilling = false;
          }
        }
        data.addressType = this.addressType;
      }
      this._http._update(`${this.appConfig.config.api.delivery_address}/${data.id}`.replace('[userID]', curUser.id), data)
        .subscribe(
        res => {
          if (res.ok) {
            if (this.enableSetDefault) {
              if (data.addressType === 'shipping') {
                if (data.defaultShipping && data.id != curUser.defaultShipping)
                  this.storage.resetDefaultShipping(data.id);
                else if (!data.defaultShipping && data.id == curUser.defaultShipping)
                  this.storage.resetDefaultShipping(0);
              }
              if (data.addressType === 'billing') {
                if (data.defaultBilling && data.id != curUser.defaultBilling)
                  this.storage.resetDefaultBilling(data.id);
                else if (!data.defaultBilling && data.id == curUser.defaultBilling)
                  this.storage.resetDefaultBilling(0);
              }
            }
            this.hasChange.emit(true);
            this.loadingService.display(false);
            this.resError = null;
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
        )
    } else if (!curUser && this.deliveryAddressForm.valid) {
      this._http._updatePut(`${this.appConfig.config.api.delivery_address_byGuest}/${data.id}`, data)
      .subscribe(
      res => {
        if (res.ok) {

            // if (data.addressType === 'shipping') {
              
            // }
            // if (data.addressType === 'billing') {

            // }
            this.storage.setDeliveryAddressByGuest(data);

          this.hasChange.emit(true);
          this.loadingService.display(false);
          this.resError = null;
        }
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
      )
    }
  }

  backToList() {
    this.modalService.close(this.id)
    this.modalService.open('delivery-addresses-modal');
  }

  changeDefault(event) {
    if (!event.target.checked) {
      this.isDefault = false;
    } else {
      this.isDefault = true;
    }
  }

}
