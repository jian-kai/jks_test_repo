import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryAddressModalFormComponent } from './delivery-address-modal-form.component';

describe('DeliveryAddressModalFormComponent', () => {
  let component: DeliveryAddressModalFormComponent;
  let fixture: ComponentFixture<DeliveryAddressModalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryAddressModalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryAddressModalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
