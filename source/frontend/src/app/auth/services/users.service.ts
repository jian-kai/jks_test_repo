import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { StorageService } from '../../core/services/storage.service';
import { RequestService } from '../../core/services/request.service';
import { AppConfig } from 'app/config/app.config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class UsersService {
  @Output() justLogout: EventEmitter<Object> = new EventEmitter();
  @Output() justLogin: EventEmitter<Object> = new EventEmitter();
  @Output() showLoginStep: EventEmitter<Object> = new EventEmitter();
  @Output() resendActiveEmail: EventEmitter<Object> = new EventEmitter();
  _loggedIn = new BehaviorSubject(false);


  constructor(private http: Http, public storage: StorageService, private request: RequestService, public appConfig: AppConfig) {
    let user = this.storage.getCurrentUser();
    if (!!this.storage.getAuthToken() && user && user.isActive) {
      this.setLogedin();
    }
  }

  setLogedin() {
    this._loggedIn.next(true);
    this.justLogin.emit(true);
  }

  applyLogoutEvent() {
    this.justLogout.emit(true);
  }

  applyShowLoginStep() {
    this.showLoginStep.emit(true);
  }

  applyResendActiveEmail(_email: string) {
    this.resendActiveEmail.emit(_email);
  }

  login(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.login}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  activeLogin(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.activeLogin}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  // register
  _register(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.register}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  logout() {
    this.storage.removeAuthToken();
    this.storage.removeCurrentUser();
    this._loggedIn.next(false);
    this.justLogout.emit(true);
    // window.location.reload();
  }

  isLoggedIn() {
    return this._loggedIn.getValue();
  }

  getLoggedIn() {
    return this._loggedIn;
  }
  // response data successfully
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  // handle if have errors
  private handleErrorObservable(error: Response | any) {
    if (error.status === 400) {
      return Observable.throw(error._body);
    }
    return Observable.throw(error.json());
  }

}
