import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { UsersService } from '../../auth/services/users.service';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthService implements CanActivate {
	public code: string;
	constructor(private location: Location, private users: UsersService, private router: Router,) {
		let params = new URLSearchParams(this.location.path(false).split('?')[1]);
		this.code = params.get('code');
		if (this.code) {
			// console.log(this.code);
		}

	}

	public auth(_provider: string, authConfig: any) {
		localStorage.setItem('authConfig', authConfig);
		localStorage.setItem('provider', _provider);
		if (_provider === 'facebook') {
			window.location.href = 'https://www.facebook.com/v2.8/dialog/oauth?client_id=' + authConfig.clientId + '&redirect_uri=' + authConfig.redirectURI + '&scope=email';
		}
	}
	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		if (this.users.isLoggedIn()) {
			return this.users.isLoggedIn();
		}

		// not logged in so redirect to login page with the return url
		this.router.navigate(['/login'], { queryParams: {} });
		return false;
	}

}