import { Component, OnInit, Input, Output, EventEmitter,  ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { numberValidator } from '../../../core/directives/number-validator.directive';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { HttpService } from '../../../core/services/http.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AuthService } from '../../../auth/services/auth.service';
import { SimpleSubStepComponent } from '../../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../../core/animations/slide-in-out-checkout.animation';
import { CountriesService } from '../../../core/services/countries.service';
import { CartService } from '../../../core/services/cart.service';
import { PlanService } from '../../../core/services/plan.service';
import { GlobalService } from '../../../core/services/global.service';
import { NotificationService } from '../../../core/services/notification.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  // styleUrls: ['../../../product/components/checkout/sign-in/sign-in.component.scss'],
  styleUrls: ['./login.component.scss'],
  animations: [slideInOutCheckoutAnimation]
})
export class LoginComponent implements OnInit {
  // @Input() emailForm: FormGroup;
  // @Input() isComfirmPassword: boolean;
  @Output() notifyStep1Status: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @ViewChild('signinRadio') signinRadio: ElementRef;
  activeEmail: string;
  allValid: boolean = false;
  resError: any;
  fbResError: any;
  stage: string; // signin | password | signup | sendEmail | emailActive
  hasToken: boolean = false;
  token: string;
  isActive: boolean = true;
  nextPage: string;
  checkingActiveStatus: boolean = false;
  public isComfirmPassword: boolean = false;
  public isLoading: boolean = true;
  public emailForm: FormGroup;
  public passwordForm: FormGroup;
  public signUpForm: FormGroup;
  public forgotPasswordForm: FormGroup;
  public userName: string = ""; // to display on password input step
  public emailFormType: string = 'signup';
  public planGroups: any = [];
  constructor(
    public builder: FormBuilder,
    public storage: StorageService,
    public usersService: UsersService,
    public alertService: AlertService,
    public loadingService: LoadingService,
    public translate: TranslateService,
    public _http: HttpService,
    public appConfig: AppConfig,
    public countriesService: CountriesService,
    public authService: AuthService,
    public router: Router,
    public cartService: CartService,
    public route: ActivatedRoute,
    public planService: PlanService,
    public location: Location,
    public globalService: GlobalService,
    public notificationService: NotificationService,
  ) {
    if(!this.storage.getCountry().isWebEcommerce) {
      this.router.navigate(['/']);
    }
    
    this.emailForm = builder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.passwordForm = builder.group({
      password: ['', Validators.required]
    });
    this.signUpForm = builder.group({
      email: ['', Validators.required],
      // firstName: ['', Validators.required],
      // lastName: ['', Validators.required],
      password: ['', Validators.required],
      // confirmPassword: ['', Validators.required],
      // dayInput: ['', numberValidator({ min: 1, max: 31 })],
      // monthInput: ['', numberValidator({ min: 1, max: 12 })],
      // yearInput: ['', numberValidator({ min: new Date().getFullYear() - 100, max: new Date().getFullYear() })],
      // gender: [''],
      hasReceiveOffer: [true]
    });
    this.forgotPasswordForm = builder.group({
      email: ['', Validators.required],
    });
    
    this.emailForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
    
    this.passwordForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
    
    this.signUpForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    // let watchingInterval = setInterval(() => {
    //   if(this.storage.getCurrentUserItem() && this.router.url === '/login') {
    //     clearInterval(watchingInterval);
    //     this.router.navigate(['/']);
    //   }
    // }, 1000);
  }

  setActive() {
    // reset form
    this.emailForm.reset();
    this.passwordForm.reset();
    this.signUpForm.reset();
    // remove authen info
    this.storage.removeAuthToken();
    this.storage.removeCurrentUser();

    this.stage = 'signup';
    this.resError = null;
  }

  validate() {
    if (this.usersService._loggedIn) {
      return {
        allValid: true
      }
    } else {
      return {
        allValid: this.allValid
      }
    }
  }

  ngOnInit() {
    this.stage = 'signup';
    this.resError = null;

    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.nextPage = decodeURIComponent(params.get('nextPage'));
    if (params && params.get('email')) {
      let email = decodeURIComponent(params.get('email'));
      if (this.globalService.validateEmail(email)) {
        this.signUpForm.controls.email.setValue(email);
        this.stage = 'signup';
        this.emailFormType = 'signup';
      } else {
        this.signUpForm.controls.email.setValue('');
        this.emailFormType = 'signin';
      }
    } else if(params.get('activeEmail')) {
      this.activeEmail = decodeURIComponent(params.get('activeEmail'));
      this.stage = "sendEmail";
      this.sendEmailActive(params.get('activeEmail'));
    }

    // check token
    this.route.params.subscribe(params => {
      if(params.token) {
        this.hasToken = true;
        this.token = params.token;

        this.activeUser();
      }
    });

    this.usersService.showLoginStep.subscribe(status => {
      if (status) {
        this.stage = 'signup';
        if (this.signinRadio && this.signinRadio.nativeElement) {
          $(this.signinRadio.nativeElement).trigger('click')
        }
      }
    })

    this.usersService.resendActiveEmail.subscribe(email => {
      if (email && email !== '') {
        this.stage = "sendEmail";
        this.activeEmail = email;
        this.sendEmailActive(email);
      }
    })
  }

  activeUser() {
    // send request to active user
    this.loadingService.display(true);
    let credentials = { token: this.token };
    this.usersService.activeLogin(credentials).subscribe(
      data => {
        // store token into localstore
        this.storage.setAuthToken(data.token);
        this.storage.setCurrentUser(data.user);
        this.usersService.setLogedin();

        // redirect user to subscription page
        if(this.nextPage && this.nextPage !== 'null') {
          // redirect user to subscription page
          this.router.navigate([this.nextPage]);
        } else {
          this.router.navigate(['/user/shave-plans']);
        }

        // add item into cre
        data.user.Cart.cartDetails.forEach(item => {
          this.cartService.addItem(item);
        });

        // sync cart item to server
        this.cartService.updateCart();

        this.loadingService.display(false);
        this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        // console.log(error)
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  checkEmail() {
    this.loadingService.display(true);
    if (this.emailForm.valid) {
      // check email is exist or not
      this._http._create(this.appConfig.config.api.checkEmail, { email: this.emailForm.value.email })
        .subscribe(res => {
          if (!res || res.isExisted === false) {
            // change step to sign up
            // this.stage = 'signup';
            this.doSignUp();
            this.resError = null;
          } else if (res && res.user.isActive) {
            // change step to input password
            this.userName = res.user.firstName;
            // this.stage = 'password';
            this.doLogin();
            this.resError = null;
          } else {
            // change step to send email
            // this.watchingActiveStatus(null);
            this.stage = 'sendEmail';
            this.activeEmail = this.emailForm.value.email;
            this.resError = null;
          }
        });
    }
    this.loadingService.display(false);
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  doSignUp() {
    this.loadingService.display(true);
    let credentials = this.signUpForm.value;
    credentials.email = this.signUpForm.value.email;
    credentials.defaultLanguage = this.translate.currentLang ? this.translate.currentLang : this.translate.defaultLang;
    credentials.countryCode = this.storage.getCountry().code;
    // if (credentials.dayInput) {
    //   credentials.birthday = credentials.yearInput + "/" + credentials.monthInput + "/" + credentials.dayInput;
    // }
    credentials.utmData = {
      source: this.storage.getGtmSource(),
      medium: this.storage.getGtmMedium(),
      campaign: this.storage.getGtmCampaign(),
      content: this.storage.getGtmContent(),
      term: this.storage.getGtmTerm()
    };
    this._http._create(`${this.appConfig.config.api.register}`, credentials).subscribe(
      data => {
        // change step to send email
        // this.watchingActiveStatus(null);
        this.stage = 'sendEmail';
        this.activeEmail = credentials.email;
        this.resError = null;
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // handle confirm password to the database
  doLogin() {
    this.loadingService.display(true);
    this.isActive = true;
    let credentials = { email: this.emailForm.value.email, password: this.emailForm.value.password };
    this.usersService.login(credentials).subscribe(
      data => {
        if(data.token) {
          // store token into localstore
          this.storage.setAuthToken(data.token);
          this.storage.setCurrentUser(data.user);
          this.usersService.setLogedin();
          if(this.storage.getCheckLoginForCustomPlan()) {
            this.storage.clearCheckLoginForCustomPlan();
            this.router.navigate(['/checkout']);
          } else {
            this.router.navigate(['/user/shave-plans']);
          }

          // add item into cre
          data.user.Cart.cartDetails.forEach(item => {
            if (item.plan) {
              if (this.planGroups.length === 0) {
                this.planService.getAllPlans().then(result=> {
                  this.planGroups = this.planService.groupPlan(result);
                  item.groupName = this.planService.getGroupName(this.planGroups, item);
                  this.cartService.addItem(item);
                })
              } else {
                item.groupName = this.planService.getGroupName(this.planGroups, item);
                this.cartService.addItem(item);           
              }
            } else {
              this.cartService.addItem(item);
            }
            
          });

          // sync cart item to server
          this.cartService.updateCart();

          // set flag when synced cart
          this.storage.setFlagSyncCart();
        } else if(!data.ok && data.code === 'inactive') {
          this.notificationService.addNotificationRemindUser(this.emailForm.value.email);  
          setTimeout(() => this.notificationService.addNotificationRemindUser(null), 30000);
        }
        this.loadingService.display(false);
        this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        // console.log(error)
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  backToSignIn(event) {
    this.stage = 'signin';
    this.resError = null;
    this.passwordForm.reset();
  }

  goToStep2() {
    this.notifyStep1Status.emit(true);
    this.allValid = true;
    this.resError = null;
    // this.router.navigate(['/']);
    this.router.navigate(['/user/shave-plans']);
  }

  // forgot password
  forgotPassword() {
    this.loadingService.display(true);
    let credentials = { email: this.forgotPasswordForm.value.email };
    if (credentials) {
      this._http._create(`${this.appConfig.config.api.reset_password}`, credentials).subscribe(
        data => {
          this.stage = 'forgotPassword_2';
          this.resError = null;
          // console.log('data', data);
          this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
      );
    }
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  // send email again
  sendEmailAgain() {
    this.loadingService.display(true);
    this._http._create(this.appConfig.config.api.resendActiveEmail, { email: this.activeEmail })
      .subscribe(res => {
        // TODO update UI
      });
    // this.watchingActiveStatus(null);
    this.loadingService.display(false);
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // send email again
  sendEmailActive(_email: string) {
    this.loadingService.display(true);
    this._http._create(this.appConfig.config.api.resendActiveEmail, { email: _email })
      .subscribe(res => {
        // TODO update UI
      });
    // this.watchingActiveStatus(null);
    this.loadingService.display(false);
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getFBLoginStatus() {
    return new Promise((resolve, reject) => {
      window['FB'].getLoginStatus(response => {
        resolve(response);
      });
    });
  }

  doLoginWithFB() {
    return new Promise((resolve, reject) => {
      window['FB'].login(function(response) {
        if (response.authResponse) {
          resolve(response);
        } else {
          reject(response);
        }
      }, {scope: 'email', auth_type: 'rerequest'});
    });
  }

  getFBUserInfo(res) {
    return new Promise((resole, reject) => {
      window['FB'].api(`/${res.authResponse.userID}`, { locale: 'en_US', fields: 'name, email, gender, birthday' }, function(response) {
        if(!response || response.error) {
          reject(response);
        } else {
          resole(response);
        }
      });
    });
  }
  
  facebookLogin() {
    this.resError = null;
    this.fbResError = null;
    this.getFBLoginStatus()
      .then(res => {
        if(res['status'] === 'connected' && res['email']) {
          return res;
        } else {
          return this.doLoginWithFB();
        }
      })
      .then(res => this.getFBUserInfo(res))
      .then(res => {
        let firstName = (res['name'].indexOf(' ') > -1) ? res['name'].split(' ')[0] : res['name'];
        let lastName = (res['name'].indexOf(' ') > -1) ? res['name'].split(' ')[1] : '';
        let user = {
          email: res['email'],
          firstName,
          lastName,
          socialId: res['id'],
          gender: res['gender'],
          birthday: res['birthday'],
          utmData: {
            source: this.storage.getGtmSource(),
            medium: this.storage.getGtmMedium(),
            campaign: this.storage.getGtmCampaign(),
            content: this.storage.getGtmContent(),
            term: this.storage.getGtmTerm()
          }
        }

        if(user.email && user.email != '') {
          // submit user object
          this._http._create(this.appConfig.config.api.signinFB, user).subscribe(
            data => {
              // store token into localstore
              this.storage.setAuthToken(data.token);
              this.storage.setCurrentUser(data.user);
              this.usersService.setLogedin();
              // this.router.navigate(['/']);
              if(data.user.isActive) {
                this.router.navigate(['/user/shave-plans']);
              } else {
                this.notificationService.addNotificationRemindUser(data.user.email);
                setTimeout(() => this.notificationService.addNotificationRemindUser(null), 30000);
              }
      
              // add item into cre
              data.user.Cart.cartDetails.forEach(item => {
                this.cartService.addItem(item);
              });
      
              // sync cart item to server
              this.cartService.updateCart();
      
              this.loadingService.display(false);
              this.resError = null;
              this.fbResError = null;
            },
            error => {
              this.loadingService.display(false);
              // console.log(error)
              this.resError = {
                type: 'error',
                message: JSON.parse(error).message
              };
            }
          );
        } else {
          this.loadingService.display(false);
          this.fbResError = {
            type: 'error',
            message: 'Your email is required to register an account with Shaves2U'
          };
        }
        // console.log(res);
      })
      .catch(err => {} // console.log(err));
      );
  }

  changeSignInUp(value) {
    this.emailFormType = value;
    this.stage = value;
    this.resError = null;
    if (this.stage === 'signin') {
      this.emailForm = this.builder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      });
    }
    if (this.stage === 'signup') {
      this.signUpForm = this.builder.group({
        email: ['', Validators.required],
        // firstName: ['', Validators.required],
        // lastName: ['', Validators.required],
        password: ['', Validators.required],
        // confirmPassword: ['', Validators.required],
        hasReceiveOffer: [true]
      });
    }
  }

  referForgotPassword() {
    this.stage = 'forgotPassword_1';
    this.resError = null;
  }

  referSignIn() {
    this.stage = 'signin';
    this.resError = null;
    this.emailForm.reset();
  }

}
