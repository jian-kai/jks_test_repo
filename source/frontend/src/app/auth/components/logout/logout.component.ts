import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../../core/services/storage.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    public storage: StorageService,
    private router: Router) {
   }

  ngOnInit() {
    
    // let watchingInterval = setInterval(() => {
    //   if(this.storage.getCurrentUserItem() && this.router.url === '/logout') {
    //     clearInterval(watchingInterval);
    //     this.router.navigate(['/']);
    //   }
    // }, 1000);
  }
  
  redirectToLoginPage() {
    this.router.navigate(['/login']);
  }

}
