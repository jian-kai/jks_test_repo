import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { numberValidator } from '../../../core/directives/number-validator.directive';
import {Router, ActivatedRoute, Params, NavigationCancel } from '@angular/router';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import * as $ from 'jquery';

import { HttpService } from '../../../core/services/http.service';
import { StorageService } from '../../../core/services/storage.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AuthService } from '../../../auth/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { CountriesService } from '../../../core/services/countries.service';
import { UsersService } from '../../../auth/services/users.service';
import { CartService } from '../../../core/services/cart.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../login/login.component.scss'],
  // styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  isActive: any;
  nextPage: any;
  message: any;
  private resetPasswordForm: FormGroup;
  public stage: string; // form | result
  public email: string;
  public token: string;
  public isLoading: boolean = true;
  resError: any;

  constructor(private builder: FormBuilder,
    public storage: StorageService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private _http: HttpService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    private authService: AuthService,
    private router: Router,
    private userService: UsersService,
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    public cartService: CartService,
    private location: Location) {

      this.resetPasswordForm = builder.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
      });

    }

  ngOnInit() {
    if(this.storage.getCurrentUserItem()) {
    this.cartService.clearCart(true);
    this.userService.logout();
    }

    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.email = decodeURIComponent(params.get('email'));
    this.token = params.get('token');
    this.isActive = params.get('isActive');
    this.message = params.get('message');
    this.nextPage = decodeURIComponent(params.get('nextPage'));
    if (!this.email || !this.token) {
      // console.log('Invalid link!')
    }
    if(this.message === 'token_expired') {
      this.stage = 'resend';
    } else {
      this.validateToken();
    }
  }

  validateToken() {
    this.loadingService.display(true);
    let credentials = { token: this.token  };
    this._http._create(`${this.appConfig.config.api.validate_token}`, credentials).subscribe(
      data => {
        this.stage = "form";
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error([error]);
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  activeUser() {
    // send request to active user
    this.loadingService.display(true);
    let credentials = { token: this.token };
    this.usersService.activeLogin(credentials).subscribe(
      data => {
        // store token into localstore
        this.storage.setAuthToken(data.token);
        this.storage.setCurrentUser(data.user);
        this.usersService.setLogedin();

        // redirect user to subscription page
        this.router.navigate([this.nextPage]);

        // add item into cre
        data.user.Cart.cartDetails.forEach(item => {
          this.cartService.addItem(item);
        });

        // sync cart item to server
        this.cartService.updateCart();

        this.loadingService.display(false);
        this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        // console.log(error)
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );
  }

  reSendMail() {
    this.resError = null;
    let credentials = { email: this.email  };
    this._http._create(`${this.appConfig.config.api.reset_password}`, credentials).subscribe(
      data => {
        this.stage = "resend";
      },
      error => {
        this.alertService.error([error]);
      }
    )
  }

  doReset() {
    this.loadingService.display(true);
    // this.stage = 'forgotPassword';
    let credentials = { token: this.token , password : this.resetPasswordForm.value.password };
    this._http._create(`${this.appConfig.config.api.update_password}`, credentials).subscribe(
      data => {
        if (data.ok && !this.isActive) {
          this.stage = "success";
          this.doLogin();
        } else if (data.ok && this.isActive) {
          this.activeUser();
        }
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.alertService.error([error]);
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  doLogin() {
    this.loadingService.display(true);
    let credentials = { email: this.email, password: this.resetPasswordForm.value.password };
    this.usersService.login(credentials).subscribe(
      data => {
        // store token into localstore
        this.storage.setAuthToken(data.token);
        this.storage.setCurrentUser(data.user);
        this.usersService.setLogedin();

        // add item into cre
        data.user.Cart.cartDetails.forEach(item => {
          this.cartService.addItem(item);
        });

        // sync cart item to server
        this.cartService.updateCart();

        this.loadingService.display(false);
        this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        // console.log(error)
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  continue() {
    this.resError = null;
    // this.router.navigate(['/']);
    this.router.navigate(['/user/shave-plans']);
  }

}
