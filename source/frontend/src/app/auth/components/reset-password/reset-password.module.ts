import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { ResetPasswordComponent } from './reset-password.component';
// module
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: ResetPasswordComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [
    ResetPasswordComponent
  ]
})
export class ResetPasswordModule { }
