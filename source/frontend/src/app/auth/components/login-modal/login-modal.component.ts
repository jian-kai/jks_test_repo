import { Component, ElementRef, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { numberValidator } from '../../../core/directives/number-validator.directive';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { HttpService } from '../../../core/services/http.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { AlertService } from '../../../core/services/alert.service';
import { LoadingService } from '../../../core/services/loading.service';
import { AuthService } from '../../../auth/services/auth.service';
import { SimpleSubStepComponent } from '../../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../../core/animations/slide-in-out-checkout.animation';
import { CountriesService } from '../../../core/services/countries.service';
import { CartService } from '../../../core/services/cart.service';
import { PlanService } from '../../../core/services/plan.service';
import { GlobalService } from '../../../core/services/global.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent extends ModalComponent {
  @Output() hasLogin: EventEmitter<any> = new EventEmitter<any>();
  public resError: any;
  public emailForm: FormGroup;
  public planGroups: any = [];
  public isLoading: boolean = true;
  public fromStep: string;
  constructor(modalService: ModalService,
    el: ElementRef,
    private builder: FormBuilder,
    public storage: StorageService,
    private usersService: UsersService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translate: TranslateService,
    private _http: HttpService,
    public appConfig: AppConfig,
    private countriesService: CountriesService,
    private authService: AuthService,
    private router: Router,
    public cartService: CartService,
    private route: ActivatedRoute,
    public planService: PlanService,
    private location: Location,
    public globalService: GlobalService) {
      super(modalService, el);
      this.id = 'app-login-modal';
      this.emailForm = builder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      });
    }

  setInitData(_data: any) {
    this.modalService.reset(this.id);
    if(_data && _data.step === 'delivery-address') {
      this.fromStep = _data.step;
    }
    this.resError = null;
    this.emailForm = this.builder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    if(_data && _data.email) {
      this.emailForm.patchValue({email: _data.email});
    }
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // handle confirm password to the database
  doLogin() {
    this.loadingService.display(true);
    let credentials = { email: this.emailForm.value.email, password: this.emailForm.value.password };
    this.usersService.login(credentials).subscribe(
      data => {
        if (data.token) {
          // store token into localstore
          this.storage.setAuthToken(data.token);
          this.storage.setCurrentUser(data.user);
          this.usersService.setLogedin();
          // this.router.navigate(['/']);

          // add item into cre
          data.user.Cart.cartDetails.forEach(item => {
            if (item.plan) {
              if (this.planGroups.length === 0) {
                this.planService.getAllPlans().then(result=> {
                  this.planGroups = this.planService.groupPlan(result);
                  item.groupName = this.planService.getGroupName(this.planGroups, item);
                  item.plan = item.planCountry;
                  this.cartService.addItem(item);
                })
              } else {
                item.groupName = this.planService.getGroupName(this.planGroups, item);
                item.plan = item.planCountry;
                this.cartService.addItem(item);           
              }
            } else {
              item.product = item.productCountry;
              this.cartService.addItem(item);
            }
            
          });

          // sync cart item to server
          this.cartService.updateCart();

          // set flag when synced cart
          this.storage.setFlagSyncCart();

          this.resError = null;
          this.hasLogin.emit(data);
          this.modalService.close(this.id);
        } else {
          this.resError = {
            type: 'error',
            message: data.message
          }; 
        }

         this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error)
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

}
