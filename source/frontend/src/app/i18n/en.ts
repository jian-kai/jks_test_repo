export const en = {
  menu: {
    products: "Products",
    subscriptions: "Subscriptions",
    articles: "Articles",
    helps: "Helps",
    support: "Support",
    about: "About us",
    login: "Login",
    logout: "Logout",
    register: "Register",
    checkout: "Checkout",
    faq: "FAQ",
    trackOrder: "Track my order",
    subscription_note: "Your subscription is active until",
    user_subscription: "Subscription",
    user_order: "Order",
    user_setting: "Settings",
    user_signout: "Sign Out",
    help: "Help",
    shave_plans: "Shave Plans",
    cart: "Cart",
    shop_all: "Shop all",
    hi: "Hi"
  },
  product: {
    field: {
      product: "Product",
      price: "Price",
      quantity: "Quantity",
      subtotal: "Subtotal",
      choice_product: "Your choice of men's shavers and grooming products"
    }
  },

  // dialog
  dialog: {
    confirm: {
      title: 'Confirm',
      content: 'Are you sure?',
      cancelling_order: 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?',
      change_plan: 'You are updating your shave plan from <font color="#fe5000">{{from}}</font> to <font color="#fe5000">{{to}}</font>. Are you sure?',
      change_payment: 'You are updating your shave plan from <font color="#fe5000">{{from}} billing</font> to <font color="#fe5000">{{to}} billing</font>. Are you sure?',
      notAllowForIsTrial: 'Please be informed your account is not eligible for Shave plan Free Trial',
      // notAllowForIsTrial: 'Oops. It’s seems like you’ve already signup for a free trial. <br>Click <a href="/user/shave-plans"><strong>here</strong></a> to check on your status. If you’ve already cancelled your shave plan but would like to restart, Click <a href="shave-plans/custom-plan"><strong>here</strong></a> to customize your new shave plan.',
      cancelling_trial_subs: 'Are you sure you want to cancel your Shave Plan?',
      trial_subs_cancelled_succ: 'Shave Plan <b>{{name}}</b> have been successfully cancelled',
      trial_request_to_cancel_succ: 'Request to cancel Shave Plan {{name}} have been sent to Customer Service, they will contact you for verification.',
      reverting_trial_subs: 'You are reverting your Shave Plan Free Trial. Are you sure?',
      trial_subs_reverted_succ: 'Request was successful',
      btn_ok: 'OK',
      btn_cancel: 'Cancel'
    },
    collect_email: {
      title:
        "Hang on, we’re still getting this ready.<br>Leave us your email, and we’ll let you know as soon as we’re done.",
      Terms_and_Conditions:
        'I have read and understand the <a class="mobile_TC_layout" href="terms-conditions" target="_blank" style="text-decoration: underline;">Terms and Conditions</a>',
      email_address: "Enter email address:",
      ok: "OK"
    },
    notification_popup: {
      title: 'Coming soon to redefine shaving!',
      message: 'For your chance to win a FREE starter pack during our launch',
      follow: 'Follow us on',
      notAllowForIsTrial: 'Oops. It’s seems like you’ve already signup for a free trial.',
      notAllowForIsTrial2: 'Oops. It’s seems like you’ve already signup for a free trial. Click <a href="/user/shave-plans"><strong>here</strong></a> to check on your status.',
      notAllowForIsTrial3: 'If you’ve already cancelled your shave plan but would like to restart, click <a href="shave-plans/custom-plan"><strong>here</strong></a> to customize your new shave plan.',
    }
    ,
    cancellation_journey: {
      title: 'Will you let us know why you’re cancelling?',
      title1: 'Will you let us know why<br>you’re cancelling?',
      subtitle: 'We’re sorry to hear that you’re unhappy with your subscription. If you have a minute, please tell us why:',
      subtitle1: 'We’re sorry to hear that you’re unhappy with your subscription.',
      subtitle2: 'If you have a minute, please tell us why:',
      other_reason: 'Other reason:',
      button_ok: 'Proceed with Cancellation',
      button_close: 'No, don’t cancel',
      button_ok_ko: 'Confirm',
      button_close_ko: 'I changed my mind'
    }
  },

  // notification
  notification: {
    // logout: 'Signed out successfully.'
    logout: "You have been signed out successfully.",
    remind_user:
      "You account is not activated. Please check your mailbox. Did not receive activation email?",
    resend_active_email_text: "Resend Activation Email"
  },

  // message
  msg: {
    validation: {
      required: "{{value}} is a mandatory field",
      required_tc:
        "Please indicate that you have read and agree to the Terms and Conditions",
      character_string:
        "Contents for this textbox must have at least one Alphabetic character",
      special_characters: "Can't enter special characters",
      select_required: "Please select a value for this field.",
      email: "Please enter a valid email address",
      email_exists: "This email already exists.",
      maxlength:
        "This field only allows max {{value}} characters, please revise accordingly.",
      minlength: "This field can be at least {{value}} characters long",
      maxDigits: "This field can be at most XXX numeric digits",
      minDigits: "This field can be at least {{value}} numeric digits",
      password_format:
        "Password should be at least 6 characters long and include at least one number or symbol.",
      password_format_lenght: "Be at least 6 characters long.",
      password_format_contain: "Include at least one number or symbol.",
      password_not_match:
        "Confirm password field does not match the password field.",
      old_pw_invalid: "Old password do not match",
      birthday_invalid: "Birthday is invalid",
      phone: "Must be a valid 10 digits phone number",
      valid: "Please enter a valid {{value}}.",
      phone_sgp: "Must be a valid {{value}} digits phone number",
      promocode_minprice:
        "You need to spend minimum {{value}} on promo specific product to enjoy this discount.",
      promocode_not_apply: "Promo code not applicable.",
      card_number_length:
        "You need to re-enter your 16 digit credit card number to save latest details",
      date_of_birth_valid: "Please fill in a complete date of birth",
      day_valid: "Please enter a valid day",
      year_valid: "Please enter a valid year",
      day: "Day",
      month: "Month",
      year: "Year",
      gender_valid: "Please select your gender",
      jan: "January",
      feb: "February",
      mar: "March",
      apr: "April",
      may: "May",
      june: "June",
      july: "July",
      aug: "August",
      sep: "September",
      oct: "October",
      nov: "November",
      dec: "December"
    },
    field: {
      email: "Email",
      email_address: "email address",
      phone: "phone number",
      postcode: "postcode",
      password: "Password"
    },
    status: {
      canceled: "Canceled",
      process: "Processing",
      on_hold: "On Hold",
      payment_received: "Payment Received",
      completed: "Completed",
      pending: "Pending",
      delivering: "Delivering",
      loading: "Loading"
    }
  },
  popup: {
    address: {
      heading_edit: {
        shipping: "Edit Shipping Address",
        billing: "Edit Billing Address"
      },
      heading_add: {
        shipping: "Add New Shipping Address",
        billing: "Add New Billing Address"
      }
    }
  },

  // btn

  btn: {
    continue: "CONTINUE",
    back: "BACK",
    save_changes: "Save Changes",
    cancel: "Cancel",
    save: "Save",
    select: "Select",
    revert: "Undo Cancellation",
    update_plan: "UPDATE PLAN"
  },

  //  shave plan page
  plan: {
    content_header:
      "You’re currently building your Shave Plan. Your Trial Kit is free. Just pay shipping at checkout.",
    plan_section: {
      select_plan: "What’s Your Shaver Flavour?",
      select_payment: "Which Payment Plan Would You Prefer?",
      plan_details: "What you’ll get",
      subtitle: "Men’s<br>{{value}}-Blade Cartridges",
      subtitle_Women: "Women’s<br>{{value}}-Blade Cartridges",
      s3_note: "Gentler grooming.",
      s5_note: "Sleeker shaving.",
      totalText: "per annum",
      btn_discount: "Save",
      note:
        'We’ll send you an e-mail before your <font color="#fe5000">first</font> payment is due, so you get the time you need to change or cancel your plan.',
      btn_add_to_cart: "PROCEED WITH FREE TRIAL",
      suggest_payment: "We accept"
    },
    read_faq: "Read our FAQs or get help from the Shaves2U team",
    frequently_asked_questions: "Frequently Asked Questions",
    free_trial: {
      select_blade: "Choose Your Blade",
      refill: {
        heading: "Choose Your Refill Frequency",
        heading_box: "Every"
      },
      not_sure: "NOT SURE?",
      payment_type: {
        heading: "Choose Your Payment Plan",
        description:
          "Billing will only start when your first refill is shipped out.",
        save: "SAVE",
        title: "PAY & DELIVER EVERY {{value}}",
        every: "Every",
        pay_as_you_go: "Pay as you go",
        annual: "Annual",
        save_an: "Save an extra"
      },
      suggest_refill: {
        heading: "Match the right refill frequency to your shaving schedule.",
        frequency_guide: "Frequency guide",
        option_1: {
          heading: "5-7 days a week.",
          note: "Every 2 months",
          note_2: "Shaves 5-7 days per week."
        },
        option_2: {
          heading: "2-4 days a week.",
          note: "Every 3 months",
          note_2: "Shaves 2-4 days per week"
        },
        option_3: {
          heading: "1 day or less per week.",
          note: "Every 4 months",
          note_2: "Shaves 1 day or less per week"
        },
        bottom_note: "1 refill consists of 4 cartridges."
      }
    },
    custom_plan: {
      go_back: "BACK TO SHAVE PLAN BUNDLES",
      build_text: "BUILD YOUR OWN SHAVE PLAN",
      custom_men: "Custom men's shaving kits and products",
      header_step1:
        '<span style="padding-right: 15px;">1.</span> Customise your shave plan.<span class="completed-checkmark"></span>',
      header_step1_overview:
        '<span style="color: #fff; padding-right: 15px;">1.</span> You’ll get <span style="color: #fff;">{{cartridgeNum}} {{ cartidgeName }}</span> and <span style="color: #fff;">{{creamNum}} shave cream</span> every <span style="color: #fff;">{{planType}}</span>',
      number_one: "one",
      header_step2:
        '<span style="padding-right: 15px;">2.</span> Review and checkout.',
      next: "next",
      send_me: "Send me",
      and: "and",
      every: "every",
      frequency: "FREQUENCY",
      price_month: "PRICE / MONTH",
      price_shipment: "PRICE / SHIPMENT",
      checkout: "proceed  to checkout",
      today_total: "TODAY’S TOTAL",
      recurring_charges: "recurring charges",
      you_will_be_charged:
        "You’ll be charged <span style=\"font-family: 'Muli Bold';\">{{currency}}{{totalPrice}}</span>, every <span style=\"font-family: 'Muli Bold';\">{{planType}}</span> upon delivery.",
      cancel_modify_any_time:
        "You may cancel or modify your Shave Plan any time on the your account dashboard.",
      cartridgeNum: "one",
      shave_cream: "Shave Cream",
      item: "Item"
    }
  },

  // checkout.component
  checkout: {
    address: "Address",
    payment: "Payment",
    review: "Review",
    signin: "Sign in",
    signin_as: "Signed in as",
    delivery_address: "Delivery address",
    payment_method: "Payment method",
    cre_deb_card: "Credit/debit card",
    ipay88: "iPay88",
    summary: "Summary & place order",
    guest: "Guest",
    step_1: "shipping details",
    step_2: "Payment details",
    step_3: "review & confirm",
    order_summary: {
      header: "Order summary"
    },
    // Step Account
    step_account: {
      account: "Account",
      signin: "Sign in",
      guest: "Checkout as Guest",
      guest_description:
        "You’ll have an opportunity to create an account later.",
      continue_by_guest: "CONTINUE TO CHECKOUT",
      sign_in_with_facebook: "Sign in with Facebook",
      note:
        "Note: Don’t worry, we won’t post any of your information without prior permission/consent.",
      sign_in: {
        email: "Email",
        password: "Password",
        forgot_pw: "FORGOT PASSWORD",
        continue: "CONTINUE"
      }
    },

    // Step address
    step_address: {
      title: "Shipping address",
      add_or_change_link: "ADD OR CHANGE ADDRESS",
      change_account_link: "CHANGE ACCOUNT",
      select_address: "Select an Address",
      signedInAs: "Signed in as",
      contact: {
        heading: "Contact Details",
        name: {
          label: "Name",
          place_holder_firstName: "First",
          place_holder_lastName: "Last"
        },
        email: {
          label: "Email",
          place_holder: "john@email.com"
        },
        phone: {
          label: "Phone",
          place_holder: "123456789"
        }
      },
      country: "Country",
      address: {
        shipping_heading: "Shipping Address",
        gift_heading: "Sending a gift?",
        billing_heading: "Billing Address",
        heading_add: "Add New Address",
        heading_edit: "Edit Address",
        email: {
          label: "Email",
          place_holder: "john@email.com",
          // note_email_exists: 'Looks like you are an existing Shaves2U customer. Speed up your checkout by logging in via the link above.',
          note_email_exists:
            "Looks like you are an existing Shaves2U customer. Speed up your checkout by",
          logging_into: "logging into",
          your_account: "your account.",
          have_an_account: "You are checking out as a guest. Have an account?",
          login_text: "Login",
          end_text: "for speedier checkout."
        },
        name: {
          label: "Name",
          place_holder_firstName: "First",
          place_holder_lastName: "Last"
        },
        fullname: {
          label: "Full Name",
          place_holder_firstName: "Full Name",
          place_holder_lastName: "Full Name"
        },
        address: {
          label: "Address (Street / Area)",
          place_holder: "Address (Street / Area)"
        },
        phone: {
          label: "Phone",
          place_holder: "123456789",
          place_holder_sgp: "12345678"
        },
        calling_code: {
          label: "Calling Code",
          place_holder: "Calling Code"
        },
        region: {
          label: "Region",
          defaultOption: "Choose a region"
        },
        city: {
          label: "Town",
          place_holder: "Town"
        },
        postal_code: {
          label: "Postcode",
          place_holder: "10000"
        },
        state: {
          label: "State",
          defaultOption: "Choose a state"
        },
        btn_continue: "continue to payment"
      },

      billing: {
        // checkbox_label: 'Make Billing address same as Shipping Address',
        checkbox_label: "Bill to a different address"
      },

      shipping_method: {
        method_heading: "Shipping method",
        economy: "Economy - Free",
        ground: "Ground - {{price}}",
        two_day: "Two Day - {{price}}",
        one_day: "One Day - {{price}}",
        estimated_delivery: "Estimated delivery {{date}}",
        estimated_delivery_eco: "Estimated delivery {{dateTo}} - {{dateFrom}}"
      },
      shipping_address_1: "Address (line 1)"
    },

    // Step payment
    step_payment: {
      credit_debit_card: "Credit/Debit Card",
      online_banking: "Online Banking",
      btn_continue: "continue to review",
      payment: "Payment",
      heading: "Select a Payment Type",
      heading_card: "Card Details",
      add_or_change_link: "CHANGE OR ADD CARD",
      heading_add: "Add New Card",
      heading_edit: "Edit Card",
      select_card: "Select a Card",
      card: {
        heading: "Card Details",
        billing_profile: "Billing Profile",
        select_payment_method: "Select a payment method",
        ipay88: "iPay88",
        ipay88_note:
          "Your will be redirected to the payment gatewway to complete your transaction.",
        credit_card: "Credit card",
        credit_card_note: "Visa / Mastercard",
        btn_add_new: "Add New card",
        name: {
          label: "Name on Card",
          place_holder: "Name on Card"
        },
        number: {
          label: "Card Number",
          place_holder: "Card Number"
        },
        expiry: {
          label: "Expiry Date",
          place_holder: "MM / YY"
        },
        code: {
          label: "CCV",
          place_holder: "CCV",
          tooltip: "The last 3 digits displayed on the back of your card."
        }
      },
      iPay88: {
        note:
          "You will be redirected to the ipay88 system to complete the payment."
      }
    },

    step_review: {
      heading: "Review Order",
      btn_pay_now: "submit purchase",
      making_purchase_note:
        'By making a purchase, you accept <a href="terms-conditions" target="_blank" style="text-decoration: underline;">Terms of Service</a>.',
      link_edit: "Edit",
      account: {
        heading: "Account"
      },
      address: {
        heading: "Address",
        heading_shipping: "Shipping details",
        heading_billing: "Billing details"
      },
      payment: {
        heading: "Payment",
        ending_in: "Ending in",
        exprired_on: "Expiring in",
        payment_fail: "Payment failed"
      }
    },

    free_trial: {
      free_trial_set: "Trial Kit",
      // refill_ship: 'Your first refill ships {{ date }}.',
      refill_ship: "Ships between {{ date1 }} {{ date2 }} -  {{ date3 }}",
      cancel_modify: "Change or cancel any time",
      free_shipping: "Free shipping",
      delivery_every: "Delivery Every",
      free: "Free",
      premium_starter_kit: "Premium Starter Kit",
      tax: "Tax",
      shipping: "Shipping",
      ship_today: "Ships today",
      ship_today_korea: "Estimated delivery - Early October 2018",
      subtotal: "Subtotal",
      today_total: "Today's total",
      place_order: "Place order",
      ships_charges: "SHIPS BETWEEN {{ date1 }} - {{ date2 }}",
      charges_14_days: "Charges 14 days after you received the trial kit.",
      text_later:
        "We’ll send a reminder email before each shipment so you have enough time to modify or cancel your plan.",
      promotion_code: "Promo Code",
      apply: "Apply",
      coupon_code: "Coupon Code",
      time: "Every {{ _time }}",
      discount_off: "{{ discountPercent }}% discount off first delivery",
      shipping_address: "Shipping address",
      payment_method: "Payment method",
      edit: "Edit",
      estimated_delivery: "Estimated delivery",
      your_account: "your account",
      create_account: "Create account",
      had_account: "Already have an account?",
      phone_tooltip:
        "We will only use your number to contact you if there are any issues with your order.",
      term_text:
        'By making a purchase, you accept the <a href="/terms-conditions">Terms of Service</a> (update effective as of 11/17/17). Your subscription will automatically renew and your credit card will be charged the subscription fee. You can cancel or modify your plan at any time from your profile page.',
      head_block_1:
        "Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.",
      content_block_1:
        "Our blade cartridges have everything you need for a superior shave: precision-honed carbon steel blades, a moisturising lubrication strip, and an anti-clog design.",
      head_block_2: "Crafted for control.",
      content_block_2:
        "Our Premium Handle is designed with a comfortable rubberised grip and weighted metal body for optimal control, topped with a striking chrome finish. For a shave with substance and style.",
      head_block_3: "Good for your skin.",
      content_block_3:
        "Our shave cream cushions and conditions your facial hair for a gentler, smoother  shave. Enriched with anti-inflammatory ingredients to help reduce post-shave redness."
    },
    ask: {
      ask_set: "Awesome Shave Kit",
      // refill_ship: 'Your first refill ships {{ date }}.',
      refill_ship: "Ships between {{ date1 }} - {{ date2 }}.",
      cancel_modify: "Change or cancel any time",
      free_shipping: "Free shipping",
      delivery_every: "Delivery Every",
      free: "Free",
      premium_starter_kit: "Premium Starter Kit",
      tax: "Tax",
      shipping: "Shipping",
      ship_today: "Ships today",
      ship_today_korea: "Estimated delivery - Early October 2018",
      subtotal: "Subtotal",
      today_total: "Today's total",
      place_order: "Place order",
      ships_charges: "SHIPS BETWEEN {{ date1 }} - {{ date2 }}",
      charges_14_days: "Charges 14 days after you received the trial kit.",
      text_later:
        "We’ll send a reminder email before each shipment so you have enough time to modify or cancel your plan.",
      promotion_code: "Promo Code",
      apply: "Apply",
      coupon_code: "Coupon Code",
      time: "Every {{ _time }}",
      discount_off: "{{ discountPercent }}% discount off first delivery",
      shipping_address: "Shipping address",
      payment_method: "Payment method",
      edit: "Edit",
      estimated_delivery: "Estimated delivery",
      your_account: "your account",
      create_account: "Create account",
      had_account: "Already have an account?",
      phone_tooltip:
        "We will only use your number to contact you if there are any issues with your order.",
      term_text:
        'By making a purchase, you accept the <a href="/terms-conditions">Terms of Service</a> (update effective as of 11/17/17). Your subscription will automatically renew and your credit card will be charged the subscription fee. You can cancel or modify your plan at any time from your profile page.',
      head_block_1:
        "Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.",
      content_block_1:
        "Our blade cartridges have everything you need for a superior shave: precision-honed carbon steel blades, a moisturising lubrication strip, and an anti-clog design.",
      head_block_2: "Crafted for control.",
      content_block_2:
        "Our Premium Handle is designed with a comfortable rubberised grip and weighted metal body for optimal control, topped with a striking chrome finish. For a shave with substance and style.",
      head_block_3: "Good for your skin.",
      content_block_3:
        "Our shave cream cushions and conditions your facial hair for a gentler, smoother  shave. Enriched with anti-inflammatory ingredients to help reduce post-shave redness."
    }
  },

  // label
  lbl: {
    // login.component
    login: {
      email_form: {
        // signin_signup: 'Sign In or Sign Up',
        signin_head: "Sign In",
        signin: "Sign In",
        login_create_account: "Log in now or sign up to <span>Shaves2U</span>",
        create_account: "Create Account",
        placehdr_email: "Email",
        login: "Log in",
        signup: "Sign up",
        return_customer: "returning customers",
        new_customer: "new customers",
        account: "Create Account",
        email: "Email",
        password: "Password",
        or: "Or",
        signin_facebook: "Sign in with Facebook",
        signup_facebook: "Sign up with Facebook",
        note: "NOTE:",
        note_content:
          "Don’t worry, we won’t post any of your information without prior permission/consent.",
        inactive_account1: "Your account does not activate yet. Please click",
        inactive_account2: "to resend active email",
        btn_continue: "Continue",
        minimum_char: "Minimum 6 characters"
      },
      password_form: {
        welcome_back: "Welcome back,",
        not_you: "Not you?",
        change_email: "Change email",
        password: "Password",
        forgot_pw: "Forgot password?",
        btn_continue: "Continue",
        hints_heading: "Your password needs to:"
      },
      signup_form: {
        welcome: "Welcome",
        not_you: "Not you?",
        change_email: "Change email",
        name: "Name",
        first_nm: "First Name",
        last_nm: "Last Name",
        password: "Password",
        new_pw: "Create New Password",
        re_enter_pw: "Re-enter Password",
        date_of_birth: "Date of Birth",
        iam: "I'm a",
        gender_male: "Male",
        gender_female: "Female",
        offers: "Receive Shaves2u offers and updates",
        minimum_8char: "Minimum 8 characters",
        btn_continue: "Continue",
        note_content: "By creating an account, you accept our",
        note_content_terms: "Terms of Use",
        note_content_and: "and",
        note_content_policy: "Privacy Policy",
        signup: "Sign up",
        create_account: "Create account",
        placehdr_email: "Email Address",
        placehdr_first_nm: "First",
        placehdr_last_nm: "Last",
        placehdr_new_pw: "Create password",
        placehdr_re_enter_pw: "Retype password",
        new_customer_note:
          "Create an account with us for quick checkout, to keep track of your order, and more.",
        not_account: "Don’t have account?",
        create_one: "Create One"
      },
      send_email: {
        check_you_inbox: "Activate your Shaves2u account",
        note_activelink:
          "Check your email! We’ve sent a message to {{email}}. Open it up and click Activate Account. We’ll take it from there.",
        note_nextstep: "Please click on the link and continue to next step.",
        not_reveive_email: "Did not receive any email?",
        btn_resend: "Send again"
      },
      email_active: {
        congratulations: "Congratulations, your account is now active.!",
        activated: "Your account has been activated.",
        btn_continue: "Shop Now"
      },
      resend_email: {
        forgot_password: "Forgot password?",
        check_inbox: "Check you inbox",
        check_inbox_note:
          "You'll receive an email from us with instructions on how to reset your password in just a few",
        back_signin: "Go back to sign in",
        btn_submit: "Submit"
      },
      forgot_password: {
        title: "Forgot your password? Don’t worry!",
        description:
          "Enter your email to receive instructions on how to reset your password.",
        check_inbox: "You’ve Got Mail",
        check_inbox_note:
          "Check your inbox in a few moments and follow the simple instructions in our email to reset your password.",
        back_signin: "Return to Sign In Page",
        btn_submit: "Continue",
        email: "Email"
      }
    },
    // register.component
    register: {
      register: "Register",
      already_acc: "Already have an account!",
      login: "Login Here",
      btn_close: "Close",
      btn_register: "Register"
    },
    // reset-password.component
    reset_password: {
      new_pw_header: "Create a new password",
      note_email:
        "Your password must include at least 6 characters and at least 1 number or special character.",
      completeAccount: "Enter new password to activate your account",
      new_pw: "New password",
      confirm_pw: "Retype new password",
      btn_continue: "Save",
      resend_email: {
        note_activelink: "An activation link has been sent to your email.",
        note_nextstep: "Please click on the link and continue to next step.",
        token_expired:
          "Your token has been expired. Please click the button bellow to get another email",
        btn_resend: "SEND AGAIN"
      },
      success: {
        note: "Your password has been changed successfully",
        btn_continue: "Go to my account"
      }
    },
    // core
    core: {
      // home.component
      home: {
        video_txt: "Viva La",
        video_txt_2: "Razorlution La Viva",
        header: "From only RM18/month",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt facilisis tortor a blandit. Duis scelerisque purus eget pretium aliquam.",
        link_try: "TRY NOW",
        header_product: "Featured Products",
        sub_header_product: "All crafted for a close, comfortable shave.",
        link_product: "See more",
        header_article: "Articles",
        link_article: "Read full article",
        // get_razor_ready: 'Get razor ready<br><span>with a Shave Plan</span>',
        shaving_experience: 'A whole new men\'s shaving experience',
        get_razor_ready: 'A better shave, delivered.<br/>Try it now for <span>Free</span>.<br/>Just pay shipping.',
        get_razor_ready2: 'A better shave, delivered.<br/>Start your Shave Plan <br/> for just <strong>{{shippingFee}}</strong>',
        get_razor_ready_korea: 'A better shave, delivered. <br>Try it now for <span>Free</span>. <br>Just pay shipping.',
        get_razor_ready_non_ecommerce: 'Get Razor Ready with <span>Shaves2U</span>',
        trial_kit: 'Trial Kit',
        trial_kit_desc1: 'Better shaves start here.',
        start_a_trial: 'Comes with Trial Kit',
        start_a_trial2: 'Comes with<br> Trial Kit',
        start_a_trial2b: 'Comes with Trial Kit',
        start_a_trial3: 'Comes with<br> All Kits',
        start_a_trial3b: 'Comes with All Kits',
        start_a_ask: 'Comes with Awesome Shave Kit',
        start_a_ask_m: 'Comes with<br> Awesome Shave Kit',
        btn_learn_more: 'Learn more',
        btn_start_a_trial: 'START FREE TRIAL',
        btn_let_me_start: 'START FREE TRIAL',
        btn_let_me_start_korea: 'PRE-ORDER NOW',
        btn_find_out_more: 'Find out more',
        // subscribe_now: 'Subscribe now to <span>shave and save.</span>',
        subscribe_now: 'Why our razors are a cut above.',
        good_for_skin: 'Good for your skin.',
        good_for_skin_note: 'Nourishes skin before and after shaving. Enriched moisturising formula. Soothes and nourishes the skin during and after shaving. Cool, fresh scent. 100ml tube. Made in Turkey.',
        half_the_price: 'Ceramic-coated Blades',
        half_the_price_note: 'Corrosion-resistant. Reduced friction. Stays sharper, longer.',
        customise_delivery: 'Easy-care',
        customise_delivery_note: 'Open-blade design for easy rinsing, zero clogging.',
        pay_it_your_way: 'Import Quality',
        pay_it_your_way_note: 'Every cartridge is proudly made in the USA.',
        subscripton_note: 'Not convinced? We’ll let our razors do the talking.',
        not_ready_for_shave_plan: 'Fear of commitment?',
        we_get_it: 'We understand.',
        awesome_shave_kit: 'Awesome Shave Kit',
        gift_box: 'Gift Box',
        start_awesome_shave_kit: 'The Awesome Shave Kit {{value}} with all the essentials just might change your mind.',
        desc_awesome_shave_kit: 'Everything you need for the ultimate shaving experience.',
        btn_explore: 'EXPLORE AWESOME',
        pick_and_mix: 'Or Just<span> Pick & Mix</span>',
        check_out_product: 'Check out our products',
        best_seller: 'Best Seller',
        add_to_cart: 'ADD TO CART',
        content_img: '5-Blade Cartridge Pack (4pcs)',
        content_img_desc: '*Average price per replacement cartridge pack for Gillette Fusion ProGlide online across Lazada, 11Street, Watsons and Guardian',
        content: '<strong>Stop The Razor Rip-Off<strong>',
        content_desc: 'We sell our blades to you directly.<br> No middleman, no hidden fee\'s. Just<br> quality blades at an honest price.',
        namecard: 'Join more than 100,000 people who shave and save with Shaves2U',

        namecard_text1: 'Fantastic package and serice from the team over at Shaves2u. Yes there is room for improvement but trust me when i say that their follow-up service is top notch and their receptivity to feedback is stellar. Definietly saved alot since my Gillette days and no turning back.Keep up the good work guys!',
        namecard_text2: 'Bought a full kit few months ago and its way way better than top gillette stuff. Blades gets easily cleaned under running water, hairs dont get stuck in between blades and shaving is as good, if not better than my old Mach 3',
        namecard_text3: 'I really don\'t need any kind of super high-tech razor cause frankly. I\'m not that hairy, so i would say this brand ticks all my boxes in terms of price and functionality. Plus they deliver! So yeah, very aptly named Shaves2U, lol.',
        namecard_text4: 'I have always been a Mach 3 Gillette user out of convenience.However, after hearing about a business like Shave2U available in Malaysia, I had to give it a try! I don\'t regret it one bit! their blades are excellent, smooth and so far hasn\'t seemed to get dull.Comparable to the Mach 3 even! Best of all, It\'s absurdly cheap and they deliver right to your door!',
        namecard_text5: 'The shave plan is a terrific because it allows me to save money.I have been looking for a better shaving brand an when i came across Shave2u, I am so impressed by the value i am getting.',
        namecard_text6: 'The grip is better, love the shaving cream as well the blae lasts longer then Gillette as well. Love and support this brand, keep up the good work!',

        namecard_name1: 'Vanga Ganasan',
        namecard_name2: 'Olivier Guia',
        namecard_name3: 'Bonnie Teh',
        namecard_name4: 'Francisco Johannes Yung',
        namecard_name5: 'Ming Yang Chan',
        namecard_name6: 'KT Wong',
        home_trial_work: {
          // how_does_work: 'How our free trial works',
          how_does_work: 'How our Shave Plan works',
          how_does_work2: 'How our Shave Plan works',
          try: 'Try for 2 weeks',
          // try_note: 'Start with a shaver and shave cream. Just pay <strong>{{shippingFee}}</strong> shipping.',
          try_note: 'Start with a shaver and shave cream. Just pay <strong>{{shippingFee}}</strong>',
          try_note2: '<strong>{{shippingFee}}</strong>',
          try_note3: '{{shippingFee}}',
          try_note4: 'Start your shave plan for only <strong>{{shippingFee}}</strong>.',
          try_note5: 'Start your shave plan <br> for only <strong>{{shippingFee}}</strong>.',
          build: 'Build your plan',
          build_note: 'Choose your preferred blade and refill frequency.',
          cancel: 'Cancel anytime',
          cancel_note: 'Cancel or change your plan at your convenience.',
        }
      },
      ask: {
        title: "Awesome Shave Kit",
        sub_title:
          "Everything you need for the<br>ultimate shaving experience.",
        choose_your_blade: "Choose your blade",
        what_comes_in_the_box: "What comes in the box",
        free_delivery:
          "Free delivery to your door for orders above {{currency}}{{price}}",
        razor: "All our cartridges fit all our handles",
        what_makes_it_good: "What makes it good",
        what_makes_it_good_sub:
          "Not ready to commit to a plan?<br>This Awesome Shave Kit will keep<br>you stocked. It's also a fantastic gift!",
        what_makes_it_good_btn: "BUY NOW",
        prefer_shave_plan: "Prefer a Shave Plan?",
        prefer_shave_plan_sub:
          "Enjoy the Shaves2U shaving experience<br>at a lower price, every time.",
        prefer_shave_plan_btn: "GET STARTED"
      },
      // footer.component
      footer: {
        assurance: {
          money: "Money back guarantee",
          quality: "Superb USA & German build quality",
          razor: "All our cartridges fit all our handles",
          delivery:
            "Free delivery to your door for orders above {{currency}}{{price}}"
        },
        need_help: "Need Help?",
        typically_replies: "Typically replies within 2-3 working days.",
        terms_conditions: "Terms & Conditions",
        privacy_policy: "Privacy Policy",
        privacy_policy_main: "Privacy Policy",
        all_rights_reserved: "© 2018 Shaves2u. All Rights Reserved.",
        follow_us_on: "Follow us on",
        contact_us: "Contact Us",
        contact_address:
          "1st floor, PMK Building Yeoksamdong 746, Gangnam-gu Seoul, Korea",

        subscription: "SHAVE PLAN",
        starter_pack: "STARTER PACKS",
        handles: "HANDLES",
        cartridge: "CARTRIDGE PACKS",
        after_shave: "AWESOME SHAVE KIT",
        skin_care: "SKIN CARE",
        discover: "Discover",
        faq: "FAQ",
        contact: "Contact",
        track_my_order: "Track My Order",
        support: "Support",
        career: "Career",
        our_company: "Our Company",
        about_ss: "About Us",
        terms_condition: "Terms & Conditions",
        policy: "Privacy Policy",
        copyright: "© 2018 Shaves2u. All Rights Reserved",
        work_calendar: "Mon-Fri, 9am-6pm, except on public holidays."
      },
      // shopping-cart.component
      shopping_cart: {
        header: "Your Cart",
        header_empty: "You have no items in your cart.",
        subtotal: "Subtotal",
        checkout: "CHECKOUT",
        estimated_total: "Estimated Total",
        total: "Total"
      }
    },
    // cart.component
    cart: {
      add: "Add",
      no_item1:
        "Your Shopping Bag lives to serve.<br>Give it purpose — fill it with shavers and more.<br>If you already have an account, ",
      no_item2: " to see your Cart",
      no_item_login:
        "Your Shopping Bag lives to serve.<br>Give it purpose — fill it with shavers and more",
      no_item_des: "",
      review_cart: "Review the shave products in your cart",
      header: "YOUR CART",
      item: "Item",
      price: "Unit Price",
      quantity: "Quantity",
      qty: "Qty",
      free: "FREE",
      promo_code: "Promo Code",
      how_to_get: "How to get?",
      btn_checkout: "checkout",
      btn_cont_shop: "continue Shopping",
      suggest: "We think you may like this:",
      total: "Product Total",
      discount: "Discount",
      subtotal: "Subtotal",
      shipping_cost: "Shipping Cost",
      // total_incld_tax: 'Total (included tax)',
      total_incld_tax: "Grand Total (Incl. tax)",
      grand_total: "Grand Total",
      payment_incld: "Payment includes {{taxRate}}% {{taxName}}",
      apply: "Apply",
      note_1: "We Accept",
      note_2:
        'We offer refunds within 30 days after your purchase*. For more information, read our <a href="help?id=3">return policy</a>',
      note_3:
        "We’ll send a reminder email before each shipment so you have enough time to modify or cancel your plan.",
      note_4: "*Terms & Conditions apply",
      return_policy: "return policy.",
      shave_with: "shave plan with",
      shipping_on: "billing will begin on",
      default_payment_option: "-- Choose Payment Type--",
      add_to_cart: "Add To Cart",
      plan_disclaimer_1: "You’ve chosen the",
      plan_disclaimer_2: "Shave Plan and will be paying",
      plan_disclaimer_3: "from",
      plan_disclaimer_4:
        "Every Shaves2U order is carefully packaged and shipped to you with our trusted courier partners and you can expect your delivery in just 7 - 10 working days.",
      notice: "Notice",
      notice_note:
        "You have removed <strong>{{value}}</strong> from your cart.",
      undo: "Undo",
      choose_payment_type: "-- Choose Payment Type--",
      shave_plan: "Shave Plan",
      billing: "Billing",
      billing_starts: "Billing starts",
      subs_start: "Subscription Starts",
      order_summary: "Order summary",
      remove: "Remove"
    },
    // checkout.component
    checkout: {
      address: "Address",
      payment: "Payment",
      review: "Review",
      signin: "Sign in",
      signin_as: "Signed in as",
      delivery_address: "Delivery address",
      payment_method: "Payment method",
      cre_deb_card: "Credit/debit card",
      ipay88: "iPay88",
      summary: "Summary & place order",
      guest: "Guest",
      need_help: "Need Help?",
      typically_replies: "Typically replies within 2-3 working days.",
      all_rights_reserved: "© 2018 Shaves2u. All Rights Reserved",
      contact_us: "Contact Us",
      contact_address:
        "1st floor, PMK Building Yeoksamdong 746, Gangnam-gu Seoul, Korea",

      // Step Account
      step_account: {
        account: "Account",
        signin: "Sign in",
        guest: "Checkout as Guest",
        guest_description:
          "You’ll have an opportunity to create an account later.",
        continue_by_guest: "CONTINUE TO CHECKOUT",
        sign_in_with_facebook: "Sign in with Facebook",
        note:
          "Note: Don’t worry, we won’t post any of your information without prior permission/consent.",
        sign_in: {
          email: "Email",
          password: "Password",
          forgot_pw: "FORGOT PASSWORD",
          continue: "CONTINUE"
        }
      },

      // Step address
      step_address: {
        address: "Your shipping address",
        contact: {
          heading: "Contact Details",
          name: {
            label: "Name",
            place_holder_firstName: "First",
            place_holder_lastName: "Last"
          },
          email: {
            label: "Email",
            place_holder: "Enter an email address"
          },
          phone: {
            label: "Phone",
            place_holder: "123456789"
          }
        },
        shipping: {
          heading: "Shipping Address",
          address: {
            label: "Address",
            place_holder: "Street/Area"
          },
          region: {
            label: "Region",
            defaultOption: "Choose a region"
          },
          postal_code: {
            label: "Postal Code",
            place_holder: "10000"
          },
          state: {
            label: "State",
            place_holder: "Choose a state"
          }
        },

        billing: {
          checkbox_label: "Make Billing address same as Shipping Address"
        },

        shipping_address_1: "Address (line 1)"
      },

      // Step payment
      step_payment: {
        payment: "Payment",
        heading: "Select a Payment Type",
        checkout_now: "Check out your shave products now",
        checkout: "Checkout",
        btn_continue: "continue to review",
        card: {
          heading: "Billing profile",
          btn_add_new: "Add New card",
          btn_continue: "Continue to review",
          name: {
            label: "Name on Card",
            place_holder: ""
          },
          number: {
            label: "Card Number",
            place_holder: ""
          },
          expiry: {
            label: "Expiry Date",
            place_holder: "MM/YY"
          },
          code: {
            label: "Security Code",
            place_holder: "",
            tooltip: ""
          }
        },
        iPay88: {
          note:
            "You will be redirected to the ipay88 system to complete the payment."
        }
      },

      // sign-in.component
      sign_in: {
        email_form: {
          your_email: "Enter your Email",
          or: "Or",
          signin_facebook: "Log in with Facebook",
          signup_facebook: "Sign up with Facebook",
          note: "NOTE:",
          note_content:
            "Don’t worry, we won’t post any of your information without prior permission/consent.",
          btn_continue: "Continue"
        },
        password_form: {
          welcome_back: "Welcome back,",
          not_you: "Not you?",
          change_email: "Change email",
          password: "Password",
          forgot_pw: "Forgot password?",
          btn_continue: "Continue"
        },
        signup_form: {
          welcome: "Welcome",
          not_you: "Not you?",
          change_email: "Change email",
          first_nm: "First Name",
          last_nm: "Last Name",
          new_pw: "Create New Password",
          re_enter_pw: "Re-enter Password",
          date_of_birth: "Date of Birth",
          iam: "I'm a",
          gender_male: "Male",
          gender_female: "Female",
          offers: "Receive SMART SHAVE offers & updates",
          btn_continue: "Continue",
          or: "or",
          btn_checkout_guest: "Check out as guest"
        },
        send_email_form: {
          note_activelink: "An activation link has been sent to your email.",
          note_nextstep: "Please click on the link and continue to next step.",
          not_reveive_email: "Did not receive email?",
          btn_resend: "Send again"
        },
        email_active_form: {
          congratulations: "Congratulations!",
          activated: "Your account has been activated.",
          btn_continue: "Continue"
        },
        forgot_pw_form: {
          reset_link: "We've sent a reset link to your email.",
          btn_continue: "Continue"
        }
      },
      // sign-up.component
      sign_up: {
        btn_back: "Back",
        btn_create: "Create my account",
        note_activelink:
          "An activation link has been sent to your email account.",
        btn_resend: "Send email again",
        btn_continue: "Continue"
      },
      // delivery-address.component
      address_form: {
        title_shipping: "Shipping Address",
        btn_add_new: "Add New Address",
        title_billing: "Billing Address",
        header_add_new: "Add New Address",
        header_add_new_billing: "Add New Billing Address",
        header_edit: "Edit Address",
        first_nm: "First name",
        last_nm: "Last name",
        contact_num: "Contact number",
        address: "Address",
        state_region: "State/Region",
        choose_region: "Choose a Region",
        town_city: "Town/City",
        postal_cd: "Postal Code",
        shipping_addr: "Shipping Address",
        billing_addr: "Billing Address",
        bill_diff_addr: "Bill to a different address",
        btn_back: "Back",
        btn_continue: "Continue",
        btn_save: "Save changes",
        placehdr_fst_nm: "Enter first name",
        placehdr_lst_nm: "Enter last name",
        placehdr_cont_num: "Enter contact number",
        placehdr_addr: "Enter address",
        placehdr_town: "Enter Town/City",
        placehdr_pot_cd: "Enter Postal Code"
      },
      // payment-method.component
      payment_form: {
        cre_deb_card: "Credit/Debit card",
        ipay88: "iPay88",
        header_add_card: "Add card details",
        card_nm: "Name on card",
        card_num: "Card Number",
        expiry_dt: "Expiry Date",
        security_cd: "Security Code",
        btn_back: "Back",
        btn_continue: "Continue",
        btn_add_new: "Add New card",
        note_ipay88:
          "You will be redirected to the ipay88 system to complete the payment.",
        placehdr_card_nm: "Enter name on card",
        placehdr_card_num: "Enter card number",
        placehdr_security_cd: "Enter security code"
      },
      // summary-place-order.component
      summary_place_order: {
        btn_pay_now: "Pay Now",
        free_trial_info: {
          heading: "FREE TRIAL",
          details_text:
            'Your subscription for <font color="#3A6E9C"><strong>{{name}}</strong></font> at <font color="#3A6E9C"><strong>{{price}}</strong></font> starts <font color="#3A6E9C"><strong>{{date}}</strong></font>',
          details_text_no_start_date:
            'Your subscription for <font color="#3A6E9C"><strong>{{name}}</strong></font> at <font color="#3A6E9C"><strong>{{price}}</strong></font> starts <font color="#3A6E9C"><strong>between {{date1}} - {{date2}}</strong></font>',
          details_text_1: "Your subscription for ",
          details_text_2: " at ",
          details_text_3: " every ",
          details_text_4: " starts "
        }
      },
      // thankyou.component
      thankyou: {
        thankyou: "Thank you!",
        view_order:
          'your order number is <a class="code click-able" href="/user/orders/{{orderId}}">{{orderNumber}}</a>',
        estimated_delivery:
          'The estimated delivery date is <strong>{{deliveryDateFrom}}–{{deliveryDateTo}}</strong>.</br>Email us at <a href="mailto:help.my@shaves2u.com">help.my@shaves2u.com</a> with any questions or suggestions.',
        estimated_delivery_sgp:
          'The estimated delivery date is <strong>{{deliveryDateFrom}}–{{deliveryDateTo}}</strong>.</br>Email us at <a href="mailto:help.sg@shaves2u.com">help.sg@shaves2u.com</a> with any questions or suggestions.',
        estimated_delivery_kor:
          'The estimated delivery date is <strong>{{deliveryDateFrom}}–{{deliveryDateTo}}</strong>.</br>Email us at <a href="mailto:help.kr@shaves2u.com">help.kr@shaves2u.com</a> with any questions or suggestions.',
        return_to_home: "RETURN TO HOME",
        complete_signup: {
          description:
            "Complete your account sign up fo faster checkout process on your next purchase.",
          btn: "COMPLETE SIGN UP"
        },
        header_order: "Your order",
        link_check_ship_order: "Check your shipping order",
        item: "Item",
        price: "Price",
        quantity: "Quantity",
        total: "Total",
        promo_cd: "Promo code",
        none: "NONE",
        discount: "Discount",
        subtotal: "Subtotal",
        shipping_cost: "Shipping Cost",
        free: "FREE",
        total_incld_tax: "Grand Total (Incl. tax)",
        grand_total: "Grand Total",
        order_details: "Order Details",
        order_refer: "Order reference:",
        note_lorem:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt condimentum imperdiet.",
        subscription: "Subscription",
        banner: "Banner",
        link_try: "Try Now",
        subs_start: "Subscription Starts"
      }
    },
    // product-detail.component
    product_detail: {
      quantity: "Quantity",
      btn_add_releated: "Add",
      btn_add: "Add to Cart",
      btn_see_details: "SEE DETAILS",
      delivery:
        "Free delivery to your door for orders above <strong>{{currency}}{{price}}</strong>",
      handles: "All our cartridges fit all our handles",
      add_cart: {
        header: "Add to Cart",
        header_info: "1 new item has been added to your cart",
        header_cart: "Your Cart",
        free: "FREE",
        discount: "Discount",
        subtotal: "Subtotal",
        shipping_cost: "Shipping Cost",
        total_incld_tax: "Grand Total (Incl. tax)",
        grand_total: "Grand Total",
        btn_checkout: "CHECKOUT",
        btn_cont_shop: "CONTINUE SHOPPING",
        suggest: "You might also like",
        btn_add: "Add to cart"
      }
    },
    // product-list.component
    product_list: {
      header_highlight: "Highlight",
      header_awesome: "Awesome Shave Kit",
      note_vivamus: "Vivamus aliquam cursus nisl vitae ullamcorper.",
      link_buy: "Buy this",
      link_buy2: 'Buy now'
    },
    support: {
      // faq.component
      faq: {
        contacts: "Contact us",
        view_map: "View in Google Map",
        work_calendar: "Mon-Fri, 9am-6pm, except on public holidays.",
        available_at: "Available",
        email_note: "Typically replies within 2-3 working days.",
        address:
          "1st floor, PMK Building Yeoksamdong 746, Gangnam-gu Seoul, Korea"
      }
    },
    user: {
      // user.component
      link_subscription: "Shave Plan",
      link_order: "Order History",
      link_settings: "Settings",
      user_signout: "Sign Out",
      order: {
        // user-order-detail.component
        detail: {
          header_history: "Order History",
          order_id: "Order ID",
          link_track_order: "Track Order",
          link_close: "BACK",
          placed_on: "Placed on",
          subtotal: "Subtotal",
          shipping_cost: "Shipping Cost",
          discount: "Discount",
          total_incld_tax: "Grand Total (Incl. tax)",
          grand_total: "Grand Total",
          free: "FREE",
          promo_cd: "Promo Code",
          none: "NONE",
          payment_method: "Payment Method",
          cre_deb_card: "Credit/Debit card",
          ipay88: "iPay88",
          shipping_details: "Shipping Address",
          item: "Item",
          price: "Price",
          quantity: "Quantity",
          total: "Total",
          order_state_on_hold: "On Hold",
          order_state_payment: "Payment Received",
          order_state_processing: "Processing Order",
          order_state_deliver: "Deliver Order",
          order_state_received: "Completed",
          order_state_cancel: "Cancel"
        },
        // user-order-list.component
        list: {
          header_history: "Order History",
          date: "Date",
          order_id: "Order No.",
          items: "Items",
          price: "Price",
          status: "Status",
          total: "Total",
          link_view_details: "Details",
          empty_order: "You don't have any order history.",
          shop_now: "Shop now",
          txt_item: "item",
          txt_items: "items"
        }
      },
      // setting.component
      setting: {
        user_details: "User Details",
        email: "EMAIL",
        pass: "PASSWORD",
        birthday: "Date of Birth",
        shipping_details: "Shipping Address",
        link_add_addr: "Add New Address",
        billing_details: "Billing Address",
        payment_details: "Card Details",
        link_add_card: "Add New card",
        prefix_card_item: "CARD",
        prefix_address_item: "ADDRESS",
        text_default: "DEFAULT",
        primary_card: "PRIMARY",
        btn_add_address: "ADD ADDRESS",
        btn_add_address_shipping: "ADD ADDRESS",
        btn_add_address_billing: "ADD ADDRESS",
        btn_add_card: "ADD CARD",
        primary_shipping: "Primary Shipping",
        primary_billing: "Primary Billing",
        primary_payment_method: "Primary Payment Method",
        card_default: "Set as primary",
        footer: {
          heading: "Terminate Account",
          note:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries ",
          btn_delete_account: "DELETE MY ACCOUNT"
        },
        delete_address: "Are you sure you want to delete this address?",
        delete_payment:
          "Are you sure you want to remove this card from your account?",
        btn_delete: "Delete",

        // create-address-modal.component
        // edit-address-modal.component
        address_modal: {
          make_as_default_checkbox: "Make this as default address.",
          make_as_default_note:
            "Checking this will set this address as default whenever making a new purchase or subscription. You can change this setting later."
        },
        // create-card-modal.component
        card_modal: {
          header_add_card: "Add Card",
          card_nm: "Name on card",
          card_num: "Card Number",
          expiry_dt: "Expiry Date",
          security_cd: "Security Code",
          btn_add_new: "Add New card",
          placehdr_card_nm: "Enter name on card",
          placehdr_card_num: "Enter card number",
          placehdr_security_cd: "Enter security code",

          make_as_default_checkbox: "Make this as default card.",
          make_as_default_note:
            "Checking this will set this card as default whenever making a new purchase or subscription. You can change this setting later. All subscriptions will tie to ONE set as default card."
        },
        // edit-user-modal.component
        user_modal: {
          header_profile: "Edit Profile",
          first_nm: "First name",
          last_nm: "Last name",
          email: "Email",
          birthday: "Date of Birthday",
          gender: "Gender",
          gender_male: "Male",
          gender_female: "Female",
          country: "Country",
          change_pw: "Change password",
          old_pw: "Old Password",
          current_pw: "Current Password",
          new_pw: "New Password",
          btn_save_chg: "Save Changes",
          placehdr_fst_nm: "Enter first name",
          placehdr_lst_nm: "Enter last name",
          placehdr_email: "Enter email address",
          placehdr_old_pw: "Enter old password",
          placehdr_new_pw: "Enter new password",
          re_enter_pw: "Re-enter Password",
          pw_updated: "Your password has been updated successfully"
        }
      },
      subscription: {
        // user-subscription-history.component
        details: "Details",
        update: "Update",
        edit_plan_details: "EDIT PLAN DETAILS",
        cancel_shave_plan: "CANCEL SHAVE PLAN",
        subscribed: "You subscribed to",
        start_from: "started from",
        cancel_email:
          'To cancel your subscription, please email us at <a href="mailto:help@shaves2u.com">help@shaves2u.com</a>',
        cancel_email_kor:
          'To cancel your subscription, please email us at <a href="mailto:help.kr@shaves2u.com">help.kr@shaves2u.com</a>',
        cancel_email_mys:
          'To cancel your subscription, please email us at <a href="mailto:help.my@shaves2u.com">help.my@shaves2u.com</a>',
        cancel_email_sgp:
          'To cancel your subscription, please email us at <a href="mailto:help.sg@shaves2u.com">help.sg@shaves2u.com</a>',
        cancel_phone:
          'You can also call us at <a href="tel:{{phone1}}">{{phone2}}</a>',
        empty_subscription_title: "It looks like you’re not on a Shave Plan.",
        empty_subscription:
          "If you’re happy with our products, check out how you can save even more on shaving with a customisable Shave Plan.",
        payment_updated: "Your payment method has been updated successfully.",
        subscribe_now: "Find out more",
        payment_method: "Payment Method",
        ending_in: "Ending in",
        expired_on: "Expiring in",
        trial_expiring_in: "FREE TRIAL (Expires {{value}})",
        trial_expiring_in_14_days:
          "FREE TRIAL (Expires 14 days after you received the trial kit)",
        next_shipment: "Next Shipment",
        items: "Items",
        charges: "Charges",
        charges_on_date: "{{currency}}{{price}} on {{date}}",
        charges_on_14_days:
          "{{currency}}{{price}} and charges 14 days after you received the trial kit.",
        status: "Status",
        on: "on",
        delete_payment:
          'You are removing your credit card ending in <font color="#fe5000">{{value}}</font>. Are you sure?',
        history: {
          header_hist: "Subscription History",
          date: "Date",
          subscription_nm: "Subscription Name",
          order_no: "Order No.",
          price: "Price",
          status: "Status",
          link_view_detail: "View Details"
        },
        // user-subscription-overview.component
        overview: {
          header: "Premium member",
          note_1: "You are currently on Monthly Subscription Plan.",
          note_2: "( Active until 20 JULY 2018.)",
          btn_update: "Update"
        },
        // user-subscription-view-detail.component
        view_detail: {
          header_detail: "Subscription detail",
          btn_back: "Back",
          btn_details: "Details",
          change_address: "Change Address",
          change_card: "Change Card",
          title: {
            plan_history: "Plan History",
            plan_type: "Plan Type",
            payment_type: "Payment Type",
            billing_cycle: "Billing Cycle",
            next_billing: "Next Billing",
            next_shipment: "Next Shipment",
            start_date: "Start Date",
            address: "Address",
            payment: "Payment Method",
            shipping: "Shipping",
            billing: "Billing",
            ending_in: "Ending in",
            expired_on: "Expiring in"
          },
          free_trial: {
            select_blade: "Blade",
            refill: {
              heading: "Refill"
            },
            not_sure: "NOT SURE?",
            payment_type: {
              heading: "Payment Type"
            },
            usage: {
              heading: "Usage"
            },
            addon_shave_cream: {
              heading: "Addon Shave Cream",
              yes: "Yes",
              no: "No"
            },
            trial_subs_cancelled_handling: "Cancellation in progress"
          }
        }
      }
    },
    subscription: {
      detail: {
        // subscription-detail.component
        quantity: "Quantity",
        btn_add: "Add to cart"
      }
    },
    modal: {
      // confirm-deletion-modal.component
      confirm_deletion: {
        header: "Confirm Delete",
        note: "Do you want to delete?",
        yes: "Yes",
        no: "No",
        cancel: "Cancel"
      }
    },
    shave_plans: {
      block1: {
        // title: 'Stay sharp with a<br><font color="#fe5000">Shave Plan</font>',
        // title: 'Try the Shave Plan.&nbsp;<font color="#fe5000">Free.</font>',
        title: "try the shave plan for free",
        title2: 'Prefer a Shave plan?',
        // note: 'One less thing to think about. Many more shaves to grin about.',
        // note: 'Subscribe and shave off up to 40%.',
        perfect_shave: 'Subscription shaving kits and razors for a perfect shave',
        //note: 'Sign up for our Free Trial',
        note: 'Sign up for our Shave Plan',
        //note_desc: 'Delivered direct to you. <br>Just cover the shipping.',
        note_desc: 'Receive your Trial Kit for just {{shippingFee}}.',
        get_started: 'Get Started',
        deliver_trial: 'Delivered direct.',
        deliver_desc: 'You choose how often you want blades, and we send them to your door.',
        start_a_trial: 'START FREE TRIAL',
        btn_start_free_trial: 'START FREE TRIAL',
        how_it_work: 'How does it work?',
      },
      block2: {
        how_does_work: 'How does the Free Trial work?',
        sign_up: 'Get started free.',
        sign_up_note: 'Start shaving with our products for free. Just pay {{shippingFee}} shipping.',
        shave_up: 'Save each time.',
        shave_up_note: 'Enjoy a higher quality shave at a lower price, every time.',
        //switch_it_up: 'Be in control.',

        switch_it_up: 'Be in control',
        // switch_it_up2: 'Be in control',
        // switch_it_up_note: 'Change your plan or cancel any time from your profile page.',
        switch_it_up_note: 'Change or stop your plan at any time from your profile page.',
        start_shave_plan: 'Start A Shave Plan',
        get_start: 'START FREE TRIAL',
        so_what_the_plan: 'So what’s <font color="#fe5000">the plan?</font>',
        choose_your_plan: "Choose your plan",
        what_you_get: "What you’ll get"
      },
      block3: {
        title: "What’s in your Shave Plan",
        first: {
          title: 'FIRST SHIPMENT ',
          title_h2: 'Try for free',
          title_h3: 'free trial',
          title_h4: 'For just {{shippingFee}}',
          title_desc: 'just pay {{shippingFee}} shipping',
          desc: 'Comes with all you need to experience our quality for yourself. Trial period lasts for 14 days.',
          //desc: 'Experience our quality for yourself. Free Trial lasts up to 2 weeks.',
          first_delivery: 'First Delivery',
          trial_pack: 'Free Trial Pack',
          includes: 'Includes:',
          list_1: '1 x Men’s Premium Handle',
          list_2: '1 x 3-Blade & 5-Blade Cartridge',
          list_3: '1 x Shave Cream (100g)',
          // note: '*Women\'s Shave Plans do not contain Shave Cream'
        },
        subsequent: {
          title: 'NEXT SHIPMENT',
          title_h2: 'Keep saving as you shave',
          title_h3: 'Shave Plan',
          title_desc: 'Stay subscribed and save',
          //desc: 'Receive blade refills based on your preferred frequency. Enjoy free shipping.',
          desc: 'Stay subscribed and save. Receive blade refills based on your preferred frequency. Enjoy free shipping.',
          subsequent_delivery: 'Subsequent Deliveries',
          includes: 'Includes:',
          list_1: '1 x Cartridge Pack every 2/3/4/12 months*.',
          note: '*Based on your selected frequency.'
        },
        get_started: "get started"
      },
      block4: {
        title: "Why our razors are a cut above.",
        note_1: "3, 5, or 6 blade cartridges to suit your shaving needs",
        note_2: "Precision-cut carbon steel blades stay sharper, longer",
        note_3:
          "Moisturising lubrication strip protects skin against razor burn",
        // note_3: 'Every Shaves2U blade is built to last. No compromise.',
        start_shave_plan: "Start A Trial"
      },
      block5: {
        title: "Lorem ipsum dolor sit amet lorem ipsum dolor sit.",
        content:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
        // note_3: 'Every Shaves2U blade is built to last. No compromise.',
        start_shave_plan: "Start A Trial"
      },
      // block5: {
      //   rectangle_1: {
      //     title: 'Other Off-the-Shelf Shaving Brands',
      //     shaver: '1x Shaver',
      //     shaver_price_mys: 'RM17',
      //     shaver_price_sgp: 'S$6',
      //     cartridges: '16x Cartridges',
      //     cartridges_price_mys: 'RM164',
      //     cartridges_price_sgp: 'S$50',
      //     shave_cream: '1x Shave Cream',
      //     shave_cream_price_mys: 'RM15',
      //     shave_cream_price_sgp: 'S$9',
      //     total_price_mys: 'RM196',
      //     total_price_sgp: 'S$65',
      //     per_year: 'per year',
      //   },
      //   rectangle_2: {
      //     title: 'Shaves2U 3-Blade Shave Plan',
      //     shaver: '1x Shaver',
      //     cartridges: '16x Cartridges',
      //     shave_cream: '1x Shave Cream',
      //     total_price_mys: 'RM108',
      //     total_price_sgp: 'S$44',
      //     per_year: 'per year',
      //   },
      //   note: '*Prices are indicative and are not to be taken as exact figures.',
      //   start_saving: 'Start Saving',
      // },
      block6: {
        frequently_asked: "Frequently Asked Questions"
      },
      block7: {
        looking: 'Looking for something else?<br>Sign up for a shave plan without a Free Trial',
        subscribe: 'Sign up for a shave plan without a Free Trial',
        not_subscribe: 'Not ready to sign up? Check out our Awesome Shave Kit',
        //not_subscribe: 'Not ready to sign up? Check out our other products',
      }
    },
    trial_plan: {
      review_order: "Review Order",
      not_sure: "Not sure?",
      block1: {
        //headline: 'What’s in your Free Trial',
        headline: 'What’s in your Trial Kit',
        headline_korea: 'What’s in your First Shipment',
        quote1: 'Premium Handle with Blade Cartridge',
        quote2: 'Shave Cream',
        quote3: 'Estimated Delivery - Early-October 2018',
        note: '{{shippingFee}} shipping'
      }
    }
  }
};
