export const ko = {
  menu: {
    products: ' 제품',
    subscriptions: 'Subscriptions',
    articles: '기사',
    helps: '고객지원',
    support: 'Support',
    about: 'About us',
    login: '로그인',
    logout: 'Logout',
    register: 'Register',
    checkout: '결제하기',
    faq: 'FAQ',
    trackOrder: 'Track my order',
    subscription_note: 'Your subscription is active until',
    user_subscription: 'Subscription',
    user_order: 'Order',
    user_setting: '설정',
    user_signout: '나가기',
    help: '고객지원',
    shave_plans: '쉐이브플랜',
    cart: '장바구니',
    shop_all: '모두 구매',
    hi: '안녕하세요'
  },
  product: {
    field: {
      product: ' 제품',
      price: '가격',
      quantity: '수량',
      subtotal: '소계',
      choice_product: 'Your choice of men\'s shavers and grooming products'
    }

  },

  // dialog
  dialog: {
    confirm: {
      title: 'Confirm',
      content: '확실합니까?',
      cancelling_order: 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?',
      change_plan: 'You are updating your shave plan from <font color="#fe5000">{{from}}</font> to <font color="#fe5000">{{to}}</font>. Are you sure?',
      change_payment: 'You are updating your shave plan from <font color="#fe5000">{{from}} billing</font> to <font color="#fe5000">{{to}} billing</font>. Are you sure?',
      notAllowForIsTrial: '본 계정은 무료체험 서비스 조건에 부합하지 않는 계정임을 알려드립니다.',
      cancelling_trial_subs: '구독을 취소하시는게 확실한가요?',
      trial_subs_cancelled_succ : '<b>{{name}}</b> 정기구독이 취소되었습니다!',
      trial_request_to_cancel_succ : 'Request to cancel Shave Plan {{name}} have been sent to Customer Service, they will contact you for verification.',
      reverting_trial_subs : 'You are reverting your Shave Plan Free Trial. Are you sure?',
      trial_subs_reverted_succ : 'Request was successful',
      btn_ok: 'OK',
      btn_cancel: '취소하기'
    },
    collect_email: {
      title: '아직 오픈 준비 중이니 조금만 기다려주세요.<br>메일을 남겨 주시면 준비가 끝나는 즉시 기쁜 소식을 들고 찾아가겠습니다.',
      Terms_and_Conditions: 'I have read and understand the <a class="mobile_TC_layout" href="terms-conditions" target="_blank" style="text-decoration: underline;">Terms and Conditions</a>',
      email_address: '당신의 이메일 주소를 입력 해주세요:',
      ok: 'OK'
    },
    notification_popup: {
      title: '면도를 재정의할 면도기가 곧 찾아갑니다!',
      message: '무료 스타터 팩을 받을 기회 놓치지 마세요!',
      follow: '팔로우 하고'
    } ,
    cancellation_journey: {
      title: '서비스 이용 중 불편하셨던 점을 알려주세요.',
      subtitle: '만족스러운 서비스를 제공해드리지 못 해 죄송합니다. 서비스 품질 향상을 위해 취소 사유를 알려주세요.',
      other_reason: '기타 사유',
      button_ok : '확인',
      button_close : '마음이 바뀌었어요'
    }
  },

    // notification
    notification: {
      // logout: 'Signed out successfully.'
      logout: '로그아웃되었습니다',
      remind_user: '고객님의 계정이 비활성화된 상태입니다. 메일을 확인해주세요. 계정활성 이메일을 받지 못 하셨나요?',
      resend_active_email_text: '계정활성화 메일을 재발송하시겠습니까'
    },

  // message
  msg: {
    validation: {
      required: '{{value}} 필수 입력란입니다.',
      character_string: 'Contents for this textbox must have at least one Alphabetic character',
      special_characters: "Can't enter special characters",
      select_required: 'Please select a value for this field.',
      email: '유효한 이메일 주소를 입력하세요',
      email_exists: '이 이메일은 이미 존재합니다.',
      maxlength: 'This field only allows max {{value}} characters, please revise accordingly.',
      minlength: 'This field can be at least {{value}} characters long',
      maxDigits: 'This field can be at most XXX numeric digits',
      minDigits: 'This field can be at least {{value}} numeric digits',
      password_format: 'Password should be at least 6 characters long and include at least one number or symbol.',
      password_format_lenght: '최소 6개 문자',
      password_format_contain: '최소 1개 숫자 또는 특수문자',
      password_not_match: "비밀번호 확인란과 입력하신 비밀번호가 일치하지 않습니다",
      old_pw_invalid: 'Old password do not match',
      birthday_invalid: 'Birthday is invalid',
      phone: 'Must be a valid 10 digits phone number',
      valid : '올바른 {{value}} 입력하십시오.',
      phone_sgp: 'Must be a valid {{value}} digits phone number',
      promocode_minprice : 'You need to spend minimum {{value}} on promo specific product to enjoy this discount.',
      promocode_not_apply : 'Promo code not applicable.',
      card_number_length : '최신 세부 사항을 저장하려면 16 자리 신용 카드 번호를 다시 입력해야합니다.',
      date_of_birth_valid: '생년월일을 입력해주세요',
      day_valid: 'Please enter a valid day',
      year_valid: '유효한 연도를 입력해주세요',
      day: '일',
      month: '월',
      year: '년',
      gender_valid: '성별 정보를 입력해주세요',
      jan: '1월',
      feb: '2월',
      mar: '3월',
      apr: '4월',
      may: '5월',
      june: '6월',
      july: '7월',
      aug: '8월',
      sep: '9월',
      oct: '10월',
      nov: '11월',
      dec: '12월',
    },
    field: {
      email: '이메일',
      email_address: '이메일 주소',
      phone: '전화번호',
      postcode: '우편번호',
      password: '비밀번호',
    },
    status: {
      canceled: '취소됨',
      process: '처리 중',
      on_hold: '정지',
      payment_received: '결제 완료',
      completed: '완료됨',
      pending: '계류중인',
      delivering: '배송 중',
      loading: '로딩'
    },
    country: {
      korea: '한국'
    }
  },
  popup: {
    address: {
      heading_edit: {
        shipping: '배송주소 수정하기',
        billing: '결제주소 수정하기'
      },
      heading_add: {
        shipping: '새 배송지 추가하기',
        billing: '새 청구지 추가하기'
      }
    }
  },

  // btn

  btn: {
    continue: 'CONTINUE',
    back: '돌아가기',
    save_changes: '변경 내용 저장하기',
    cancel: '취소하기',
    save: '저장하기',
    select: '선택',
    revert: 'Undo Cancellation',
    update_plan : '업데이트',
  },

  //  shave plan page
  plan: {
    content_header: 'You’re currently building your Shave Plan. Your Trial Kit is free. Just pay shipping at checkout.',
    plan_section: {
      select_plan: 'What’s Your Shaver Flavour?',
      select_payment: 'Which Payment Plan Would You Prefer?',
      plan_details : 'What you’ll get',
      subtitle: 'Men’s<br>{{value}}-Blade Cartridges',
      subtitle_Women: 'Women’s<br>{{value}}-Blade Cartridges',
      s3_note: 'Gentler grooming.',
      s5_note: 'Sleeker shaving.',
      totalText: 'per annum',
      btn_discount: '저장하기',
      note: 'We’ll send you an e-mail before your <font color="#fe5000">first</font> payment is due, so you get the time you need to change or cancel your plan.',
      btn_add_to_cart: 'PROCEED WITH FREE TRIAL',
      suggest_payment: 'We accept'
    },
    frequently_asked_questions: 'FAQ',
    read_faq: 'Read our FAQs or get help from the Shaves2U team',
    free_trial: {
      select_blade: 'Choose Your Blade',
      refill: {
        heading: 'Choose Your Refill Frequency',
        heading_box: 'Every'
      },
      not_sure: '확실하지 않으신가요?',
      payment_type: {
        heading: 'Choose Your Payment Plan',
        description: 'Billing will only start when your first refill is shipped out.',
        save: '저장하기',
        title: 'PAY & DELIVER EVERY {{value}}',
        every: '마다',
        pay_as_you_go: '선불',
        annual: 'Annual',
        save_an: 'Save an extra',
      },
      suggest_refill: {
        heading: '원하시는 리필 주기를 선택하세요.',
        frequency_guide: '주기 설명',
        option_1: {
          heading: '5-7 days a week.',
          note: '2개월마다',
          note_2: '주 5-7일 면도'
        },
        option_2: {
          heading: '2-4 days a week.',
          note: '3개월마다',
          note_2: '주 2-4일 면도'
        },
        option_3: {
          heading: '1 day or less per week.',
          note: '4개월마다',
          note_2: '주 1일 이하 면도'
        },
        bottom_note: '1 refill consists of 4 cartridges.',
      }
    },
    custom_plan: {
      go_back: '구독 번들로 돌아가기',
      build_text: '내 쉐이브플랜 만들기',
      custom_men: 'Custom men\'s shaving kits and products',
      header_step1: '<span style="padding-right: 15px;">1.</span> 내 쉐이브플랜 만들기.<span class="completed-checkmark"></span>',
      header_step1_overview: '<span style="color: #fff; padding-right: 15px;">1.</span><span style="color: #fff;">{{cartridgeNum}} {{ cartidgeName }}</span>  <span style="color: #fff;">{{creamNum}} 쉐이브 크림</span><span style="color: #fff;">{{planType}}</span>에 한번 배송될 예정입니다',
      number_one: '하나',
      header_step2: '<span style="padding-right: 15px;">2.</span> 확인 및 결제.',
      next: '다음',
      send_me: '를 보내주세요',
      and: ', ',
      every: '매',
      every_2: '마다',
      frequency: '배송 빈도',
      price_month: 'PRICE / MONTH',
      price_shipment: '가격/배송',
      checkout: '나가기',
      today_total: '금일 합계',
      recurring_charges: '정기결제',
      you_will_be_charged: '배송되는 매 <span style="font-family: \'Muli Bold\';">{{currency}}{{totalPrice}}</span>, 마다  <span style="font-family: \'Muli Bold\';">{{planType}}</span> 이 결제됩니다.',
      cancel_modify_any_time: '언제든지 계정 대시보드에서 쉐이브플랜을 수정 또는 취소할 수 있습니다.',
      cartridgeNum: '1개',
      shave_cream: '쉐이브 크림',
      item:'제품'
    }
  },

  // checkout.component
  checkout: {
      address : '주소',
      payment : '결제',
      review : 'Review',
      signin: '로그인하기',
      signin_as: 'Signed in as',
      delivery_address: 'Delivery address',
      payment_method: '결제수단',
      cre_deb_card: '신용/직불 카드',
      ipay88: 'iPay88',
      summary: 'Summary & place order',
      guest: 'Guest',
      step_1: '배송 정보',
      step_2: '결제 정보',
      step_3: '보기 및 확인하기',
      order_summary: {
        header: '주문정보',
      },
      // Step Account
      step_account: {
        account: '계정',
        signin: '로그인하기',
        guest: 'Checkout as Guest',
        guest_description: 'You’ll have an opportunity to create an account later.',
        continue_by_guest: 'CONTINUE TO CHECKOUT',
        sign_in_with_facebook: '페이스북으로 로그인하기',
        note: 'Note: Don’t worry, we won’t post any of your information without prior permission/consent.',
        sign_in: {
          email: '이메일',
          password: '비밀번호',
          forgot_pw: 'FORGOT PASSWORD',
          continue: 'CONTINUE'
        }


      },

      // Step address
      step_address: {
        title: '배송지 주소',
        add_or_change_link: 'ADD OR CHANGE ADDRESS',
        change_account_link: 'CHANGE ACCOUNT',
        select_address: 'Select an Address',
        signedInAs: 'Signed in as',
        contact: {
          heading: 'Contact Details',
          name: {
            label: '이름',
            place_holder_firstName: '이름',
            place_holder_lastName: '성'
          },
          email: {
            label: '이메일',
            place_holder : 'john@email.com'
          },
          phone: {
            label: '전화번호',
            place_holder: '123456789'
          },
        },
        country: '국가',
        address: {
          shipping_heading: '배송지',
          gift_heading: 'Sending a gift?',
          billing_heading: '청구지 주소',
          heading_add: 'Add New Address',
          heading_edit: 'Edit Address',
          email: {
            label: '이메일',
            place_holder : 'john@email.com',
            // note_email_exists: 'Looks like you are an existing Shaves2U customer. Speed up your checkout by logging in via the link above.',
            note_email_exists: 'Looks like you are an existing Shaves2U customer. Speed up your checkout by',
            logging_into: 'logging into',
            your_account: 'your account.',
            have_an_account: 'You are checking out as a guest. Have an account?',
            login_text: 'Login',
            end_text: 'for speedier checkout.'
          },
          name: {
            label: '이름',
            place_holder_firstName: '이름',
            place_holder_lastName: '성'
          },
          fullname: {
            label: '이름',
            place_holder_firstName: '이름',
            place_holder_lastName: '이름'
          },
          address: {
            label: '주소',
            place_holder : '주소'
          },
          phone: {
            label: '전화번호',
            place_holder: '123456789',
            place_holder_sgp: '12345678',
          },
          calling_code: {
            label: 'Calling Code',
            place_holder: 'Calling Code'
          },
          region: {
            label: 'Region',
            defaultOption: 'Choose a region'
          },
          city:{
            label: '군/구/동',
            place_holder: '군/구/동'
          },
          postal_code:{
            label: '우편번호',
            place_holder: '10000'
          },
          state: {
            label: '도',
            defaultOption: '도/시'
          },
          btn_continue: '결제하기'
        },

        billing: {
          // checkbox_label: 'Make Billing address same as Shipping Address',
          checkbox_label: 'Bill to a different address',

        },

        shipping_method: {
          method_heading: 'Shipping method',
          economy: 'Economy - Free',
          ground: 'Ground - {{price}}',
          two_day: 'Two Day - {{price}}',
          one_day: 'One Day - {{price}}',
          estimated_delivery: '{{date}} 배송 예정',
          estimated_delivery_eco: '{{dateTo}} - {{dateFrom}} 배송 예정',
        },
        shipping_address_1: 'Address (line 1)'
      },

      // Step payment
      step_payment: {
        credit_debit_card: '신용/직불 카드',
        online_banking : 'Online Banking',
        btn_continue: '확인하기',
        payment: '결제',
        heading: 'Select a Payment Type',
        heading_card: '카드 정보',
        add_or_change_link: 'CHANGE OR ADD CARD',
        heading_add: '새 카드 추가하기',
        heading_edit: 'Edit Card',
        select_card: '카드 선택',
        card: {
          heading: '카드 정보',
          billing_profile: '결제 프로필',
          select_payment_method: '결제수단 선택하기',
          ipay88: 'iPay88',
          ipay88_note: '결제 완료를 위해 결제 서비스로 이동합니다',
          credit_card: '신용 카드',
          credit_card_note: '비자/마스터 카드',
          btn_add_new: '새 카드 추가하기',
          name: {
            label: 'Name on Card',
            place_holder: 'Name on Card',
          },
          number: {
            label: '카드 번호',
            place_holder: '카드 번호'
          },
          expiry: {
            label: '유효기간',
            place_holder: '월월 / 년년'
          },
          code: {
            label: 'CCV',
            place_holder: 'CCV',
            tooltip: 'The last 3 digits displayed on the back of your card.'
          }
        },
        iPay88: {
          note: 'You will be redirected to the ipay88 system to complete the payment.',
        }
      },

      step_review: {
        heading: '주문 보기',
        btn_pay_now: '구매하기',
        making_purchase_note: '구매함으로써 <a href="terms-conditions" target="_blank" style="text-decoration: underline;">서비스약관에</a> 동의합니다',
        link_edit: '수정하기',
        account: {
          heading: '계정'
        },
        address: {
          heading: '주소',
          heading_shipping: '배송 정보',
          heading_billing: '결제 정보'
        },
        payment: {
          heading: '결제',
          ending_in: '카드 번호',
          exprired_on: '유효기간',
          payment_fail: '결제 실패'
        }
      },

			free_trial: {
				free_trial_set: '무료체험 키트',
				// refill_ship: '첫 보충 제품 배송 {{ date }}.',
				refill_ship: '예상배송일자 {{ date1 }} - {{ date2 }}.',
				cancel_modify: '언제든지 변경 또는 취소 가능 ',
				free_shipping: '배송비 무료',
				delivery_every: '배송 주기',
				free: '무료',
				premium_starter_kit: 'Premium Starter Kit',
				tax: 'Tax',
				shipping: '배송비',
        ship_today: '오늘 배송',
				ship_today_korea: '예상 배송일 - 2018년 10월 초',
				subtotal: '소계',
				today_total: '오늘 총액',
				place_order: '주문하기',
				// ships_charges: '{{ date }} 배송 및 결제',
				ships_charges: '예상배송일자 {{ date1 }} - {{ date2 }}',
				charges_14_days: '무료체험 제품을 수령하시고 14일 후에 결제가 진행됩니다.',
        text_later: '플랜을 수정 또는 취소할 충분한 시간을 드리기 위해 매 배송 전 이메일을 보내 알려드립니다.',
        promotion_code: '프로모션 코드',
				apply: '신청하기',
				coupon_code: 'Coupon Code',
				time: '{{ _time }} 마다',
				discount_off: '{{ discountPercent }}% discount off first delivery',
				shipping_address: '배송지',
				payment_method: '결제수단',
				edit: '수정하기',
				estimated_delivery: '배송 예정',
				your_account: '내 계정',
				create_account: '계정 만들기',
				had_account: '이미 계정이 있으세요?',
				phone_tooltip: 'We will only use your number to contact you if there are any issues with your order.',
				term_text: '구매함으로써 서비스약관에 동의합니다 (수정된 부분은 2017년 11월 17일 이후 효력 있습니다. 신청은 자동 갱신되며 신용카드로 신청비가 청구됩니다. 프로필 페이지에서 언제든지 플랜을 취소 또는 수정할 수 있습니다.',
				head_block_1: '정교한 면도날. 보습력 있는 윤활밴드. <br>오픈형 면도날 구조.',
				content_block_1: 'Shaves2U 면도날은 깔끔한 면도를 위한 모든 것을 갖췄습니다. 정교한 탄소강 면도날, 보습력 있는 윤활밴드, 오픈형 날구조로 막히지 않도록 설계됐습니다.',
				head_block_2: '섬세한 컨트롤',
				content_block_2: '프리미엄 핸들은 고무로 마무리 해 잡기 편하고, 이상적인 무게감을 위해 크롬으로 제작했습니다. 실용적이면서 세련된 면도경험을 선사합니다.',
				head_block_3: '피부에 좋습니다.',
				content_block_3: '쉐이브 크림은 수염을 순하게 만들어 더 부드러운 면도를 할 수 있습니다. 염증을 방지하는 성분이 추가돼 면도 후 피부가 붉어지는 현상을 줄일 수 있습니다.',
			}
  },

  // label
  lbl: {
    // login.component
    login: {
      email_form: {
        // signin_signup: 'Sign In or Sign Up',
        signin_head: '로그인하기',
        signin: '로그인하세요',
        login_create_account: 'Log in now or sign up to <span>Shaves2U</span>',
        create_account: '계정 만들기',
        placehdr_email: '이메일',
        login: 'Log in',
        signup: '가입하기',
        return_customer: '기존 고객',
        new_customer: '신규 고객',
        account: '계정 만들기',
        email: '이메일',
        password: '비밀번호',
        or: 'Or',
        signin_facebook: '페이스북으로 로그인하기',
        signup_facebook: '페이스북으로 가입하기',
        note: '알림:',
        note_content: '걱정하지 마세요. 사전 승인 또는 동의 없이 여러분의 정보는 게재되지 않습니다.',
        inactive_account1: 'Your account does not activate yet. Please click',
        inactive_account2: 'to resend active email',
        btn_continue: 'Continue',
        minimum_char: 'Minimum 6 characters'
      },
      password_form: {
        welcome_back: 'Welcome back,',
        not_you: 'Not you?',
        change_email: 'Change email',
        password: '비밀번호',
        forgot_pw: '비밀번호를 잊으셨나요?',
        btn_continue: 'Continue',
        hints_heading: '비밀번호와 관련해 다음 사항이 필요합니다.',
      },
      signup_form: {
        welcome: 'Welcome',
        not_you: 'Not you?',
        change_email: 'Change email',
        name: '이름',
        first_nm: '이름',
        last_nm: '성',
        password: '비밀번호',
        new_pw: 'Create New Password',
        re_enter_pw: '비밀번호 재입력하기',
        date_of_birth: 'Date of Birth',
        iam: "I'm a",
        gender_male: '남성',
        gender_female: '여성',
        offers: 'Receive Shaves2u offers and updates',
        minimum_8char: 'Minimum 8 characters',
        btn_continue: 'Continue',
        note_content: '계정을 생성하시면',
        note_content_terms: '이용약관',
        note_content_and: '및',
        note_content_policy: '개인정보처리방침',
        note_content_end: '에 동의하게 됩니다.',
        signup: '가입하기',
        create_account: '계정 만들기',
        placehdr_email: '이메일',
        placehdr_first_nm: '이름',
        placehdr_last_nm: '성',
        placehdr_new_pw: '비밀번호 생성하기',
        placehdr_re_enter_pw: '비밀번호 재입력하기',
        new_customer_note: '계정을 생성하면 결제, 배송 조회 등을 더 빠르게 할 수 있습니다.',
        not_account: '계정이 없으신가요?',
        create_one: '계정을 생성해보세요',
      },
      send_email: {
        check_you_inbox: 'S2U 계정 활성화하기',
        note_activelink: '이메일을 확인하세요! {{email}}으로 이메일을 발송했습니다. 이메일을 열어 계정 활성화하기를 누르시면 계속 진행됩니다.',
        not_reveive_email: '이메일을 받지 않았나요?',
        btn_resend: '다시 발송하기',
      },
      email_active: {
        congratulations: 'Congratulations, your account is now active.!',
        activated: 'Your account has been activated.',
        // btn_continue: 'Shop Now',
        btn_continue: '쇼핑하기',
      },
      resend_email: {
        forgot_password: '비밀번호를 잊으셨나요?',
        check_inbox: 'Check you inbox',
        check_inbox_note: 'You\'ll receive an email from us with instructions on how to reset your password in just a few',
        back_signin: "Go back to sign in",
        btn_submit: '제출하기',
      },
      forgot_password: {
        title: '비밀번호를 잊으셨나요?',
        description: '비밀번호를 재설정하는 방법을 보시려면 이메일을 입력하세요',
        check_inbox: '이메일을 받았습니다',
        check_inbox_note: '비밀번호를 재설정하려면 잠시 후 이메일을 확인해 간단한 절차를 따라주세요.',
        back_signin: "로그인 페이지로 돌아가기",
        btn_submit: 'Continue',
        email: '이메일',
      }
    },
    // register.component
    register: {
      register: 'Register',
      already_acc: 'Already have an account!',
      login: 'Login Here',
      btn_close: 'Close',
      btn_register: 'Register',
    },
    // reset-password.component
    reset_password: {
      new_pw_header: '비밀번호 생성하기',
      note_email: '비밀번호는 최소 6개 문자와 최소 1개 숫자 또는 특수문자를 포함해야 합니다.',
      completeAccount: 'Enter new password to activate your account',
      new_pw: '새로운 비밀번호',
      confirm_pw: '새 비밀번호 재입력하기',
      btn_continue: '저장하기',
      resend_email: {
        note_activelink: 'An activation link has been sent to your email.',
        note_nextstep: 'Please click on the link and continue to next step.',
        token_expired: 'Your token has been expired. Please click the button bellow to get another email',
        btn_resend: '다시 발송하기',
      },
      success: {
        note: '비밀번호가 변경되었습니다.',
        btn_continue: '내 계정으로 가기',
      }
    },
    // core
    core: {
      // home.component
      home: {
        video_txt: 'Viva La',
        video_txt_2: 'Razorlution La Viva',
        header: 'From only RM18/month',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt facilisis tortor a blandit. Duis scelerisque purus eget pretium aliquam.',
        link_try: 'TRY NOW',
        header_product: '제품',
        sub_header_product: '더 편안하고 깔끔한 밀착면도를 보장합니다.',
        link_product: '더 보기',
        header_article: 'Articles',
        link_article: 'Read full article',
        // get_razor_ready: 'Get razor ready<br><span>with a Shave Plan</span>',
        get_razor_ready: '더 매끈한 면도를 할 수 있습니다.<br/>무료체험을 해보세요.<br/>배송비만 내면 됩니다.',
        get_razor_ready_korea: '10월이 되기 전에 선주문 하시고 첫달은 <span>무료로</span> 이용하세요.<br/>배송비만 내시면 됩니다.',
        shaving_experience: 'A whole new men\'s shaving experience',
        get_razor_ready_non_ecommerce: '면도기는 <span>Shaves2U</span>와 함께',
        btn_learn_more: 'Learn more',
        btn_start_a_trial: '무료체험 시작하기',
        btn_let_me_start: '무료체험 시작하기',
        btn_let_me_start_korea: '지금 선주문하세요!',
        btn_find_out_more: '더 알아보기',
        // subscribe_now: 'Subscribe now to <span>shave and save.</span>',
        subscribe_now: 'Shaves2U 면도기의 우수성',
        good_for_skin: 'Good for your skin.',
        good_for_skin_note: 'Nourishes skin before and after shaving. Enriched moisturising formula. Soothes and nourishes the skin during and after shaving. Cool, fresh scent. 100ml tube. Made in Turkey.',
        half_the_price: '세라믹 코팅',
        half_the_price_note: ' 면도날이 세라믹 코팅 처리가 되어 내부식성 및 마찰을 감소해주고, 날카롭게 더 오래 유지해 줍니다.',
        customise_delivery: '이지 케어',
        customise_delivery_note: '오픈 블레이드 디자인으로 막힘 없이 잘 밀리고 간편하게 세척할 수 있습니다.',
        pay_it_your_way: '우수한 품질',
        pay_it_your_way_note: 'Shaves2U는 우수한 품질의 미국 및 독일산 카트리지를 사용합니다.',
        subscripton_note: 'Not convinced? We’ll let our razors do the talking.',
        not_ready_for_shave_plan: 'Fear of commitment?',
        we_get_it: 'We understand.',
        start_awesome_shave_kit: 'The Awesome Shave Kit {{value}} with all the essentials just might change your mind.',
        btn_explore: 'EXPLORE AWESOME',
        pick_and_mix: 'Or Just<span> Pick & Mix</span>',
        check_out_product: '제품 보기',
        best_seller: 'Best Seller',
        add_to_cart: '장바구니 담기',
        home_trial_work: {
          how_does_work: '무료체험 방식',
          try: '2주 체험하기',
          try_note: '면도기와 쉐이빙 크림을 체험해보세요. <br>배송비 <strong>{{shippingFee}}</strong>만 내면 됩니다.',
          build: '계획하기',
          build_note: '원하시는 면도날과 배송 주기를 선택하세요.',
          cancel: '언제든지 취소하기',
          cancel_note: '언제든지 구독플랜을 수정하거나 취소하세요.',
        }
      },
      // footer.component
      footer: {
        assurance: {
          money: '환불 보장',
          quality: '최고의 미국 & 독일 생산 품질',
          razor: '우리의 모든 카트리지가 모든 핸들에 맞습니다',
          delivery: 'Free delivery to your door for orders above {{currency}}{{price}}',

        },
        need_help: 'Need Help?',
        typically_replies: 'Typically replies within 2-3 working days.',
        terms_conditions: '이용약관',
        privacy_policy: '개인정보처리방침',
        privacy_policy_main: '개인정보정책',
        all_rights_reserved: '© 2018 Shaves2u 무단 전재 및 재배포 금지',
        follow_us_on: 'Follow us on',
        contact_us: '연락처',
        contact_address: '서울시강남구역삼동746, PMK빌딩1층',

        subscription: '쉐이브플랜',
        starter_pack: '스타터팩',
        handles: '핸들',
        cartridge: '카트리지',
        after_shave: 'AWESOME SHAVE KIT',
        skin_care: 'SKIN CARE',
        discover: '알아보기',
        faq: 'FAQ',
        contact: '연락처',
        track_my_order: 'Track My Order',
        support: ' 도움',
        career: ' 채용',
        our_company: '우리 회사',
        about_ss: '회사소개',
        terms_condition: '조건',
        policy: '개인정보보호정책',
        copyright: '© 2018 Shaves2u. All Rights Reserved',
        work_calendar: '공휴일 외 월-금, 오전9시-오후6시.',
      },
      // shopping-cart.component
      shopping_cart: {
        header: '장바구니',
        header_empty: '장바구니가 비어있습니다.',
        subtotal: '소계',
        checkout: '결제하기',
        estimated_total: 'Estimated Total',
        total: '총 제품 가격',
      }
    },
    // cart.component
    cart: {
      add: 'Add',
      no_item1: '장바구니는 여러분을 위한 것입니다.<br>장바구니를 면도기 등으로 채워주세요.<br>If you already have an account, Log In to see your Cart.',
      no_item2: ' to see your Cart',
      no_item_login: '장바구니는 여러분을 위한 것입니다.<br>장바구니를 면도기 등으로 채워주세요',
      login:'계정이 있는 경우, 장바구니를 보기 위해 <a href="/login">로그인</a>해주세요',
      no_item_des: '',
      review_cart: 'Review the shave products in your cart',
      header: '장바구니',
      item: '제품',
      price: '제품 가격',
      quantity: '수량',
      qty: '수량',
      free: '무료',
      promo_code: '프로모션 코드',
      how_to_get: 'How to get?',
      btn_checkout: '결제하기',
      btn_cont_shop: '쇼핑 계속하기',
      suggest: '추천 상품',
      total: '총 제품 가격',
      discount: '할인',
      subtotal: '소계',
      shipping_cost: '배송비',
      // total_incld_tax: 'Total (included tax)',
      total_incld_tax: '총액 (세금 포함)',
      grand_total: '총액',
      payment_incld: '결제액에는 {{taxRate}}% {{taxName}} 가 포함됩니다',
      apply: '신청하기',
      note_1: 'We Accept',
      note_2: 'We offer refunds within 30 days after your purchase*. For more information, read our <a href="help?id=3">return policy</a>',
      note_3: '플랜을 수정 또는 취소할 충분한 시간을 드리기 위해 매 배송 전 이메일을 보내 알려드립니다.',
      note_4: '*Terms & Conditions apply',
      return_policy: 'return policy.',
      shave_with: 'shave plan with',
      shipping_on: 'billing will begin on',
      default_payment_option: '-- Choose Payment Type--',
      add_to_cart: '장바구니 담기',
      plan_disclaimer_1: 'You’ve chosen the',
      plan_disclaimer_2: 'Shave Plan and will be paying',
      plan_disclaimer_3: 'from',
      plan_disclaimer_4: 'Every Shaves2U order is carefully packaged and shipped to you with our trusted courier partners and you can expect your delivery in just 7 - 10 working days.',
      notice: 'Notice',
      notice_note: 'You have removed <strong>{{value}}</strong> from your cart.',
      undo: 'Undo',
      choose_payment_type: '-- Choose Payment Type--',
      shave_plan: '쉐이브플랜',
      billing: 'Billing',
      billing_starts: 'Billing starts',
      subs_start: 'Subscription Starts',
      order_summary: '주문내용',
      remove: '제거하기',
    },
    // checkout.component
    checkout: {

				address : '주소',
				payment : '결제',
				review : 'Review',
        signin: '로그인하기',
        signin_as: 'Signed in as',
        delivery_address: 'Delivery address',
        payment_method: '결제수단',
        cre_deb_card: '신용/직불 카드',
        ipay88: 'iPay88',
        summary: 'Summary & place order',
        guest: 'Guest',
        need_help: 'Need Help?',
        typically_replies: 'Typically replies within 2-3 working days.',
        all_rights_reserved: '© 2018 Shaves2u 무단 전재 및 재배포 금지',
        contact_us: '연락처',
        contact_address: '서울시강남구역삼동746, PMK빌딩1층',

				// Step Account
				step_account: {
					account: '계정',
					signin: '로그인하기',
					guest: 'Checkout as Guest',
					guest_description: 'You’ll have an opportunity to create an account later.',
					continue_by_guest: 'CONTINUE TO CHECKOUT',
					sign_in_with_facebook: '페이스북으로 로그인하기',
					note: 'Note: Don’t worry, we won’t post any of your information without prior permission/consent.',
					sign_in: {
						email: '이메일',
						password: '비밀번호',
						forgot_pw: 'FORGOT PASSWORD',
						continue: 'CONTINUE'
					}


				},

				// Step address
				step_address: {
					address: 'Your shipping address',
					contact: {
						heading: 'Contact Details',
						name: {
							label: '이름',
							place_holder_firstName: '이름',
							place_holder_lastName: '성'
						},
						email: {
							label: '이메일',
							place_holder : '이메일 입력하기'
						},
						phone: {
							label: '전화번호',
							place_holder: '123456789'
						},
					},
					shipping: {
						heading: '배송지',
						address: {
							label: '주소',
							place_holder : 'Street/Area'
						},
						region: {
							label: 'Region',
							defaultOption: 'Choose a region'
						},
						postal_code:{
							label: '우편번호',
							place_holder: '10000'
						},
            state: {
              label: '도/시',
              place_holder: 'Choose a state'
            }
					},

          billing: {
            checkbox_label: 'Make Billing address same as Shipping Address',

          },

					shipping_address_1: 'Address (line 1)'
				},

        // Step payment
        step_payment: {
          payment: '결제',
          heading: 'Select a Payment Type',
          checkout_now: 'Check out your shave products now',
          checkout: '결제하기',
          btn_continue: '확인하기',
          card: {
            heading: '결제 프로필',
            btn_add_new: '새 카드 추가하기',
            btn_continue: '확인하기',
            name: {
              label: 'Name on Card',
              place_holder: '',
            },
            number: {
              label: '카드 번호',
              place_holder: ''
            },
            expiry: {
              label: '유효기간',
              place_holder: '월월/년년'
            },
            code: {
              label: 'Security Code',
              place_holder: '',
              tooltip: ''
            }
          },
          iPay88: {
            note: 'You will be redirected to the ipay88 system to complete the payment.',
          }
        },

      // sign-in.component
      sign_in: {
        email_form: {
          your_email: 'Enter your Email',
          or: 'Or',
          signin_facebook: '페이스북으로 로그인하기',
          signup_facebook: '페이스북으로 가입하기',
          note: '알림:',
          note_content: '걱정하지 마세요. 사전 승인 또는 동의 없이 여러분의 정보는 게재되지 않습니다.',
          btn_continue: 'Continue',
        },
        password_form: {
          welcome_back: 'Welcome back,',
          not_you: 'Not you?',
          change_email: 'Change email',
          password: '비밀번호',
          forgot_pw: '비밀번호를 잊으셨나요?',
          btn_continue: 'Continue',
        },
        signup_form: {
          welcome: 'Welcome',
          not_you: 'Not you?',
          change_email: 'Change email',
          name: '이름',
          first_nm: '이름',
          last_nm: '성',
          password: '비밀번호',
          new_pw: 'Create New Password',
          re_enter_pw: '비밀번호 재입력하기',
          date_of_birth: 'Date of Birth',
          iam: "I'm a",
          gender_male: '남성',
          gender_female: '여성',
          offers: 'Receive Shaves2u offers and updates',
          minimum_8char: 'Minimum 8 characters',
          btn_continue: 'Continue',
          note_content: 'By creating an account, you accept our',
          note_content_terms: 'Terms of Use',
          note_content_and: 'and',
          note_content_policy: 'Privacy Policy',
          signup: '가입하기',
          create_account: '계정 만들기',
          placehdr_email: '이메일',
          placehdr_first_nm: '이름',
          placehdr_last_nm: '성',
          placehdr_new_pw: '비밀번호 생성하기',
          placehdr_re_enter_pw: '비밀번호 재입력하기',
          new_customer_note: '계정을 생성하면 결제, 배송 조회 등을 더 빠르게 할 수 있습니다.'
        },
        send_email_form: {
          note_activelink: 'An activation link has been sent to your email.',
          note_nextstep: 'Please click on the link and continue to next step.',
          not_reveive_email: 'Did not receive email?',
          btn_resend: '다시 발송하기',
        },
        email_active_form: {
          congratulations: 'Congratulations!',
          activated: 'Your account has been activated.',
          btn_continue: 'Continue',
        },
        forgot_pw_form: {
          reset_link: "We've sent a reset link to your email.",
          btn_continue: 'Continue',
        }
      },
      // sign-up.component
      sign_up: {
        btn_back: '돌아가기',
        btn_create: 'Create my account',
        note_activelink: 'An activation link has been sent to your email account.',
        btn_resend: 'Send email again',
        btn_continue: 'Continue',
      },
      // delivery-address.component
      address_form: {
        title_shipping: '배송지 주소',
        btn_add_new: 'Add New Address',
        title_billing: '청구지 주소',
        header_add_new: 'Add New Address',
        header_add_new_billing: 'Add New Billing Address',
        header_edit: 'Edit Address',
        first_nm: '이름',
        last_nm: '성',
        contact_num: 'Contact number',
        address: '주소',
        state_region: 'State/Region',
        choose_region: 'Choose a Region',
        town_city: 'Town/City',
        postal_cd: 'Postal Code',
        shipping_addr: '배송지 주소',
        billing_addr: '청구지 주소',
        bill_diff_addr: 'Bill to a different address',
        btn_back: '돌아가기',
        btn_continue: 'Continue',
        btn_save: '변경 내용 저장하기',
        placehdr_fst_nm: '이름 입력하기',
        placehdr_lst_nm: '성 입력하기',
        placehdr_cont_num: 'Enter contact number',
        placehdr_addr: 'Enter address',
        placehdr_town: 'Enter Town/City',
        placehdr_pot_cd: 'Enter Postal Code',
      },
      // payment-method.component
      payment_form: {
        cre_deb_card: '신용/직불 카드',
        ipay88: 'iPay88',
        header_add_card: 'Add card details',
        card_nm: 'Name on card',
        card_num: '카드 번호',
        expiry_dt: '유효기간',
        security_cd: 'Security Code',
        btn_back: '돌아가기',
        btn_continue: 'Continue',
        btn_add_new: '새 카드 추가하기',
        note_ipay88: 'You will be redirected to the ipay88 system to complete the payment.',
        placehdr_card_nm: 'Enter name on card',
        placehdr_card_num: 'Enter card number',
        placehdr_security_cd: 'Enter security code',
      },
      // summary-place-order.component
      summary_place_order: {
        btn_pay_now: 'Pay Now',
        free_trial_info: {
          heading: '무료체험',
          details_text: '<font color="#3A6E9C"><strong>{{name}}</strong></font> 구독이  <font color="#3A6E9C"><strong>{{price}}</strong></font>에 <font color="#3A6E9C"><strong>{{date}}</strong></font>에 시작합니다',
          details_text_no_start_date: '<font color="#3A6E9C"><strong>{{name}}</strong></font> 계정으로 신청된 정기구매는 <font color="#3A6E9C"><strong>{{price}}</strong></font>에 <font color="#3A6E9C"><strong>{{date1}} - {{date2}}</strong></font>사이에 시작됩니다.',
          details_text_1: 'Your subscription for ',
          details_text_2: ' at ',
          details_text_3: ' every ',
          details_text_4: ' starts '
        }
      },
      // thankyou.component
      thankyou: {
        thankyou: '감사합니다!',
        view_order: '주문번호는 <a class="code click-able" href="/user/orders/{{orderId}}">{{orderNumber}}</a>입니다.',
        estimated_delivery_kor: '배송예정일은 <strong>{{deliveryDateFrom}}–{{deliveryDateTo}}</strong>입니다.</br>궁금한 점이 있으시면 <a href="mailto:help.kr@shaves2u.com">help.kr@shaves2u.com</a>으로 문의해주세요',
        return_to_home: 'RETURN TO HOME',
        complete_signup: {
          description: 'Complete your account sign up fo faster checkout process on your next purchase.',
          btn: 'COMPLETE SIGN UP',
        },
        header_order: '주문',
        link_check_ship_order: 'Check your shipping order',
        item: '제품',
        price: '가격',
        quantity: '수량',
        total: '합계',
        promo_cd: '프로모션 코드',
        none: '없음',
        discount: '할인',
        subtotal: '소계',
        shipping_cost: '배송비',
        free: '무료',
        total_incld_tax: '총액 (세금 포함)',
        grand_total: '총액',
        order_details: '주문정보',
        order_refer: 'Order reference:',
        note_lorem: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt condimentum imperdiet.',
        subscription: 'Subscription',
        banner: 'Banner',
        link_try: 'Try Now',
        subs_start: 'Subscription Starts',
      },
    },
    // product-detail.component
    product_detail: {
      quantity: '수량',
      btn_add_releated: 'Add',
      btn_add: '장바구니 담기',
      btn_see_details: 'SEE DETAILS',
      delivery: '<strong>{{currency}}{{price}}</strong> 이상이면 무료 배송됩니다.',
      handles: '저희 모든 카트리지는 저희 모든 손잡이와 호환가능합니다.',
      add_cart: {
        header: '장바구니 담기',
        header_info: '1 new item has been added to your cart',
        header_cart: '장바구니',
        free: '무료',
        discount: '할인',
        subtotal: '소계',
        shipping_cost: '배송비',
        total_incld_tax: '총액 (세금 포함)',
        grand_total: '총액',
        btn_checkout: '결제하기',
        btn_cont_shop: '쇼핑 계속하기',
        suggest: 'You might also like',
        btn_add: '장바구니 담기',
      }
    },
    // product-list.component
    product_list: {
      header_highlight: 'Highlight',
      header_awesome: 'Awesome Shave Kit',
      note_vivamus: 'Vivamus aliquam cursus nisl vitae ullamcorper.',
      link_buy: 'Buy this',
    },
    support: {
      // faq.component
      faq: {
        contacts: '연락처',
        view_map: 'View in Google Map',
        work_calendar: '공휴일 외 월-금 오전 9시-오후6시.',
        available_at: 'Available',
        email_note: 'Typically replies within 2-3 working days.',
        address: '서울시강남구역삼동746, PMK빌딩1층',
      }
    },
    user: {
      // user.component
      link_subscription: '쉐이브플랜',
      link_order: '주문 목록',
      link_settings: '설정',
      user_signout: '나가기',
      order: {
        // user-order-detail.component
        detail: {
          header_history: '주문 목록',
          order_id: '주문 ID',
          link_track_order: '주문 조회',
          link_close: '돌아가기',
          placed_on: '에 접수',
          subtotal: '소계',
          shipping_cost: '배송비',
          discount: '할인',
          total_incld_tax: '총액 (세금 포함)',
          grand_total: 'Grand Total',
          free: '무료',
          promo_cd: '프로모션 코드',
          none: '없음',
          payment_method: '결제수단',
          cre_deb_card: '신용/직불 카드',
          ipay88: 'iPay88',
          shipping_details: '배송지 주소',
          item: '제품',
          price: '가격',
          quantity: '수량',
          total: '합계',
          order_state_on_hold: '정지',
          order_state_payment: 'Payment Received',
          order_state_processing: 'Processing Order',
          order_state_deliver: 'Deliver Order',
          order_state_received: '완료됨',
          order_state_cancel: '취소하기',
        },
        // user-order-list.component
        list: {
          header_history: '주문 목록',
          date: '날짜',
          order_id: '주문 번호',
          items: '제품',
          price: '가격',
          status: '상태',
          total: '합계',
          link_view_details: '정보',
          empty_order: '주문 내역이 존재하지 않습니다.',
          shop_now: '쇼핑하기',
          txt_item: '상품',
          txt_items: '제품',
        }
      },
      // setting.component
      setting: {
          user_details: '사용자 정보',
          email: '이메일',
          pass: '비밀번호',
          birthday: 'Date of Birth',
          shipping_details: '배송지 주소',
          link_add_addr: 'Add New Address',
          billing_details: '청구지 주소',
          payment_details: '카드 정보',
          link_add_card: '새 카드 추가하기',
          prefix_card_item : 'CARD',
          prefix_address_item : '주소',
          text_default: '기본',
          primary_card: 'PRIMARY',
          btn_add_address: 'ADD ADDRESS',
          btn_add_address_shipping: '배송지 추가하기',
          btn_add_address_billing: '청구지 추가하기',
          btn_add_card: '카드 추가하기',
          primary_shipping: 'Primary Shipping',
          primary_billing: 'Primary Billing',
          primary_payment_method: '주결제수단',
          card_default: '기본 카드로 설정하기',
          footer: {
            heading: 'Terminate Account',
            note: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries ",
            btn_delete_account: 'DELETE MY ACCOUNT'
          },
          delete_address: '이 주소를 삭제하시겠습니까?',
          delete_payment: '계정에서 선택된 카드정보를 삭제하시겠습니까?',
          btn_delete: '삭제',
        // create-address-modal.component
        // edit-address-modal.component
        address_modal: {

           make_as_default_checkbox: '기본 주소로 설정',
           make_as_default_note: '새로운 구매 또는 신청을 할 때마다 기본 주소로 설정됩니다. 나중에 설정을 변경할 수 있습니다.',

         },
        // create-card-modal.component
        card_modal: {
          header_add_card: '카드 추가하기',
          card_nm: 'Name on card',
          card_num: '카드 번호',
          expiry_dt: '유효기간',
          security_cd: 'Security Code',
          btn_add_new: '새 카드 추가하기',
          placehdr_card_nm: 'Enter name on card',
          placehdr_card_num: 'Enter card number',
          placehdr_security_cd: 'Enter security code',

          make_as_default_checkbox: '기본 카드로 설정하기',
          make_as_default_note: '새로운 구매 또는 신청을 할 때마다 기본 카드로 설정됩니다. 나중에 설정을 변경할 수 있습니다. 모든 신청은 기본 설정된 카드 하나로 연결됩니다.',
        },
        // edit-user-modal.component
        user_modal: {
          header_profile: '프로필 수정하기',
          first_nm: '이름',
          last_nm: '성',
          email: '이메일',
          birthday: '생일',
          gender: '성별',
          gender_male: '남성',
          gender_female: '여성',
          country: '국가',
          change_pw: '비밀번호 변경하기',
          old_pw: 'Old Password',
          current_pw: '현재 비밀번호',
          new_pw: '새 비밀번호',
          btn_save_chg: '변경 내용 저장하기',
          placehdr_fst_nm: '이름 입력하기',
          placehdr_lst_nm: '성 입력하기',
          placehdr_email: '이메일 주소 입력하기',
          placehdr_old_pw: 'Enter old password',
          placehdr_new_pw: 'Enter new password',
          re_enter_pw: '비밀번호 재입력하기',
          pw_updated: 'Your password has been updated successfully'
        }
      },
      subscription: {
        // user-subscription-history.component
        details: '정보',
        update: '업데이트',
        edit_plan_details: '쉐이브플랜 수정하기',
        cancel_shave_plan: '쉐이브플랜 취소하기',
        subscribed: '귀하의 신청 내용입니다.',
        start_from: 'started from',
        cancel_email_kor: '신청을 취소하려면 <a href="mailto:help.kr@shaves2u.com">help.kr@shaves2u.com</a> ',
        cancel_phone: '또는 <a href="tel:{{phone1}}">{{phone2}}</a>로 연락주세요.',
        empty_subscription_title: '쉐이브플랜이 없으시군요.',
        empty_subscription: '저희 제품에 만족하시면 여러분에게 맞춘 쉐이브플랜으로 더 많이 할인 받을 수 있는 방법을 확인해보세요.',
        payment_updated: 'Your payment method has been updated successfully',
        subscribe_now: ' 더 알아보기',
        payment_method:'결제수단',
        ending_in: '카드 번호',
        expired_on: '유효기간',
        trial_expiring_in: '무료 체험 (유효기간 {{value}})',
        trial_expiring_in_14_days: '무료 체험기간 (트라이얼 킷을 수령하시고 14일 후에 만료됩니다)',
        next_shipment: '다음 배송',
        items: '제품',
        charges: '가격',
        charges_on_date: '{{currency}}{{price}} 에 {{date}}',
        charges_on_14_days: '무료체험 제품을 수령하시고 14일 후에 {{currency}}{{price}}가 청구됩니다.',
        status: '상태',
        on:'에',
        delete_payment: '<font color="#fe5000">{{value}}</font>로 끝나는 신용 카드를 제거합니다. 확실합니까?',
        history: {
          header_hist: 'Subscription History',
          date: '날짜',
          subscription_nm: 'Subscription Name',
          order_no: '주문 번호',
          price: '가격',
          status: '상태',
          link_view_detail: '정보',
        },
        // user-subscription-overview.component
        overview: {
          header: 'Premium member',
          note_1: 'You are currently on Monthly Subscription Plan.',
          note_2: '( Active until 20 JULY 2018.)',
          btn_update: '업데이트',
        },
        // user-subscription-view-detail.component
        view_detail: {
          header_detail: 'Subscription detail',
          btn_back: '돌아가기',
          btn_details: '정보',
          change_address: 'Change Address',
          change_card: 'Change Card',
          title: {
            plan_history: '쉐이브플랜 기록',
            plan_type: 'Plan Type',
            payment_type: '결제 종류',
            billing_cycle: 'Billing Cycle',
            next_billing: '다음 결제',
            next_shipment: '다음 배송',
            start_date: 'Start Date',
            address: '주소',
            payment: '결제수단',
            shipping: '배송지',
            billing: '청구지',
            ending_in: '카드 번호',
            expired_on: '유효기간',
          },
          free_trial: {
            select_blade: '면도날',
            refill: {
              heading: 'Refill',
            },
            not_sure: '확실하지 않으신가요?',
            payment_type: {
              heading: '결제 종류',
              pay_as_you_go: '선불'
            },
            usage: {
              heading: '이용',
            },
            addon_shave_cream: {
              heading: '쉐이브 크림 추가하기',
              yes: '네',
              no: '아니오',
            },
            trial_subs_cancelled_handling : 'Cancellation in progress',
          },
        }
      }
    },
    subscription: {
      detail: {
        // subscription-detail.component
        quantity: '수량',
        btn_add: '장바구니 담기',
      }
    },
    modal: {
      // confirm-deletion-modal.component
      confirm_deletion: {
        header: 'Confirm Delete',
        note: 'Do you want to delete?',
        yes: '네',
        no: '아니오',
        cancel: '취소하기'
      }
    },
    shave_plans: {
      block1: {
        // title: 'Stay sharp with a<br><font color="#fe5000">Shave Plan</font>',
        // title: 'Try the Shave Plan.&nbsp;<font color="#fe5000">Free.</font>',
        title: 'try the shave plan for free',
        // note: 'One less thing to think about. Many more shaves to grin about.',
        // note: 'Subscribe and shave off up to 40%.',
        perfect_shave: 'Subscription shaving kits and razors for a perfect shave',
        note: '무료체험을 신청하세요',
        note_desc: '여러분께 직접 배송됩니다. <br>배송비만 내면 됩니다.',
        get_started: 'Get Started',
        start_a_trial: '무료체험 시작하기',
        btn_start_free_trial: '무료체험 시작하기',
        how_it_work: 'How does it work?',
      },
      block2: {
        how_does_work: 'How does the Free Trial work?',
        sign_up: '무료로 시작해보세요.',
        sign_up_note: '무료로 저희 제품으로 면도해보세요. {{shippingFee}} 배송비만 내면 됩니다.',
        shave_up: '매번 알뜰하게 구매하세요.',
        shave_up_note: '항상 더 높은 품질의 면도기를 더 저렴한 가격으로 이용할 수 있습니다.',
        switch_it_up: '선택하세요.',
        switch_it_up_note: '언제든지 프로필 페이지에서 쉐이브플랜을 수정하거나 취소하세요.',
        start_shave_plan: 'Start A Shave Plan',
        get_start: '무료체험 시작하기',
        so_what_the_plan: 'So what’s <font color="#fe5000">the plan?</font>',
        choose_your_plan: 'Choose your plan',
        what_you_get: 'What you’ll get',
      },
      block3: {
        title: '쉐이브플랜 내용',
        first: {
          title: '첫 배송',
          title_h2: '무료로 체험해보세요',
          title_h3: '무료체험',
          title_desc: '{{shippingFee}} 배송비만 내면 됩니다.',
          desc: '직접 사용하신 후 결정하세요. <br>최대 2주 동안 체험하실 수 있습니다.',
          first_delivery: 'First Delivery',
          trial_pack: 'Free Trial Pack',
          includes: 'Includes:',
          list_1: '1 x Men’s Premium Handle',
          list_2: '1 x 3-Blade & 5-Blade Cartridge',
          list_3: '1 x Shave Cream (100g)',
          // note: '*Women\'s Shave Plans do not contain Shave Cream'
        },
        subsequent: {
          title: '다음 배송',
          title_h2: '면도와 절약을 한번에',
          title_h3: '쉐이브플랜',
          title_desc: '정기 구매를 신청하시고 할인 받으세요.',
          desc: '원하시는 주기로 면도날을 받아보세요. <br>무료로 배송됩니다.',
          subsequent_delivery: 'Subsequent Deliveries',
          includes: 'Includes:',
          list_1: '1 x Cartridge Pack every 2/3/4/12 months*.',
          note: '*Based on your selected frequency.'
        },
        get_started: 'get started',
      },
      block4: {
        title: 'S2U 면도기가 우수한 이유',
        note_1: '3중, 5중, 6중 면도날 카트리지의 다양한 옵션',
        note_2: '예리하게 다져진 정교한 탄소강 면도날',
        note_3: '자극으로부터 피부를 보호하는 촉촉한 윤활밴드',
        // note_3: 'Every Shaves2U blade is built to last. No compromise.',
        start_shave_plan: 'Start A Trial',
      },
      block5: {
        title: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit.',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        // note_3: 'Every Shaves2U blade is built to last. No compromise.',
        start_shave_plan: 'Start A Trial',
      },
      // block5: {
      //   rectangle_1: {
      //     title: 'Other Off-the-Shelf Shaving Brands',
      //     shaver: '1x Shaver',
      //     shaver_price_mys: 'RM17',
      //     shaver_price_sgp: 'S$6',
      //     cartridges: '16x Cartridges',
      //     cartridges_price_mys: 'RM164',
      //     cartridges_price_sgp: 'S$50',
      //     shave_cream: '1x Shave Cream',
      //     shave_cream_price_mys: 'RM15',
      //     shave_cream_price_sgp: 'S$9',
      //     total_price_mys: 'RM196',
      //     total_price_sgp: 'S$65',
      //     per_year: 'per year',
      //   },
      //   rectangle_2: {
      //     title: 'Shaves2U 3-Blade Shave Plan',
      //     shaver: '1x Shaver',
      //     cartridges: '16x Cartridges',
      //     shave_cream: '1x Shave Cream',
      //     total_price_mys: 'RM108',
      //     total_price_sgp: 'S$44',
      //     per_year: 'per year',
      //   },
      //   note: '*Prices are indicative and are not to be taken as exact figures.',
      //   start_saving: 'Start Saving',
      // },
      block6: {
        frequently_asked: 'Frequently Asked Questions',
      },
      block7: {
        looking: '다른 것을 찾으시나요?',
        subscribe: '무료체험 없이 쉐이브플랜을 신청하세요',
        not_subscribe: '아직 신청하지 않으셨나요? 다른 제품을 확인해보세요.',
      }
    },
    trial_plan: {
      review_order: '주문 보기',
      not_sure: '확실하지 않으신가요?',
      block1: {
        headline: '무료체험 구성',
        headline_korea: '무료체험 상품의 구성은 어떻게 되어있을까요?',
        quote1: '면도날 카트리지 및 프리미엄 손잡이 ',
        quote2: '쉐이브 크림',
        quote3: '예상 배송일 - 2018년 10월 초',
        note: '{{shippingFee}} 배송비'
      }
    }
  }
};
