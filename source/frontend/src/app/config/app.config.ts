import { Inject, Injectable, Output , EventEmitter} from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BaseConfig } from './base.config';
import { StorageService } from '../core/services/storage.service';
import { RequestService } from '../core/services/request.service';
import { CountriesService } from '../core/services/countries.service';
import env from './env';

@Injectable()
export class AppConfig {
  @Output() configEvent: EventEmitter<Object> = new EventEmitter();
  public config: any = null;
  private env: any = null;
  private baseConfig: any;

  constructor(private http: Http, public storage: StorageService, private request: RequestService, private countriesService: CountriesService) {
    countriesService.country.subscribe(country => {
      this.getStripeConfig();
      this.getShippingFeeConfig();
    });
  }

  /**
   * Use to get the data found in the second file (config file)
   */
  public getConfig(key: any) {
    return key ? this.config[key] : this.config;
  }

  /**
   * Use to get the data found in the first file (env file)
   */
  public getEnv(key: any) {
    return this.env[key];
  }

  private getWorkingEnv() {
    return new Promise((resolve, reject) => {
      // this.http.get('./../../app/config/env.json').map(res => res.json()).catch((error: any): any => {
      //   // console.log('Configuration file "env.json" could not be read');
      //   resolve(true);
      //   return Observable.throw(error.json().error || 'Server error');
      // }).subscribe((envResponse) => {
      //   resolve(envResponse);
      // });
      // console.log(env);
      resolve({env: env.env});
    });
  }

  private getEnvConfig(envResponse) {
    this.env = envResponse.env || 'local';
    return new Promise((resolve, reject) => {
      let request: any = null;

      this.baseConfig = new BaseConfig(this.env);
      this.config = this.baseConfig.getEnvironmentConfig(this.env);
      this.config = Object.assign(this.config, this.baseConfig.getConfig());
      resolve(true);
      // console.log('app finish');
    });
  }

  private getStripeConfig() {
    return new Promise((resolve, reject) => {
      // set country code to header
      this.http.get(`${this.config.api.stripe_pubkey}?type=website`, {headers: this.request.getHeaders('json')})
        .map(res => res.json())
        .subscribe((responseData) => {
          this.config.stripePubkey = responseData.stripePub;
          this.configEvent.emit(this.config);
          resolve(true);
        });

      // let countryCode = this.storage.getCountry().code;
      // if(!countryCode || !this.config.keyStripe.hasOwnProperty(countryCode.toUpperCase())) {
      //   this.config.stripePubkey = this.config.keyStripe.default['website'].publishableKey;
      // } else {
      //   this.config.stripePubkey = this.config.keyStripe[countryCode.toUpperCase()]['website'].publishableKey;
      // }
      // this.configEvent.emit(this.config);
      // resolve(true);
    });
  }

  private getShippingFeeConfig() {
    return new Promise((resolve, reject) => {
      let countryCode = this.storage.getCountry().code;
      // this.config.shippingFeeConfig = this.baseConfig.getShippingFee(countryCode);
      // this.configEvent.emit(this.config);
      // resolve(true);
      this.http.get(this.config.api.shipping_fee_configuration, {headers: this.request.getHeaders('json')})
        .map(res => res.json())
        .subscribe((responseData) => {
          this.config.shippingFeeConfig = responseData;
          this.configEvent.emit(this.config);
          resolve(true);
        });
    });
  }

  /**
   * This method:
   *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
   *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
   */
  public load() {
    return this.getWorkingEnv()
      .then(envResponse => this.getEnvConfig(envResponse))
      .then(() => this.getStripeConfig())
      .then(() => this.getShippingFeeConfig());
  }
}