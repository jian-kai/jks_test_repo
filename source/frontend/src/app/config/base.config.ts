export class BaseConfig {
  private config: any;
  private environmentConfig: any;
  private shippingFee: any;

  constructor(private env: any) { 

    this.shippingFee = {
      MYS: {
        minAmount: 75,
        fee: 6.5,
        trialFee: 6.5
      },
      SGP: {
        minAmount: 30,
        fee: 5,
        trialFee: 5
      },
      HKG: {
        minAmount: 0,
        fee: 0,
        trialFee: 0
      },
      KOR: {
        minAmount: 0,
        fee: 0,
        trialFee: 0
      }
    }

    this.environmentConfig = {
      local: {
        "apiURL": "http://localhost:8080",
        "baseUrl": "http://localhost:8081",
        "iPay88PostUrl": "http://smartshave.bjdev.net/payment-gateway/ipay88",
        "iPay88OriginUrl": "http://smartshave.bjdev.net",
        "facebook": {
          "appId" : "363802884052208"
        },
        "stripe": {
          "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
        },
        "keyStripe": {
          "MYS": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "SGP": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "default": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          }
        }
      },
      development: {
        "apiURL": "http://128.199.201.237:8080",
        "baseUrl": "http://128.199.201.237:8080",
        "iPay88PostUrl": "http://128.199.201.237:8080/payment-gateway/ipay88",
        "iPay88OriginUrl": "http://128.199.201.237:8080",
        "facebook": {
          "appId" : "186012911968259"
        },
        "stripe": {
          "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
        },
        "keyStripe": {
          "MYS": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "SGP": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "default": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          }
        }
    },
      staging: {
        "apiURL": "http://smartshave.bjdev.net",
        "baseUrl": "http://smartshave.bjdev.net",
        "iPay88PostUrl": "http://smartshave.bjdev.net/payment-gateway/ipay88",
        "iPay88OriginUrl": "http://smartshave.bjdev.net",
        "facebook": {
          "appId" : "333076577140986"
        },
        "stripe": {
          "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
        },
        "keyStripe": {
          "MYS": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "SGP": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          },
          "default": {
            "secretKey": "sk_test_JVeKQl2KYQqoZ23audsfBuqO",
            "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
          }
        }
    },
      production: {
        "apiURL": "https://shaves2u.com",
        "baseUrl": "https://shaves2u.com",
        "iPay88PostUrl": "https://shaves2u.com/payment-gateway/ipay88",
        "iPay88OriginUrl": "https://shaves2u.com",
        "facebook": {
          "appId" : "285747145163644"
        },
        "stripe": {
          "publishableKey": "pk_test_bWhMz8ITOiIbGYpOjiENwops"
        },
        "keyStripe": {
          "MYS": {
            "secretKey": "sk_live_WOUr69qqLttZMuj2T6MczL8e",
            "publishableKey": "pk_live_kGzoZdvfBDQn3H9Ith4ZgaD8"
          },
          "SGP": {
            "secretKey": "sk_live_WOUr69qqLttZMuj2T6MczL8e",
            "publishableKey": "pk_live_kGzoZdvfBDQn3H9Ith4ZgaD8"
          },
          "default": {
            "secretKey": "sk_live_WOUr69qqLttZMuj2T6MczL8e",
            "publishableKey": "pk_live_kGzoZdvfBDQn3H9Ith4ZgaD8"
          }
        }
      }
    }

    this.config = {
      apiURL: this.environmentConfig[env].apiURL,
      baseUrl: this.environmentConfig[env].baseUrl,
      multipleLanguage: {
        countryCodeService: 'http://ws.geonames.org/countryCode',
        username: 'kimthi'
      },
      api: {
        login: this.environmentConfig[env].apiURL + '/api/users/login',
        activeLogin: this.environmentConfig[env].apiURL + '/api/users/active-login',
        register: this.environmentConfig[env].apiURL + '/api/users/register',
        signinFB: this.environmentConfig[env].apiURL + '/api/users/signinFB',
        reset_password: this.environmentConfig[env].apiURL + '/api/users/reset-password',
        users: this.environmentConfig[env].apiURL + '/api/users',
        checkout: this.environmentConfig[env].apiURL + '/api/checkout',
        // delivery_address_list: this.environmentConfig[env].apiURL + '/api/users/[]/deliveries',
        delivery_address: this.environmentConfig[env].apiURL + '/api/users/[userID]/deliveries',
        cards: this.environmentConfig[env].apiURL + '/api/users/[userID]/cards',
        // edit_delivery_address: this.environmentConfig[env].apiURL + '/api/users/[userID]/deliveries',
        ipay88_payment_method: this.environmentConfig[env].apiURL + '/payment-gateway/ipay88',
        products: this.environmentConfig[env].apiURL + '/api/products',
		products_ask: this.environmentConfig[env].apiURL + '/api/products-ask',
        product_types: this.environmentConfig[env].apiURL + '/api/product-types',
        articles: this.environmentConfig[env].apiURL + '/api/articles',
        article_types: this.environmentConfig[env].apiURL + '/api/article-types',
        articles_featured: this.environmentConfig[env].apiURL + '/api/articles-featured',
        carts: this.environmentConfig[env].apiURL + '/api/users/[userID]/carts',
        order: this.environmentConfig[env].apiURL + '/api/users/[userID]/orders',
        place_order: this.environmentConfig[env].apiURL + '/api/payment/orders',
        pendding_order: this.environmentConfig[env].apiURL + '/api/users/[userID]/orders/pending',
        history_order: this.environmentConfig[env].apiURL + '/api/users/[userID]/orders/history',
        subscription: this.environmentConfig[env].apiURL + '/api/users/[userID]/subscriptions',
        plan_type: this.environmentConfig[env].apiURL + '/api/plan-types',
        plan: this.environmentConfig[env].apiURL + '/api/plans',
        countries: this.environmentConfig[env].apiURL + '/api/countries',
        stripe_pubkey: this.environmentConfig[env].apiURL + '/api/countries/stripe-pubkey',
        stripe_checkout: this.environmentConfig[env].apiURL + '/payment-gateway/stripe',
        checkEmail: this.environmentConfig[env].apiURL + '/api/users/check-email',
        resendActiveEmail: this.environmentConfig[env].apiURL + '/api/users/send-active-email',
        validate_token: this.environmentConfig[env].apiURL + '/api/users/validate-token',
        update_password: this.environmentConfig[env].apiURL + '/api/users/update-password',
        faqs: this.environmentConfig[env].apiURL + '/api/faqs',
        shipping_fee_configuration: this.environmentConfig[env].apiURL + '/api/countries/shipping-fee',
        check_promo_code: this.environmentConfig[env].apiURL + '/api/promotions/check',
        cart_rules: this.environmentConfig[env].apiURL + '/api/promotions/cart-rules',
        directory_countries: this.environmentConfig[env].apiURL + '/api/directory-countries',
        detect_current_location: this.environmentConfig[env].apiURL + '/api/countries/current-country',
        delivery_address_byGuest: this.environmentConfig[env].apiURL + '/api/users/deliveries',
        seo_configuration: this.environmentConfig[env].apiURL + '/api/SEO-configuration',
        free_trial: this.environmentConfig[env].apiURL + '/api/plans/free-trial',
        place_order_trial: this.environmentConfig[env].apiURL + '/api/payment/trial-plan',
        subscription_trial: this.environmentConfig[env].apiURL + '/api/users/[userID]/trial-plan',
        cancel_trial_plan: this.environmentConfig[env].apiURL + '/api/subscriptions',
        revert_cancel_trial_plan: this.environmentConfig[env].apiURL + '/api/subscriptions/revert',
        non_ecommerce: this.environmentConfig[env].apiURL + '/api/non-ecommerce',
        terms: this.environmentConfig[env].apiURL + '/api/terms',
        privacy: this.environmentConfig[env].apiURL + '/api/privacy',
        place_order_custom_plan: this.environmentConfig[env].apiURL + '/api/payment/custom-plan',
		place_order_awesome_shave_kit: this.environmentConfig[env].apiURL + '/api/payment/awesome-shave-kit',
        deliver_date_for_korea: this.environmentConfig[env].apiURL + '/api/deliver-date-for-korea',
        subscription_custom: this.environmentConfig[env].apiURL + '/api/users/[userID]/custom-plan',
        message: this.environmentConfig[env].apiURL + '/api/message',
        user_order_list: this.environmentConfig[env].apiURL + '/api/user-order-list',
      },
      auth: {
        facebook: {
          authEndpoint: this.environmentConfig[env].baseUrl + '/auth/facebook',
          redirectURI : this.environmentConfig[env].baseUrl + '/auth'
        }
      },
      supportedLanguage: [
        {
          key: 'en',
          value: 'Malaysia',
          countryCode: 'MY'
        },
        {
          key: 'en',
          value: 'Singapore',
          countryCode: 'SG'
        },
        {
          key: 'en',
          value: 'Thailand',
          countryCode: 'TH'
        },
        {
          key: 'vn',
          value: 'Vietname',
          countryCode: 'VN'
        }
      ],
      trial_payment_types: ['Annual', 'Pay As You Go'],
      zendeskHost: 'shaves-2u.zendesk.com',
    };
  }

  public getConfig() {
    return this.config;
  }

  public getEnvironmentConfig(env) {
    return this.environmentConfig[env];
  }

  public getShippingFee(env) {
    return this.shippingFee[env];
  }
}