import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { HelpComponent } from './help.component';

// Modules
import { SharedModule } from './../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: HelpComponent }
];


@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    // FaqAnswerModule
    
  ],
  declarations: [
    HelpComponent
  ],
})
export class HelpModule { }
