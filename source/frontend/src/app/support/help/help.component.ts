import { Component, OnInit, ElementRef } from '@angular/core';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';

import $ from "jquery";

import { HttpService } from '../../core/services/http.service';
import { AppConfig } from '../../config/app.config';
import { GlobalService } from '../../core/services/global.service';
import { RouterService } from '../../core/helpers/router.service';
import { CountriesService } from '../../core/services/countries.service';
import { LoadingService } from '../../core/services/loading.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  selectedType: string;
  typeList: Array<string> = [];
  detailList: Array<any> = [];
  isLoading: boolean = true;
  public id;

  constructor(
    el: ElementRef,
    private loadingService: LoadingService,
    public storage: StorageService,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private routerService: RouterService,
    private location: Location,
    public countriesService: CountriesService) {

    // set info
    this.getInitFaq();
    // set default type
    // this.selectedType = this.camelize("frequently asked");

    this.countriesService.language.subscribe(
      data => {
        this.getInitFaq();
      }
    )
  }

  ngOnInit() {
    this.routerService.setFooterAssuranceSection(false);
    this.routerService.setFooterAssuranceSection(false);
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    if (params && params.get('id')) {
      this.id = params.get('id');
    }
  }

  // get all faqs info
  getInitFaq() {
    this.loadingService.display(true);
    this.typeList = [];
    this.detailList = [];
    this._http._getList(`${this.appConfig.config.api.faqs}?langCode=${this.storage.getLanguageCode()}`).subscribe(
      data => {
        // this.typeList.push("frequently asked");
        if (data.length > 0) {
          data.forEach((item, index) => {
            this.typeList.push(item.type);
            item.faqTranslate.forEach(element => {
              this.detailList.push(element);
            });
          });

          // // console.log('faqsList', this.typeList);
          // // console.log('detailList', this.detailList);

          // set default type
          if (this.id) {
            this.selectedType = data[this.id - 1].type;
          } else {
            this.selectedType = data[0].type;
          }
          this.getInfoByType(this.selectedType);
        }

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // get faqs info by type
  getInfoByType(type: string) {
    this.selectedType = type;

    // if(type === "frequently asked") {
    //   this.getAllInfo();
    //   return;
    // }

    this.loadingService.display(true);
    this.detailList = [];

    let options = {
      type: type,
      langCode: this.storage.getLanguageCode()
    };
    let params = this.globalService._URLSearchParams(options);
    this._http._getList(`${this.appConfig.config.api.faqs}?` + params.toString()).subscribe(
      data => {

        data.forEach((item, index) => {
          item.faqTranslate.forEach(element => {
            if (type === 'Delivery / Shipping' && element.question === 'Do you offer free delivery?') {
              if (this.storage.getCountry().code === 'SGP') {
                element.answer = element.answer.replace('RM75 or ', '');
              } else {
                element.answer = element.answer.replace(' or SGD30', '');
              }
            }
            if (type === 'Payment' && element.question === 'What forms of payment does Shaves2U accept?') {
              if (this.storage.getCountry().code === 'SGP') {
                element.answer = element.answer.replace(' <img src="https://d8pi1a7qa1dfv.cloudfront.net/assets/images/ipay88_logo.png" style="width:60px; margin-right:10px;">', '');
              }
            }
            this.detailList.push(element);
          });
        });

        // // console.log('faqsList', this.typeList);
        // // console.log('detailList', this.detailList);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
      if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
      return index == 0 ? match.toUpperCase() : " " + match.toUpperCase();
    });
  }

}
