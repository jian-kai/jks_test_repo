import { Component, OnInit, ElementRef } from '@angular/core';

import { LoadingService } from '../../core/services/loading.service';
import { StorageService } from '../../core/services/storage.service';

import $ from "jquery";

import { HttpService } from '../../core/services/http.service';
import { AppConfig } from '../../config/app.config';
import { GlobalService } from '../../core/services/global.service';
import { CountriesService } from '../../core/services/countries.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  selectedType: string;
  faqsTypeList: Array<string> = [];
  faqsDetailList: Array<any> = [];
  isLoading: boolean = true;

  constructor(
    el: ElementRef,
    private loadingService: LoadingService,
    public storage: StorageService,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public countriesService: CountriesService) {

    // set faqs info
    this.getInitFaq();

    // set default type
    // this.selectedType = this.camelize("frequently asked");

    this.countriesService.language.subscribe(
      data => {
        this.getInitFaq();
      }
    )
  }

  ngOnInit() { }

  // get all faqs info
  getInitFaq() {
    this.loadingService.display(true);
    this.faqsTypeList = [];
    this.faqsDetailList = [];
    this._http._getList(`${this.appConfig.config.api.faqs}?langCode=${this.storage.getLanguageCode()}`).subscribe(
      data => {
        // this.faqsTypeList.push("frequently asked");

        data.forEach((item, index) => {
          this.faqsTypeList.push(item.type);
          item.faqTranslate.forEach(element => {
            this.faqsDetailList.push(element);
          });
        });

        // console.log('faqsList', this.faqsTypeList);
        // console.log('faqsDetailList', this.faqsDetailList);

        // set default type
        this.selectedType = data[0].type;
        this.getFaqsInfoByType(this.selectedType);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // get faqs info by type
  getFaqsInfoByType(type: string) {
    this.selectedType = type;

    // if(type === "frequently asked") {
    //   this.getAllFaqsInfo();
    //   return;
    // }

    this.loadingService.display(true);
    this.faqsDetailList = [];

    let options = {
      type: type,
      langCode: this.storage.getLanguageCode()
    };
    let params = this.globalService._URLSearchParams(options);
    this._http._getList(`${this.appConfig.config.api.faqs}?` + params.toString()).subscribe(
      data => {

        data.forEach((item, index) => {
          item.faqTranslate.forEach(element => {
            this.faqsDetailList.push(element);
          });
        });

        // console.log('faqsList', this.faqsTypeList);
        // console.log('faqsDetailList', this.faqsDetailList);
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
      if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
      return index == 0 ? match.toUpperCase() : " " + match.toUpperCase();
    });
  }

}
