import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { CurrencyPipe } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { routes } from './core/app.routes';

// import service
import { StorageService } from './core/services/storage.service';
import { RequestService } from './core/services/request.service';
import { UsersService } from './auth/services/users.service';
import { ModalService } from './core/services/modal.service';
import { AuthService } from './auth/services/auth.service';
import { HttpService } from './core/services/http.service';
import { StripeService } from './core/services/stripe.service';
import { CheckoutService } from './product/services/checkout.service';
import { AlertService } from './core/services/alert.service';
import { LoadingService } from './core/services/loading.service';
import { CartService } from './core/services/cart.service';
import { GlobalService } from './core/services/global.service';
import { WorkflowService } from './product/components/checkout/workflow/workflow.service';
import { PlanCartService } from './core/services/plan-cart.service';
import { CountriesService } from './core/services/countries.service';
import { DeliveryAddressService } from './core/services/delivery-address.service';
import { DialogService } from './core/services/dialog.service';
import { CardService } from './core/services/card.service';
import { PlanService } from './core/services/plan.service';
import { NotificationService } from './core/services/notification.service';
import { RouterService } from './core/helpers/router.service';
import { GTMService } from './core/services/gtm.service';
import { ZendeskService } from './core/services/zendesk.service';


// import component
import { AppComponent } from './core/components/app/app.component';
import { MenuComponent } from './core/components/partials/header/menu/menu.component';
import { AboutComponent } from './core/components/about/about.component';
// import { LoginComponent } from './auth/components/login/login.component';

//import { ProductListComponent } from './product/components/product-list/product-list.component';

import { SocialLoginComponent } from './auth/components/social-login/social-login.component';

import { SignUpComponent } from './product/components/checkout/sign-up/sign-up.component';
import { HeaderComponent } from './core/components/partials/header/header.component';
import { FooterComponent } from './core/components/partials/footer/footer.component';

// import { ProductDetailComponent } from './product/components/product-detail/product-detail.component';
import { ArticleListComponent } from './article/components/article-list/article-list.component';
import { ArticleDetailComponent } from './article/components/article-detail/article-detail.component';

import { SupportComponent } from './support/support.component';

// import config
import { AppConfig } from './config/app.config';
// import { UserSubscriptionComponent } from './user/components/subscription/user-subscription.component';
// import { SettingComponent } from './user/components/setting/setting.component';
import { ChangeAvatarModalComponent } from './user/components/change-avatar-modal/change-avatar-modal.component';
// import { EditUserModalComponent } from './user/components/setting/edit-user-modal/edit-user-modal.component';
import { EditAddressModalComponent } from './user/components/setting/address-modal/edit-address-modal/edit-address-modal.component';
import { CreateAddressModalComponent } from './user/components/setting/address-modal/create-address-modal/create-address-modal.component';
// import { UserOrderListComponent } from './user/components/order/user-order-list/user-order-list.component';
// import { UserOrderDetailComponent } from './user/components/order/user-order-detail/user-order-detail.component';

// import guard
import { LoggedInGuard } from './core/guards/logged-in.guard';

// import { UserSubscriptionOverviewComponent } from './user/components/subscription/user-subscription-overview/user.subscription-overview.component';
import { UserSubscriptionHistoryComponent } from './user/components/subscription/user-subscription-history/user-subscription-history.component';
// import { UserSubscriptionViewDetailComponent } from './user/components/subscription/user-subscription-view-detail/user-subscription-view-detail.component';

import { SimpleMultiStepComponent } from './core/components/partials/simple-multi-step/simple-multi-step.component';
import { SimpleSubStepComponent } from './core/components/partials/simple-sub-step/simple-sub-step.component';
// import { ThankyouComponent } from './product/components/checkout/thankyou/thankyou.component';
import { CreateCardModalComponent } from './user/components/setting/create-card-modal/create-card-modal.component';

import { LogoutComponent } from './auth/components/logout/logout.component';


import { ShoppingCartComponent } from './core/components/partials/header/shopping-cart/shopping-cart.component';

// import { ChangePasswordModalComponent } from './user/components/setting/change-password-modal/change-password-modal.component';
// import { ChangePlanComponent } from './product/components/change-plan/change-plan.component';
import { NotificationComponent } from './core/components/partials/header/notification/notification.component';

import { ListViewsComponent } from './core/components/list-views/list-views.component';
// import { PaginationComponent } from './core/components/pagination/pagination.component';

import { LoginModalComponent } from './auth/components/login-modal/login-modal.component';
import { NotificationRemindActiveEmailComponent } from './core/components/partials/header/notification-remind-active-email/notification-remind-active-email.component';
import { AskComponent } from './ask/ask.component';

// Modules
import { SharedModule } from "./core/directives/share-modules";

// Add this function
export function initConfig(config: AppConfig) {
  return () => config.load()
}

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AboutComponent,
    // LoginComponent,
  // ProductListComponent,
    SocialLoginComponent,
    SignUpComponent,
    HeaderComponent,
    FooterComponent,
    // ProductDetailComponent,
    ArticleListComponent,
    ArticleDetailComponent,
    SupportComponent,
    ChangeAvatarModalComponent,
    // UserSubscriptionComponent,
    // EditUserModalComponent,
    EditAddressModalComponent,
    CreateAddressModalComponent,
    // UserOrderListComponent,
    // UserOrderDetailComponent,
    // UserSubscriptionOverviewComponent,
    UserSubscriptionHistoryComponent,
    // UserSubscriptionViewDetailComponent,
    SimpleMultiStepComponent,
    SimpleSubStepComponent,
    // ThankyouComponent,
    CreateCardModalComponent,
    LogoutComponent,
    // ChangePasswordModalComponent,
    // ChangePlanComponent,
    ShoppingCartComponent,
    NotificationComponent,
    ListViewsComponent,
    // PaginationComponent,
    LoginModalComponent,
    NotificationRemindActiveEmailComponent,
    AskComponent
  ],

  imports: [
    HttpModule,
    BrowserModule.withServerTransition({ appId: 'shaves2u' }),
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    RouterModule.forRoot(routes, {
      // useHash: true
      // preloadingStrategy: PreloadAllModules
    })
  ],
  providers: [StorageService,
    RequestService,
    UsersService,
    ModalService,
    AuthService,
    HttpService,
    StripeService,
    CheckoutService,
    AlertService,
    LoadingService,
    CartService,
    GlobalService,
    WorkflowService,
    LoggedInGuard,
    PlanCartService,
    CountriesService,
    DeliveryAddressService,
    CardService,
    DialogService,
    PlanService,
    NotificationService,
    RouterService,
    GTMService,
    ZendeskService,
    AppConfig,
    CurrencyPipe,
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true },
  ],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }