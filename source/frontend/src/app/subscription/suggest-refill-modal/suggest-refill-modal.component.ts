import { Component, ElementRef, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
import { numberValidator } from '../../core/directives/number-validator.directive';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { HttpService } from '../../core/services/http.service';
import { StorageService } from '../../core/services/storage.service';
import { UsersService } from '../../auth/services/users.service';
import { AlertService } from '../../core/services/alert.service';
import { LoadingService } from '../../core/services/loading.service';
import { AuthService } from '../../auth/services/auth.service';
import { SimpleSubStepComponent } from '../../core/components/partials/simple-sub-step/simple-sub-step.component';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from 'app/config/app.config';
import { slideInOutCheckoutAnimation } from '../../core/animations/slide-in-out-checkout.animation';
import { CountriesService } from '../../core/services/countries.service';
import { CartService } from '../../core/services/cart.service';
import { PlanService } from '../../core/services/plan.service';
import { GlobalService } from '../../core/services/global.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { DialogService } from '../../core/services/dialog.service';

@Component({
  selector: 'app-suggest-refill-modal',
  templateUrl: './suggest-refill-modal.component.html',
  styleUrls: ['./suggest-refill-modal.component.scss']
})
export class SuggestRefillModalComponent extends ModalComponent {
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,) {
      super(modalService, el);
      this.id = 'app-suggest-refill-modal';
    }

  changeRefillOption(_type) {
    this.hasChange.emit(_type);
    this.modalService.close(this.id);
  }

  closeModal() {
    this.modalService.close(this.id)
  } 

}
