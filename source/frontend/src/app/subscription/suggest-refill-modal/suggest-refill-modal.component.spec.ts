import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestRefillModalComponent } from './suggest-refill-modal.component';

describe('SuggestRefillModalComponent', () => {
  let component: SuggestRefillModalComponent;
  let fixture: ComponentFixture<SuggestRefillModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestRefillModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestRefillModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
