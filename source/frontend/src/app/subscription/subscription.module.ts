import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { SubscriptionComponent } from './subscription.component';

// Modules
import { SharedModule } from './../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: SubscriptionComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [SubscriptionComponent],
})
export class SubscriptionModule { }
