// 
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { CustomPlanComponent } from './custom-plan.component';
// import { SuggestRefillModalComponent } from './../suggest-refill-modal/suggest-refill-modal.component';

// Modules
import { SharedModule } from './../../core/directives/share-modules';


// routes
export const ROUTES: Routes = [
  { path: '', component: CustomPlanComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [CustomPlanComponent],
})
export class CustomPlanModule { }
