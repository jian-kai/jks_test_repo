import { Component, OnInit, Output, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';
import { LoadingService } from '../../core/services/loading.service';
import { PlanService } from '../../core/services/plan.service';
import { DialogService } from '../../core/services/dialog.service';
import { CartService } from '../../core/services/cart.service';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from '../../core/services/global.service';
import { HttpService } from '../../core/services/http.service';
import { AppConfig } from '../../../app/config/app.config';
import { Router, NavigationEnd } from '@angular/router';
import $ from "jquery";
import { Location } from '@angular/common';
@Component({
  selector: 'app-custom-plan',
  templateUrl: './custom-plan.component.html',
  styleUrls: ['./custom-plan.component.scss']
})
export class CustomPlanComponent implements OnInit, AfterViewInit {
  public cartridges: any = [];
  public shaveCreams: any = [];
  public planTypes: any = [];
  public activeCartridge: any;
  public activeShaveCream: any;
  public activePlanType: any;
  public productList: any = [];
  public productTypes: any = [];
  public isLoading = true;
  public currentStep = 'step1';
  public totalPrice:number = 0.00;
  public i18nDialog: any;
  private promoCode_param: any;
  private promoCode: any;

  constructor(public storage: StorageService,
    public planService: PlanService,
    private _dialogService: DialogService,
    public appConfig: AppConfig,
    private globalService: GlobalService,
    private loadingService: LoadingService,
    public cartService: CartService,
    private router: Router,
    private translate: TranslateService,
    private dialogService: DialogService,
    private httpService: HttpService,
    private location: Location) {
      this.translate.get('dialog').subscribe(res => {
        this.i18nDialog = res;
      });
    }

  ngAfterViewInit() {
    setTimeout(() => {
      $('html').css('overflow-y', 'auto');
    }, 500)
  }

  ngOnInit() {
    this.loadingService.display(true);
    this.getDataType();
    $('html').css('overflow-y', 'hidden');
    const promoCode_param = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.promoCode = promoCode_param.get('promo');
    // this.activeGroup = this.planGroups.filter((group)=> group.name === this.item.groupName)[0];
    // this.loadingService.status.subscribe((value: boolean) => {
    //   this.isLoading = value;
    // });
    // this.deliveryDate = new Date();
    // // this.deliveryDate.setMonth(this.deliveryDate.getMonth() + 3);
    // this.translate.get('dialog').subscribe(res => {
    //   this.i18nDialog = res;
    // });
  }

  ngOnDestroy() {
    $('body').css('background', '#ffffff');
    $('html').css('overflow-y', 'auto');
  }

  getDataType() {
    this.productList = [];
    this.productTypes = [];
    this.planTypes = [];

    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        data.forEach((product, index) => {
          let productImage: any;

          // fetch to find default image
          product.productImages.forEach((imageItem, index) => {
            if (imageItem.isDefault) {
              productImage = imageItem;
            }
          });

          // push to product list
          if(this.productList[product.ProductType.name]) {
            this.productList[product.ProductType.name].push(product);
          } else {
            this.productList[product.ProductType.name] = [product];
            this.productTypes.push(product.ProductType);
          }
        });

        this.cartridges = this.productList['Cartridge Packs'];
        this.shaveCreams.push({sku: null});
        this.shaveCreams.push(this.productList['Skin Care'].filter(product => product.sku === 'A5/2018')[0]);
        this.activeCartridge = this.cartridges[0];
        this.activeShaveCream = this.shaveCreams[0];
        // console.log('this.cartridges', this.cartridges);
        // console.log('this.shaveCreams', this.shaveCreams);

        this.httpService._getList(`${this.appConfig.config.api.plan_type}`).subscribe(
          data => {
            // let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
            data.forEach((planType, index) => {
              if(planType.prefix.indexOf('2-') > -1 ||
              planType.prefix.indexOf('3-') > -1 ||
              planType.prefix.indexOf('4-') > -1) {
                // planType.planTypeTranslate = planType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang) ? planType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang) : planType.planTypeTranslate[0];
                this.planTypes.push(planType);
              }
            });
            this.activePlanType = this.planTypes[0];
						// console.log('1111111111111',this.activePlanType);
            this.loadingService.display(false);
            this.isLoading = false;
          },
          error => {
            // console.log(error.error)
            this.loadingService.display(false);
            this.isLoading = false;
          }
        )
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
        this.isLoading = false;
      }
    )
  }

  changeCartridge(_cartridge:any) {
    this.activeCartridge = _cartridge;
  }

  changeShaveCream(_shaveCream:any) {
    this.activeShaveCream = _shaveCream;
  }

  changePlanType(_planType:any) {
    this.activePlanType = _planType;
  }

  newWindowDetail(_item:any) {
      window.open(`/products/${_item.slug}`);
  }

  gotoStep2() {
    this.currentStep = 'step2';
    this.totalPrice = parseFloat(this.activeCartridge.productCountry.sellPrice) + parseFloat((this.activeShaveCream.sku ? this.activeShaveCream.productCountry.sellPrice : 0));
  }

  gotoStep1() {
    this.currentStep = 'step1';
  }

  processCheckout() {
    // // console.log('this.paymentTypeActive --- ', this.paymentTypeActive);
    // // console.log('this.bladeActive --- ', this.bladeActive);
    this.loadingService.display(true);
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let item = {
      PlanTypeId: this.activePlanType.id,
      customPlanDetail: [
        {
          qty: 1,
          product: this.activeCartridge,
          duration: this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang) ?
                    this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang).name :
                    this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === 'EN').name
        }
      ]
    }

    if(this.activeShaveCream.sku) {
      item.customPlanDetail.push(
        {
          qty: 1,
          product: this.activeShaveCream,
          duration: this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang) ?
                    this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === currentLang).name :
                    this.activePlanType.planTypeTranslate.find(value => value.langCode.toUpperCase() === 'EN').name
        }
      );
    }

    let currentUser = this.storage.getCurrentUserItem();
    if (currentUser) {
      this.cartService.getSubscriptionsByUser(currentUser).then(
        (data: any) => {
          this.loadingService.display(false);
          currentUser.subscriptions = data;
          this.storage.setCurrentUser(currentUser);
          this.storage.setCustomPlanItem(item);
          if (this.promoCode) {
            this.router.navigateByUrl(`/checkout?promo=` + this.promoCode);
          } else {
            this.router.navigateByUrl(`/checkout`);
          }
          // if (data && data.length > 0) {
          //     let checkIsTrialSub : boolean = false;
          //     data.forEach(item => {
          //       if (item.isTrial && item.status !== "Canceled") {
          //         checkIsTrialSub = true;
          //       }
          //     });
          //     // console.log('checkIsTrialSub --- ', checkIsTrialSub)
          //     if (checkIsTrialSub) {
          //       this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
          //         if (result) {
          //           this.router.navigate(['/']);
          //         }
          //       }));
          //     } else {
          //       this.storage.setCustomPlanItem(item);
          //       this.router.navigateByUrl(`/checkout`);
          //     }
          //   } else {
          //     this.storage.setCustomPlanItem(item);
          //     this.router.navigateByUrl(`/checkout`);
          //   }
        },
        error => {
          this.loadingService.display(false);
          // console.log(error.error)
        }
      )

    } else {
      this.loadingService.display(false);
      this.storage.setCustomPlanItem(item);
      if (this.promoCode) {
        this.router.navigateByUrl(`/checkout?promo=` + this.promoCode);
      } else {
        this.router.navigateByUrl(`/checkout`);
      }
      // this.storage.setCheckLoginForCustomPlan(true);
      // this.router.navigateByUrl(`/login`);
    }
  }

}
