import { Component, ElementRef, OnInit, Output, Input, EventEmitter, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from '../core/directives/modal/modal.component';
import { ModalService } from '../core/services/modal.service';
import { HttpService } from '../core/services/http.service';
import { GlobalService } from '../core/services/global.service';
import { LoadingService } from '../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { AlertService } from '../core/services/alert.service';
import { CountriesService } from '../core/services/countries.service';
import { PlanCartService } from '../core/services/plan-cart.service';

import $ from "jquery";

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  public shippingFee: any;
  // public activeStepSelectPlan: boolean = false;
  // public activeStepAddProduct: boolean = false;
  // public activeStepObj = [
  //   { step: 'entry', valid: true },
  //   { step: 'select_plan', valid: false },
  //   { step: 'add_product', valid: false },
  //   { step: 'checkout', valid: false },
  // ];
  planList: Object = {};
  planTypes: Array<String> = [];
  planTypeSelected: String;
  plansSlider = [];
  public isloading: boolean = true;
  faqList: Array<any> = [];
  constructor(public storage: StorageService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    public appConfig: AppConfig,
    private httpService: HttpService,
    private globalService: GlobalService,
    public modalService: ModalService,
    private router: Router) {
    let curCountry = this.storage.getCountry();
    if (!curCountry.isWebSubscription || !curCountry.isWebStripe || !curCountry.isWebEcommerce) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    })
    this.loadingService.status.subscribe((value: boolean) => {
      this.isloading = value;
    })
  }

  emailLeadPopout() {
    if (this.storage.getLanguageCode() === 'KO' || this.storage.getCountry().code.toUpperCase() === 'KOR' || this.storage.getCountry().code.toUpperCase() === 'HKG') {
      this.modalService.open('collect-email-modal');
    }
  }

  reloadPage() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.planList = [];
    this.plansSlider = [];
    // get all plans
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
      data => {
        data.forEach((plan, index) => {
          let planImage: any;

          // fetch to find default image
          plan.planImages.forEach((imageItem, index) => {
            if (imageItem.isDefault) {
              planImage = imageItem;
            }
          });

          // push to plan list
          if (this.planList[plan.PlanType.name]) {
            this.planList[plan.PlanType.name].push(plan);
          } else {
            this.planList[plan.PlanType.name] = [plan];
          }

          // get planType
          this.planTypes = Object.keys(this.planList);

          // check image is exist and set variable
          let _imageUrl: string = "";
          if (planImage) {
            _imageUrl = planImage.url;
          } else if (plan.planImages && plan.planImages.length > 0) {
            _imageUrl = plan.planImages[0].url;
          }

          // push to plan Slider
          this.plansSlider.push({
            id: plan.id,
            title: plan.planTranslate.name,
            description: plan.planTranslate.description,
            imageUrl: _imageUrl
          });
        });
        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )

    this.getInfoByType();

    this.shippingFee = `${this.storage.getCountry().currencyDisplay}${this.appConfig.getConfig('shippingFeeConfig').trialFee}`;
  }

  goPlanDetail(planId) {
    this.router.navigateByUrl(`/shave-plans/${planId}`);
  }

  goToStartShavePlan() {
    $('html,body').animate({ scrollTop: $("#start-shave-plan").offset().top }, "slow");
  }

  // get faqs info by type
  getInfoByType() {

    this.loadingService.display(true);
    this.faqList = [];

    let options = {
      isSubscription: true,
      langCode: this.storage.getLanguageCode()
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.faqs}?` + params.toString()).subscribe(
      data => {
        // console.log('data',data);
        data.forEach((item, index) => {
          let i = 0;
          item.faqTranslate.forEach(element => {
            i++;
            if (i <= 3) {
              this.faqList.push(element);
            }
          });
        });

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }

  gotoCustomPlan() {
    this.router.navigateByUrl(`/shave-plans/custom-plan`);
  }

  // gotoProduct() {
  //   this.router.navigateByUrl(`/products`);
  // }

  gotoASK() {
    this.router.navigateByUrl(`/ask`);
  }


  // // check status active Step AddProduct
  // checkActiveStep(step: string) {
  //   for (var i = 0; i < this.activeStepObj.length; i++) {
  //     if (this.activeStepObj[i].step === step) {
  //       return this.activeStepObj[i].valid;
  //     }
  //   }
  //   return false;
  // }


  // // go to select plan page
  // goToSelectPlan() {
  //   // this.activeStepSelectPlan = true;
  //   this.setActiveStep('select_plan', true);
  // }

  // // get status selected plan and set it
  // getEmitSelectedPlan(status: boolean) {
  //   if (status)
  //     this.setActiveStep('add_product', true);
  //   // this.activeStepAddProduct = status;
  // }

  // // get status added product and set it
  // getEmitAddedProduct(status: boolean) {
  //   if (status)
  //     this.setActiveStep('checkout', true);
  //   // this.activeStepAddProduct = status;
  // }

  // public setActiveStep(step: string, valid: boolean) {
  //   // If the state is found, set the valid field to true
  //   for (var i = 0; i < this.activeStepObj.length; i++) {
  //     if (this.activeStepObj[i].step === step) {
  //       this.activeStepObj[i].valid = true;
  //     } else {
  //       this.activeStepObj[i].valid = false;
  //     }
  //   }
  // }

  // clearCart() {
  //   this.storage.clearCart();
  //   this.storage.clearPlan();
  // }

  // // get emit from checkout step
  // getEmitChangePlan(status: boolean) {
  //   this.setActiveStep('select_plan', status);
  // }

}
