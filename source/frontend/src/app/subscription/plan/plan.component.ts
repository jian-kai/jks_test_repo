import { Component, ElementRef, OnInit, Output, Input, EventEmitter, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { LoadingService } from '../../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { CountriesService } from '../../core/services/countries.service';
import { PlanCartService } from '../../core/services/plan-cart.service';
import { PlanService } from '../../core/services/plan.service';
import { CartService } from '../../core/services/cart.service';
import { GTMService } from '../../core/services/gtm.service';

import $ from "jquery";

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
  planList: Object = {};
  public planGroups: any = [];
  public activeGroup: any = {};
  public activePayment: any = {};
  planTypes: Array<String> = [];
  planTypeSelected: String;
  plansSlider = [];
  public isloading : boolean = true;
  faqList: Array<any> = [];
  constructor(public storage: StorageService,
    private loadingService: LoadingService,
    private countriesService: CountriesService,
    public appConfig: AppConfig,
    private httpService: HttpService,
    private globalService: GlobalService,
    public planService: PlanService,
    public cartService: CartService,
    private gtmService: GTMService,
    private router: Router) { }

  ngOnInit() {
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    })
    this.loadingService.status.subscribe((value : boolean) => {
      this.isloading = value;
    })
  }
  
  reloadPage() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.planList = [];
    this.plansSlider = [];
    // get all plans
    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.plan}?` + params.toString()).subscribe(
      data => {
        if (data) {
          this.planGroups = this.planService.groupPlan(data);
          if (this.planGroups.length > 0) {
            this.activeGroup = this.planGroups[0];
            this.activePayment = this.activeGroup.item[0];
            this.gtmService.productDetail(this.activePayment);
            this.gtmService.itemImpressions(this.planGroups);
          }
          // console.log('this.planGroups', this.planGroups);
        }
        


        // data.forEach((plan, index) => {
        //   let planImage: any;

        //   // fetch to find default image
        //   plan.planImages.forEach((imageItem, index) => {
        //     if (imageItem.isDefault) {
        //       planImage = imageItem;
        //     }
        //   });

        //   // push to plan list
        //   if(this.planList[plan.PlanType.name]) {
        //     this.planList[plan.PlanType.name].push(plan);
        //   } else {
        //     this.planList[plan.PlanType.name] = [plan];
        //   }

        //   // get planType
        //   this.planTypes = Object.keys(this.planList);

        //   // push to plan Slider
        //   this.plansSlider.push({
        //     id: plan.id,
        //     title: plan.planTranslate.name,
        //     description: plan.planTranslate.description,
        //     imageUrl: planImage ? planImage.url : plan.planImages[0].url
        //   });
        // });
        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )
    
    this.getInfoByType();
  }

  changeGroup(_group: any) {
    this.loadingService.display(true);
    this.activeGroup = _group;
    this.activePayment = _group.item[0];
    this.gtmService.productDetail(this.activePayment);
    setTimeout(() => {
      this.loadingService.display(false);
    }, 100);
    $('html,body').animate({scrollTop: $("#payment-type").offset().top}, "slow");
  }

  changePayment(_paymentType: any) {
    this.loadingService.display(true);
    this.activePayment = _paymentType;
    this.gtmService.productDetail(this.activePayment);
    setTimeout(() => {
      this.loadingService.display(false);
    }, 100);
    $('html,body').animate({scrollTop: $("#plan-details").offset().top}, "slow");
  }

  // add active plan to cart
  addToCart() {
    let item = {
      id: this.activePayment.id,
      plan: this.activePayment,
      qty : 1,
      groupName: this.activeGroup.name
    }
    this.cartService.addItem(item);
    // sync cart item to server
    this.cartService.updateCart();
    this.cartService.getCart();
    this.router.navigateByUrl(`/cart`);
  }

  goPlanDetail(planId) {
    this.router.navigateByUrl(`/shave-plans/${planId}`);
  }
  
  // get faqs info by type
  getInfoByType() {

    this.loadingService.display(true);
    this.faqList = [];

    let options = {
      isSubscription: true,
      langCode: this.storage.getLanguageCode()
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.faqs}?` + params.toString()).subscribe(
      data => {
        // console.log('data',data);
        data.forEach((item, index) => {
          let i = 0;
          item.faqTranslate.forEach(element => {
            i ++;
            if(i <= 3) {
              this.faqList.push(element);
            }
          });
        });

        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
  }
}