import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { PlanComponent } from './plan.component';
import { SharedModule } from './../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: PlanComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [
    PlanComponent,
  ]
})
export class PlanModule { }
