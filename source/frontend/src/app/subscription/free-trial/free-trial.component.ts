import { Component, ElementRef, OnInit, Output, Input, EventEmitter, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from '../../core/directives/modal/modal.component';
import { ModalService } from '../../core/services/modal.service';
import { HttpService } from '../../core/services/http.service';
import { GlobalService } from '../../core/services/global.service';
import { LoadingService } from '../../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../core/services/storage.service';
import { AlertService } from '../../core/services/alert.service';
import { CountriesService } from '../../core/services/countries.service';
import { PlanCartService } from '../../core/services/plan-cart.service';
import { PlanService } from '../../core/services/plan.service';
import { CartService } from '../../core/services/cart.service';
import { GTMService } from '../../core/services/gtm.service';
import { DialogService } from '../../core/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common'

import $ from "jquery";

declare var $: any;

@Component({
	selector: 'app-free-trial',
	templateUrl: './free-trial.component.html',
	styleUrls: ['./free-trial.component.scss']
})
export class FreeTrialComponent implements OnInit {
	planList: Object = {};

	// for slider
	public selectedPlan: any;
	public sliderOptions = {
		autoSlide: false, //why this line code not working, to solve we have to increase 'intervalDuration' value 
		listPosition: 'left',
		displayControls: true,
		intervalDuration: 600000,
	}
	public planSlider = [];
	public planType
	public freeAmount: any;
	public planGroups: any = [];
	public activeGroup: any = {};
	public refillGroups: any = {};
	public activePayment: any = {};
	public planTypesName: any = [];
	public groupPlanTypes: any = [];

	public planGroupId: number;

	public bladeList: any = [];
	public bladeListWithoutY: any = [];
	public bladeActive: any = {};
	public refillList: any = [];
	public refillActive: any = {};
	public paymentTypeList: any = [];
	public paymentTypeActive: any = {};
	public selectList: any = [];
	public _currentPlan: any;
	public monthData: any = [];
	public planImage: any = [];
	public refillships_date1: any;
	public refillships_date2: any;
	public refillships_date3: any;

	planTypes: Array<String> = [];
	planTypeSelected: String;
	monthSelected: number;
	itemProductSelected: number;
	typeMonth: string;
	storageDataSelect: any = [];
	public isloading: boolean = true;
	faqList: Array<any> = [];
	public i18nDialog: any;
	private prodId_param: any;
	private prodId: any;

	constructor(public storage: StorageService,
		private loadingService: LoadingService,
		private countriesService: CountriesService,
		public appConfig: AppConfig,
		private httpService: HttpService,
		private globalService: GlobalService,
		public planService: PlanService,
		public cartService: CartService,
		private gtmService: GTMService,
		private router: Router,
		public modalService: ModalService,
		private dialogService: DialogService,
		private translate: TranslateService,
		private location: Location) {
		this.translate.get('dialog').subscribe(res => {
			this.i18nDialog = res;
		});
		let curCountry = this.storage.getCountry();
		if (!curCountry.isWebSubscription || !curCountry.isWebStripe || !curCountry.isWebEcommerce) {
			this.router.navigate(['/']);
		}
		this.loadingService.status.subscribe((value: boolean) => {
			this.isloading = value;
		});
	}

	ngOnInit() {
		this.reloadPage();
		this.countriesService.language.subscribe(country => {
			this.reloadPage();
		});

		this.getRefillShipDate();

		// Time
		// let currDate = new Date();
		// this.refillships_date1 = currDate.setDate(currDate.getDate() + 13);
		// this.refillships_date2 = currDate.setDate(currDate.getDate() + 3);
	}
	getRefillShipDate() {
		if (this.storage.getCountry().code.toUpperCase() === 'KOR') {
			this.httpService._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
				data => {
					if (data) {
						let currDate = new Date(data);
						this.refillships_date1 = currDate.setDate(currDate.getDate() );
						this.refillships_date2 = currDate.setDate(currDate.getDate() );
						this.refillships_date3 = currDate.setDate(currDate.getDate() + 6);
					}
				},
			)
		} else {
			let currDate = new Date();
			this.refillships_date1 = currDate.setDate(currDate.getDate() );
			this.refillships_date2 = currDate.setDate(currDate.getDate() );
			this.refillships_date3 = currDate.setDate(currDate.getDate() + 6);
		}
	}
	reBuildInitData() {
		this.selectList = [];
		this._currentPlan = this.planGroups.filter(group => (group.id === this.planGroupId));
		let _monthsAnnual = this._currentPlan[0]['Annual'];
		let _monthsPayasyougo = this._currentPlan[0]['Pay As You Go'];
		if (_monthsPayasyougo.length > 0) {
			_monthsPayasyougo.forEach(value => {
				this.selectList.push({ id: value.id, name: value.PlanType.planTypeTranslate, type: 'Pay As You Go' });
			});
		}
		if (_monthsAnnual && _monthsAnnual.length > 0) {
			_monthsAnnual.forEach(value => {
				this.selectList.push({ id: value.id, name: value.planTranslate.shortDescription, type: 'Annual' });
			});
		}

		// if(typeof this.typeMonth == "undefined") {
		// 	this.typeMonth = 'Pay As You Go';
		// }
		// this.monthSelected = _monthsPayasyougo[0].id;
		this.initMonthData()
	}

	initMonthData() {
		this.typeMonth = 'Pay As You Go';
		this.monthSelected = this._currentPlan[0][this.typeMonth][0].id;
		this.itemProductSelected = this._currentPlan[0][this.typeMonth][0].planCountry.planOptions[0] ? this._currentPlan[0][this.typeMonth][0].planCountry.planOptions[0].id : null;
	}

	reBuildMonthData() {
		let _dataType = this._currentPlan[0][this.typeMonth];
		let _dataMonthFilter = _dataType.filter(item => (item.id == this.monthSelected));
		this.storageDataSelect = _dataMonthFilter;
		let _planCountry = _dataMonthFilter[0].planCountry;
		// // console.log(this.monthSelected);
		if (_dataMonthFilter.length > 0) {
			this.monthData = _planCountry.planOptions;
			this.itemProductSelected = this.monthData[0] ? this.monthData[0].id : null;
			// Free amount
			let planTrialProducts = _planCountry.planTrialProducts;
			let _price_freeAmount = 0;
			planTrialProducts.forEach(price => {
				_price_freeAmount = _price_freeAmount + parseFloat(price.ProductCountry.sellPrice);
			});
			this.freeAmount = _price_freeAmount.toFixed(2);
			// re-build slider data
			this.planSlider = [];
			_dataMonthFilter[0].planImages.forEach(image => {
				this.planSlider.push({
					imageUrl: image.url
				});
			});
		}
	}

	reloadPage() {
		let cur = this.storage.getCurrentUserItem();
		this.loadingService.display(true);
		this.planList = [];
		this.planSlider = [];
		// get all plans
		let options = {
			userTypeId: 1
		};
		let params = this.globalService._URLSearchParams(options);
		this.httpService._getList(`${this.appConfig.config.api.free_trial}?` + params.toString()).subscribe(
			data => {
				console.log('data', data);
				if (data) {
					this.planGroups = this.planService.groupPlan(data);

					const prodId_param = new URLSearchParams(this.location.path(false).split('?')[1]);
					this.prodId = prodId_param.get('prodId');
					switch (this.prodId) {
						case "1":
							console.log('id ----- ' + (this.prodId));
							this.planGroupId = 1;
							console.log('planid1 ----- ' + (this.planGroupId));
							break;
						case "2":
							console.log('id ----- ' + (this.prodId));
							this.planGroupId = 2;
							console.log('planid1 ----- ' + (this.planGroupId));
							break;
						case "3":
							console.log('id ----- ' + (this.prodId));
							this.planGroupId = 3;
							console.log('planid1 ----- ' + (this.planGroupId));
							break;
						default:
							console.log('id ----- ' + (this.prodId));
							this.planGroupId = this.planGroups[0].id;
							console.log('planid1 ----- ' + (this.planGroupId));
							break;
					}
					// if (this.prodId) {
					// 	if (this.prodId == '1') {
					// 		console.log('id ----- ' + (this.prodId));
					// 		this.planGroupId = 1;
					// 		console.log('planid1 ----- ' + (this.planGroupId));
					// 	}
					// 	if (this.prodId == '2') {
					// 		console.log('id ----- ' + (this.prodId));
					// 		this.planGroupId = 2;
					// 		console.log('planid2 ----- ' + (this.planGroupId));
					// 	}
					// 	if (this.prodId == '3') {
					// 		console.log('id ----- ' + (this.prodId));
					// 		this.planGroupId = 3;
					// 		console.log('planid3 ----- ' + (this.planGroupId));
					// 	}
					// } else {
					// 	console.log('id ----- ' + (this.prodId));
					// 	this.planGroupId = this.planGroups[0].id;
					// 	console.log('planidB ----- ' + (this.planGroupId));
					// }
					// console.log('id ----- ' +(this.prodId));
					// get current plan
					//  this.planGroupId =   this.planGroups[0].id;
					// console.log('id ----- ' +(this.planGroupId));
					// rebuild Plan slider
					this.reBuildInitData();
					this.reBuildMonthData();
				}
				this.loadingService.display(false);
			},
			error => {
				// console.log(error.error)
				this.loadingService.display(false);
			}
		)
		this.getInfoByType();
	}

	getRefillList(item) {
		item.forEach(plan => {
			this.refillList.push(plan.PlanType.name);
		});
		this.refillList.sort();
		// console.log('this.refillList --- ', this.refillList)
	}

	getPaymentTypeList() {
		let _groupPlanType = this.planGroups.filter(group => group.name === this.bladeActive.name);
		let groupPlanTypeY = this.planGroups.filter(group => group.name === 'Y-' + this.bladeActive.name);
		let _bladeListWithoutY: any;
		_bladeListWithoutY = this.bladeList.filter(element => element.name === this.bladeActive.name);
		_bladeListWithoutY = _bladeListWithoutY[0];

		_bladeListWithoutY.item.forEach(plan => {
			this.planTypesName.push(plan.PlanType.name)
			this.paymentTypeList[plan.PlanType.name] = [];

			// set for 2, 3, 4 months
			if (Math.floor(plan.planCountry.savePercent) === Math.ceil(plan.planCountry.savePercent)) {
				plan.planCountry.savePercent = Math.round(plan.planCountry.savePercent);
			}
			this.paymentTypeList[plan.PlanType.name][0] = plan;

			// set for 12 months
			let spl = plan.sku.split(plan.groupName);
			let planY = groupPlanTypeY[0].item.filter(plan => plan.sku === (spl[0] + 'Y-' + this.bladeActive.name));
			if (Math.floor(planY[0].planCountry.savePercent) === Math.ceil(planY[0].planCountry.savePercent)) {
				planY[0].planCountry.savePercent = Math.round(planY[0].planCountry.savePercent);
			}
			this.paymentTypeList[plan.PlanType.name][1] = planY[0];
		});
		// console.log('this.paymentTypeList --- ', this.paymentTypeList);
	}

	// changeGroup(_group: any) {
	// 	this.loadingService.display(true);
	// 	let _planGroups = this.planGroups.filter(group => (group.name === _group.name));
	// 	// console.log('this.planGroups', this.planGroups)
	// 	this.bladeActive = _planGroups[0];
	// 	// get refill list
	// 	this.getRefillList(this.bladeActive.item);
	// 	// get refill active
	// 	let _refillList = [];
	// 	this.refillList.forEach(element => {
	// 		if (!_refillList.includes(element)) {
	// 			_refillList.push(element);
	// 		}
	// 	});
	// 	this.refillList = _refillList;
	// 	this.refillActive = this.refillList[1];
	// 	// console.log('this.refillActive --- ', this.refillActive);

	// 	// get payment type list
	// 	this.getPaymentTypeList();
	// 	// set payment type
	// 	this.paymentTypeActive = this.paymentTypeList[this.refillActive][0];
	// 	// console.log('this.paymentTypeActive --- ', this.paymentTypeActive)

	// 	this.activeGroup = _group;
	// 	this.activePayment = _group.item[0];
	// 	this.gtmService.productDetail(this.activePayment);
	// 	setTimeout(() => {
	// 		this.loadingService.display(false);
	// 	}, 100);
	// 	$('html,body').animate({ scrollTop: $("#select-refill").offset().top - 25 }, "slow");
	// }

	changePlan(id: any) {
		this.loadingService.display(true);
		this.planGroupId = id;
		this.reBuildInitData();
		this.initMonthData();
		this.reBuildMonthData();

		setTimeout(() => {
			this.loadingService.display(false);
		}, 100);
		//$('html,body').animate({ scrollTop: $("#select-refill").offset().top - 85 }, "slow");
	}

	changeItemplan(event) {
		this.loadingService.display(true);
		let type = event.target.options[event.target.selectedIndex].getAttribute('data-type');
		this.typeMonth = type;
		this.reBuildMonthData();

		setTimeout(() => {
			this.loadingService.display(false);
		}, 100);
	}

	// changeRefill(_refill: any) {
	// 	this.loadingService.display(true);
	// 	this.refillActive = _refill.PlanType.name;
	// 	// get payment type list
	// 	this.getPaymentTypeList();
	// 	// set payment type
	// 	this.paymentTypeActive = this.paymentTypeList[this.refillActive][0];
	// 	// console.log('this.paymentTypeActive --- ', this.paymentTypeActive)


	// 	this.activePayment = _refill;
	// 	this.gtmService.productDetail(this.activePayment);
	// 	setTimeout(() => {
	// 		this.loadingService.display(false);
	// 	}, 100);
	// 	$('html,body').animate({ scrollTop: $("#payment-type").offset().top - 25 }, "slow");
	// 	//plan-details
	// }

	changePayment(_paymentType: any) {
		this.loadingService.display(true);
		let _paymentTypeGroup = this.paymentTypeList[this.refillActive];
		this.paymentTypeActive = _paymentTypeGroup.filter(pm => pm.id === _paymentType.id);
		this.paymentTypeActive = this.paymentTypeActive[0];
		this.activePayment = _paymentType;
		this.gtmService.productDetail(this.activePayment);
		setTimeout(() => {
			this.loadingService.display(false);
		}, 100);
		$('html,body').animate({ scrollTop: $("#plan-details").offset().top - 25 }, "slow");
	}

	// add active plan to cart
	addToCart() {
		// console.log('this.paymentTypeActive --- ', this.paymentTypeActive);
		// console.log('this.bladeActive --- ', this.bladeActive);
		this.loadingService.display(true);
		let item = {
			// id: this.paymentTypeActive.id,
			// plan: this.paymentTypeActive,
			qty: 1,
			groupName: this.planGroups.filter(group => (group.id === this.planGroupId))[0].name,
			planActive: this.storageDataSelect[0],
			productSelected: this.itemProductSelected
		}
		let currentUser = this.storage.getCurrentUserItem();
		if (currentUser) {
			this.cartService.getSubscriptionsByUser(currentUser).then(
				(data: any) => {
					// this.loadingService.display(false);
					currentUser.subscriptions = data;
					this.storage.setCurrentUser(currentUser);
					if (data && data.length > 0) {
						let checkIsTrialSub: boolean = false;
						data.forEach(item => {
							if (item.isTrial && item.status !== "Canceled") {
								checkIsTrialSub = true;
							}
							if (item.isTrial && item.status === "Canceled") {
								if (item.cancellationReason === "Canceled by user from E-commerce site") {
									checkIsTrialSub = true;
								}
							}
							/*if (item.isTrial && item.status !== "Canceled") {
								checkIsTrialSub = true;
							}*/
						});
						// console.log('checkIsTrialSub --- ', checkIsTrialSub)
						if (checkIsTrialSub) {
							this.loadingService.display(false);
							this.modalService.open('free-trial-modal');
							// this.dialogService.result(this.i18nDialog.confirm.notAllowForIsTrial).afterClosed().subscribe((result => {
							// 	// if (result) {
							// 	// 	this.router.navigate(['/']);
							// 	// }
							// }));
						} else {
							this.storage.setFreeTrialItem(item);
							this.loadingService.display(false);
							this.router.navigateByUrl(`/checkout-trial`);
						}
					} else {
						this.storage.setFreeTrialItem(item);
						this.loadingService.display(false);
						this.router.navigateByUrl(`/checkout-trial`);
					}
				},
				error => {
					this.loadingService.display(false);
					// console.log(error.error)
				}
			)

		} else {
			this.loadingService.display(false);
			this.storage.setFreeTrialItem(item);
			this.router.navigateByUrl(`/checkout-trial`);
		}
	}

	goPlanDetail(planId) {
		this.router.navigateByUrl(`/shave-plans/${planId}`);
	}

	// get faqs info by type
	getInfoByType() {

		this.faqList = [];

		let options = {
			isSubscription: true,
			langCode: this.storage.getLanguageCode()
		};
		let params = this.globalService._URLSearchParams(options);
		this.httpService._getList(`${this.appConfig.config.api.faqs}?` + params.toString()).subscribe(
			data => {
				// console.log('data', data);
				data.forEach((item, index) => {
					let i = 0;
					item.faqTranslate.forEach(element => {
						i++;
						if (i <= 3) {
							this.faqList.push(element);
						}
					});
				});

			},
			error => {
				// console.log(error.error)
			}
		)
	}

	//notSure
	notSure() {
		this.modalService.open('app-suggest-refill-modal');
	}

	//has Change Refill from 'not sure' modal
	hasChangeRefill(_refillType: string) {
		this.typeMonth = 'Pay As You Go';
		this.monthSelected = this._currentPlan[0][this.typeMonth].filter(plan => plan.PlanType.name === _refillType)[0].id;
		this.reBuildMonthData();
		$('#selectFrequency').modal('hide');
	}
}
