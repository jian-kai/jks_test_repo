//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Components
import { FreeTrialComponent } from './free-trial.component';
import { SuggestRefillModalComponent } from './../suggest-refill-modal/suggest-refill-modal.component';

// Modules
import { SharedModule } from './../../core/directives/share-modules';


// routes
export const ROUTES: Routes = [
  { path: '', component: FreeTrialComponent }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
	  FormsModule   
  ],
  declarations: [FreeTrialComponent, SuggestRefillModalComponent],
})
export class FreeTrialModule { }
