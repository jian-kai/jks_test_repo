import { Component, ElementRef, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent extends ModalComponent {
  private currentUser: any;
  public changePasswordForm: FormGroup;
  public resError: any;

  public i18nDialog: any;
  public pwUpdated: string;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private builder: FormBuilder,
    private _dialogService: DialogService,
    private translate: TranslateService,
    private alertService: AlertService, ) {
    super(modalService, el);
    this.id = 'change-password-modal';
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }

    this.changePasswordForm = builder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required],
    });

    this.changePasswordForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.resError = null;
  }
  
  setInitData(_card) {
    this.changePasswordForm.reset();
    this.resError = null;
  }

  // do change pasword
  doChangePassword() {
    this.resError = null;
    let userForm = this.changePasswordForm;
    let formData: FormData = new FormData();
    for (var key in userForm.value) {
      if (key === 'newPassword') {
        formData.append('password', userForm.value[key]);
      } else {
        formData.append(key, userForm.value[key]);
      }
    }
    this.translate.get('lbl').subscribe(res => {
      this.i18nDialog = res;
    });
    this.pwUpdated = this.i18nDialog.user.setting.user_modal.pw_updated;

    this._http._update(`${this.appConfig.config.api.users}/${this.currentUser.id}`, formData, 'form-data').subscribe(
      data => {
        this.close();
        //Show dialog
        this._dialogService.result(this.pwUpdated);
      },
      error => {
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.alertService.error([error]);
      }
    )
  }


}
