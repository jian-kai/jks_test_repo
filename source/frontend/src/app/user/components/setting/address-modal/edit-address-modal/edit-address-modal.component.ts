import { Component, ElementRef, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../../core/services/modal.service';
import { HttpService } from '../../../../../core/services/http.service';
import { GlobalService } from '../../../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../../core/services/storage.service';
import { LoadingService } from '../../../../../core/services/loading.service';
import { AlertService } from '../../../../../core/services/alert.service';

@Component({
  selector: 'edit-address-modal',
  templateUrl: './edit-address-modal.component.html',
  styleUrls: ['./edit-address-modal.component.scss']
})
export class EditAddressModalComponent extends ModalComponent {
  countryStates: Array<any>;
  public deliveryAddressForm: any;
  public isLoading: boolean = true;
  private currentUser: any;
  private deliveryAddressInfo: any;
  resError: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Input() addressId: number;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private loadingService: LoadingService,
    public storage: StorageService,
    builder: FormBuilder,
    private alertService: AlertService) {
    super(modalService, el);
    this.id = 'edit-address-modal';

    this.deliveryAddressForm = builder.group({
      id: [undefined],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      contactNumber: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      portalCode: ['', Validators.required]
    })

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    setTimeout(_ => this.getCountryStates());
    
    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });
  }

  setInitData(_addresss) {
    this.deliveryAddressForm.patchValue(_addresss);
    this.resError = null;
  }

  // get country states
  getCountryStates() {
    this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
      data => {
        this.countryStates = data;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // do add new address
  doEditDeliveryAddress(data) {
    let curUser = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    if (curUser) {
      data.CountryId = 1;
      this._http._update(`${this.appConfig.config.api.delivery_address}/${data.id}`.replace('[userID]', curUser.id), data)
        .subscribe(
        res => {
          if (res.ok) {
            this.hasChange.emit(true);
            this.close();
            this.loadingService.display(false);
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
      )
    }
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
}
