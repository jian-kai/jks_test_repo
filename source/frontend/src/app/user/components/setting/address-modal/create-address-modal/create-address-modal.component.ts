import { Component, ElementRef, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../../core/services/modal.service';
import {HttpService} from '../../../../../core/services/http.service';
import {GlobalService} from '../../../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../../../../core/services/storage.service';
import { LoadingService } from '../../../../../core/services/loading.service';
import {AlertService} from '../../../../../core/services/alert.service';
import { DeliveryAddressService } from '../../../../../core/services/delivery-address.service';

@Component({
  selector: 'create-address-modal',
  templateUrl: './create-address-modal.component.html',
  styleUrls: ['./create-address-modal.component.scss']
})
export class CreateAddressModalComponent extends ModalComponent {
  countryStates: Array<any>;
  public deliveryAddressForm;
  public isLoading: boolean = true;
  private currentUser: any;
  addressType: Array<string> = [];
  resError: any;
  public callingCodes: any;
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    private loadingService: LoadingService,
    private deliveryAddressService: DeliveryAddressService,
    private alertService: AlertService) {

    super(modalService, el);
    this.id = 'create-address-modal';
    this.deliveryAddressForm = this.deliveryAddressService.initAddress();
    this.deliveryAddressForm.patchValue({callingCode: '+' + this.storage.getCountry().callingCode});
    this.deliveryAddressForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    setTimeout(_ => this.getCountryStates());

  }

  setInitData(_addresss) {
    this.callingCodes = this.globalService.getCountryCodes();
    this.deliveryAddressForm.reset();
    this.addressType = [];
    this.resError = null;
  }

  // get country states
  getCountryStates() {
    this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
      data => {
        this.countryStates = data;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // do add new address
  doAddDeliveryAddress(data) {
    this.loadingService.display(true);
    let curUser = this.storage.getCurrentUserItem();
    if (curUser) {
      data.CountryId = this.storage.getCountry().id;
      data.contactNumber = data.callingCode + ' ' + data.contactNumber;
      this._http._create(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', this.currentUser.id), data)
      .subscribe(
        res => {
          if (res.ok) {
            this.hasChange.emit(true);
            this.close();
            this.loadingService.display(false);
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
      )
    }
    
    // this.loadingService.display(false);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  executeAddDeliveryAddress(data, callback?) {
    this.loadingService.display(true);
    let curUser = this.storage.getCurrentUserItem();
    if (curUser) {
      data.CountryId = this.storage.getCountry().id;
      // data.addressType = this.addressType;
      // this.addressType = [];
      this._http._create(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', this.currentUser.id), data)
      .subscribe(
        res => {
          if (res.ok) {
            if(callback) {
              callback();
            }
            this.hasChange.emit(true);
            this.close();
            this.loadingService.display(false);
          }
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        }
      )
    }
    
    // this.loadingService.display(false);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  addressChange(addressType, event) {
    if(event.target.checked) {
      this.addressType.push(addressType);
    } else {
      this.addressType = this.addressType.filter(value => value !== addressType);
    }
  }

}
