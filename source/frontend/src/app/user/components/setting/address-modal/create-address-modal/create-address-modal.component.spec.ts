import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAddressModalComponent } from './create-address-modal.component';

describe('CreateAddressModalComponent', () => {
  let component: CreateAddressModalComponent;
  let fixture: ComponentFixture<CreateAddressModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAddressModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAddressModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
