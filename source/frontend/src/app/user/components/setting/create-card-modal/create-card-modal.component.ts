import { Component, ElementRef, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { StripeService } from '../../../../core/services/stripe.service';
import { UsersService } from '../../../../auth/services/users.service';
import * as $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'create-card-modal',
  templateUrl: './create-card-modal.component.html',
  styleUrls: ['./create-card-modal.component.scss']
})

export class CreateCardModalComponent extends ModalComponent {
  // public deliveryAddressForm: FormGroup;
  private currentUser: any;
  public isLoading: boolean = true;
  // @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  allValid: boolean = false;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cardType: string;
  cardName: string;
  cardForm: FormGroup;
  Stripe: any;
  cartElement: any;
  stripeToken: string;
  message: string;
  yearList: Array<string> = [];
  monthList: Array<string> = [];
  resError: any;

  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private alertService: AlertService,
    stripeService: StripeService,) {
      super(modalService, el);
      this.id = 'create-card-modal';
      this.cardForm = builder.group({
        cardName: ['', Validators.required],
        cardNumber: ['', Validators.required],
        expiryMonth: ['', Validators.required],
        expiryYear: ['', Validators.required],
        cvc: ['', Validators.required]
      });
      // update card valid
      this.cardForm.valueChanges.subscribe(() => {
        this.allValid = false;
      });
      this.Stripe = stripeService.getInstance();
      this.cartElement = $(el.nativeElement);
      
      // init data
      for(let num = 2017; num <= 2500; num++) {
        this.yearList.push(`${num}`);
      }
      for(let num = 1; num <= 12; num++) {
        this.monthList.push(`${num}`);
      }

      this.cardForm.valueChanges.subscribe(() => {
        this.resError = null;
      });

      this.resError = null;
  }
  
  setInitData(_card) {
    this.cardForm.reset();
    this.resError = null;
  }
  
  validate() {
    if(this.cardType === 'stripe') {
      return {
        allValid: this.allValid,
        stripeToken: this.stripeToken
      }
    } else {
      return {
        allValid: true
      }
    }
  }
  
  // create stripe token
  createToken() {
    let $form = this.cartElement.find('#cardForm');
    return new Promise((resolve, reject) => {
      this.Stripe.card.createToken($form, (status, response) => {
        if(response.error) {
          this.resError = {
            type: 'error',
            message: response.error.message
          }
          this.loadingService.display(false);
          reject(response.error.message);
        } else {
          this.resError = null;
          resolve(response.id);
        }
      });
    });
  }

  // create stripe customer
  createCustomer(stripeToken) {
    return new Promise((resolve, reject) => {
      this._http._create(`${this.appConfig.config.api.cards}`.replace('[userID]', this.storage.getCurrentUserItem().id), {stripeToken, CountryId: this.storage.getCountry().id}).subscribe(
        data => {
          resolve(data);
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
          reject(error);
        }
      );
    });
  }
  
   // do add new address
   submitCard(cardInfo) {
    this.loadingService.display(true);
    if(this.cardForm.valid) {
      this.createToken()
        .then(stripeToken => this.createCustomer(stripeToken))
        .then(customer => {
          this.hasChange.emit(true);
          this.loadingService.display(false);
          this.close();
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
        });
    }
    
    // this.loadingService.display(false);
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  // onchange card number
  changeCardNumber() {
    this.cardType = this.globalService.getCardType(this.cardForm.controls['cardNumber'].value).toUpperCase();
    
    this.autoSpacing();
    let value = $('#cardNumber').val();
    if(value.substr(value.length - 1) === ' ') {
      $('#cardNumber').val(value.slice(0, value.length - 1));
    }
  }
  
  // auto spacing for card number
  autoSpacing() {
    var value = $('#cardNumber').val().replace(/\s/g,'');
    let count:number = Math.floor(value.length / 4);
    var output:any = '';

    for(var i = 1; i <= count; i++) {
      if(i === 1) {
        output += [value.slice(0, i * 4), ' '].join('');
      }else if(i !== 1) {
        output += [value.slice((i-1)*4, i * 4), ' '].join('');
      }
      
      if(i === count) {
        output += [value.slice(i * 4)];
      }
    }

    if(output != '') {
      $('#cardNumber').val(output);
    }
  }

}
