import { Component, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { numberValidator } from '../../../../core/directives/number-validator.directive';

import { ModalComponent } from '../../../../core/directives/modal/modal.component';
import { ModalService } from '../../../../core/services/modal.service';
import { HttpService } from '../../../../core/services/http.service';
import { GlobalService } from '../../../../core/services/global.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { AlertService } from '../../../../core/services/alert.service';
import { LoadingService } from '../../../../core/services/loading.service';

@Component({
  selector: 'edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss']
})
export class EditUserModalComponent extends ModalComponent{
  hasChangePassword: boolean = false;
  private userInfo : any;
  private currentUser: any;
  public isLoading: boolean = true;
  public userForm: FormGroup;
  private countries: any;
  public monthList : any[];
  resError: any;
  emailList: any = [];
  @Output() hasChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  constructor(modalService: ModalService,
    el: ElementRef,
    private _http: HttpService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    public storage: StorageService,
    builder: FormBuilder,
    private loadingService: LoadingService,
    private alertService: AlertService
  ) {
    super(modalService, el);
    this.id = 'edit-user-modal';
    
    this.userForm = builder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      dayInput: ['', [Validators.required, numberValidator({min: 1, max: 31})]],
      monthInput: ['', Validators.required],
      yearInput: ['', [Validators.required, numberValidator({min: new Date().getFullYear() - 100, max: new Date().getFullYear()})]],
      birthday: [''],
      gender: ['', Validators.required],
      oldPassword: [''],
      password: [''],
      // CountryId: ['']
    });

    this.userForm.valueChanges.subscribe(() => {
      this.resError = null;
    });

    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }

    setTimeout(_ => this.getUserInfo());

  }
  
  setInitData(_user) {
    this.userForm.patchValue(_user);
    setTimeout(_ => this.getUserInfo());
    this.resError = null;
  }
  
  onInit() {
    this.hasChangePassword = false;
    this.userForm.controls['password'].setValue('');
    this.userForm.controls['oldPassword'].setValue('');
    this.monthList = this.globalService.months();
  }
    
  // get user info
  getUserInfo() {
    this.loadingService.display(true);
    // this.userForm.controls['CountryId'].setValue(this.currentUser.Country.id);
    let countries = [];
    this._http._getList(`${this.appConfig.config.api.countries}`).subscribe(
      data => {
        this.countries = data;
      },
      error => {
        // console.log(error.error)
      }
    )

    this.userInfo = this._http._getDetail(`${this.appConfig.config.api.users}/${this.currentUser.id}`).subscribe(
      data => {
        this.userForm = this.globalService.setValueFormBuilder(this.userForm, data);
        // this.userForm.controls['CountryId'].setValue(data.Country.id);
        if (data.birthday) {
          this.userForm.controls['dayInput'].setValue(new Date(data.birthday).getDate());
          this.userForm.controls['monthInput'].setValue(new Date(data.birthday).getMonth() + 1);
          this.userForm.controls['yearInput'].setValue(new Date(data.birthday).getFullYear());
        } else {
          this.userForm.controls['dayInput'].setValue('');
          this.userForm.controls['monthInput'].setValue('');
          this.userForm.controls['yearInput'].setValue('');
        }
        this.userInfo = data;
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        // console.log(error)
      }
    )

    this.getEmail();
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  showPassword($event) {
    this.hasChangePassword = $event.target.checked;
  }
  
  // do edit
  doEdit() {
    console.log(this.userForm.controls.errors);
    this.loadingService.display(true);
    let formData:FormData = new FormData();
    if (this.userForm.value['yearInput'] && this.userForm.value['monthInput'] && this.userForm.value['dayInput']) {
      this.userForm.value['birthday'] = this.userForm.value['yearInput'] + '-' +
                                      this.userForm.value['monthInput'] + '-' +
                                      this.userForm.value['dayInput'];
    } else {
      this.userForm.value['birthday'] = "";
    }

    for ( var key in this.userForm.value ) {
      if((key === 'password' || key === 'oldPassword') && !this.hasChangePassword) { 
      } else {
        formData.append(key, this.userForm.value[key]);
      }
    }
 
    if(this.userForm.value['email'] !== this.userInfo.email && this.emailList.indexOf(this.userForm.value['email']) !== -1) {
      this.resError = {type: 'error', message: 'This email already exists.'};
      this.loadingService.display(false);
    } else if(this.hasChangePassword && 
              this.userForm.value['oldPassword'] === '') {
      this.resError = {type: 'error', message: 'old password is required'};
      this.loadingService.display(false);
    // } else if(this.hasChangePassword && 
    //           this.userForm.value['oldPassword'] !== '' && 
    //           this.userForm.value['password'] === '') {
    //   this.resError = {type: 'error', message: 'new_pw required'};
    //   this.loadingService.display(false);
    } else {
      this._http._update(`${this.appConfig.config.api.users}/${this.userInfo.id}`, formData, 'form-data').subscribe(
        data => {
          this.hasChange.emit(true);
          Object.keys(data).forEach(name => {
            if (this.currentUser[name]) {
              this.currentUser[name] = data[name];
            }
          });
          this.storage.setCurrentUser(this.currentUser);
          this.close();
          this.loadingService.display(false);
          this.resError = null;
        },
        error => {
          this.loadingService.display(false);
          this.resError = {
            type: 'error',
            message: JSON.parse(error).message
          };
          if(this.resError.message.indexOf('gender') > -1) {
            this.resError.message = 'Please choose a gender.';
          }
          // // console.log("error", this.resError);
        }
      )
    }
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getEmail() {
    // get email exists
    this.emailList = [];
    this.userInfo = this._http._getList(`${this.appConfig.config.api.users}`).subscribe(
      data => {
        data.forEach(element => {
          this.emailList.push(element.email);
        });
      },
      error => {
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        // console.log(error)
      }
    )
  }
  checkValidDay() {
    var maxDay = 31;
    var dayValue = this.userForm.value['dayInput'];
    var newNum = parseFloat(dayValue);
    if (newNum > maxDay) {
      //console.log('input not in range (' + min + ", " + max + ")");
      this.userForm.controls['dayInput'].setValue('');
    }
  }
  checkValidYear() {
    var currentYear = new Date().getFullYear();
    var yearValue = this.userForm.value['yearInput'];
    var newNum = parseInt(yearValue);
    if (newNum > currentYear) {
      this.userForm.controls['yearInput'].setValue('');
    }
  }
}
