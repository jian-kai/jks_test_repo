import { Component, OnInit, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ModalService } from '../../../core/services/modal.service';
import { LoadingService } from '../../../core/services/loading.service';
import { StorageService } from '../../../core/services/storage.service';
import { CartService } from '../../../core/services/cart.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AlertService } from '../../../core/services/alert.service';
import $ from "jquery";

import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from '../../../config/app.config';
import { DialogService } from '../../../core/services/dialog.service';
import { UsersService } from '../../../auth/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  currentUser: object;
  deliveryAddressList: Array<any> = [];
  billingAddressList: Array<any> = [];
  cardsList: Array<any>;
  // addressItem: Array<any>;
  private addressId: number;
  isloadingPersional: boolean = true;
  isloadingAddress: boolean = true;
  isloadingPayment: boolean = true;
  public isLoading: boolean = true;
  private resErrorAddress: any;
  private resErrorCard: any;
  public defaultShipping: any;
  public defaultBilling: any;
  public errorCardId: any;
  public i18nDialog: any;
  public strDeleteAdress: string;
  public strDeletePayment: string;
  public strDelete: string;

  constructor(
    el: ElementRef,
    private loadingService: LoadingService,
    public storage: StorageService,
    public modalService: ModalService,
    private _http: HttpService,
    private alertService: AlertService,
    public appConfig: AppConfig,
    private _dialogService: DialogService,
    private usersService: UsersService,
    private router: Router,
    private translate: TranslateService,
  ) {
    if (!this.storage.getCurrentUserItem() || !this.storage.getCurrentUserItem().id) {
      // console.log('User is not exist');
      return;
    }
    this.loadingService.display(true);
    // set user info
    this.getUserinfo();
    // set delivery addresses
    this.getDeliveryAddressList();
    // set cards
    this.getCardsList();
    this.loadingService.display(false);

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  ngOnInit() {
    this.usersService.justLogout.subscribe((value: boolean) => {
      if (value) {
        this.router.navigate(['/']);
      }
    })
  }

  // get user info 
  getUserinfo() {
    this.isloadingPersional = true;
    this._http._getDetail(`${this.appConfig.config.api.users}/${this.storage.getCurrentUserItem().id}`).subscribe(
      data => {
        this.currentUser = data;
        this.isloadingPersional = false;
      },
      error => {
        // console.log(error.error);
      }
    )
  }

  // get delivery address list
  getDeliveryAddressList() {
    this.isloadingAddress = true;
    this.resErrorAddress = null;
    this.resErrorCard = null;
    this.deliveryAddressList = [];
    this.billingAddressList = [];
    this._http._getList(`${this.appConfig.config.api.delivery_address}`.replace('[userID]', this.storage.getCurrentUserItem().id)).subscribe(
      data => {
        this.deliveryAddressList = data;
        this.billingAddressList = data;
        // isDefault
        this.defaultShipping = this.storage.getCurrentUserItem().defaultShipping;
        this.defaultBilling = this.storage.getCurrentUserItem().defaultBilling;
        // if (this.deliveryAddressList && this.deliveryAddressList.length === 1) {
        //   this.deliveryAddressList[0].isDefault = true;
        //   this.billingAddressList[0].isDefault = true;
        // }
        this.isloadingAddress = false;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // get Cards list
  getCardsList() {
    this.isloadingPayment = true;
    this.resErrorAddress = null;
    this.resErrorCard = null;
    this.errorCardId = null;
    this.cardsList = [];

    this._http._getList(`${this.appConfig.config.api.users}/${this.storage.getCurrentUserItem().id}/cards?CountryId=${this.storage.getCountry().id}`).subscribe(
      data => {
        this.cardsList = data;
        // console.log('cardsList', data);
        this.isloadingPayment = false;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  showEditUserModal() {
    this.modalService.reset('edit-user-modal');
    this.modalService.open('edit-user-modal', this.currentUser);
  }

  // show change password modal
  changePassword() {
    this.modalService.reset('change-password-modal');
    this.modalService.open('change-password-modal');
  }

  // create address
  addNewAddress(_addressType?: string) {
    let data: any = { reGetDB: true };
    if (_addressType) {
      data.type = _addressType;
    }
    data.enableSetDefault = true;
    this.modalService.reset('delivery-address-modal-form');
    this.modalService.open('delivery-address-modal-form', data);
  }

  // edit address
  editAddress(_address: any, _addressType?: string) {
    if (_addressType) {
      _address.type = _addressType;
    }
    _address.enableSetDefault = true;
    this.modalService.reset('delivery-address-modal-form');
    this.modalService.open('delivery-address-modal-form', _address);
  }

  // Confirm Deletion modal
  showConfirmDeletionModal(item: any) {
    // this.modalService.open('confirm-deletion-modal', { item: item, event: null });
  }

  // delete address
  deleteAddress(id) {
    this.translate.get('lbl').subscribe(res => {
    this.i18nDialog = res;
    });
    this.strDeleteAdress = this.i18nDialog.user.setting.delete_address;
    this.strDelete = this.i18nDialog.user.setting.btn_delete;
    this._dialogService.confirm(this.strDeleteAdress, this.strDelete).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        if (!id) {
          // console.log("Id not exist!!")
        }
        this._http._delete(`${this.appConfig.config.api.delivery_address}/${id}`.replace('[userID]', this.storage.getCurrentUserItem().id)).subscribe(
          data => {
            this.getDeliveryAddressList();
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
            // console.log(error.error);
            this.resErrorAddress = {
              type: 'error',
              message: JSON.parse(error).message
            };
          }
        )

        this.loadingService.status.subscribe((value: boolean) => {
          this.isLoading = value;
        })
      }
    }));
  }

  // when user edited address
  hasChangeAddress(status: boolean) {
    this.loadingService.display(true);
    if (status) {
      this.modalService.close('delivery-address-modal-form');
      this.deliveryAddressList = [];
      this.billingAddressList = [];
      this.getDeliveryAddressList();
    }
    this.loadingService.display(false);

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // when user edit profile
  hasChangeUserInfo(status: boolean) {
    this.loadingService.display(true);
    if (status)
      this.getUserinfo();
    this.loadingService.display(false);

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  // show form edit card
  editCard(_card: any) {
    this.modalService.open('card-modal-form', _card);
    this.resErrorAddress = null;
    this.resErrorCard = null;
    this.errorCardId = null;
  }

  // create card
  addNewCard(card) {
    this.modalService.open('card-modal-form', { reGetDB: true, enableSetDefault: true });
  }

  // when user edited card
  hasChangeCard(data: any) {
    this.loadingService.display(true);
    if (data)
      this.getCardsList();
      this.loadingService.display(false);

      this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  // delete card
  deleteCard(card) {
    this.translate.get('lbl').subscribe(res => {
      this.i18nDialog = res;
    });
    this.strDeletePayment = this.i18nDialog.user.setting.delete_payment;
    this.strDelete = this.i18nDialog.user.setting.btn_delete;
    this._dialogService.confirm(this.strDeletePayment, this.strDelete).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        if (!card.id) {
          // console.log("Id not exist!!")
        }
        this._http._delete(`${this.appConfig.config.api.users}/${this.storage.getCurrentUserItem().id}/cards/${card.id}`).subscribe(
          data => {
            this.getCardsList();
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
            this.resErrorCard = {
              type: 'error',
              message: JSON.parse(error).message
            };
            this.errorCardId = card.id;
            // console.log(error.error);
          }
        )

        this.loadingService.status.subscribe((value: boolean) => {
          this.isLoading = value;
        })
      }
    }));
  }

  checkDelete(event) {}

  //setCardDefault
  setCardDefault(_card: any) {
    if (_card.isDefault) { return false; }
    this.loadingService.display(true);
    this._http._update(`${this.appConfig.config.api.users}/${this.storage.getCurrentUserItem().id}/cards/set-default`, {cardId: _card.id, CountryId: this.storage.getCountry().id}).subscribe(
      data => {
        this.cardsList.forEach(item => {
          if (item.isDefault) {
            item.isDefault = false;
          }
          if (item.id === _card.id) {
            item.isDefault = true;
          }
        });
        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error);
        this.loadingService.display(false)
      }
    )
  }
}
