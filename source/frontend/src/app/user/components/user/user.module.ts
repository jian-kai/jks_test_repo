import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

// Components
import { UserComponent } from './user.component';
import { UserOrderListComponent } from '../order/user-order-list/user-order-list.component';
import { UserOrderDetailComponent } from '../order/user-order-detail/user-order-detail.component';
import { UserSubscriptionComponent } from '../subscription/user-subscription.component';
import { UserSubscriptionOverviewComponent } from '../subscription/user-subscription-overview/user.subscription-overview.component';
import { UserSubscriptionViewDetailComponent } from '../subscription/user-subscription-view-detail/user-subscription-view-detail.component';
import { EditUserModalComponent } from '../setting/edit-user-modal/edit-user-modal.component';
import { ChangePasswordModalComponent } from '../setting/change-password-modal/change-password-modal.component';
import { SettingComponent } from '../setting/setting.component';
import { PaginationComponent } from '../../../core/components/pagination/pagination.component';

// Modules
import { SharedModule } from './../../../core/directives/share-modules';

// routes
export const ROUTES: Routes = [
  { path: '', component: UserComponent,
    children: [
      { path: '', redirectTo: 'settings', pathMatch: 'full' },
      { path: 'orders', component: UserOrderListComponent },
      { path: 'orders/:orderId', component: UserOrderDetailComponent },
      { path: 'shave-plans', component: UserSubscriptionComponent },
      { path: 'settings', component: SettingComponent }
    ]
  }
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    UserComponent,
    UserOrderListComponent,
    UserOrderDetailComponent,
    UserSubscriptionComponent,
    SettingComponent,
    PaginationComponent,
    UserSubscriptionOverviewComponent,
    UserSubscriptionViewDetailComponent,
    EditUserModalComponent,
    ChangePasswordModalComponent
  ]
})
export class UserModule { }
