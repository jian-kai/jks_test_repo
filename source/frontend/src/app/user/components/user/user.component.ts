import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { StorageService } from '../../../core/services/storage.service';
import { CartService } from '../../../core/services/cart.service';
import { UsersService } from '../../../auth/services/users.service';
import { LoadingService } from '../../../core/services/loading.service';
import { NotificationService } from '../../../core/services/notification.service';
import $ from "jquery";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public userMenuSelected: string;

  constructor(private userService: UsersService,
    private loadingService: LoadingService,
    public storage: StorageService,
    public cartService: CartService,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private router: Router,) {
      if(!this.storage.getCountry().isWebEcommerce) {
        this.router.navigate(['/']);
      }
    }

  ngOnInit() {
    let key = 'link_subscription';
    if(this.router.url.indexOf('/user/orders') !== -1) {
      key = 'link_order';
    }
    if(this.router.url.indexOf('/user/settings') !== -1) {
      key = 'link_settings';
    }
    this.translate.get('lbl.user').subscribe(res => {
      this.userMenuSelected = res[key];
    });
  }

  logout(event) {
    event.preventDefault();
    this.cartService.clearCart(true);
    this.storage.clearFlagSyncCart();
    this.userService.logout();
    this.translate.get('notification').subscribe(res => {
      this.notificationService.addNotification(res.logout);
    });    
    setTimeout(() => {
       this.notificationService.addNotification(null);
    }, 2500);
    this.router.navigate(['/']);
  }

  selectUserMenu(_event, _key) {
    this.translate.get('lbl.user').subscribe(res => {
      this.userMenuSelected = res[_key];
      $("#menuSelected").addClass("collapsed");
      $("#userMenu").removeClass("show");
    });
  }

}
