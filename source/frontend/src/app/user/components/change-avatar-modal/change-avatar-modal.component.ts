import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { UsersService } from '../../../auth/services/users.service';
import { AuthService } from '../../../auth/services/auth.service';
import {AlertService} from '../../../core/services/alert.service';
import {StorageService} from  '../../../core/services/storage.service';
import { AppConfig } from 'app/config/app.config';
import {HttpService} from '../../../core/services/http.service';
import {RequestService} from 'app/core/services/request.service';
import {Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'change-avatar-modal',
  templateUrl: './change-avatar-modal.component.html',
  styleUrls: ['./change-avatar-modal.component.scss']
})
export class ChangeAvatarModalComponent extends ModalComponent {

  private _usersService: UsersService;
  private _router: Router;

  constructor(modalService: ModalService,
    el: ElementRef,
    usersService: UsersService,
    router: Router,
    builder: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    public storage: StorageService,
    public appConfig: AppConfig,
    // private _http: HttpService,
    private _http: Http,
    private requestService: RequestService
  ) {
    super(modalService, el);
    this.id = 'change-avatar-modal';
  }

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('avatar', file);
      // console.log(file);
      let _data = {'avatar': file};
      let curUser = this.storage.getCurrentUserItem();
      if (curUser) {
        this.requestService.getAuthHeaders().headers.append('Content-Type', 'multipart/form-data')
        return this._http.post(`${this.appConfig.config.api.users}/${curUser.id}`, _data, this.requestService.getAuthHeaders())
        .map(
          (res: Response) => {
            let body = res.json();
              // console.log(body)
          }
        )
        .catch(
          (error: Response | any) => {
            // console.log(error.json())
            return Observable.throw(error.json());
          }
        );
        // this._http._update(`${this.appConfig.config.api.users}/${curUser.id}`, _data).subscribe(
        //   data => {
        //     // console.log('ok');
        //   },
        //   error => {
        //     // console.log(error);
        //   }
        // )
      }
    }
    return;
  }

}
