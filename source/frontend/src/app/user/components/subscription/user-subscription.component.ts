import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-subscription',
  templateUrl: './user-subscription.component.html',
  styleUrls: ['./user-subscription.component.scss']
})
export class UserSubscriptionComponent implements OnInit {
  public isDashboard: boolean = true;
  public subscriptionId: number;

  constructor() {
    this.subscriptionId = null;
  }

  ngOnInit() {
  }

  getEmitIsDashboard(status: boolean) {
    this.isDashboard = status;
  }

  // get sub id emited from subscription list
  getEmitSubscriptionId(id: number) {
    if (id) {
      this.isDashboard = false;
      this.subscriptionId = id;
    }
  }

}
