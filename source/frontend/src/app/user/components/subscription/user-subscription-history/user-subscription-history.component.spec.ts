import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubscriptionHistoryComponent } from './user-subscription-history.component';

describe('UserSubscriptionHistoryComponent', () => {
  let component: UserSubscriptionHistoryComponent;
  let fixture: ComponentFixture<UserSubscriptionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubscriptionHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubscriptionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
