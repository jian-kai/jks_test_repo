import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import {StorageService} from '../../../../core/services/storage.service';

@Component({
  selector: 'app-user-subscription-history',
  templateUrl: './user-subscription-history.component.html',
  styleUrls: ['./user-subscription-history.component.scss']
})
export class UserSubscriptionHistoryComponent implements OnInit {
  public subscriptionId: number;
  @Output() emitSubscriptionId: EventEmitter<number> = new EventEmitter<number>();
  public currentSubscriptions: any = [];
  public historySubscriptions: any = [];
  public currentUser : any;
  public isloading: boolean = true;
  public isViewDetail: boolean = false;
  constructor(private _http: HttpService,
    public appConfig: AppConfig,
    public storage: StorageService) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    } 
  }

  ngOnInit() {
    this.getSubscriptionList();
  }

  viewDetails(id: number) {
    // this.subscriptionId = id;
    this.emitSubscriptionId.emit(id);
  }

  // get subscription list of current user
  getSubscriptionList() {
    this.currentSubscriptions = [];
    this.historySubscriptions = [];
    this.isloading = true;
    this._http._getList(`${this.appConfig.config.api.subscription}`.replace('[userID]', this.currentUser.id)).subscribe(
      data => {
        // console.log('data',data);
        this.currentSubscriptions = data.filter(order => order.status === "Processing");
        this.historySubscriptions = data;
        this.isloading = false;
      },
      error => {
        // console.log(error.error)
      }
    )
  }

  // get emit from subscription detail page
  getEmitIsViewDetail(status: boolean) {
    this.isViewDetail = status;
  }

}
