import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubscriptionViewDetailComponent } from './user-subscription-view-detail.component';

describe('UserSubscriptionViewDetailComponent', () => {
  let component: UserSubscriptionViewDetailComponent;
  let fixture: ComponentFixture<UserSubscriptionViewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubscriptionViewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubscriptionViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
