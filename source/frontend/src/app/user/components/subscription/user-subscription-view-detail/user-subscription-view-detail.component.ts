import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { GlobalService } from '../../../../core/services/global.service';
import { ModalService } from '../../../../core/services/modal.service';
import { Router } from '@angular/router';
import { LoadingService } from '../../../../core/services/loading.service';
import { PlanService } from '../../../../core/services/plan.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { CountriesService } from '../../../../core/services/countries.service';

import { TranslateService } from '@ngx-translate/core';
import { ProductTranslatePipe } from '../../../../core/pipes/product-translate.pipe';

@Component({
  selector: 'app-user-subscription-view-detail',
  templateUrl: './user-subscription-view-detail.component.html',
  styleUrls: ['./user-subscription-view-detail.component.scss'],
  providers: [ProductTranslatePipe]
})

export class UserSubscriptionViewDetailComponent implements OnInit {
  @Input() subscriptionId: number;
  @Output() emitIsDashboard: EventEmitter<boolean> = new EventEmitter<boolean>(false);
  show: boolean = false;
  public isloading: boolean = true;
  public subsInfo: any;
  public currentUser: any;
  public currentSub: any;
  public selectedCard: any;
  public showChange: boolean = false;
  
  
  // free trial
  public planGroups: any = [];
  public activeGroup: any = {};
  public refillGroups: any = {};
  public activePayment: any = {};
  public planTypesName: any = [];
  public groupPlanTypes: any = [];

  public bladeList: any = [];
  public bladeListWithoutY: any = [];
  public bladeActiveEntry: any = {};
  public bladeActive: any = {};
  public originBladeActive: any = {};
  public refillList: any = [];
  public refillEntry: any = {};
  public refillActive: any = {};
  public paymentTypeList: any = [];
  public usageList: any = [];
  public usageActive: any;
  public usageOptionActive: any = [];
  public originUsageActive: any;
  public paymentTypeEntry: any = {};
  public paymentTypeActive: any;
  public planOptionListActive: any = [];
  public planOptionActive: any;
  public originPlanOptionActive: any;
  public originPaymentTypeActive: any = {};
  public planActive: any = {};
  public planTypeSelectedEntry: String;
  public planTypeSelected: String;
  public plansSlider = [];
  public planList: Object = {};
  public allowUpdate : boolean = false;
  public i18nDialog: any;
  public currentCountryId: any;
  public currentSubId: any;
  public allowEditCancel : boolean = true;

  // custom plan
  public cartridges: any = [];
  public shaveCreams: any = [];
  public planTypes: any = [];
  public productList: any = [];
  public productTypes: any = [];
  public activeCartridge: any;
  public activeShaveCream: any;
  public activePlanType: any;
  public nextBillingPriceForCustom: any;
  public originActiveCartridge: any;
  public originActiveShaveCream: any;
  public originActivePlanType: any;

  constructor(private _http: HttpService,
    public appConfig: AppConfig,
    private globalService: GlobalService,
    public storage: StorageService,
    private router: Router,
    public modalService: ModalService,
    private loadingService: LoadingService,
    public planService: PlanService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private countriesService: CountriesService,
    private ProductTranslatePipe: ProductTranslatePipe,) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
  }

  ngOnInit() {
    this.loadingService.display(true);
    this.getSubscriptionDetail();
    this.countriesService.language.subscribe(country => {

      if (this.subsInfo.isTrial) {
        this.getListPlanTrial();
      }
      if(this.subsInfo.isCustom) {
        this.getListPlanCustom();
      }
    })
  }

  // get order detail info
  getSubscriptionDetail() {
    this.isloading = true;
    this._http._getDetail(`${this.appConfig.config.api.subscription}/${this.subscriptionId}`.replace('[userID]', this.currentUser.id)).subscribe(
      data => {
        this._http._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
          koDeliverDate => {
          if(!data.nextDeliverDate) {
            data.nextDeliverDate = new Date(data.createdAt);
            data.nextDeliverDate = data.nextDeliverDate.setDate(data.nextDeliverDate.getDate() + 13);

            // check deliver Date for Korea pre-order
            let countryCode = data.CustomPlan ? data.CustomPlan.Country.code : data.PlanCountry.Country.code;
            if(countryCode.toUpperCase() === 'KOR') {
              if (koDeliverDate) {
                data.nextDeliverDate = new Date(koDeliverDate);
                data.nextDeliverDate = data.nextDeliverDate.setDate(data.nextDeliverDate.getDate() + 13);
              }
            }
          }
          this.subsInfo = data;
          // console.log('this.subsInfo', this.subsInfo);
          if (this.subsInfo.isTrial) {
            let currentDate = new Date();
            var str = "";
            str += currentDate.getFullYear() + '-';
            str += ( (currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1) ) + '-';
            str += (currentDate.getDate() < 10) ? ( '0' + currentDate.getDate() ) : currentDate.getDate();
            
            currentDate = new Date(str);
            let _currentDate = currentDate.getFullYear() + '-' + currentDate.getMonth() + '-' + currentDate.getDate();

            var seconds: any  = 0;
            if(this.subsInfo.startDeliverDate) {
              let _startDeliverDate = new Date(this.subsInfo.startDeliverDate);
              seconds = (currentDate.getTime() - _startDeliverDate.getTime());
            } else {
              seconds = -1;
            }

            if (seconds < 0) {
              this.allowUpdate = true;
            } else {
              this.allowUpdate = true;
              // this.allowUpdate = false;
            }

            if(this.subsInfo.PlanCountry) {
              this.currentCountryId = this.subsInfo.PlanCountry.Country.id;
            }
            if(this.subsInfo.CustomPlan) {
              this.currentCountryId = this.subsInfo.CustomPlan.Country.id;
            }

            this.getListPlanTrial();

            if(this.subsInfo.orders && this.subsInfo.orders.length === 1) {
              if(!this.subsInfo.orders[0].isSubsequentOrder && this.subsInfo.orders[0].states === 'Delivering') {
                this.allowEditCancel = false;
              }
            }
            
          } else if(this.subsInfo.isCustom) {
            if(this.subsInfo.PlanCountry) {
              this.currentCountryId = this.subsInfo.PlanCountry.Country.id;
            }
            if(this.subsInfo.CustomPlan) {
              this.currentCountryId = this.subsInfo.CustomPlan.Country.id;
            }

            this.allowUpdate = true;
            this.getListPlanCustom();
          } else {
            this.isloading = false;
          }
          

          // this.subsInfo.nextWorkingDate = data.nextDeliverDate ? this.globalService.calculateWorkingDay(data.nextDeliverDate, 6) : null;
          this.subsInfo.nextWorkingDate = new Date(this.subsInfo.nextDeliverDate);
          this.subsInfo.nextWorkingDate = this.subsInfo.nextWorkingDate.setDate(this.subsInfo.nextWorkingDate.getDate() + 3);
          
          this.show = true;
          //this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
        })
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error);
      }
    )
  }

  /// back to dashboard
  backToDashboard() {
    this.emitIsDashboard.emit(true);
  }

  viewOrderDetails(orderId) {
    this.router.navigate([`/user/orders/${orderId}`], { queryParams: {back: 'pl' } });
  }

  // change cart another to Billing
  changeCard(_subs: any) {
    this.currentSub = _subs;
    if(_subs.PlanCountry) {
      this.currentCountryId = _subs.PlanCountry.Country.id;
    }
    if(_subs.CustomPlan) {
      this.currentCountryId = _subs.CustomPlan.Country.id;
    }
    this.currentSubId = this.currentSub.id;
    this.selectedCard = {id: this.currentSub.Card.id};
    this.modalService.open('card-list-modal', {reGetDB: true, CountryId: this.currentCountryId});
  }

  hasResetDefault(data) {
    if (data && data.id !== this.currentSub.Card.id) {
      this.isloading = true;
      let _data = {cardId : data.id}
      this._http._updatePut(`${this.appConfig.config.api.subscription}/${this.currentSub.id}`.replace('[userID]', this.currentUser.id), _data).subscribe(
        data => {
           if (data) {
            this.getSubscriptionDetail();
           }
        },
        error => {
          // console.log(error.error)
        }
      )
    }
  }

  // subscriber if address has change from card list modal
  hasChangeCard(_res) {
    if (_res.data && _res.data.id === this.currentSub.Card.id) {
      // this.getCardList();
      this.getSubscriptionDetail();
    }
  }

  getListPlanTrial() {
    this.isloading = true;
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.planList = [];
    this.plansSlider = [];
    // get all plans
    let options = {
      userTypeId: 1,
      CountryId: this.currentCountryId
    };
    let params = this.globalService._URLSearchParams(options);
    this._http._getList(`${this.appConfig.config.api.free_trial}?` + params.toString()).subscribe(
      data => {
        if (data) {
          let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
          let currentCountry = this.storage.getCountry() ? this.storage.getCountry().code.toUpperCase() : 'MYR';
          this.planGroups = this.planService.groupPlan(data);
          // console.log('this.planGroups -- ', this.planGroups)
          // get blade list
          // this.bladeList = this.planService.groupPlan(data);
          // // console.log('this.bladeList --- ', this.bladeList);
          // if (this.bladeList && this.bladeList.length > 0) {
          //   // set blade active
          //   let _groupNameActive: string = '';
          //   this.bladeList.forEach(blade => {
          //     blade.item.forEach(element => {
          //       if (this.subsInfo.PlanCountry.PlanId === element.planCountry[0].PlanId) {
          //         _groupNameActive = blade.name;
          //       }
          //     });
          //   });
          //   if (_groupNameActive === 'Y-TK3' || _groupNameActive === 'Y-TK5' || _groupNameActive === 'Y-S6') {
          //     _groupNameActive = _groupNameActive.split('Y-')[1];          
          //   }
          //   let _planGroupsWithoutY = this.planGroups.filter(group => (group.name !== 'Y-TK3' && group.name !== 'Y-TK5' && group.name !== 'Y-S6'));
          //   this.bladeListWithoutY = _planGroupsWithoutY;
          //   let _bladeActiveTmp = this.bladeListWithoutY.filter(blade => blade.name === _groupNameActive);
            this.bladeActive = this.planGroups.find(item => this.subsInfo.PlanCountry.Plan.PlanGroup ? item.id === this.subsInfo.PlanCountry.Plan.PlanGroup.id : false);
            // console.log('this.bladeActive --- ', this.bladeActive);
            // // console.log('this.paymentTypeList --- ', this.paymentTypeList);
            // if (this.subsInfo.PlanCountry && this.subsInfo.PlanCountry.Plan && this.subsInfo.PlanCountry.Plan.PlanType.name === '12 Months') {
            //   this.paymentTypeActive = this.paymentTypeList[0];
            // } else {
            //   this.paymentTypeActive = this.paymentTypeList[1];
            // }
            this.paymentTypeList = this.appConfig.config.trial_payment_types;

            if(!this.bladeActive) {
              this.bladeActive = {name: this.subsInfo.PlanCountry.Plan.sku.slice(-2)};
              this.allowUpdate = false;
            } else {
              this.paymentTypeList.forEach(element => {
                this.bladeActive[element].forEach(plan => {
                  if(plan.planCountry.id === this.subsInfo.PlanCountry.id) {
                    this.paymentTypeActive = element;
                    this.usageActive = plan;
                    this.planOptionActive = this.subsInfo.PlanOption;
                    this.usageOptionActive = this.bladeActive[element];
                    this.planOptionListActive = this.subsInfo.PlanCountry.planOptions;
                  }
                });
                
              });
            }
            this.paymentTypeList.forEach((element, index) => {
              if(this.bladeActive[element].length <= 0) {
                delete this.bladeActive[element];
                this.paymentTypeList.splice(index, 1);
              }
            });

            if(!this.paymentTypeActive) {
              this.paymentTypeActive = this.subsInfo.PlanCountry.Plan.sku.indexOf('Y-') !== -1 ? 'Annual' : 'Pay As You Go';
              this.allowUpdate = false;
            }
            if(!this.usageActive) {
              this.usageActive = this.subsInfo.PlanCountry.Plan;
              if(this.usageActive.planTranslate[0]) {
                this.usageActive.planTranslate = this.usageActive.planTranslate.find(value => value.langCode.toUpperCase() === currentLang);
              }
              if(this.usageActive.planCountry[0]) {
                this.usageActive.planCountry = this.usageActive.planCountry.find(value => value.Country.code.toUpperCase() === currentCountry);
              }
              this.allowUpdate = false;
            }
            // console.log('this.paymentTypeActive --- ', this.paymentTypeActive);
            this.originPaymentTypeActive = this.paymentTypeActive;
            this.originBladeActive = this.bladeActive;
            this.originUsageActive = this.usageActive;
            this.originPlanOptionActive = this.planOptionActive;
            // console.log('this.usageActive --- ', this.usageActive);
            // console.log('this.planOptionListActive --- ', this.planOptionListActive);
            // console.log('this.planOptionActive --- ', this.planOptionActive);

            // this.getUsage();
            // if (!this.usageActive.sku) {
            //   let _usageActive = this.usageOptionActive.filter(item => (item.planCountry[0].PlanId === this.subsInfo.PlanCountry.Plan.id));
            //   this.usageActive = _usageActive[0];
            // }

            
            this.isloading = false;

          // }

          // // console.log("2 Months", this.paymentTypeList['2 Months'])
        }

        this.loadingService.display(false);
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )
    
  }

  getListPlanCustom() {
    this.isloading = true;
    this.productList = [];
    this.productTypes = [];
    this.planTypes = [];

    let options = {
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this._http._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        data.forEach((product, index) => {
          let productImage: any;

          // fetch to find default image
          product.productImages.forEach((imageItem, index) => {
            if (imageItem.isDefault) {
              productImage = imageItem;
            }
          });

          // push to product list
          if(this.productList[product.ProductType.name]) {
            this.productList[product.ProductType.name].push(product);
          } else {
            this.productList[product.ProductType.name] = [product];
            this.productTypes.push(product.ProductType);
          }
        });

        this.cartridges = this.productList['Cartridge Packs'];
        this.shaveCreams.push({sku: null});
        this.shaveCreams.push(this.productList['Skin Care'].filter(product => product.sku === 'A5/2018')[0]);
        
        this.subsInfo.CustomPlan.customPlanDetail.forEach(element => {
          if(element.ProductCountry.Product.sku !== 'A5/2018') {
            this.activeCartridge = this.cartridges.find(cartridge => cartridge.sku === element.ProductCountry.Product.sku);
          } else {
            this.activeShaveCream = this.shaveCreams.find(shaveCream => shaveCream.sku === element.ProductCountry.Product.sku);
          }
        });

        this.activeCartridge = this.activeCartridge ? this.activeCartridge : this.cartridges[0];
        this.activeShaveCream = this.activeShaveCream ? this.activeShaveCream : this.shaveCreams[0];
        // console.log('this.cartridges', this.cartridges);
        // console.log('this.shaveCreams', this.shaveCreams);
        this.originActiveCartridge = this.activeCartridge;
        this.originActiveShaveCream = this.activeShaveCream;

        this.nextBillingPriceForCustom = this.subsInfo.pricePerCharge;

        this._http._getList(`${this.appConfig.config.api.plan_type}`).subscribe(
          data => {
            data.forEach((planType, index) => {
              if(planType.prefix.indexOf('2-') > -1 ||
              planType.prefix.indexOf('3-') > -1 ||
              planType.prefix.indexOf('4-') > -1) {
                this.planTypes.push(planType);
              }
            });
            this.activePlanType = this.subsInfo.CustomPlan.PlanType ? this.subsInfo.CustomPlan.PlanType : this.planTypes[0];
            this.originActivePlanType = this.activePlanType;
						// console.log('1111111111111',this.activePlanType);
            this.loadingService.display(false);
            this.isloading = false;
          },
          error => {
            // console.log(error.error)
            this.loadingService.display(false);
            this.isloading = false;
          }
        )
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
        this.isloading = false;
      }
    )
  }

  getUsage() {
    this.usageList['Paygo'] = [];
    this.usageList['Paygo'] = this.bladeActive.item;
    let _bladeY = this.planGroups.filter(group => (group.name === 'Y-' + this.bladeActive.name))
    this.usageList['Annual'] = _bladeY[0].item;
    // console.log('order info -- ', this.subsInfo);
    // console.log('this.usageList --- ', this.usageList);
    this.usageOptionActive = this.usageList[this.paymentTypeActive];
    // console.log('this.usageOptionActive --- ', this.usageOptionActive);
  }

  getInfoEntryTrial() {
    // console.log('order info -- ', this.subsInfo);
    // console.log('this.subsInfo', this.subsInfo);
    this.bladeList.forEach(blade => {
      if (!this.bladeActiveEntry.item) {
        blade.item.forEach(item => {
          if (item.planCountry && this.subsInfo.PlanCountry.id === item.planCountry.id) {
            if((blade.name === 'Y-TK3' || blade.name === 'Y-TK5' || blade.name === 'Y-S6')) {
              let spl = blade.name.split('Y-');
              this.bladeActiveEntry = this.bladeList.filter(blade => blade.name === spl[1]);
              this.bladeActiveEntry = this.bladeActiveEntry[0];
              this.bladeActive = this.bladeActiveEntry;
            } else {
              this.bladeActiveEntry = blade;
              this.bladeActive = blade;
            }

            // console.log('this.bladeActiveEntry --- ', this.bladeActiveEntry)
          }
        });
      }
    });
  }

  getRefillList(item) {
    item.forEach(plan => {
      this.refillList.push(plan.PlanType.name);
    });
    // console.log('this.refillList --- ', this.refillList)
  }

  updateShavePlan() {
    this.showChange = true;
    // console.log('this.usageActive --- ', this.usageActive)
  }
  
  cancel() {
    this.paymentTypeActive = this.originPaymentTypeActive;
    this.bladeActive = this.originBladeActive;
    this.usageActive = this.originUsageActive;
    this.planOptionActive = this.originPlanOptionActive;

    this.activeCartridge = this.originActiveCartridge;
    this.activeShaveCream = this.originActiveShaveCream;
    this.activePlanType = this.originActivePlanType;

    this.showChange = false;
  }

  saveChange() {
    this.loadingService.display(true);
    if(this.subsInfo.isTrial) {
      let _odata = {planCountryId: this.usageActive.planCountry.id};
      if(this.planOptionActive) {
        _odata['planOptionId'] = this.planOptionActive.id;
      }
      this._http._updatePut(`${this.appConfig.config.api.subscription_trial}/${this.subsInfo.id}`.replace('[userID]', this.storage.getCurrentUser().id),
         _odata).subscribe(
        data => {
          this.showChange = false;
          this.bladeActiveEntry = {};
          this.getSubscriptionDetail();
          // console.log('data', data)
          this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
          // console.log(error.error);
        });
    }
    if(this.subsInfo.isCustom) {
      let _odata = {
        PlanTypeId: this.activePlanType.id,
        CustomPlanId: this.subsInfo.CustomPlanId,
        customPlanDetail: [
          {
            qty: 1,
            CustomPlanId: this.subsInfo.CustomPlanId,
            ProductCountryId: this.activeCartridge.productCountry.id,
            productName: this.activeCartridge.productTranslate[0].name,
            duration: this.activePlanType.name
          }
        ]
      };
      if(this.activeShaveCream.sku) {
        _odata.customPlanDetail.push({
          qty: 1,
          CustomPlanId: this.subsInfo.CustomPlanId,
          ProductCountryId: this.activeShaveCream.productCountry.id,
          productName: this.activeShaveCream.productTranslate[0].name,
          duration: this.activePlanType.name
        });
      }
      this._http._updatePut(`${this.appConfig.config.api.subscription_custom}/${this.subsInfo.id}`.replace('[userID]', this.storage.getCurrentUser().id),
         _odata).subscribe(
        data => {
          this.showChange = false;
          this.bladeActiveEntry = {};
          this.getSubscriptionDetail();
          // console.log('data', data)
          this.loadingService.display(false);
        },
        error => {
          this.loadingService.display(false);
          // console.log(error.error);
        });
    }
  }

  changeGroup(_group: any) {
    let _planGroups = this.planGroups.filter(group => (group.name === _group.name ));
    this.bladeActive = _planGroups[0];
    
    this.paymentTypeActive = this.paymentTypeList[0];
    // this.getUsage();
    
    this.usageOptionActive = this.bladeActive[this.paymentTypeActive];
    this.usageActive = this.bladeActive[this.paymentTypeActive][0];
    this.planOptionListActive = this.usageActive.planCountry.planOptions;
    this.planOptionActive = this.usageActive.planCountry.planOptions[0];
    // console.log('this.usageActive --- ', this.usageActive);
    // console.log('this.planOptionActive --- ', this.planOptionActive);

  }

  changeUsage(_usage: any) {
    this.usageActive = _usage;
    this.planOptionListActive = this.usageActive.planCountry.planOptions;
    this.planOptionActive = this.usageActive.planCountry.planOptions[0];
    // console.log('this.usageActive --- ', this.usageActive);
    // console.log('this.planOptionActive --- ', this.planOptionActive);
  }

  changePayment(_paymentType: any) {
    this.paymentTypeActive  = _paymentType;
    // this.getUsage();
    this.usageOptionActive = this.bladeActive[this.paymentTypeActive];
    this.usageActive = this.bladeActive[this.paymentTypeActive][0];
    this.planOptionListActive = this.usageActive.planCountry.planOptions;
    this.planOptionActive = this.usageActive.planCountry.planOptions[0];
    // console.log('this.usageActive --- ', this.usageActive);
    // console.log('this.planOptionActive --- ', this.planOptionActive);
  }

  changePlanOption(_option: any) {
    this.planOptionActive = _option;
  }

  changeCartridge(_cartridge:any) {
    this.activeCartridge = _cartridge;
    // console.log('this.activeCartridge', this.activeCartridge);
    // console.log('this.activeShaveCream', this.activeShaveCream);
    this.nextBillingPriceForCustom = parseFloat(this.activeCartridge.productCountry.sellPrice) + parseFloat(this.activeShaveCream.productCountry ? this.activeShaveCream.productCountry.sellPrice : 0);
  }

  changeShaveCream(_shaveCream:any) {
    this.activeShaveCream = _shaveCream;
    this.nextBillingPriceForCustom = parseFloat(this.activeCartridge.productCountry.sellPrice) + parseFloat(this.activeShaveCream.productCountry ? this.activeShaveCream.productCountry.sellPrice : 0);
  }

  changePlanType(_planType:any) {
    this.activePlanType = _planType;
  }

  cancelSubs() {    
    console.log("test")
    this.modalService.open('cancellation-journey-modal', this.subsInfo);
    // this.dialogService.confirm(this.i18nDialog.confirm.cancelling_trial_subs).afterClosed().subscribe((result => {
    //   if (result) {
    //     this.loadingService.display(true);  
    //     // readd mising param
    //     this._http._delete(`${this.appConfig.config.api.cancel_trial_plan}/${this.subsInfo.id}?isUserECommerce=true`).subscribe(
    //       data => {
    //         let _plan_name;
    //         if(this.subsInfo.PlanCountry) {
    //           this.subsInfo.PlanCountry.Plan.planTranslate = this.subsInfo.PlanCountry.Plan.planTranslate[0] ? this.subsInfo.PlanCountry.Plan.planTranslate : [this.subsInfo.PlanCountry.Plan.planTranslate];
    //           _plan_name = this.ProductTranslatePipe.transform(this.subsInfo.PlanCountry.Plan.planTranslate, 'name');
    //         }
    //         if(this.subsInfo.CustomPlan) {
    //           _plan_name = this.subsInfo.CustomPlan.description;
    //         }

    //         // console.log('_plan_name --- ', _plan_name)
    //         let _msg = '';
    //         // console.log('_msg --- ', _msg)
    //         // if (this.allowUpdate) {
    //         _msg = this.i18nDialog.confirm.trial_subs_cancelled_succ.replace('{{name}}', _plan_name);
    //         this.subsInfo.status = 'Canceled';
    //         this.showChange = false;
    //         // } else {
    //         //   _msg = this.i18nDialog.confirm.trial_request_to_cancel_succ.replace('{{name}}', _plan_name);
    //         //    this.subsInfo.hasSendCancelRequest = true;
    //         // }           
    //         this.loadingService.display(false);
    //         this.dialogService.result(_msg).afterClosed().subscribe((result => {
    //           //
    //         }));
    //       },
    //       error => {
    //         let message = typeof error === 'string' ? error : JSON.parse(error).message
    //         this.loadingService.display(false);
    //         this.dialogService.result(message).afterClosed().subscribe((result => {
    //           this.loadingService.display(false);
    //         }));
    //         // console.log(error);
    //       }
    //     );

    //   }
    // }));
  }

  revertShavePlan() {
    let _odata = {planCountryId: this.usageActive.planCountry.PlanId}
    this.dialogService.confirm(this.i18nDialog.confirm.reverting_trial_subs).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);  

        this._http._updatePut(`${this.appConfig.config.api.revert_cancel_trial_plan}/${this.subsInfo.id}`, _odata).subscribe(
          data => {
            this.subsInfo.hasSendCancelRequest = false;
            this.loadingService.display(false);
            this.dialogService.result(this.i18nDialog.confirm.trial_subs_reverted_succ).afterClosed().subscribe((result => {
              //
            }));
          },
          error => {
            let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this.dialogService.result(message).afterClosed().subscribe((result => {
              this.loadingService.display(false);
            }));
            // console.log(error);
          }
        );

      }
    }));
  }

  // viewDetails(id: number) {
  //   this.subscriptionId = id;
  //   this.getSubscriptionDetail();
  // }
}
