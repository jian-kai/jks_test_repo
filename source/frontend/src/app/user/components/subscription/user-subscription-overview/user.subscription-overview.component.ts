import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../../../core/services/http.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { GlobalService } from '../../../../core/services/global.service';
import { ModalService } from '../../../../core/services/modal.service';
import { CountriesService } from '../../../../core/services/countries.service';

@Component({
  selector: 'app-user-subscription-overview',
  templateUrl: './user-subscription-overview.component.html',
  styleUrls: ['./user-subscription-overview.component.scss']
})
export class UserSubscriptionOverviewComponent implements OnInit {
  @Output() emitSubscriptionId: EventEmitter<number> = new EventEmitter<number>();
  public subscriptionsList: any = [];
  public currentUser : any;
  public isloading: boolean = true;
  public isViewDetail: boolean = false;
  public currentSub: any;
  public currentSubId: any;
  public selectedCard: any;
  public isUpdatedCard: boolean = false;
  public currentCountryId: any;
  public currentLang: any;

  constructor(private _http: HttpService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private globalService: GlobalService,
    public modalService: ModalService,
    private loadingService: LoadingService,
    public countriesService: CountriesService,
    private router: Router) {
      this.currentUser = this.storage.getCurrentUserItem();
    }

  ngOnInit() {
    this.getSubscriptionList();
    this.countriesService.language.subscribe(
      data => {
        this.getSubscriptionList();
      }
    )
  }
  
  // get subscription list of current user
  getSubscriptionList() {
    this.subscriptionsList = [];
    this.isloading = true;
    this.loadingService.display(true);
    this.currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    this._http._getList(`${this.appConfig.config.api.subscription}`.replace('[userID]', this.currentUser.id)).subscribe(
      data => {
        this._http._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
          koDeliverDate => {
            data.forEach(subs => {
              if(!subs.nextDeliverDate) {
                subs.nextDeliverDate = new Date(subs.createdAt);
                subs.nextDeliverDate = subs.nextDeliverDate.setDate(subs.nextDeliverDate.getDate() + 13);

                // check deliver Date for Korea pre-order
                let countryCode = subs.CustomPlan ? subs.CustomPlan.Country.code : subs.PlanCountry.Country.code;
                if(countryCode.toUpperCase() === 'KOR') {
                  if (koDeliverDate) {
                    subs.nextDeliverDate = new Date(koDeliverDate);
                    subs.nextDeliverDate = subs.nextDeliverDate.setDate(subs.nextDeliverDate.getDate() + 13);
                  }
                }
              }
              if(subs.PlanCountry) {
                if(subs.PlanCountry.Plan.planImages.length > 0) {
                  subs.PlanCountry.Plan.planImages.forEach((element, index) => {
                    if(element.isDefault) {
                      subs.PlanCountry.Plan.planImages = element;
                      return;
                    }
                  });
                }
                // subs.nextWorkingDate = this.globalService.calculateWorkingDay(subs.nextDeliverDate, 6);
                subs.nextWorkingDate = new Date(subs.nextDeliverDate);
                subs.nextWorkingDate = subs.nextWorkingDate.setDate(subs.nextWorkingDate.getDate() + 3);
                this.subscriptionsList.push(subs);
                if(this.currentSub && this.currentSub.id === subs.id) {
                  this.currentSub = subs;
                }
              }
              if(subs.CustomPlan) {
                // if(subs.CustomPlan.Plan.planImages.length > 0) {
                //   subs.CustomPlan.Plan.planImages.forEach((element, index) => {
                //     if(element.isDefault) {
                //       subs.CustomPlan.Plan.planImages = element;
                //       return;
                //     }
                //   });
                // }
                // subs.nextWorkingDate = this.globalService.calculateWorkingDay(subs.nextDeliverDate, 6);
                subs.nextWorkingDate = new Date(subs.nextDeliverDate);
                subs.nextWorkingDate = subs.nextWorkingDate.setDate(subs.nextWorkingDate.getDate() + 3);
                this.subscriptionsList.push(subs);
                if(this.currentSub && this.currentSub.id === subs.id) {
                  this.currentSub = subs;
                }
              }
            });
            
            this.isloading = false;
            this.loadingService.display(false);
          },
          error => {
            this.loadingService.display(false);
          }
        )
      },
      error => {
        // console.log(error.error)
        this.loadingService.display(false);
      }
    )
  }
  
  viewDetails(id: number) {
    // this.subscriptionId = id;
    this.emitSubscriptionId.emit(id);
  }
  
  // get emit from subscription detail page
  getEmitIsViewDetail(status: boolean) {
    this.isViewDetail = status;
  }
  
  updateSupscriptionPreference() {
    // console.log('todo...')
  }

  goToSubscription() {
    this.router.navigate(['/plans']);
  }

  // change cart another to Billing
  changeCard(_subs: any) {
    this.currentSub = _subs;
    if(_subs.PlanCountry) {
      this.currentCountryId = _subs.PlanCountry.Country.id;
    }
    if(_subs.CustomPlan) {
      this.currentCountryId = _subs.CustomPlan.Country.id;
    }
    this.selectedCard = {id: this.currentSub.Card.id};
    this.currentSubId = this.currentSub.id;
    this.modalService.open('card-list-modal', {reGetDB: true, CountryId: this.currentCountryId});
  }

  hasResetDefault(card) {
    if (card && card.id !== this.currentSub.Card.id) {
      //console.log('this.currentSub', this.currentSub);
      // this.isloading = true;
      // let _data = {cardId : data.id}
      // this._http._updatePut(`${this.appConfig.config.api.subscription}/${this.currentSub.id}`.replace('[userID]', this.currentUser.id), _data).subscribe(
      //   data => {
      //     // console.log('data',data);
      //      if (data) {
            this.getSubscriptionList();
      //      }
      //     this.isloading = false;
      //   },
      //   error => {
      //     // console.log(error.error)
      //   }
      // )
    }
  }

  // subscriber if address has change from card list modal
  hasChangeCard(_res) {
    if (_res.data && _res.data.id === this.currentSub.Card.id) {
      // this.getCardList();
      this.getSubscriptionList();
    }
  }

  hasUpdatingCard() {
    this.isUpdatedCard = true;
  }

}
