import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubscriptionOverviewComponent } from './user-subscription-overview.component';

describe('UserSubscriptionOverviewComponent', () => {
  let component: UserSubscriptionOverviewComponent;
  let fixture: ComponentFixture<UserSubscriptionOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubscriptionOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubscriptionOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
