import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { CartService } from '../../../../core/services/cart.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { URLSearchParams, } from '@angular/http';
import { Location } from '@angular/common';
import { GlobalService } from '../../../../core/services/global.service';

@Component({
  selector: 'app-user-order-detail',
  templateUrl: './user-order-detail.component.html',
  styleUrls: ['./user-order-detail.component.scss']
})
export class UserOrderDetailComponent implements OnInit {
  orderId: string;
  state: number;
  barValue: any = [1, 2, 3, 4, 5];
  public isLoading: boolean = true;
  public orderInfo: any;
  public currentUser: any;
  public deliveryDate;
  public orderCurrencyCode: string = '';
  public countries: any;
  public BASE_VALUE = '00000000000';
  public deliverDate1: any;
  public deliverDate2: any;
  constructor(private _http: HttpService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    private loadingService: LoadingService,
    public cartService: CartService,
    private location: Location,
    private globalService: GlobalService,
    ) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      return;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orderId = params['orderId'];
      this.getOrderDetail();
    });
    this.deliveryDate = new Date();
  }

  backToList() {
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    if (params && params.get('back')) {
      this.router.navigate(['/user/shave-plans']);
    } else {
      this.router.navigate(['/user/orders']);
    }
  }

  liveTracking() {
    // console.log('totdo...')
  }

  // get order detail info
  getOrderDetail() {
    this.loadingService.display(true);
    this._http._getDetail(`${this.appConfig.config.api.order}/${this.orderId}`.replace('[userID]', this.currentUser.id)).subscribe(
      data => {
        if(data && Object.keys(data).length > 0) {
          this.orderInfo = data;
          // this.orderCurrencyCode = this.orderInfo.orderDetail[0].currency;
          // console.log('data', data);
          if(data.states === 'On Hold') {
            this.state = 1;
          } else if(data.states === 'Payment Received') {
            this.state = 2;
          } else if(data.states === 'Processing Order') {
            this.state = 3;
          } else if(data.states === 'Deliver Order') {
            this.state = 4;
          } else if(data.states === 'Completed') {
            this.state = 5;
          } else if(data.states === 'Cancel') {
            this.state = 6;
          }
          for(var i = 0; i < this.state; i++){
            this.barValue[i] = '\u2713';
          }
          this.getCountries();
         
          if(!this.orderInfo.Subscriptions[0].startDeliverDate) {
            let currDate = new Date(this.orderInfo.Subscriptions[0].createdAt);
            this.deliverDate1 = currDate.setDate(currDate.getDate() + 13);
            this.deliverDate2 = currDate.setDate(currDate.getDate() + 3);

            if(this.orderInfo.Country.code.toUpperCase() === 'KOR') {
              this._http._getList(`${this.appConfig.config.api.deliver_date_for_korea}`).subscribe(
                data => {
                  if (data) {
                    currDate = new Date(data);
                    this.deliverDate1 = currDate.setDate(currDate.getDate() + 13);
                    this.deliverDate2 = currDate.setDate(currDate.getDate() + 3);
                  }
                  this.loadingService.display(false);
                },
                error => {
                  this.loadingService.display(false);
                }
              )
            } else {
              this.loadingService.display(false);
            }
          } else {
            this.loadingService.display(false);
          }
          
        } else {
          this.loadingService.display(false);
          this.router.navigate(['/']);
        }
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error);
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

  getCountries() {
    if (!this.countries) {
      this._http._getList(this.appConfig.config.api.countries).subscribe(
        data => {
          this.loadingService.display(false);
          this.countries = data;
          // this.orderCurrencyCode = this.globalService.getCurrencyDisplay(this.countries, this.orderInfo.orderDetail[0].currency)
          this.orderCurrencyCode = this.globalService.getCurrencyDisplay(this.countries, this.orderInfo.orderDetail[0].currency)
            ? this.globalService.getCurrencyDisplay(this.countries, this.orderInfo.orderDetail[0].currency)
            : this.orderInfo.orderDetail[0].currency;
        },
        error => {
          this.loadingService.display(false);
          // console.log(`Cannot get country: ${error}`)
        }
      ) 
    }

  }

}
