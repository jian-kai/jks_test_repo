import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { ListViewsComponent } from '../../../../core/components/list-views/list-views.component';
import { RouterService } from '../../../../core/helpers/router.service';
import { GlobalService } from '../../../../core/services/global.service';

@Component({
  selector: 'app-user-order-list',
  templateUrl: './user-order-list.component.html',
  styleUrls: ['./user-order-list.component.scss']
})
export class UserOrderListComponent extends ListViewsComponent {
  totalItems: number = 0;
  options: URLSearchParams;
  public orderId: number;
  public activeOrderList: boolean = true;
  public orderList: any;
  public currentOrders: any = [];
  public historyOrders: any = [];
  public currentUser : any;
  public isLoading: boolean = true;
  public orderCurrencyCode: string = '';
  public countries: any;
  public BASE_VALUE = '00000000000';

  constructor(location: Location,
    routerService: RouterService,
    el: ElementRef,
    private _http: HttpService,
    public appConfig: AppConfig,
    public storage: StorageService,
    private loadingService: LoadingService,
    private router: Router,
    private globalService: GlobalService,
  ) {
    super(location, routerService, el);
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      // console.log('User is not exist');
      return;
    }
  }

  viewDetails(id: number, event: Event) {
    this.router.navigate([`/user/orders/${id}`]);
  }

  // get emit from order detail page
  getEmitActiveOrderList(status: boolean) {
    this.activeOrderList = status;
  }

  // get order list of current user
  getData() {
    this.loadingService.display(true);
    this.historyOrders = [];
    
    this.options.set('orderBy', 'createdAt_DESC');

    this._http._getList(`${this.appConfig.config.api.order.replace('[userID]', this.currentUser.id)}?${this.options.toString()}`).subscribe(
      result => {
        this.historyOrders = result.rows;
        if (this.historyOrders && this.historyOrders.length > 0) {
          this.historyOrders.forEach(item => {
            item.currency = item.orderDetail[0].currency;
            // if (this.countries && this.countries.length > 0) {
            //   this.getCountries().then(data => {
            //     this.countries = data;
            //     item.orderCurrencyCode = this.globalService.getCurrencyDisplay(this.countries, this.historyOrders[0].currency)
            // ? this.globalService.getCurrencyDisplay(this.countries, this.historyOrders[0].currency) 
            // : this.historyOrders[0].currency;
            //   })
            // } else {
            //   // item.currency = item.orderDetail[0].currency;
            //   item.orderCurrencyCode = this.globalService.getCurrencyDisplay(this.countries, this.historyOrders[0].currency)
            //     ? this.globalService.getCurrencyDisplay(this.countries, this.historyOrders[0].currency) 
            //     : this.historyOrders[0].currency;
            // }
          });
          this.totalItems = result.count;
        }
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        // console.log(error.error)
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
    // this._http._getList(this.appConfig.config.api.history_order.replace('[userID]', this.currentUser.id)).subscribe(
    //   data => {
    //     this.historyOrders = data;
    //     this.isloading = false;
    //   },
    //   error => {
    //     // console.log(error.error)
    //   }
    // )
  }
  
  goToProduct() {
    this.router.navigate(['/products']);
  }

  getCountries() {
    return new Promise((resolve, reject) => {
      this._http._getList(this.appConfig.config.api.countries).subscribe(
        data => {
          // this.loadingService.display(false);
          this.countries = data;
        },
        error => {
          // this.loadingService.display(false);
          // console.log(`Cannot get country: ${error}`)
        }
      ) 
    });

  }

}
