import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { HttpService } from '../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../core/services/storage.service';
import { LoadingService } from '../core/services/loading.service';
import { GlobalService } from '../core/services/global.service';
import { CountriesService } from '../core/services/countries.service'
import { RouterService } from '../core/helpers/router.service';
import { ModalService } from '../core/services/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PlanService } from '../core/services/plan.service';
import { CartService } from '../core/services/cart.service';
import { Item } from '../core/models/item.model';
import { GTMService } from '../core/services/gtm.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-ask',
  templateUrl: './ask.component.html',
  styleUrls: ['./ask.component.scss']
})
export class AskComponent implements OnInit {
  public productInfo: any;
  public slug: string;
  public productsSlider = [];
  public item: Item;
  public currentLang: any;
  isLoading: boolean = true;
  public id;
  public featuredProducts: any;

  //get ask products ---
  public relatedProducts: any;
  public featuredAskProducts: any;
  public productType: any;
  public productTypeTranslate: any;
  public productImages: any;
  public productDetails: any;
  public productTranslate: any;
  public productCountry: any;
  public productRelateds: any;
  public currentSelectedProductItem: any;
  //ask-cart
  public productRelatedList = [];

  public hasLoadedFeaturedProducts: boolean = false;
  public featuredAskProductss: any;
  public hasLoadedfeaturedAskProductss: boolean = false;
  public cartRules: any;
  awesomeKitPrice: any;
  public shippingFee: any;
  public articles: any;
  public apiURL: String;
  public selectedProductID: any;





  constructor(
    private httpService: HttpService,
    public storage: StorageService,
    public modalService: ModalService,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private router: Router,
    public cartService: CartService,
    private globalService: GlobalService,
    public appConfig: AppConfig,
    private gtmService: GTMService,
    public countriesService: CountriesService) {
    this.apiURL = appConfig.config.apiURL;

  }

  ngOnInit() {
    // get all products
    this.hasLoadedFeaturedProducts = false;
    this.reloadPage();
    this.countriesService.country.subscribe(country => {
      this.reloadPage();
    });
    let countryCode = this.router.url;
    if (countryCode) {
      if (countryCode === '/my') {
        countryCode = 'MYS';
      }
      if (countryCode === '/sg') {
        countryCode = 'SGP';
      }
      this.getCountries().then((res: any) => {
        let countries = res || [];
        let country = countries.find(value => value.code === countryCode);
        if (country) {
          this.countriesService.changeCountry(country);
        }
      })
    }
    
  }
  
  startAskJourney(){
    var element = document.getElementsByClassName("start-ask-journey");
    element[0].scrollIntoView({ block: 'start',  behavior: 'smooth' });
    element[0].setAttribute("style", "padding-top:50px;");
  }
  showCartridgeDesc() {
    var el1 = document.getElementById("CartridgeDesc");
    var el2 = document.getElementById("appendChevron_Cartridges");
    let chevronUp = '<img id="chevronUpActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/66a92620-c707-11e8-8e47-efb823843057.png">';
    let chevronDown = '<img id="chevronDownActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/6603e570-c707-11e8-8e47-efb823843057.png">';
    if ($(el1).is(":visible")) {
      $(el1).hide(500, function() {
      });
      $(el2).html(chevronDown);
    } else {
      $(el1).show(500, function() {
      });
      $(el2).html(chevronUp);
    }
  }
  showBladeDesc() {
    var el1 = document.getElementById("BladeDesc");
    var el2 = document.getElementById("appendChevron_BladeDesc");
    var el3 = document.getElementById("BladeImage");
    // var el3_image1 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/0142ba20-c7ad-11e8-aa0a-33cf643095e9.png">';
    // var el3_image2 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/00bdf9c0-c7ad-11e8-aa0a-33cf643095e9.png">';
    let chevronUp = '<img id="chevronUpActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/66a92620-c707-11e8-8e47-efb823843057.png">';
    let chevronDown = '<img id="chevronDownActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/6603e570-c707-11e8-8e47-efb823843057.png">';
    if ($(el1).is(":visible")) {
      $(el1).hide(500, function() {
      });
      $("#Swivel1").show();
      $("#Swivel2").hide();
      $(el2).html(chevronDown);
    } else {
      $(el1).show(500, function() {
      });
      $("#Swivel1").hide();
      $("#Swivel2").show();
      $(el2).html(chevronUp);
    }
  }
  showShaveCreamDesc() {
    var el1 = document.getElementById("ShaveCreamDesc");
    var el2 = document.getElementById("appendChevron_ShaveCreamDesc");
    var el3 = document.getElementById("ShaveCreamImage");
    // var el3_image1 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/8e8e0bb0-cbd0-11e8-924a-d302a0276fa8.png">';
    // var el3_image2 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/7d267150-c707-11e8-8e47-efb823843057.png">';
    let chevronUp = '<img id="chevronUpActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/66a92620-c707-11e8-8e47-efb823843057.png">';
    let chevronDown = '<img id="chevronDownActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/6603e570-c707-11e8-8e47-efb823843057.png">';
    if ($(el1).is(":visible")) {
      $(el1).hide(500, function() {
      });
      $("#ShaveCream1").show();
      $("#ShaveCream2").hide();
      $(el2).html(chevronDown);
    } else {
      $(el1).show(500, function() {
      });
      $("#ShaveCream1").hide();
      $("#ShaveCream2").show();
      $(el2).html(chevronUp);
    }
  }
  showAfterShaveDesc() {
    var el1 = document.getElementById("AfterShaveDesc");
    var el2 = document.getElementById("appendChevron_AfterShaveDesc");
    var el3 = document.getElementById("AfterShaveImage");
    // var el3_image1 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/8f10a930-cbd0-11e8-924a-d302a0276fa8.png">';
    // var el3_image2 = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/7f23ef00-c707-11e8-8e47-efb823843057.png">';
    let chevronUp = '<img id="chevronUpActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/66a92620-c707-11e8-8e47-efb823843057.png">';
    let chevronDown = '<img id="chevronDownActive" style="width:20px;float:right;cursor:pointer;" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/6603e570-c707-11e8-8e47-efb823843057.png">';
    if ($(el1).is(":visible")) {
      $(el1).hide(500, function() {
      });
      $("#AfterShave1").show();
      $("#AfterShave2").hide();
      $(el2).html(chevronDown);
    } else {
      $(el1).show(500, function() {
      });
      $("#AfterShave1").hide();
      $("#AfterShave2").show();
      $(el2).html(chevronUp);
    }
  }
  selectionASK(selector) {

    let item_1: any;
    let item_2: any;
    let item_3: any;

    switch (selector) {
      case 0:
        item_1 = selector;
        item_2 = 1;
        item_3 = 2;
        break;
      case 1:
        item_1 = 0;
        item_2 = selector;
        item_3 = 2;
        break;
      case 2:
        item_1 = 0;
        item_2 = 1;
        item_3 = selector;
        break;
    }
    var selectorID = $("#askcategory_" + item_1);
    var nonSelected_2 = $("#askcategory_" + item_2);
    var nonSelected_3 = $("#askcategory_" + item_3);
    if (selector === 0) {
      selectorID.addClass('add-border-ask-selection');
      nonSelected_2.removeClass('add-border-ask-selection');
      nonSelected_3.removeClass('add-border-ask-selection');
      $("#3BladeImage").show();
      $("#5BladeImage").hide();
      $("#6BladeImage").hide();
      let imgasdsad = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/b9b59e70-c7ac-11e8-aa0a-33cf643095e9.png">';
      var number_for_blade = '3';
      var text = 'Blade Cartridges (5 Pieces)';
      // $('#appendBladeImage').html(imgasdsad);
      $('#appendBladeType').html(number_for_blade+" "+text);
      $("#appendnumberforblade").html(number_for_blade);
      selectorID.removeClass('add-padding');
      nonSelected_2.addClass('add-padding');
      nonSelected_3.addClass('add-padding');

    } else if (selector === 1) {
      selectorID.removeClass('add-border-ask-selection');
      nonSelected_2.addClass('add-border-ask-selection');
      nonSelected_3.removeClass('add-border-ask-selection');
      selectorID.addClass('add-padding');
      nonSelected_2.removeClass('add-padding');
      nonSelected_3.addClass('add-padding');
      $("#3BladeImage").hide();
      $("#5BladeImage").show();
      $("#6BladeImage").hide();
      let imgasdsad = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/ba09d8f0-c7ac-11e8-aa0a-33cf643095e9.png">';
      // $('#appendBladeImage').html(imgasdsad);
      var number_for_blade = '5';
      var text = 'Blade Cartridges (5 Pieces)';
      $('#appendBladeType').html(number_for_blade+" "+text);
      $("#appendnumberforblade").html(number_for_blade);

    } else if (selector === 2) {
      selectorID.removeClass('add-border-ask-selection');
      nonSelected_2.removeClass('add-border-ask-selection');
      nonSelected_3.addClass('add-border-ask-selection');
      selectorID.addClass('add-padding');
      nonSelected_2.addClass('add-padding');
      nonSelected_3.removeClass('add-padding');
      $("#3BladeImage").hide();
      $("#5BladeImage").hide();
      $("#6BladeImage").show();
      let imgasdsad = '<img _ngcontent-c11="" src="https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/ba584710-c7ac-11e8-aa0a-33cf643095e9.png">';
      // $('#appendBladeImage').html(imgasdsad);
      var number_for_blade = '6';
      var text = 'Blade Cartridges (5 Pieces)';
      $('#appendBladeType').html(number_for_blade+" "+text);
      $("#appendnumberforblade").html(number_for_blade);

    }
  }

  getCountries() {
    return new Promise((resolved, reject) => {
      this.httpService._getList(this.appConfig.config.api.countries)
        .subscribe(
          res => {
            resolved(res);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  reloadPage() {
    this.triggerDefaultProduct();
    this.getFeaturedProducts();
    this.getRelatedProduct();
    this.shippingFee = `${this.storage.getCountry().currencyDisplay}${this.appConfig.getConfig('shippingFeeConfig').trialFee}`;
  }

  triggerDefaultProduct(){
    setTimeout(function(){
      console.log("Trigger Default Product Click");
      $("#askcategory_2").trigger('click');
    },4800);

  }

UpdatePrice(price, id, productId, index) {
    var width = screen.width;
    let productPrice = price;
    let product_country_id = id;
    // this.getRelatedProductList(product_country_id, productId);
    this.getSelectedProductDetails(product_country_id, productId);
    let priceCurrency = $("#askcategory_price_" + index).text();
    let add_to_cart:any;
    if (productPrice) {
      let price = "Buy Now | " + priceCurrency;
      if (width <= 500) {
        add_to_cart =
          '<a style="width:100%;" _ngcontent-c11 class="btn btn-primary float-right" href="/checkout-ask">' +
          price +
          "</a>";
      } else {
        add_to_cart =
          '<a _ngcontent-c11 class="btn btn-primary float-right" href="/checkout-ask">' +
          price +
          "</a>";
      }

      $("#buyNow").html(add_to_cart);
    }
  }

  getFeaturedProducts() {
    let options = {
      isFeaturedAsk: true,
      limit: 3,
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        this.featuredProducts = data;
        console.log(this.featuredProducts);
        this.hasLoadedFeaturedProducts = true;
      },
      error => {
        this.hasLoadedFeaturedProducts = true;
        // console.log(error.error)
      }
    )
  }

  getRelatedProduct() {
    console.log("info", "getRelatedProduct start");
    let options = {
      isFeaturedAsk: true,
      limit: 3,
      userTypeId: 1
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products_ask}?` + params.toString()).subscribe(
      data => {
        this.featuredAskProducts = data;
        console.log(this.featuredAskProducts);
        this.hasLoadedFeaturedProducts = true;
      },
      error => {
        this.hasLoadedFeaturedProducts = true;
        // console.log(error.error)
      }
    )
  }
  //Get selected item details
  getSelectedProductDetails(productCountryId, productId) {
    console.log("info", "getSelectedProductDetails start");
    //get Product Index
    var productClicked = this.featuredAskProducts;
    var index = productClicked.findIndex(function(item, i) {
      return item.id === productId
    });
    this.currentSelectedProductItem = this.featuredAskProducts[index];
    this.storage.clearASKItem();
    this.storage.setASKItem(this.currentSelectedProductItem);
  }
  //Add all related products into array
  getRelatedProductList(productCountryId, productId) {
    // this.productRelateds.id.forEach(related => {
    // })
  }
  //Each related product in array -> send to API, get details
  getEachRelatedProductDetail() {
  }
}
