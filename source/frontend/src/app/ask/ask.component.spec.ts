import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionASKComponent } from './ask.component';

describe('SubscriptionASKComponent', () => {
  let component: SubscriptionASKComponent;
  let fixture: ComponentFixture<SubscriptionASKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionASKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionASKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
