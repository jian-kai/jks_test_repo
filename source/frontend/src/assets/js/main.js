$(document).ready(function () {
	$("body").on("click", function (event) {
		if (!$(event.target).is(".ico-shopping-cart")) {
			if ($(".shopping-cart").hasClass("show")) {
				$(".shopping-cart").removeClass("show");
				$(".shopping-cart").fadeToggle( "fast");
			}
		}
	});
	// $('#myModal').modal({ show: false})
});