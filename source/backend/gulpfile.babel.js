'use strict';

import gulp from 'gulp';
import run from 'run-sequence';
import del from 'del';
import rimraf from 'rimraf';
import server from 'gulp-live-server';
import babel from 'gulp-babel';

const paths = {
  src: './src',
  dest: './dist'
};

//Catch the server instance
let express;

gulp.task('default', cb => {
  run('server', 'build', 'watch', cb);
});

gulp.task('build', cb => {
  run('clean', 'babel', 'copy-views', 'copy-public', 'copy-upload', 'copy-json', 'copy-csv', 'copy-key', 'restart', cb);
});

//build when a file has changed
gulp.task('watch', () => {
  gulp.watch([
    `${paths.src}/**/*.js`,
    `${paths.src}/views/*.hbs`
  ], ['build']);
});

gulp.task('server', () => {
  express = server.new(paths.dest);
});

gulp.task('restart', () => {
  if(express) {
    express.start.bind(express)();
  }
});

//Clean the app destination, to prepare for new files
gulp.task('clean', cb => {
  rimraf(paths.dest, cb);
});

// template views copy
gulp.task('copy-views', () => {
  del(`${paths.dest}/views/**/*.hbs`).then(() => {
    gulp.src(`${paths.src}/views/**/*.hbs`)
      .pipe(gulp.dest(`${paths.dest}/views`));
  });
});

// copy JSON config
gulp.task('copy-json', () => {
  del(`${paths.dest}/**/*.json`).then(() => {
    gulp.src(`${paths.src}/**/*.json`)
      .pipe(gulp.dest(`${paths.dest}/`));
  });
});

// copy CSV file
gulp.task('copy-csv', () => {
  del(`${paths.dest}/**/*.csv`).then(() => {
    gulp.src(`${paths.src}/**/*.csv`)
      .pipe(gulp.dest(`${paths.dest}/`));
  });
});

// copy key file
gulp.task('copy-key', () => {
  del(`${paths.dest}/**/*.pem`).then(() => {
    gulp.src(`${paths.src}/**/*.pem`)
      .pipe(gulp.dest(`${paths.dest}/`));
  });
});

// template copy public files
gulp.task('copy-public', () => {
  del(`${paths.dest}/public/**`).then(() => {
    gulp.src(`${paths.src}/public/**/*`)
      .pipe(gulp.dest(`${paths.dest}/public`));
  });
});

// template copy upload files
gulp.task('copy-upload', () => {
  del(`${paths.dest}/uploads/**`).then(() => {
    gulp.src(`${paths.src}/uploads/**/*`)
      .pipe(gulp.dest(`${paths.dest}/uploads`));
  });
});

//Transform back-end ES6 to ES5
//only transform features not supported by node v5
gulp.task('babel', () => {
  let src = paths.src;
  return gulp.src(`${src}/**/*.js`)
    .pipe(babel({
      presets: ['es2015', 'stage-0']
    }))
    .pipe(gulp.dest(paths.dest));
});