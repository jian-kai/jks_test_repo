# import fabric third-party
from fabric.api import local, run, env
from fabric.colors import green, red
from fabric.contrib.files import exists

# local env config
PATH_INSTALL_FILE = './bash/install_environment.sh'
KEY_FILE = './bjtech.key'
schemaList = [
    'blockchain.sql',
    'environment.sql',
    'histories.sql',
    'identity.sql',
    'server.sql',
    'tracker.sql',
    'registry.sql'
]

SERVERS = dict(
    devel = dict(
        host = '128.199.201.237',
        user = 'root',
        branch = 'devel',
        src = '../',
        dest = '/home/shaves2u',
        excludeFile = '\'../rsync_exclude.txt\'',
        adminSrc = '../../admin/',
        baSrc = '../../BA-website/',
        adminDest = '/home/shaves2u-admin',
        baDest = '/home/shaves2u-ba-website',
        keyFile = './id_rsa'
    ),
    staging = dict(
        # host = '128.199.138.112', old staging
        host = '104.215.192.11',
        user = 'shaves2u',
        branch = 'master',
        src = '../',
        dest = '/home/shaves2u/backend',
        excludeFile = '\'../rsync_exclude.txt\'',
        adminSrc = '../../admin/',
        baSrc = '../../BA-website/',
        adminDest = '/home/shaves2u/admin',
        baDest = '/home/shaves2u/ba-website',
        keyFile = './shaves2u.key'
    ),
    production = dict(
        host = '52.187.77.63',
        user = 'shaves2u',
        branch = 'master',
        src = '../',
        excludeFile = '\'../rsync_exclude.txt\'',
        adminSrc = '../../admin/',
        baSrc = '../../BA-website/',
        dest = '/home/shaves2u/backend',
        adminDest = '/home/shaves2u/admin',
        baDest = '/home/shaves2u/ba-website',
        keyFile = './shaves2u.key'
    ),
    production2 = dict(
        host = '137.116.149.254',
        user = 'shaves2u',
        branch = 'master',
        src = '../',
        excludeFile = '\'../rsync_exclude.txt\'',
        adminSrc = '../../admin/',
        baSrc = '../../BA-website/',
        dest = '/home/shaves2u/backend',
        adminDest = '/home/shaves2u/admin',
        baDest = '/home/shaves2u/ba-website',
        keyFile = './shaves2u.key'
    ),
    admin_production = dict(
        host = '52.230.31.233',
        user = 'shaves2u',
        branch = 'master',
        src = '../',
        excludeFile = '\'../rsync_exclude.txt\'',
        adminSrc = '../../admin/',
        baSrc = '../../BA-website/',
        dest = '/home/shaves2u/backend',
        adminDest = '/home/shaves2u/admin',
        baDest = '/home/shaves2u/ba-website',
        keyFile = './shaves2u.key'
    ),
)


def prepare_env(server_name='devel'):
    env.host_string = SERVERS[server_name]['host']
    env.user = SERVERS[server_name]['user']
    if ('admin_production' not in server_name):
        env.key_filename = SERVERS[server_name]['keyFile']
    env['name'] = server_name


def connect_server(server_name='devel'):
    """
    Script to connect to server
    """
    prepare_env(server_name)

    if exists('/etc'):
        print(green('Connected to %s server.....' % server_name))
        return True
    else:
        print(red('Can\'t connect to %s server. Please double check your list of IPs in hosts' % server_name))
        return False

def install_server(server_name='devel'):
    """
    Script to install environment
    """

    # read install environment file
    install_file = open(PATH_INSTALL_FILE, 'r')

    if install_file:
        if connect_server(server_name):

            # iterator all line of install file
            for line in install_file:
                if line[0] == '#':
                    run('echo "%s"' % line.strip())
                else:
                    run(line.strip())
    else:
        print(red('the install file is not existed!'))

    install_file.close()


def deploy(server_name='devel'):
    """
    Script to deploy special server
    """
    print(green('Start deploy %s.....' % server_name))

    # stach current changes and pull new code from config branch
    # if(server_name == 'production'):
    #     pull(SERVERS[server_name].branch)

    # copy environment file
    local('cp ./frontend-env/%s.env.ts ../../frontend/src/app/config/env.ts' % server_name)

    # run build frontend
    local('cd ../../frontend/ && ng build --prod --aot')

    # copy build files to public file
    local('rm -rf ../src/public && cp -a ../../frontend/dist/. ../src/public')

    # copy robots file
    local('cp ../../backend/deployment/sitemap/%s.robots.txt ../src/public/robots.txt' % server_name)

    # copy sitemap file
    local('cp ../../backend/deployment/sitemap/%s.sitemap.xml ../src/public/sitemap.xml' % server_name)

    local('cp ./frontend-env/local.env.ts ../../frontend/src/app/config/env.ts')

    if ('production' not in server_name):
        sync_code_to_server(server_name)
        restart_server(server_name)
    else:
        sync_code_to_server('production')
        sync_code_to_server('production2')
        restart_server('production')
        restart_server('production2')

def deploy_admin(server_name='devel'):
    """
    Script to deploy admin panel
    """
    print(green('Start deploy %s.....' % server_name))

    # run build frontend
    local('cd ../../admin/ && ng build --prod --aot')

    local('echo ')
    if(server_name == 'devel') or (server_name == 'staging'):
       local('rsync -avHPe ssh %s -e "ssh -i %s" --rsync-path="sudo rsync" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['adminSrc'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['adminDest'], SERVERS[server_name]['excludeFile']))
    else:
       local('rsync -avHPe ssh %s -e "ssh -i %s" --rsync-path="sudo rsync" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['adminSrc'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['adminDest'], SERVERS[server_name]['excludeFile']))

    #if(server_name == 'devel') or (server_name == 'staging'):
     #   local('rsync -avHPe ssh %s -e "ssh -i %s" --rsync-path="sudo rsync" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['baSrc'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['baDest'], SERVERS[server_name]['excludeFile']))
    #else:
    #    local('rsync -avHPe ssh %s -e "ssh" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['baSrc'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['baDest'], SERVERS[server_name]['excludeFile']))
def deploy_ba_website(server_name='devel'):
    """
    Script to deploy admin panel
    """
    print(green('Start deploy %s.....' % server_name))

    # run build frontend
    local('cd %s && ng build --prod --aot=false' % SERVERS[server_name]['baSrc'])

    local('echo ')
    #if(server_name == 'devel') or (server_name == 'staging'):
    #    local('rsync -avHPe ssh %s -e "ssh -i %s" --rsync-path="sudo rsync" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['baSrc'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['baDest'], SERVERS[server_name]['excludeFile']))
    #else:
    #    local('rsync -avHPe ssh %s -e "ssh" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['baSrc'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['baDest'], SERVERS[server_name]['excludeFile']))
    local('rsync -avHPe ssh %s -e "ssh -i %s" --rsync-path="sudo rsync" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['baSrc'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['baDest'], SERVERS[server_name]['excludeFile']))

def pull(branch='devel'):
    """
    Script to pull code from special branch on github to local
    """
    print(green('Start pushing code.....'))

    local('git stash')
    local('git checkout %s' % branch)
    local('git pull origin %s' % branch)
    print(green('All code has been pulled to githup.....'))


def sync_code_to_server(server_name='devel'):
    """
    Script to sync built code from local to server
    """
    if connect_server(server_name):
        if not exists(SERVERS[server_name]['dest']):
            run('mkdir -p %s' % SERVERS[server_name]['dest'])

    print(green('Start sync code to %s server.....' % server_name))
    local('echo Build app...')
    local('gulp build')
    if(server_name == 'devel') or (server_name == 'staging'):
        local('rsync -avHPe ssh %s -e "ssh -i %s" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['src'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['dest'], SERVERS[server_name]['excludeFile']))
    else:
        local('rsync -avHPe ssh %s -e "ssh -i %s" %s@%s:%s --exclude-from %s' % (SERVERS[server_name]['src'], SERVERS[server_name]['keyFile'], SERVERS[server_name]['user'], SERVERS[server_name]['host'], SERVERS[server_name]['dest'], SERVERS[server_name]['excludeFile']))


def restart_server(server_name='devel'):
    """
    Script to restart special server
    """
    print(green('Restart shaves2u on %s server.....' % server_name))
    if connect_server(server_name):
        run('sudo service shaves2u restart')
