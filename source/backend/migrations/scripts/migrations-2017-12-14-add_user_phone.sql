ALTER TABLE `shave2u`.`Users` 
ADD COLUMN `isBaGuest` TINYINT NULL DEFAULT '0' AFTER `NotificationTypeId`,
ADD COLUMN `phone` VARCHAR(30) NULL AFTER `gender`;

UPDATE `shave2u`.`Users` 
INNER JOIN `shave2u`.`DeliveryAddresses`
	ON (`shave2u`.`Users`.`defaultShipping` = `shave2u`.`DeliveryAddresses`.`id`)
SET `shave2u`.`Users`.`phone` = `shave2u`.`DeliveryAddresses`.`contactNumber`
WHERE `shave2u`.`Users`.`isBaGuest` = true;

