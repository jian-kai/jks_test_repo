ALTER TABLE `shave2u`.`Orders` 
ADD COLUMN `CountryId` INT(11) NULL DEFAULT NULL AFTER `carrierAgent`,
ADD INDEX `orders_ibfk_6_idx` (`CountryId` ASC);
ALTER TABLE `shave2u`.`Orders` 
ADD CONSTRAINT `orders_ibfk_6`
  FOREIGN KEY (`CountryId`)
  REFERENCES `shave2u`.`Countries` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

SELECT  * FROM `shave2u`.`Orders` WHERE `states` = 'Delivering' OR `states` = 'Canceled';
UPDATE `shave2u`.`Orders` SET `states` = 'On Hold' WHERE `states` = 'Deliver';
UPDATE `shave2u`.`Orders` SET `CountryId` = 1 WHERE `CountryId` IS NULL;

ALTER TABLE `shave2u`.`Orders` 
CHANGE COLUMN `states` `states` ENUM('On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled') NULL DEFAULT 'On Hold' ;

UPDATE `shave2u`.`Orders` SET `states` = 'Delivering' WHERE `states` = 'On Hold';

