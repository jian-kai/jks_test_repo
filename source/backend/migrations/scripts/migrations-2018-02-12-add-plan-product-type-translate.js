import initializeDb from '../../src/db';
const DATA_PATH = '../init-data/';
import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';


const INIT_TABLES = [
  'ProductTypeTranslate',
  'PlanTypeTranslate'
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let planTypes = [];
      let productTypes = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.ProductTypeTranslate.drop(options))
        .then(() => db.PlanTypeTranslate.drop(options))
        .then(() => db.ProductTypeTranslate.sync(options))
        .then(() => db.PlanTypeTranslate.sync(options))
        .then(() => db.PlanType.findAll())
        .then(_planTypes => {
          planTypes = SequelizeHelper.convertSeque2Json(_planTypes);
        })
        .then(() => db.ProductType.findAll())
        .then(_productTypes => {
          productTypes = SequelizeHelper.convertSeque2Json(_productTypes);
        })
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            if(table === 'PlanTypeTranslate') {
              for(let i = 0; i < planTypes.length; i++) {
                for(let j = 0; j < data.length; j++) {
                  if((i + 1) === data[j].PlanTypeId) {
                    data[j].PlanTypeId = planTypes[i].id;
                  }
                }
              }
            } else {
              for(let i = 0; i < productTypes.length; i++) {
                for(let j = 0; j < data.length; j++) {
                  if((i + 1) === data[j].ProductTypeId) {
                    data[j].ProductTypeId = productTypes[i].id;
                  }
                }
              }
            }
            return db[table].bulkCreate(data);
          }
        ))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
