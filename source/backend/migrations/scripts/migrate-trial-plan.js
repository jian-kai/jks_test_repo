import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanTypes` CHANGE COLUMN `totalDeliverTimes` `totalDeliverTimes` INT(11) NULL, CHANGE COLUMN `totalChargeTimes` `totalChargeTimes` INT(11) NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` CHANGE COLUMN `totalDeliverTimes` `totalDeliverTimes` INT(11) NULL, CHANGE COLUMN `totalChargeTimes` `totalChargeTimes` INT(11) NULL', null, options))
        .then(() => db.PlanTrialProduct.sync(options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
