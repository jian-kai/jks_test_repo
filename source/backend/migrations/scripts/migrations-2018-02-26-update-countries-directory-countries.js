import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Countries\' AND COLUMN_NAME = \'order\'', null, options))
        .then(order => {
          if(!order[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `order` INTEGER AFTER `shippingTrialFee`', null, options);
          }
        })
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `shave2u`.`Countries`.`order` = `shave2u`.`Countries`.`id`', null, options))
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'DirectoryCountries\' AND COLUMN_NAME = \'CountryId\'', null, options))
        .then(CountryId => {
          if(!CountryId[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`DirectoryCountries` ADD COLUMN `CountryId` INTEGER AFTER `callingCode`', null, options);
          }
        })
        .then(() => db.sequelize.query('UPDATE `shave2u`.`DirectoryCountries` INNER JOIN `shave2u`.`Countries` ON `shave2u`.`DirectoryCountries`.`code` = `shave2u`.`Countries`.`code` SET `shave2u`.`DirectoryCountries`.`CountryId` = `shave2u`.`Countries`.`id`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
