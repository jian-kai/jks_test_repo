import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `campaign` VARCHAR(50) NULL DEFAULT \'none\' AFTER `medium`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `term` VARCHAR(50) NULL DEFAULT \'none\' AFTER `campaign`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `content` VARCHAR(50) NULL DEFAULT \'none\' AFTER `term`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
