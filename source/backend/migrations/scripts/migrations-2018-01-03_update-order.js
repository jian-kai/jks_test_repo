import Promise from 'bluebird';
import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `source` VARCHAR(50) NULL DEFAULT \'direct\'', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `medium` VARCHAR(50) NULL DEFAULT \'none\'', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `campaign` VARCHAR(50) NULL DEFAULT \'none\'', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `term` VARCHAR(50) NULL DEFAULT \'none\'', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `content` VARCHAR(50) NULL DEFAULT \'none\'', null, options))
        .then(() => db.SaleReport.findAll())
        .then(saleReports => Promise.mapSeries(saleReports, saleReport => db.Order.update({
          source: saleReport.source,
          medium: saleReport.medium,
          campaign: saleReport.campaign,
          term: saleReport.term,
          content: saleReport.content
        }, {where: {id: saleReport.orderId}, transaction: t})))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
