import initializeDb from '../../src/db';
import Promise from 'bluebird';


const DATA_PATH = '../init-data/';
const INIT_TABLES = [
  'SupportedLanguage'
];

let updateDirectoryCountries = [
  {
    languageCode: 'ab',
    languageName: 'аҧсуа'
  },
  {
    languageCode: 'aa',
    languageName: 'Afaraf'
  },
  {
    languageCode: 'af',
    languageName: 'Afrikaans'
  },
  {
    languageCode: 'ak',
    languageName: 'Akan'
  },
  {
    languageCode: 'sq',
    languageName: 'Shqip'
  },
  {
    languageCode: 'am',
    languageName: 'አማርኛ'
  },
  {
    languageCode: 'ar',
    languageName: 'العربية'
  },
  {
    languageCode: 'an',
    languageName: 'Aragonés'
  },
  {
    languageCode: 'hy',
    languageName: 'Հայերեն'
  },
  {
    languageCode: 'as',
    languageName: 'অসমীয়া'
  },
  {
    languageCode: 'av',
    languageName: 'авар мацӀ'
  },
  {
    languageCode: 'ae',
    languageName: 'avesta'
  },
  {
    languageCode: 'ay',
    languageName: 'aymar aru'
  },
  {
    languageCode: 'az',
    languageName: 'azərbaycan dili'
  },
  {
    languageCode: 'bm',
    languageName: 'bamanankan'
  },
  {
    languageCode: 'ba',
    languageName: 'башҡорт теле'
  },
  {
    languageCode: 'eu',
    languageName: 'euskara'
  },
  {
    languageCode: 'be',
    languageName: 'Беларуская'
  },
  {
    languageCode: 'bn',
    languageName: 'বাংলা'
  },
  {
    languageCode: 'bh',
    languageName: 'भोजपुरी'
  },
  {
    languageCode: 'bi',
    languageName: 'Bislama'
  },
  {
    languageCode: 'bs',
    languageName: 'bosanski jezik'
  },
  {
    languageCode: 'br',
    languageName: 'brezhoneg'
  },
  {
    languageCode: 'bg',
    languageName: 'български език'
  },
  {
    languageCode: 'my',
    languageName: 'ဗမာစာ'
  },
  {
    languageCode: 'ca',
    languageName: 'Català'
  },
  {
    languageCode: 'ch',
    languageName: 'Chamoru'
  },
  {
    languageCode: 'ce',
    languageName: 'нохчийн мотт'
  },
  {
    languageCode: 'ny',
    languageName: 'chiCheŵa'
  },
  {
    languageCode: 'zh',
    languageName: '中文'
  },
  {
    languageCode: 'cv',
    languageName: 'чӑваш чӗлхи'
  },
  {
    languageCode: 'kw',
    languageName: 'Kernewek'
  },
  {
    languageCode: 'co',
    languageName: 'corsu'
  },
  {
    languageCode: 'cr',
    languageName: 'ᓀᐦᐃᔭᐍᐏᐣ'
  },
  {
    languageCode: 'hr',
    languageName: 'hrvatski'
  },
  {
    languageCode: 'cs',
    languageName: 'česky'
  },
  {
    languageCode: 'da',
    languageName: 'dansk'
  },
  {
    languageCode: 'dv',
    languageName: 'ދިވެހި'
  },
  {
    languageCode: 'nl',
    languageName: 'Nederlands'
  },
  {
    languageCode: 'en',
    languageName: 'English'
  },
  {
    languageCode: 'eo',
    languageName: 'Esperanto'
  },
  {
    languageCode: 'et',
    languageName: 'eesti'
  },
  {
    languageCode: 'ee',
    languageName: 'Eʋegbe'
  },
  {
    languageCode: 'fo',
    languageName: 'føroyskt'
  },
  {
    languageCode: 'fj',
    languageName: 'vosa Vakaviti'
  },
  {
    languageCode: 'fi',
    languageName: 'suomi'
  },
  {
    languageCode: 'fr',
    languageName: 'français'
  },
  {
    languageCode: 'ff',
    languageName: 'Fulfulde'
  },
  {
    languageCode: 'gl',
    languageName: 'Galego'
  },
  {
    languageCode: 'ka',
    languageName: 'ქართული'
  },
  {
    languageCode: 'de',
    languageName: 'Deutsch'
  },
  {
    languageCode: 'el',
    languageName: 'Ελληνικά'
  },
  {
    languageCode: 'gn',
    languageName: 'Avañeẽ'
  },
  {
    languageCode: 'gu',
    languageName: 'ગુજરાતી'
  },
  {
    languageCode: 'ht',
    languageName: 'Kreyòl ayisyen'
  },
  {
    languageCode: 'ha',
    languageName: 'Hausa'
  },
  {
    languageCode: 'he',
    languageName: 'עברית'
  },
  {
    languageCode: 'hz',
    languageName: 'Otjiherero'
  },
  {
    languageCode: 'hi',
    languageName: 'हिन्दी'
  },
  {
    languageCode: 'ho',
    languageName: 'Hiri Motu'
  },
  {
    languageCode: 'hu',
    languageName: 'Magyar'
  },
  {
    languageCode: 'ia',
    languageName: 'Interlingua'
  },
  {
    languageCode: 'id',
    languageName: 'Bahasa Indonesia'
  },
  {
    languageCode: 'ie',
    languageName: 'Originally called Occidental; then Interlingue after WWII'
  },
  {
    languageCode: 'ga',
    languageName: 'Gaeilge'
  },
  {
    languageCode: 'ig',
    languageName: 'Asụsụ Igbo'
  },
  {
    languageCode: 'ik',
    languageName: 'Iñupiaq'
  },
  {
    languageCode: 'io',
    languageName: 'Ido'
  },
  {
    languageCode: 'is',
    languageName: 'Íslenska'
  },
  {
    languageCode: 'it',
    languageName: 'Italiano'
  },
  {
    languageCode: 'iu',
    languageName: 'ᐃᓄᒃᑎᑐᑦ'
  },
  {
    languageCode: 'ja',
    languageName: '日本語'
  },
  {
    languageCode: 'jv',
    languageName: 'basa Jawa'
  },
  {
    languageCode: 'kl',
    languageName: 'kalaallisut'
  },
  {
    languageCode: 'kn',
    languageName: 'ಕನ್ನಡ'
  },
  {
    languageCode: 'kr',
    languageName: 'Kanuri'
  },
  {
    languageCode: 'ks',
    languageName: 'कश्मीरी'
  },
  {
    languageCode: 'kk',
    languageName: 'Қазақ тілі'
  },
  {
    languageCode: 'km',
    languageName: 'ភាសាខ្មែរ'
  },
  {
    languageCode: 'ki',
    languageName: 'Gĩkũyũ'
  },
  {
    languageCode: 'rw',
    languageName: 'Ikinyarwanda'
  },
  {
    languageCode: 'ky',
    languageName: 'кыргыз тили'
  },
  {
    languageCode: 'kv',
    languageName: 'коми кыв'
  },
  {
    languageCode: 'kg',
    languageName: 'KiKongo'
  },
  {
    languageCode: 'ko',
    languageName: '한국어'
  },
  {
    languageCode: 'ku',
    languageName: 'Kurdî'
  },
  {
    languageCode: 'kj',
    languageName: 'Kuanyama'
  },
  {
    languageCode: 'la',
    languageName: 'latine'
  },
  {
    languageCode: 'lb',
    languageName: 'Lëtzebuergesch'
  },
  {
    languageCode: 'lg',
    languageName: 'Luganda'
  },
  {
    languageCode: 'li',
    languageName: 'Limburgs'
  },
  {
    languageCode: 'ln',
    languageName: 'Lingála'
  },
  {
    languageCode: 'lo',
    languageName: 'ພາສາລາວ'
  },
  {
    languageCode: 'lt',
    languageName: 'lietuvių kalba'
  },
  {
    languageCode: 'lu',
    languageName: ''
  },
  {
    languageCode: 'lv',
    languageName: 'latviešu valoda'
  },
  {
    languageCode: 'gv',
    languageName: 'Gaelg'
  },
  {
    languageCode: 'mk',
    languageName: 'македонски јазик'
  },
  {
    languageCode: 'mg',
    languageName: 'Malagasy fiteny'
  },
  {
    languageCode: 'ms',
    languageName: 'bahasa Melayu'
  },
  {
    languageCode: 'ml',
    languageName: 'മലയാളം'
  },
  {
    languageCode: 'mt',
    languageName: 'Malti'
  },
  {
    languageCode: 'mi',
    languageName: 'te reo Māori'
  },
  {
    languageCode: 'mr',
    languageName: 'मराठी'
  },
  {
    languageCode: 'mh',
    languageName: 'Kajin M̧ajeļ'
  },
  {
    languageCode: 'mn',
    languageName: 'монгол'
  },
  {
    languageCode: 'na',
    languageName: 'Ekakairũ Naoero'
  },
  {
    languageCode: 'nv',
    languageName: 'Diné bizaad'
  },
  {
    languageCode: 'nb',
    languageName: 'Norsk bokmål'
  },
  {
    languageCode: 'nd',
    languageName: 'isiNdebele'
  },
  {
    languageCode: 'ne',
    languageName: 'नेपाली'
  },
  {
    languageCode: 'ng',
    languageName: 'Owambo'
  },
  {
    languageCode: 'nn',
    languageName: 'Norsk nynorsk'
  },
  {
    languageCode: 'no',
    languageName: 'Norsk'
  },
  {
    languageCode: 'ii',
    languageName: 'ꆈꌠ꒿ Nuosuhxop'
  },
  {
    languageCode: 'nr',
    languageName: 'isiNdebele'
  },
  {
    languageCode: 'oc',
    languageName: 'Occitan'
  },
  {
    languageCode: 'oj',
    languageName: 'ᐊᓂᔑᓈᐯᒧᐎᓐ'
  },
  {
    languageCode: 'cu',
    languageName: 'ѩзыкъ словѣньскъ'
  },
  {
    languageCode: 'om',
    languageName: 'Afaan Oromoo'
  },
  {
    languageCode: 'or',
    languageName: 'ଓଡ଼ିଆ'
  },
  {
    languageCode: 'os',
    languageName: 'ирон æвзаг'
  },
  {
    languageCode: 'pa',
    languageName: 'ਪੰਜਾਬੀ'
  },
  {
    languageCode: 'pi',
    languageName: 'पाऴि'
  },
  {
    languageCode: 'fa',
    languageName: 'فارسی'
  },
  {
    languageCode: 'pl',
    languageName: 'polski'
  },
  {
    languageCode: 'ps',
    languageName: 'پښتو'
  },
  {
    languageCode: 'pt',
    languageName: 'Português'
  },
  {
    languageCode: 'qu',
    languageName: 'Runa Simi'
  },
  {
    languageCode: 'rm',
    languageName: 'rumantsch grischun'
  },
  {
    languageCode: 'rn',
    languageName: 'kiRundi'
  },
  {
    languageCode: 'ro',
    languageName: 'română'
  },
  {
    languageCode: 'ru',
    languageName: 'русский язык'
  },
  {
    languageCode: 'sa',
    languageName: 'संस्कृतम्'
  },
  {
    languageCode: 'sc',
    languageName: 'sardu'
  },
  {
    languageCode: 'sd',
    languageName: 'सिन्धी'
  },
  {
    languageCode: 'se',
    languageName: 'Davvisámegiella'
  },
  {
    languageCode: 'sm',
    languageName: 'gagana faa Samoa'
  },
  {
    languageCode: 'sg',
    languageName: 'yângâ tî sängö'
  },
  {
    languageCode: 'sr',
    languageName: 'српски језик'
  },
  {
    languageCode: 'gd',
    languageName: 'Gàidhlig'
  },
  {
    languageCode: 'sn',
    languageName: 'chiShona'
  },
  {
    languageCode: 'si',
    languageName: 'සිංහල'
  },
  {
    languageCode: 'sk',
    languageName: 'slovenčina'
  },
  {
    languageCode: 'sl',
    languageName: 'slovenščina'
  },
  {
    languageCode: 'so',
    languageName: 'Soomaaliga'
  },
  {
    languageCode: 'st',
    languageName: 'Sesotho'
  },
  {
    languageCode: 'es',
    languageName: 'español'
  },
  {
    languageCode: 'su',
    languageName: 'Basa Sunda'
  },
  {
    languageCode: 'sw',
    languageName: 'Kiswahili'
  },
  {
    languageCode: 'ss',
    languageName: 'SiSwati'
  },
  {
    languageCode: 'sv',
    languageName: 'svenska'
  },
  {
    languageCode: 'ta',
    languageName: 'தமிழ்'
  },
  {
    languageCode: 'te',
    languageName: 'తెలుగు'
  },
  {
    languageCode: 'tg',
    languageName: 'тоҷикӣ'
  },
  {
    languageCode: 'th',
    languageName: 'ไทย'
  },
  {
    languageCode: 'ti',
    languageName: 'ትግርኛ'
  },
  {
    languageCode: 'bo',
    languageName: 'བོད་ཡིག'
  },
  {
    languageCode: 'tk',
    languageName: 'Türkmen'
  },
  {
    languageCode: 'tl',
    languageName: 'Wikang Tagalog'
  },
  {
    languageCode: 'tn',
    languageName: 'Setswana'
  },
  {
    languageCode: 'to',
    languageName: 'faka Tonga'
  },
  {
    languageCode: 'tr',
    languageName: 'Türkçe'
  },
  {
    languageCode: 'ts',
    languageName: 'Xitsonga'
  },
  {
    languageCode: 'tt',
    languageName: 'татарча'
  },
  {
    languageCode: 'tw',
    languageName: 'Twi'
  },
  {
    languageCode: 'ty',
    languageName: 'Reo Tahiti'
  },
  {
    languageCode: 'ug',
    languageName: 'Uyƣurqə'
  },
  {
    languageCode: 'uk',
    languageName: 'українська'
  },
  {
    languageCode: 'ur',
    languageName: 'اردو'
  },
  {
    languageCode: 'uz',
    languageName: 'zbek'
  },
  {
    languageCode: 've',
    languageName: 'Tshivenḓa'
  },
  {
    languageCode: 'vi',
    languageName: 'Tiếng Việt'
  },
  {
    languageCode: 'vo',
    languageName: 'Volapük'
  },
  {
    languageCode: 'wa',
    languageName: 'Walon'
  },
  {
    languageCode: 'cy',
    languageName: 'Cymraeg'
  },
  {
    languageCode: 'wo',
    languageName: 'Wollof'
  },
  {
    languageCode: 'fy',
    languageName: 'Frysk'
  },
  {
    languageCode: 'xh',
    languageName: 'isiXhosa'
  },
  {
    languageCode: 'yi',
    languageName: 'ייִדיש'
  },
  {
    languageCode: 'yo',
    languageName: 'Yorùbá'
  },
  {
    languageCode: 'za',
    languageName: 'Saɯ cueŋƅ'
  }
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db =>  // truncate all data
    db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.SupportedLanguage.drop(options))
        .then(() => db.SupportedLanguage.sync(options))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            return db[table].bulkCreate(data);
          }
        ))
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'DirectoryCountries\' AND COLUMN_NAME = \'languageName\'', null, options))
        .then(languageName => {
          if(!languageName[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`DirectoryCountries` ADD COLUMN `languageName` VARCHAR(50) NULL DEFAULT NULL AFTER `languageCode`', null, options);
          }
        })
        .then(() => 
          Promise.map(updateDirectoryCountries, item => {
            let _option = Object.assign({where: {languageCode: item.languageCode}}, options);
            return db.DirectoryCountry.update(item, _option);
          })
        )
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    })
  )
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
