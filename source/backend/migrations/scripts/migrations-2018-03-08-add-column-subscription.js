import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column cancellationReason is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'cancellationReason\'', null, options))
      .then(cancellationReason => {
        if(!cancellationReason[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `cancellationReason` TEXT NULL DEFAULT NULL', null, options);
        }
      })
      // check column isCustom is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'isCustom\'', null, options))
      .then(isCustom => {
        if(!isCustom[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `isCustom` TINYINT NULL DEFAULT \'0\' AFTER `isTrial`', null, options);
        }
      })
      // check column CustomPlanId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'CustomPlanId\'', null, options))
      .then(CustomPlanId => {
        if(!CustomPlanId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `CustomPlanId`  INT(11) NULL', null, options);
        }
      })
      // check column PlanOptionId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'PlanOptionId\'', null, options))
      .then(PlanOptionId => {
        if(!PlanOptionId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `PlanOptionId`  INT(11) NULL', null, options);
        }
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
