import initializeDb from '../../src/db';
import Promise from 'bluebird';
const DATA_PATH = '../init-data/';
const DROP_TABLES = [
  'MarketingOffice',
];
const INIT_TABLES = [
  'MarketingOffice',
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => Promise.mapSeries(DROP_TABLES, 
        table => db[table].drop(options)
      ))
      .then(() => Promise.mapSeries(INIT_TABLES, 
        table => db[table].sync(options)
      ))
      .then(() => Promise.mapSeries(INIT_TABLES,
        table => { // insert new data
          let data = require(`${DATA_PATH}${table}`).default;
          return db[table].bulkCreate(data);
        }
      ))
      // check column MarketingOfficeId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Admins\' AND COLUMN_NAME = \'MarketingOfficeId\'', null, options))
      .then(MarketingOfficeId => {
        if(!MarketingOfficeId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Admins` ADD COLUMN `MarketingOfficeId` INTEGER', null, options);
        }
      })
      // check column MarketingOfficeId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SellerUsers\' AND COLUMN_NAME = \'MarketingOfficeId\'', null, options))
      .then(MarketingOfficeId => {
        if(!MarketingOfficeId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SellerUsers` ADD COLUMN `MarketingOfficeId` INTEGER', null, options);
        }
      })
      // check column MarketingOfficeId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Orders\' AND COLUMN_NAME = \'MarketingOfficeId\'', null, options))
      .then(MarketingOfficeId => {
        if(!MarketingOfficeId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `MarketingOfficeId` INTEGER', null, options);
        }
      })
      .then(() => 
        db.sequelize.query('UPDATE `shave2u`.`Orders` INNER JOIN `shave2u`.`SellerUsers` ON `shave2u`.`Orders`.`SellerUserId` = `shave2u`.`SellerUsers`.`id` SET `shave2u`.`Orders`.`MarketingOfficeId` = `shave2u`.`SellerUsers`.`MarketingOfficeId`', null, options)
      )
      // check column MarketingOfficeId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'MarketingOfficeId\'', null, options))
      .then(MarketingOfficeId => {
        if(!MarketingOfficeId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `MarketingOfficeId` INTEGER', null, options);
        }
      })
      .then(() => 
        db.sequelize.query('UPDATE `shave2u`.`Subscriptions` INNER JOIN `shave2u`.`SellerUsers` ON `shave2u`.`Subscriptions`.`SellerUserId` = `shave2u`.`SellerUsers`.`id` SET `shave2u`.`Subscriptions`.`MarketingOfficeId` = `shave2u`.`SellerUsers`.`MarketingOfficeId`', null, options)
      )
      // check column MarketingOfficeId is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'MarketingOfficeId\'', null, options))
      .then(MarketingOfficeId => {
        if(!MarketingOfficeId[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `MarketingOfficeId` INTEGER', null, options);
        }
      })
      .then(() => 
        db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`SellerUsers` ON `shave2u`.`SaleReports`.`badgeId` = `shave2u`.`SellerUsers`.`badgeId` SET `shave2u`.`SaleReports`.`MarketingOfficeId` = `shave2u`.`SellerUsers`.`MarketingOfficeId`', null, options)
      )
      // check column moCode is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'moCode\'', null, options))
      .then(moCode => {
        if(!moCode[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `moCode` VARCHAR(5) NULL DEFAULT NULL', null, options);
        }
      })
      .then(() => 
        db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`MarketingOffices` ON `shave2u`.`SaleReports`.`MarketingOfficeId` = `shave2u`.`MarketingOffices`.`id` SET `shave2u`.`SaleReports`.`moCode` = `shave2u`.`MarketingOffices`.`moCode`', null, options)
      )
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
