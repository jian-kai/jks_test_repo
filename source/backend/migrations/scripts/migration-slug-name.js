import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Products` ADD COLUMN `slug` VARCHAR(50) NULL AFTER `ProductTypeId`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Plans` ADD COLUMN `slug` VARCHAR(50) NULL AFTER `PlanTypeId`', null, options))
        .then(() => db.Product.findAll({include: ['productTranslate']}))
        .then(products => {
          products = SequelizeHelper.convertSeque2Json(products);
          return Promise.mapSeries(products, (product, index) => {
            let slug = `${product.productTranslate[0].name.toLowerCase().replace(/\s/g, '_').replace(/\’/g, '').replace(/\-/g, '')}_${index}`;
            return db.Product.update({slug}, {where: {id: product.id}});
          });
        })
        .then(() => db.Plan.findAll({include: ['planTranslate']}))
        .then(plans => {
          plans = SequelizeHelper.convertSeque2Json(plans);
          return Promise.mapSeries(plans, (plan, index) => {
            let slug = `${plan.planTranslate[0].name.toLowerCase().replace(/\s/g, '_').replace(/\’/g, '').replace(/\-/g, '')}_${index}`;
            return db.Plan.update({slug}, {where: {id: plan.id}});
          });
        })
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Products` CHANGE COLUMN `slug` `slug` VARCHAR(50) NOT NULL , ADD UNIQUE INDEX `slug_UNIQUE` (`slug` ASC)', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Plans` CHANGE COLUMN `slug` `slug` VARCHAR(50) NOT NULL , ADD UNIQUE INDEX `slug_UNIQUE` (`slug` ASC)', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
