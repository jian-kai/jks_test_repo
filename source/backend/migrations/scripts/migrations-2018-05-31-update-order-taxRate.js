import initializeDb from '../../src/db';
import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let ordersUpdate = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Subscription.findAll())
        .then(subscriptions => 
          Promise.mapSeries(subscriptions, subscription => 
            db.Order.findAll({
              order: ['id'],
              where: {subscriptionIds: {
                $or: [
                  {$like: `%\"${subscription.id}\"%`},
                  {$like: `%[${subscription.id}]%`},
                  {$like: `%[${subscription.id},%`},
                  {$like: `%,${subscription.id},%`},
                  {$like: `%,${subscription.id}]%`}
                ]
              }}
            })
            .then(orders => {
              ordersUpdate = [];
              let taxRate = 0;
              if(orders.length > 1) {
                orders.forEach((order, index) => {
                  if(index === 0) {
                    taxRate = order.taxRate;
                  } else {
                    ordersUpdate.push({id: order.id, taxRate});
                  }
                });
              }
            })
            .then(() => SequelizeHelper.bulkUpdate(ordersUpdate, db, 'Order', {transaction: t}))
          )
        )
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
