import Promise from 'bluebird';
import CryptoUtil from 'crypto-js';
import fs from 'fs';
import InitializeDb from '../../../src/db';
import MailchimpHelper from '../../../src/helpers/mailchimp';
import Config from '../../../src/config';

process.env.CONFIG_ENV = process.env.CONFIG_ENV || 'local';

const csvFolderName = process.env.CONFIG_ENV === 'production' ? process.env.CONFIG_ENV : 'staging';
const mailchimpLists = [
  {
    name: 'Register-List',
    utmColumnIndex: 5,
    id: Config.email.registerListId
  },
  {
    name: 'Subscriber-List',
    utmColumnIndex: 5,
    id: Config.email.subscriberListId
  },
  {
    name: 'Trial-Subscriber-List',
    utmColumnIndex: 5,
    id: Config.email.trialSubscriberList
  }
];

/*------------------------------------------------------------------------------------------------*/
/*-------------------------------------- functions -----------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
function readFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, fileContent) => {
      if(err) {
        console.log(err);
        reject(`Can not read file: ${filePath}!!!`);
      }

      resolve(fileContent);
    });
  });
}

function writeFile(filePath, fileContent) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, fileContent, function(err) {
      if(err) {
        console.log(err);
        reject(`Can not write file: ${filePath}!!!`);
      }

      resolve();
    });
  });
}

InitializeDb()
  .then(db => {
    console.log(`Start migrations mailchimp utm data on ---${process.env.CONFIG_ENV}---`);
    console.log(`Read csv in folder ---${csvFolderName}---`);

    let mailchimp = MailchimpHelper.getInstance();

    return Promise.mapSeries(mailchimpLists, list => {
      let indexPath = `${__dirname}/${csvFolderName}/${list.name}-Index.txt`;

      console.log(`Start reading index file: ---${indexPath}---`);
      return readFile(indexPath)
        .then(indexFileContent => {
          indexFileContent = indexFileContent.toString();
          let csvIndex = 0;

          if(indexFileContent) {
            csvIndex = parseInt(indexFileContent);
          }

          let csvPath = `${__dirname}/${csvFolderName}/${list.name}.csv`;

          console.log(`Start reading csv: ---${csvPath}---`);
          return readFile(csvPath)
            .then(fileContent => {
              // split file content lines
              let csvContentArray = fileContent.toString().replace(/\r/g, '').split(/\r?\n/);
              let rowValue = [];

              return Promise.mapSeries(csvContentArray, (rowData, rowIndex) => {
                // only do the continue
                if(rowIndex <= csvIndex) {
                  return '';
                }

                if(rowIndex !== 0 && rowData !== '') {
                  // skip row of names

                  // split row cell
                  rowValue = rowData.split(/\,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);

                  let memberEmail = rowValue[0];
                  
                  // validate row data
                  if(memberEmail && memberEmail.indexOf('@') < 0) {
                    console.log(`rowValue error: ${rowValue}`);
                    throw new Error('Csv wrong format!');
                  }

                  memberEmail = memberEmail.replace(/"/g, '');
                  let memberUtmSource = rowValue[list.utmColumnIndex].replace(/"/g, '');

                  if(memberUtmSource === '') {
                    // only update member that does not have utm data
                    let where = {
                      email: memberEmail
                    };

                    if(list.name !== 'Register-List') {
                      // only get order that has subscriptionIds
                      where = {
                        $and: [
                          {email: memberEmail},
                          {subscriptionIds: {$not: null}}
                        ]
                      };
                    }

                    return db.Order.findOne({ where, order: [['createdAt', 'ASC']] })
                      .then(order => {
                        if(!order) {
                          order = {};
                        }

                        let mergeFields = {};
                        mergeFields.USOURCE = order.source ? order.source : 'direct';
                        mergeFields.UMEDIUM = order.medium ? order.medium : 'none';
                        mergeFields.UCAMPAIGN = order.campaign ? order.campaign : 'none';
                        mergeFields.UCONTENT = order.content ? order.content : 'none';
                        mergeFields.UTERM = order.term ? order.term : 'none';

                        let memberId = CryptoUtil.MD5(memberEmail.toLowerCase()).toString();

                        console.log(`Start update for member ${memberEmail}`);
                        return mailchimp.updateMember(list.id, memberId, {'merge_fields': mergeFields})
                        .then(result => {
                          if(result && result.id) {
                            writeFile(indexPath, rowIndex);
                            console.log(`Done update for member ${memberEmail}!!!`);
                          }
                        });
                      });
                  }
                }
              }); // end csv row map series
            })
            .catch(error => {
              console.log(error);
            });
        })
        .catch(error => {
          console.log(error);
        });
    }); // end list map series
  })
  .then(() => {
    console.log('Mailchimp utm data updated!!!');
  })
  .catch(err => {
    console.log(`Can't update mailchimp utm data ${err.stack || JSON.stringify(err)}`);
  });
