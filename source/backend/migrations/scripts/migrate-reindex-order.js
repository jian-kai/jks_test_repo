import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

let db;
const FIELD_LENGTH = 10;
const BASE_VALUE = '0000000000';

function fillZero(number) {
  return `${BASE_VALUE.substr(0, (FIELD_LENGTH - number.length))}${number}`;
}

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    let orderData;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll()
        .then(orders => {
          orderData = SequelizeHelper.convertSeque2Json(orders);
          return;
        })
        .then(() => {
          return Promise.mapSeries(orderData, order => {
            if(!order.UserId) {
              return db.User.findOne({where: {email: order.email}})
                .then(user => {
                  if(user) {
                    return db.Order.update({UserId: user.id}, {where: {id: order.id}, transaction: t});
                  }
                });
            }
          })
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options)));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
