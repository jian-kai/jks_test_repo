import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import _ from 'underscore';
import moment from 'moment';
import Promise from 'bluebird';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    let orderIncludes = [
      // 'User', 'SellerUser', 
      'Country', 'DeliveryAddress', 'BillingAddress',
      // {
      //   model: db.Receipt,
      //   require: true
      // },
      {
        model: db.OrderDetail,
        as: 'orderDetail',
        include: [{
          model: db.PlanCountry,
          include: ['Country', {
            model: db.Plan,
            include: ['planTranslate']
          }]
        }, {
          model: db.ProductCountry,
          include: ['Country', {
            model: db.Product,
            include: ['productTranslate']
          }]
        }]
      }
    ];
    let bulkOrderIncludes = [
      'Country', 'DeliveryAddress', 'BillingAddress',
      {
        model: db.BulkOrderDetail,
        as: 'bulkOrderDetail',
        include: [{
          model: db.PlanCountry,
          include: ['Country', {
            model: db.Plan,
            include: ['planTranslate']
          }]
        }, {
          model: db.ProductCountry,
          include: ['Country', {
            model: db.Product,
            include: ['productTranslate']
          }]
        }]
      }
    ];
    let data = {};
    
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        // check column productCategory is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'productCategory\'', null, options))
        .then(productCategory => {
          if(!productCategory[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `productCategory` ENUM(\'Ala Carte\', \'Subscription\') NULL AFTER `email`', null, options);
          }
        })
        // check column taxInvoiceNo is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'taxInvoiceNo\'', null, options))
        .then(taxInvoiceNo => {
          if(!taxInvoiceNo[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `taxInvoiceNo` VARCHAR(20)', null, options);
          }
        })
        // check column bulkOrderId is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'bulkOrderId\'', null, options))
        .then(bulkOrderId => {
          if(!bulkOrderId[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `bulkOrderId` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `orderId`', null, options);
          }
        })
        // check column qty is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'qty\'', null, options))
        .then(qty => {
          if(!qty[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `qty` TEXT NOT NULL AFTER `sku`', null, options);
          }
        })
        // change column orderId
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` CHANGE COLUMN `orderId` `orderId` INT(10) UNSIGNED ZEROFILL NULL', null, options))
        // update taxInvoiceNo
        .then(() => db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`Orders` ON `shave2u`.`SaleReports`.`orderId` = `shave2u`.`Orders`.`id` AND `shave2u`.`SaleReports`.`category` IN (\'client\', \'sales app\') SET `shave2u`.`SaleReports`.`taxInvoiceNo` = `shave2u`.`Orders`.`taxInvoiceNo`', null, options))
        // update productCategory and qty
        // update productCategory and qty of Order for SaleReport
        .then(() => db.Order.findAll({include: orderIncludes, options}))
        .then(orders => Promise.mapSeries(orders, order => {
          let productCategory;
          let qty = '';
          order.orderDetail.forEach(orderDetail => {
            qty += `${orderDetail.qty}, `;
            if(orderDetail.ProductCountry) {
              productCategory = 'Ala Carte';
            } else {
              productCategory = 'Subscription';
            }
          });
          qty = qty.replace(/^(.*)\,\s$/g, '$1');
          return db.SaleReport.update({productCategory, qty}, {where: {orderId: order.id, category: {$in: ['client', 'sales app']}}, options});
        }))
        // update productCategory of BulkOrder for SaleReport
        .then(() => db.BulkOrder.findAll({include: bulkOrderIncludes, options}))
        .then(_bulkOrders => Promise.mapSeries(_bulkOrders, bulkOrder => {
          let productCategory;
          let qty = '';
          bulkOrder.bulkOrderDetail.forEach(bulkOrderDetail => {
            qty += `${bulkOrderDetail.qty}, `;
            if(bulkOrderDetail.ProductCountry) {
              productCategory = 'Ala Carte';
            } else {
              productCategory = 'Subscription';
            }
          });
          qty = qty.replace(/^(.*)\,\s$/g, '$1');
          return db.SaleReport.update({productCategory, qty}, {where: {bulkOrderId: bulkOrder.id, category: 'Bulk Order'}, options});
        }))
        // update column category
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` CHANGE COLUMN `category` `category` ENUM(\'client\', \'sales app\', \'Bulk Order\') NULL DEFAULT \'client\'', null, options))
        // migration data from BulkOrder to SaleReport
        .then(() => db.SaleReport.findAll({where: {category: 'Bulk Order'}}))
        .then(saleReports => {
          data.saleReports = SequelizeHelper.convertSeque2Json(saleReports);
          data.bulkOrderId = _.pluck(data.saleReports, 'bulkOrderId');
          return db.BulkOrder.findAll({where: {id: {$notIn: data.bulkOrderId}}, include: bulkOrderIncludes});
        })
        .then(bulkOrders => {
          let reportList = [];
          data.bulkOrders = SequelizeHelper.convertSeque2Json(bulkOrders);
          data.bulkOrders.forEach(bulkOrder => {
            let productCategory = null;
            let productName = '';
            let sku = '';
            let qty = '';
            let price = 0.00;
            let deliveryAddress = '';
            let billingAddress = '';
            let deliveryContact = '';
            let billingContact = '';
            let agentName = (bulkOrder.SellerUser) ? bulkOrder.SellerUser.agentName : '';
            let badgeId = (bulkOrder.SellerUser) ? bulkOrder.SellerUser.badgeId : '';
            let region = bulkOrder.Country.code;
            let category = 'Bulk Order';
            let customerName = bulkOrder.DeliveryAddress ? `${bulkOrder.DeliveryAddress.firstName} ${bulkOrder.DeliveryAddress.lastName ? bulkOrder.DeliveryAddress.lastName : ''}` : '';
            if(bulkOrder.DeliveryAddress) {
              deliveryAddress = `${bulkOrder.DeliveryAddress.address}, ${bulkOrder.DeliveryAddress.city}, ${bulkOrder.DeliveryAddress.portalCode}, ${bulkOrder.DeliveryAddress.state}`;
              deliveryContact = bulkOrder.DeliveryAddress.contactNumber;
            }
            if(bulkOrder.BillingAddress) {
              billingAddress = `${bulkOrder.BillingAddress.address}, ${bulkOrder.BillingAddress.city}, ${bulkOrder.BillingAddress.portalCode}, ${bulkOrder.BillingAddress.state}`;
              billingContact = bulkOrder.BillingAddress.contactNumber;
            }
            let email = bulkOrder.email;
            // if(order.User) {
            //   email = order.User.email;
            // } else if(order.SellerUser) {
            //   email = order.SellerUser.email;
            // } else {
            //   email = order.email;
            // }
            bulkOrder.bulkOrderDetail.forEach(bulkOrderDetail => {
              console.log(`buildReportInfo ==== ${JSON.stringify(bulkOrderDetail)}`);
              qty += `${bulkOrderDetail.qty}, `;
              if(bulkOrderDetail.ProductCountry) {
                productCategory = 'Ala Carte';
                productName += `${_.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name}, `;
                sku += `${bulkOrderDetail.ProductCountry.Product.sku}, `;
              } else if(bulkOrderDetail.PlanCountry) {
                productCategory = 'Subscription';
                productName += `${_.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name}, `;
                sku += `${bulkOrderDetail.PlanCountry.Plan.sku}, `;
              }
              price = price + (parseFloat(bulkOrderDetail.price) * bulkOrderDetail.qty);
            });
            productName = productName.replace(/^(.*)\,\s$/g, '$1');
            qty = qty.replace(/^(.*)\,\s$/g, '$1');
            sku = sku.replace(/^(.*)\,\s$/g, '$1');
            // if(bulkOrder.Receipt) {
            let reportInfo = {
              badgeId,
              agentName,
              email,
              customerName,
              deliveryAddress,
              deliveryContact,
              billingAddress,
              billingContact,
              productCategory,
              productName,
              sku,
              qty,
              region,
              category,
              deliveryId: bulkOrder.deliveryId,
              carrierAgent: bulkOrder.carrierAgent,
              states: bulkOrder.states,
              promoCode: bulkOrder.promoCode,
              paymentType: bulkOrder.paymentType,
              bulkOrderId: bulkOrder.id,
              receiptId: null,
              currency: bulkOrder.Country.currencyCode,
              createdAt: bulkOrder.createdAt,
              updatedAt: bulkOrder.updatedAt,
              saleDate: new Date(moment(bulkOrder.createdAt).format('YYYY-MM-DD')),
              // totalPrice: (parseFloat(order.Receipt.subTotalPrice) + parseFloat(order.Receipt.discountAmount)).toFixed(2),
              // subTotalPrice: order.Receipt.subTotalPrice,
              // discountAmount: order.Receipt.discountAmount,
              // shippingFee: order.Receipt.shippingFee,
              // taxAmount: order.Receipt.taxAmount,
              // grandTotalPrice: order.Receipt.totalPrice,
              // source: order.source,
              // medium: order.medium,
              // campaign: order.campaign,
              // term: order.term,
              totalPrice: price,
              subTotalPrice: price,
              discountAmount: 0,
              shippingFee: 0,
              taxAmount: 0,
              grandTotalPrice: price,
              source: null,
              medium: null,
              campaign: null,
              term: null,
              content: null,
              CountryId: bulkOrder.Country.id
            };
            reportList.push(reportInfo);
            // }
          });
          return reportList;
        })
        .then(reports => {
          console.log('reports ==== ======== ========= ', reports);
          db.SaleReport.bulkCreate(reports);
        }) // create sale report
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
