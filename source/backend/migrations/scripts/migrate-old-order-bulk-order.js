import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

const CLIENT_ACCOUNT = [14, 139, 255, 284, 15577, 15643, null];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    let data = {};
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({where: {
          UserId: {$in: CLIENT_ACCOUNT}
        }, include: ['orderDetail', 'orderHistories']}))
        .then(orders => {
          orders = SequelizeHelper.convertSeque2Json(orders);
          // build bulk order
          let bulkOrders = [];
          orders.forEach(order => {
            Reflect.deleteProperty(order, 'id');
            // buil order details
            order.orderDetail.forEach(orderDetail => {
              Reflect.deleteProperty(orderDetail, 'id');
              Reflect.deleteProperty(orderDetail, 'OrderId');
            });
            // build order history
            order.orderHistories.forEach(orderHistory => {
              Reflect.deleteProperty(orderHistory, 'id');
              Reflect.deleteProperty(orderHistory, 'OrderId');
            });

            // renew order
            order.bulkOrderDetail = order.orderDetail;
            order.bulkOrderHistories = order.orderHistories;
            bulkOrders.push(order);
          });
          console.log(`bulkOrders === ${JSON.stringify(bulkOrders[0])} ==== ${bulkOrders.length}`);
          return SequelizeHelper.bulkCreate(bulkOrders, db, 'BulkOrder', {include: ['bulkOrderDetail', 'bulkOrderHistories'], transaction: t});
        })
        .then(() => db.Order.destroy({where: {UserId: {$in: CLIENT_ACCOUNT}}, transaction: t})) // remove order
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
