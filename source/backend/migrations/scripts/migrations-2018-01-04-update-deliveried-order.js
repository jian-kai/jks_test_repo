import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import { emailHelper } from '../../src/helpers/email-helper';
import { aftership } from '../../src/helpers/aftership';
import _ from 'underscore';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    // get all delivering order
    let orders;
    return _db.Order.findAll({where: {states: 'Delivering'}})
      .then(_orders => {
        orders = SequelizeHelper.convertSeque2Json(_orders);
        return aftership.getTrackings();
      })
      .then(trackings => Promise.mapSeries(trackings, tracking => {
        if(tracking) {
          let order = _.findWhere(orders, {deliveryId: tracking.tracking_number});
          if(order) {
            return _db.Order.update({states: 'Completed'}, {where: {id: order.id}})
              .then(() => emailHelper.sendReceiptsEmail({orderId: order.id}, _db));
          }
        }
      }));
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
