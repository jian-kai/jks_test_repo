import initializeDb from '../../src/db';
// import SequelizeHelper from '../../src/helpers/sequelize-helper';
import SendEmailQueueService from '../../src/services/send-email.queue.service';
import AppContext from '../../src/services/app-context';
import Promise from 'bluebird';

let db;
const orderIds = [
  '0000016339',
  '0000016340',
  '0000016341',
  '0000016342',
  '0000016343',
  '0000016344',
  '0000016345',
  '0000016346',
  '0000016349',
  '0000016350',
  '0000016354',
  '0000016358',
  '0000016359',
  '0000016361',
  '0000016367',
  '0000016369',
  '0000016381',
  '0000016387',
  '0000016388',
  '0000016401',
  '0000016403',
  '0000016404',
  '0000016406',
  '0000016407',
  '0000016408',
  '0000016409',
  '0000016410',
  '0000016411',
  '0000016412',
  '0000016413',
  '0000016414',
  '0000016415',
  '0000016416',
  '0000016451',
  '0000016454',
  '0000016455',
  '0000016456',
  '0000016457',
  '0000016458',
  '0000016460',
  '0000016461',
  '0000016462',
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    // init app-context
    global.appContext = new AppContext();
    return Promise.delay(1000);
  })
  .then(() => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => {
        let sendEmailQueueService = SendEmailQueueService.getInstance();
        return Promise.mapSeries(orderIds, orderId => 
          sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId}, langCode: 'en'})
        );
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
