import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.SaleReport.findAll({where: {
          $or: [
            {sku: {$like: '%S3/2018%'}},
            {sku: {$like: '%S5/2018%'}},
            {sku: {$like: '%S6/2018%'}},
          ]
        }}))
        .then(reports => {
          reports = SequelizeHelper.convertSeque2Json(reports);
          return Promise.mapSeries(reports, report => {
            report.sku = report.sku.replace(/S3\/2018/g, 'S3');
            report.sku = report.sku.replace(/S5\/2018/g, 'S5');
            report.sku = report.sku.replace(/S6\/2018/g, 'S6');
            return db.SaleReport.update({sku: report.sku}, {where: {id: report.id}, transaction: t});
          });
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
