import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.sequelize.query('UPDATE `shave2u`.`OrderDetails` SET `shave2u`.`OrderDetails`.`currency` = \'RM\' WHERE `shave2u`.`OrderDetails`.`currency` = \'MYR\'', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`OrderDetails` SET `shave2u`.`OrderDetails`.`currency` = \'S$\' WHERE `shave2u`.`OrderDetails`.`currency` = \'SGD\'', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`Receipts` SET `shave2u`.`Receipts`.`currency` = \'RM\' WHERE `shave2u`.`Receipts`.`currency` = \'MYR\'', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`Receipts` SET `shave2u`.`Receipts`.`currency` = \'S$\' WHERE `shave2u`.`Receipts`.`currency` = \'SGD\'', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
