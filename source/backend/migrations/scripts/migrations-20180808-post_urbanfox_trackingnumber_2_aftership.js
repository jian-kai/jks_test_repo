import Promise from 'bluebird';

import initializeDb from '../../src/db';
import { aftership } from '../../src/helpers/aftership';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({where: {
          states: {$in: ['Delivering']},
          carrierAgent: 'courex'
        }, include: ['Country', 'DeliveryAddress']}))
        .then(orders => Promise.mapSeries(orders, order => {
          aftership.postTracking(order, false);
          return;
        }))
        .then(() => db.BulkOrder.findAll({where: {
          states: {$in: ['Delivering']},
          carrierAgent: 'courex'
        }, include: ['Country', 'DeliveryAddress']}))
        .then(orders => Promise.mapSeries(orders, order => {
          aftership.postTracking(order, false);
          return;
        }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
