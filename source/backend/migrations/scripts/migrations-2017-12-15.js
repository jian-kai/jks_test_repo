import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = `shave2u` AND TABLE_NAME = `Users` AND COLUMN_NAME = `isBaGuest`', null, options))
        .then(column => {
          if(column) {
            db.sequelize.query('ALTER TABLE `shave2u`.`Users` DROP COLUMN `isBaGuest`', null, options)
          }
        })
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Users` ADD COLUMN `badgeId` VARCHAR(10) NULL AFTER `NotificationTypeId`', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
