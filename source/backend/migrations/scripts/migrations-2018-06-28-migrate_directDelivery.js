import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import Promise from 'bluebird';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.Subscription.findAll({where: {isDirectTrial: true}}))
      .then(subscriptions => {
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
        return Promise.mapSeries(subscriptions, subscription => db.Order.update({isDirectTrial: true}, {where: {subscriptionIds: subscription.id}}));
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
