import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.sequelize.query('UPDATE `shave2u`.`Receipts` SET `subTotalPrice` = `totalPrice` WHERE `subTotalPrice` <> `totalPrice` AND `shippingFee` = 0.00 AND `taxAmount` = 0.00 AND `last4` IS NOT NULL', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`Receipts` ON `shave2u`.`SaleReports`.`orderId` = `shave2u`.`Receipts`.`OrderId` SET `shave2u`.`SaleReports`.`subTotalPrice` = `shave2u`.`Receipts`.`subTotalPrice`,  `shave2u`.`SaleReports`.`totalPrice` = `shave2u`.`Receipts`.`totalPrice` WHERE `shave2u`.`SaleReports`.`subTotalPrice` <> `shave2u`.`SaleReports`.`grandTotalPrice` AND `shave2u`.`SaleReports`.`shippingFee` = 0.00 AND `shave2u`.`SaleReports`.`taxAmount` = 0.00', null, options))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
