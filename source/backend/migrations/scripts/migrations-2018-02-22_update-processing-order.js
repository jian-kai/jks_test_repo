import Promise from 'bluebird';
import fs from 'fs';

import _ from 'underscore';

import AppContext from '../../src/services/app-context';
import initializeDb from '../../src/db';
import WareHouseHelper from '../../src/helpers/warehouse-helper';

// init app-context
global.appContext = new AppContext();
const OUTPUT_PATH = `${__dirname}/../../src/warehouse-output-files/`;

function readOrderOutBoundFile(db) {
  return new Promise((resolve, reject) => {
    fs.readdir(OUTPUT_PATH, (err, files) => {
      if(err) {
        reject(err)
      } else {
        Promise.mapSeries(files, file => {
          if(file.indexOf('WMSSHP') > -1) {
            return WareHouseHelper.readOrderConfirmation(file, db);
          }
        })
          .then(result => resolve(_.flatten(result)))
          .catch(err => reject(err));
      }
    });
  });
}

console.log('start reading outbound files');

initializeDb()
  .then(db => {
    readOrderOutBoundFile(db)
      .then(outputOrders => {
        outputOrders.forEach(order => {
          console.log(JSON.stringify(order));
        })
      })
        // .then(() => {
        //   return db.OrderUpload.bulkCreate(csvData);
        // })
        .then(() => console.log('Finished!'));
  });
