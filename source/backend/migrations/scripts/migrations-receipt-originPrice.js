import Promise from 'bluebird';
import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Receipt.findAll())
        .then(receipts => {
          let updates = [];
          let tmp = {}; 
          receipts.forEach(receipt => {
            let discountAmount = 0;
            if(receipt.discountAmount) {
              discountAmount = parseFloat(receipt.discountAmount);
            } else {
              tmp.discountAmount = 0;
            }
            tmp.subTotalPrice = receipt.totalPrice - receipt.shippingFee;
            tmp.originPrice = tmp.subTotalPrice + discountAmount;
            tmp.id = receipt.id;
            updates.push(Object.assign({}, tmp));
          });
          return Promise.map(updates, item => db.Receipt.update(item, {where: {id: item.id}}, {transaction: t}));
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
