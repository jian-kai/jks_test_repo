import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
// import _ from 'underscore';
// import moment from 'moment';
// import { aftership } from '../../src/helpers/aftership';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({
          where: {subscriptionIds: { $not: null }}
        }))
        .then(orders => Promise.mapSeries(orders, order => {
          order = SequelizeHelper.convertSeque2Json(order);
          if(order.subscriptionIds[0]) {
            return db.SaleReport.update({productCategory: 'Subscription'}, {where: {orderId: order.id}, transaction: t});
          }
        }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
