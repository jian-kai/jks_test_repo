import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import _ from 'underscore';
import moment from 'moment';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    let orderIncludes = [
      'User', 'SellerUser', 'Country', 'DeliveryAddress', 'BillingAddress',
      {
        model: db.Receipt,
        require: true
      },
      {
        model: db.OrderDetail,
        as: 'orderDetail',
        include: [{
          model: db.PlanCountry,
          include: ['Country', {
            model: db.Plan,
            include: ['planTranslate']
          }]
        }, {
          model: db.ProductCountry,
          include: ['Country', {
            model: db.Product,
            include: ['productTranslate']
          }]
        }]
      }
    ];
    let data = {};
    
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.SaleReport.findAll())
        .then(saleReports => {
          data.saleReports = SequelizeHelper.convertSeque2Json(saleReports);
          data.orderIds = _.pluck(data.saleReports, 'orderId');
          return db.Order.findAll({where: {id: {$notIn: data.orderIds}}, include: orderIncludes});
        })
        .then(orders => {
          let reportList = [];
          data.orders = SequelizeHelper.convertSeque2Json(orders);
          data.orders.forEach(order => {
            let productName = '';
            let sku = '';
            let deliveryAddress = '';
            let billingAddress = '';
            let deliveryContact = '';
            let billingContact = '';
            let agentName = (order.SellerUser) ? order.SellerUser.agentName : '';
            let badgeId = (order.SellerUser) ? order.SellerUser.badgeId : '';
            let region = order.Country.code;
            let category = (order.SellerUser) ? 'sales app' : 'client';
            let customerName = order.User ? `${order.User.firstName} ${order.User.lastName ? order.User.lastName : ''}` : '';
            if(order.DeliveryAddress) {
              deliveryAddress = `${order.DeliveryAddress.address}, ${order.DeliveryAddress.city}, ${order.DeliveryAddress.portalCode}, ${order.DeliveryAddress.state}`;
              deliveryContact = order.DeliveryAddress.contactNumber;
            }
            if(order.BillingAddress) {
              billingAddress = `${order.BillingAddress.address}, ${order.BillingAddress.city}, ${order.BillingAddress.portalCode}, ${order.BillingAddress.state}`;
              billingContact = order.BillingAddress.contactNumber;
            }
            let email;
            if(order.User) {
              email = order.User.email;
            } else if(order.SellerUser) {
              email = order.SellerUser.email;
            } else {
              email = order.email;
            }
            order.orderDetail.forEach(orderDetail => {
              console.log(`buildReportInfo ==== ${JSON.stringify(orderDetail)}`);
              if(orderDetail.ProductCountry) {
                productName += `${_.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name}, `;
                sku += `${orderDetail.ProductCountry.Product.sku}, `;
              } else if(orderDetail.PlanCountry) {
                productName += `${_.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name}, `;
                sku += `${orderDetail.PlanCountry.Plan.sku}, `;
              }
            });
            productName = productName.replace(/^(.*)\,\s$/g, '$1');
            sku = sku.replace(/^(.*)\,\s$/g, '$1');
            if(order.Receipt) {
              let reportInfo = {
                badgeId,
                agentName,
                email,
                customerName,
                deliveryAddress,
                deliveryContact,
                billingAddress,
                billingContact,
                productName,
                sku,
                region,
                category,
                deliveryId: order.deliveryId,
                carrierAgent: order.carrierAgent,
                states: order.states,
                promoCode: order.promoCode,
                paymentType: order.paymentType,
                orderId: order.id,
                receiptId: order.Receipt.id,
                currency: order.Country.currencyCode,
                createdAt: order.createdAt,
                updatedAt: order.updatedAt,
                saleDate: new Date(moment(order.createdAt).format('YYYY-MM-DD')),
                totalPrice: (parseFloat(order.Receipt.subTotalPrice) + parseFloat(order.Receipt.discountAmount)).toFixed(2),
                subTotalPrice: order.Receipt.subTotalPrice,
                discountAmount: order.Receipt.discountAmount,
                shippingFee: order.Receipt.shippingFee,
                taxAmount: order.Receipt.taxAmount,
                grandTotalPrice: order.Receipt.totalPrice,
                source: order.source,
                medium: order.medium,
                campaign: order.campaign,
                term: order.term,
                content: order.content,
                CountryId: order.Country.id
              };
              reportList.push(reportInfo);
            }
          });
          return reportList;
        })
        .then(reports => db.SaleReport.bulkCreate(reports)) // create sale report
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
