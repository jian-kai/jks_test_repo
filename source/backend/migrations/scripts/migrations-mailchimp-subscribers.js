import Promise from 'bluebird';
import initializeDb from '../../src/db';
import mailchimpHelper from '../../src/helpers/mailchimp';

let db; 

initializeDb()
  .then(_db => { 
    db = _db;
    let UsersData; 
    let listId = '61ec0f9922';
    let mailchimp = new mailchimpHelper();   
    return db.User.findAll({    
      where: {
        isActive: 1,
        updatedAt: {$lt: '2017-11-15 02:53:32'}
      }
      //limit : 10
    })      
      .then(users => {
        UsersData = users;    
        return Promise.mapSeries(UsersData, user => Promise.delay(1000).then(() => {
          let options = {
            'email_address': user.dataValues.email,
            'status': 'subscribed',
            'merge_fields': {
              FNAME: user.dataValues.firstName,
              LNAME: user.dataValues.lastName
            }
          }        
          mailchimp.createMember(listId, options);
        }));
      });
  })
  .then(() => {
    console.log('Create mailchimp list are success!!!');
  })
  .catch(err => {
    console.log(`Create mailchimp list ${err.stack || JSON.stringify(err)}`);
  });
