import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.sequelize.query('UPDATE `shave2u`.`DirectoryCountries` SET `flag` = \'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/icons/malaysia-flag.png\' WHERE `flag` = \'https://restcountries.eu/data/mys.svg\'', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`DirectoryCountries` SET `flag` = \'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/icons/singapore-flag.png\' WHERE `flag` = \'https://restcountries.eu/data/sgp.svg\'', null, options))
      .then(() => db.sequelize.query('UPDATE `shave2u`.`DirectoryCountries` SET `flag` = \'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/icons/korea-flag.png\' WHERE `flag` = \'https://restcountries.eu/data/kor.svg\'', null, options))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
