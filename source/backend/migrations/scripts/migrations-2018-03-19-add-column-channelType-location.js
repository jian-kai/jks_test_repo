import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column channelType is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Orders\' AND COLUMN_NAME = \'channelType\'', null, options))
      .then(channelType => {
        if(!channelType[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `channelType` ENUM(\'Event\', \'Streets\', \'B2B\', \'RES\') NULL DEFAULT NULL', null, options);
        }
      })
      // check column eventLocationCode is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Orders\' AND COLUMN_NAME = \'eventLocationCode\'', null, options))
      .then(eventLocationCode => {
        if(!eventLocationCode[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `eventLocationCode` TEXT NULL DEFAULT NULL', null, options);
        }
      })
      // check column channelType is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'channelType\'', null, options))
      .then(channelType => {
        if(!channelType[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `channelType` ENUM(\'Event\', \'Streets\', \'B2B\', \'RES\') NULL DEFAULT NULL AFTER `agentName`', null, options);
        }
      })
      // check column eventLocationCode is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SaleReports\' AND COLUMN_NAME = \'eventLocationCode\'', null, options))
      .then(eventLocationCode => {
        if(!eventLocationCode[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `eventLocationCode` TEXT NULL DEFAULT NULL AFTER `channelType`', null, options);
        }
      })
      // check column channelType is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'channelType\'', null, options))
      .then(channelType => {
        if(!channelType[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `channelType` ENUM(\'Event\', \'Streets\', \'B2B\', \'RES\') NULL DEFAULT NULL', null, options);
        }
      })
      // check column eventLocationCode is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Subscriptions\' AND COLUMN_NAME = \'eventLocationCode\'', null, options))
      .then(eventLocationCode => {
        if(!eventLocationCode[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `eventLocationCode` TEXT NULL DEFAULT NULL', null, options);
        }
      })
      // check column channelType is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SellerUsers\' AND COLUMN_NAME = \'channelType\'', null, options))
      .then(channelType => {
        if(!channelType[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SellerUsers` ADD COLUMN `channelType` ENUM(\'Event\', \'Streets\', \'B2B\', \'RES\') NULL DEFAULT NULL', null, options);
        }
      })
      // check column eventLocationCode is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'SellerUsers\' AND COLUMN_NAME = \'eventLocationCode\'', null, options))
      .then(eventLocationCode => {
        if(!eventLocationCode[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`SellerUsers` ADD COLUMN `eventLocationCode` TEXT NULL DEFAULT NULL', null, options);
        }
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
