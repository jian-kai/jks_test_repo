// import Promise from 'bluebird';
import initializeDb from '../../src/db';
import moment from 'moment';
import shortId from 'shortid';

const promotionData = {
  discount: 0,
  expiredAt: moment('2018-09-13'),
  activeAt: moment(),
  planCountryIds: 'all',
  freeProductCountryIds: '',
  CountryId: 1,
  isOffline: 0,
  promotionTranslate: {
    name: 'Voucher for UMobile',
    description: 'Exclusive U Mobile Promotion',
    langCode: 'EN'
  },
  promotionCodes: []
  
};


let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Product.findOne(
          {
            where: {sku: 'A2'},
            include: [{
              model: db.ProductCountry,
              as: 'productCountry',
              include: {
                model: db.Country,
                where: {code: 'MYS'}
              }
            }]
          }
        ))
        .then(product => {
          let promotionCodeDataList = [];
          promotionData.freeProductCountryIds = `[${product.productCountry[0].id}]`;

          let _code = '';
          let pattern = /[a-zA-Z0-9]{14}/;
          let index = 1;
          while(index <= 2000) {
            _code = `UMOBILETTC${shortId.generate().substr(0, 4).toUpperCase()}`;
            if(promotionCodeDataList.find(item => item.code === _code) || !pattern.test(_code)) {
              console.log('invalid _code ==== ==== === ', _code);
            } else {
              promotionCodeDataList.push({code: _code});
              index++;
            }
          }
          promotionData.promotionCodes = promotionCodeDataList;
          return db.Promotion.create(promotionData, {include: ['promotionTranslate', 'promotionCodes'], returning: true, transaction: t});
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
