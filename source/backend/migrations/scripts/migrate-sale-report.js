import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SaleReportQueueService from '../../src/services/sale-report.queue.service';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import AppContext from '../../src/services/app-context';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    // init app-context
    global.appContext = new AppContext();
    return Promise.delay(1000);
  })
  .then(() => {
    let saleReportQueueService = SaleReportQueueService.getInstance();
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.SaleReport.drop(options))
        .then(() => db.SaleReport.sync(options))
        .then(() => db.Order.findAll())
        .then(orders => {
          orders = SequelizeHelper.convertSeque2Json(orders);
          return Promise.mapSeries(orders, order => saleReportQueueService.addTask({id: order.id}));
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
