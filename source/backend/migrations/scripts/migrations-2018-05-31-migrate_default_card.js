import Promise from 'bluebird';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.User.findAll({where: {'$cards.id$': {$not: null}}, include: ['cards'], transaction: t}))
      .then(users => {
        console.log(`users == ${users.length}`);
        return Promise.mapSeries(users, user => {
          let hasDefault = false;
          user.cards.forEach(card => {
            if(card.isDefault) {
              hasDefault = true;
            }
          });
          if(!hasDefault) {
            return db.Card.update({isDefault: true}, {where: {id: user.cards[0].id}, transaction: t});
          }
        });
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
