import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import Promise from 'bluebird';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let ordersUpdate = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column isSubsequentOrder is exists
      .then(() => db.Subscription.findAll())
      .then(_subscriptions => {
        _subscriptions = SequelizeHelper.convertSeque2Json(_subscriptions);
        return Promise.mapSeries(_subscriptions, subscription => 
          db.Order.findAll({
            where: {$or: [
              {subscriptionIds: {$like: `%[${subscription.id}]%`}},
              {subscriptionIds: {$like: `%,${subscription.id},%`}},
              {subscriptionIds: {$like: `%[${subscription.id},%`}},
              {subscriptionIds: {$like: `%,${subscription.id}]%`}},
              {subscriptionIds: {$like: `%"${subscription.id}"%`}},
              {'$Receipt.SubscriptionId$': subscription.id}
            ]},
            include: ['Receipt'],
            order: ['id']
          })
          .then(_orders => {
            _orders = SequelizeHelper.convertSeque2Json(_orders);
            if(_orders.length > 1) {
              _orders.forEach((order, index) => {
                if(index > 0) {
                  let item = {id: order.id, isSubsequentOrder: true};
                  ordersUpdate.push(item);
                }
              });
            }
            subscription.orders = _orders;
            return subscription;
          })
        );
      })
      .then(() => SequelizeHelper.bulkUpdate(ordersUpdate, db, 'Order'))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
