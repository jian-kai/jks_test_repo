import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column isDirectTrial is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'ProductTranslates\' AND COLUMN_NAME = \'seoTitle\'', null, options))
      .then(isDirectTrial => {
        if(!isDirectTrial[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`ProductTranslates` ADD COLUMN `seoTitle` TEXT NULL DEFAULT NULL', null, options);
        }
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
