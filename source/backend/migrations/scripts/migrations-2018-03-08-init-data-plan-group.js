import Promise from 'bluebird';
import initializeDb from '../../src/db';
import moment from 'moment';

const DATA_PATH = '../init-data/';
const DROP_TABLES = [
  'PlanGroupTranslate',
  'PlanGroup',
];
const INIT_TABLES = [
  'PlanGroup',
  'PlanGroupTranslate',
];

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => Promise.mapSeries(DROP_TABLES, 
          table => db[table].drop(options)
        ))
        .then(() => Promise.mapSeries(INIT_TABLES, 
          table => db[table].sync(options)
        ))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            return db[table].bulkCreate(data);
          }
        ))
        // check column PlanGroupId is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Plans\' AND COLUMN_NAME = \'PlanGroupId\'', null, options))
        .then(PlanGroupId => {
          if(!PlanGroupId[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`Plans` ADD COLUMN `PlanGroupId` INT(11) NULL DEFAULT NULL', null, options);
          }
        })
        // update PlanGroupId
        .then(() => {
          db.sequelize.query('UPDATE `shave2u`.`Plans` SET PlanGroupId = 1 WHERE sku IN (\'2-TK3\', \'3-TK3\', \'4-TK3\')', null, options);
        })
        // update PlanGroupId
        .then(() => {
          db.sequelize.query('UPDATE `shave2u`.`Plans` SET PlanGroupId = 2 WHERE sku IN (\'2-TK5\', \'3-TK5\', \'4-TK5\')', null, options);
        })
        // check path plan-groups is exists
        .then(() => db.sequelize.query('SELECT * FROM shave2u.Paths WHERE pathValue = \'subscriptions/plan-groups\'', null, options))
        .then(PlanGroup => {
          if(!PlanGroup[0][0]) {
            let date = moment().format('YYYY-MM-DD hh:mm:ss');
            db.sequelize.query('INSERT INTO `shave2u`.`Paths` (id, pathValue, createdAt, updatedAt) VALUES(NULL, \'subscriptions/plan-groups\', \'' + date + '\', \'' + date + '\')', null, options);
          }
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
