import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import TaxInvoiceQueueService from '../../src/services/invoice-number.service';
import AppContext from '../../src/services/app-context';
import TaxInvoicelJob from '../../src/scheduler/jobs/tax-invoice.job';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */

let ORDER_IDS = [
  14707,
  14619,
  14620,
  14622,
  14625,
  14626,
  14659,
  14661,
  14662,
  14666,
  14668,
  14690,
  14621,
  14623,
  14624,
  14632,
  14633,
  14634,
  14635,
  14636,
  14637,
  14638,
  14639,
  14640,
  14641,
  14642,
  14643,
  14644,
  14645,
  14646,
  14647,
  14648,
  14649,
  14650,
  14651,
  14652,
  14653,
  14654,
  14655,
  14656,
  14657,
  14658,
  14660,
  14663,
  14664,
  14665,
  14667,
  14669,
  14670,
  14671,
  14672,
  14673,
  14674,
  14675,
  14676,
  14677,
  14678,
  14679,
  14680,
  14681,
  14682,
  14683,
  14684,
  14685,
  14686,
  14687,
  14688,
  14689,
  14691,
  14692,
  14693,
  14694,
  14695,
  14696,
  14697,
  14698,
  14699,
  14701,
  14702,
  14703,
  14704,
  14705,
  14706,
  14709,
  14710,
  14712,
  14713
];

// init app-context
global.appContext = new AppContext();

initializeDb()
  .then(db => { // truncate all data
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({where: {id: {$in: ORDER_IDS}}}))
        .then(orders => Promise.mapSeries(orders, order => {
          if(!order.taxInvoiceNo) {
            return taxInvoiceQueueService.addTask(order);
          }
        }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    })
    .then(() => {
      let taxInvoicelJob = new TaxInvoicelJob();
      return taxInvoicelJob.process(db);
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
