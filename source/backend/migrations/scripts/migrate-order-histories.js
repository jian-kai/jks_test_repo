import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll())
        .then(orders => SequelizeHelper.convertSeque2Json(orders))
        .then(orders => {
          let orderHistories = [];
          let orderHistory;
          // build order history
          orders.forEach(order => {
            console.log(`order == ${JSON.stringify(order)}`);
            orderHistory = {
              OrderId: order.id,
              message: order.states
            };
            orderHistories.push(orderHistory);
          });
          return db.OrderHistory.bulkCreate(orderHistories, {transaction: t});
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
