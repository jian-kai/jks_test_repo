import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.sequelize.query('UPDATE `shave2u`.`Users` SET `firstName` = CASE WHEN LENGTH(SUBSTRING_INDEX(`email`,\'@\',1)) <= 20 THEN SUBSTRING_INDEX(`email`,\'@\',1) ELSE SUBSTRING(`email`,1,20) END WHERE (`firstName` IS NULL OR `firstName` = \'\') AND (`lastName` IS NULL OR `lastName` = \'\')', null, options))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
