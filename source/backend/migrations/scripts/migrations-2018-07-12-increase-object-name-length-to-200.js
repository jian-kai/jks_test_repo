import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PromotionTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`ProductTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`ProductTypes` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`ProductTypeTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanGroups` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanGroupTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanTypes` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanTypeTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanTranslates` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
