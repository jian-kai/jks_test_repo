// import Promise from 'bluebird';
import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('UPDATE `shave2u`.`PlanCountries` INNER JOIN `shave2u`.`Plans` ON `shave2u`.`PlanCountries`.`PlanId` = `shave2u`.`Plans`.`id` SET `shave2u`.`PlanCountries`.`order` = `shave2u`.`Plans`.`order`', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`ProductCountries` INNER JOIN `shave2u`.`Products` ON `shave2u`.`ProductCountries`.`ProductId` = `shave2u`.`Products`.`id` SET `shave2u`.`ProductCountries`.`order` = `shave2u`.`Products`.`order`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
