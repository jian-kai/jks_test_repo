import Promise from 'bluebird';
import initializeDb from '../../src/db';
import config from '../../src/config';
import EmailHelper from '../../src/helpers/email-helper';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

const ORDER_LENGTH = 9;

/**
 * Force initialize database. the old table will be replace with new structure
 */
let emailHelper = new EmailHelper('en');
Promise.delay(5000)
  .then(() => initializeDb())
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({where: {
          $or: [
            {paymentType: 'stripe'},
            {paymentType: 'ipay88'},
          ],
          $and: [
            {states: {$not: 'Canceled'}},
            {states: {$not: 'On Hold'}}
          ],
          CountryId: 1,
          email: 'kimtn129@gmail.com',          
          createdAt: {$lt: '2018-02-28 03:31:51'}
        }, include: ['Country', {
          model: db.User,
          include: ['Country']
        }], transaction: t
        }))
        .then(orders => {
          console.log(`Number of Email: ${orders.length}`);
          return Promise.mapSeries(orders, order => {
            order = SequelizeHelper.convertSeque2Json(order);
            order.isOld = true;
            order.orderId = order.id;
            order.langCode = 'en';
            return emailHelper.sendReceiptsEmail(order, db);
          });
        })
        // .then(() => db.Order.findAll({where: {
        //   $or: [
        //     {paymentType: 'stripe'},
        //     {paymentType: 'ipay88'},
        //   ],
        //   $and: [
        //     {states: {$not: 'Canceled'}},
        //     {states: {$not: 'On Hold'}}
        //   ],
        //   CountryId: 7
        // }, include: ['Country'], transaction: t
        // }))
        // .then(orders => Promise.mapSeries(orders, order => {
        //   order = SequelizeHelper.convertSeque2Json(order);
        //   order.isOld = true;
        //   emailHelper.sendReceiptsEmail(order, db);
        // }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
