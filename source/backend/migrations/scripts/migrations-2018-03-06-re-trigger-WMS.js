import initializeDb from '../../src/db';
import BulkOrderInboundQueueService from '../../src/services/create-bulk-order-inbound.service';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import AppContext from '../../src/services/app-context';
import Promise from 'bluebird';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    // init app-context
    global.appContext = new AppContext();
    return Promise.delay(1000);
  })
  .then(() => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let result;
      let updateBulkOrders = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => db.sequelize.query('SELECT `shave2u`.`BulkOrders`.`id` FROM `shave2u`.`BulkOrders` LEFT JOIN `shave2u`.`OrderUploads` ON `shave2u`.`BulkOrders`.`id` = `shave2u`.`OrderUploads`.`bulkOrderId` WHERE `shave2u`.`OrderUploads`.`bulkOrderId` IS NULL AND `shave2u`.`BulkOrders`.`states` = \'Processing\'', null, options))
      .then(_result => {
        result = SequelizeHelper.convertSeque2Json(_result[0]);
        result.forEach(ele => {
          console.log('BulkOrders.id ========= ========= ======== ', ele.id);
          updateBulkOrders.push({id: ele.id, states: 'Payment Received'});
        });
        return SequelizeHelper.bulkUpdate(updateBulkOrders, db, 'BulkOrder', options);
      })
      .then(() => {
        let bulkOrderInboundQueueService = BulkOrderInboundQueueService.getInstance();
        result.forEach(ele => {
          bulkOrderInboundQueueService.addTask(ele.id);
        });
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
