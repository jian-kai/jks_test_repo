import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.SaleReport.findAll({where: {
          $or: [
            {sku: {$like: '%S3,%'}},
            {sku: {$like: '%S3'}},
            {sku: {$like: '%S5,%'}},
            {sku: {$like: '%S5'}},
            {sku: {$like: '%S6,%'}},
            {sku: {$like: '%S6'}}
          ]
        }}))
        .then(reports => {
          reports = SequelizeHelper.convertSeque2Json(reports);
          return Promise.mapSeries(reports, report => {
            report.sku = report.sku.replace(/(\s)S3|^S3/g, '$1S3/2018');
            report.sku = report.sku.replace(/(\s)S5|^S5/g, '$1S5/2018');
            report.sku = report.sku.replace(/(\s)S6|^S6/g, '$1S6/2018');
            return db.SaleReport.update({sku: report.sku}, {where: {id: report.id}, transaction: t});
          });
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
