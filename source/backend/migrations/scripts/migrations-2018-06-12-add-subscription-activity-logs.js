import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('DROP TABLE IF EXISTS `SubscriptionHistories`', null, options))
        .then(() => db.sequelize.query('CREATE TABLE IF NOT EXISTS `SubscriptionHistories` (`id` int(11) NOT NULL AUTO_INCREMENT, `message` varchar(300) NOT NULL, `detail` text, `subscriptionId` int(11) NOT NULL, `createdAt` datetime NOT NULL, `updatedAt` datetime NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `SubscriptionHistories` ADD KEY `subscriptionId_idx` (`subscriptionId`), ADD CONSTRAINT `foreign_key_subscriptionId` FOREIGN KEY (`subscriptionId`) REFERENCES `Subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE', null, options))
        .then(() => db.sequelize.query('SHOW INDEX FROM `SubscriptionHistories`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });