import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import _ from 'underscore';
import Sequelize from 'sequelize';

import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

const PRODUCTION_CONNECTION = {
  host: 'shaves2u.mysql.database.azure.com',
  user: 'shaves2u@shaves2u',
  password: 'Y}K7G{Sd(75/<Vq!',
  database: 'shave2u',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
}

const DEVELOP_CONNECTION = {
  host: '128.199.201.237',
  user: 'shave2u',
  password: 'Admin@123',
  database: 'shave2u',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
};

const STAGING_CONNECTION = {
  host: '128.199.138.112',
  user: 'shave2u',
  password: 'Admin@123',
  database: 'shave2u',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
};

function readFilePromise() {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/../../src/config/certs/BaltimoreCyberTrustRoot.crt.pem`, (err, data) => {
      if(err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function initStagingDb() {
  return readFilePromise()
    .then(ca => {
      let db = {};
      const MODEL_PATH = `${__dirname}/../../src/models`;
      let connectionConfig = {
        host: STAGING_CONNECTION.host,
        dialect: 'mysql',
        pool: STAGING_CONNECTION.pool,
        define: {
          timestamps: true
        }
      };
      // if(process.env.CONFIG_ENV === 'production') {
      //   connectionConfig.dialectOptions = {
      //     encrypt: true,
      //     ssl: {
      //       ca
      //     }
      //   }
      // }
      let sequelize = new Sequelize(STAGING_CONNECTION.database, STAGING_CONNECTION.user, STAGING_CONNECTION.password, connectionConfig);
      return new Promise((resolved, reject) => {
        fs.readdir(MODEL_PATH, (err, files) => {
          if(err) {
            reject(err);
          } else {
            // fetch all files in models folder
            files.forEach(file => {
              if(file.indexOf('.') !== 0 && file !== 'index.js' && file.slice(-3) === '.js') {
                let model = sequelize.import(path.join(`${__dirname}/../../src/models`, file));
                db[model.name] = model;
              }
            });
            // make constrain for model
            Object.keys(db).forEach(function(modelName) {
              if('associate' in db[modelName]) {
                db[modelName].associate(db);
              }
            });
    
            db.sequelize = sequelize;
            db.Sequelize = Sequelize;
            resolved(db);
          }
        });
      });
    });
}

function initProductionDb() {
  return readFilePromise()
    .then(ca => {
      let db = {};
      const MODEL_PATH = `${__dirname}/../../src/models`;
      let connectionConfig = {
        host: DEVELOP_CONNECTION.host,
        dialect: 'mysql',
        pool: DEVELOP_CONNECTION.pool,
        define: {
          timestamps: true
        }
      };
      // connectionConfig.dialectOptions = {
      //   encrypt: true,
      //   ssl: {
      //     ca
      //   }
      // }
      let sequelize = new Sequelize(DEVELOP_CONNECTION.database, DEVELOP_CONNECTION.user, DEVELOP_CONNECTION.password, connectionConfig);
      return new Promise((resolved, reject) => {
        fs.readdir(MODEL_PATH, (err, files) => {
          if(err) {
            reject(err);
          } else {
            // fetch all files in models folder
            files.forEach(file => {
              if(file.indexOf('.') !== 0 && file !== 'index.js' && file.slice(-3) === '.js') {
                let model = sequelize.import(path.join(`${__dirname}/../../src/models`, file));
                db[model.name] = model;
              }
            });
            // make constrain for model
            Object.keys(db).forEach(function(modelName) {
              if('associate' in db[modelName]) {
                db[modelName].associate(db);
              }
            });
    
            db.sequelize = sequelize;
            db.Sequelize = Sequelize;
            resolved(db);
          }
        });
      });
    });
}

// insert data to new DB
let stagingDb;
let productionDb;
let data = {};
initStagingDb()
  .then(db => {
    stagingDb = db;
    return initProductionDb();
  })
  .then(db => {
    productionDb = db;
    return stagingDb.Product.findAll({
      where: {
        status: 'active'
      },
      include: [
        'productCountry',
        'productTranslate',
        'productImages'
      ]
    });
  })
  // migrate exist product
  .then(products => {
    data.stagingProducts = SequelizeHelper.convertSeque2Json(products);
    return productionDb.sequelize.transaction(t => {
      console.log('get production product');
      return Promise.mapSeries(data.stagingProducts, product => {
        console.log('start transaction');
        return productionDb.Product.findOne({where: {sku: product.sku}, include: ['productCountry', 'productTranslate', 'productImages']})
          .then(productNew => {
            if(productNew) {
              // update product country
              productNew = SequelizeHelper.convertSeque2Json(productNew);
              console.log(`productNew ${JSON.stringify(productNew)}`);
  
              // prepare data for product country
              let insertProductCountry = [];
              let updateProductCountry = [];
              product.productCountry.forEach(productCountry => {
                let tmp = _.findWhere(productNew.productCountry, {CountryId: productCountry.CountryId});
                if(tmp) {
                  productCountry.id = tmp.id;
                  productCountry.ProductId = tmp.ProductId;
                  updateProductCountry.push(Object.assign({}, productCountry));
                } else {
                  Reflect.deleteProperty(productCountry, 'id');
                  productCountry.ProductId = productNew.id;
                  insertProductCountry.push(Object.assign({}, productCountry));
                }
              });
              data.updateProductCountry = updateProductCountry;
              data.insertProductCountry = insertProductCountry;
  
              // prepare data for product translate
              let insertProductTranslate = [];
              let updateProductTranslate = [];
              product.productTranslate.forEach(productTranslate => {
                let tmp = _.findWhere(productNew.productTranslate, {langCode: productTranslate.langCode});
                if(tmp) {
                  productTranslate.id = tmp.id;
                  productTranslate.ProductId = tmp.ProductId;
                  updateProductTranslate.push(Object.assign({}, productTranslate));
                } else {
                  Reflect.deleteProperty(productTranslate, 'id');
                  productTranslate.ProductId = productNew.id;
                  insertProductTranslate.push(Object.assign({}, productTranslate));
                }
              });
              data.updateProductTranslate = updateProductTranslate;
              data.insertProductTranslate = insertProductTranslate;
  
              // prepare data for product image
              let insertProductImage = [];
              let imageIdstoDelete = [];
              productNew.productImages.forEach(productImage => {
                imageIdstoDelete.push(productImage.id);
              });
              product.productImages.forEach(productImage => {
                Reflect.deleteProperty(productImage, 'id');
                productImage.ProductId = productNew.id;
                insertProductImage.push(Object.assign({}, productImage));
              });
              data.imageIdstoDelete = imageIdstoDelete;
              data.insertProductImage = insertProductImage;
              return;
            } else {
              return;
            }
          })
          .then(() => {
            console.log('update product country');
            if(data.updateProductCountry && data.updateProductCountry.length > 0) {
              return Promise.mapSeries(data.updateProductCountry, pro => productionDb.ProductCountry.update(pro, {where: {id: pro.id}, transaction: t}));
            } else {
              return;
            }
          })
          .then(() => {
            console.log('insert product country');
            if(data.insertProductCountry && data.insertProductCountry.length > 0) {
              return productionDb.ProductCountry.bulkCreate(data.insertProductCountry, {transaction: t});
            } else {
              return;
            }
          })
          .then(() => {
            console.log('update product translate');
            if(data.updateProductTranslate && data.updateProductTranslate.length > 0) {
              return Promise.mapSeries(data.updateProductTranslate, pro => productionDb.ProductTranslate.update(pro, {where: {id: pro.id}, transaction: t}));
            } else {
              return;
            }
          })
          .then(() => {
            console.log('insert product translate');
            if(data.insertProductTranslate && data.insertProductTranslate.length > 0) {
              productionDb.ProductTranslate.bulkCreate(data.insertProductTranslate, {transaction: t});
            } else {
              return;
            }
          })
          .then(() => {
            console.log('delete product image');
            console.log('data.imageIdstoDelete === === ', data.imageIdstoDelete);
            if(data.imageIdstoDelete && data.imageIdstoDelete.length > 0) {
              return productionDb.ProductImage.destroy({where: {id: {$in: data.imageIdstoDelete}}, transaction: t});
            } else {
              return;
            }
          })
          .then(() => {
            console.log('insert product image');
            if(data.insertProductImage && data.insertProductImage.length > 0) {
              productionDb.ProductImage.bulkCreate(data.insertProductImage, {transaction: t});
            } else {
              return;
            }
          });
      });
    });
  })
  // migrate new product
  .then(() => {
    let skuList = ['H3S6/2018', 'H1S6/2018', 'H1S5/2018', 'H1S3/2018'];
    return stagingDb.Product.findAll({
      where: {
        status: 'active',
        sku: {$in: skuList}
      },
      include: [
        'productCountry',
        'productTranslate',
        'productImages',
        'productDetails'
      ]
    });
  })
  .then(productData => {
    console.log('migrate new product');
    productData = SequelizeHelper.convertSeque2Json(productData);
    
    if(productData) {
      productData.forEach(product => {
        Reflect.deleteProperty(product, 'id');
        product.productCountry.forEach(proCountry => {
          Reflect.deleteProperty(proCountry, 'id');
        });
        product.productTranslate.forEach(proTranslate => {
          Reflect.deleteProperty(proTranslate, 'id');
        });
        product.productImages.forEach(proImage => {
          Reflect.deleteProperty(proImage, 'id');
        });
        product.productDetails.forEach(proDetail => {
          Reflect.deleteProperty(proDetail, 'id');
        });
      });

      return Promise.mapSeries(productData, prod => 
        productionDb.sequelize.transaction(t => {
          let productResult;
          return productionDb.Product.create(prod, {returning: true, transaction: t})
          .then(result => {
            productResult = SequelizeHelper.convertSeque2Json(result);
            prod.productCountry.forEach(proCountry => {
              proCountry.ProductId = productResult.id;
            });
            return productionDb.ProductCountry.bulkCreate(prod.productCountry, {transaction: t});
          })
          .then(() => {
            prod.productTranslate.forEach(proTranslate => {
              proTranslate.ProductId = productResult.id;
            });
            return productionDb.ProductTranslate.bulkCreate(prod.productTranslate, {transaction: t});
          })
          .then(() => {
            prod.productImages.forEach(proImage => {
              proImage.ProductId = productResult.id;
            });
            return productionDb.ProductImage.bulkCreate(prod.productImages, {transaction: t});
          })
          .then(() => {
            prod.productDetails.forEach(proDetail => {
              proDetail.ProductId = productResult.id;
            });
            return productionDb.ProductDetail.bulkCreate(prod.productDetails, {transaction: t});
          });
        })
      );
    } else {
      return;
    }
  })
  .then(() => console.log('Migrate succeed'))
  .catch(err => console.log(err));
