import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column customOneDayShippingFee is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Countries\' AND COLUMN_NAME = \'customOneDayShippingFee\'', null, options))
      .then(customOneDayShippingFee => {
        if(!customOneDayShippingFee[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `customOneDayShippingFee` DECIMAL(10,2) NULL DEFAULT \'0\'', null, options);
        }
      })
      // check column customTwoDayShippingFee is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Countries\' AND COLUMN_NAME = \'customTwoDayShippingFee\'', null, options))
      .then(customTwoDayShippingFee => {
        if(!customTwoDayShippingFee[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `customTwoDayShippingFee` DECIMAL(10,2) NULL DEFAULT \'0\'', null, options);
        }
      })
      // check column customGroundShippingFee is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'Countries\' AND COLUMN_NAME = \'customGroundShippingFee\'', null, options))
      .then(customGroundShippingFee => {
        if(!customGroundShippingFee[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `customGroundShippingFee` DECIMAL(10,2) NULL DEFAULT \'0\'', null, options);
        }
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
