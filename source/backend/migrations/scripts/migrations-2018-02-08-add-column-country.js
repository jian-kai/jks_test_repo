import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isBaEcommerce` TINYINT NULL DEFAULT \'0\' AFTER `phone`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isBaSubscription` TINYINT NULL DEFAULT \'0\' AFTER `isBaEcommerce`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isBaAlaCarte` TINYINT NULL DEFAULT \'0\' AFTER `isBaSubscription`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isBaStripe` TINYINT NULL DEFAULT \'0\' AFTER `isBaAlaCarte`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isWebEcommerce` TINYINT NULL DEFAULT \'0\' AFTER `isBaStripe`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isWebSubscription` TINYINT NULL DEFAULT \'0\' AFTER `isWebEcommerce`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isWebAlaCarte` TINYINT NULL DEFAULT \'0\' AFTER `isWebSubscription`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `isWebStripe` TINYINT NULL DEFAULT \'0\' AFTER `isWebAlaCarte`', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isBaEcommerce` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isBaSubscription` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isBaAlaCarte` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isBaStripe` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isWebEcommerce` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isWebSubscription` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isWebAlaCarte` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `isWebStripe` = \'1\' WHERE `code` IN (\'MYS\', \'SGP\')', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
