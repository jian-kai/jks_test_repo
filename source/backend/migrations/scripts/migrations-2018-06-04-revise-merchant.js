import Promise from 'bluebird';
import initializeDb from '../../src/db';

const DATA_PATH = '../init-data/';
const INIT_TABLES = [
  'Role',
  'RoleDetail',
  'Path',
];

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Cards` ADD COLUMN `CountryId` INTEGER NULL, ADD CONSTRAINT `Cards_ibfk_2` FOREIGN KEY (`CountryId`) REFERENCES `shave2u`.`Countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
