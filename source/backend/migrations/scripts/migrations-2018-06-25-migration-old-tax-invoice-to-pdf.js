import initializeDb from '../../src/db';
import Promise from 'bluebird';
import fs from 'fs';
import pdf from 'html-pdf';
import moment from 'moment';
import Handlebars from 'handlebars';
import jwt from 'jsonwebtoken';
import AppContext from '../../src/services/app-context';
import _ from 'underscore';
import config from '../../src/config';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import OrderHelper from '../../src/helpers/order-helper';
import { awsHelper } from '../../src/helpers/aws-helper';
// import { in18 } from '../in18';
import DatetimeHelper from '../../src/helpers/datetime-helpers';
// import MailChimp from './mailchimp';

Handlebars.registerHelper('displayCurrency', value => {
  value = parseFloat(value);
  return value.toLocaleString('us-EN', {minimumFractionDigits: 2, maximumFractionDigits: 2});
});

let db;
// loading template
let baseTmp;
let leftLogoTmp;
let receiptBodyEmailTmp;
_readFile(`${__dirname}/../../src/views/email-templates/base/base.hbs`)
.then(sText => {
  baseTmp = sText;
});

_readFile(`${__dirname}/../../src/views/email-templates/base/left-logo.hbs`)
.then(sText => {
  leftLogoTmp = sText;
});

_readFile(`${__dirname}/../../src/views/email-templates/en/receipt-body.hbs`)
.then(sText => {
  receiptBodyEmailTmp = sText;
});


let receiptBulkOrderEmailTmp;
_readFile(`${__dirname}/../../src/views/email-templates/en/receipt-bulk-order.hbs`)
.then(sText => {
  receiptBulkOrderEmailTmp = sText;
});

let baseTemplate;
let template;
let logoTemplate;
let bulkTemplate;

// let fileUploadList = [];
// let insertList = [];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    // init app-context
    global.appContext = new AppContext();
    return Promise.delay(2000).then(() => {
      baseTemplate = Handlebars.compile(baseTmp);
      template = Handlebars.compile(receiptBodyEmailTmp);
      logoTemplate = Handlebars.compile(leftLogoTmp);
      bulkTemplate = Handlebars.compile(receiptBulkOrderEmailTmp);
    });
  })
  .then(() =>
    db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({
          where: {
            taxInvoiceNo: {$not: null}, 
            UserId: {$not: null}, 
            id: {$notIn: db.sequelize.literal('(SELECT orderId FROM FileUploads WHERE orderId IS NOT NULL)')}
          }, 
          include: [{
            model: db.Country,
            as: 'Country',
            where: {code: 'MYS'}
          }]
        }))
        // .then(() => db.Order.findAll({where: {taxInvoiceNo: {$not: null}}, include: ['Country']}))
        // .then(() => db.Order.findAll({where: {taxInvoiceNo: 'S2U-MY-000000002'}, include: ['Country']}))
        .then(orders => {
          if(orders.length > 0) {
            let index = 0;
            return Promise.mapSeries(orders, order => {
              index++;
              console.log(`Order generationPDF processing === === === === ${index}/${orders.length}`);
              return generationPDF({orderId: order.id, langCode: order.Country.defaultLang.toLowerCase()});
            });
          }
        })
        .then(() => db.BulkOrder.findAll({
          where: {
            taxInvoiceNo: {$not: null}, 
            id: {$notIn: db.sequelize.literal('(SELECT bulkOrderId FROM FileUploads WHERE bulkOrderId IS NOT NULL)')}
          }, 
          include: [{
            model: db.Country,
            as: 'Country',
            where: {code: 'MYS'}
          }]
        }))
        .then(orders => {
          if(orders.length > 0) {
            let index = 0;
            return Promise.mapSeries(orders, order => {
              index++;
              console.log(`Bulk Order generationPDF processing === === === === ${index}/${orders.length}`);
              return generationPDFBulkOrder({orderId: order.id, langCode: order.Country.defaultLang.toLowerCase()});
            });
          }
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    })
  )
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });

function generationPDF(options) {
  // get order detail
  return _getOrderData(options.orderId)
    .then(order => {
      if(!order.SellerUser || order.subscriptionIds) {
        order.states = 'Payment Received';
      } else {
        order.states = 'Completed';
      }

      let emailTitle;
      let emailSubject;
      let isTaxInvoice;
      // get link to order detail
      let expireAt = moment().add(config.email.expiredTime);
      if(order.User.isActive) {
        order.link = `${config.webUrl}user/orders/${order.id}?utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
        order.isOldUser = true;
      } else if(!order.User.isGuest) {
        order.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
        order.isActiveUser = true;
      } else {
        let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${order.User.email}&token=${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
        order.link = activeAndViewOrderLink;
        order.isNewUser = true;
      }

      // check e-commerce
      if(order.Country.isWebEcommerce) {
        order.isEcommerce = true;
      } else {
        order.link = `${config.webUrl}`;
      }

      // process status
      order.checkMysConfirm = order.countryCode === 'mys';
      order.checkTaxInvoice = order.countryCode === 'mys';
      if(order.states === 'Payment Received') {
        emailTitle = 'Tax Invoice';
        isTaxInvoice = true;
        if(order.SellerUser) {
          order.isBaSale = true;
          order.isBaSubSaleMys = true;
          order.link = `${config.webUrl}`;
        }
        if(order.subscriptionIds) {
          if(order.checkMysConfirm) {
            emailSubject = '[Shaves2U] Order {value}: Your S2U Tax Invoice'.replace('{value}', order.displayId);
          } else {
            emailSubject = '[Shaves2U] Order {value}: Your order has been confirmed!'.replace('{value}', order.displayId);
          }
        } else {
          emailSubject = '[Shaves2U] Order {value}: Your order has been confirmed!'.replace('{value}', order.displayId);
        }
        order.orderConfirmed = true;
      } else if(order.states === 'Completed') {
        order.orderCompleted = true;
        if(order.SellerUser) {
          if(order.checkMysConfirm && (order.Promotion && order.Promotion.promotionCodes && order.Promotion.promotionCodes[0].code === 'EVENTRM1')) {
            emailTitle = 'Tax Invoice';
            emailSubject = '[Shaves2U] Order {value}: Your S2U Tax Invoice'.replace('{value}', order.displayId);
            isTaxInvoice = true;
            order.isBaAlaSaleMys = true;
            order.link = `${config.webUrl}`;
          } else {
            emailTitle = 'Delivery Confirmation';
            emailSubject = '[Shaves2U] Your order has been delivered';
            isTaxInvoice = true;
          }
          order.isBaSale = true;
        }
      }

      order.orderDetail.forEach(orderDetail => {
        orderDetail.currency = order.Country.currencyDisplay;
        if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan && orderDetail.PlanCountry.Plan.isTrial) {
          order.isTrial = true;
        }

        orderDetail.taxCode = order.Country.taxCode;
        orderDetail.taxRate = order.Country.taxRate;
        orderDetail.taxName = order.Country.taxName;
        
        orderDetail.priceExcludeTax = (0.00).toFixed(2);
        orderDetail.taxPrice = (0.00).toFixed(2);
        if(order.Country.includedTaxToProduct) {
          orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
          orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
        }
      });

      order.Receipt.priceExcludeTax = (0.00).toFixed(2);
      order.Receipt.taxPrice = (0.00).toFixed(2);
      order.Receipt.cashRebate = (0.00).toFixed(2);
      order.Receipt.totalPayable = (order.Receipt.totalPrice - order.Receipt.cashRebate).toFixed(2);
      if(order.hasShippingFee) {
        order.Receipt.priceExcludeTax = (order.Receipt.shippingFee / (1 + (order.Country.taxRate / 100))).toFixed(2);
        order.Receipt.taxPrice = (order.Receipt.shippingFee - order.Receipt.priceExcludeTax).toFixed(2);
      }
      
      order.priceExcludeTax = (order.Receipt.totalPrice / (1 + (order.Country.taxRate / 100))).toFixed(2);
      order.taxPrice = (order.Receipt.totalPrice - order.priceExcludeTax).toFixed(2);

      order.isOffline = !!order.SellerUserId;
      if(!order.isOffline) {
        order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
      } else {
        order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery).format('DD-MMM-YYYY');
      }

      let createdAt = order.createdAt;
      order.createdAt = moment(new Date(order.createdAt)).format('DD MMM YYYY');
      let bodyHtml = template(order);
      let logoHtml = logoTemplate({
        homepage: config.webUrl,
        title: emailTitle
      });
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml,
        countryCode: order.countryCode,
        checkMysConfirm: order.checkMysConfirm,
        checkTaxInvoice: order.checkTaxInvoice,
        country: order.Country
      });
      
      if(isTaxInvoice && order.checkMysConfirm) {
        let fileName = `TaxInvoice_${order.displayId}_${moment(createdAt).format('YYYY-MM-DD_HHmmssSSS')}.pdf`;
        return createPDF(emailHtml, `${config.taxInvoicePath}/${fileName}`, order, fileName);
      } else {
        return;
      }
    })
    .catch(err => {
      console.log(`generation PDF error : ${err.stack || JSON.stringify(err)}`);
    });
}

function createPDF(emailHtml, url, order, fileName) {
  return new Promise((resolve, reject) => {
    pdf.create(emailHtml, { height: '2043px', width: '1444px' }).toFile(url, (err, res) => {
    // pdf.create(emailHtml, { height: '23.3in', width: '18in' }).toFile(url, (err, res) => {
      if(err) reject(err);

      // upload file to S3
      let fileUpload = {
        fieldName: 'url',
        originalFilename: fileName,
        path: res.filename,
        name: fileName,
        type: 'application/pdf',
        orderId: order.id
      };
      
      awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
        .then(uploadedFile => db.FileUpload.create(
          {
            name: fileName,
            url: uploadedFile.Location,
            orderId: order.id,
            isDownloaded: false

          },
          {returning: true})
        )
        .then(() => 
          fs.unlink(res.filename, err => {
            if(err) reject(err);
            resolve();
          })
        )
        .catch(error => reject(error));
    });
  });
}

// send receipts bulk order email
function generationPDFBulkOrder(options) {
  // get order detail
  return _getBulkOrderData(options.orderId, db)
    .then(order => {
      let emailTitle;
      let emailSubject;
      // get link to order detail
      
      order.link = `${config.webUrl}`;

      // process status
      emailTitle = 'Tax Invoice';
      emailSubject = '[Shaves2U] Order {value}: Your S2U Tax Invoice';

      order.totalPrice = 0.00;
      order.bulkOrderDetail.forEach(orderDetail => {
        orderDetail.currency = order.Country.currencyDisplay;
        if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan && orderDetail.PlanCountry.Plan.isTrial) {
          order.isTrial = true;
        }

        orderDetail.taxCode = order.Country.taxCode;
        orderDetail.taxRate = order.Country.taxRate;
        orderDetail.taxName = order.Country.taxName;
        
        orderDetail.priceExcludeTax = (0.00).toFixed(2);
        orderDetail.taxPrice = (0.00).toFixed(2);
        if(order.Country.includedTaxToProduct) {
          orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
          orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
        }
        order.totalPrice = parseFloat(order.totalPrice) + (parseFloat(orderDetail.price) * orderDetail.qty);
      });

      order.Receipt = {};
      order.Receipt.originPrice = order.totalPrice.toFixed(2);
      order.Receipt.discountAmount = (0.00).toFixed(2);
      order.Receipt.totalPrice = order.totalPrice.toFixed(2);
      order.Receipt.priceExcludeTax = (0.00).toFixed(2);
      order.Receipt.taxPrice = (0.00).toFixed(2);
      order.Receipt.cashRebate = (0.00).toFixed(2);
      order.Receipt.currency = order.Country.currencyDisplay;
      order.Receipt.totalPayable = (order.totalPrice - order.Receipt.cashRebate).toFixed(2);
      
      order.priceExcludeTax = (order.totalPrice / (1 + (order.Country.taxRate / 100))).toFixed(2);
      order.taxPrice = (order.totalPrice - order.priceExcludeTax).toFixed(2);

      let createdAt = order.createdAt;
      order.createdAt = moment(new Date(order.createdAt)).format('DD MMM YYYY');
      let logoHtml = logoTemplate({
        homepage: config.webUrl,
        title: emailTitle
      });
      let bodyHtml = bulkTemplate(order);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml,
        countryCode: order.countryCode,
        country: order.Country
      });

      let fileName = `TaxInvoice_${order.displayId}_${moment(createdAt).format('YYYY-MM-DD_HHmmssSSS')}.pdf`;

      // return pdf.create(emailHtml, { format: 'Letter' }).toFile(`${config.taxInvoicePath}/${fileName}`, function(err, res) {
      //   if(err) return console.log('err ===== ', err);

      //   // upload file to S3
      //   let fileUpload = {
      //     fieldName: 'url',
      //     originalFilename: fileName,
      //     path: res.filename,
      //     name: fileName,
      //     type: 'application/pdf'
      //   };
        
      //   awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
      //   .then(uploadedFile => 
      //     db.FileUpload.create(
      //       {
      //         name: fileName,
      //         url: uploadedFile.Location,
      //         bulkOrderId: order.id,
      //         isDownloaded: false

      //       },
      //       {returning: true}
      //     )
      //     .then(() => 
      //       fs.unlink(res.filename, err => {
      //         if(err) throw err;
      //         console.log(`successfully deleted ${res.filename}`);
      //       })
      //     )
      //   );
      //   return res; // { filename: '/app/businesscard.pdf' }
      // });
      return new Promise((resolve, reject) => {
        pdf.create(emailHtml, { height: '2043px', width: '1444px' }).toFile(`${config.taxInvoicePath}/${fileName}`, (err, res) => {
          if(err) reject(err);
    
          // upload file to S3
          let fileUpload = {
            fieldName: 'url',
            originalFilename: fileName,
            path: res.filename,
            name: fileName,
            type: 'application/pdf'
          };
          
          awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
            .then(uploadedFile => 
              db.FileUpload.create(
                {
                  name: fileName,
                  url: uploadedFile.Location,
                  bulkOrderId: order.id,
                  isDownloaded: false
      
                },
                {returning: true})
            )
            .then(() => 
              fs.unlink(res.filename, err => {
                if(err) reject(err);
                resolve();
              })
            )
            .catch(error => reject(error));
        });
      });
    })
    .catch(err => {
      console.log(`Send receipt bulk order email error : ${err.stack || JSON.stringify(err)}`);
    });
}

function _readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'UTF8', (oErr, sText) => {
      if(!oErr) {
        resolve(sText);
      } else {
        reject(oErr);
      }
    });
  });
}

function _getOrderData(orderId) {
  // get order detail
  let order;
  return db.Order.findOne({
    where: {id: orderId},
    include: [
      'User',
      'SellerUser',
      'Receipt',
      'DeliveryAddress',
      'BillingAddress',
      'Country',
      {
        model: db.OrderDetail,
        as: 'orderDetail',
        include: [
          {
            model: db.ProductCountry,
            include: [
              {
                model: db.Product,
                include: ['productTranslate', 'productImages']
              }
            ]
          },
          {
            model: db.PlanCountry,
            include: [
              {
                model: db.Plan,
                include: ['PlanType', 'planTranslate', 'planImages']
              }, {
                model: db.PlanDetail,
                as: 'planDetails',
                include: [{
                  model: db.ProductCountry,
                  include: [{
                    model: db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }, {
                model: db.PlanTrialProduct,
                as: 'planTrialProducts',
                include: [{
                  model: db.ProductCountry,
                  include: [{
                    model: db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }
            ]
          }
        ]
      },
      {
        model: db.Promotion,
        as: 'Promotion',
        include: ['promotionCodes']
      }
    ]
  })
    .then(_order => {
      order = SequelizeHelper.convertSeque2Json(_order);
      // detemine language
      let lang = order.Country.defaultLang;

      // build order
      order.displayId = OrderHelper.fromatOrderNumber(order);

      if(order.User && order.User.firstName) {
        order.displayName = order.User.firstName;
      } else if(order.fullName) {
        order.displayName = order.fullName;
      } else {
        order.displayName = 'Guest';
      }

      // build email template data
      order.orderDetail.forEach(orderDetail => {
        orderDetail.totalPrice = 0;
        orderDetail.currency = order.Country.currencyDisplay;

        if(orderDetail.ProductCountry) {
          orderDetail.ProductCountry.Product.productTranslate = _.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: lang});
          orderDetail.ProductCountry.Product.productImages = _.findWhere(orderDetail.ProductCountry.Product.productImages, {isDefault: true});
          orderDetail.totalPrice = (orderDetail.price * orderDetail.qty).toFixed(2);
          orderDetail.type = 'ALA-CARTE';
          orderDetail.isProduct = true;
          orderDetail.isFreeProduct = order.Promotion ? (order.Promotion.freeProductCountryIds) : false;
        }
        if(orderDetail.PlanCountry) {
          orderDetail.PlanCountry.Plan.planTranslate = _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: lang});
          orderDetail.PlanCountry.Plan.planImages = _.findWhere(orderDetail.PlanCountry.Plan.planImages, {isDefault: true});
          orderDetail.unitPrice = (orderDetail.price * orderDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
          orderDetail.totalPrice = (orderDetail.price * orderDetail.qty).toFixed(2);
          orderDetail.type = 'SUBSCRIPTION';
          orderDetail.isPlan = true;

          // build delivery product
          orderDetail.deliveryDetails = [];
          let deliveryDetail;
          
          orderDetail.PlanCountry.planTrialProducts.forEach(planTrialProduct => {
            let productName = _.findWhere(planTrialProduct.ProductCountry.Product.productTranslate, {langCode: lang}).name;
            deliveryDetail = {
              time: 0,
              isTrial: true,
              productNames: [{productName, qty: planTrialProduct.qty}],
            };
            orderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
          });
          
          orderDetail.PlanCountry.planDetails.forEach(planDetail => {
            let deliverPlan = JSON.parse(planDetail.deliverPlan);
            let times = Object.keys(deliverPlan);
            times.forEach(time => {
              time = parseInt(time);
              let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: lang}).name;
              let productImgUrl = _.findWhere(planDetail.ProductCountry.Product.productImages, {isDefault: true}).url;
              deliveryDetail = _.findWhere(orderDetail.deliveryDetails, {time});
              if(deliveryDetail) {
                let product = _.findWhere(deliveryDetail.productNames, {productName});
                if(product) {
                  product.qty += deliverPlan[time];
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                }
              } else if(deliverPlan[time] !== 0) {
                deliveryDetail = {
                  time,
                  productNames: [{
                    productName,
                    qty: deliverPlan[time],
                    imgUrl: productImgUrl
                  }],
                };
                if(orderDetail.PlanCountry.Plan.isTrial) {
                  deliveryDetail.isFirstTrial = true;
                }
                orderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
              }
            });
          });
          
          // TODO work around for design
          let tmp = [];
          orderDetail.deliveryDetails.forEach(obj => {
            if(obj.time === 1) {
              obj.isFirst = true;
            } else {
              obj.notFirst = true;
            }
            if(obj.time < 3) {
              tmp.push(obj);
            }
          });
          orderDetail.deliveryDetails = tmp;
        }
      });

      // build extra data
      order.hasDiscount = order.Receipt.discountAmount > 0;
      order.hasShippingFee = order.Receipt.shippingFee > 0;
      order.isFreeShipping = parseFloat(order.Receipt.shippingFee) === 0;
      order.hasTaxAmount = order.Receipt.taxAmount > 0;
      order.isPayByStripe = order.paymentType === 'stripe';
      order.isPayByIpay88 = order.paymentType === 'iPay88';
      order.isPayByCash = order.paymentType === 'cash';
      // order.createdAt = moment(new Date(order.createdAt)).format('DD MMM YYYY');
      order.shippingDate = moment(new Date(order.createdAt)).add(1, 'days').format('DD MMM YYYY');
      order.hasShippingAddress = order.DeliveryAddress !== null;
      order.hasBillingAddress = order.BillingAddress !== null;
      order.countryCode = order.Country.code.toLowerCase();

      // detemate ongound order
      order.isOnground = !!order.SellerUser;
      return order;
    });
}

function _getBulkOrderData(orderId) {
  // get order detail
  let order;
  return db.BulkOrder.findOne({
    where: {id: orderId},
    include: [
      'DeliveryAddress',
      'BillingAddress',
      'Country',
      {
        model: db.BulkOrderDetail,
        as: 'bulkOrderDetail',
        include: [
          {
            model: db.ProductCountry,
            include: [
              {
                model: db.Product,
                include: ['productTranslate', 'productImages']
              }
            ]
          },
          {
            model: db.PlanCountry,
            include: [
              {
                model: db.Plan,
                include: ['PlanType', 'planTranslate', 'planImages']
              }, {
                model: db.PlanDetail,
                as: 'planDetails',
                include: [{
                  model: db.ProductCountry,
                  include: [{
                    model: db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }
            ]
          }
        ]
      }
    ]
  })
    .then(_order => {
      order = SequelizeHelper.convertSeque2Json(_order);
      // detemine language
      let lang = order.Country.defaultLang;

      // build order
      order.displayId = OrderHelper.fromatBulkOrderNumber(order);
      order.displayName = 'Guest';

      console.log(`lang ===== ${lang} === ${JSON.stringify(order)}`);
      // build email template data
      order.bulkOrderDetail.forEach(bulkOrderDetail => {
        bulkOrderDetail.totalPrice = 0;

        if(bulkOrderDetail.ProductCountry) {
          bulkOrderDetail.ProductCountry.Product.productTranslate = _.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: lang});
          bulkOrderDetail.ProductCountry.Product.productImages = _.findWhere(bulkOrderDetail.ProductCountry.Product.productImages, {isDefault: true});
          bulkOrderDetail.totalPrice = (bulkOrderDetail.price * bulkOrderDetail.qty).toFixed(2);
          bulkOrderDetail.type = 'ALA-CARTE';
          bulkOrderDetail.isProduct = true;
        }
        if(bulkOrderDetail.PlanCountry) {
          bulkOrderDetail.PlanCountry.Plan.planTranslate = _.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: lang});
          bulkOrderDetail.PlanCountry.Plan.planImages = _.findWhere(bulkOrderDetail.PlanCountry.Plan.planImages, {isDefault: true});
          bulkOrderDetail.unitPrice = (bulkOrderDetail.price * bulkOrderDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
          bulkOrderDetail.totalPrice = (bulkOrderDetail.price * bulkOrderDetail.qty).toFixed(2);
          bulkOrderDetail.type = 'SUBSCRIPTION';
          bulkOrderDetail.isPlan = true;

          // build delivery product
          bulkOrderDetail.deliveryDetails = [];
          let deliveryDetail;
          bulkOrderDetail.PlanCountry.planDetails.forEach(planDetail => {
            let deliverPlan = JSON.parse(planDetail.deliverPlan);
            let times = Object.keys(deliverPlan);
            times.forEach(time => {
              time = parseInt(time);
              let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: lang}).name;
              deliveryDetail = _.findWhere(bulkOrderDetail.deliveryDetails, {time});
              if(deliveryDetail) {
                let product = _.findWhere(deliveryDetail.productNames, {productName});
                if(product) {
                  product.qty += deliverPlan[time];
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                }
              } else if(deliverPlan[time] !== 0) {
                deliveryDetail = {
                  time,
                  productNames: [{productName, qty: deliverPlan[time]}],
                };
                bulkOrderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
              }
            });
          });
          // TODO work around for design
          let tmp = [];
          bulkOrderDetail.deliveryDetails.forEach(obj => {
            if(obj.time === 1) {
              obj.isFirst = true;
            } else {
              obj.notFirst = true;
            }
            if(obj.time < 3) {
              tmp.push(obj);
            }
          });
          bulkOrderDetail.deliveryDetails = tmp;
          console.log(`bulkOrderDetail.deliveryDetails ==== ${JSON.stringify(bulkOrderDetail.deliveryDetails)}`);
        }
      });

      // build extra data
      order.isPayByCash = order.paymentType === 'cash';
      // order.createdAt = moment(new Date(order.createdAt)).format('DD MMM YYYY');
      order.shippingDate = moment(new Date(order.createdAt)).add(1, 'days').format('DD MMM YYYY');
      order.hasShippingAddress = order.DeliveryAddress !== null;
      order.hasBillingAddress = order.BillingAddress !== null;
      order.countryCode = order.Country.code.toLowerCase();

      return order;
    });
}
