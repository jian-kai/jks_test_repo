import Promise from 'bluebird';
import initializeDb from '../../src/db';

const FREE_TRIAL_ID = [
  {old: 20, new: 33},
  {old: 21, new: 34},
  {old: 22, new: 35},
  {old: 23, new: 36},
  {old: 24, new: 39},
  {old: 25, new: 40},
  {old: 26, new: 41},
  {old: 27, new: 42},
  {old: 28, new: 43},
  {old: 29, new: 44},
  {old: 30, new: 45},
  {old: 31, new: 46}
];

const FREE_TRIAL_SKU_OLD = [
  {old: '2-TK3', new: '2-Trial-S3'},
  {old: '3-TK3', new: '3-Trial-S3'},
  {old: '4-TK3', new: '4-Trial-S3'},
  {old: '2-TK5', new: '2-Trial-S5'},
  {old: '3-TK5', new: '3-Trial-S5'},
  {old: '4-TK5', new: '4-Trial-S5'},
  {old: '2-Y-TK3', new: '2-Trial-Y-S3'},
  {old: '3-Y-TK3', new: '3-Trial-Y-S3'},
  {old: '4-Y-TK3', new: '4-Trial-Y-S3'},
  {old: '2-Y-TK5', new: '2-Trial-Y-S5'},
  {old: '3-Y-TK5', new: '3-Trial-Y-S5'},
  {old: '4-Y-TK5', new: '4-Trial-Y-S5'},
];

const FREE_TRIAL_SKU_NEW = [
  {old: '2T-TK3', new: '2-TK3'},
  {old: '3T-TK3', new: '3-TK3'},
  {old: '4T-TK3', new: '4-TK3'},
  {old: '2T-TK5', new: '2-TK5'},
  {old: '3T-TK5', new: '3-TK5'},
  {old: '4T-TK5', new: '4-TK5'},
  {old: '2T-Y-TK3', new: '2-Y-TK3'},
  {old: '3T-Y-TK3', new: '3-Y-TK3'},
  {old: '4T-Y-TK3', new: '4-Y-TK3'},
  {old: '2T-Y-TK5', new: '2-Y-TK5'},
  {old: '3T-Y-TK5', new: '3-Y-TK5'},
  {old: '4T-Y-TK5', new: '4-Y-TK5'},
];


/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      return Promise.mapSeries(FREE_TRIAL_ID, obj => db.Subscription.update({PlanCountryId: obj.new}, {where: {PlanCountryId: obj.old, createdAt: {$gt: '2018-04-26 09:54:04'}}, transaction: t}))
        .then(() => Promise.mapSeries(FREE_TRIAL_SKU_OLD, obj => db.Plan.update({sku: obj.new, slug: obj.new, status: 'removed'}, {where: {sku: obj.old}, transaction: t})))
        .then(() => Promise.mapSeries(FREE_TRIAL_SKU_NEW, obj => db.Plan.update({sku: obj.new, slug: obj.new}, {where: {sku: obj.old}, transaction: t})));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
