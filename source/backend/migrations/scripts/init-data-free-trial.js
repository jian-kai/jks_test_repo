import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

// const countryServiceUrl = 'https://restcountries.eu/rest/v2/all';
const DATA_PATH = '../init-data/';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let mysId = 1;
      let sgpId = 7;
      let mysS3 = 0;
      let mysS5 = 0;
      let sgpS3 = 0;
      let sgpS5 = 0;
      let mysFreePack = 0;
      let mysFreeCream = 0;
      let sgpFreePack = 0;
      let sgpFreeCream = 0;
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)

        .then(() => db.Country.findAll({where: {code: {$in: ['MYS', 'SGP']}}}))
        .then(Countries => {
          Countries = SequelizeHelper.convertSeque2Json(Countries);
          Countries.forEach(country => {
            if(country.code === 'MYS') {
              mysId = country.id;
            }
            if(country.code === 'SGP') {
              sgpId = country.id;
            }
          });
          return db.Product.findAll(
            {
              where: {sku: {$in: ['S3', 'S5', 'PS', 'A5']}}, 
              include: [
                {
                  model: db.ProductCountry,
                  as: 'productCountry',
                  where: {CountryId: {$in: [mysId, sgpId]}}
                }
              ]
            });
        })
        .then(Products => {
          Products = SequelizeHelper.convertSeque2Json(Products);
          Products.forEach(product => {
            product.productCountry.forEach(proCountry => {
              if(proCountry.CountryId === mysId) {
                if(product.sku === 'S3') {
                  mysS3 = proCountry.id;
                  console.log('proCountry.id ============= ', proCountry.id);
                }
                if(product.sku === 'S5') {
                  mysS5 = proCountry.id;
                }
                if(product.sku === 'PS') {
                  mysFreePack = proCountry.id;
                }
                if(product.sku === 'A5') {
                  mysFreeCream = proCountry.id;
                }
              } else {
                if(product.sku === 'S3') {
                  sgpS3 = proCountry.id;
                }
                if(product.sku === 'S5') {
                  sgpS5 = proCountry.id;
                }
                if(product.sku === 'PS') {
                  sgpFreePack = proCountry.id;
                }
                if(product.sku === 'A5') {
                  sgpFreeCream = proCountry.id;
                }
              }
            });
          });

          return;
        })
        .then(() => Promise.mapSeries(['PlanType'],
          table => { // insert new data for PlanType
            let data = require(`${DATA_PATH}FreeTrial_PlanType`).default;
            return db[table].bulkCreate(data, {transaction: t});
          }
        ))
        .then(PlanTypes => Promise.mapSeries(['Plan'],
          table => { // insert new data for Plan
            let data = require(`${DATA_PATH}FreeTrial_Plan`).default;
            PlanTypes = SequelizeHelper.convertSeque2Json(PlanTypes);

            for(let i = 0; i <= 1; i++) {
              data[i].PlanTypeId = PlanTypes[0][0].id;
            }
            for(let i = 2; i <= 3; i++) {
              data[i].PlanTypeId = PlanTypes[0][1].id;
            }
            for(let i = 4; i <= 5; i++) {
              data[i].PlanTypeId = PlanTypes[0][2].id;
            }
            for(let i = 6; i <= 11; i++) {
              data[i].PlanTypeId = PlanTypes[0][3].id;
            }
            return db[table].bulkCreate(data, {transaction: t});
          }
        ))
        .then(Plans => Promise.mapSeries(['PlanImage', 'PlanTranslate', 'PlanCountry'],
          table => { // insert new data for PlanImage, PlanTranslate, PlanCountry
            Plans = SequelizeHelper.convertSeque2Json(Plans);
            let data = require(`${DATA_PATH}FreeTrial_${table}`).default;
            if(table !== 'PlanCountry') {
              for(let i = 0; i < data.length; i++) {
                data[i].PlanId = Plans[0][i].id;
              }
            } else {
              for(let i = 0; i < data.length / 2; i++) {
                data[i].PlanId = Plans[0][i].id;
                data[i].CountryId = mysId;
              }
              for(let i = data.length / 2; i < data.length; i++) {
                data[i].PlanId = Plans[0][i - (data.length / 2)].id;
                data[i].CountryId = sgpId;
              }
            }
            return db[table].bulkCreate(data, {transaction: t});
          }
        ))
        .then(PlanCountries => Promise.mapSeries(['PlanDetail', 'PlanTrialProduct'],
          table => {
            PlanCountries = SequelizeHelper.convertSeque2Json(PlanCountries);
            let data = require(`${DATA_PATH}FreeTrial_${table}`).default;

            for(let i = 0; i < data.length; i++) {
              if(table === 'PlanDetail') {
                if(i < (data.length / 2)) {
                  if((i % 2) === 0) {
                    data[i].ProductCountryId = mysS3;
                  } else {
                    data[i].ProductCountryId = mysS5;
                  }
                } else {
                  if((i % 2) === 0) {
                    data[i].ProductCountryId = sgpS3;
                  } else {
                    data[i].ProductCountryId = sgpS5;
                  }
                }
                data[i].PlanCountryId = PlanCountries[2][i].id;
              } else {
                if(i < (data.length / 2)) {
                  if(i < (data.length / 4)) {
                    data[i].ProductCountryId = mysFreePack;
                    data[i].PlanCountryId = PlanCountries[2][i].id;
                  } else {
                    data[i].ProductCountryId = mysFreeCream;
                    data[i].PlanCountryId = PlanCountries[2][i - (data.length / 4)].id;
                  }
                } else {
                  if(i < (data.length / 4 * 3)) {
                    data[i].ProductCountryId = sgpFreePack;
                    data[i].PlanCountryId = PlanCountries[2][i - (data.length / 4)].id;
                  } else {
                    data[i].ProductCountryId = sgpFreeCream;
                    data[i].PlanCountryId = PlanCountries[2][i - (data.length / 2)].id;
                  }
                }
              }
            }
            return db[table].bulkCreate(data, {transaction: t});
          }
        ))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${JSON.stringify(err)}`);
  });
