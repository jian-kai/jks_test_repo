import initializeDb from '../../src/db';
import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Card.findAll({include: ['User']}))
        .then(cards => {
          cards = SequelizeHelper.convertSeque2Json(cards);
          return Promise.mapSeries(cards, card => {
            if(card.User) {
              return db.Card.update({CountryId: card.User.CountryId}, {where: {id: card.id}, transaction: t}); 
            }
          });
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
