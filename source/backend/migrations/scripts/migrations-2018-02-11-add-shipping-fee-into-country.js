import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `shippingMinAmount` INT(11) NULL DEFAULT \'0\' AFTER `isWebStripe`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `shippingFee` INT(11) NULL DEFAULT \'0\' AFTER `shippingMinAmount`', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Countries` ADD COLUMN `shippingTrialFee` INT(11) NULL DEFAULT \'0\' AFTER `shippingFee`', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `shippingMinAmount` = \'75\', `shippingFee` = \'6.5\', `shippingTrialFee` = \'5\' WHERE `code` = \'MYS\'', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Countries` SET `shippingMinAmount` = \'30\', `shippingFee` = \'5\', `shippingTrialFee` = \'3\' WHERE `code` = \'SGP\'', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
