import Promise from 'bluebird';
import initializeDb from '../../src/db';

const DATA_PATH = '../init-data/';
const INIT_TABLES = [
  'Role',
  'RoleDetail',
  'Path',
];

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('DROP TABLE IF EXISTS `RoleDetails`', null, options))
        .then(() => db.sequelize.query('DROP TABLE IF EXISTS `Roles`', null, options))
        .then(() => db.sequelize.query('DROP TABLE IF EXISTS `Paths`', null, options))
        .then(() => db.Role.sync(options))
        .then(() => db.Path.sync(options))
        .then(() => db.RoleDetail.sync(options))
        .then(() => Promise.mapSeries(INIT_TABLES, 
          table => db[table].truncate({ cascade: true })
        ))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            return db[table].bulkCreate(data);
          }
        ))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` ADD COLUMN `RoleId` INTEGER NULL, ADD CONSTRAINT `Admins_ibfk_3` FOREIGN KEY (`RoleId`) REFERENCES `shave2u`.`Roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE', null, options)
          .then(() => db.sequelize.query('UPDATE `shave2u`.`Admins` SET `RoleId` = 1 WHERE `role` = \'super admin\'', null, options))
          .then(() => db.sequelize.query('UPDATE `shave2u`.`Admins` SET `RoleId` = 2 WHERE `role` = \'admin\'', null, options))
          .then(() => db.sequelize.query('UPDATE `shave2u`.`Admins` SET `RoleId` = 3 WHERE `role` = \'staff\'', null, options))
          .then(() => db.sequelize.query('UPDATE `shave2u`.`Admins` SET `RoleId` = 4 WHERE `role` = \'report\'', null, options))
          // .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` CHANGE COLUMN `role` `role` VARCHAR(50) NULL', null, options))
          .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` DROP COLUMN `role`', null, options))
        )
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` ADD COLUMN `viewAllCountry` TINYINT NULL DEFAULT \'0\' AFTER `isActive`', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Admins` SET `viewAllCountry` = 1 WHERE `RoleId` = 1', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
