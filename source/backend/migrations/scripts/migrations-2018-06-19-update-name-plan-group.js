import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => {
        return db.PlanGroup.findAll({include: ['planGroupTranslate']})
        .then(planGroups => {
          let groupUpdate = [];
          planGroups.forEach(planGroup => {
            groupUpdate.push({id: planGroup.id, name: `TK${planGroup.planGroupTranslate[0].name.replace(/^(.*?)(\d)(.*?)$/, '$2')}`});
          });
          return SequelizeHelper.bulkUpdate(groupUpdate, db, 'PlanGroup', {transaction: t});
        });
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
