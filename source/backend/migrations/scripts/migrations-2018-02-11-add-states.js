import initializeDb from '../../src/db';
const DATA_PATH = '../init-data/';
import Promise from 'bluebird';


const INIT_TABLES = [
  'State',
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.State.sync(options))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            return db[table].bulkCreate(data);
          }
        ))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
