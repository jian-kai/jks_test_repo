import Promise from 'bluebird';
import initializeDb from '../../src/db';
import config from '../../src/config';

const ORDER_LENGTH = 9;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        // check column taxInvoiceNo is exists
        .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'BulkOrders\' AND COLUMN_NAME = \'taxInvoiceNo\'', null, options))
        .then(taxInvoiceNo => {
          if(!taxInvoiceNo[0][0]) {
            db.sequelize.query('ALTER TABLE `shave2u`.`BulkOrders` ADD COLUMN `taxInvoiceNo` VARCHAR(20) NULL DEFAULT NULL', null, options);
          }
        })
        // .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` ADD COLUMN `taxInvoiceNo` VARCHAR(20)', null, options))
        .then(() => db.BulkOrder.findAll({where: {
          $or: [
            {paymentType: 'stripe'},
            {paymentType: 'ipay88'},
            {paymentType: 'cash'},
          ],
          $and: [
            {states: {$not: 'Canceled'}},
            {states: {$not: 'On Hold'}}
          ],
          CountryId: 1
        }, include: ['Country'], transaction: t
        }))
        .then(orders => Promise.mapSeries(orders, (order, idx) => {
          let taxInvoiceNo = `${idx}`;
          let countryCode = config.countryCodes[order.Country.code] || order.Country.code;
          let zeros = '';
          for(let i = 0; i < ORDER_LENGTH - taxInvoiceNo.length; i++) {
            zeros += '0';
          }
          taxInvoiceNo = `S2U-${countryCode}-B-${zeros}${taxInvoiceNo}`;
          console.log(`order: ${taxInvoiceNo}`);
          // update tax invoice for Malaysia
          return db.BulkOrder.update({taxInvoiceNo}, {where: {id: order.id}, transaction: t});
        }))
        .then(() => db.BulkOrder.findAll({where: {
          $or: [
            {paymentType: 'stripe'},
            {paymentType: 'ipay88'},
            {paymentType: 'cash'},
          ],
          $and: [
            {states: {$not: 'Canceled'}},
            {states: {$not: 'On Hold'}}
          ],
          CountryId: 7
        }, include: ['Country'], transaction: t
        }))
        .then(orders => Promise.mapSeries(orders, (order, idx) => {
          let taxInvoiceNo = `${idx}`;
          let countryCode = config.countryCodes[order.Country.code] || order.Country.code;
          let zeros = '';
          for(let i = 0; i < ORDER_LENGTH - taxInvoiceNo.length; i++) {
            zeros += '0';
          }
          taxInvoiceNo = `S2U-${countryCode}-B-${zeros}${taxInvoiceNo}`;
          console.log(`order: ${taxInvoiceNo}`);
          // update tax invoice for Singapore
          return db.BulkOrder.update({taxInvoiceNo}, {where: {id: order.id}, transaction: t});
        }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
