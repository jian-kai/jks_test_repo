import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.User.findAll({
          where: {phone: null},
          include: ['deliveryAddresses', 'orders'],
          transaction: t
        }))
        .then(users => {
          console.log(`usersUpdateList.length === ${users.length}`);
          let usersUpdateList = [];
          users = SequelizeHelper.convertSeque2Json(users);
          users.forEach(user => {
            if(user.deliveryAddresses.length > 0) {
              user.phone = user.deliveryAddresses[0].contactNumber;
              usersUpdateList.push(user);
            } else if(user.orders.length > 0) {
              user.orders.forEach(order => {
                if(order.phone && !user.phone) {
                  user.phone = order.phone;
                  usersUpdateList.push(user);
                }
              });
            }
          });
          console.log(`usersUpdateList.length === ${usersUpdateList.length}`);
          if(usersUpdateList.length > 0) {
            return Promise.map(users, user => db.User.update({phone: user.phone}, {where: {id: user.id}, transaction: t}));
          }
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
