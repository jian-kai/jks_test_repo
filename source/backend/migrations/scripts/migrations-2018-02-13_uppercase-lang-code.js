import initializeDb from '../../src/db';
const DATA_PATH = '../init-data/';
import Promise from 'bluebird';


const INIT_TABLES = [
  'Faq',
  'PlanTranslate',
  'PlanTypeTranslate',
  'Privacy',
  'ProductDetail',
  'ProductTranslate',
  'ProductTypeTranslate',
  'PromotionTranslate',
  'Term',
  'DirectoryCountry',
  'Country',
  'User'
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => Promise.mapSeries(INIT_TABLES, table => db[table].findAll()
          .then(results => {
            console.log(`table === ${table}`);
            return Promise.mapSeries(results, result => {
              if(result.langCode) {
                return db[table].update({langCode: result.langCode.toUpperCase()}, {where: {id: result.id}, transaction: t});
              } else if(result.languageCode) {
                return db[table].update({languageCode: result.languageCode.toUpperCase()}, {where: {id: result.id}, transaction: t});
              } else if(result.defaultLang) {
                return db[table].update({defaultLang: result.defaultLang.toUpperCase()}, {where: {id: result.id}, transaction: t});
              } else if(result.defaultLanguage) {
                return db[table].update({defaultLanguage: result.defaultLanguage.toUpperCase()}, {where: {id: result.id}, transaction: t});
              }
            });
          })))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
