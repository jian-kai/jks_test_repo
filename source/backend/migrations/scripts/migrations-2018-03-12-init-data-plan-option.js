import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

const DATA_PATH = '../init-data/';
const DROP_TABLES = [
  'PlanOptionProduct',
  'PlanOption',
];
const INIT_TABLES = [
  'PlanOption',
  'PlanOptionProduct',
];

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    let planOptionData = [];
    let planOptionProductData = [];
    let productCountries = [];
    let planCountries = [];
    // let planOptions = [];
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0;', null, options)
        .then(() => Promise.mapSeries(DROP_TABLES, 
          table => db[table].drop(options)
        ))
        .then(() => Promise.mapSeries(INIT_TABLES, 
          table => db[table].sync(options)
        ))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            if(table === 'PlanOption') {
              planOptionData = require(`${DATA_PATH}${table}`).default;
            } else {
              planOptionProductData = require(`${DATA_PATH}${table}`).default;
            }
            return;
          }
        ))
        .then(() => db.ProductCountry.findAll(
          {
            include: [
              {
                model: db.Product,
                as: 'Product',
                where: {sku: {$in: ['S3', 'S5', 'A5']}},
                order: 'sku'
              },
              {
                model: db.Country,
                as: 'Country',
                where: {
                  code: {$in: ['MYS', 'SGP']}
                }
              }
            ],
            options
          }
        )
        )
        .then(_productCountries => {
          productCountries = SequelizeHelper.convertSeque2Json(_productCountries);
          return db.PlanCountry.findAll(
            {
              include: [
                {
                  model: db.Plan,
                  as: 'Plan',
                  where: {
                    isTrial: true
                  }
                },
                {
                  model: db.Country,
                  as: 'Country',
                  where: {
                    code: {$in: ['MYS', 'SGP']}
                  }
                }
              ],
              options
            });
        })
        .then(_planCountries => {
          planCountries = SequelizeHelper.convertSeque2Json(_planCountries);
          let temp = [];
          planCountries.forEach(planCountry => {
            planOptionData.forEach(data => {
              let tempData = {};
              tempData.url = data.url;
              tempData.description = data.description;
              tempData.PlanCountryId = planCountry.id;
              tempData.actualPrice = data.actualPrice;
              tempData.sellPrice = data.sellPrice;
              tempData.pricePerCharge = data.pricePerCharge;
              tempData.savePercent = data.savePercent;
              if(planCountry.Country.code === 'MYS') {
                if(planCountry.Plan.sku.indexOf('S5') !== -1) {
                  if(tempData.actualPrice === 42) {
                    tempData.actualPrice = 54;
                    tempData.sellPrice = 54;
                    tempData.pricePerCharge = 54;
                  } else {
                    tempData.actualPrice = 39;
                    tempData.sellPrice = 39;
                    tempData.pricePerCharge = 39;
                  }
                }
              } else {
                if(planCountry.Plan.sku.indexOf('S5') !== -1) {
                  if(tempData.actualPrice === 42) {
                    tempData.actualPrice = 28;
                    tempData.sellPrice = 28;
                    tempData.pricePerCharge = 28;
                  } else {
                    tempData.actualPrice = 22;
                    tempData.sellPrice = 22;
                    tempData.pricePerCharge = 22;
                  }
                } else {
                  if(tempData.actualPrice === 42) {
                    tempData.actualPrice = 18;
                    tempData.sellPrice = 18;
                    tempData.pricePerCharge = 18;
                  } else {
                    tempData.actualPrice = 12;
                    tempData.sellPrice = 12;
                    tempData.pricePerCharge = 12;
                  }
                }
              }
              temp.push(tempData);
            });
          });
          return db.PlanOption.bulkCreate(temp)
          .then(() => 
            db.PlanOption.findAll(
              {
                include: [
                  {
                    model: db.PlanCountry,
                    as: 'PlanCountry',
                    include: [
                      {
                        model: db.Country,
                        as: 'Country'
                      },
                      'Plan'
                    ]
                  }
                ],
                options
              })
          );
        })
        .then(planOptions => {
          planOptions = SequelizeHelper.convertSeque2Json(planOptions);
          let temp = [];
          planOptions.forEach((planOption, index) => {
            let tempData = {};
            tempData.qty = planOptionProductData[0].qty;
            tempData.PlanOptionId = planOption.id;
            if(index % 2 === 0) {
              if(planOption.PlanCountry.Plan.sku.indexOf('S3') !== -1 && planOption.PlanCountry.Plan.sku.indexOf('TK3') !== -1) {
                tempData.ProductCountryId = productCountries.filter(tmp => tmp.Country.code === planOption.PlanCountry.Country.code)[0].id;
              } else {
                tempData.ProductCountryId = productCountries.filter(tmp => tmp.Country.code === planOption.PlanCountry.Country.code)[1].id;
              }
              temp.push(tempData);
              tempData = {};
              tempData.qty = planOptionProductData[0].qty;
              tempData.PlanOptionId = planOption.id;
              tempData.ProductCountryId = productCountries.filter(tmp => tmp.Country.code === planOption.PlanCountry.Country.code)[2].id;
            } else {
              if(planOption.PlanCountry.Plan.sku.indexOf('S3') !== -1 && planOption.PlanCountry.Plan.sku.indexOf('TK3') !== -1) {
                tempData.ProductCountryId = productCountries.filter(tmp => tmp.Country.code === planOption.PlanCountry.Country.code)[0].id;
              } else {
                tempData.ProductCountryId = productCountries.filter(tmp => tmp.Country.code === planOption.PlanCountry.Country.code)[1].id;
              }
            }
            temp.push(tempData);
          });
          return db.PlanOptionProduct.bulkCreate(temp);
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1;', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
