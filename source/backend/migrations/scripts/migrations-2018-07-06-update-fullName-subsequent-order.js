import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import Promise from 'bluebird';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let orders;
      let ordersUpdate = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => 
      db.Order.findAll({where: {
        fullName: null,
        SellerUserId: { $not: null }
      }}))
      .then(_order => {
        orders = SequelizeHelper.convertSeque2Json(_order);
        return Promise.mapSeries(orders, order => {
          if(order.subscriptionIds) {
            return db.Subscription.findAll({where: {
              id: {$in: JSON.parse(order.subscriptionIds)}}
            })
            .then(subscriptions => {
              // order.Subscriptions = subscriptions;
              if(subscriptions[0]) {
                let orderUpdate = {id: order.id, fullName: subscriptions[0].fullname};
                ordersUpdate.push(orderUpdate);
              }
            });
          } else {
            return;
          }
        })
        .then(() => 
          Promise.map(ordersUpdate, item => {
            let _option = Object.assign({where: {id: item.id}}, {transaction: t});
            return db.Order.update(item, _option)
            .then(() => db.SaleReport.update({customerName: item.fullName}, {where: {orderId: item.id}}, {transaction: t}));
          })
        );
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
