import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import _ from 'underscore';
import Sequelize from 'sequelize';

import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

const PRODUCTION_CONNECTION = {
  host: 'shaves2u.mysql.database.azure.com',
  user: 'shaves2u@shaves2u',
  password: 'Y}K7G{Sd(75/<Vq!',
  database: 'shave2u',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
}

function readFilePromise() {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/../../src/config/certs/BaltimoreCyberTrustRoot.crt.pem`, (err, data) => {
      if(err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function initProductionDb() {
  return readFilePromise()
    .then(ca => {
      let db = {};
      const MODEL_PATH = `${__dirname}/../../src/models`;
      let connectionConfig = {
        host: PRODUCTION_CONNECTION.host,
        dialect: 'mysql',
        pool: PRODUCTION_CONNECTION.pool,
        define: {
          timestamps: true
        }
      };
      if(process.env.CONFIG_ENV === 'production') {
        connectionConfig.dialectOptions = {
          encrypt: true,
          ssl: {
            ca
          }
        }
      }
      let sequelize = new Sequelize(PRODUCTION_CONNECTION.database, PRODUCTION_CONNECTION.user, PRODUCTION_CONNECTION.password, connectionConfig);
      return new Promise((resolved, reject) => {
        fs.readdir(MODEL_PATH, (err, files) => {
          if(err) {
            reject(err);
          } else {
            // fetch all files in models folder
            files.forEach(file => {
              if(file.indexOf('.') !== 0 && file !== 'index.js' && file.slice(-3) === '.js') {
                let model = sequelize.import(path.join(`${__dirname}/../../src/models`, file));
                db[model.name] = model;
              }
            });
            // make constrain for model
            Object.keys(db).forEach(function(modelName) {
              if('associate' in db[modelName]) {
                db[modelName].associate(db);
              }
            });
    
            db.sequelize = sequelize;
            db.Sequelize = Sequelize;
            resolved(db);
          }
        });
      });
    });
}

// insert data to new DB
let currentDb;
let productionDb;
let data = {};
initializeDb()
  .then(db => {
    currentDb = db;
    return initProductionDb();
  })
  .then(db => {
    productionDb = db;
    return productionDb.Product.findAll({
      include: [
        'productCountry',
        'productTranslate',
        'productImages',
        'productDetails'
      ]
    });
  })
  .then(products => {
    data.newProducts = SequelizeHelper.convertSeque2Json(products);
    // remove id of new product
    data.newProducts.forEach(product => Reflect.deleteProperty(product, 'id'));
    console.log(`ids ${_.pluck(data.newProducts, 'id')}`);
    return currentDb.Product.findAll({where: {sku: {$in: _.pluck(data.newProducts, 'sku')}}});
  })
  .then(products => {
    data.oldProducts = SequelizeHelper.convertSeque2Json(products);
    console.log(`asdasd ${JSON.stringify(data.newProducts)} == ${data.oldProducts.length}`);
    return currentDb.sequelize.transaction(t => {
      console.log('start transaction');
      return currentDb.Product.destroy({where: {id: {$in: _.pluck(data.oldProducts, 'id')}}, transaction: t})
        .then(() => currentDb.ProductCountry.destroy({where: {ProductId: {$in: _.pluck(data.oldProducts, 'id')}}, transaction: t}))
        .then(() => currentDb.ProductTranslate.destroy({where: {ProductId: {$in: _.pluck(data.oldProducts, 'id')}}, transaction: t}))
        .then(() => currentDb.ProductImage.destroy({where: {ProductId: {$in: _.pluck(data.oldProducts, 'id')}}, transaction: t}))
        .then(() => currentDb.ProductDetail.destroy({where: {ProductId: {$in: _.pluck(data.oldProducts, 'id')}}, transaction: t}))
        .then(() => {
          // remove all id in sub object
          data.newProducts.forEach(product => {
            product.productCountry.forEach(obj => Reflect.deleteProperty(obj, 'id'));
            product.productTranslate.forEach(obj => Reflect.deleteProperty(obj, 'id'));
            product.productImages.forEach(obj => Reflect.deleteProperty(obj, 'id'));
            product.productDetails.forEach(obj => Reflect.deleteProperty(obj, 'id'));
          });
        })
        .then(() => Promise.mapSeries(data.newProducts, product => {
          console.log(`product == ${product.sku}`);
          return currentDb.Product.create(product, {include: ['productCountry', 'productTranslate', 'productImages', 'productDetails'], transaction: t});
        }));
    });
  })
  // .then(productionDb.Plan.findAll({
  //   include: [
  //     'planTranslate',
  //     'planImages',
  //     {
  //       model: productionDb.PlanCountry,
  //       as: 'planCountry',
  //       include: ['planDetails', 'planTrialProducts']
  //     }
  //   ]
  // }))
  // .then(plans => {
  //   data.newPlans = SequelizeHelper.convertSeque2Json(plans);
  //   return currentDb.Plan.findAll({where: {sku: {$in: _.pluck(data.newPlans, 'sku')}}});
  // })
  // .then(plan => {
  //   data.oldPlans = SequelizeHelper.convertSeque2Json(plan);
  //   console.log(`asdasd ${JSON.stringify(data.newPlans)} == ${data.oldPlans.length}`);
  //   return currentDb.sequelize.transaction(t => {
  //     console.log('start transaction');
  //     return currentDb.Product.destroy({where: {id: {$in: _.pluck(data.oldPlans, 'id')}}, transaction: t})
  //       .then(() => currentDb.PlanCountry.destroy({where: {PlanId: {$in: _.pluck(data.oldPlans, 'id')}}, transaction: t}))
  //       .then(() => currentDb.PlanTranslate.destroy({where: {PlanId: {$in: _.pluck(data.oldPlans, 'id')}}, transaction: t}))
  //       .then(() => currentDb.PlanImage.destroy({where: {PlanId: {$in: _.pluck(data.oldPlans, 'id')}}, transaction: t}))
  //       .then(() => {
  //         // remove all id in sub object
  //         data.newPlans.forEach(product => {
  //           product.productTranslate.forEach(obj => Reflect.deleteProperty(obj, 'id'));
  //           product.productImages.forEach(obj => Reflect.deleteProperty(obj, 'id'));
  //           product.productCountry.forEach(obj => {
  //             Reflect.deleteProperty(obj, 'id');
  //             obj.planDetails.forEach(obj1 => Reflect.deleteProperty(obj1, 'id'));
  //             obj.planTrialProducts.forEach(obj1 => Reflect.deleteProperty(obj1, 'id'));
  //           });
  //         });
  //       })
  //       .then(() => Promise.mapSeries(data.newPlans, product => {
  //         console.log(`product == ${product.sku}`);
  //         return currentDb.Product.create(product, {include: [
  //           'planTranslate',
  //           'planImages',
  //           {
  //             model: productionDb.PlanCountry,
  //             as: 'planCountry',
  //             include: ['planDetails', 'planTrialProducts']
  //           }
  //         ], transaction: t});
  //       }));
  //   });
  // })
  .then(() => console.log('Migrate succeed'))
  .catch(err => console.log(err));
