import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import Promise from 'bluebird';
const DROP_TABLES = [
  'PlanOptionTranslate',
];
const INIT_TABLES = [
  'PlanOptionTranslate',
];

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      let planOptionTranslates = [];
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      .then(() => Promise.mapSeries(DROP_TABLES, 
        table => db[table].drop(options)
      ))
      .then(() => Promise.mapSeries(INIT_TABLES, 
        table => db[table].sync(options)
      ))
      .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`PlanOptions` CHANGE COLUMN `description` `description` TEXT NULL', null, options))
      // check column isSubsequentOrder is exists
      .then(() => db.PlanOption.findAll())
      .then(_planOptions => {
        _planOptions = SequelizeHelper.convertSeque2Json(_planOptions);
        _planOptions.forEach(element => {
          planOptionTranslates.push({
            name: element.description,
            langCode: 'EN',
            PlanOptionId: element.id
          });
        });

        return db.PlanOptionTranslate.bulkCreate(planOptionTranslates, {returning: true, transaction: t});
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
