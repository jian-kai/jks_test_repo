import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import _ from 'underscore';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Promotions` ADD COLUMN `isBaApp` TINYINT NULL DEFAULT \'0\'', null, options))
        .then(() => db.PromotionTranslate.findAll({where: {name: 'BA-App promotion'}}))
        .then(promotionTranslates => {
          promotionTranslates = SequelizeHelper.convertSeque2Json(promotionTranslates);
          let promotionIds = _.pluck(promotionTranslates, 'PromotionId');
          return db.Promotion.update({isBaApp: true}, {where: {id: {$in: promotionIds}}, options});
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
