import Promise from 'bluebird';
import moment from 'moment';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import initializeDb from '../../src/db';
import DateTimeHelper from '../../src/helpers/datetime-helpers';
import config from '../../src/config';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({
          where: {
            createdAt: {$gte: new Date(moment('2018-08-06'))},
            states: 'Completed',
            subscriptionIds: {$not: null}
          },
          include: ['orderHistories']
        }))
        .then(orders => {
          orders = SequelizeHelper.convertSeque2Json(orders);
          return Promise.mapSeries(orders, order => {
            // update subscription last delivery date
            let arrPatt = new RegExp(/\[([^,]+,?)+\]/g);
            let completeDate;

            order.orderHistories.forEach(history => {
              if(history.message === 'Completed') {
                completeDate = moment(new Date(moment(history.createdAt))).format('YYYY-MM-DD');
              }
            });

            if(order && order.subscriptionIds && arrPatt.test(order.subscriptionIds) && completeDate) {
              let startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.startFirstDelivery, completeDate);
              return db.Subscription.update(
                {
                  nextDeliverDate: startDeliverDate,
                  nextChargeDate: startDeliverDate,
                  startDeliverDate,
                },
                {
                  where: {
                    id: {$in: JSON.parse(order.subscriptionIds)},
                    startDeliverDate: null
                  }
                }
              );
            }
          });
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
