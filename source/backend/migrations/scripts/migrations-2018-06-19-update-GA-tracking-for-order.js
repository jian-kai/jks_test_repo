import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import Promise from 'bluebird';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // .then(() => db.sequelize.query('UPDATE `shave2u`.`Orders` INNER JOIN `shave2u`.`SaleReports` ON `shave2u`.`Orders`.`id` = `shave2u`.`SaleReports`.`OrderId` AND `shave2u`.`Orders`.`source` <> `shave2u`.`SaleReports`.`source` SET `shave2u`.`Orders`.`source` = `shave2u`.`SaleReports`.`source`, `shave2u`.`Orders`.`medium` = `shave2u`.`SaleReports`.`medium`, `shave2u`.`Orders`.`campaign` = `shave2u`.`SaleReports`.`campaign`, `shave2u`.`Orders`.`term` = `shave2u`.`SaleReports`.`term`, `shave2u`.`Orders`.`content` = `shave2u`.`SaleReports`.`content`', null, options))
      .then(() => db.SaleReport.findAll({where: {orderId: {$not: null}}}))
      .then(saleReports => {
        saleReports = SequelizeHelper.convertSeque2Json(saleReports);
        let orderUpdate = [];
        saleReports.forEach(saleReport => {
          orderUpdate.push(
            {
              id: saleReport.orderId,
              source: saleReport.source,
              medium: saleReport.medium,
              campaign: saleReport.campaign,
              term: saleReport.term,
              content: saleReport.content
            }
          );
        });

        return Promise.map(orderUpdate, item => {
          let _option = Object.assign({where: {id: item.id, source: {ne: item.source}}}, options);
          return db.Order.update(item, _option);
        });
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
