import initializeDb from '../../src/db';
import SaleReportQueueService from '../../src/services/sale-report.queue.service';
import AppContext from '../../src/services/app-context';
import Promise from 'bluebird';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    // init app-context
    global.appContext = new AppContext();
    return Promise.delay(1000);
  })
  .then(() =>
    db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        // .then(() => db.sequelize.query('SELECT `shave2u`.`Orders`.`id`, `shave2u`.`Orders`.`source`, `shave2u`.`Orders`.`medium`, `shave2u`.`Orders`.`campaign`, `shave2u`.`Orders`.`term`, `shave2u`.`Orders`.`content` FROM `shave2u`.`Orders` LEFT JOIN `shave2u`.`SaleReports` ON `shave2u`.`Orders`.`id` = `shave2u`.`SaleReports`.`OrderId` WHERE `shave2u`.`SaleReports`.`OrderId` IS NULL;'))
        .then(() => db.Order.findAll({where: {id: {$notIn: db.sequelize.literal('(SELECT OrderId FROM SaleReports WHERE OrderId IS NOT NULL)')}}}))
        .then(orders => {
          if(orders.length > 0) {
            let saleReportQueueService = SaleReportQueueService.getInstance();
            orders.forEach(order => {
              saleReportQueueService.addTask({
                id: order.id,
                source: order.source,
                medium: order.medium,
                campaign: order.campaign,
                term: order.term,
                content: order.content
              });
            });
          }
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    })
  )
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
