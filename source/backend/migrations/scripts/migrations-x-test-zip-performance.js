import initializeDb from '../../src/db';
// import SequelizeHelper from '../../src/helpers/sequelize-helper';
// import Promise from 'bluebird';
// import zip from 'node-zip';
// import zipdir from 'zip-dir';
// import fs from 'fs';
// import config from '../../src/config';

import childProcess from 'child_process';

const folderDir = '/home/thap-bonsey/01.Works/BonseyJaden/shaves2u/source/backend/src/Tax-Invoice-for-Sales-report';


/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    // // compression file
    // let zipFile = new zip();
    // fs.readdirSync(folderDir).forEach(file => {
    //   // console.log(file);
    //   zipFile.file(file, fs.readFileSync(`${folderDir}/${file}`));
    // });
    
    // let data = zipFile.generate({ base64: false, compression: 'DEFLATE' });

    // // it's important to use *binary* encode
    // return fs.writeFileSync(`${folderDir}/test.zip`, data, 'binary');


    // return new Promise((resolve, reject) => 
    //   zipdir(folderDir, { saveTo: `${folderDir}/test.zip` }, function(err, buffer) {
    //     if(err) reject(err);
    //     resolve(buffer);
    //     // `buffer` is the buffer of the zipped file
    //     // And the buffer was saved to `~/myzip.zip`
    //   })
    // );

    return new Promise((resolve, reject) => 
      childProcess.exec(`cd ${folderDir}
                          tar -zcvf tar-archive-name.tar.gz test`, (err, res) => {
        if(err) reject(err);
        resolve(res);
      })
    );
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
