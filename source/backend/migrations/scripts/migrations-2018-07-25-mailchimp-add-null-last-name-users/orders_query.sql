SELECT o.email,
  u.firstName,
  SUBSTRING(c.code, 1, 2) as 'countryCode',
  (CASE
    WHEN o.SellerUserId IS NULL THEN 'Website'
    ELSE 'BA'
  END) as 'webSource',
  (CASE
    WHEN o.source IS NULL THEN 'direct'
    ELSE o.source
  END) as 'source',
  (CASE
    WHEN o.campaign IS NULL THEN 'none'
    ELSE o.campaign
  END) as 'campaign',
  (CASE
    WHEN o.medium IS NULL THEN 'none'
    ELSE o.medium
  END) as 'medium',
  (CASE
    WHEN o.content IS NULL THEN 'none'
    ELSE o.content
  END) as 'content',
  (CASE
    WHEN o.term IS NULL THEN 'none'
    ELSE o.term
  END) as 'term'
FROM Orders as o
  INNER JOIN Countries as c ON CountryId = c.id
  INNER JOIN Users as u ON UserId = u.id
WHERE u.lastName IS NULL
  AND o.subscriptionIds IS NOT NULL
GROUP BY(o.email)
ORDER BY o.id  DESC