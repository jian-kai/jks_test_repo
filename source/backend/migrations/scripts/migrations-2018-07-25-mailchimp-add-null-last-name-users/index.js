import Promise from 'bluebird';
import fs from 'fs';
import MailchimpHelper from '../../../src/helpers/mailchimp';
import Config from '../../../src/config';

process.env.CONFIG_ENV = process.env.CONFIG_ENV || 'local';

const csvFolderName = process.env.CONFIG_ENV === 'production' ? process.env.CONFIG_ENV : 'staging';
const orderList = [
  {
    name: 'null_last_name_shave_plans_orders',
    type: 'plan'
  },
  {
    name: 'null_last_name_products_orders',
    type: 'product'
  }
];

/*------------------------------------------------------------------------------------------------*/
/*-------------------------------------- functions -----------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
function readFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, fileContent) => {
      if(err) {
        console.log(err);
        reject(`Can not read file: ${filePath}!!!`);
      }

      resolve(fileContent);
    });
  });
}

function writeFile(filePath, fileContent) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, fileContent, function(err) {
      if(err) {
        console.log(err);
        reject(`Can not write file: ${filePath}!!!`);
      }

      resolve();
    });
  });
}

console.log(`INFO: !!! Start migrations mailchimp utm data on ${process.env.CONFIG_ENV}`);
console.log(`INFO: !!! Read csv in folder ${csvFolderName}`);

let mailchimp = MailchimpHelper.getInstance();

Promise.mapSeries(orderList, list => {
  let indexPath = `${__dirname}/${csvFolderName}/${list.name}.txt`;

  console.log(`INFO: !!! Start reading index file: ${indexPath}`);
  return readFile(indexPath)
    .then(indexFileContent => {
      indexFileContent = indexFileContent.toString();
      let csvIndex = indexFileContent ? parseInt(indexFileContent) : -1;

      let csvPath = `${__dirname}/${csvFolderName}/${list.name}.csv`;

      console.log(`INFO: !!! Start reading csv: ${csvPath}`);
      return readFile(csvPath)
        .then(fileContent => {
          // split file content lines
          let csvContentArray = fileContent.toString().replace(/\r/g, '').split(/\r?\n/);
          let rowValue = [];

          return Promise.mapSeries(csvContentArray, (rowData, rowIndex) => {
            // only do the continue
            if(rowData === '' || rowIndex < csvIndex) {
              return '';
            }

            // split row cell
            rowValue = rowData.split(/\,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);
            
            // validate row data
            if(rowValue.length !== 9) {
              console.log(`ERROR ??? rowValue error: ${rowValue}`);
              throw new Error('Csv wrong format!');
            }

            let memberEmail = rowValue[0].replace(/"/g, '');

            let mailchimpBody = {
              'email_address': memberEmail,
              'status': 'subscribed',
              'merge_fields': {
                FNAME: rowValue[1].replace(/"/g, ''),
                COUNTRY: rowValue[2].replace(/"/g, ''),
                SOURCE: rowValue[3].replace(/"/g, ''),
                USOURCE: rowValue[4].replace(/"/g, ''),
                UCAMPAIGN: rowValue[5].replace(/"/g, ''),
                UMEDIUM: rowValue[6].replace(/"/g, ''),
                UCONTENT: rowValue[7].replace(/"/g, ''),
                UTERM: rowValue[8].replace(/"/g, '')
              }
            };

            console.log(`INFO: !!! Start add email: ${memberEmail}`);

            let addToRegister = new Promise(resolve => {
              // add to register

              mailchimp._sendRequest({
                path: `/lists/${Config.email.registerListId}/members`,
                method: 'post',
                body: mailchimpBody
              })
              .then(() => {
                console.log('-------------------------------------------');
                console.log(`| Done add to register: ${memberEmail}!!! |`);
                console.log('-------------------------------------------');
                resolve();
              })
              .catch(err => {
                console.log(`~~~~~~~~~~ Add to register error: ${memberEmail} | ${err.detail ? err.detail : JSON.stringify(err)} ~~~~~~~~~~~~`);
                resolve();
              });
            });

            return addToRegister.then(() => {
              if(list.type == 'plan') {
                // add to trial list
                return mailchimp._sendRequest({
                  path: `/lists/${Config.email.trialSubscriberList}/members`,
                  method: 'post',
                  body: mailchimpBody
                })
                .then(() => {
                  console.log('----------------------------------------');
                  console.log(`| Done add to trial: ${memberEmail}!!! |`);
                  console.log('----------------------------------------');
                  writeFile(indexPath, rowIndex);
                })
                .catch(err => {
                  console.log(`~~~~~~~~~~ Add to trial error: ${memberEmail} | ${err.detail ? err.detail : JSON.stringify(err)} ~~~~~~~~~~~~`);
                  writeFile(indexPath, rowIndex);
                });
              } else {
                writeFile(indexPath, rowIndex);
              }
            });
          }); // end csv row map series
        }) // end read csv
        .catch(error => {
          console.log(error);
        });
    }) // end read index
    .catch(error => {
      console.log(error);
    });
})
.then(() => {
  console.log('INFO: !!! Mailchimp utm data updated!!!');
})
.catch(err => {
  console.log(`ERROR ??? Can't update mailchimp utm data ${err.stack || JSON.stringify(err)}`);
});
