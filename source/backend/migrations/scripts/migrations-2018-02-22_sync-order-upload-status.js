import Promise from 'bluebird';
import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import _ from 'underscore';
import moment from 'moment';
import { aftership } from '../../src/helpers/aftership';
import OrderHelper from '../../src/helpers/order-helper';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.OrderUpload.findAll())
        .then(orders => Promise.mapSeries(orders, order => {
          console.log(`order == ${JSON.stringify(order)}`);
          let orderId;
          if(order.orderNumber[2] === 'B') {
            orderId = OrderHelper.getBulkOrderNumber(order.orderNumber);
            return db.BulkOrder.findOne({where: {id: orderId}})
              .then(obj => {
                if(obj) {
                  return db.OrderUpload.update({
                    status: obj.states,
                    deliveryId: obj.deliveryId,
                    carrierAgent: obj.carrierAgent
                  }, {where: {id: order.id}, transaction: t});
                }
              });
          } else {
            orderId = OrderHelper.getOrderNumber(order.orderNumber);
            return db.BulkOrder.findOne({where: {id: orderId}})
              .then(obj => {
                if(obj) {
                  return db.OrderUpload.update({
                    status: obj.states,
                    deliveryId: obj.deliveryId,
                    carrierAgent: obj.carrierAgent
                  }, {where: {id: order.id}, transaction: t});
                }
              });
          }
        }))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
