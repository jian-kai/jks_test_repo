import initializeDb from '../../src/db';
import SequelizeHelper from '../../src/helpers/sequelize-helper';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
      // check column name is exists
      .then(() => db.sequelize.query('SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = \'shave2u\' AND TABLE_NAME = \'PlanGroups\' AND COLUMN_NAME = \'name\'', null, options))
      .then(name => {
        if(!name[0][0]) {
          db.sequelize.query('ALTER TABLE `shave2u`.`PlanGroups` ADD COLUMN `name` VARCHAR(50) NOT NULL', null, options);
        }
      })
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
