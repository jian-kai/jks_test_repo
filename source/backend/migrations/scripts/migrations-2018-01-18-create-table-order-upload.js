import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('DROP TABLE IF EXISTS `OrderUploads`', null, options))
        .then(() => db.sequelize.query('CREATE TABLE IF NOT EXISTS `OrderUploads` (`id` INTEGER auto_increment , `doFileName` VARCHAR(50), `wareHouseFileName` VARCHAR(50), `orderNumber` VARCHAR(50), `orderId` INTEGER(10) ZEROFILL, `bulkOrderId` INTEGER(10) ZEROFILL, `sku` VARCHAR(64), `qty` INTEGER DEFAULT 0, `deliveryId` VARCHAR(30), `carrierAgent` VARCHAR(30), `status` ENUM(\'Pending\', \'On Hold\', \'Payment Received\', \'Processing\', \'Delivering\', \'Completed\', \'Canceled\') DEFAULT \'On Hold\', `region` VARCHAR(5), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', null, options))
        .then(() => db.sequelize.query('SHOW INDEX FROM `OrderUploads`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
