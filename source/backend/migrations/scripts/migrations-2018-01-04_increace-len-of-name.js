import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Users` CHANGE COLUMN `firstName` `firstName` VARCHAR(100) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Users` CHANGE COLUMN `lastName` `lastName` VARCHAR(100) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` CHANGE COLUMN `fullname` `fullname` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SellerUsers` CHANGE COLUMN `agentName` `agentName` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` CHANGE COLUMN `agentName` `agentName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` CHANGE COLUMN `customerName` `customerName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Orders` CHANGE COLUMN `fullName` `fullName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`DirectoryCountries` CHANGE COLUMN `name` `name` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`DirectoryCountries` CHANGE COLUMN `nativeName` `nativeName` VARCHAR(200) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`DeliveryAddresses` CHANGE COLUMN `firstName` `firstName` VARCHAR(100) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`DeliveryAddresses` CHANGE COLUMN `lastName` `lastName` VARCHAR(100) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`DeliveryAddresses` CHANGE COLUMN `fullName` `fullName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Cards` CHANGE COLUMN `cardName` `cardName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`BulkOrders` CHANGE COLUMN `fullName` `fullName` VARCHAR(200) NULL DEFAULT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` CHANGE COLUMN `firstName` `firstName` VARCHAR(100) NOT NULL', null, options))
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Admins` CHANGE COLUMN `lastName` `lastName` VARCHAR(100) NOT NULL', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
