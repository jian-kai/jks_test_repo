import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`SaleReports` ADD COLUMN `CountryId` INT(11) NOT NULL', null, options))
        .then(() => db.SaleReport.update({CountryId: 1}, {where: {region: 'MYS'}, transaction: t}))
        .then(() => db.SaleReport.update({CountryId: 7}, {where: {region: 'SGP'}, transaction: t}))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
