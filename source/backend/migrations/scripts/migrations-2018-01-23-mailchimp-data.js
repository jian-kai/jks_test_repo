import Promise from 'bluebird';
import initializeDb from '../../src/db';
import mailchimpHelper from '../../src/helpers/mailchimp';

let db; 

initializeDb()
  .then(_db => { 
    db = _db;
    let UsersData; 
    let listId = 'a319039f76';
    let mailchimp = new mailchimpHelper();

    return mailchimp.getMembers(listId)
      .then(members => Promise.mapSeries(members, member => Promise.delay(10)
        .then(() => db.User.findOne({where: {email: member.email_address}, include: ['Country']}))
        .then(user => {
          if(user) {
            let data = {
              'merge_fields': {
                FNAME: user.firstName,
                LNAME: user.lastName,
                COUNTRY: user.Country.code.substr(0, 2),
                SOURCE: user.badgeId ? 'BA app' : 'Website'
              }
            };
            return mailchimp._sendRequest({
              path: `/lists/${listId}/members/${member.id}`,
              method: 'patch',
              body: data
            });
          } else {
            return mailchimp._sendRequest({
              path: `/lists/${listId}/members/${member.id}`,
              method: 'delete'
            });
          }
        })
      ));

    // return db.User.findAll({    
    //   where: {
    //     isActive: 1,
    //     updatedAt: {$lt: '2017-11-15 02:53:32'}
    //   }
    //   //limit : 10
    // })      
    //   .then(users => {
    //     UsersData = users;    
    //     return Promise.mapSeries(UsersData, user => Promise.delay(1000).then(() => {
    //       let options = {
    //         'email_address': user.dataValues.email,
    //         'status': 'subscribed',
    //         'merge_fields': {
    //           FNAME: user.dataValues.firstName,
    //           LNAME: user.dataValues.lastName
    //         }
    //       }        
    //       mailchimp.createMember(listId, options);
    //     }));
    //   });
  })
  .then(() => {
    console.log('Create mailchimp list are success!!!');
  })
  .catch(err => {
    console.log(`Create mailchimp list ${err.stack || JSON.stringify(err)}`);
  });
