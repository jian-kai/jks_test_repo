import Promise from 'bluebird';
import SequelizeHelper from '../../src/helpers/sequelize-helper';
import initializeDb from '../../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */

initializeDb()
  .then(db => { // truncate all data
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.Order.findAll({where: {
          SellerUserId: {$not: null}
        }}))
        .then(orders => {
          orders = SequelizeHelper.convertSeque2Json(orders);
          return Promise.mapSeries(orders, order => db.SaleReport.update({
            customerName: order.fullName,
            billingContact: order.phone,
            deliveryContact: order.phone
          }, {where: {orderId: order.id}, transaction: t}));
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
