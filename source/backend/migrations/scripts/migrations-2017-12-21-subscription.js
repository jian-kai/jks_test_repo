import initializeDb from '../../src/db';

let db;

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => db.sequelize.query('ALTER TABLE `shave2u`.`Subscriptions` ADD COLUMN `pricePerCharge` DECIMAL(10,2) NOT NULL AFTER `totalChargeTimes`', null, options))
        .then(() => db.sequelize.query('UPDATE `shave2u`.`Subscriptions` INNER JOIN `shave2u`.`PlanCountries` ON `shave2u`.`Subscriptions`.`PlanCountryId` = `shave2u`.`PlanCountries`.`id` SET `shave2u`.`Subscriptions`.`pricePerCharge` = `shave2u`.`PlanCountries`.`sellPrice` / `shave2u`.`Subscriptions`.`totalChargeTimes`', null, options))
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
