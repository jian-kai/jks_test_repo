import Promise from 'bluebird';
import { awsHelper } from '../src/helpers/aws-helper';
import config from '../src/config';

const IMAGE_PATH = '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/migrations/init-data/productImages/';
const IMAGE_NAME = [
  'men-starter-pack-main_1.jpg',
  'men-starter-pack-main.jpg',
  'men-handle-main_1.jpg',
  'men-premium-razor-handle-01.jpg',
  'men-premium-razor-handle-02.jpg',
  'men-s3-blade-05_1_3.jpg',
  'men-s3-blade-main_2.jpg',
  's3-cartridge.jpg',
  'men-s5-blade-05.jpg',
  'men-s5-blade-main.jpg',
  's5-cartridge.jpg',
  'product_photo-1.jpg',
  'product_photo-2.jpg',
  'product_photo-3.jpg',
  'women-starter-pack-main_1.jpg',
  'women-starter-pack-main.jpg',
  'women-premium-handle-main.jpg',
  'women-premium-razor-handle-02.jpg',
  'women-premium-razor-handle-03.jpg',
  'women-blade-f5-05.jpg',
  'women-blade-main.jpg',
  'women-f5-cassettes_1.jpg',
  'v1.0_s2u_arko_product_1800px-3.jpg',
  'v1.0_s2u_arko_product_1800px-4.jpg',
  'v1.0_s2u_arko_product_1800px-5.jpg',
  'arko-premium-shaving-foam-01.jpg',
  'arko-premium-shaving-gel-01.jpg',
  'arko-premium-shaving-after-shave-cream-01.jpg',
  'arko-premium-shaving-after-shave-cream-03.jpg'
];

Promise.mapSeries(IMAGE_NAME, fileName => awsHelper.upload(config.AWS.productImagesAlbum, {
  path: `${IMAGE_PATH}${fileName}`,
  originalFilename: fileName
})
  .then(uploadFile => {
    console.log(`uploaded file ${uploadFile.Location}`);
    return uploadFile;
  })
)
  .then(uploadFiles => {
    console.log('Finish upload file finish');
    uploadFiles.forEach((file, idx) => {
      console.log(`File: ${file.Location}, ${IMAGE_NAME[idx]}`);
    });
  })
  .catch(err => console.log(`ERR: ${err.stack}`));
