SELECT
sub.email,
CONCAT(da.firstName, ' ', da.lastName) as `Customer Name`,
sub.discountPercent,
sub.pricePerCharge as `Price`,
sub.promoCode,
ADDTIME(sub.createdAt, '08:00:00') as `Subscribe Date`,
pl.sku,
sub.status
FROM 
shave2u.subscriptions  as sub
inner join users as u on u.id = sub.UserId
inner join deliveryaddresses as da on da.id = sub.DeliveryAddressId
inner join plancountries as plc on plc.id = sub.PlanCountryId
inner join plans as pl on pl.id = plc.PlanId
where sub.promoCode in ('S2USPCQDR','S2USPCPOP','S2USPCPPP','S2USPCOPL','S2USPCLOL','S2USPCLOO','S2USPCASD','S2USPCQWE','S2USPCMNB','S2USPCFGH','S2USPCICP','S2USPCTUP','S2USPCDNP','S2USPCTTY','S2USPCMSE','S2USPCOBW','S2USPCIII','S2USPCWGC','S2USPCNTM','S2USPCNIM','S2USPCVMT','S2USPCFEH','S2USPCHLC','S2USPCGOB','S2USPCHLK','S2USPCVBN','S2USPCTYJ','S2USPCZMQ','S2USPCQPG','S2USPCGKD');