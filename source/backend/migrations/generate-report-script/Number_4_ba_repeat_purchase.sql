select u.*, 
(select createdAt  from orders as o where o.createdAt >= '2018-04-28 16:00:00' and 
		o.UserId = u.id AND 
        o.SellerUserId is not null AND
        o.subscriptionIds is null AND
        (
			(o.updatedAt > ADDTIME(o.createdAt, '01:00:00') AND o.states = 'canceled') OR
            o.states <> 'canceled'
		) limit 1) as `First Order Date`,
(select count(*) from orders as o 
	where o.createdAt >= '2018-04-28 16:00:00' and 
		o.UserId = u.id AND 
        o.SellerUserId is not null AND
        o.subscriptionIds is null AND
        (
			(o.updatedAt > ADDTIME(o.createdAt, '01:00:00') AND o.states = 'canceled') OR
            o.states <> 'canceled'
		)
) as `Ala carte`,
(select count(DISTINCT o.subscriptionIds) from orders as o 
	where o.createdAt >= '2018-04-28 16:00:00' and 
		o.SellerUserId is not null AND
		o.UserId = u.id AND o.subscriptionIds is not null AND
        (
			(o.updatedAt > ADDTIME(o.createdAt, '01:00:00') AND o.states = 'canceled') OR
            o.states <> 'canceled'
		)
) as `Subscription`
from users as u
where 
(select count(*) from orders as o 
	where o.createdAt >= '2018-04-28 16:00:00' and 
		o.UserId = u.id AND 
        o.SellerUserId is not null AND
        o.subscriptionIds is null AND
        (
			(o.updatedAt > ADDTIME(o.createdAt, '01:00:00') AND o.states = 'canceled') OR
            o.states <> 'canceled'
		)
) > 0 or
(select count(DISTINCT o.subscriptionIds) from orders as o 
	where o.createdAt >= '2018-04-28 16:00:00' and 
		o.SellerUserId is not null AND
		o.UserId = u.id AND o.subscriptionIds is not null AND
        (
			(o.updatedAt > ADDTIME(o.createdAt, '01:00:00') AND o.states = 'canceled') OR
            o.states <> 'canceled'
		)
) > 0