select 
(concat(
	(case
		when o.CountryId = 1 then 'MY'
        when o.CountryId = 7 then 'SG'
    end), o.id, '-', DATE_FORMAT(ADDTIME(o.createdAt, '08:00:00'), '%Y%m%d')
)) as `Order No`,
o.id as `ID`,
o.states as `State`,
(case
	when (od.ProductCountryId is not null) then p.sku
    when (od.PlanCountryId is not null) then pl.sku
end) as `SKU`,
(case
	when (od.ProductCountryId is not null) then 'A La Carte'
    when (od.PlanCountryId is not null and pl.isTrial) then 'Trial'
    else 'Shave Plan'
end) as `Type`,
(case
	when o.medium is not null then CONCAT(o.source, '/', o.medium)
    else CONCAT(o.source, '/none')
end) as `Source`,
o.email as `Email`,
od.qty as `Units`,
od.price as `Unit Price`,
(od.qty * od.price) as `Total`,
od.currency as `Currency`,
o.paymentType as `Payment Type`,
c.code as `Region`,
(case
	when o.SellerUserId is not null then 'BA App'
    else 'E-commerce'
end) as `Category`,
DATE_FORMAT(ADDTIME(o.createdAt, '08:00:00'), '%b %d, %Y') as `Sale Date`
from orders as o
inner join countries as c on c.id = o.CountryId
inner join orderdetails as od on od.OrderId = o.id
left join productcountries as pc on pc.id = od.ProductCountryId
left join products as p on p.id = pc.ProductId
left join plancountries as plc on plc.id = od.PlanCountryId
left join plans as pl on pl.id = plc.PlanId
where 
o.createdat > '2017-11-14';