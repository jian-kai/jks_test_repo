SELECT sub.*, pl.sku,
sub.pricePerCharge as `Old Price`,
(
	select pc.sellPrice from plandetails as pd
    inner join productcountries as pc on pd.ProductCountryId = pc.id
    where pd.PlanCountryId = plc.id limit 1
) as `New Price`
FROM shave2u.subscriptions as sub
inner join plancountries as plc on plc.id = sub.PlanCountryId
inner join plans as pl on pl.id = plc.PlanId
where sub.PlanCountryId in (7, 8, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58) AND sub.status IN ('Processing')
AND plc.CountryId = 1;