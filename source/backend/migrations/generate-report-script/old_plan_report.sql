select 
sub.id,
sub.email,
sub.pricePerCharge as `Old Price`,
(case
	when sub.PlanOptionId is null then pc.pricePerCharge
    else po.pricePerCharge
end) as `New Price`,
sub.createdAt,
sub.nextChargeDate,
sub.status
from subscriptions as sub 
left join plancountries as pc on pc.id = sub.PlanCountryId
left join planoptions as po on po.id = sub.PlanOptionId
where
sub.isTrial = true and (sub.status = 'Processing' or sub.status = 'On Hold') and
(
	(sub.pricePerCharge <> pc.pricePerCharge AND sub.PlanOptionId is null and pc.CountryId = 7) or
    (sub.pricePerCharge <> po.pricePerCharge AND sub.PlanOptionId is not null and pc.CountryId = 7)
)
order by sub.nextChargeDate;