SELECT
    (case 
        when (c.id is not null) then c.code
        when (cc.id is not null) then cc.code
    END)  as `Location`,
    (case when (u.isActive = true)
        then
            ADDTIME(u.createdAt, '08:00:00')
        else
            null
    END)  as `User signup Date`,
    sub.email,
    (case when (sub.SellerUserId is not null)
        then
            sub.fullname
        else
            CONCAT(da.firstName, ' ', da.lastName)
    END)  as `Customer Name`,
    (case when (sub.SellerUserId is not null)
        then
            sub.phone
        else
            da.contactNumber
    END)  as `Phone`,
    
    (select GROUP_CONCAT(ADDTIME(ord.updatedAt, '08:00:00') SEPARATOR ', ') from orders as ord where ord.subscriptionIds = CONCAT('[', sub.id, ']') ) as `Package`,
    (select GROUP_CONCAT(ADDTIME(ord.createdAt, '08:00:00') SEPARATOR ', ') from orders as ord where ((sub.isTrial = true and ord.createdAt > ADDTIME(sub.createdAt, '01:00:00')) OR sub.isCustom = true) AND ord.subscriptionIds = CONCAT('[', sub.id, ']') ) as `Paid`,

    (case 
        when (sub.currentDeliverNumber = 0 AND sub.status <> 'Canceled') then 'Currently Trial Period'
        when (sub.currentDeliverNumber > 0 AND sub.status <> 'Canceled' AND sub.isTrial = true) then 'Active Customer (From Free Trial)'
        when (sub.status <> 'Canceled' AND sub.isCustom = true) then 'Active Customer (Without Free Trial)'
        when (sub.status = 'Canceled' AND sub.isCustom = true) then 'Cancelled (Without Free Trial)'
        when (sub.currentDeliverNumber = 0 AND sub.status = 'Canceled' AND sub.isTrial = true) then 'Cancelled (During Free Trial)'
        when (sub.currentDeliverNumber > 0 AND sub.status = 'Canceled' AND sub.isTrial = true) then 'Cancelled (After Free Trial)'
    END)  as `User Status`,
    ADDTIME(sub.createdAt, '08:00:00') as `Order Date`,
    (case when (sub.status = 'Canceled')
        then
            sub.updatedAt
        else
            null
    END)  as `Canceled Date`,
    (case
        when p.sku is not null then p.sku
        when cp.id is not null then cp.description
    END) as `Shave Plan SKU`,
    da.address as `Location`,
    (case
        when su.id is not null then 'BA'
        when su.id is null then 'Ecommece'
    END) as `Source`,
    (select CONCAT(ord.source, '/', ord.medium) from orders as ord where ord.subscriptionIds = CONCAT('[', sub.id, ']') limit 1) as `Referring Channel (GA)`,
    (
    Case when ((select count(*) from orders as ord where ord.UserId = sub.UserId AND ord.createdAt > '2017-11-14') > 0)
        then 
            'True'
        else 
            'False'
    END) as `A La Carte User?` 
FROM subscriptions as sub
    inner join users as u on u.id = sub.UserId
    inner join deliveryaddresses as da on da.id = sub.DeliveryAddressId
    left join plancountries as pc on pc.id = sub.PlanCountryId
    left join plans as p on p.id = pc.PlanId
    left join customplans as cp on cp.id = sub.CustomPlanId
    left join countries as c on c.id = pc.CountryId
    left join countries as cc on cc.id = cp.CountryId
    left join sellerusers as su on su.id = sub.SellerUserId

where 
sub.createdAt > '2018-01-16' AND sub.createdAt < '2018-06-25 16:00:00' AND
sub.status <> 'On Hold' AND 
(sub.status <> 'Canceled' OR (sub.status = 'Canceled' AND ADDTIME(sub.createdAt, '00:00:05') < sub.updatedAt))
ORDER BY sub.createdAt;