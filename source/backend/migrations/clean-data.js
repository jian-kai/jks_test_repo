import Promise from 'bluebird';
import initializeDb from '../src/db';
import request from 'request';

const countryServiceUrl = 'https://restcountries.eu/rest/v2/all';
const DATA_PATH = './init-data/';
const INIT_TABLES = [
  'User',
  'CartDetail',
  'Cart',
  'Card',
  'DeliveryAddress',
  'NotificationType',
  'OrderDetail',
  'Receipt',
  'Order',
  'SaleReport',
  'Subscription',
  'UsedToken'
];

let db;

function getCountryDetails() {
  return new Promise((resolve, reject) => {
    let options = {
      url: countryServiceUrl,
      method: 'GET'
    };
    request(options, (error, response, body) => {
      if(!error && response.statusCode === 200) {
        resolve(JSON.parse(body));
      } else {
        reject(error);
      }
    });
  });
}

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => Promise.mapSeries(INIT_TABLES, 
          table => db[table].truncate({ cascade: true })
        )).then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('clean data is success!!!');
  })
  .catch(err => {
    console.log(`can't clean data ${err.stack || JSON.stringify(err)}`);
  });
