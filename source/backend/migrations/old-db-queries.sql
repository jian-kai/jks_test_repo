# Attribute 
SELECT *  FROM  eav_attribute;

# PRODUCT ENTITY
SELECT * FROM catalog_product_entity;

# CUSTOMER ENTIRY
# main customer info
SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at,
        cev.attribute_id, cev.value
    FROM customer_entity ce
    INNER JOIN customer_entity_varchar cev ON ce.entity_id = cev.entity_id;

SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at,
        cei.attribute_id, cei.value
    FROM customer_entity ce
    INNER JOIN customer_entity_int cei ON ce.entity_id = cei.entity_id;

SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at,
        ced.attribute_id AS dAttribute_id, ced.value AS dValue
    FROM customer_entity ce
    INNER JOIN customer_entity_datetime ced ON ce.entity_id = ced.entity_id;

# facebook customer
SELECT * FROM  belvg_facebook_customer

# customer address info
SELECT * FROM  customer_address_entity cae
    INNER JOIN customer_address_entity_int caei ON cae.entity_id = caei.entity_id;

# ORDER ENTITY
SELECT * FROM sales_flat_order;
SELECT * FROM sales_flat_order_address;
SELECT * FROM sales_flat_order_grid;
SELECT * FROM sales_flat_order_item;
SELECT * FROM sales_flat_order_payment;
SELECT * FROM sales_flat_order_status_history;