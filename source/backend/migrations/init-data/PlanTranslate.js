export default [
  {
    id: 1,
    PlanId: 1,
    name: 'S3 - Annual',
    description: '- RM220 per annum.\n - 4 deliveries / year.\n - First delivery: 2 X S3 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S3 Cassettes.\n - FREE delivery. \n Normal cost: RM277 - Save RM57.',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 2,
    PlanId: 2,
    name: 'S5 - Annual',
    description: '- RM280 per annum.\n - 4 deliveries / year.\n- First delivery: 2 X S5 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S5 Cassettes.\n - FREE delivery. \n Normal cost: RM373 - Save RM93.',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 3,
    PlanId: 3,
    name: 'S3 - Quarterly',
    description: '- RM220 per annum (4 payments of RM55 every quarter).\n - 4 deliveries / year.\n - First delivery: 2 X S3 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S3 Cassettes.\n - FREE delivery.\n - Credit card is required. \n Normal cost: RM277 - Save RM57',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 4,
    PlanId: 4,
    name: 'S5 - Quarterly',
    description: '- RM280 per annum (4 payments of RM70 every quarter).\n - 4 deliveries / year.\n - First delivery: 2 X S5 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S5 Cassettes.\n - FREE delivery.\n - Credit card is required. \n Normal cost: RM373 - Save RM93',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 5,
    PlanId: 5,
    name: 'F5 - Annual',
    description: '- RM300 per annum.\n - 4 deliveries / year.\n - First delivery: 2 X F5 Cassettes (8 Cartridges), FREE one Premium Handle.\n - Subsequent deliveries: 2 X F5 Cassettes.\n - FREE delivery. \n Normal cost: RM399 - Save RM99.',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 6,
    PlanId: 6,
    name: 'F5 - Quarterly',
    description: '- RM300 per annum (4 payments of RM75 every quarter).\n - 4 deliveries / year.\n - First delivery: 2 X F5 Cassettes (8 Cartridges), FREE one Premium Handle.\n - Subsequent deliveries: 2 X F5 Cassettes.\n - FREE delivery.\n - Credit card is required. \n Normal cost: RM399 - Save RM99',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 7,
    PlanId: 7,
    name: 'S3 - Monthly',
    description: '- RM277 per annum (12 payments of RM23.1 every quarter).\n - 4 deliveries / year.\n - First delivery: 2 X S3 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S3 Cassettes.\n - FREE delivery.\n - Credit card is required. \n Normal cost: RM277',
    langCode: 'EN',
    isDefault: true
  },
  {
    id: 8,
    PlanId: 8,
    name: 'S5 - Monthly',
    description: '- RM373 per annum (12 payments of RM31.1 every month).\n - 4 deliveries / year.\n - First delivery: 2 X S5 Cassettes (8 Cartridges), FREE one Premium Handle and one can of Shaving Foam.\n - Subsequent deliveries: 2 X S5 Cassettes.\n - FREE delivery.\n - Credit card is required. \n Normal cost: RM373',
    langCode: 'EN',
    isDefault: true
  },
];
