
export default [
  {
    id: 1,
    ProductTypeId: 1,
    langCode: 'EN',
    name: 'Awesome Shave Kit',
  },
  {
    id: 2,
    ProductTypeId: 2,
    langCode: 'EN',
    name: 'Starter Packs',
  },
  {
    id: 3,
    ProductTypeId: 3,
    langCode: 'EN',
    name: 'Handles',
  },
  {
    id: 4,
    ProductTypeId: 4,
    langCode: 'EN',
    name: 'Cartridge Packs',
  },
  {
    id: 5,
    ProductTypeId: 5,
    langCode: 'EN',
    name: 'Skin Care',
  },
  {
    id: 6,
    ProductTypeId: 6,
    langCode: 'EN',
    name: 'Sample',
  }
];
