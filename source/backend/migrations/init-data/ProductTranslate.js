
export default [
  {
    id: 1,
    ProductId: 1,
    langCode: 'EN',
    name: 'Men’s Starter Pack',
    description: 'Sample our 3 and 5 blade cartridges to find which suits you best.\n - 1 x Razor Handle - fits both 3 and 5 blade cartridges.\n - 1 x Men’s S3 cartridge.\n - 1 x Men’s S5 cartridge.\n - Packaged in ultra-secure, hygienic packaging.'
  },
  {
    id: 2,
    ProductId: 2,
    langCode: 'EN',
    name: 'Men’s Razor Handle',
    description: '- Crafted metal handle with soft rubber grip and chrome finish.\n - Fits both S3 and S5 cartridges.\n - Precision balanced grip for smooth shaving.\n - A beautiful looking handle that has great heft.'
  },
  {
    id: 3,
    ProductId: 3,
    langCode: 'EN',
    name: 'Men’s S3 Cartridges',
    description: '- An awesome shave at an awesome price.\n - 3 Blade razor cartridge with aloe vera lubricating strip.\n - 4 Cartridges per cassette.\n - Ceramic coated blades for a cleaner and crisper shave.\n - Open blade architecture design for easy rinsing and no clogging.\n - Large aloe vera lubrication strip soothes sensitive skin.'
  },
  {
    id: 4,
    ProductId: 4,
    langCode: 'EN',
    name: 'Men’s S5 Cartridges',
    description: '- Our best shave at an awesome price.\n - 5 Blade razor with aloe vera lubricating strip and trimmer blade.\n - 4 Cartridges per cassette.\n - Ceramic coated blades for a cleaner and crisper shave.\n - Open blade architecture design for easy rinsing and no clogging.\n - Larger aloe vera lubrication strip soothes sensitive skin.'
  },
  {
    id: 5,
    ProductId: 5,
    langCode: 'EN',
    name: 'Awesome Shave Kit',
    description: 'Your Awesome Shave Kit contains a great grippy sturdy handle, a pack of each super ceramic coated 5 blade razors & 3 blade razors, cooling shave cream and soothing after shave cream. Basically the best of our goodies from www.shaves2u.com - hand-picked, packed & delivered 2 U with love.'
  },
  {
    id: 6,
    ProductId: 6,
    langCode: 'EN',
    name: 'Women’s Starter Pack',
    description: '- Sample our cartridges before buying in bulk.\n - 1 x Women’s Razor Handle.\n - 2 x Women’s F5 cartridges.\n - Packaged in ultra-secure, hygienic packaging.'
  },
  {
    id: 7,
    ProductId: 7,
    langCode: 'EN',
    name: 'Women’s Razor Handle',
    description: '- Ergonomically crafted handle with soft rubber grip.\n - Precision balanced grip for added confidence.\n - Rubberized finger rest.\n - Push button release with arrow indicator.'
  },
  {
    id: 8,
    ProductId: 8,
    langCode: 'EN',
    name: 'Women’s F5 Cartridges',
    description: '- 5 blade cartridge with oversized aloe vera lubrication strip.\n - Ceramic coated blades for a ultra smooth, clean shave.\n - Larger aloe vera lubrication strip soothes sensitive skin.\n - Open cartridge design for easy rinsing and no clogging.\n - Smooth pivoting head for an even, clean shave.'
  },
  {
    id: 9,
    ProductId: 9,
    langCode: 'EN',
    name: 'Shaving Soap',
    description: '- An \'Old School\' classic.\n - Light scented formula for a super comfortable shave.\n - Excellent for avoiding shaving \'nicks\' or razor rash.\n - Use a shaving brush or hand to lather up and apply to face.\n - Made in Turkey.'
  },
  {
    id: 10,
    ProductId: 10,
    langCode: 'EN',
    name: 'Shaving Foam',
    description: '- Enriched moisturising formula.\n - Soothes and nourishes the skin during and after shaving.\n - Cool, fresh scent.\n - Large 200 ml can\n - Made in Turkey.'
  },
  {
    id: 11,
    ProductId: 11,
    langCode: 'EN',
    name: 'Shaving Gel',
    description: '- Enriched moisturising formula.\n - Soothes and nourishes the skin during and after shaving.\n - Cool, fresh scent.\n - Large 200 ml can.\n - Made in Turkey.'
  },
  {
    id: 12,
    ProductId: 12,
    langCode: 'EN',
    name: 'After-shave Cream',
    description: '- Restores moisture lost during shaving.\n - Nourishes skin with vitamins.\n - Calms and soothes sensitive skin.\n - Light, fresh scent.'
  },
  {
    id: 13,
    ProductId: 13,
    langCode: 'EN',
    name: 'Swivel Handle',
    description: '- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique.\n - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas nec urna sed bibendum. Etiam.'
  }
];
