export default [
  {
    id: 1,
    sku: 'Y-Sub-S3',
    PlanTypeId: 1,
    slug: 1
  },
  {
    id: 2,
    sku: 'Y-Sub-S5',
    PlanTypeId: 1,
    slug: 2
  },
  {
    id: 3,
    sku: 'Sub-S3',
    PlanTypeId: 2,
    slug: 3
  },
  {
    id: 4,
    sku: 'Sub-S5',
    PlanTypeId: 2,
    slug: 4
  },
  {
    id: 5,
    sku: 'Y-Sub-F5',
    PlanTypeId: 1,
    slug: 5
  },
  {
    id: 6,
    sku: 'Sub-F5',
    PlanTypeId: 2,
    slug: 6
  },
  {
    id: 7,
    sku: 'M-Sub-S3',
    PlanTypeId: 3,
    slug: 7
  },
  {
    id: 8,
    sku: 'M-Sub-S5',
    PlanTypeId: 3,
    slug: 8
  },
];
