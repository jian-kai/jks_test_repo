
export default [
  {
    // id: 4,
    name: '2 Months',
    // totalDeliverTimes: 1,
    subsequentDeliverDuration: '2 months',
    // totalChargeTimes: 1,
    subsequentChargeDuration: '2 months',
    prefix: '2-'
  },
  {
    // id: 5,
    name: '3 Months',
    // totalDeliverTimes: 1,
    subsequentDeliverDuration: '3 months',
    // totalChargeTimes: 1,
    subsequentChargeDuration: '3 months',
    prefix: '3-'
  },
  {
    // id: 6,
    name: '4 Months',
    // totalDeliverTimes: 1,
    subsequentDeliverDuration: '4 months',
    // totalChargeTimes: 1,
    subsequentChargeDuration: '4 months',
    prefix: '4-'
  },
  {
    // id: 7,
    name: '12 Months',
    // totalDeliverTimes: 1,
    subsequentDeliverDuration: '12 months',
    // totalChargeTimes: 1,
    subsequentChargeDuration: '12 months',
    prefix: 'Y-'
  }
];
