export default [
  {
    FaqId: 1,
    CountryId: 1,
    question: 'How does a Shave Plan work?',
    answer: 'It’s the easiest way to save on shaves, while ensuring that you’re always razor-ready\n\nChoose your preferred type of blade (3 Blade, 5 Blade or 6 Blade) and delivery interval (2, 3, or 4 months) depending on your shaving needs. We’ll deliver brand new razors right to your doorstep and bill your credit card accordingly.\n\nYou will need to sign up for a Shaves2U account [here](https://shaves2u.com/login). Then, if you\'d like, you can choose to start with a [Free Trial](https://shaves2u.com/shave-plans) before subscribing for a Shave Plan.\n\nYou are always in control of your plan. Change or cancel any time here. '
  },
  {
    FaqId: 1,
    CountryId: 1,
    question: 'How does the Free Trial work?',
    answer: 'We understand that it’s hard to buy before you try. Hence, we’ll send you a handle, the cartridge of your choice and our shave cream so you can decide if our products are right for you before you commit.\n\nAll you need to do is pay for the cost of shipping, which is RM6.50.\n\nFrom then, you will have a 21-day (14 days if you sign up via any of our on-ground roadshows) trial period to make sure that the products are right for you. If you do not like our products for any reason, there will be no further charges should you cancel within the trial period.\n\nYour Shave Plan automatically kicks in at the end of the trial period. Every subsequent delivery will also be billed.\n\nAs always, you are in control of your Shave Plan. Change or cancel any time.',
  },
  {
    FaqId: 1,
    CountryId: 1,
    question: 'How do I change my Shave Plan?',
    answer: 'Simply go to your [Profile Page](https://shaves2u.com/user/shave-plans) and click on the “Details” button under the “Shave Plan” tab. Then, click on “Edit Plan Details” and change the blades or shipment frequency that best suit your needs.'
  },
  // {
  //   FaqId: 1,
  //   CountryId: 1,
  //   question: 'What\'s the billing process for a Shave Plan?',
  //   answer: 'There are two options for payment. Choose “Pay-as-you-go” if you’d prefer to pay as you go over the course of 2, 3 or 4 months.\nHowever, if you’d prefer an annual payment plan, choose “Annual Payment” instead. Customers are billed according to the payment plan chosen.'
  // },
  {
    FaqId: 1,
    CountryId: 1,
    question: 'Do I have to sign up for a Shave Plan subscription?',
    answer: 'No. If you’d prefer to order as-needed, simply head over to the [Products page](https://shaves2u.com/products). However, our Free Trial is only available when you subscribe to our Shave Plan.'
  },
  {
    FaqId: 1,
    CountryId: 1,
    question: 'Why do I need a payment method to start my Free Trial?',
    answer: 'This is to cover the shipping cost, as well as to ensure a smooth transition from Free Trial to Shave Plan.\n\nTo verify your payment method, we will need to send an authorisation request to your financial institution. These requests are not charged.\n\nHowever, in some cases, they may affect your available account balance temporarily. Worry not, this is just the bank checking to ensure that the account provided is valid.'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'How long will my order take to arrive?',
    answer: 'We try our best to ensure that our customers receive their orders at their doorstep within 7 - 14 working days.\n\nHowever, shipping may take longer in certain regions. The moment your order leaves our warehouse, we will furnish you with a tracking number you can use to stay up to date with its status.'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'Do you offer free delivery?',
    answer: 'Yes we do!\n\nYou can enjoy FREE shipping for orders above RM75.'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'My delivery is late. Is this normal?',
    answer: 'We do our best to ensure that you receive your products within 7–14 working days. If it’s been longer than that,please drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) with your order details so we can get this sorted out immediately.'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'I wasn\'t around when my delivery arrived! What now?',
    answer: 'Our delivery services partner should have provided you with a \'Missed Delivery\' card, which contains detailed instructions on how you can easily reschedule the delivery.\n\nIf you have any concerns or enquiries, kindly contact our customer service at +603 2773 2882 or [help@shaves2u.com](mailto:help@shaves2u.com) '
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'Which locations do you ship to?',
    answer: 'For now, we only ship to Malaysia and Singapore. However, we\'re looking to expand our services to other countries in the near future.\n\nWant premium, yet affordable shavers in your country? Let us know at [help@shaves2u.com](mailto:help@shaves2u.com)'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'How do I change or cancel my order?',
    answer: 'If you\'re on a Shave Plan or our Free Trial, you can cancel or customise it to your liking on your [Profile Page](https://shaves2u.com/login).\n\nFor other orders, please feel free to reach out to our Customer Service team at [help@shaves2u.com](mailto:help@shaves2u.com) or +603 2773 2882 and we\'ll do our best to help you take care of it.'
  },
  {
    FaqId: 2,
    CountryId: 1,
    question: 'For some reason, I can\'t track my order. Help!',
    answer: 'If you are still unable to track your order after placing it for at least 24 hours, do provide us the following details at [help@shaves2u.com](mailto:help@shaves2u.com) so that we can find out why:\n\n-  Order Confirmation Number\n- Date of Purchase\n- Items Purchased\n- Transaction Number'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'What are your policies on returns?',
    answer: 'We do offer a money back guarantee on all our products. If you’re not fully satisfied with our products for any reason, do get in touch with us at [help@shaves2u.com](mailto:help@shaves2u.com) within 30 days of your purchase and we\'ll help sort you out.'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'How do I return the product(s)?',
    answer: 'You will need to write to us with your refund request at [help@shaves2u.com](mailto:help@shaves2u.com), and we’ll get back to you with details on how to return the products to us. Please include your order number and details of your original purchase.\n\nShaves2U reserves the right to deny refund requests for products that are deemed damaged through misuse, lack of care, mishandling, as well as requests that were made after the 30-day mark or other reasons at our discretion.'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'Will the refund include the entire amount?',
    answer: 'Our refund covers the price of product. It does not cover shipping or handling costs. The amount due will be refunded via the payment method you selected during the purchase.'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'How long will the refund take?',
    answer: 'We will process your request within 10 business days. Please note that we have no control over the time it takes for your bank to reflect the transaction back into your account. Do contact your card’s issuing bank for further information.'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'My item(s) has a defect, would a refund be possible?',
    answer: 'We will honour our money-back guarantee for any defective products within 30 days of purchase. Please email us at [help@shaves2u.com](mailto:help@shaves2u.com) and we will sort out your replacement or refund as soon as possible.'
  },
  {
    FaqId: 3,
    CountryId: 1,
    question: 'I received items I never ordered.',
    answer: 'Oh no! Please drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) and tell us the details. We\'ll sort it out from there.'
  },
  {
    FaqId: 4,
    CountryId: 1,
    question: 'What forms of payment does Shaves2U accept?',
    answer: 'We accept <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/visa-logo.png" style="width:40px; margin-right:10px;"> <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/mastercard-logo.png" style="width:40px; margin-right:10px;"> <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/ipay88_logo.png" style="width:60px; margin-right:10px;"> <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/direct-debit-logo.svg" style="width:70px">'
  },
  {
    FaqId: 4,
    CountryId: 1,
    question: 'Shaves2U rejected my credit card. Do you know why?',
    answer: 'There might be an issue with your card. Please check with your bank or financial institution about why your card was rejected to rule out any possible errors on their end.\n\nIf the problem persists, drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) and we\'ll do our best to resolve the situation.'
  },
  {
    FaqId: 4,
    CountryId: 1,
    question: 'Argh, I accidentally refreshed my browser while paying! What now?!',
    answer: 'Don\'t worry, it happens!\n\nEvery successful Shaves2U transaction will trigger a confirmation email and subsequently, an email containing a tracking number.\n\nIf you didn\'t receive any of the above, just place your order again or contact us at [help@shaves2u.com](mailto:help@shaves2u.com) for confirmation and to prevent duplicate orders'
  },
  {
    FaqId: 4,
    CountryId: 1,
    question: 'I see \'S2U AppCo\' on my bank statement. What\’s that?',
    answer: 'There\'s nothing to worry about, Appco Sdn Bhd is an authorised Shaves2U retailer.\n\nIf you have any questions or concerns about this, feel free to reach out to our customer service and we’ll help confirm your order!'
  },
  {
    FaqId: 4,
    CountryId: 1,
    question: 'I\'ve got a Shaves2U promo code. How do I use it?',
    answer: 'After adding the Shaves2U products you want into your cart, proceed to checkout. Enter your promo code in the box located below “Shipping Cost” and click “APPLY”. The difference should then be reflected in your Grand Total*.\n\n*Promo codes are not applicable for the Free Trial.'
  },
  
  {
    FaqId: 5,
    CountryId: 1,
    question: 'I can\'t remove items from my shopping cart!',
    answer: 'To remove items from your cart, click on the little red X next to each item.\n\nFor some reason, if you still can\'t, clear your cache, refresh your browser and try again. It should be fine.'
  },
  {
    FaqId: 5,
    CountryId: 1,
    question: 'My transaction was rejected on the website, but I received a notification from my bank saying I paid for it. What do I do?',
    answer: 'If you have received an order confirmation from us, clear your cache and restart your browser. The website should reload and reflect the correct status of your order.\n\nOtherwise, drop us an email at help@shaves2u.com with the following details and we\'ll fix it in a jiffy:\n\n- Order Confirmation Number\n- Screenshot of Payment Statement'
  },
  {
    FaqId: 6,
    CountryId: 1,
    question: 'I can\'t login to my account!',
    answer: 'If you’ve  trouble logging in, just clear your cache and cookies, before restarting your Internet browser. It should be all good after that!'
  },
  // {
  //   FaqId: 6,
  //   CountryId: 1,
  //   question: 'For some reason, I can\'t track my order. Help!',
  //   answer: 'If you are still unable to track your order at least 24 hours after placing it, do provide us the following details at [help@shaves2u.com](mailto:help@shaves2u.com) so that we can find out why:\n\nOrder Number, Date of Purchase, Items Purchased, Transaction Number.'
  // },
  {
    FaqId: 6,
    CountryId: 1,
    question: 'How do I terminate my account?',
    answer: 'If you would like to terminate your Shaves2U account, kindly send an Account Termination request email to us at [help@shaves2u.com](mailto:help@shaves2u.com)and we\'ll help you with what you need.'
  },
  {
    FaqId: 6,
    CountryId: 1,
    question: 'I forgot my password, what do I do now?',
    answer: 'No problem! Click “Forgot Password” at the login page and enter your email address.\n\nWe\'ll send you an email with simple instructions on how to reset your password without any issues.'
  },
  {
    FaqId: 6,
    CountryId: 1,
    question: 'How do I change/add my card details, shipping address(es) or billing address(es)?',
    answer: 'First, login to your account and go to the Profile page. Then, click on the “Settings” tab on the left menu bar and your user details, shipping addresses, billing addresses and card details will be presented.\n\nTo edit, click on the pencil icon on the top right of each section.\n\nTo make your Shaves2U ordering experience more convenient, you can also change/add up to 2 shipping addresses and billing addresses to your account.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'Where are Shaves2U blades made?',
    answer: 'Our 3- and 5-blade razors are made in the USA, while the 6-blade is made in Germany. All blades are manufactured to our exacting quality standards from the highest quality carbon steel.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'What are the differences between the 3 Blade, 5 Blade and 6 Blades?',
    answer: '*6 Blade — For that shave above the rest*.\n Made with our patented I.C.E. Diamond Coating for durability and sharpness, the 6 Blade also features the MicroSilver BG<sub>™</sub> enhanced lubricating strip for a shave that maxes out on smoothness. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection.\n\n*5 Blade — Chisel with the classic*\nEquipped with a double-coated blade, aloe lubricating strip, and the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.\n\n*3 Blade — Smooth shaves for sensitive skin*.\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Featuring a lubricating strip to reduce irritation and anti-clog cartridges for ease of cleaning, the 3 Blade Razor works great on thinner stubble while keeping skin irritation at bay.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'What are the differences between the Swivel Handle and the Premium Handle?',
    answer: '*The Swivel Handle*/nArmed with the dual-axis swivel mechanism that perfectly follows your facial contours, clean-shaven perfection is now within reach. Ideal for guys who just shave everything off.\n\nThe Premium Handle \nCrafted with a comfortable rubberised grip for confident handling even under wet conditions, a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair.\n\nBoth handles are similar in length, width and weight.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'How often should I change blades?',
    answer: 'Our blades are made from carbon steel, and will stay sharp for a long, long time!\n\nHowever, we recommend changing your blade every week if you shave daily. If you shave 2–3 times a week, change it every 3–4 weeks. This will give you the best shaving experience and help avoid razor burn.\n\nThe recommended minimum time frame for changing razors is once a month, for hygiene purposes.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'How do I replace the cartridge on my handle?',
    answer: 'On your handle, push the release button forward and the razor blade will come off.\n\nInsert the handle into a new cartridge and once it clicks into place, gently pull it out from the cassette.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'What are the ingredients in your lubricating strips?',
    answer: 'Every Shaves2U cartridge comes fitted with a lubricating strip that contains Vitamin E and Aloe Vera to help reduce skin irritation and to give you smoother shaves.\n\nOn the 6-blade cartridges, the lubricating strip also contains Shea Butter, Cocoa Butter and MicroSilver BG<sub>™</sub>. MicroSilver BG<sub>™</sub> has a high antimicrobial effect and helps to regulate the population of harmful germs and microorganisms on the skin’s surface.\n\nSkincare products with MicroSilver BG<sub>™</sub> improve skin condition quickly, and for a long period of time. It helps prevent skin problems caused by overpopulation of germs and is the only pure metallic silver proven by studies to be safe and effective in personal care products.'
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'Are your blades and handles compatible with other brands?',
    answer: 'All our blades and handles are designed to work perfectly with each other. However, they will not fit with parts from other brands.',
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'What are the differences between the Shave Soap and Shave Cream?',
    answer: 'The difference between our Shave Soap and Shave Cream boils down to time and effort.\n\n*Shave Soap*\nTakes a while to build up a rich lather. However, it is also the most lasting. Think of it as the perfect travel partner—just wet the Shave Soap and rub it all over!\n\n*Shave Cream*\nLathers much faster. Just squeeze a little onto your wet palm and rub it with your fingertips for a rich lather. Use a shaving brush for better results.',
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'Are Shaves2U skincare products suitable for sensitive skin?',
    answer: 'Although our products are not made specially for sensitive skin, our entire skincare range has been put through strict dermatological tests prior to production to ensure their safety. If you have sensitive skin, we recommend trying a small amount on a small patch of your skin before using.',
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'Are your skincare products made from all-natural ingredients?',
    answer: 'Our products are not made solely from all-natural ingredients. However, they have been through strict dermatologist tests prior to production to ensure their safety for use.',
  },
  {
    FaqId: 7,
    CountryId: 1,
    question: 'Where are the skincare products from?',
    answer: 'They are produced in Turkey by ARKO MEN.',
  },
  {
    FaqId: 8,
    CountryId: 1,
    question: 'Can I buy Shaves2U products in retail or online stores?',
    answer: 'At the moment, retail stores do not carry Shaves2U products. However, Shaves2U products are available in the Lazada online store.'
  },
  {
    FaqId: 8,
    CountryId: 1,
    question: 'I\'m interested in a partnership with Shaves2U, who do I speak to?',
    answer: 'If you think we could work out a mutually beneficial business agreement or partnership, drop us an email at [marketing@shaves2u.com](mailto:marketing@shaves2u.com) with your information or proposal.\n\nWe will review it and get back to you within a period of time to continue the conversation. We look forward to working with you!'
  },
  {
    FaqId: 9,
    CountryId: 7,
    question: 'How does a Shave Plan work?',
    answer: 'It’s the easiest way to save on shaves, while ensuring that you’re always razor-ready\n\nChoose your preferred type of blade (3 Blade, 5 Blade or 6 Blade) and delivery interval (2, 3, or 4 months) depending on your shaving needs. We’ll deliver brand new razors right to your doorstep and bill your credit card accordingly.\n\nYou will need to sign up for a Shaves2U account [here](https://shaves2u.com/login). Then, if you\'d like, you can choose to start with a [Free Trial](https://shaves2u.com/shave-plans) before subscribing for a Shave Plan.\n\nYou are always in control of your plan. Change or cancel any time here. '
  },
  {
    FaqId: 9,
    CountryId: 7,
    question: 'How does the Free Trial work?',
    answer: 'We understand that it’s hard to buy before you try. Hence, we’ll send you a handle, the cartridge of your choice and our shave cream so you can decide if our products are right for you before you commit.\n\nAll you need to do is pay for the cost of shipping, which is $5.00\n\nFrom then, you will have a 21-day (14 days if you sign up via any of our on-ground roadshows) trial period to make sure that the products are right for you. If you do not like our products for any reason, there will be no further charges should you cancel within the trial period.\n\nYour Shave Plan automatically kicks in at the end of the trial period. Every subsequent delivery will also be billed.\n\nAs always, you are in control of your Shave Plan. Change or cancel any time.'
  },
  {
    FaqId: 9,
    CountryId: 7,
    question: 'How do I change my Shave Plan?',
    answer: 'Simply go to your [Profile Page](https://shaves2u.com/login) and click on the “Details” button under the “Shave Plan” tab. Then, click on “Edit Plan Details” and change the blades or shipment frequency that best suit your needs.'
  },
  // {
  //   FaqId: 1,
  //   CountryId: 1,
  //   question: 'What\'s the billing process for a Shave Plan?',
  //   answer: 'There are two options for payment. Choose “Pay-as-you-go” if you’d prefer to pay as you go over the course of 2, 3 or 4 months.\nHowever, if you’d prefer an annual payment plan, choose “Annual Payment” instead. Customers are billed according to the payment plan chosen.'
  // },
  {
    FaqId: 9,
    CountryId: 7,
    question: 'Do I have to sign up for a Shave Plan subscription?',
    answer: 'No. If you’d prefer to order as-needed, simply head over to the [Products page](https://shaves2u.com/products). However, our Free Trial is only available when you subscribe to our Shave Plan.'
  },
  {
    FaqId: 9,
    CountryId: 7,
    question: 'Why do I need a payment method to start my Free Trial?',
    answer: 'This is to cover the shipping cost, as well as to ensure a smooth transition from Free Trial to Shave Plan.\n\nTo verify your payment method, we will need to send an authorisation request to your financial institution. These requests are not charged.\n\nHowever, in some cases, they may affect your available account balance temporarily. Worry not, this is just the bank checking to ensure that the account provided is valid.'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'How long will my order take to arrive?',
    answer: 'We try our best to ensure that our customers receive their orders at their doorstep within 7 - 14 working days.\n\nHowever, shipping may take longer in certain regions. The moment your order leaves our warehouse, we will furnish you with a tracking number you can use to stay up to date with its status.'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'Do you offer free delivery?',
    answer: 'Yes we do!\n\nYou can enjoy FREE shipping for orders above $30.'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'My delivery is late. Is this normal?',
    answer: 'We do our best to ensure that you receive your products within 7–14 working days. If it’s been longer than that,please drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) with your order details so we can get this sorted out immediately.'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'I wasn\'t around when my delivery arrived! What now?',
    answer: 'Our delivery services partner should have provided you with a \'Missed Delivery\' card, which contains detailed instructions on how you can easily reschedule the delivery.\n\nIf you have any concerns or enquiries, kindly contact our customer service at +65 6329 1488 or [help@shaves2u.com](mailto:help@shaves2u.com) '
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'Which locations do you ship to?',
    answer: 'For now, we only ship to Malaysia and Singapore. However, we\'re looking to expand our services to other countries in the near future.\n\nWant premium, yet affordable shavers in your country? Let us know at [help@shaves2u.com](mailto:help@shaves2u.com)'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'How do I change or cancel my order?',
    answer: 'If you\'re on a Shave Plan or our Free Trial, you can cancel or customise it to your liking on your [Profile Page](https://shaves2u.com/user/shave-plans).\n\nFor other orders, please feel free to reach out to our Customer Service team at [help@shaves2u.com](mailto:help@shaves2u.com) or +65 6329 1488 and we\'ll do our best to help you take care of it.'
  },
  {
    FaqId: 10,
    CountryId: 7,
    question: 'For some reason, I can\'t track my order. Help!',
    answer: 'If you are still unable to track your order after placing it for at least 24 hours, do provide us the following details at [help@shaves2u.com](mailto:help@shaves2u.com) so that we can find out why:\n\n-  Order Confirmation Number\n- Date of Purchase\n- Items Purchased\n- Transaction Number'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'What are your policies on returns?',
    answer: 'We do offer a money back guarantee on all our products. If you’re not fully satisfied with our products for any reason, do get in touch with us at [help@shaves2u.com](mailto:help@shaves2u.com) within 30 days of your purchase and we\'ll help sort you out.'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'How do I return the product(s)?',
    answer: 'You will need to write to us with your refund request at [help@shaves2u.com](mailto:help@shaves2u.com), and we’ll get back to you with details on how to return the products to us. Please include your order number and details of your original purchase.\n\nShaves2U reserves the right to deny refund requests for products that are deemed damaged through misuse, lack of care, mishandling, as well as requests that were made after the 30-day mark or other reasons at our discretion.'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'Will the refund include the entire amount?',
    answer: 'Our refund covers the price of product. It does not cover shipping or handling costs. The amount due will be refunded via the payment method you selected during the purchase.'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'How long will the refund take?',
    answer: 'We will process your request within 10 business days. Please note that we have no control over the time it takes for your bank to reflect the transaction back into your account. Do contact your card’s issuing bank for further information.'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'My item(s) has a defect, would a refund be possible?',
    answer: 'We will honour our money-back guarantee for any defective products within 30 days of purchase. Please email us at [help@shaves2u.com](mailto:help@shaves2u.com) and we will sort out your replacement or refund as soon as possible.'
  },
  {
    FaqId: 11,
    CountryId: 7,
    question: 'I received items I never ordered.',
    answer: 'Oh no! Please drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) and tell us the details. We\'ll sort it out from there.'
  },
  {
    FaqId: 12,
    CountryId: 7,
    question: 'What forms of payment does Shaves2U accept?',
    answer: 'We accept <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/visa-logo.png" style="width:40px; margin-right:10px;"> <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/mastercard-logo.png" style="width:40px; margin-right:10px;"> <img src="http://d8pi1a7qa1dfv.cloudfront.net/assets/images/direct-debit-logo.svg" style="width:70px">'
  },
  {
    FaqId: 12,
    CountryId: 7,
    question: 'Shaves2U rejected my credit card. Do you know why?',
    answer: 'There might be an issue with your card. Please check with your bank or financial institution about why your card was rejected to rule out any possible errors on their end.\n\nIf the problem persists, drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) and we\'ll do our best to resolve the situation.'
  },
  {
    FaqId: 12,
    CountryId: 7,
    question: 'Argh, I accidentally refreshed my browser while paying! What now?!',
    answer: 'Don\'t worry, it happens!\n\nEvery successful Shaves2U transaction will trigger a confirmation email and subsequently, an email containing a tracking number.\n\nIf you didn\'t receive any of the above, just place your order again or contact us at [help@shaves2u.com](mailto:help@shaves2u.com) for confirmation and to prevent duplicate orders'
  },
  {
    FaqId: 12,
    CountryId: 7,
    question: 'I see \'S2U AppCo\' on my bank statement. What\’s that?',
    answer: 'There\'s nothing to worry about, Appco Sdn Bhd is an authorised Shaves2U retailer.\n\nIf you have any questions or concerns about this, feel free to reach out to our customer service and we’ll help confirm your order!'
  },
  {
    FaqId: 12,
    CountryId: 7,
    question: 'I\'ve got a Shaves2U promo code. How do I use it?',
    answer: 'After adding the Shaves2U products you want into your cart, proceed to checkout. Enter your promo code in the box located below “Shipping Cost” and click “APPLY”. The difference should then be reflected in your Grand Total*.\n\n*Promo codes are not applicable for the Free Trial.'
  },
  
  {
    FaqId: 13,
    CountryId: 7,
    question: 'I can\'t remove items from my shopping cart!',
    answer: 'To remove items from your cart, click on the little red X next to each item.\n\nFor some reason, if you still can\'t, clear your cache, refresh your browser and try again. It should be fine.'
  },
  {
    FaqId: 13,
    CountryId: 7,
    question: 'My transaction was rejected on the website, but I received a notification from my bank saying I paid for it. What do I do?',
    answer: 'If you have received an order confirmation from us, clear your cache and restart your browser. The website should reload and reflect the correct status of your order.\n\nOtherwise, drop us an email at help@shaves2u.com with the following details and we\'ll fix it in a jiffy:\n\n- Order Confirmation Number\n- Screenshot of Payment Statement'
  },
  {
    FaqId: 14,
    CountryId: 7,
    question: 'I can\'t login to my account!',
    answer: 'If you’ve  trouble logging in, just clear your cache and cookies, before restarting your Internet browser. It should be all good after that!'
  },
  // {
  //   FaqId: 6,
  //   CountryId: 1,
  //   question: 'For some reason, I can\'t track my order. Help!',
  //   answer: 'If you are still unable to track your order at least 24 hours after placing it, do provide us the following details at [help@shaves2u.com](mailto:help@shaves2u.com) so that we can find out why:\n\nOrder Number, Date of Purchase, Items Purchased, Transaction Number.'
  // },
  {
    FaqId: 14,
    CountryId: 7,
    question: 'How do I terminate my account?',
    answer: 'If you would like to terminate your Shaves2U account, kindly send an Account Termination request email to us at [help@shaves2u.com](mailto:help@shaves2u.com)and we\'ll help you with what you need.'
  },
  {
    FaqId: 14,
    CountryId: 7,
    question: 'I forgot my password, what do I do now?',
    answer: 'No problem! Click “Forgot Password” at the login page and enter your email address.\n\nWe\'ll send you an email with simple instructions on how to reset your password without any issues.'
  },
  {
    FaqId: 14,
    CountryId: 7,
    question: 'How do I change/add my card details, shipping address(es) or billing address(es)?',
    answer: 'First, login to your account and go to the Profile page. Then, click on the “Settings” tab on the left menu bar and your user details, shipping addresses, billing addresses and card details will be presented.\n\nTo edit, click on the pencil icon on the top right of each section.\n\nTo make your Shaves2U ordering experience more convenient, you can also change/add up to 2 shipping addresses and billing addresses to your account.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'Where are Shaves2U blades made?',
    answer: 'Our 3- and 5-blade razors are made in the USA, while the 6-blade is made in Germany. All blades are manufactured to our exacting quality standards from the highest quality carbon steel.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'What are the differences between the 3 Blade, 5 Blade and 6 Blades?',
    answer: '*6 Blade — For that shave above the rest*.\n Made with our patented I.C.E. Diamond Coating for durability and sharpness, the 6 Blade also features the MicroSilver BG<sub>™</sub> enhanced lubricating strip for a shave that maxes out on smoothness. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection.\n\n*5 Blade — Chisel with the classic*\nEquipped with a double-coated blade, aloe lubricating strip, and the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.\n\n*3 Blade — Smooth shaves for sensitive skin*.\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Featuring a lubricating strip to reduce irritation and anti-clog cartridges for ease of cleaning, the 3 Blade Razor works great on thinner stubble while keeping skin irritation at bay.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'What are the differences between the Swivel Handle and the Premium Handle?',
    answer: '*The Swivel Handle*/nArmed with the dual-axis swivel mechanism that perfectly follows your facial contours, clean-shaven perfection is now within reach. Ideal for guys who just shave everything off.\n\nThe Premium Handle \nCrafted with a comfortable rubberised grip for confident handling even under wet conditions, a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair.\n\nBoth handles are similar in length, width and weight.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'How often should I change blades?',
    answer: 'Our blades are made from carbon steel, and will stay sharp for a long, long time!\n\nHowever, we recommend changing your blade every week if you shave daily. If you shave 2–3 times a week, change it every 3–4 weeks. This will give you the best shaving experience and help avoid razor burn.\n\nThe recommended minimum time frame for changing razors is once a month, for hygiene purposes.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'How do I replace the cartridge on my handle?',
    answer: 'On your handle, push the release button forward and the razor blade will come off.\n\nInsert the handle into a new cartridge and once it clicks into place, gently pull it out from the cassette.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'What are the ingredients in your lubricating strips?',
    answer: 'Every Shaves2U cartridge comes fitted with a lubricating strip that contains Vitamin E and Aloe Vera to help reduce skin irritation and to give you smoother shaves.\n\nOn the 6-blade cartridges, the lubricating strip also contains Shea Butter, Cocoa Butter and MicroSilver BG<sub>™</sub>. MicroSilver BG<sub>™</sub> has a high antimicrobial effect and helps to regulate the population of harmful germs and microorganisms on the skin’s surface.\n\nSkincare products with MicroSilver BG<sub>™</sub> improve skin condition quickly, and for a long period of time. It helps prevent skin problems caused by overpopulation of germs and is the only pure metallic silver proven by studies to be safe and effective in personal care products.'
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'Are your blades and handles compatible with other brands?',
    answer: 'All our blades and handles are designed to work perfectly with each other. However, they will not fit with parts from other brands.',
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'What are the differences between the Shave Soap and Shave Cream?',
    answer: 'The difference between our Shave Soap and Shave Cream boils down to time and effort.\n\n*Shave Soap*\nTakes a while to build up a rich lather. However, it is also the most lasting. Think of it as the perfect travel partner—just wet the Shave Soap and rub it all over!\n\n*Shave Cream*\nLathers much faster. Just squeeze a little onto your wet palm and rub it with your fingertips for a rich lather. Use a shaving brush for better results.',
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'Are Shaves2U skincare products suitable for sensitive skin?',
    answer: 'Although our products are not made specially for sensitive skin, our entire skincare range has been put through strict dermatological tests prior to production to ensure their safety. If you have sensitive skin, we recommend trying a small amount on a small patch of your skin before using.',
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'Are your skincare products made from all-natural ingredients?',
    answer: 'Our products are not made solely from all-natural ingredients. However, they have been through strict dermatologist tests prior to production to ensure their safety for use.',
  },
  {
    FaqId: 15,
    CountryId: 7,
    question: 'Where are the skincare products from?',
    answer: 'They are produced in Turkey by ARKO MEN.',
  },
  {
    FaqId: 16,
    CountryId: 7,
    question: 'Can I buy Shaves2U products in retail or online stores?',
    answer: 'At the moment, retail stores do not carry Shaves2U products. However, Shaves2U products are available in the Lazada online store.'
  },
  {
    FaqId: 16,
    CountryId: 7,
    question: 'I\'m interested in a partnership with Shaves2U, who do I speak to?',
    answer: 'If you think we could work out a mutually beneficial business agreement or partnership, drop us an email at [marketing@shaves2u.com](mailto:marketing@shaves2u.com) with your information or proposal.\n\nWe will review it and get back to you within a period of time to continue the conversation. We look forward to working with you!'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: 'S2U면도날은어디에서생산되나요?',
    answer: '우리 면도날은 고품질 세라믹 코팅 혹은 내구성 있는 다이아몬드 코팅을 하여 생산된 제품입니다. 이 제품들은 미국 혹은 독일의 수준 높은 기준에 따라 제작되었습니다.'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: 'S2U면도날중S3, S5그리고S6사이의차이점은무엇인가요?',
    answer: 'S6 \- S6 - 6개면도날로구성된가장고급쉐이브옵션입니다\n - 아큐블레이드<sup>®</sup>트리머가염소수염,구레나룻그리고코아래부분을다듬기적합하게카트리지뒤쪽에장착되어있습니다.\n - 막히는걸방지하고간편하게헹굴수있도록6개의L자모양면도날이가깝게위치해있습니다.\n - 더정확한쉐이빙을위해특허를출원한ICE (이온탄소칼날)에다이아몬드코팅을하여내구성과날카로움을높였습니다.\n - 알로에베라윤활스트립이면도를할때마다편안하게해줍니다.\n - 각카세트는카트리지당4개면도날로구성되어있습니다.\n - 내구성있고정밀성있게세라믹코팅을한독일제입니다.\n\nS5 - 5개의면도날이더많은면도날보다좋습니다\n - 세라믹으로더블코팅된칼날이더욱편안하고정확한면도를도와줍니다.\n - 막히는걸방지하고간편하게헹굴수있도록5개의면도날이가깝게위치해있습니다.\n - 아큐블레이드<sup>®</sup>트리머는염소수염,구레나룻그리고코아래부분을다듬기적합하게카트리지뒤쪽에장착되어있습니다.\n - 30프로더큰비타민E와알로에베라로된윤활스트립이붉은기와자극방지를돕습니다.\n - 민감한피부에사용할수있게저자극성및피부과전문의승인을받았습니다.\n - 각카세트는카트리지당4개면도날로구성되어있습니다.\n\nS3 - 3개의최소한의칼날로도충분합니다\n - 세라믹으로더블코팅된면도날이더욱편안하고정확한면도를도와줍니다.\n - 막히는걸방지하고간편하게헹굴수있도록3개의면도날이가깝게위치해있습니다.\n - 비타민E와알로에베라로된윤활스트립이피부자극을줄여주고더부드러운면도를돕습니다.\n - 민감한피부에사용할수있게저자극성및피부과전문의승인을받았습니다.\n - 각카세트는카트리지당4개면도날로구성되어있습니다.'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: '스위블핸들과프리미엄핸들의차이점은무엇인가요?',
    answer: '스위블핸들은자신감있고정확한조작이가능하도록고무그립을세련된크롬으로마무리하였습니다.회전헤드가얼굴의윤곽을따라면도날이다닐수있게합니다.다시말해매번더안전하고깨끗한제모가가능해집니다.프리미엄핸들은매력적인크롬으로마무리한부드러운고무소재로만들었습니다.더욱정확한균형과자신감있는그립을주기위해조금더무겁습니다.때문에회전하는헤드가아니지만원할때멋진Shaves2U제모를가능하게해줍니다.핸들은두개모두길이,넓이및무게가유사합니다.'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: '얼마나자주면도날을갈아주어야하나요?',
    answer: '우리면도날은내구성있게제작되어아주오래사용이가능합니다.만약매일면도를할경우,면도날을매주바꿔줄것을권장합니다. 1주일에2-3번정도면도를한다면3-4주에한번교체하시면됩니다.그것보다더적게사용하더라도위생을생각해최소1달에한번은교체해주실것을권장합니다.즐거운면도되세요!'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: '핸들에서카트리지를어떻게교체하나요?',
    answer: '핸들의카트리지뒤쪽에버튼이있습니다.그버튼을앞으로밀면카트리지가빠집니다.핸들을자신의카세트의새로운카트리지에넣고클릭소리가나게위치해준후카세트에서부드럽게꺼내줍니다.'
  },
  {
    FaqId: 17,
    CountryId: 8,
    question: '윤활스트립재료는무엇인가요?',
    answer: '모든Shaves2U카트리지는피부자극을줄이고부드러운면도를위한비타민E와알로에베라성분이들어있는윤활스트립을가지고있습니다. 6블레이드S6카트리지의경우,윤활스트립에시어버터,코코아버터그리고마이크로실버BG™도들어있습니다.마이크로실버BG™는높은항균효과가있어피부표피위에안좋은세균이나미생물번식을막아줍니다.마이크로실버BG™를사용한스킨케어제품들의경우피부상태를빠르게개선시켜주며이를오래지속시켜줍니다.세균증식으로인한피부문제도예방해주며각종연구를통해개인미용및위생용품에안전하고효과적으로밝혀진순금속성은으로만들었습니다.'
  },
  {
    FaqId: 18,
    CountryId: 8,
    question: 'Shaves2U제품을소매점혹은온라인에서구매할수있나요?',
    answer: '현재소매점에서는Shaves2U제품을취급하고있지않습니다.하지만, Shaves2U제품은라자다온라인상점에서구매가가능합니다.'
  },
  {
    FaqId: 18,
    CountryId: 8,
    question: 'Shaves2U와파트너십에관심이있습니다.어디에연락해야할까요?',
    answer: '우리와상호이익의사업계약혹은파트너십을구축할수있다고생각하시면저희측으로귀하의정보혹은제안서를이메일([help@shaves2u.com](mailto:help@shaves2u.com))주십시오.검토후빠른시일내에연락드리겠습니다.함께일하길고대하고있습니다!'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'Where are Shaves2U blades made?',
    answer: 'We have blades that are manufactured with a high quality ceramic coating or a durable diamond coating. They are manufacture to the highest stands in either USA or Germany.'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'What are the differences between the S3, S5 and S6 Shaves2U blades?',
    answer: 'S6 \- Our most premium shave option with 6 blades\n - Accublade<sup>®</sup> Trimmer built into the back of the cartridge for trimming goatee, sideburns and area under the nose.\n - 6 L-shaped Blades spaced close together to prevent clogging and for easy rinsing.\n - Patented I.C.E (Ionic Carbon Edge)* Diamond Coating increases durability and sharpness for more precise shaves.\n - Aloe Vera lubricating strip for a comfortable shave every time.\n - Each cassette comes with 4 blades/cartridges.\n - Made in the Germany with a durable precision ceramic coating.\n\nS5 \- The more blades the merrier with 5 blades\n - Double Coated Ceramic blade edge for a more comfortable and precise shave.\n - 5 blades spaced close together to prevent clogging and for easy rinsing.\n - Accublade<sup>®</sup> Trimmer built into the back of the cartridge for shaving goatee, sideburns and under the nose\n - 30% larger lubrication strip with Vitamin E & Aloe Vera to help protect against redness and irritation.\n - Hypoallergenic and dermatologist approved for use on sensitive skin.\n - Each cassette comes with 4 blades/cartridges.\n\nS3 \- When the 3-blade bare minimum is so much more than enough\n - Double Coated Ceramic blade edge for a more comfortable and precise shave.\n - 3 blades spaced close together to prevent clogging and for easy rinsing.\n - Lubricating Strip with Vitamin E & Aloe Vera to reduce skin irritation and for smoother shaves.\n - Hypoallergenic and dermatologist approved for use on sensitive skin.\n - Each cassette comes with 4 blades/cartridges.'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'What are the differences between the Swivel Handle and the Premium Handle?',
    answer: 'The Swivel Handle with its refined chrome finish is fitted with a rubberised grip for confident and precise control. It has a pivot head that allows the blade to navigate contours of your face. This means a comfortable yet cleaner shave every time!\n\nThe Premium Handle is made from a soft rubberised material with a striking chrome finish. It has some heft to it, giving you more precise balance and a confident grip. Although it doesn\'t have the swivel head, it still gives you that great Shaves2U shave when you want it.\n\nBoth handles are similar in length, width and weight.'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'How often should I change blades?',
    answer: 'Our blades are built for durability and therefore they last a long, long time! If you shave everyday, we recommend changing your blades every week. If you shave maybe 2-3 times a week, change it every 3-4 weeks If you shave even less than that, we still recommend  you change it every month due to hygienic purposes. Happy shaving!'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'How do I replace the cartridge on my handle?',
    answer: 'There is a button behind the cartridge on your handle, push it forward to release the cartridge. Insert the handle into the a new cartridge in your casette and once it clicks into place, pull it out from the casette gently.'
  },
  {
    FaqId: 19,
    CountryId: 8,
    question: 'What are the ingredients in your lubricating strips?',
    answer: 'Every Shaves2U cartridge comes fitted with a lubricating strip that contains Vitamin E and Aloe Vera to reduce skin irritation and to give you smoother shaves.\n\nOn the 6-blade S6 cartridges, the lubricating strip also contains Shea Butter, Cocoa Butter and MicroSilver BG™. MicroSilver™ has a high antimicrobial effect and regulates the population of harmul germs and microorganisms on the skin’s surface. Skincare products with MicroSilver BG™ improve the skin condition quickly and for a lasting period of time. It prevents skin problems caused by overpopulation of germs and it is the only pure metallic silver that studies have shown to be safe and effective in personal care products.'
  },
  {
    FaqId: 20,
    CountryId: 8,
    question: 'Can I buy Shaves2U products in retail or online stores?',
    answer: 'At the moment, retail stores do not carry Shaves2U products. However, Shaves2U products are available in the Lazada online store.'
  },
  {
    FaqId: 20,
    CountryId: 8,
    question: 'I\'m interested in a partnership with Shaves2U, who do I speak to?',
    answer: 'If you think we could work out a mutually beneficial business agreement or partnership, drop us an email at [help@shaves2u.com](mailto:help@shaves2u.com) with your information or proposal. We will review it and get back to you within a period of time to continue the conversation. We look forward to working with you!'
  },
];
