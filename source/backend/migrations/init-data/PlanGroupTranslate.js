
export default [
  {
    id: 1,
    name: '3 Blade Cartridge',
    langCode: 'EN',
    PlanGroupId: 1
  },
  {
    id: 2,
    name: '5 Blade Cartridge',
    langCode: 'EN',
    PlanGroupId: 2
  },
  {
    id: 3,
    name: '6 Blade Cartridge',
    langCode: 'EN',
    PlanGroupId: 3
  },
];
