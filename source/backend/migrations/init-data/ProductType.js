export default [
  {
    id: 1,
    name: 'Awesome Shave Kit',
  },
  {
    id: 2,
    name: 'Starter Packs',
  },
  {
    id: 3,
    name: 'Handles',
  },
  {
    id: 4,
    name: 'Cartridge Packs',
  },
  {
    id: 5,
    name: 'Skin Care',
  },
  {
    id: 6,
    name: 'Sample',
  }
];
