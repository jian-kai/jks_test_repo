export default [
  {
    id: 1,
    role: 'super admin',
    email: 'admin@gmail.com',
    password: '0192023a7bbd73250516f069df18b500',
    firstName: 'Administrator',
    lastName: 'Administrator',
    staffId: 'xxxxxxx',
    CountryId: 1,
    RoleId: 1,
    viewAllCountry: 1,
    phone: '+841659612968'
  },
  {
    id: 2,
    role: 'admin',
    email: 'admin-mys@gmail.com',
    password: '0192023a7bbd73250516f069df18b500',
    firstName: 'Admin',
    lastName: 'malaysia',
    staffId: 'yyyyyyy',
    CountryId: 1,
    RoleId: 2,
    phone: '+841659612968'
  },
  {
    id: 3,
    role: 'staff',
    email: 'staff-mys@gmail.com',
    password: '0192023a7bbd73250516f069df18b500',
    firstName: 'Staff',
    lastName: 'malaysia',
    staffId: 'yyyyyyy',
    CountryId: 1,
    RoleId: 3,
    phone: '+841659612968'
  },
  {
    id: 4,
    role: 'admin',
    email: 'admin-sg@gmail.com',
    password: '0192023a7bbd73250516f069df18b500',
    firstName: 'Admin',
    lastName: 'singapore',
    staffId: 'yyyyyyy',
    CountryId: 7,
    RoleId: 2,
    phone: '+841659612968'
  },
  {
    id: 5,
    role: 'super admin',
    email: 'kimtn129@gmail.com',
    password: '0192023a7bbd73250516f069df18b500',
    firstName: 'Kim',
    lastName: 'Thi',
    staffId: 'xxxxxxx',
    CountryId: 1,
    RoleId: 1,
    viewAllCountry: 1,
    phone: '+841659612968'
  }
];
