export default [
  {
    id: 1,
    UserId: 1,
    firstName: 'Kim',
    lastName: 'Thi',
    contactNumber: '+841659612968',
    address: '75 Kg Sg Ramal Luar',
    state: 'Selangor',
    city: 'Kajang',
    portalCode: '43000',
    isDefault: true,
    CountryId: 1
  }
];
