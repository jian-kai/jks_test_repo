export default [
  {
    id: 1,
    PromotionId: 1,
    name: 'thank you',
    description: 'discount for next purchasing',
    langCode: 'EN'
  },
  {
    id: 2,
    PromotionId: 2,
    name: 'new year',
    description: 'discount for new year vacation',
    langCode: 'EN'
  },
  {
    id: 3,
    PromotionId: 3,
    name: 'wellcome',
    description: 'wellcome new member',
    langCode: 'EN'
  },
  {
    id: 4,
    PromotionId: 4,
    name: 'freeshipping',
    description: 'free shipping for first order',
    langCode: 'EN'
  }
];
