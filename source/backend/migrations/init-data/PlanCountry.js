export default [
  {
    id: 1,
    PlanId: 1,
    actualPrice: 277.00,
    sellPrice: 220.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 2,
    PlanId: 2,
    actualPrice: 373,
    sellPrice: 280.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 3,
    PlanId: 3,
    actualPrice: 277.00,
    sellPrice: 220.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 4,
    PlanId: 4,
    actualPrice: 373,
    sellPrice: 280.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 5,
    PlanId: 5,
    actualPrice: 399.00,
    sellPrice: 300.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 6,
    PlanId: 6,
    actualPrice: 399,
    sellPrice: 300.00,
    CountryId: 1,
    savePercent: 20
  },
  {
    id: 7,
    PlanId: 7,
    actualPrice: 277.00,
    sellPrice: 277.00,
    CountryId: 1,
    savePercent: 0
  },
  {
    id: 8,
    PlanId: 8,
    actualPrice: 373,
    sellPrice: 373,
    CountryId: 1,
    savePercent: 0
  },
];
