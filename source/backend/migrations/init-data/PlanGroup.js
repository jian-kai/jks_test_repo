
export default [
  {
    id: 1,
    url: 'https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/plan-images/88c0a0b0-5373-11e8-a647-3fe0ef493ba6.png',
  },
  {
    id: 2,
    url: 'https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/plan-images/932107c0-5373-11e8-a647-3fe0ef493ba6.png',
  },
  {
    id: 3,
    url: 'https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/7658ddc0-5373-11e8-a647-3fe0ef493ba6.png',
  },
];
