export default [
  {
    id: 1,
    ProductId: 1,
    langCode: 'EN',
    type: 'right',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/bca86860-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 2,
    ProductId: 1,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 3,
    ProductId: 2,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 4,
    ProductId: 3,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 5,
    ProductId: 4,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 6,
    ProductId: 5,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  },
  {
    id: 7,
    ProductId: 6,
    langCode: 'EN',
    type: 'center',
    imageUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c0619fd0-8b99-11e7-a6cf-65e786dcfa6c.png',
    title: 'Vivamus et tincidunt leo.',
    details: 'Praesent consectetur semper ante, ut sodales elit varius eu. Proin ultricies tincidunt libero, fringilla dictum mauris hendrerit vel.'
  }
];
