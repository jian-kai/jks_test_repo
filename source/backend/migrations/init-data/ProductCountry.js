export default [
  {
    id: 1,
    ProductId: 1,
    qty: 100,
    maxQty: 1000,
    basedPrice: 16,
    sellPrice: 18,
    CountryId: 1
  },
  {
    id: 2,
    ProductId: 2,
    qty: 200,
    maxQty: 1000,
    basedPrice: 10,
    sellPrice: 13,
    CountryId: 1
  },
  {
    id: 3,
    ProductId: 3,
    qty: 300,
    maxQty: 1000,
    basedPrice: 25,
    sellPrice: 27,
    CountryId: 1
  },
  {
    id: 4,
    ProductId: 4,
    qty: 400,
    maxQty: 1000,
    basedPrice: 30,
    sellPrice: 39,
    CountryId: 1
  },
  {
    id: 5,
    ProductId: 5,
    qty: 100,
    maxQty: 1000,
    basedPrice: 109,
    sellPrice: 109,
    CountryId: 1
  },
  {
    id: 6,
    ProductId: 6,
    qty: 100,
    maxQty: 200,
    basedPrice: 15,
    sellPrice: 18,
    CountryId: 1
  },
  {
    id: 7,
    ProductId: 7,
    qty: 100,
    maxQty: 200,
    basedPrice: 10,
    sellPrice: 13,
    CountryId: 1
  },
  {
    id: 8,
    ProductId: 8,
    qty: 100,
    maxQty: 200,
    basedPrice: 40,
    sellPrice: 45,
    CountryId: 1
  },
  {
    id: 9,
    ProductId: 9,
    qty: 100,
    maxQty: 200,
    basedPrice: 10,
    sellPrice: 12,
    CountryId: 1
  },
  {
    id: 10,
    ProductId: 10,
    qty: 100,
    maxQty: 200,
    basedPrice: 20,
    sellPrice: 22,
    CountryId: 1
  },
  {
    id: 11,
    ProductId: 11,
    qty: 100,
    maxQty: 200,
    basedPrice: 20,
    sellPrice: 22,
    CountryId: 1
  },
  {
    id: 12,
    ProductId: 12,
    qty: 100,
    maxQty: 200,
    basedPrice: 15,
    sellPrice: 17,
    CountryId: 1
  },
  {
    id: 13,
    ProductId: 13,
    qty: 100,
    maxQty: 200,
    basedPrice: 1,
    sellPrice: 1,
    CountryId: 1
  },
  {
    id: 14,
    ProductId: 3,
    qty: 300,
    maxQty: 1000,
    basedPrice: 25,
    sellPrice: 27,
    CountryId: 7
  },
  {
    id: 15,
    ProductId: 4,
    qty: 400,
    maxQty: 1000,
    basedPrice: 30,
    sellPrice: 39,
    CountryId: 7
  },
  {
    id: 16,
    ProductId: 1,
    qty: 100,
    maxQty: 1000,
    basedPrice: 16,
    sellPrice: 18,
    CountryId: 7
  },
  {
    id: 17,
    ProductId: 10,
    qty: 100,
    maxQty: 200,
    basedPrice: 20,
    sellPrice: 22,
    CountryId: 7
  },
];
