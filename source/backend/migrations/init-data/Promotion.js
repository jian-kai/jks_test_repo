export default [
  {
    id: 1,
    discount: 10,
    bannerUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/bca86860-8b99-11e7-a6cf-65e786dcfa6c.png',
    CountryId: 1,
    expiredAt: '2020-10-10',
    productCountryIds: 'all',
    planCountryIds: 'all'
  },
  {
    id: 2,
    discount: 20,
    bannerUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/bca86860-8b99-11e7-a6cf-65e786dcfa6c.png',
    CountryId: 1,
    expiredAt: '2020-10-10',
    planCountryIds: JSON.stringify([1]),
    productCountryIds: JSON.stringify([2, 3])
  },
  {
    id: 3,
    discount: 10,
    bannerUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/bca86860-8b99-11e7-a6cf-65e786dcfa6c.png',
    CountryId: 1,
    expiredAt: '2020-10-10',
    planCountryIds: 'all',
    productCountryIds: 'all',
    isGeneric: true,
    minSpend: 50,
    maxDiscount: 10
  },
  {
    id: 4,
    discount: 0,
    bannerUrl: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/bca86860-8b99-11e7-a6cf-65e786dcfa6c.png',
    CountryId: 1,
    expiredAt: '2020-10-10',
    planCountryIds: 'all',
    productCountryIds: 'all',
    isGeneric: true,
    isFreeShipping: true
  }
];
