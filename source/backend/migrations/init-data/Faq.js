export default [
  {
    id: 1,
    type: 'Shave Plan',
    isSubscription: true,
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 2,
    type: 'Delivery / Shipping',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 3,
    type: 'Returns / Refunds',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 4,
    type: 'Payment',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 5,
    type: 'Technical Issues',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 6,
    type: 'Account',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 7,
    type: 'Products',
    isAlaCarte: true,
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 8,
    type: 'Company Questions',
    CountryId: 1,
    langCode: 'EN'
  },
  {
    id: 9,
    type: 'Shave Plan',
    isSubscription: true,
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 10,
    type: 'Delivery / Shipping',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 11,
    type: 'Returns / Refunds',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 12,
    type: 'Payment',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 13,
    type: 'Technical Issues',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 14,
    type: 'Account',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 15,
    type: 'Products',
    isAlaCarte: true,
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 16,
    type: 'Company Questions',
    CountryId: 7,
    langCode: 'EN'
  },
  {
    id: 17,
    type: '제품',
    isAlaCarte: true,
    CountryId: 8,
    langCode: 'KO'
  },
  {
    id: 18,
    type: '회사문의',
    CountryId: 8,
    langCode: 'KO'
  },
  {
    id: 19,
    type: 'Products',
    isAlaCarte: true,
    CountryId: 8,
    langCode: 'EN'
  },
  {
    id: 20,
    type: 'Company Questions',
    CountryId: 8,
    langCode: 'EN'
  }
];
