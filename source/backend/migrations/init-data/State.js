export default [
  {
    id: 1,
    name: 'Selangor',
    CountryId: 1,
  },
  {
    id: 2,
    name: 'Kuala Lumpur',
    isDefault: true,
    CountryId: 1,
  },
  {
    id: 3,
    name: 'Sarawak',
    CountryId: 1,
  },
  {
    id: 4,
    name: 'Johor',
    CountryId: 1,
  },
  {
    id: 5,
    name: 'Pulau Pinang',
    CountryId: 1,
  },
  {
    id: 6,
    name: 'Sabah',
    CountryId: 1,
  },
  {
    id: 7,
    name: 'Perak',
    CountryId: 1,
  },
  {
    id: 8,
    name: 'Pahang',
    CountryId: 1,
  },
  {
    id: 9,
    name: 'Negeri Sembilan',
    CountryId: 1,
  },
  {
    id: 10,
    name: 'Kedah',
    CountryId: 1,
  },
  {
    id: 11,
    name: 'Melaka',
    CountryId: 1,
  },
  {
    id: 12,
    name: 'Terengganu',
    CountryId: 1,
  },
  {
    id: 13,
    name: 'Kelantan',
    CountryId: 1,
  },
  {
    id: 14,
    name: 'Labuan',
    CountryId: 1,
  },
  {
    id: 15,
    name: 'Perlis',
    CountryId: 1,
  },
  {
    id: 16,
    name: 'Singapore',
    isDefault: true,
    CountryId: 7,
  }
];
