export default [
  {
    id: 1,
    roleName: 'super admin',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique.',
  },
  {
    id: 2,
    roleName: 'admin',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique.',
  },
  {
    id: 3,
    roleName: 'staff',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique.',
  },
  {
    id: 4,
    roleName: 'report',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique.',
  },
];
