export default [
  {
    id: 1,
    moCode: 'IM',
    organization: 'Invention Marketing Group'
  },
  {
    id: 2,
    moCode: 'LO',
    organization: 'Liger Infinity Sdn Bhd'
  },
  {
    id: 3,
    moCode: 'BO',
    organization: 'BEO'
  },
  {
    id: 4,
    moCode: 'CAM',
    organization: 'Appco'
  },
  {
    id: 5,
    moCode: 'OPS',
    organization: 'Appco'
  },
  {
    id: 6,
    moCode: 'AP',
    organization: 'Appco Team'
  }
];
