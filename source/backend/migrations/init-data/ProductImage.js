export default [
  {
    id: 1,
    ProductId: 1,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/efcb9b60-8ec1-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 2,
    ProductId: 1,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/f4f838f0-8ec1-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 3,
    ProductId: 1,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/f735cce0-8ec1-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 4,
    ProductId: 2,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/faa0bd40-8ec1-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 5,
    ProductId: 2,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/ffcfa4c0-8ec1-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 6,
    ProductId: 3,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/01f32100-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 7,
    ProductId: 3,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/05564930-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 8,
    ProductId: 3,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/08738ec0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 9,
    ProductId: 4,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/0c1c4b70-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 10,
    ProductId: 4,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/0ffdf270-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 11,
    ProductId: 4,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/1376ec90-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 12,
    ProductId: 5,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/16cb20a0-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 13,
    ProductId: 5,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/1c194ff0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 14,
    ProductId: 5,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/1f656db0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 15,
    ProductId: 6,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/2337ab60-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 16,
    ProductId: 6,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/26270320-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 17,
    ProductId: 7,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/299093f0-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 18,
    ProductId: 7,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/2c1e1ca0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 19,
    ProductId: 7,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/2f267aa0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 20,
    ProductId: 8,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/32881c30-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 21,
    ProductId: 8,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/33bca490-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 22,
    ProductId: 8,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/348f5de0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 23,
    ProductId: 9,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/366a3860-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 24,
    ProductId: 9,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/3922c8b0-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 25,
    ProductId: 9,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/3e0e3e90-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 26,
    ProductId: 10,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/40b65420-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 27,
    ProductId: 11,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/41a5e440-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 28,
    ProductId: 12,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/42959b70-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 29,
    ProductId: 12,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/44990c90-8ec2-11e7-a8fb-db47f621c82a.jpg'
  },
  {
    id: 30,
    ProductId: 13,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/42959b70-8ec2-11e7-a8fb-db47f621c82a.jpg',
    isDefault: true
  },
  {
    id: 31,
    ProductId: 13,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/44990c90-8ec2-11e7-a8fb-db47f621c82a.jpg'
  }
];
