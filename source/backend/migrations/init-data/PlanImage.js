export default [
  {
    id: 1,
    PlanId: 1,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c251c480-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 2,
    PlanId: 1,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c2b6eef0-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 3,
    PlanId: 2,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c30d4c50-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 4,
    PlanId: 2,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c3629840-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 5,
    PlanId: 3,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c251c480-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 6,
    PlanId: 3,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c2b6eef0-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 7,
    PlanId: 4,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c30d4c50-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 8,
    PlanId: 4,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c3629840-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 9,
    PlanId: 5,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c3bb66a0-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 10,
    PlanId: 5,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c40abf20-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 11,
    PlanId: 6,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c3bb66a0-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 12,
    PlanId: 6,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c40abf20-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 13,
    PlanId: 7,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c251c480-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 14,
    PlanId: 7,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c2b6eef0-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
  {
    id: 15,
    PlanId: 8,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c30d4c50-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: true
  },
  {
    id: 16,
    PlanId: 8,
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/plan-images/c3629840-9d07-11e7-83b9-5d205be3ba1e.jpg',
    isDefault: false
  },
];
