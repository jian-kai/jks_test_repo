
export default [
  {
    id: 1,
    ArticleTypeId: 1,
    bannerUrl: '/uploads/articles/articles1.png',
    hashTag: '#male#shave',
    views: 10,
    postBy: 1,
    title: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',
    content: `- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non metus nec risus tempor elementum eu sed nunc. Suspendisse sit amet ultricies eros. Nam aliquet congue lorem et rutrum.
            Ut mollis dui eget ante varius mollis. Sed a quam lectus. Nam ac justo congue nulla congue semper. Donec lacinia lacus quis varius molestie. Donec at lectus sit amet ex semper fermentum.
            - Aenean vitae sem tristique, ullamcorper nisl nec, congue justo. Mauris non molestie dolor, sit amet dignissim nisl. Duis orci orci, pellentesque vel quam sit amet, mattis tristique ante.
            Fusce eu dolor nisi. Pellentesque vitae justo vitae tellus dictum pretium.
            Duis tempus, tellus nec porta volutpat, neque lorem venenatis nunc, vel bibendum sem ex ut enim. Nullam eu erat vitae velit tempor feugiat at et sapien.
            Pellentesque vel risus egestas ligula sollicitudin lobortis. Integer tincidunt sagittis purus, vitae placerat eros viverra quis. Nulla facilisi.
            - Phasellus vestibulum sollicitudin diam eget porttitor. Sed sed scelerisque ligula. Ut dignissim ornare leo nec hendrerit. Quisque vel magna porta, venenatis lectus nec, fermentum tellus.
            Ut sagittis tempus urna nec condimentum. Proin mi lacus, aliquam at tincidunt eu, ullamcorper mattis justo. Nam non enim at metus hendrerit lacinia.
            Pellentesque quis leo consectetur, porta elit eleifend, pharetra massa.
            Pellentesque urna leo, tempor sed urna non, gravida iaculis sem. Ut sit amet ligula molestie, consectetur est id, dignissim nulla.
            - In commodo justo eget ligula vestibulum, eget accumsan leo sollicitudin. Pellentesque congue auctor diam, sit amet lobortis arcu viverra ac.
            Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut tristique diam in dolor placerat facilisis.
            Aenean non augue ut augue euismod gravida eget sed risus.
            Pellentesque euismod pharetra ante, vitae mattis elit mollis euismod. Morbi ut augue sit amet nunc facilisis convallis non vel justo.
            - Vestibulum ac pellentesque ipsum, quis laoreet dolor. Vestibulum quis tellus sed risus suscipit finibus. Sed vitae lacus at velit hendrerit dapibus at rhoncus justo.
            Vivamus eu semper est, commodo mattis justo. Cras lorem orci, tristique in velit nec, finibus euismod dolor. Nulla vel lacus odio.
            Aliquam erat tortor, dignissim vitae maximus vel, sagittis vel neque.
            Donec vehicula ipsum a mauris pharetra interdum a non quam. Sed eget risus a leo mollis tincidunt. Curabitur tincidunt nulla a lorem malesuada, eu suscipit diam suscipit.
            Fusce quis sollicitudin orci. Nulla bibendum, nunc vel elementum dictum, orci ipsum ullamcorper lacus, sed commodo nisi nulla sed libero.
            Vestibulum lorem felis, imperdiet in maximus at, accumsan at lorem.`
  },
  {
    id: 2,
    ArticleTypeId: 2,
    bannerUrl: '/uploads/articles/articles2.png',
    hashTag: '#female#beauty',
    views: 1,
    postBy: 2,
    title: 'There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...',
    content: `- Donec enim magna, efficitur et turpis ut, pulvinar congue turpis. Pellentesque gravida justo sit amet orci blandit consectetur. Donec luctus posuere odio auctor gravida.
            Suspendisse in nibh in felis facilisis eleifend. Phasellus ac pretium diam, at aliquet elit. In ornare non justo eu iaculis. Integer efficitur lacus metus, eget varius velit pulvinar at.
            Duis tempus, tellus nec porta volutpat, neque lorem venenatis nunc, vel bibendum sem ex ut enim. Nullam eu erat vitae velit tempor feugiat at et sapien.
            Pellentesque vel risus egestas ligula sollicitudin lobortis. Integer tincidunt sagittis purus, vitae placerat eros viverra quis. Nulla facilisi.
            - Phasellus vestibulum sollicitudin diam eget porttitor. Sed sed scelerisque ligula. Ut dignissim ornare leo nec hendrerit. Quisque vel magna porta, venenatis lectus nec, fermentum tellus.
            Ut sagittis tempus urna nec condimentum. Proin mi lacus, aliquam at tincidunt eu, ullamcorper mattis justo. Nam non enim at metus hendrerit lacinia.
            Pellentesque quis leo consectetur, porta elit eleifend, pharetra massa.
            Pellentesque urna leo, tempor sed urna non, gravida iaculis sem. Ut sit amet ligula molestie, consectetur est id, dignissim nulla.`
  },
  {
    id: 3,
    ArticleTypeId: 1,
    bannerUrl: '/uploads/articles/articles3.png',
    hashTag: '#male',
    views: 9,
    postBy: 1,
    title: 'There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...',
    content: `- Donec enim magna, efficitur et turpis ut, pulvinar congue turpis. Pellentesque gravida justo sit amet orci blandit consectetur. Donec luctus posuere odio auctor gravida.
            Suspendisse in nibh in felis facilisis eleifend. Phasellus ac pretium diam, at aliquet elit. In ornare non justo eu iaculis. Integer efficitur lacus metus, eget varius velit pulvinar at.
            Duis tempus, tellus nec porta volutpat, neque lorem venenatis nunc, vel bibendum sem ex ut enim. Nullam eu erat vitae velit tempor feugiat at et sapien.
            Pellentesque vel risus egestas ligula sollicitudin lobortis. Integer tincidunt sagittis purus, vitae placerat eros viverra quis. Nulla facilisi.
            - Phasellus vestibulum sollicitudin diam eget porttitor. Sed sed scelerisque ligula. Ut dignissim ornare leo nec hendrerit. Quisque vel magna porta, venenatis lectus nec, fermentum tellus.
            Ut sagittis tempus urna nec condimentum. Proin mi lacus, aliquam at tincidunt eu, ullamcorper mattis justo. Nam non enim at metus hendrerit lacinia.
            Pellentesque quis leo consectetur, porta elit eleifend, pharetra massa.
            Pellentesque urna leo, tempor sed urna non, gravida iaculis sem. Ut sit amet ligula molestie, consectetur est id, dignissim nulla.`
  }
];
