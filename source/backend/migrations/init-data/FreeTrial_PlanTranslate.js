export default [
  {
    // id: 9,
    PlanId: 9,
    name: 'S3-2mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S3 Cartridge',
    shortDescription: '6 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 10,
    PlanId: 10,
    name: 'S5-2mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S5 Cartridge',
    shortDescription: '6 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  // {
  //   // id: 11,
  //   PlanId: 11,
  //   name: 'S6 - 2 - Months',
  //   description: '- 1 cassette every 2 months.\n- 1 X Men’s Razor Handle.\n- 1 X S6 Cassettes',
  //   shortDescription: '6 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
  {
    // id: 12,
    PlanId: 12,
    name: 'S3-3mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S3 Cartridge',
    shortDescription: '4 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 13,
    PlanId: 13,
    name: 'S5-3mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S5 Cartridge',
    shortDescription: '4 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  // {
  //   // id: 14,
  //   PlanId: 14,
  //   name: 'S6 - 3 - Months',
  //   description: '- 1 cassette every 3 months.\n- 1 X Men’s Razor Handle.\n- 1 X S6 Cassettes',
  //   shortDescription: '4 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
  {
    // id: 15,
    PlanId: 15,
    name: 'S3-4mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S3 Cartridge',
    shortDescription: '3 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 16,
    PlanId: 16,
    name: 'S5-4mth-paygo',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 1 x S5 Cartridge',
    shortDescription: '3 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  // {
  //   // id: 17,
  //   PlanId: 17,
  //   name: 'S6 - 4 - Months',
  //   description: '- 1 cassette every 4 months.\n- 1 X Men’s Razor Handle.\n- 1 X S6 Cassettes',
  //   shortDescription: '3 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
  {
    // id: 18,
    PlanId: 18,
    name: 'S3-annual-6CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 6 x S3 Cartridge',
    shortDescription: '6 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 21,
    PlanId: 21,
    name: 'S5-annual-6CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 6 x S5 Cartridge',
    shortDescription: '6 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 19,
    PlanId: 19,
    name: 'S3-annual-4CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 4 x S3 Cartridge',
    shortDescription: '4 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 22,
    PlanId: 22,
    name: 'S5-annual-4CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 4 x S5 Cartridge',
    shortDescription: '4 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 20,
    PlanId: 20,
    name: 'S3-annual-3CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 3 x S3 Cartridge',
    shortDescription: '3 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  {
    // id: 23,
    PlanId: 23,
    name: 'S5-annual-3CP',
    description: '#### Trial Pack\n- Men\'s Starter Pack\n- Shaving Cream\n#### Subsequent Deliveries\n- 3 x S5 Cartridge',
    shortDescription: '3 Cartridge Packs/year',
    langCode: 'EN',
    isDefault: true
  },
  // {
  //   // id: 24,
  //   PlanId: 24,
  //   name: 'S6 - Year - for 2 months',
  //   description: '- 6 cassette every 1 year.\n- 1 X Men’s Razor Handle.\n- 6 X S6 Cassettes',
  //   shortDescription: '6 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
  // {
  //   // id: 25,
  //   PlanId: 25,
  //   name: 'S6 - Year - for 3 months',
  //   description: '- 4 cassette every 1 year.\n- 1 X Men’s Razor Handle.\n- 4 X S6 Cassettes',
  //   shortDescription: '4 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
  // {
  //   // id: 26,
  //   PlanId: 26,
  //   name: 'S6 - Year - for 4 months',
  //   description: '- 3 cassette every 1 year.\n- 1 X Men’s Razor Handle.\n- 3 X S6 Cassettes',
  //   shortDescription: '3 Cartridge Packs/year',
  //   langCode: 'EN',
  //   isDefault: true
  // },
];
