export default [
  {
    // id: 9,
    sku: '2-TK3',
    PlanTypeId: 4,
    slug: '2-TK3',
    isTrial: 1,
    order: 6
  },
  {
    // id: 10,
    sku: '2-TK5',
    PlanTypeId: 4,
    slug: '2-TK5',
    isTrial: 1,
    order: 5
  },
  // {
  //   // id: 11,
  //   sku: '2-Trial-S6',
  //   PlanTypeId: 4,
  //   slug: '2-Trial-S6',
  //   isTrial: 1
  // },
  {
    // id: 12,
    sku: '3-TK3',
    PlanTypeId: 5,
    slug: '3-TK3',
    isTrial: 1,
    order: 4
  },
  {
    // id: 13,
    sku: '3-TK5',
    PlanTypeId: 5,
    slug: '3-TK5',
    isTrial: 1,
    order: 3
  },
  // {
  //   // id: 14,
  //   sku: '3-Trial-S6',
  //   PlanTypeId: 5,
  //   slug: '3-Trial-S6',
  //   isTrial: 1
  // },
  {
    // id: 15,
    sku: '4-TK3',
    PlanTypeId: 6,
    slug: '4-TK3',
    isTrial: 1,
    order: 2
  },
  {
    // id: 16,
    sku: '4-TK5',
    PlanTypeId: 6,
    slug: '4-TK5',
    isTrial: 1,
    order: 1
  },
  // {
  //   // id: 17,
  //   sku: '4-Trial-S6',
  //   PlanTypeId: 6,
  //   slug: '4-Trial-S6',
  //   isTrial: 1
  // },
  {
    // id: 18,
    sku: '2-Y-TK3',
    PlanTypeId: 7,
    slug: '2-Y-TK3',
    isTrial: 1
  },
  {
    // id: 21,
    sku: '2-Y-TK5',
    PlanTypeId: 7,
    slug: '2-Y-TK5',
    isTrial: 1
  },
  {
    // id: 19,
    sku: '3-Y-TK3',
    PlanTypeId: 7,
    slug: '3-Y-TK3',
    isTrial: 1
  },
  {
    // id: 22,
    sku: '3-Y-TK5',
    PlanTypeId: 7,
    slug: '3-Y-TK5',
    isTrial: 1
  },
  {
    // id: 20,
    sku: '4-Y-TK3',
    PlanTypeId: 7,
    slug: '4-Y-TK3',
    isTrial: 1
  },
  {
    // id: 23,
    sku: '4-Y-TK5',
    PlanTypeId: 7,
    slug: '4-Y-TK5',
    isTrial: 1
  },
  // {
  //   // id: 24,
  //   sku: '2-Trial-Y-S6',
  //   PlanTypeId: 7,
  //   slug: '2-Trial-Y-S6',
  //   isTrial: 1
  // },
  // {
  //   // id: 25,
  //   sku: '3-Trial-Y-S6',
  //   PlanTypeId: 7,
  //   slug: '3-Trial-Y-S6',
  //   isTrial: 1
  // },
  // {
  //   // id: 26,
  //   sku: '4-Trial-Y-S6',
  //   PlanTypeId: 7,
  //   slug: '4-Trial-Y-S6',
  //   isTrial: 1
  // },
];
