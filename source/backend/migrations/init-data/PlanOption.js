export default [
  {
    // id: 1,
    description: '1 Cartridge, 1 Shave Cream',
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/c4e96620-c616-11e7-8020-836884d9c08a.png',
    actualPrice: 42.00,
    sellPrice: 42.00,
    pricePerCharge: 42.00,
    savePercent: 0.00,
    PlanCountryId: 9,
    hasShaveCream: 1

  },
  {
    // id: 2,
    description: '1 Cartridge Only',
    url: 'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/08738ec0-8ec2-11e7-a8fb-db47f621c82a.jpg',
    actualPrice: 27.00,
    sellPrice: 27.00,
    pricePerCharge: 27.00,
    savePercent: 0.00,
    PlanCountryId: 9,
    hasShaveCream: 0
  },
];
