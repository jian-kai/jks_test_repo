export default [
  {
    id: 1,
    CountryId: 1,
    badgeId: 'DO00001',
    icNumber: 'S9217590A',
    agentName: 'JASMINE LIM',
    startDate: '2017-12-12'
  },
  {
    id: 2,
    CountryId: 1,
    badgeId: 'DO00003',
    icNumber: 'S9303682D',
    agentName: 'LIM SI HUI CASSANDRA',
    startDate: '2017-12-12'
  },
  {
    id: 3,
    CountryId: 1,
    badgeId: 'DO00012',
    icNumber: 'S9422690B',
    agentName: 'YEO WEI ZHONG',
    startDate: '2017-12-12'
  },
  {
    id: 4,
    CountryId: 1,
    badgeId: 'DO00013',
    icNumber: 'S9313599G',
    agentName: 'WONG KIN HENG',
    startDate: '2017-12-12'
  },
  {
    id: 5,
    CountryId: 1,
    badgeId: 'DO00018',
    icNumber: 'S9428722G',
    agentName: 'NG YU YANG',
    startDate: '2017-12-12'
  }
];
