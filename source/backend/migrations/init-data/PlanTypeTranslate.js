
export default [
  {
    id: 1,
    PlanTypeId: 1,
    langCode: 'EN',
    name: 'Annual',
  },
  {
    id: 2,
    PlanTypeId: 2,
    langCode: 'EN',
    name: 'Quarterly',
  },
  {
    id: 3,
    PlanTypeId: 3,
    langCode: 'EN',
    name: 'Monthly',
  },
  {
    id: 4,
    PlanTypeId: 4,
    langCode: 'EN',
    name: '2 Months',
  },
  {
    id: 5,
    PlanTypeId: 5,
    langCode: 'EN',
    name: '3 Months',
  },
  {
    id: 6,
    PlanTypeId: 6,
    langCode: 'EN',
    name: '4 Months',
  },
  {
    id: 7,
    PlanTypeId: 7,
    langCode: 'EN',
    name: '12 Months',
  },
  {
    id: 8,
    PlanTypeId: 1,
    langCode: 'KO',
    name: '일년생 식물',
  },
  {
    id: 9,
    PlanTypeId: 2,
    langCode: 'KO',
    name: '계간지',
  },
  {
    id: 10,
    PlanTypeId: 3,
    langCode: 'KO',
    name: '월간 간행물',
  },
  {
    id: 11,
    PlanTypeId: 4,
    langCode: 'KO',
    name: '2 개월',
  },
  {
    id: 12,
    PlanTypeId: 5,
    langCode: 'KO',
    name: '3 개월',
  },
  {
    id: 13,
    PlanTypeId: 6,
    langCode: 'KO',
    name: '4 개월',
  },
  {
    id: 14,
    PlanTypeId: 7,
    langCode: 'KO',
    name: '12 개월',
  }
];
