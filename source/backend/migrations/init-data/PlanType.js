
export default [
  {
    id: 1,
    name: 'Annual',
    totalDeliverTimes: 4,
    subsequentDeliverDuration: '3 months',
    totalChargeTimes: 1,
    subsequentChargeDuration: '12 months',
    prefix: 'Y-Sub-'
  },
  {
    id: 2,
    name: 'Quarterly',
    totalDeliverTimes: 4,
    subsequentDeliverDuration: '3 months',
    totalChargeTimes: 4,
    subsequentChargeDuration: '3 months',
    prefix: 'Sub-'
  },
  {
    id: 3,
    name: 'Monthly',
    totalDeliverTimes: 4,
    subsequentDeliverDuration: '3 months',
    totalChargeTimes: 12,
    subsequentChargeDuration: '1 months',
    prefix: 'M-Sub-'
  },
];
