export default [
  // {
  //   id: 1,
  //   pathValue: 'login',
  // },
  {
    id: 1,
    pathValue: 'orders/pending',
  },
  {
    id: 2,
    pathValue: 'orders/history',
  },
  {
    id: 3,
    pathValue: 'bulk-orders',
  },
  {
    id: 4,
    pathValue: 'products',
  },
  {
    id: 5,
    pathValue: 'products/category',
  },
  {
    id: 6,
    pathValue: 'subscriptions/subscribers',
  },
  {
    id: 7,
    pathValue: 'subscriptions/subscribers-free-trial',
  },
  {
    id: 8,
    pathValue: 'subscriptions/plan-types',
  },
  {
    id: 9,
    pathValue: 'subscriptions/plans',
  },
  {
    id: 10,
    pathValue: 'promotions',
  },
  {
    id: 11,
    pathValue: 'countries',
  },
  {
    id: 12,
    pathValue: 'users/staffs',
  },
  {
    id: 13,
    pathValue: 'users/customers',
  },
  {
    id: 14,
    pathValue: 'users/sample-product',
  },
  {
    id: 15,
    pathValue: 'reports',
  },
  // {
  //   id: 17,
  //   pathValue: 'settings',
  // },
  {
    id: 16,
    pathValue: 'admin/reset-password',
  },
  {
    id: 17,
    pathValue: 'users/roles',
  },
  {
    id: 18,
    pathValue: 'permission/paths',
  },
  {
    id: 19,
    pathValue: 'orders/upload',
  },
  {
    id: 20,
    pathValue: 'utils/faq',
  },
  {
    id: 21,
    pathValue: 'utils/terms',
  },
  {
    id: 22,
    pathValue: 'utils/privacy',
  }
];
