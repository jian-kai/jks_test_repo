import Promise from 'bluebird';
import { awsHelper } from '../src/helpers/aws-helper';
import config from '../src/config';

const IMAGE_PATH = '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/migrations/init-data/planImages/';
const IMAGE_NAME = [
  'subscriptions-s3-main_1.jpg',
  's3-cartridge_1_2.jpg',
  'subscriptions-s5-main_1.jpg',
  's5-cartridge_1_1.jpg',
  'subscriptions-f5-main_1.jpg',
  'women-f5-cassettes_3.jpg'
];

Promise.mapSeries(IMAGE_NAME, fileName => awsHelper.upload(config.AWS.planImagesAlbum, {
  path: `${IMAGE_PATH}${fileName}`,
  originalFilename: fileName
})
  .then(uploadFile => {
    console.log(`uploaded file ${uploadFile.Location}`);
    return uploadFile;
  })
)
  .then(uploadFiles => {
    console.log('Finish upload file finish');
    uploadFiles.forEach((file, idx) => {
      console.log(`File: ${file.Location}, ${IMAGE_NAME[idx]}`);
    });
  })
  .catch(err => console.log(`ERR: ${err.stack}`));
