# Initialize database structure and data

## Precondition

- Install babel

    ```
    npm install babel -g
    ```

- Install babel-cli and helper library

    ```
    npm install --save-dev babel-cli babel-core babel-preset-es2015 babel-preset-stage-0 nodemon
    ```

- Install bluebird to handle Promise asynchronus task

    ```
    npm intall --save-dev bluebird
    ```

## Init Database ##

<span style="color:#ff7979; font-weight: bold;">**Note: this action will be drop all table and create new one**</span>

```
npm run sync-db
```

## Init Data ##

<span style="color:#ff7979; font-weight: bold;">**Note: this action will be truncate all data in table**</span>

```
npm run init-data
```