'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
export default {
  oldDb: {
    MYS: {
      host: 'localhost',
      user: 'shave2u',
      password: 'Admin@123',
      database: 'sg-live',
      pool: {
        max: 5,
        min: 0,
        idle: 10000 
      }
    },
    SGP: {
      host: 'localhost',
      user: 'shave2u',
      password: 'Admin@123',
      database: 'sg-live',
      pool: {
        max: 5,
        min: 0,
        idle: 10000 
      }
    }
  }
};
