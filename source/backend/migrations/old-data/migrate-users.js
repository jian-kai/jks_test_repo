import Promise from 'bluebird';
import initializeDb from '../../src/db';
import Sequelize from 'sequelize';
import config from './config';
import _ from 'underscore';
import countryData from '../init-data/Country';

// establish connection to old DB (SGP)
let sequelizeOldMYS = new Sequelize(config.oldDb.MYS.database, config.oldDb.MYS.user, config.oldDb.MYS.password, {
  host: config.oldDb.MYS.host,
  dialect: 'mysql',
  pool: config.oldDb.MYS.pool,
  define: {
    timestamps: true
  }
});

let sequelizeOldSGP = new Sequelize(config.oldDb.SGP.database, config.oldDb.SGP.user, config.oldDb.SGP.password, {
  host: config.oldDb.SGP.host,
  dialect: 'mysql',
  pool: config.oldDb.SGP.pool,
  define: {
    timestamps: true
  }
});

let sequelizeOld = {
  MYS: sequelizeOldMYS,
  SGP: sequelizeOldSGP
};

// query data from old DB
function getSgpUser(countryCode) {
  // get user entry varchar
  let data = {};
  return sequelizeOld[countryCode].query(
    `SELECT DISTINCT(entity_id)
      FROM customer_entity
      GROUP BY entity_id
      LIMIT 10`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
  )
    .then(entityIds => {
      data.entityIds = _.pluck(entityIds, 'entity_id');
      return sequelizeOld[countryCode].query(
        `SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at, ea.attribute_code, cev.value
          FROM customer_entity ce
          INNER JOIN customer_entity_varchar cev ON ce.entity_id = cev.entity_id
          INNER JOIN eav_attribute ea ON cev.attribute_id = ea.attribute_id
          WHERE ce.entity_id IN (${data.entityIds})`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
      );
    })
    .then(result => {
      data.customerEntryVarchar = result;
      return sequelizeOld[countryCode].query(
        `SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at, ea.attribute_code, cei.value
          FROM customer_entity ce
          INNER JOIN customer_entity_int cei ON ce.entity_id = cei.entity_id
          INNER JOIN eav_attribute ea ON cei.attribute_id = ea.attribute_id
          WHERE ce.entity_id IN (${data.entityIds})`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
      );
    })
    .then(result => {
      data.customerEntryInt = result;
      return sequelizeOld[countryCode].query(
        `SELECT ce.entity_id, ce.is_active, ce.email, ce.created_at, ce.updated_at, ea.attribute_code, ced.value
          FROM customer_entity ce
          INNER JOIN customer_entity_datetime ced ON ce.entity_id = ced.entity_id
          INNER JOIN eav_attribute ea ON ced.attribute_id = ea.attribute_id
          WHERE ce.entity_id IN (${data.entityIds})`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
      );
    })
    .then(result => {
      data.customerEntryDateTime = result;
      return sequelizeOld[countryCode].query(
        `SELECT cae.entity_id, cae.is_active, cae.parent_id, cae.created_at, cae.updated_at, ea.attribute_code, caet.value
          FROM customer_address_entity cae
          INNER JOIN customer_address_entity_text caet ON cae.entity_id = caet.entity_id
          INNER JOIN eav_attribute ea ON caet.attribute_id = ea.attribute_id
          WHERE cae.parent_id IN (${data.entityIds})`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
      );
    })
    .then(result => {
      data.customerAddressEntryText = result;
      return sequelizeOld[countryCode].query(
        `SELECT cae.entity_id, cae.is_active, cae.parent_id, cae.created_at, cae.updated_at, ea.attribute_code, caev.value
          FROM customer_address_entity cae
          INNER JOIN customer_address_entity_varchar caev ON cae.entity_id = caev.entity_id
          INNER JOIN eav_attribute ea ON caev.attribute_id = ea.attribute_id
          WHERE cae.parent_id IN (${data.entityIds})`, { type: sequelizeOld[countryCode].QueryTypes.SELECT}
      );
    })
    .then(result => {
      data.customerAddressEntryVarchar = result;
      // mapping data to new DB
      let userData = [];
      data.entityIds.forEach(entityId => {
        let user = {id: entityId};
        let userInfos = _.union(_.where(data.customerEntryVarchar, {'entity_id': entityId}), _.where(data.customerEntryInt, {'entity_id': entityId}), _.where(data.customerEntryDateTime, {'entity_id': entityId}));
        let userAddressInfos = _.union(_.where(data.customerAddressEntryText, {'parent_id': entityId}), _.where(data.customerAddressEntryVarchar, {'parent_id': entityId}));

        // build user info
        user.email = userInfos[0].email;
        user.password = 'migration';
        user.isActive = userInfos[0].is_active;
        user.createdAt = userInfos[0].created_at;
        user.updatedAt = userInfos[0].updated_at;
        userInfos.forEach(userInfo => {
          switch (userInfo.attribute_code) {
          case 'firstname':
            user.firstName = userInfo.value;
            break;
          case 'lastname':
            user.lastName = userInfo.value;
            break;
          case 'default_billing':
            user.defaultBilling = userInfo.value;
            break;
          case 'default_shipping':
            user.defaultShipping = userInfo.value;
            break;
          }
        });
        // build delivery address
        user.deliveryAddresses = [];
        let deliveryAddress = {};
        let currentAddressEntiryId;
        userAddressInfos = _.sortBy(userAddressInfos, elem => elem.entity_id);
        userAddressInfos.forEach((userAddress, idx) => {
          if((currentAddressEntiryId && currentAddressEntiryId !== userAddress.entity_id) || (idx === userAddressInfos.length - 1)) {
            user.deliveryAddresses.push(Object.assign({}, deliveryAddress));
            deliveryAddress = {};
          }
          currentAddressEntiryId = userAddress.entity_id;
          deliveryAddress.id = userAddress.entity_id;
          switch (userAddress.attribute_code) {
          case 'firstname':
            deliveryAddress.firstName = userAddress.value;
            break;
          case 'lastname':
            deliveryAddress.lastName = userAddress.value;
            break;
          case 'company':
            deliveryAddress.company = userAddress.value;
            break;
          case 'city':
            deliveryAddress.city = userAddress.value;
            break;
          case 'country_id':
            deliveryAddress.countryId = _.findWhere(countryData, {iso2: userAddress.value}).id;
            break;
          case 'region':
            deliveryAddress.state = userAddress.value;
            break;
          case 'postcode':
            deliveryAddress.portalCode = userAddress.value;
            break;
          case 'telephone':
            deliveryAddress.contactNumber = userAddress.value ? userAddress.value : '';
            break;
          case 'street':
            deliveryAddress.address = userAddress.value;
            break;
          }
        });

        userData.push(user);
      });
      console.log(`userData ==== ${JSON.stringify(userData)}`);
      return userData;
    });
}
// insert data to new DB
let currentDb;
initializeDb()
  .then(db => {
    currentDb = db;
    return db.sequelize.transaction(t => currentDb.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, {transaction: t})
      .then(() => currentDb.DeliveryAddress.destroy({ truncate: true, cascade: true }))
      .then(() => currentDb.User.destroy({ truncate: true, cascade: true }))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, {transaction: t}))
    );
  })
  .then(() => getSgpUser('MYS'))
  .then(users => Promise.map(users, user => currentDb.User.create(user, {include: ['deliveryAddresses'], returning: true})))
  .then(users => console.log(`user ==== ${JSON.stringify(users)}`))
  .catch(err => console.log(err));
