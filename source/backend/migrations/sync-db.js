import initializeDb from '../src/db';

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(db => {
    return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, null)
      .then(() => db.sequelize.sync({ force: true }))
      .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, null))
      .then(() => {
        console.log('Sync database success!');
      });
  });
