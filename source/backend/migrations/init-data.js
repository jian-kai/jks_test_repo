import Promise from 'bluebird';
import initializeDb from '../src/db';
import request from 'request';

const countryServiceUrl = 'https://restcountries.eu/rest/v2/all';
const DATA_PATH = './init-data/';
const INIT_TABLES = [
  'Country',
  'ArticleType',
  'Admin',
  'ProductType',
  'ProductTypeTranslate',
  'PlanType',
  'Product',
  'ProductTranslate',
  'ProductImage',
  'ProductCountry',
  'ProductDetail',
  'User',
  'Cart',
  'DeliveryAddress',
  'Article',
  'Plan',
  'PlanImage',
  'PlanDetail',
  'PlanTranslate',
  'PlanCountry',
  'NotificationType',
  'Promotion',
  'PromotionCode',
  'PromotionTranslate',
  'Faq',
  'FaqTranslate',
  'PlanTrialProduct',
  'Role',
  'RoleDetail',
  'Path',
  'OrderUpload',
  'MarketingOffice'
];

let db;

function getCountryDetails() {
  return new Promise((resolve, reject) => {
    let options = {
      url: countryServiceUrl,
      method: 'GET'
    };
    request(options, (error, response, body) => {
      if(!error && response.statusCode === 200) {
        resolve(JSON.parse(body));
      } else {
        reject(error);
      }
    });
  });
}

/**
 * Force initialize database. the old table will be replace with new structure
 */
initializeDb()
  .then(_db => { // truncate all data
    db = _db;
    return db.sequelize.transaction(t => {
      let options = { raw: true, transaction: t };
      return db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, options)
        .then(() => Promise.mapSeries(INIT_TABLES, 
          table => db[table].truncate({ cascade: true })
        ))
        .then(() => Promise.mapSeries(INIT_TABLES,
          table => { // insert new data
            let data = require(`${DATA_PATH}${table}`).default;
            return db[table].bulkCreate(data);
          }
        ))
        .then(() => db.DirectoryCountry.truncate({cascade: true}))
        .then(() => getCountryDetails())
        .then(countries => {
          console.log(`get countries ${countries.length} === ${JSON.stringify(countries[0])}`);
          return Promise.map(countries, country => {
            if(country.currencies[0].code) {
              return db.DirectoryCountry.create({
                code: country.alpha3Code,
                codeIso2: country.alpha2Code,
                name: country.name,
                nativeName: country.nativeName,
                currencyCode: country.currencies[0].code,
                languageCode: country.languages[0].iso639_1 || 'en',
                flag: country.flag,
                callingCode: country.callingCodes[0]
              });
            }
            return;
          });
        })
        .then(() => db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options));
    });
  })
  .then(() => {
    console.log('initialize data is success!!!');
  })
  .catch(err => {
    console.log(`can't initilize data ${err.stack || JSON.stringify(err)}`);
  });
