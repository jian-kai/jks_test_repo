import moment from 'moment';
import config from '../config';
import { jobs } from './jobs';
import { loggingHelper } from '../helpers/logging-helper';

class Scheduler {

  constructor(db) {
    this.db = db;
  }

  setTimeoutJob(job) {
    if(job.startAt) {
      // calculate start time
      let intervalTime;
      if(job.startAt === moment(job.startAt, 'ddd hh:mm:SS').format('ddd HH:mm:SS')) {
        // ddd hh:mm:SS -> day of week (e.g Mon 20:00:00 = Monday 20:00:00)
        intervalTime = moment(job.startAt, 'ddd hh:mm:SS').diff(moment());
      } else if(moment(job.startAt, 'hh:mm:SS').isValid()) {
        intervalTime = moment(job.startAt, 'hh:mm:SS').diff(moment());
      } else if(moment(job.startAt, 'DD hh:mm:SS').isValid()) {
        intervalTime = moment(job.startAt, 'DD hh:mm:SS').diff(moment());
      } else if(moment(job.startAt, 'MM-DD hh:mm:SS').isValid()) {
        intervalTime = moment(job.startAt, 'MM-DD hh:mm:SS').diff(moment());
      } else {
        intervalTime = 0;
      }

      console.log(`intervalTime === ${intervalTime}`);
      // run the this task immediatly if startat time is over
      intervalTime = intervalTime < 0 ? job.interval + intervalTime : intervalTime;
      console.log(`intervalTime === ${intervalTime}`);

      // remove startAt attribute for next time
      Reflect.deleteProperty(job, 'startAt');

      // set timeout to start this job again
      return setTimeout(this.executeJobSchedule.bind(this, job), intervalTime);
    } else {
      return setTimeout(this.executeJobSchedule.bind(this, job), job.interval);
    }
  }

  executeJobSchedule(job) {
    if(config.jobConfig[job.name]) {
      job.target.process(this.db).then(() => this.setTimeoutJob(job))
      .catch(err => {
        console.log(err);
        loggingHelper.log('error', `Execute job ERROR: ${err}`);
        this.setTimeoutJob(job);
      });
    }
  }

  startJobs() {
    jobs.forEach(job => {
      this.setTimeoutJob(job);
    });
  }

}

export default Scheduler;
