import Promise from 'bluebird';
import WareHouseHelper from '../../helpers/warehouse-helper';
import OrderInboundQueueService from '../../services/create-order-inbound.service';
import ReadOrderOutboundJob from './read-order-outbound.job';
import CreateBulkOrderOutboundJob from './create-bulk-order-inbound.job';
import { loggingHelper } from '../../helpers/logging-helper';
import config from '../../config';
import UrbanFoxHelper from '../../helpers/urbanfox-helpers';

class CreateOrderInboundJob {
  constructor() {
    console.log('create new instance update product quantity job');
    loggingHelper.log('info', 'create new instance update product quantity job');
    this.readOrderOutboundJob = new ReadOrderOutboundJob();
    this.createBulkOrderOutboundJob = new CreateBulkOrderOutboundJob();
  }

  process(db) {
    // loggingHelper.log('info', 'create-order-inbound.job process start');
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    this.db = db;
    let orderIds;
    // get keys in update product queue
    return orderInboundQueueService.getKeysInQueue()
      .then(_orderIds => {
        if(_orderIds && _orderIds.length === 0) {
          // loggingHelper.log('error', 'Create order inbound job: queue_empty'); 
          throw new Error('queue_empty');
        }
        console.log(`orderInboundQueueService.getKeysInQueue ===== ${_orderIds}`);
        // only process x record per times
        if(_orderIds.length > config.warehouse.maxFiles) {
          orderIds = _orderIds.slice(0, config.warehouse.maxFiles);
        } else {
          orderIds = _orderIds;
        }

        return Promise.mapSeries(orderIds, data => {
          data = JSON.parse(data);
          if(data.retry <= config.warehouse.retry && data.countryId !== 7) {
            return WareHouseHelper.createOrderInbound(data.orderId, this.db)
              .then(() => Promise.delay(1001))
              .then(() => JSON.stringify(data))
              .catch(err => {
                loggingHelper.log('error', `createOrderInbound data:${JSON.stringify(data)} === ${err}`);
                // retry 
                orderInboundQueueService.removeKeyFromQueue(JSON.stringify(data));
                data.retry += 1;
                orderInboundQueueService.addTask(data);
              });
          } else if(data.retry <= config.warehouse.retry && data.countryId === 7) {
            return UrbanFoxHelper.createTransactions(data.orderId, this.db)
              .then(() => Promise.delay(1001))
              .then(() => JSON.stringify(data))
              .catch(err => {
                loggingHelper.log('error', `createOrderInbound data:${JSON.stringify(data)} === ${err}`);
                // retry 
                orderInboundQueueService.removeKeyFromQueue(JSON.stringify(data));
                data.retry += 1;
                orderInboundQueueService.addTask(data);
              });
          } else {
            return JSON.stringify(data);
          }
        });
      })
      .then(orderIdsCreated => orderInboundQueueService.removeKeysFromQueue(orderIdsCreated))
      .then(() => this.createBulkOrderOutboundJob.process(db))
      .then(() => this.readOrderOutboundJob.process(db))
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `create-order-inbound job: ${error}`); 
          throw error;
        }
        return this.createBulkOrderOutboundJob.process(db)
          .then(() => this.readOrderOutboundJob.process(db));
      })
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `create-order-inbound job: ${error}`); 
          throw error;
        }
        return this.readOrderOutboundJob.process(db);
      })
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `create-order-inbound job: ${error}`); 
        }
      });
  }
}

export default CreateOrderInboundJob;
