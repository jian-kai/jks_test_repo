import Promise from 'bluebird';
import moment from 'moment';
import SendEmailQueueService from '../../services/send-email.queue.service';
import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';

class SendFollowUpTrial1 {
  constructor() {
    console.log('create new instance SendFollowUpTrial1');
  }

  process(db) {
    this.db = db;
    // get the subscription to prepare for start
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let followUpEmail1 = config.trialPlan.followUpEmail1;
    let startFirstDelivery = config.trialPlan.startFirstDelivery;
    return this.db.Subscription.findAll({where: {
      hasSendFollowUp1: false,
      status: 'processing',
      isTrial: true,
      // createdAt: {$lte: moment().subtract(followUpEmail1.split(' ')[0], followUpEmail1.split(' ')[1]).format('YYYY-MM-DD')}
      nextDeliverDate: {$lte: moment()
                        .add(startFirstDelivery.split(' ')[0], startFirstDelivery.split(' ')[1])
                        .subtract(followUpEmail1.split(' ')[0], followUpEmail1.split(' ')[1]).format('YYYY-MM-DD')}
    }, include: [
      {
        model: db.User,
        include: ['Country']
      },
      {
        model: db.PlanCountry,
        include: ['Country']
      }
    ]})
      .then(subscriptions => Promise.map(subscriptions, subscription => sendEmailQueueService.addTask({method: 'sendFollowUpTrialEmail', data: {id: subscription.id, type: 1}, langCode: subscription.PlanCountry.Country.defaultLang.toLowerCase()})
        .then(() => this.db.Subscription.update({hasSendFollowUp1: true}, {where: {id: subscription.id}}))
        .catch(error => {
          console.log(`Send Follow up email 1 [${subscription.id}] error: ${error.stack}`);
          loggingHelper.log('error', `Send Follow up email 1 [${subscription.id}] error: ${error.stack}`); 
          throw error;
        })))
      .catch(error => {
        console.log(`SendFollowUpTrial1 ERROR = ${error.stack}`);
        loggingHelper.log('error', `SendFollowUpTrial1 ERROR = ${error.stack}`); 
      });
  }
}

export default SendFollowUpTrial1;
