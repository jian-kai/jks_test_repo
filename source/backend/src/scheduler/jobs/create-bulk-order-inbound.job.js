import Promise from 'bluebird';
import WareHouseHelper from '../../helpers/warehouse-helper';
import BulkOrderInboundQueueService from '../../services/create-bulk-order-inbound.service';
import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';
import UrbanFoxHelper from '../../helpers/urbanfox-helpers';

class CreateBulkOrderOutboundJob {
  constructor() {
    console.log('create new bulk order inbound job');
    loggingHelper.log('info', 'create new bulk order inbound job');
  }

  process(db) {
    // loggingHelper.log('info', 'create-bulk-order-inbound.job process start');
    let bulkOrderInboundQueueService = BulkOrderInboundQueueService.getInstance();
    this.db = db;
    let orderIds;
    // get keys in update product queue
    return bulkOrderInboundQueueService.getKeysInQueue()
      .then(_orderIds => {
        console.log(`create bulk order job ===== ${JSON.stringify(_orderIds)}`);
        if(_orderIds && _orderIds.length === 0) {
          // loggingHelper.log('error', 'queue_empty');
          throw new Error('queue_empty');
        }

        // only process x record per times
        if(_orderIds.length > config.warehouse.maxFiles) {
          orderIds = _orderIds.slice(0, config.warehouse.maxFiles);
        } else {
          orderIds = _orderIds;
        }
        return Promise.mapSeries(orderIds, data => {
          console.log(`bulkOrderInboundQueueService === ${data} == config.warehouse.retry`);
          data = JSON.parse(data);
          if(data.retry <= config.warehouse.retry && data.countryId !== 7) {
            return WareHouseHelper.createBulkOrderInbound(data.orderId, this.db)
            .then(() => Promise.delay(1001))
            .then(() => JSON.stringify(data))
            .catch(err => {
              loggingHelper.log('error', `createBulkOrderInbound data:${JSON.stringify(data)} === ${err}`);
              // retry 
              bulkOrderInboundQueueService.removeKeyFromQueue(JSON.stringify(data));
              data.retry = data.retry + 1;
              bulkOrderInboundQueueService.addTask(data);
            });
          } else if(data.retry <= config.warehouse.retry && data.countryId === 7) {
            return UrbanFoxHelper.createBulkTransactions(data.orderId, this.db)
              .then(() => Promise.delay(1001))
              .then(() => JSON.stringify(data))
              .catch(err => {
                loggingHelper.log('error', `CreateBulkOrderOutboundJob data:${JSON.stringify(data)} === ${err}`);
                // retry 
                bulkOrderInboundQueueService.removeKeyFromQueue(JSON.stringify(data));
                data.retry += 1;
                bulkOrderInboundQueueService.addTask(data);
              });
          } else {
            return JSON.stringify(data);
          }
        });
      })
      .then(createdBulkOrderIds => bulkOrderInboundQueueService.removeKeysFromQueue(createdBulkOrderIds))
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `create-bulk-order-inbound job: ${error}`);
        }
      });
  }
}

export default CreateBulkOrderOutboundJob;
