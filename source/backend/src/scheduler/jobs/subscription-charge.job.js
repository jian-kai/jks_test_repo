import Promise from 'bluebird';
import moment from 'moment';
import SendEmailQueueService from '../../services/send-email.queue.service';
import { stripeHelper } from '../../helpers/stripe-helper';
import { loggingHelper } from '../../helpers/logging-helper';

class SubsciptionChargeJob {
  constructor() {
    console.log('create new instance SubsciptionChargeJob');
    loggingHelper.log('info', 'create new instance SubsciptionChargeJob');
  }

  // get all proccesing subscription with next charge date is current date or null
  getSubscription() {
    loggingHelper.log('info', 'getSubscription start');
    return this.db.Subscription.findAll({where: {
      status: 'processing',
      nextChargeDate: {$lte: moment().format('YYYY-MM-DD')}
    }, include: [
      'Card',
      {
        model: this.db.User,
        include: ['Country']
      },
      {
        model: this.db.PlanCountry,
        include: ['Country',
          {
            model: this.db.Plan,
            include: ['PlanType']
          }
        ]
      },
      {
        model: this.db.CustomPlan,
        include: [
          'Country',
          'PlanType',
          'customPlanDetail'
        ]
      }
    ]});
  }

  performCharge(subscription) {
    loggingHelper.log('info', 'performCharge start');
    loggingHelper.log('info', `param: ${subscription}`);
    // calculate charge amount
    let result = { subscription, receiptObject: {}};
    let amount = 0;
    if((subscription.PlanCountry && !subscription.PlanCountry.Plan.isTrial) || subscription.CustomPlan) {
      amount = subscription.pricePerCharge * subscription.qty;
    } else {
      amount = subscription.pricePerCharge;
    }

    // calculate discount amount
    result.receiptObject.discountAmount = 0;
    if(subscription.discountPercent !== 0 && subscription.currentChargeNumber === 0 && subscription.isTrial) {
      result.receiptObject.discountAmount = amount * (subscription.discountPercent / 100);
      amount = amount - result.receiptObject.discountAmount;
    }

    // calculate tax amount
    // if(!subscription.PlanCountry.Country.includedTaxToProduct) {
      // let taxAmount = result.receiptObject.totalPrice * (subscription.PlanCountry.Country.taxRate / 100);
      // result.receiptObject.taxAmount = taxAmount;
      // amount = amount + taxAmount;
    // }

    // calculate shipping fee
    result.receiptObject.shippingFee = 0;
    result.receiptObject.totalPrice = amount;

    let country;
    if(subscription.PlanCountry) {
      country = subscription.PlanCountry.Country;
    }
    if(subscription.CustomPlan) {
      country = subscription.CustomPlan.Country;
    }
    let chargeObject = {
      amount: amount,
      currency: country.currencyCode.toLowerCase(),
      customer: subscription.Card.customerId,
      description: `Shave2u charge for subscription: ${subscription.currentOrderId}`
    };
    // perform charge to stripe
    return stripeHelper.createCharge(chargeObject, country.code, subscription.Card.type)
      .then(chargeObj => {
        result.chargeObj = chargeObj;
        return stripeHelper.retrieveBalanceTransaction(chargeObj.balance_transaction, country.code, subscription.Card.type);
      })
      .then(balanceTransaction => {
        result.chargeObj.chargeFee = balanceTransaction.fee;
        result.chargeObj.chargeCurrency = balanceTransaction.currency;
        return result;
      });
  }

  updateSubscriptionStatus(data) {
    loggingHelper.log('info', 'updateSubscriptionStatus start');
    loggingHelper.log('info', `param: ${data}`);
    let country;
    if(data.subscription.PlanCountry) {
      country = data.subscription.PlanCountry.Country;
    }
    if(data.subscription.CustomPlan) {
      country = data.subscription.CustomPlan.Country;
    }
    return this.db.sequelize.transaction(t => this.db.Receipt.create({ // create receipt
      SubscriptionId: data.subscription.id,
      chargeId: data.chargeObj.id,
      chargeFee: data.chargeObj.chargeFee / 100,
      chargeCurrency: data.chargeObj.chargeCurrency,
      last4: data.subscription.Card.cardNumber,
      branchName: data.subscription.Card.branchName,
      expiredYear: data.subscription.Card.expiredYear,
      expiredMonth: data.subscription.Card.expiredMonth,
      subTotalPrice: data.receiptObject.totalPrice,
      discountAmount: data.receiptObject.discountAmount,
      shippingFee: data.receiptObject.shippingFee,
      taxAmount: data.receiptObject.taxAmount,
      totalPrice: data.receiptObject.totalPrice,
      currency: country.currencyDisplay,
      originPrice: data.receiptObject.totalPrice + data.receiptObject.discountAmount
      // taxAmount: 
    }, {transaction: t})
    .then(() => {
      let planType;
      if(data.subscription.PlanCountry) {
        planType = data.subscription.PlanCountry.Plan.PlanType;
      }
      if(data.subscription.CustomPlan) {
        planType = data.subscription.CustomPlan.PlanType;
      }
      let subsequentChargeDuration = planType.subsequentChargeDuration;
      let nextChargeDate = moment().add(subsequentChargeDuration.split(' ')[0], subsequentChargeDuration.split(' ')[1]);
      let status = data.subscription.status;
      status = data.subscription.currentChargeNumber + 1 === planType.totalChargeTimes && data.subscription.currentDeliverNumber === planType.totalDeliverTimes ? 'Finished' : status;

      let subscriptionChangeInfo = {
        nextChargeDate,
        status,
        currentChargeNumber: data.subscription.currentChargeNumber + 1,
        totalChargeTimes: planType.totalChargeTimes
      };
      // update subscription info
      if ( 'Finished' == status ) {
        this.db.SubscriptionHistory.create({
          message: status,
          subscriptionId: data.subscription.id
        });
      }
        
      return this.db.Subscription.update(subscriptionChangeInfo, {where: {id: data.subscription.id}, transaction: t});
    }));
  }
  
  // getDiscount

  process(db) {
    // loggingHelper.log('info', 'subscription-charge.job process start');
    this.db = db;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return this.getSubscription()
      .then(subscriptions => {
        console.log(`subscription charge === ${JSON.stringify(subscriptions)}`);
        return Promise.mapSeries(subscriptions, subscription => {
          // check total charge time
          if((subscription.PlanCountry && subscription.currentChargeNumber + 1 <= subscription.PlanCountry.Plan.PlanType.totalChargeTimes)
            || (subscription.PlanCountry && !subscription.PlanCountry.Plan.PlanType.totalChargeTimes)
            || (subscription.CustomPlan)) {
            return this.performCharge(subscription)
              .then(data => this.updateSubscriptionStatus(data))
              .catch(error => {
                console.log(`Charge subscription [${subscription.id}] error: ${error.stack}`);
                loggingHelper.log('error', `Charge subscription [${subscription.id}] error: ${error.stack}`);
                // update subscription status
                return db.Subscription.update({status: 'On Hold'}, {where: {id: subscription.id}})
                  .then(() => {
                    db.SubscriptionHistory.create({
                      message: 'On Hold',
                      subscriptionId: subscription.id
                    });
                  })
                  .then(() => sendEmailQueueService.addTask({method: 'sendChargeSubscriptionFail', data: {subscriptionId: subscription.id}, langCode: subscription.User.Country.defaultLang.toLowerCase()})); // send email to user
              });
          }
        });
      })
      .catch(error => {
        console.log(`SubsciptionChargeJob ERROR = ${error.stack}`);
        loggingHelper.log('error', `SubsciptionChargeJob ERROR = ${error.stack}`);
      });
  }
}

export default SubsciptionChargeJob;
