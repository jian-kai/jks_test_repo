import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SendUsersSaleReportEmailJob {
  constructor() {
    console.log('create new instance SendUsersSaleReportEmailJob');
  }

  process(db) {
    this.db = db;

    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return sendEmailQueueService.addTask({method: 'sendUsersSaleReportEmail', data: {langCode: 'en'}})
    .catch(error => {
      console.log(`SendUsersSaleReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendUsersSaleReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendUsersSaleReportEmailJob;
