import Promise from 'bluebird';

import config from '../../config';
import SendEmailQueueService from '../../services/send-email.queue.service';
import CancelOrderQueueService from '../../services/cancel-order.queue.service';
import SequelizeHelper from '../../helpers/sequelize-helper';
import { loggingHelper } from '../../helpers/logging-helper';
import UrbanFoxHelper from '../../helpers/urbanfox-helpers';
import DateTimeHelper from '../../helpers/datetime-helpers';
import { aftership } from '../../helpers/aftership';

class UpdateUrbanFoxOrderJob {
  constructor() {
    console.log('create new instance update delivered order job');
    loggingHelper.log('info', 'create new instance update delivered order job');
  }

  process(db) {
    console.log('UpdateUrbanFoxOrderJob start');
    // get all delivering order
    this.db = db;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let orders;
    let data = {};
    return db.Order.findAll({where: {
      states: {$in: ['Processing']},
      carrierAgent: 'courex'
    }, include: ['Country', 'DeliveryAddress']})
      .then(_orders => {
        console.log(`_orders.length ${_orders.length}`);
        orders = SequelizeHelper.convertSeque2Json(_orders);
        return Promise.mapSeries(orders, order => UrbanFoxHelper.getTransactionInfo(order)
          .then(result => {
            data = {};
            if(!result) {
              return; 
            }
            console.log(`urbanfox return: ${JSON.stringify(result)}`);
            result.data.getOrder.fulfillments.forEach(fullfill => {
              if(fullfill.status === 'DELIVERED') {
                data.states = 'Completed';
              } else if(fullfill.status === 'CANCELLED') {
                data.states = 'Canceled';
              } else if(fullfill.trackingNumber && fullfill.trackingNumber !== '') {
                data.states = 'Delivering';
                data.deliveryId = fullfill.trackingNumber;
                order.deliveryId = fullfill.trackingNumber;
              }
            });
          })
          .then(() => {
            console.log(`urbanfox return: ${JSON.stringify(data)}`);
            if(data.states && data.states === 'Delivering' && order.states !== data.states) {
              return db.Order.update({states: data.states, deliveryId: data.deliveryId}, {where: {id: order.id}})
                .then(() => {
                  if(data.states) {
                    return this.db.OrderHistory.create({
                      OrderId: order.id,
                      message: data.states
                    });
                  }
                })
                .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()}))
                .then(() => aftership.postTracking(order, false))
                .then(() => {
                  // update subscription last delivery date
                  let arrPatt = new RegExp(/\[([^,]+,?)+\]/g);
                  if(order && order.subscriptionIds && arrPatt.test(order.subscriptionIds)) {
                    return this.db.Subscription.update({lastDeliverydate: new Date()}, {where: {id: {$in: JSON.parse(order.subscriptionIds)}}});
                  }
                });
                // .then(() => {
                //   // update subscription start delivery date
                //   let arrPatt = new RegExp(/\[([^,]+,?)+\]/g);
                //   let startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.startFirstDelivery);
                //   if(data.states && data.states === 'Completed' && order && order.subscriptionIds && arrPatt.test(order.subscriptionIds)) {
                //     return this.db.Subscription.update(
                //       {
                //         nextDeliverDate: startDeliverDate,
                //         nextChargeDate: startDeliverDate,
                //         startDeliverDate,
                //       },
                //       {
                //         where: {
                //           id: {$in: JSON.parse(order.subscriptionIds)},
                //           startDeliverDate: null
                //         }
                //       }
                //     );
                //   }
                // });
            } else if(data.states && data.states === 'Canceled') {
              let cancelOrderQueueService = CancelOrderQueueService.getInstance();
              return cancelOrderQueueService.addTask(order.id);
            }
          })
        );
      })
      .then(() => db.BulkOrder.findAll({where: {
        states: {$in: ['Processing']},
        carrierAgent: 'courex'
      }, include: ['Country', 'DeliveryAddress']}))
      .then(_orders => {
        orders = SequelizeHelper.convertSeque2Json(_orders);
        return Promise.mapSeries(orders, order => UrbanFoxHelper.getTransactionInfo(order, true)
          .then(result => {
            data = {};
            if(!result) {
              return; 
            }
            console.log(`result ${JSON.stringify(result)}`);
            result.data.getOrder.fulfillments.forEach(fullfill => {
              if(fullfill.status === 'DELIVERED') {
                data.states = 'Completed';
              } else if(fullfill.status === 'CANCELLED') {
                data.states = 'Canceled';
              } else if(fullfill.trackingNumber && fullfill.trackingNumber !== '') {
                data.states = 'Delivering';
                data.deliveryId = fullfill.trackingNumber;
                order.deliveryId = fullfill.trackingNumber;
              }
            });
          })
          .then(() => {
            console.log(`urbanfox return: ${JSON.stringify(data)}`);
            if(data.states && data.states === 'Delivering' && order.states !== data.states) {
              return db.BulkOrder.update({states: data.states, deliveryId: data.deliveryId}, {where: {id: order.id}})
                .then(() => {
                  if(order) {
                    return this.db.BulkOrderHistory.create({
                      BulkOrderId: order.id,
                      message: data.states
                    });
                  }
                })
                .then(() => aftership.postTracking(order, true));
            } else if(data.states && data.states === 'Canceled') {
              // let cancelOrderQueueService = CancelOrderQueueService.getInstance();
              // return cancelOrderQueueService.addTask(order.id);
              return this.db.BulkOrder.update({states: 'Canceled'}, {where: {id: order.id}})
                .then(() => this.db.BulkOrderHistory.create({
                  BulkOrderId: order.id,
                  message: 'Canceled'
                }))
                .then(() => sendEmailQueueService.addTask({method: 'sendCancelBulkOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()}));
            }
          })
        );
      })
        .catch(err => {
          console.log(`Urbanfox UpdateUrbanFoxOrderJob ERROR ${err}`);
        });
  }
}

export default UpdateUrbanFoxOrderJob;
