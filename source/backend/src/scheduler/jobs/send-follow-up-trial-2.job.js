import Promise from 'bluebird';
import moment from 'moment';
import SendEmailQueueService from '../../services/send-email.queue.service';
import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';

class SendFollowUpTrial2 {
  constructor() {
    console.log('create new instance SendFollowUpTrial2');
  }

  process(db) {
    this.db = db;
    // get the subscription to prepare for start
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let followUpEmail2 = config.trialPlan.followUpEmail2;
    return this.db.Subscription.findAll({where: {
      hasSendFollowUp2: false,
      status: 'processing',
      isTrial: true,
      nextDeliverDate: {$lte: moment().add(followUpEmail2.split(' ')[0], followUpEmail2.split(' ')[1]).format('YYYY-MM-DD')}
    }, include: [
      {
        model: db.User,
        include: ['Country']
      },
      {
        model: db.PlanCountry,
        include: ['Country']
      }
    ]})
      .then(subscriptions => Promise.map(subscriptions, subscription => sendEmailQueueService.addTask({method: 'sendFollowUpTrialEmail', data: {id: subscription.id, type: 2}, langCode: subscription.PlanCountry.Country.defaultLang.toLowerCase()})
        .then(() => this.db.Subscription.update({hasSendFollowUp2: true}, {where: {id: subscription.id}}))
        .catch(error => {
          console.log(`Send Follow up email 2 [${subscription.id}] error: ${error.stack}`);
          loggingHelper.log('error', `Send Follow up email 2 [${subscription.id}] error: ${error.stack}`); 
          throw error;
        })))
      .catch(error => {
        console.log(`SendFollowUpTrial2 ERROR = ${error.stack}`);
        loggingHelper.log('error', `SendFollowUpTrial2 ERROR = ${error.stack}`); 
      });
  }
}

export default SendFollowUpTrial2;
