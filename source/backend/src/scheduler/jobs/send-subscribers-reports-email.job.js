import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SendSubscribersReportEmailJob {
  constructor() {
    console.log('create new instance SendSubscribersReportEmailJob');
  }

  process(db) {
    this.db = db;

    let sendEmailQueueService = SendEmailQueueService.getInstance();

    return sendEmailQueueService.addTask({method: 'sendSubscribersReportEmail', data: {langCode: 'en'}})
    .catch(error => {
      console.log(`SendSubscribersReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendSubscribersReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendSubscribersReportEmailJob;
