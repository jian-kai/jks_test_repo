import Promise from 'bluebird';
import SequelizeHelper from '../../helpers/sequelize-helper';
import { aftership } from '../../helpers/aftership';
import { loggingHelper } from '../../helpers/logging-helper';
import OrderInboundQueueService from '../../services/create-order-inbound.service';
import SendEmailQueueService from '../../services/send-email.queue.service';
import SaleReportQueueService from '../../services/sale-report.queue.service';
import OrderController from '../../controllers/orders.controller';
import _ from 'underscore';

class UpdateDeliveredOrderJob {
  constructor() {
    console.log('create new instance update delivered order job');
    loggingHelper.log('info', 'create new instance update delivered order job');
  }

  process(db) {
    console.log('update-delivered-order.job process start');
    // get all delivering order
    this.db = db;
    this.orderController = new OrderController(db);
    let orders;
    return db.BulkOrder.findAll({where: {states: 'Delivering'}})
      .then(_orders => {
        orders = SequelizeHelper.convertSeque2Json(_orders);
        return aftership.getTrackings();
      })
      .then(trackings => Promise.mapSeries(trackings, tracking => {
        if(tracking) {
          console.log(`update-delivered-order.job process start === ${JSON.stringify(tracking)}`);
          let order = _.findWhere(orders, {deliveryId: tracking.tracking_number});
          if(order) {
            return db.BulkOrder.update({states: 'Completed'}, {where: {id: order.id}})
              .then(() => db.BulkOrderHistory.create({
                BulkOrderId: order.id,
                message: 'Completed'
              }));
          }
        }
      }));
  }

  renewSubscription(order) {
    if(order.subscriptionIds) {
      let req = {};
      
      this.db.Country.findOne({where: {
        id: order.CountryId}
      })
      .then(country => {
        req.country = SequelizeHelper.convertSeque2Json(country);
        return this.db.Subscription.findAll({where: {
          id: {$in: JSON.parse(order.subscriptionIds)}}
        });
      })
      .then(subscriptions => {
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);

        let subscriptionItems = [];
        subscriptions.forEach(subscription => {
          subscriptionItems.push({PlanCountryId: subscription.PlanCountryId, qty: subscription.qty});
        });

        req.body = {
          CardId: subscriptions[0].CardId,
          DeliveryAddressId: order.DeliveryAddressId,
          BillingAddressId: order.BillingAddressId,
          // promotionCode: order.promoCode,
          userTypeId: 1,
          subsItems: subscriptionItems,
          email: order.email,
          userId: subscriptions[0].UserId,
          source: order.source,
          medium: order.medium,
          campaign: order.campaign,
          term: order.term,
          content: order.content
        };
        req.isRenew = true;
        
        let params = req.body;
        let sendEmailQueueService = SendEmailQueueService.getInstance();
        let saleReportQueueService = SaleReportQueueService.getInstance();
    
        // set payment type as stripe
        req.body.paymentType = 'stripe';
    
        let result;
        let orderInboundQueueService = OrderInboundQueueService.getInstance();
    
        this.orderController.processCheckout(req)
          .then(_result => {
            result = _result;
            return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);
          })
          .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))
          .then(() => saleReportQueueService.addTask({
            id: result.order.id,
            source: params.utmSource,
            medium: params.utmMedium,
            campaign: params.utmCampaign,
            term: params.utmTerm,
            content: params.utmContent
          }));
      });
    }
  }
}

export default UpdateDeliveredOrderJob;
