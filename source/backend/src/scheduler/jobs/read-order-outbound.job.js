import Promise from 'bluebird';
import WareHouseHelper from '../../helpers/warehouse-helper';
import { ftpHelper } from '../../helpers/warehouse-ftp-helper';
import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';

class ReadOrderOutboundJob {
  constructor() {
    console.log('create new instance update product quantity job');
    loggingHelper.log('info', 'create new instance update product quantity job');
  }

  process(db) {
    // loggingHelper.log('info', 'read-order-outbound.job process start');
    // get list files on FTP outbound folder
    let fileNames = [];
    return ftpHelper.listFiles(config.warehouse.ftpResultFolder)
      .then(files => {
        // determine order outbound file
        files.forEach(file => {
          if(file.name.indexOf('WMSSHP') > -1) {
            fileNames.push(file.name);
          }
        });
        if(fileNames.length > 0) {
          loggingHelper.log('info', `Warehouse output files: ${JSON.stringify(fileNames)}`);
        }
      })
      .then(() => Promise.mapSeries(fileNames, fileName => ftpHelper.getFile(`${config.warehouse.ftpResultFolder}/${fileName}`, `${config.warehouse.localDownloadFolder}/${fileName}`) // download order outbound file
        .then(() => WareHouseHelper.readOrderConfirmation(`${config.warehouse.localDownloadFolder}/${fileName}`, db))) // read data in order outbound file and update order status
      )
      .catch(error => {
        loggingHelper.log('error', `ReadOrderOutboundJob job: ${error}`); 
      });
  }
}

export default ReadOrderOutboundJob;
