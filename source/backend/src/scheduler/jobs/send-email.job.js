import Promise from 'bluebird';
import fs from 'fs';

import SendEmailQueueService from '../../services/send-email.queue.service';
import EmailHelper from '../../helpers/email-helper';
import { loggingHelper } from '../../helpers/logging-helper';
import TaxInvoicelJob from './tax-invoice.job';
import TaxInvoiceBulkOrderJob from './tax-invoice-bulk-order.job';

const TEMPLATE_EMAIL = `${__dirname}/../../views/email-templates/`;

function statPromise(file) {
  return new Promise((resolve, reject) => {
    fs.stat(`${TEMPLATE_EMAIL}/${file}`, (err, stat) => {
      if(err) {
        reject(err);
      } else if(stat && stat.isDirectory()) {
        fs.readdir(`${TEMPLATE_EMAIL}/${file}`, (err, files) => {
          if(err) {
            loggingHelper.log('error', `SendEmailJob: ${err}`);
            reject(err);
          } else if(files.length > 0) {
            resolve(file);
          } else {
            resolve();
          }
        });
      } else {
        resolve();
      }
    });
  });
}

class SendEmailJob {
  constructor() {
    loggingHelper.log('info', 'create new instance send eamil job');
    this.emailHelper = {};
    this.taxInvoicelJob = new TaxInvoicelJob();
    this.taxInvoiceBulkOrderJob = new TaxInvoiceBulkOrderJob();

    // featch all email templates in template folder
    fs.readdir(TEMPLATE_EMAIL, (err, list) => {
      if(err) {
        loggingHelper.log('error', `SendEmailJob: ${err}`);
        return;
      }
      Promise.mapSeries(list, file => statPromise(file))
        .then(files => {
          files.forEach(file => {
            if(file && file !== 'base') {
              this.emailHelper[file] = new EmailHelper(file);
            }
          });
          loggingHelper.log('info', `email helper === ${Object.keys(this.emailHelper)}`);
        });
    });
  }

  process(db) {
    // loggingHelper.log('info', 'send-email.job process start');
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    this.db = db;
    let emailOptions;
    let currentOption;
    // get keys in update product queue
    return this.taxInvoicelJob.process(db)
    .then(() => this.taxInvoiceBulkOrderJob.process(db))
      .then(() => sendEmailQueueService.getKeysInQueue())
      .then(_emailOptions => {
        if(_emailOptions && _emailOptions.length === 0) {
          // loggingHelper.log('error', 'send-email job: queue_empty'); 
          throw new Error('queue_empty');
        }
        emailOptions = _emailOptions;
        return Promise.mapSeries(emailOptions, option => {
          option = JSON.parse(option);
          // detect langCode
          option.langCode = option.langCode ? option.langCode : 'en';
          option.data.langCode = option.langCode;
          currentOption = option;
          return this.emailHelper[option.langCode][option.method](option.data, this.db)
            .catch(err => {
              console.log(`err = ${err}`);
              loggingHelper.log('error', `send-email err = ${err.stack ? err.stack : err.message}`);
              sendEmailQueueService.removeKeyFromQueue(JSON.stringify(option));
            });
        });
      })
      .then(() => sendEmailQueueService.removeKeysFromQueue(emailOptions))
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `send-email job: ${error}`); 
          sendEmailQueueService.removeKeyFromQueue(JSON.stringify(currentOption));
        }
      });
  }
}

export default SendEmailJob;
