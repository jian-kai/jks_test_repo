import Promise from 'bluebird';
// import fs from 'fs';

import CompressedFileToDownloadQueueService from '../../services/create-compressed-file-to-download.queue.service';
// import EmailHelper from '../../helpers/email-helper';
import { loggingHelper } from '../../helpers/logging-helper';
import { compressedFileHelper } from '../../helpers/compressed-file-helper';
// import TaxInvoicelJob from './tax-invoice.job';
// import TaxInvoiceBulkOrderJob from './tax-invoice-bulk-order.job';

// const TEMPLATE_EMAIL = `${__dirname}/../../views/email-templates/`;

// function statPromise(file) {
//   return new Promise((resolve, reject) => {
//     fs.stat(`${TEMPLATE_EMAIL}/${file}`, (err, stat) => {
//       if(err) {
//         reject(err);
//       } else if(stat && stat.isDirectory()) {
//         fs.readdir(`${TEMPLATE_EMAIL}/${file}`, (err, files) => {
//           if(err) {
//             loggingHelper.log('error', `SendEmailJob: ${err}`);
//             reject(err);
//           } else if(files.length > 0) {
//             resolve(file);
//           } else {
//             resolve();
//           }
//         });
//       } else {
//         resolve();
//       }
//     });
//   });
// }

class CreateCompressedFileToDownloadJob {
  constructor() {
    loggingHelper.log('info', 'create compressed file to download job');
    this.downloadProcessing = false;
    // this.compressedFileHelper = new CompressedFileHelper();
    // this.emailHelper = {};
    // this.taxInvoicelJob = new TaxInvoicelJob();
    // this.taxInvoiceBulkOrderJob = new TaxInvoiceBulkOrderJob();

    // // featch all email templates in template folder
    // fs.readdir(TEMPLATE_EMAIL, (err, list) => {
    //   if(err) {
    //     loggingHelper.log('error', `SendEmailJob: ${err}`);
    //     return;
    //   }
    //   Promise.mapSeries(list, file => statPromise(file))
    //     .then(files => {
    //       files.forEach(file => {
    //         if(file && file !== 'base') {
    //           this.emailHelper[file] = new EmailHelper(file);
    //         }
    //       });
    //       loggingHelper.log('info', `email helper === ${Object.keys(this.emailHelper)}`);
    //     });
    // });
  }

  process(db) {
    // loggingHelper.log('info', 'send-email.job process start');
    let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();
    this.db = db;
    // let compressedFileOptions;
    let currentOption;
    // get keys in update product queue
    return compressedFileToDownloadQueueService.getKeysInQueue()
      .then(_compressedFileOptions => {
        if(_compressedFileOptions && _compressedFileOptions.length === 0) {
          // loggingHelper.log('error', 'send-email job: queue_empty'); 
          throw new Error('queue_empty');
        }
        console.log('this.downloadProcessing === === === ', this.downloadProcessing);
        if(!this.downloadProcessing) {
          this.downloadProcessing = true;
          let compressedFileOptions = _compressedFileOptions;
          console.log('compressedFileOptions ==== == ', compressedFileOptions);
          return Promise.mapSeries(compressedFileOptions, option => {
            option = JSON.parse(option);
            console.log('option ==== == ', option);
            // detect langCode
            // option.langCode = option.langCode ? option.langCode : 'en';
            // option.data.langCode = option.langCode;
            currentOption = option;
            return compressedFileHelper[option.method](option.queueOptions, this.db)
            .then(() => JSON.stringify(option))
              .catch(err => {
                console.log(`err = ${err}`);
                loggingHelper.log('error', `create-compressed-file-to-download err = ${err}`);
                this.downloadProcessing = false;
                compressedFileToDownloadQueueService.removeKeyFromQueue(JSON.stringify(option));
              });
          });
        } else {
          return null;
        }
      })
      .then(compressedFileOptions => {
        if(compressedFileOptions) {
          this.downloadProcessing = false;
          return compressedFileToDownloadQueueService.removeKeysFromQueue(compressedFileOptions);
        }
      })
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `create-compressed-file-to-download job: ${error}`); 
          this.downloadProcessing = false;
          compressedFileToDownloadQueueService.removeKeyFromQueue(JSON.stringify(currentOption));
        }
      });
  }
}

export default CreateCompressedFileToDownloadJob;
