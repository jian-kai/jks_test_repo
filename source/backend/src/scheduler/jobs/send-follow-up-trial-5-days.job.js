import Promise from 'bluebird';
import moment from 'moment';
import SendEmailQueueService from '../../services/send-email.queue.service';
import config from '../../config';

class SendFollowUpTrial5Days {
  constructor() {
    console.log('create new instance SendFollowUpTrial5Days');
  }

  process(db) {
    this.db = db;
    // get the subscription to prepare for start
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let followUpEmail5Days = config.trialPlan.followUpEmail5Days;
    let startFirstDelivery = config.trialPlan.startFirstDelivery;
    return this.db.Subscription.findAll({where: {
      hasSendFollowUp5Days: false,
      status: 'processing',
      isTrial: true,
      // createdAt: {$lte: moment().subtract(followUpEmail5Days.split(' ')[0], followUpEmail5Days.split(' ')[1]).format('YYYY-MM-DD')}
      nextDeliverDate: {$lte: moment()
                        .add(startFirstDelivery.split(' ')[0], startFirstDelivery.split(' ')[1])
                        .subtract(followUpEmail5Days.split(' ')[0], followUpEmail5Days.split(' ')[1]).format('YYYY-MM-DD')}
    }, include: [
      {
        model: db.User,
        include: ['Country']
      },
      {
        model: db.PlanCountry,
        include: ['Country']
      }
    ]})
      .then(subscriptions => Promise.map(subscriptions, subscription => sendEmailQueueService.addTask({method: 'sendFollowUpTrialEmail', data: {id: subscription.id, type: '5days'}, langCode: subscription.PlanCountry.Country.defaultLang.toLowerCase()})
        .then(() => this.db.Subscription.update({hasSendFollowUp5Days: true}, {where: {id: subscription.id}}))
        .catch(error => {
          console.log(`Send Follow up email 5 days [${subscription.id}] error: ${error.stack}`);
          throw error;
        })))
      .catch(error => {
        console.log(`SendFollowUpTrial5Days ERROR = ${error.stack}`);
      });
  }
}

export default SendFollowUpTrial5Days;
