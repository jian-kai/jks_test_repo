import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SendStudentPromoCodeReportEmailJob {
  constructor() {
    console.log('create new instance SendStudentPromoCodeReportEmailJob');
  }

  process(db) {
    this.db = db;

    let sendEmailQueueService = SendEmailQueueService.getInstance();

    return sendEmailQueueService.addTask({method: 'sendStudentPromoCodeReportEmail', data: {langCode: 'en'}})
    .catch(error => {
      console.log(`SendStudentPromoCodeReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendStudentPromoCodeReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendStudentPromoCodeReportEmailJob;
