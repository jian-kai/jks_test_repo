import Promise from 'bluebird';

import TaxInvoiceQueueService from '../../services/invoice-number.service';
import { loggingHelper } from '../../helpers/logging-helper';
import config from '../../config';

const TAX_LENGTH = 9;

class TaxInvoicelJob {
  constructor() {
    loggingHelper.log('info', 'Tax invoice init');
  }

  formatTaxInvoice(taxNo, countryCode, SellerUserId, moCode) {
    let iso2 = config.countryCodes[countryCode] || countryCode;
    let zeros = '';
    taxNo = `${taxNo}`;
    for(let i = 0; i < TAX_LENGTH - taxNo.length; i++) {
      zeros += '0';
    }
    if(SellerUserId) {
      if(moCode) {
        return `${moCode}/SH/D${zeros}${taxNo}`;
      } else {
        return null;
      }
    } else {
      return `S2U-${iso2}-${zeros}${taxNo}`;
    }
  }

  process(db) {
    let data = {};
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    return taxInvoiceQueueService.getKeysInQueue()
      .then(orders => {
        if(orders.length > 0) {
          data.orders = orders;
          return db.sequelize.transaction(t => Promise.mapSeries(orders, obj => {
            obj = JSON.parse(obj);

            return db.Country.findOne({where: {id: obj.CountryId}, transaction: t})
            .then(country => {
              let whereOption = {CountryId: obj.CountryId};
              if(obj.SellerUserId) {
                let moCode;
                // find max taxInvoiceNo of BA-web
                whereOption = Object.assign(whereOption, {SellerUserId: {$not: null}, taxInvoiceNo: {$like: '%/SH/D%'}, MarketingOfficeId: obj.MarketingOfficeId});
                return db.MarketingOffice.findOne({where: {id: obj.MarketingOfficeId}, transaction: t})
                  .then(marketingOffice => {
                    moCode = marketingOffice ? marketingOffice.moCode : null;
                    return db.Order.findOne({where: whereOption, order: [['taxInvoiceNo', 'DESC']], include: ['Country'], transaction: t});
                  })
                  .then(order => {
                    let maxTaxInvoice;
                    // if(order) {
                    console.log(`formatTaxInvoice ${JSON.stringify(order)}`);
                    // generate taxInvoiceNo for BA-web
                    maxTaxInvoice = (order && order.taxInvoiceNo) ? order.taxInvoiceNo.replace(/^(.*?\/SH\/D)(\d+)/g, '$2') : 0;
                    maxTaxInvoice = parseInt(maxTaxInvoice);
                    return db.Order.update({taxInvoiceNo: this.formatTaxInvoice(maxTaxInvoice + 1, country.code, obj.SellerUserId, moCode)}, {where: {id: obj.id}, transaction: t});
                    // } else {
                    //   // find max old taxInvoiceNo of BA-web
                    //   whereOption = {CountryId: obj.CountryId, SellerUserId: {$not: null}};
                    //   return db.Order.findOne({where: whereOption, order: [['taxInvoiceNo', 'DESC']], include: ['Country'], transaction: t})
                    //   .then(order2 => {
                    //     console.log(`formatTaxInvoice ${JSON.stringify(order2)}`);
                    //     // generate taxInvoiceNo for BA-web
                    //     maxTaxInvoice = order2.taxInvoiceNo ? order2.taxInvoiceNo.replace(/^(S2U\-\w{2}\-)(\d+)/g, '$2') : -1;
                    //     maxTaxInvoice = parseInt(maxTaxInvoice);
                    //     return db.Order.update({taxInvoiceNo: this.formatTaxInvoice(maxTaxInvoice + 1, order2.Country.code, obj.SellerUserId)}, {where: {id: obj.id}, transaction: t});
                    //   })
                    // }
                  })
                  .catch(error => loggingHelper.log('error', `Create tax invoice error: ${error.stack}`));
              } else {
                // find max taxInvoiceNo of website
                whereOption = Object.assign(whereOption, {SellerUserId: null});
                return db.Order.findOne({where: whereOption, order: [['taxInvoiceNo', 'DESC']], include: ['Country'], transaction: t})
                  .then(order => {
                    console.log(`formatTaxInvoice ${JSON.stringify(order)}`);
                    // generate taxInvoiceNo for website
                    let maxTaxInvoice = (order && order.taxInvoiceNo) ? order.taxInvoiceNo.replace(/^(S2U\-\w{2}\-)(\d+)/g, '$2') : 0;
                    maxTaxInvoice = parseInt(maxTaxInvoice);
                    return db.Order.update({taxInvoiceNo: this.formatTaxInvoice(maxTaxInvoice + 1, country.code, obj.SellerUserId, null)}, {where: {id: obj.id}, transaction: t});
                  })
                  .catch(error => loggingHelper.log('error', `Create tax invoice error: ${error.stack}`));
              }
            });
          })
            .then(() => taxInvoiceQueueService.removeKeysFromQueue(data.orders)))
            .catch(error => loggingHelper.log('error', `Create tax invoice error: ${error.stack}`));
        }
      })
      .catch(error => loggingHelper.log('error', `Create tax invoice error: ${error.stack}`));
  }
}

export default TaxInvoicelJob;
