import Promise from 'bluebird';
import CancelOrderQueueService from '../../services/cancel-order.queue.service';
import SendEmailQueueService from '../../services/send-email.queue.service';
import { stripeHelper } from '../../helpers/stripe-helper';
import { loggingHelper } from '../../helpers/logging-helper';
import UrbanfoxHelper from '../../helpers/urbanfox-helpers';

class CancelOrderJob {
  constructor() {
    loggingHelper.log('info', 'create new instance send eamil job');
  }

  cancelOrder(orderData) {
    let orderDataObj = JSON.parse(orderData);
    let orderId = orderDataObj.orderId;

    loggingHelper.log('info', 'cancelOrder start');
    loggingHelper.log('info', `cancelOrder start param: orderId = ${orderId}`);

    let order;
    let cancelReason = '';
    let cancelOrderQueueService = CancelOrderQueueService.getInstance();
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return this.db.Order.find(
      {
        where: {id: orderId},
        include: ['Country', 'User', 'Receipt', 'SellerUser']
      }
    )
      .then(_order => {
        order = _order;
        if(order.subscriptionIds && order.subscriptionIds !== '') {
          return this.db.Subscription.findOne({where: {id: order.subscriptionIds.replace(/^\["?(\d+)"?\]$/g, '$1')}});
        }
      })
      .then(subscription => {
        if(!order) {
          throw new Error('order_not_found');
        } else {
          cancelReason = order.SellerUser ? 'Canceled by user from BA web' : 'Canceled per user\'s request';
          if(orderDataObj.canceledFrom == 'warehouse') {
            cancelReason = 'Canceled from warehouse';
          } else if(orderDataObj.canceledFrom == 'BA') {
            cancelReason = 'Canceled by user from BA web';
          } else {
            cancelReason = 'Canceled per user\'s request';
          }

          if(order && order.paymentType === 'stripe' && order.Receipt.chargeId) {
            // calculate refund amount
            let refundAmount = parseFloat(order.Receipt.totalPrice);
            console.log(`refundAmount ${refundAmount} ==== ${(!subscription || (subscription && subscription.isTrial && !subscription.isDirectTrial))} === ${JSON.stringify(subscription)}`);
            if((!subscription || (subscription && subscription.isTrial && !subscription.isDirectTrial)) && ['Delivering', 'Completed'].indexOf(order.states) > -1) {
              refundAmount -= parseFloat(order.Receipt.shippingFee);
            }
            console.log(`refundAmount 111111 ${refundAmount}`);

            // send request to stripe to refund
            // send email cancel to SS
            let type = order.SellerUser ? 'baWeb' : 'website';
            if(refundAmount > 0) {
              return stripeHelper.createRefund(order.Receipt.chargeId, refundAmount, order.Country.code, type);
            }
          } else {
            return;
          }
        }
      })
      .then(() => {
        // if(order.subscriptionIds) {
        //   let params = {status: 'Canceled', cancellationReason: cancelReason};
        //   let subsIds = JSON.parse(order.subscriptionIds);
        //   let whereParams = {
        //     where: {
        //       id: {
        //         $in: subsIds
        //       },
        //       status: {
        //         $not: 'Canceled'
        //       }
        //     }
        //   };

        //   // update SubscriptionHistory
        //   this.db.Subscription.findAll(whereParams)
        //   .then(subs => {
        //     let subsHistoryData = [];

        //     for(var i = 0; i < subs.length; i++) {
        //       subsHistoryData.push({
        //         message: 'Canceled',
        //         subscriptionId: subs[i].id,
        //         detail: cancelReason
        //       });
        //     }

        //     this.db.SubscriptionHistory.bulkCreate(subsHistoryData);
        //   });

        //   return this.db.Subscription.update(params, whereParams);
        // }
      })
      .then(() => { 
        if(!order.carrierAgent || (order.carrierAgent && order.carrierAgent !== 'courex')) {
          return sendEmailQueueService.addTask({method: 'sendCancelOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()});
        } else if(order.carrierAgent === 'courex') {
          return UrbanfoxHelper.cancelOrder(order)
          .then(() => sendEmailQueueService.addTask({method: 'sendCancelOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()}));
        }
      }) // update order status to canceled
      .then(() => this.db.Order.update({states: 'Canceled'}, {where: {id: order.id}}))
      .then(() => this.db.OrderHistory.create({
        OrderId: order.id,
        message: 'Canceled',
        cancellationReason: cancelReason
      }))
      .catch(err => {
        cancelOrderQueueService.removeKeyFromQueue(orderData);
        if(err.message === 'order_not_found') {
          return;
        } else if(err.type === 'StripeInvalidRequestError') {
          return loggingHelper.log('error', `cancelOrder ERROR: ${err.message}`);
        } else {
          return loggingHelper.log('error', err);
        }
      });
  }

  process(db) {
    // loggingHelper.log('info', 'cancel-order.job process start');
    let cancelOrderQueueService = CancelOrderQueueService.getInstance();
    this.db = db;
    let orderDatas;
    // get order from order ID
    return cancelOrderQueueService.getKeysInQueue()
      .then(_orderDatas => {
        if(_orderDatas && _orderDatas.length === 0) {
          // loggingHelper.log('error', 'cancel-order job: queue_empty');
          throw new Error('queue_empty');
        }
        orderDatas = _orderDatas;
        return Promise.mapSeries(orderDatas, orderData => this.cancelOrder(orderData));
      })
      .then(() => cancelOrderQueueService.removeKeysFromQueue(orderDatas))
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `cancel-order job: ${error}`);
          throw error;
        }
      });
  }
}

export default CancelOrderJob;
