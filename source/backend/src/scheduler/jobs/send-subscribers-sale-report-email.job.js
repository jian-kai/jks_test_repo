import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SendSubscribersSaleReportEmailJob {
  constructor() {
    console.log('create new instance SendSubscribersSaleReportEmailJob');
  }

  process(db) {
    this.db = db;

    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return sendEmailQueueService.addTask({method: 'sendSendSubscribersSaleReportEmail', data: {langCode: 'en'}})
    .catch(error => {
      console.log(`SendSubscribersSaleReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendSubscribersSaleReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendSubscribersSaleReportEmailJob;
