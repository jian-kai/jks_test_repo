import _ from 'underscore';
import moment from 'moment';
import SequelizeHelper from '../../helpers/sequelize-helper';
import SaleReportQueueService from '../../services/sale-report.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';
import TaxInvoiceBulkOrderJob from './tax-invoice-bulk-order.job';

class CreateSaleReportJob {
  constructor() {
    console.log('create new instance sales report Job');
    loggingHelper.log('info', 'create new instance sales report Job');
    this.taxInvoiceBulkOrderJob = new TaxInvoiceBulkOrderJob();
  }

  // find sales item group by name, user, created date
  getOrders(orderIds) {
    loggingHelper.log('info', 'getOrders process start');
    loggingHelper.log('info', `param: orderIds = ${orderIds}`);

    let options = {
      where: {
        $and: [
          {id: {$in: orderIds}},
          {id: {$notIn: this.db.sequelize.literal('(SELECT OrderId FROM SaleReports WHERE OrderId IS NOT NULL)')}}
        ]
      },
      include: [
        'User', 'SellerUser', 'Country', 'DeliveryAddress', 'BillingAddress',
        {
          model: this.db.Receipt,
          require: true
        },
        {
          model: this.db.OrderDetail,
          as: 'orderDetail',
          include: [{
            model: this.db.PlanCountry,
            include: ['Country', {
              model: this.db.Plan,
              include: ['planTranslate']
            }]
          }, {
            model: this.db.ProductCountry,
            include: ['Country', {
              model: this.db.Product,
              include: ['productTranslate']
            }]
          }]
        }
      ]
    };
    return this.db.Order.findAll(options);
  }

  // find sales item group by name, user, created date
  getBulkOrders(orderIds) {
    loggingHelper.log('info', 'getBulkOrders process start');
    loggingHelper.log('info', `param: orderIds = ${orderIds}`);

    let options = {
      where: {
        $and: [
          {id: {$in: orderIds}},
          {id: {$notIn: this.db.sequelize.literal('(SELECT bulkOrderId FROM SaleReports WHERE bulkOrderId IS NOT NULL)')}}
        ]
      },
      include: [
        'Country', 'DeliveryAddress', 'BillingAddress',
        {
          model: this.db.BulkOrderDetail,
          as: 'bulkOrderDetail',
          include: [{
            model: this.db.PlanCountry,
            include: ['Country', {
              model: this.db.Plan,
              include: ['planTranslate']
            }]
          }, {
            model: this.db.ProductCountry,
            include: ['Country', {
              model: this.db.Product,
              include: ['productTranslate']
            }]
          }]
        }
      ]
    };
    return this.db.BulkOrder.findAll(options);
  }

  // build detail info
  buildReportInfo(orders, saleReportQueue) {
    loggingHelper.log('info', 'buildReportInfo process start');
    loggingHelper.log('info', `param: orders = ${orders}`);
    let reportList = [];
    return this.db.MarketingOffice.findAll()
    .then(marketingOffices => {
      marketingOffices = SequelizeHelper.convertSeque2Json(marketingOffices);
      orders.forEach(order => {
        let marketingOffice = _.findWhere(marketingOffices, {id: order.MarketingOfficeId});
        let productCategory = null;
        let productName = '';
        let sku = '';
        let qty = '';
        let deliveryAddress = '';
        let billingAddress = '';
        let deliveryContact = '';
        let billingContact = '';
        let agentName = (order.SellerUser) ? order.SellerUser.agentName : '';
        let badgeId = (order.SellerUser) ? order.SellerUser.badgeId : '';
        let region = order.Country.code;
        let category = (order.SellerUser) ? 'sales app' : 'client';
        let customerName = '';
        if(order.SellerUser) {
          customerName = order.fullName;
        } else {
          customerName = order.User ? `${order.User.firstName} ${order.User.lastName ? order.User.lastName : ''}` : '';
        }

        if(order.DeliveryAddress) {
          deliveryAddress = `${order.DeliveryAddress.address}, ${order.DeliveryAddress.city}, ${order.DeliveryAddress.portalCode}, ${order.DeliveryAddress.state}`;
          deliveryContact = order.DeliveryAddress.contactNumber;
        }
        if(order.BillingAddress) {
          billingAddress = `${order.BillingAddress.address}, ${order.BillingAddress.city}, ${order.BillingAddress.portalCode}, ${order.BillingAddress.state}`;
          billingContact = order.BillingAddress.contactNumber;
        }
        if(order.SellerUserId) {
          deliveryContact = order.phone;
          billingContact = order.phone;
        }
        let email;
        if(order.User) {
          email = order.User.email;
        } else if(order.SellerUser) {
          email = order.SellerUser.email;
        } else {
          email = order.email;
        }
        if(order.subscriptionIds && order.subscriptionIds[0]) {
          productCategory = 'Subscription';
        } else {
          productCategory = 'Ala Carte';
        }
        order.orderDetail.forEach(orderDetail => {
          qty += `${orderDetail.qty}, `;
          if(orderDetail.ProductCountry) {
            // productCategory = 'Ala Carte';
            productName += `${_.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name}, `;
            sku += `${orderDetail.ProductCountry.Product.sku}, `;
          } else if(orderDetail.PlanCountry) {
            // productCategory = 'Subscription';
            productName += `${_.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name}, `;
            sku += `${orderDetail.PlanCountry.Plan.sku}, `;
          }
        });
        productName = productName.replace(/^(.*)\,\s$/g, '$1');
        qty = qty.replace(/^(.*)\,\s$/g, '$1');
        sku = sku.replace(/^(.*)\,\s$/g, '$1');
        let saleReport = _.findWhere(saleReportQueue, {id: order.id});
        let source = saleReport.source ? saleReport.source.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : saleReport.source;
        if(order.Receipt) {
          let reportInfo = {
            badgeId,
            agentName,
            channelType: order.channelType,
            eventLocationCode: order.eventLocationCode,
            email,
            customerName,
            deliveryAddress,
            deliveryContact,
            billingAddress,
            billingContact,
            productCategory,
            productName,
            sku,
            qty,
            region,
            category,
            deliveryId: order.deliveryId,
            carrierAgent: order.carrierAgent,
            states: order.states,
            promoCode: order.promoCode,
            paymentType: order.paymentType,
            orderId: order.id,
            receiptId: order.Receipt.id,
            currency: order.Country.currencyCode,
            createdAt: order.createdAt,
            updatedAt: order.updatedAt,
            saleDate: new Date(moment(order.createdAt).format('YYYY-MM-DD')),
            totalPrice: (parseFloat(order.Receipt.subTotalPrice) + parseFloat(order.Receipt.discountAmount)).toFixed(2),
            subTotalPrice: order.Receipt.subTotalPrice,
            discountAmount: order.Receipt.discountAmount,
            shippingFee: order.Receipt.shippingFee,
            taxAmount: order.Receipt.taxAmount,
            grandTotalPrice: order.Receipt.totalPrice,
            source,
            medium: saleReport.medium,
            campaign: saleReport.campaign,
            term: saleReport.term,
            content: saleReport.content,
            CountryId: order.Country.id,
            taxInvoiceNo: order.taxInvoiceNo,
            moCode: marketingOffice ? marketingOffice.moCode : null,
            MarketingOfficeId: order.MarketingOfficeId,
            taxRate: order.taxRate,
            includedTaxToProduct: order.includedTaxToProduct,
            isDirectTrial: order.isDirectTrial
          };
          reportList.push(reportInfo);
        }
      });
      return reportList;
    });
  }

  // build detail info
  buildReportInfoForBulkOrder(bulkOrders) {
    loggingHelper.log('info', 'buildReportInfoForBulkOrder process start');
    loggingHelper.log('info', `param: bulkOrders = ${bulkOrders}`);
    let reportList = [];
    bulkOrders.forEach(bulkOrder => {
      let productCategory = null;
      let productName = '';
      let sku = '';
      let qty = '';
      let price = 0.00;
      let deliveryAddress = '';
      let billingAddress = '';
      let deliveryContact = '';
      let billingContact = '';
      let agentName = (bulkOrder.SellerUser) ? bulkOrder.SellerUser.agentName : '';
      let badgeId = (bulkOrder.SellerUser) ? bulkOrder.SellerUser.badgeId : '';
      let region = bulkOrder.Country.code;
      let category = 'Bulk Order';
      let customerName = bulkOrder.DeliveryAddress ? `${bulkOrder.DeliveryAddress.firstName} ${bulkOrder.DeliveryAddress.lastName ? bulkOrder.DeliveryAddress.lastName : ''}` : '';
      if(bulkOrder.DeliveryAddress) {
        deliveryAddress = `${bulkOrder.DeliveryAddress.address}, ${bulkOrder.DeliveryAddress.city}, ${bulkOrder.DeliveryAddress.portalCode}, ${bulkOrder.DeliveryAddress.state}`;
        deliveryContact = bulkOrder.DeliveryAddress.contactNumber;
      }
      if(bulkOrder.BillingAddress) {
        billingAddress = `${bulkOrder.BillingAddress.address}, ${bulkOrder.BillingAddress.city}, ${bulkOrder.BillingAddress.portalCode}, ${bulkOrder.BillingAddress.state}`;
        billingContact = bulkOrder.BillingAddress.contactNumber;
      }
      let email = bulkOrder.email;
      bulkOrder.bulkOrderDetail.forEach(bulkOrderDetail => {
        qty += `${bulkOrderDetail.qty}, `;
        if(bulkOrderDetail.ProductCountry) {
          productCategory = 'Ala Carte';
          productName += `${_.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name}, `;
          sku += `${bulkOrderDetail.ProductCountry.Product.sku}, `;
        } else if(bulkOrderDetail.PlanCountry) {
          productCategory = 'Subscription';
          productName += `${_.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name}, `;
          sku += `${bulkOrderDetail.PlanCountry.Plan.sku}, `;
        }
        price = price + (parseFloat(bulkOrderDetail.price) * bulkOrderDetail.qty);
      });
      productName = productName.replace(/^(.*)\,\s$/g, '$1');
      qty = qty.replace(/^(.*)\,\s$/g, '$1');
      sku = sku.replace(/^(.*)\,\s$/g, '$1');
      // let saleReport = _.findWhere(saleReportQueue, {id: bulkOrder.id});
      // let source = saleReport.source ? saleReport.source.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : saleReport.source;
      // if(order.Receipt) {
      let reportInfo = {
        badgeId,
        agentName,
        email,
        customerName,
        deliveryAddress,
        deliveryContact,
        billingAddress,
        billingContact,
        productCategory,
        productName,
        sku,
        qty,
        region,
        category,
        deliveryId: bulkOrder.deliveryId,
        carrierAgent: bulkOrder.carrierAgent,
        states: bulkOrder.states,
        promoCode: bulkOrder.promoCode,
        paymentType: bulkOrder.paymentType,
        bulkOrderId: bulkOrder.id,
        receiptId: null,
        currency: bulkOrder.Country.currencyCode,
        createdAt: bulkOrder.createdAt,
        updatedAt: bulkOrder.updatedAt,
        saleDate: new Date(moment(bulkOrder.createdAt).format('YYYY-MM-DD')),
        totalPrice: price,
        subTotalPrice: price,
        discountAmount: 0,
        shippingFee: 0,
        taxAmount: 0,
        grandTotalPrice: price,
        source: null,
        medium: null,
        campaign: null,
        term: null,
        content: null,
        CountryId: bulkOrder.Country.id,
        taxInvoiceNo: bulkOrder.taxInvoiceNo,
        taxRate: bulkOrder.taxRate,
        includedTaxToProduct: bulkOrder.includedTaxToProduct
      };
      reportList.push(reportInfo);
      // }
    });
    return reportList;
  }

  // insert into sale-report table

  process(db) {
    // loggingHelper.log('info', 'create-sales-reports.job process start');
    this.db = db;
    // get item from sale report queue
    let saleReportQueue = SaleReportQueueService.getInstance();
    let saleReports = [];
    let bulkSaleReports = [];
    return saleReportQueue.getKeysInQueue()
      .then(_saleReports => {
        _saleReports.forEach(item => {
          if(!item.bulkOrder && !saleReports.find(saleReport => saleReport.id === item.id)) {
            saleReports.push(item);
          }
          if(item.bulkOrder && !bulkSaleReports.find(bulkSaleReport => bulkSaleReport.id === item.id)) {
            bulkSaleReports.push(item);
          }
        });
        if(saleReports.length > 0 && !saleReports[0].bulkOrder) {
          // get orders
          return this.getOrders(_.pluck(saleReports, 'id'))
            .then(orders => {
              orders = SequelizeHelper.convertSeque2Json(orders);
              // build data
              return this.buildReportInfo(orders, saleReports);
            })
            .then(reports => this.db.SaleReport.bulkCreate(reports))
            .then(() => {
              if(bulkSaleReports.length > 0 && bulkSaleReports[0].bulkOrder) {
                // get orders
                return this.taxInvoiceBulkOrderJob.process(db)
                .then(() => this.getBulkOrders(_.pluck(bulkSaleReports, 'id')))
                  .then(bulkOrders => {
                    bulkOrders = SequelizeHelper.convertSeque2Json(bulkOrders);
                    // build data
                    return this.buildReportInfoForBulkOrder(bulkOrders);
                  })
                  .then(reports => this.db.SaleReport.bulkCreate(reports)); // create sale report
              }
            }); // create sale report
        }
        if(bulkSaleReports.length > 0 && bulkSaleReports[0].bulkOrder) {
          // get orders
          return this.taxInvoiceBulkOrderJob.process(db)
          .then(() => this.getBulkOrders(_.pluck(bulkSaleReports, 'id')))
            .then(bulkOrders => {
              bulkOrders = SequelizeHelper.convertSeque2Json(bulkOrders);
              // build data
              return this.buildReportInfoForBulkOrder(bulkOrders);
            })
            .then(reports => this.db.SaleReport.bulkCreate(reports)); // create sale report
        }
      })
      .catch(error => {
        loggingHelper.log('error', `saleReportQueue job: ${error}`); 
      });
  }
}

export default CreateSaleReportJob;
