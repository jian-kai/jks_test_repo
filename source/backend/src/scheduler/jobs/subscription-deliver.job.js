import Promise from 'bluebird';
import moment from 'moment';
import _ from 'underscore';
import SequelizeHelper from '../../helpers/sequelize-helper';
import OrderInboundQueueService from '../../services/create-order-inbound.service';
import SaleReportQueueService from '../../services/sale-report.queue.service';
import TaxInvoiceQueueService from '../../services/invoice-number.service';
import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SubsciptionDeliverJob {
  constructor() {
    console.log('create new instance SubsciptionDeliverJob');
    loggingHelper.log('info', 'create new instance SubsciptionDeliverJob');
  }

  // get all proccesing subscription with next deliver date is current date or null
  getSubscription() {
    loggingHelper.log('info', 'getSubscription start');
    return this.db.Subscription.findAll({where: {
      status: 'processing',
      nextDeliverDate: {$lte: moment().format('YYYY-MM-DD')}
    }, include: [
      'Card',
      {
        model: this.db.PlanCountry,
        include: ['Country', 'planDetails',
          {
            model: this.db.Plan,
            include: ['PlanType']
          }
        ]
      },
      {
        model: this.db.CustomPlan,
        include: [
          'Country',
          'PlanType',
          'customPlanDetail'
        ]
      },
      {
        model: this.db.PlanOption,
        include: ['planOptionProducts']
      }
    ]})
      .then(subscriptions => {
        console.log(`subscription === ${JSON.stringify(subscriptions)}`);
        return Promise.map(subscriptions, subscription => {
          let planType;
          if(subscription.PlanCountry) {
            planType = subscription.PlanCountry.Plan.PlanType;
          }
          if(subscription.CustomPlan) {
            planType = subscription.CustomPlan.PlanType;
          }
          if(subscription.currentDeliverNumber + 1 <= planType.totalDeliverTimes || !planType.totalDeliverTimes) {
            return this.processSubscription(subscription)
            .catch(error => {
              loggingHelper.log('error', `SubsciptionDeliverJob [${subscription.id}] ERROR = ${error.stack}`);
            });
          }
        });
      })
      .catch(error => {
        loggingHelper.log('error', `SubsciptionDeliverJob ERROR = ${error.stack}`);
      });
  }

  processSubscription(subscription) {
    loggingHelper.log('info', 'processSubscription start');
    loggingHelper.log('info', `param: ${subscription}`);

    let country;
    if(subscription.PlanCountry) {
      country = subscription.PlanCountry.Country;
    }
    if(subscription.CustomPlan) {
      country = subscription.CustomPlan.Country;
    }
    let order = {
      subscriptionIds: JSON.stringify([subscription.id]),
      UserId: subscription.UserId,
      DeliveryAddressId: subscription.DeliveryAddressId,
      BillingAddressId: subscription.BillingAddressId,
      currency: country.currency,
      email: subscription.email,
      paymentType: 'stripe',
      states: 'Payment Received',
      CountryId: country.id,
      SellerUserId: subscription.SellerUserId,
      channelType: subscription.channelType,
      eventLocationCode: subscription.eventLocationCode,
      MarketingOfficeId: subscription.MarketingOfficeId,
      taxRate: country.taxRate,
      fullName: subscription.fullname,
      phone: subscription.phone,
      isSubsequentOrder: true
    };

    if(subscription.discountPercent !== 0 && subscription.currentDeliverNumber === 0 && subscription.isTrial) {
      order.promoCode = subscription.promoCode;
      order.PromotionId = subscription.promotionId;
    }

    let updateItems = [];

    // build order details object
    let orderDetails = [];
    if(subscription.PlanCountry && !subscription.PlanCountry.Plan.isTrial) {
      subscription.PlanCountry.planDetails.forEach(planDetail => {
        let deliverPlan = JSON.parse(planDetail.deliverPlan);
        let deliverAt = Object.keys(deliverPlan);
        if(deliverAt.includes(`${subscription.currentDeliverNumber + 1}`) && deliverPlan[subscription.currentDeliverNumber + 1] > 0) {
          orderDetails.push({
            ProductCountryId: planDetail.ProductCountryId,
            qty: deliverPlan[subscription.currentDeliverNumber + 1] * subscription.qty
          });
  
          // create update items
          updateItems.push({
            id: planDetail.ProductCountryId,
            qty: deliverPlan[subscription.currentDeliverNumber + 1] * subscription.qty
          });
        }
      });
    } else if(subscription.CustomPlan) {
      subscription.CustomPlan.customPlanDetail.forEach(detail => {
        // let deliverPlan = JSON.parse(detail.deliverPlan);
        let qty = detail.qty;
        if(qty > 0) {
          orderDetails.push({
            ProductCountryId: detail.ProductCountryId,
            qty: qty * subscription.qty
          });
  
          // create update items
          updateItems.push({
            id: detail.ProductCountryId,
            qty: qty * subscription.qty
          });
        }
      });
    } else {
      if(subscription.PlanOption) {
        subscription.PlanOption.planOptionProducts.forEach(optionProduct => {
          let qty = optionProduct.qty;
          if(qty > 0) {
            orderDetails.push({
              ProductCountryId: optionProduct.ProductCountryId,
              qty: qty * subscription.qty
            });
    
            // create update items
            updateItems.push({
              id: optionProduct.ProductCountryId,
              qty: qty * subscription.qty
            });
          }
        });
      } else {
        subscription.PlanCountry.planDetails.forEach(planDetail => {
          let deliverPlan = JSON.parse(planDetail.deliverPlan);
          let qty = deliverPlan[1];
          if(qty > 0) {
            orderDetails.push({
              ProductCountryId: planDetail.ProductCountryId,
              qty: qty * subscription.qty
            });
    
            // create update items
            updateItems.push({
              id: planDetail.ProductCountryId,
              qty: qty * subscription.qty
            });
          }
        });
      }
    }
    order.orderDetail = orderDetails;

    // console.log(`order.orderDetail === ${JSON.stringify(order.orderDetail)} == ${JSON.stringify(subscription.PlanCountry.planDetails)}`);
    // build order history
    order.orderHistories = [{
      message: order.states
    }];

    let saleReportQueueService = SaleReportQueueService.getInstance();

    // get product country
    return this.db.ProductCountry.findAll({where: {
      id: {$in: _.pluck(updateItems, 'id')},
    }, include: ['Country']})
      .then(productCountries => {
        productCountries = SequelizeHelper.convertSeque2Json(productCountries);
        let tmp = [];
        order.orderDetail.forEach(orderDetail => {
          let productCountry = _.findWhere(productCountries, {id: orderDetail.ProductCountryId});
          if(productCountry) {
            orderDetail.price = productCountry.sellPrice;
            orderDetail.currency = productCountry.Country.currencyDisplay;
            tmp.push(orderDetail);
          }
        });
        order.orderDetail = tmp;
      })
      .then(() => this.db.sequelize.transaction(t => this.db.Order.create(order, { include: ['orderDetail', 'orderHistories'], returning: true, transaction: t})
          .then(_order => {
            order = _order;
            // calculate next delivery date
            let planType;
            if(subscription.PlanCountry) {
              planType = subscription.PlanCountry.Plan.PlanType;
            }
            if(subscription.CustomPlan) {
              planType = subscription.CustomPlan.PlanType;
            }
            let subsequentDeliverDuration = planType.subsequentDeliverDuration;
            let nextDeliverDate = moment().add(subsequentDeliverDuration.split(' ')[0], subsequentDeliverDuration.split(' ')[1]);
            
            // determined status
            let status = subscription.status;
            status = subscription.currentDeliverNumber + 1 === planType.totalDeliverTimes && subscription.currentChargeNumber === planType.totalChargeTimes ? 'Finished' : status;

            let subscriptionChangeInfo = {
              nextDeliverDate,
              status,
              currentDeliverNumber: subscription.currentDeliverNumber + 1,
              totalDeliverTimes: planType.totalDeliverTimes,
              currentOrderId: _order.id,
              followUpEmail2: false
            };
            // update subscription info
            if ( 'Finished' == status ) {
              this.db.SubscriptionHistory.create({
                message: status,
                subscriptionId: subscription.id
              });
            }
            
            return this.db.Subscription.update(subscriptionChangeInfo, {where: {id: subscription.id}, transaction: t});

          })
          .then(() => this.db.Receipt.findOne({where: {SubscriptionId: subscription.id, OrderId: null}, transaction: t}))
          .then(receipt => {
            if(receipt) {
              // update receipt
              return this.db.Receipt.update({OrderId: order.id}, {where: {SubscriptionId: subscription.id, OrderId: null}, transaction: t});
            } else {
              return this.db.Receipt.create({
                OrderId: order.id,
                SubscriptionId: subscription.id,
                last4: subscription.Card.cardNumber,
                branchName: subscription.Card.branchName,
                expiredYear: subscription.Card.expiredYear,
                expiredMonth: subscription.Card.expiredMonth,
                currency: country.currencyDisplay
              }, {transaction: t});
            }
          }))
          .then(() => {
            let orderInboundQueueService = OrderInboundQueueService.getInstance();
            return orderInboundQueueService.addTask(order.id, order.CountryId);
          })
          .then(() => {
            let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
            return taxInvoiceQueueService.addTask(order);
          })
          .then(() => {
            let sendEmailQueueService = SendEmailQueueService.getInstance();
            return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: country.defaultLang.toLowerCase()});
          })
          .then(() => saleReportQueueService.addTask({
            id: order.id
          }))
        );
  }

  process(db) {
    // loggingHelper.log('info', 'subscription-deliver.job process start');
    this.db = db;
    return this.getSubscription();
  }
}

export default SubsciptionDeliverJob;
