import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import moment from 'moment';
import _ from 'underscore';
import config from '../../config';
import SequelizeHelper from '../../helpers/sequelize-helper';
import { ssFtpHelper } from '../../helpers/ss-system-ftp-helper';
import { loggingHelper } from '../../helpers/logging-helper';

const SELLER_COLUMNS = ['campaign', 'badgeId', 'icNumber', 'agentName', 'startDate', 'email', 'mo'];

let latestUpdatedDate = moment('1-1-1990', 'DD-MM-YYYY');

class ReadBadgeFileJob {
  constructor() {
    console.log('create new instance ReadBadgeFileJob');
    loggingHelper.log('info', 'create new instance ReadBadgeFileJob');
  }

  getFiles() {
    loggingHelper.log('info', 'getFiles start');
    return new Promise((resolve, reject) => {
      fs.readdir(config.ongroundApp.localBadgePath, (err, files) => {
        if(err) {
          loggingHelper.log('error', `read-badge-file job: ${err}`); 
          reject(err);
        } else {
          resolve(files);
        }
      });
    });
  }

  readFileAsync(fileName) {
    loggingHelper.log('info', 'readFileAsync start');
    loggingHelper.log('info', `param: fileName = ${fileName}`);
    let dirName = config.ongroundApp.localBadgePath;
    return new Promise((resolve, reject) => {
      fs.readFile(path.resolve(dirName, fileName), (err, data) => {
        if(err) {
          loggingHelper.log('error', `read-badge-file job: ${err}`); 
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  readFiles(files, countries) {
    loggingHelper.log('info', 'readFiles start');
    loggingHelper.log('info', `param: files = ${files}, countries = ${countries}`);
    // fetch all files in models folder
    return Promise.mapSeries(files, fileName => {
      if(fileName.indexOf('.csv') !== -1) {
        let countryCode = fileName.split('_')[0];
        let date = moment(fileName.split('_')[1], 'YYYYMMDD');
        if(latestUpdatedDate.isBefore(date) || latestUpdatedDate.isSame(date)) {
          latestUpdatedDate = date;
          return this.readFileAsync(fileName)
          .then(data => {
            let dataArray = data.toString().split(/\r?\n/);
    
            dataArray.forEach((v, i) => {
              let seller = Object.assign({});
              // set country ID
              let country = _.findWhere(countries, {code: countryCode});
              if(country) {
                seller.CountryId = country.id;

                // add countryid
                if(this.hasCountryIds.indexOf(country.id) === -1) {
                  this.hasCountryIds.push(country.id);
                }
                
                // bind data into sale object
                if(i !== 0 && v !== '') {
                  v = v.replace(/"(.+),(.+)"/g, '$1||$2');
                  let values = v.split(',');
                  values.forEach((value, idx) => {
                    if(idx === 4) {
                      seller[SELLER_COLUMNS[idx]] = moment(value, 'DD/MM/YYYY');
                    } else if(idx === 2) {
                      seller[SELLER_COLUMNS[idx]] = value.replace(/\-/g, '');
                    } else {
                      seller[SELLER_COLUMNS[idx]] = value.replace(/\|\|/g, ',');
                    }
                  });
                  let marketingOffice = _.findWhere(this.marketingOffices, {moCode: seller.mo});
                  if(marketingOffice) {
                    seller.MarketingOfficeId = marketingOffice.id;
                  }
                  if(!_.findWhere(this.sellers, {badgeId: seller.badgeId})) {
                    this.sellers.push(seller);
                  }
                }
              }
            });
            return;
          });
        } else {
          return;
        }
      } else {
        return;
      }
    });
  }

  process(db) {
    // loggingHelper.log('info', 'read-badge-file.job process start');
    this.db = db;
    this.sellers = [];
    this.hasCountryIds = [];
    let fileNames = [];
    // get country
    return this.db.MarketingOffice.findAll()
      .then(marketingOffices => {
        this.marketingOffices = SequelizeHelper.convertSeque2Json(marketingOffices);
        return this.db.Country.findAll();
      })
      .then(countries => {
        this.countries = SequelizeHelper.convertSeque2Json(countries);
        // get file
        return this.db.SellerUser.findOne();
      })
      .then(sellerUser => {
        latestUpdatedDate = sellerUser ? moment(sellerUser.latestUpdatedDate) : latestUpdatedDate;
        return ssFtpHelper.listFiles(config.ongroundApp.ftpBadgePath);
      })
      .then(files => {
        // determine order outbound file
        files.forEach(file => {
          fileNames.push(file.name);
        });
        console.log(`filenames == ${JSON.stringify(fileNames)}`);
        return fileNames;
      })
      .then(() => Promise.mapSeries(fileNames, fileName => ssFtpHelper.getFile(`${config.ongroundApp.ftpBadgePath}/${fileName}`, `${config.ongroundApp.localBadgePath}/${fileName}`)))
      .then(() => this.getFiles())
      .then(files => this.readFiles(files, this.countries)) // readfile
      .then(() => {
        console.log(`this.countries == ${JSON.stringify(this.hasCountryIds)}`);
        if(this.sellers.length > 0) {
          return this.db.SellerUser.update({isActive: false}, {where: {CountryId: {$in: this.hasCountryIds}}});
        }
      })
      .then(() => Promise.mapSeries(this.sellers, seller => {
        console.log(`seller == ${JSON.stringify(seller)}`);
        return this.db.SellerUser.findOne({where: {badgeId: seller.badgeId}})
          .then(result => {
            if(!result) {
              return this.db.SellerUser.create(seller);
            } else {
              seller.isActive = true;
              return this.db.SellerUser.update(seller, {where: {id: result.id}});
            }
          })
          // .then(() => {
          //   // create promo code
          //   let country = _.findWhere(this.countries, {id: seller.CountryId});
          //   let promoCode = {
          //     discount: config.saleApp.discountPercent,
          //     expiredAt: moment().add(1, 'day'),
          //     planCountryIds: 'all',
          //     productCountryIds: 'all',
          //     isGeneric: true,
          //     timePerUser: config.saleApp.maxTimes,
          //     promotionCodes: [{ code: seller.icNumber }],
          //     CountryId: seller.CountryId,
          //     isBaApp: true,
          //     promotionTranslate: [{
          //       name: 'BA-App promotion',
          //       description: `promotion for ${seller.icNumber}`,
          //       langCode: country.defaultLang.toUpperCase()
          //     }]
          //   };
          //   console.log(`promoCode ===== ${promoCode}`);
          //   return this.db.Promotion.create(promoCode, {include: ['promotionCodes', 'promotionTranslate']});
          // })
          .catch(err => {
            console.log(`read padge file ERROR : ${err}`);
            loggingHelper.log('error', `read padge file ERROR : ${err}`);
          });
      }))
      .then(() => this.db.SellerUser.update({latestUpdatedDate}, {where: {id: {$not: null}}})) // update latestUpdatedDate
      .then(() => {
        this.sellers = [];
        console.log('read badge file success');
        loggingHelper.log('info', 'read badge file success');
      })
      .catch(error => {
        console.log(`read badge file error = ${error.stack || JSON.stringify(error)}`);
        loggingHelper.log('error', `read badge file error = ${error.stack || JSON.stringify(error)}`);
      });
  }
}

export default ReadBadgeFileJob;
