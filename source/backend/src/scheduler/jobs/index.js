import config from '../../config';
import SubscriptionDeliverJob from './subscription-deliver.job';
import SubscriptionChargeJob from './subscription-charge.job';
import ReadBadgeFileJob from './read-badge-file.job';
import CreateSaleReportJob from './create-sales-reports.job';
import UpdateProductJob from './update-product.job';
import CreateOrderInboundJob from './create-order-inbound.job';
import SendEmailJob from './send-email.job';
import CancelOrderJob from './cancel-order.job';
import SendFollowUpTrial1 from './send-follow-up-trial-1.job';
import SendFollowUpTrial2 from './send-follow-up-trial-2.job';
import SendFollowUpTrial5Days from './send-follow-up-trial-5-days.job';
import UpdateDeliveredOrderJob from './update-delivered-order.job.js';
import SendSaleReportEmailJob from './send-sales-reports-email.job.js';
import UpdateUrbanFoxOrderJob from './update-urbanfox-orders.job';
import SendSubscribersReportEmailJob from './send-subscribers-reports-email.job.js';
import UpdateSalesReportsJob from './update-sales-reports.job.js';
import CreateCompressedFileToDownloadJob from './create-compressed-file-to-download.job.js';
import SendUsersSaleReportEmailJob from './send-users-sale-report-email.job.js';
import SendUsersReportEmailJob from './send-users-report-email.job.js';
import SendSubscribersSaleReportEmailJob from './send-subscribers-sale-report-email.job.js';
import SendStudentPromoCodeReportEmailJob from './send-student-promo-code-reports-email.job.js';

export let jobs = [
  {
    name: 'SubscriptionDeliverJob',
    target: new SubscriptionDeliverJob(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: 10000,
    startAt: '6:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'SubscriptionChargeJob',
    target: new SubscriptionChargeJob(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: 10000,
    startAt: '2:45:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
    // startAt: moment().add(1, 'minutes').format('hh:mm:SS')
  },
  {
    name: 'SendFollowUpTrial1',
    target: new SendFollowUpTrial1(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: config.scheduler.short
    startAt: '6:15:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'SendFollowUpTrial2',
    target: new SendFollowUpTrial2(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: config.scheduler.short
    startAt: '6:30:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'SendFollowUpTrial5Days',
    target: new SendFollowUpTrial5Days(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: config.scheduler.short
    startAt: '6:45:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'ReadBadgeFileJob',
    target: new ReadBadgeFileJob(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: config.scheduler.short,
    startAt: '23:59:59' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'CreateSaleReportJob',
    target: new CreateSaleReportJob(),
    // interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    interval: config.scheduler.short,
    // startAt: '4:30:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'UpdateProductJob',
    target: new UpdateProductJob(),
    interval: config.scheduler.short,
  },
  {
    name: 'CreateOrderInboundJob',
    target: new CreateOrderInboundJob(),
    interval: config.scheduler.min3,
  },
  {
    name: 'SendEmailJob',
    target: new SendEmailJob(),
    interval: config.scheduler.short,
  },
  {
    name: 'CancelOrderJob',
    target: new CancelOrderJob(),
    interval: config.scheduler.short,
  },
  {
    name: 'UpdateDeliveredOrderJob',
    target: new UpdateDeliveredOrderJob(),
    interval: config.scheduler.short,
  },
  {
    name: 'SendSaleReportEmailJob',
    target: new SendSaleReportEmailJob(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    // interval: config.scheduler.short
    startAt: '08:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'UpdateSalesReportsJob',
    target: new UpdateSalesReportsJob(),
    interval: config.scheduler.min10,
  },
  {
    name: 'SendSubscribersReportEmailJob',
    target: new SendSubscribersReportEmailJob(),
    // interval: 10 * 1000, // this interval time that this task will be run after the first time
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    startAt: '06:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'SendUsersReportEmailJob',
    target: new SendUsersReportEmailJob(),
    // interval: 10 * 1000, // this interval time that this task will be run after the first time
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    startAt: '00:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'UpdateUrbanFoxOrderJob',
    target: new UpdateUrbanFoxOrderJob(),
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    startAt: '04:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'CreateCompressedFileToDownloadJob',
    target: new CreateCompressedFileToDownloadJob(),
    interval: config.scheduler.short,
  },
  {
    name: 'SendUsersSaleReportEmailJob',
    target: new SendUsersSaleReportEmailJob(),
    // interval: 10 * 1000,
    interval: config.scheduler.everyDay, // this interval time that this task will be run after the first time
    startAt: '02:00:00' // this value should be folow those format: 'MM-DD hh:mm:SS', 'DD hh:mm:SS', 'hh:mm:SS'
  },
  {
    name: 'SendSubscribersSaleReportEmailJob',
    target: new SendSubscribersSaleReportEmailJob(),
    interval: config.scheduler.everyWeek, // weekly
    startAt: 'Mon 00:30:00' // 00:30:00 Monday
    // interval: 20 * 1000,
  },
  {
    name: 'SendStudentPromoCodeReportEmailJob',
    target: new SendStudentPromoCodeReportEmailJob(),
    interval: config.scheduler.everyWeek, // weekly
    startAt: 'Mon 00:40:00' // 00:40:00 Monday
    // interval: 20 * 1000,
  }
];
