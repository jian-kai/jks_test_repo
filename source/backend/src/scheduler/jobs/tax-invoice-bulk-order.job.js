import Promise from 'bluebird';

import TaxInvoiceBulkOrderQueueService from '../../services/invoice-number_bulk-order.service';
import { loggingHelper } from '../../helpers/logging-helper';
import config from '../../config';

const TAX_LENGTH = 9;

class TaxInvoiceBulkOrderJob {
  constructor() {
    loggingHelper.log('info', 'Tax invoice (Bulk Order) init');
  }

  formatTaxInvoice(taxNo, countryCode) {
    let iso2 = config.countryCodes[countryCode] || countryCode;
    let zeros = '';
    taxNo = `${taxNo}`;
    for(let i = 0; i < TAX_LENGTH - taxNo.length; i++) {
      zeros += '0';
    }
    return `S2U-${iso2}B-${zeros}${taxNo}`;
  }

  process(db) {
    let data = {};
    let taxInvoiceBulkOrderQueueService = TaxInvoiceBulkOrderQueueService.getInstance();
    return taxInvoiceBulkOrderQueueService.getKeysInQueue()
      .then(orders => {
        if(orders.length > 0) {
          data.orders = orders;
          return db.sequelize.transaction(t => Promise.mapSeries(orders, obj => {
            obj = JSON.parse(obj);
            return db.Country.findOne({where: {id: obj.CountryId}, transaction: t})
            .then(country => 
              db.BulkOrder.findOne({where: {CountryId: obj.CountryId, taxInvoiceNo: {$like: 'S2U-__B-%'}}, order: [['taxInvoiceNo', 'DESC']], include: ['Country'], transaction: t})
                .then(order => {
                  console.log(`formatTaxInvoice ${JSON.stringify(order)}`);
                  let maxTaxInvoice = (order && order.taxInvoiceNo) ? order.taxInvoiceNo.replace(/^(S2U\-\w{3}\-)(\d+)/g, '$2') : 0;
                  maxTaxInvoice = parseInt(maxTaxInvoice);
                  return db.BulkOrder.update({taxInvoiceNo: this.formatTaxInvoice(maxTaxInvoice + 1, country.code)}, {where: {id: obj.id}, transaction: t});
                })
                .catch(error => loggingHelper.log('error', `Create tax invoice (Bulk Order) error: ${error.stack}`))
            );
          })
            .then(() => taxInvoiceBulkOrderQueueService.removeKeysFromQueue(data.orders)))
            .catch(error => loggingHelper.log('error', `Create tax invoice (Bulk Order) error: ${error.stack}`));
        }
      })
      .catch(error => loggingHelper.log('error', `Create tax invoice (Bulk Order) error: ${error.stack}`));
  }
}

export default TaxInvoiceBulkOrderJob;
