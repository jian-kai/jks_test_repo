// import Promise from 'bluebird';
// import moment from 'moment';
import SendEmailQueueService from '../../services/send-email.queue.service';
// import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';

class SendSaleReportEmailJob {
  constructor() {
    console.log('create new instance SendSaleReportEmailJob');
  }

  process(db) {
    this.db = db;
    // get the subscription to prepare for start
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'appco_MYS'}, langCode: 'en'})
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'appco_SGP'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_IM'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_LO'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_BO'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_CAM'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_OPS'}, langCode: 'en'}))
    .then(() => sendEmailQueueService.addTask({method: 'sendSaleReportEmail', data: {reportType: 'mo_AP'}, langCode: 'en'}))
    .catch(error => {
      console.log(`SendSaleReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendSaleReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendSaleReportEmailJob;
