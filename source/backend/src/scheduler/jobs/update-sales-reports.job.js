// import Promise from 'bluebird';
// import config from '../../config';
import { loggingHelper } from '../../helpers/logging-helper';
import SaleReportQueueService from '../../services/sale-report.queue.service';
import moment from 'moment';

class UpdateSalesReportsJob {
  constructor() {
    console.log('create new instance UpdateSalesReportsJob');
  }

  process(db) {
    this.db = db;
    // get the subscription to prepare for start
    return db.sequelize.transaction(t => {
      let options = { raw: true };
      // return this.db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`BulkOrders` ON `shave2u`.`SaleReports`.`bulkOrderId` = `shave2u`.`BulkOrders`.`id` SET `shave2u`.`SaleReports`.`taxInvoiceNo` = `shave2u`.`BulkOrders`.`taxInvoiceNo` WHERE `shave2u`.`SaleReports`.`taxInvoiceNo` IS NULL AND `shave2u`.`SaleReports`.`createdAt` >= \'' + moment().subtract(12, 'minutes').utc().format('YYYY-MM-DD hh:mm:ss') + '\';', null, options)
      // .then(() => this.db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`Orders` ON `shave2u`.`SaleReports`.`orderId` = `shave2u`.`Orders`.`id` SET `shave2u`.`SaleReports`.`taxInvoiceNo` = `shave2u`.`Orders`.`taxInvoiceNo` WHERE `shave2u`.`SaleReports`.`taxInvoiceNo` IS NULL AND `shave2u`.`SaleReports`.`createdAt` >= \'' + moment().subtract(12, 'minutes').utc().format('YYYY-MM-DD hh:mm:ss') + '\';', null, options))
      // return this.db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`BulkOrders` ON `shave2u`.`SaleReports`.`bulkOrderId` = `shave2u`.`BulkOrders`.`id` SET `shave2u`.`SaleReports`.`states` = `shave2u`.`BulkOrders`.`states`, `shave2u`.`SaleReports`.`deliveryId` = `shave2u`.`BulkOrders`.`deliveryId`, `shave2u`.`SaleReports`.`carrierAgent` = `shave2u`.`BulkOrders`.`carrierAgent`, `shave2u`.`SaleReports`.`taxInvoiceNo` = `shave2u`.`BulkOrders`.`taxInvoiceNo`, `shave2u`.`SaleReports`.`updatedAt` = `shave2u`.`BulkOrders`.`updatedAt` WHERE `shave2u`.`BulkOrders`.`updatedAt` >= \'' + moment().subtract(10, 'minutes').utc().format('YYYY-MM-DD hh:mm:ss') + '\';', null, options)
      // .then(() => this.db.sequelize.query('UPDATE `shave2u`.`SaleReports` INNER JOIN `shave2u`.`Orders` ON `shave2u`.`SaleReports`.`orderId` = `shave2u`.`Orders`.`id` SET `shave2u`.`SaleReports`.`states` = `shave2u`.`Orders`.`states`, `shave2u`.`SaleReports`.`deliveryId` = `shave2u`.`Orders`.`deliveryId`, `shave2u`.`SaleReports`.`carrierAgent` = `shave2u`.`Orders`.`carrierAgent`, `shave2u`.`SaleReports`.`taxInvoiceNo` = `shave2u`.`Orders`.`taxInvoiceNo`, `shave2u`.`SaleReports`.`updatedAt` = `shave2u`.`Orders`.`updatedAt` WHERE `shave2u`.`Orders`.`updatedAt` >= \'' + moment().subtract(10, 'minutes').utc().format('YYYY-MM-DD hh:mm:ss') + '\';', null, options))
      return this.db.sequelize.query(`UPDATE shave2u.SaleReports AS SR 
                                      INNER JOIN shave2u.BulkOrders AS BORD 
                                        ON SR.bulkOrderId = BORD.id 
                                      SET SR.states = BORD.states, 
                                        SR.deliveryId = BORD.deliveryId, 
                                        SR.carrierAgent = BORD.carrierAgent, 
                                        SR.taxInvoiceNo = BORD.taxInvoiceNo, 
                                        SR.updatedAt = BORD.updatedAt 
                                      WHERE SR.states <> BORD.states OR 
                                        SR.deliveryId <> BORD.deliveryId OR 
                                        SR.carrierAgent <> BORD.carrierAgent OR 
                                        SR.taxInvoiceNo <> BORD.taxInvoiceNo OR 
                                        SR.updatedAt <> BORD.updatedAt OR
                                        (SR.deliveryId IS NULL AND BORD.deliveryId IS NOT NULL) OR 
                                        (SR.carrierAgent IS NULL AND BORD.carrierAgent IS NOT NULL) OR 
                                        (SR.taxInvoiceNo IS NULL AND BORD.taxInvoiceNo IS NOT NULL);`, null, options)
      .then(() => this.db.sequelize.query(`UPDATE shave2u.SaleReports AS SR 
                                            INNER JOIN shave2u.Orders AS ORD 
                                              ON SR.orderId = ORD.id 
                                            SET SR.states = ORD.states, 
                                              SR.deliveryId = ORD.deliveryId, 
                                              SR.carrierAgent = ORD.carrierAgent, 
                                              SR.taxInvoiceNo = ORD.taxInvoiceNo, 
                                              SR.updatedAt = ORD.updatedAt 
                                            WHERE SR.states <> ORD.states OR 
                                              SR.deliveryId <> ORD.deliveryId OR 
                                              SR.carrierAgent <> ORD.carrierAgent OR 
                                              SR.taxInvoiceNo <> ORD.taxInvoiceNo OR 
                                              SR.updatedAt <> ORD.updatedAt OR 
                                              (SR.deliveryId IS NULL AND ORD.deliveryId IS NOT NULL) OR 
                                              (SR.carrierAgent IS NULL AND ORD.carrierAgent IS NOT NULL) OR 
                                              (SR.taxInvoiceNo IS NULL AND ORD.taxInvoiceNo IS NOT NULL);`, null, options))
      .then(() => this.db.Order.findAll({where: {id: {$notIn: this.db.sequelize.literal('(SELECT OrderId FROM SaleReports WHERE OrderId IS NOT NULL)')}}}))
      .then(orders => {
        if(orders.length > 0) {
          let saleReportQueueService = SaleReportQueueService.getInstance();
          orders.forEach(order => {
            saleReportQueueService.addTask({
              id: order.id,
              source: order.source,
              medium: order.medium,
              campaign: order.campaign,
              term: order.term,
              content: order.content
            });
          });
        }
      });
    })
    .catch(error => {
      console.log(`UpdateSalesReportsJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `UpdateSalesReportsJob ERROR = ${error.stack}`); 
    });
  }
}

export default UpdateSalesReportsJob;
