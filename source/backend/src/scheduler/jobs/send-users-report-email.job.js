import SendEmailQueueService from '../../services/send-email.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class SendUsersReportEmailJob {
  constructor() {
    console.log('create new instance SendUsersReportEmailJob');
  }

  process(db) {
    this.db = db;

    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return sendEmailQueueService.addTask({method: 'sendUsersReportEmail', data: {langCode: 'en'}})
    .catch(error => {
      console.log(`SendUsersReportEmailJob ERROR = ${error.stack}`);
      loggingHelper.log('error', `SendUsersReportEmailJob ERROR = ${error.stack}`); 
    });
  }
}

export default SendUsersReportEmailJob;
