import SequelizeHelper from '../../helpers/sequelize-helper';
import _ from 'underscore';
import UpdateProductQueueService from '../../services/update-product.queue.service';
import { loggingHelper } from '../../helpers/logging-helper';

class UpdateProductJob {
  constructor() {
    console.log('create new instance update product quantity job');
    loggingHelper.log('info', 'create new instance update product quantity job');
  }

  process(db) {
    // loggingHelper.log('info', 'update-product.job process start');
    let updateProductQueueService = UpdateProductQueueService.getInstance();
    this.db = db;
    let items;
    let keys;
    // get keys in update product queue
    return updateProductQueueService.getKeyInQueue()
      .then(_keys => {
        keys = _keys;
        if(keys && keys.length === 0) {
          // loggingHelper.log('error', 'update-product job: queue_empty'); 
          throw new Error('queue_empty'); 
        } else {
          // get value in update product queue
          return updateProductQueueService.getItems(keys);
        }
      })
      .then(_items => {
        items = _items;
        return this.db.ProductCountry.findAll({
          where: {
            id: {
              $in: _.pluck(items, 'id')
            }
          }
        });
      })
      .then(productCountrys => {
        console.log(`build items === ${JSON.stringify(items)} === ${JSON.stringify(productCountrys)}`);
        items.forEach(item => {
          let tmp = _.findWhere(productCountrys, {id: item.id});
          if(tmp) {
            item.qty = tmp.qty - item.qty;
          }
        });
        console.log(`build items 22 === ${JSON.stringify(items)}`);
      })
      .then(() => SequelizeHelper.bulkUpdate(items, this.db, 'ProductCountry', {}))
      .then(() => updateProductQueueService.removeItemsFromQueue(keys))
      .catch(error => {
        if(error.message !== 'queue_empty') {
          loggingHelper.log('error', `update-product job: ${error}`);
        }
      });
  }
}

export default UpdateProductJob;
