import ResponseHelper from '../helpers/response-helper';
import { loggingHelper } from '../helpers/logging-helper';

class CashApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/cash', this.postChargeCash.bind(this));
  }

  postChargeCash(req, res) {
    loggingHelper.log('info', 'postChargeCash start');
    let params = req.body;
    // check input params
    if(!params.token) {
      return ResponseHelper.responseMissingParam(res);
    }
    // check token
    global.tokenService.verifyToken(params.token)
      .then(decoded => {
        console.log(`decoded === ${JSON.stringify(decoded)}`);
        return this.db.Order.update({
          states: this.db.Order.rawAttributes.states.values[1],
          paymentType: this.db.Order.rawAttributes.paymentType.values[2],
        }, {where: {id: decoded.orderId}})
          .then(() => this.db.OrderHistory.create({
            OrderId: decoded.orderId,
            message: this.db.Order.rawAttributes.states.values[1]
          }));
      })

      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.message === 'cart_invalid') {
          return ResponseHelper.responseErrorParam(res, 'Card invalid');
        } else if(error.message === 'token_expired') {
          return ResponseHelper.responseError(res, 'Token is expired');
        } else if(error.message === 'token_is_used') {
          return ResponseHelper.responseError(res, 'Token has been used');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
}

export default function(...args) {
  return new CashApi(...args);
}
