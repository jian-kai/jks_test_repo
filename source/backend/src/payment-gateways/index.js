import { Router } from 'express';
import IPay88 from './ipay88';
import StripeApi from './stripe';
import CashApi from './cash';

export default ({ db }) => {
  let api = Router();

  // register API
  IPay88(api, db);
  StripeApi(api, db);
  CashApi(api, db);

  return api;
};
