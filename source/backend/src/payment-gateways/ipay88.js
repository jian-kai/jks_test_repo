import crypto from 'crypto';
import config from '../config';
import SendEmailQueueService from '../services/send-email.queue.service';
import TaxInvoiceQueueService from '../services/invoice-number.service';
import OrderInboundQueueService from '../services/create-order-inbound.service';
import OrderHelper from '../helpers/order-helper';
import { loggingHelper } from '../helpers/logging-helper';
import moment from 'moment';

class Ipay88API {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/ipay88', this.postIPay88Form.bind(this));
    this.api.post('/responseUrlHandler', this.responseUrlHandler.bind(this));
    this.api.post('/backendUrlHandler', this.backendUrlHandler.bind(this));
  }

  /** POST /ipay88
   * get all users in DB
   */
  postIPay88Form(req, res) {
    loggingHelper.log('info', 'postIPay88Form start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    console.log(`params.orderId === ${params.orderId}`);

    if(!params.orderId) {
      return res.render('ipay88-post-form', {valid: false, errorMessage: 'missing params'});
    }

    // check payment id
    // let paymentIds = Object.values(config.ipay88.paymentId);
    // if(!paymentIds.contains(params.paymentId)) {
    //   return res.render('ipay88-post-form', {valid: false, errorMessage: 'invalid payment id'});
    // }

    // get order detail
    this.db.Order.findOne({where: {id: params.orderId}, include: ['User', 'Receipt', 'Country']})
      .then(order => {
        if(!order) {
          return res.render('ipay88-post-form', {valid: false, errorMessage: 'invalid order id'});
        } else {
          console.log(`oreder === =${JSON.stringify(order)}`);
          // create signature
          let signature = `${config.ipay88.merchantKey}${config.ipay88.merchantCode}${OrderHelper.fromatOrderNumber(order, true)}${parseFloat(order.Receipt.totalPrice).toFixed(2).replace('.', '').replace(',', '')}${order.Country.currencyCode}`;
          console.log(`signature === ${signature}`);
          signature = crypto.createHash('sha1').update(signature).digest('base64');


          // build ipay88 form info
          let ipay88Form = {
            valid: true,
            merchantCode: config.ipay88.merchantCode,
            refNo: OrderHelper.fromatOrderNumber(order, true),
            amount: parseFloat(order.Receipt.totalPrice).toFixed(2),
            currency: order.Country.currencyCode,
            prodDesc: `Charge for order(${params.orderId})`,
            userName: `${order.User.firstName} ${order.User.lastName}`,
            userEmail: order.User.email,
            userContact: 'xxx',
            signature,
            responseURL: config.ipay88.responseURL,
            backendURL: config.ipay88.backendURL
          };
          return res.render('ipay88-post-form', ipay88Form);
        }
      })
      .catch(error => res.render('ipay88-post-form', {valid: false, errorMessage: error.stack}));
  }


  updateOrder(params) {
    loggingHelper.log('info', 'updateOrder start');
    loggingHelper.log('info', `param: ${params}`);
    // start transaction
    let order;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    return this.db.sequelize.transaction(t => this.db.Order.findOne({
      where: {id: OrderHelper.getOrderNumber(params.RefNo)},
      include: ['Country', 'Promotion', 'Receipt', 'User']
    }, {transaction: t}) // get order
      .then(_order => {
        order = _order;
        if(order) {
          // update order
          return this.db.Order.update({
            states: 'Payment Received'
          }, {where: {id: order.id}}, {transaction: t})
            .then(() => this.db.OrderHistory.create({
              OrderId: order.id,
              message: 'Payment Received'
            }));
        } else {
          throw new Error('order_not_existed');
        }
      })

      .then(() => this.db.Cart.findOne({where: {UserId: order.User.id}}))
      .then(cart => {
        if(cart) {
          return this.db.CartDetail.destroy({where: {cartId: cart.id}});
        } else {
          return;
        }
      })
      .then(() => taxInvoiceQueueService.addTask(order))
      .then(() => this.db.Receipt.update({
        chargeId: params.PaymentId,
        chargeFee: 0,
        chargeCurrency: params.Currency
      }, {where: {OrderId: order.id}}, {transaction: t}))
    )
    .then(() => this.db.Cart.findOne({where: {UserId: order.UserId}}))
    .then(cart => {
      if(cart) {
        return this.db.CartDetail.destroy({where: {cartId: cart.id, CountryId: order.Country.id}});
      } else {
        return;
      }
    })
      .then(() => {
        if(order.Receipt.discountAmount > 0 && order.Promotion && !order.Promotion.isGeneric) {
          return this.db.PromotionCode.update({isValid: false}, {where: {code: order.promoCode}});
        } else if(order.Receipt.discountAmount > 0 && order.Promotion && order.Promotion.isGeneric) {
          let appliedTo = JSON.parse(order.Promotion.appliedTo);
          if(!appliedTo) {
            appliedTo = {};
            appliedTo[order.User.id] = 1;
          } else {
            if(appliedTo[order.User.id]) {
              appliedTo[order.User.id] = appliedTo[order.User.id]++;
            } else {
              appliedTo[order.User.id] = 1;
            }
          }
          return this.db.Promotion.update({appliedTo: JSON.stringify(appliedTo)}, {where: {id: order.Promotion.id}});
        }
      })
      .then(() => {
        console.log(`orderInboundQueueService ==== ${JSON.stringify(order)}`);
        if(order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
          return orderInboundQueueService.addTask(order.id, order.CountryId);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => {
        console.log(`sendReceiptsEmail ==== ${JSON.stringify(order)}`);
        return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
      });
  }

  responseUrlHandler(req, res) {
    loggingHelper.log('info', 'responseUrlHandler start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    console.log(`responseUrlHandler ==== ${JSON.stringify(req.body)}`);
    let result = req.body;
    // update order status
    // creat receipt
    if(result.Status === 1 || result.Status === '1') {
      this.updateOrder(result)
        .then(() => {
          res.redirect(`${config.webUrl}thankyou/${OrderHelper.getOrderNumber(result.RefNo)}?ipay88=true&utm_nooverride=1`);
        })
        .catch(error => {
          console.log(`Ipay 88 charge fail: ${error.stack}`);
          loggingHelper.log('error', `Ipay 88 charge fail: ${error.stack}`);
          res.redirect(`${config.webUrl}ipay88/error/${OrderHelper.getOrderNumber(result.RefNo)}/${error.message}`);
        });
    } else {
      console.log(`Ipay 88 charge fail: ${JSON.stringify(result.ErrDesc)}`);
      loggingHelper.log('error', `Ipay 88 charge fail: ${JSON.stringify(result.ErrDesc)}`);
      res.redirect(`${config.webUrl}ipay88/error/${OrderHelper.getOrderNumber(result.RefNo)}/${result.ErrDesc}`);
    }
  }

  backendUrlHandler(req, res) {
    loggingHelper.log('info', 'backendUrlHandler start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    console.log(`backendUrlHandler ==== ${JSON.stringify(req.body)}`);
    // update order status
    let result = req.body;
    // update order status
    // creat receipt
    if(result.Status === 1 || result.Status === '1') {
      this.updateOrder(result)
        .then(() => {
          res.render('ipay88-response-page', { responseObject: JSON.stringify(result)});
        })
        .catch(error => {
          console.log(`Ipay 88 charge fail: ${JSON.stringify(error)}`);
          loggingHelper.log('error', `Ipay 88 charge fail: ${JSON.stringify(error)}`);
          res.render('ipay88-response-page', { responseObject: JSON.stringify(result)});
        });
    } else {
      console.log(`Ipay 88 charge fail: ${JSON.stringify(result.body.ErrDesc)}`);
      loggingHelper.log('error', `Ipay 88 charge fail: ${JSON.stringify(result.body.ErrDesc)}`);
      res.redirect(`${config.webUrl}ipay88/error/${result.RefNo}`);
    }
  }
}

export default function(...args) {
  return new Ipay88API(...args);
}
