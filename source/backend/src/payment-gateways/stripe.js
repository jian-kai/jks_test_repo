import ResponseHelper from '../helpers/response-helper';
import { stripeHelper } from '../helpers/stripe-helper';
import { loggingHelper } from '../helpers/logging-helper';

class StripeApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/stripe', this.postChargeStripe.bind(this));
    this.api.post('/seller/stripe', this.postSellerChargeStripe.bind(this));
  }

  postChargeStripe(req, res) {
    loggingHelper.log('info', 'postChargeStripe start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let countryCode = req.headers['country-code'];
    // check input params
    if(!params.token || !params.cardId) {
      return ResponseHelper.responseMissingParam(res);
    }
    // start transaction
    let order;
    let chargeInfo;
    global.tokenService.verifyToken(params.token)
      .then(decoded => {
        console.log(`decoded === ${JSON.stringify(decoded)}`);
        return this.db.sequelize.transaction(t => this.db.Order.findOne({where: {id: decoded.orderId}}, {transaction: t}) // get order
          .then(_order => {
            order = _order;
            return this.db.Card.findOne({where: {id: params.cardId, isRemoved: false}});
          })
          .then(card => {
            if(!card) {
              throw new Error('card_invalid');
            }
            // make charge to stripe
            let chargeObject = {
              amount: order.totalPrice,
              currency: order.currency.toLowerCase(),
              customer: card.customerId,
              description: `SmartShaves charge for orderId: ${order.id}`
            };
            return stripeHelper.createCharge(chargeObject, countryCode);
          })
          .then(_chargeInfo => {
            chargeInfo = _chargeInfo;
            // get detail fee
            return stripeHelper.retrieveBalanceTransaction(chargeInfo.balance_transaction, countryCode);
          })
          .then(balanceTransaction => {
            console.log(`balanceTransaction == ${JSON.stringify(balanceTransaction)}`);
            chargeInfo.chargeFee = balanceTransaction.fee;
            chargeInfo.chargeCurrency = balanceTransaction.currency;
            // update order
            return this.db.Order.update({
              states: this.db.Order.rawAttributes.states.values[1],
              paymentType: this.db.Order.rawAttributes.paymentType.values[0],
            }, {where: {id: order.id}}, {transaction: t})
              .then(() => this.db.OrderHistory.create({
                OrderId: order.id,
                message: this.db.Order.rawAttributes.states.values[1]
              }));
          })
          .then(() => this.db.Cart.findOne({where: {UserId: order.UserId}}))
          .then(cart => {
            if(cart) {
              return this.db.CartDetail.destroy({where: {cartId: cart.id}});
            }
          })
          .then(() => this.db.Receipt.create({
            OrderId: order.id,
            chargeId: chargeInfo.id,
            chargeFee: chargeInfo.chargeFee / 100,
            chargeCurrency: chargeInfo.chargeCurrency
          }, {transaction: t}))
        );
      })
      .then(() => this.db.UsedToken.create({token: params.token}))
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.message === 'card_invalid') {
          return ResponseHelper.responseError(res, 'Card invalid');
        } else if(error.message === 'token_is_used') {
          return ResponseHelper.responseError(res, 'Token is used');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /seller/stripe
   * charge order from seller app
   * @param {*} req 
   * @param {*} res 
   */
  postSellerChargeStripe(req, res) {
    loggingHelper.log('info', 'postSellerChargeStripe start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let countryCode = req.headers['country-code'];
    // check input params
    if(!params.token || !params.stripeToken) {
      return ResponseHelper.responseMissingParam(res);
    }
    // start transaction
    let order;
    let chargeInfo;
    global.tokenService.verifyToken(params.token)
      .then(decoded => {
        console.log(`decoded === ${JSON.stringify(decoded)}`);
        return this.db.sequelize.transaction(t => this.db.Order.findOne({where: {id: decoded.orderId}}, {transaction: t}) // get order
          .then(_order => {
            order = _order;
            // make charge to stripe
            let chargeObject = {
              amount: order.totalPrice,
              currency: order.currency.toLowerCase(),
              source: params.stripeToken,
              description: `SmartShaves charge for orderId: ${order.id}`
            };
            return stripeHelper.createCharge(chargeObject, countryCode);
          })
          .then(_chargeInfo => {
            chargeInfo = _chargeInfo;
            // get detail fee
            return stripeHelper.retrieveBalanceTransaction(chargeInfo.balance_transaction, countryCode);
          })
          .then(balanceTransaction => {
            console.log(`balanceTransaction == ${JSON.stringify(balanceTransaction)}`);
            chargeInfo.chargeFee = balanceTransaction.fee;
            chargeInfo.chargeCurrency = balanceTransaction.currency;
            // update order
            return this.db.Order.update({
              states: this.db.Order.rawAttributes.states.values[1],
              paymentType: this.db.Order.rawAttributes.paymentType.values[0],
            }, {where: {id: order.id}}, {transaction: t})
              .then(() => this.db.OrderHistory.create({
                OrderId: order.id,
                message: this.db.Order.rawAttributes.states.values[1]
              }));
          })
          .then(() => this.db.Cart.findOne({where: {UserId: order.UserId}}))
          .then(cart => {
            if(cart) {
              return this.db.CartDetail.destroy({where: {cartId: cart.id}});
            }
          })
          .then(() => this.db.Receipt.create({
            OrderId: order.id,
            chargeId: chargeInfo.id,
            chargeFee: chargeInfo.chargeFee / 100,
            chargeCurrency: chargeInfo.chargeCurrency
          }, {transaction: t}))
        );
      })
    .then(() => this.db.UsedToken.create({token: params.token}))
    .then(() => res.json({ok: true}))
    .catch(error => {
      if(error.message === 'card_invalid') {
        return ResponseHelper.responseErrorParam(res, 'Card invalid');
      } else if(error.type === 'StripeCardError') {
        return ResponseHelper.responseError(res, error);
      } else if(error.name === 'StripeInvalidRequestError') {
        return ResponseHelper.responseError(res, error);
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }
}

export default function(...args) {
  return new StripeApi(...args);
}
