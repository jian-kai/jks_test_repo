import Promise from 'bluebird';
import jwt from 'jsonwebtoken';
import _ from 'underscore';
import moment from 'moment';
import config from '../config';
import { stripeHelper } from '../helpers/stripe-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import OrderHelper from '../helpers/order-helper';
import MailchimpHelper from '../helpers/mailchimp';
import DateTimeHelper from '../helpers/datetime-helpers';
import SendEmailQueueService from '../services/send-email.queue.service';
import TaxInvoiceQueueService from '../services/invoice-number.service';

class TrialPlanController {

  constructor(db) {
    this.db = db;
    this.mailchimp = MailchimpHelper.getInstance();
  }

  getIdentityForCheckOut(req, data) {
    return new Promise((resolved, reject) => {
      if(!req.headers['x-access-token'] && !data.params.email) {
        reject(new Error('missing_param'));
      } else if(!req.headers['x-access-token'] && data.params.email) {
        resolved({
          email: data.params.email,
          guest: true
        });
      } else {
        let token = req.headers['x-access-token'];
        jwt.verify(token, config.accessToken.secret, function(err, decoded) {
          if(err) {
            reject(new Error('token_expired'));
          } else {
            resolved(decoded);
          }
        });
      }
    });
  }

  // proccess validate promotion code and update it if it's valided
  proccessPromotion(data) {
    return this.db.Promotion.findOne({where: {
      expiredAt: {
        $gte: moment().format('YYYY-MM-DD')
      },
      CountryId: data.country.id
    }, include: [
      {
        model: this.db.PromotionCode,
        as: 'promotionCodes',
        where: {
          code: data.params.promotionCode,
          isValid: true
        }
      }
    ]})
      .then(promotion => {
        if(!promotion) {
          return {
            id: undefined,
            discount: 0
          };
        } else {
          // check production ID
          let intersectionProduct;
          let intersectionPlan;
          let returnObj = promotion;
          if(promotion.productCountryIds && promotion.productCountryIds !== 'all') {
            intersectionProduct = _.intersection(JSON.parse(promotion.productCountryIds), data.params.productCountryIds);
            returnObj.productCountryIds = intersectionProduct;
          }
          if(promotion.productCountryIds && promotion.planCountryIds !== 'all') {
            intersectionPlan = _.intersection(JSON.parse(promotion.planCountryIds), data.params.planCountryIds);
            returnObj.planCountryIds = intersectionPlan;
          }
          if(promotion.freeProductCountryIds) {
            returnObj.freeProductCountryIds = JSON.parse(promotion.freeProductCountryIds);
          }
          
          if(promotion.isGeneric && data.params.userTypeId) {
            let appliedTo = JSON.parse(promotion.appliedTo);
            if(data.params.userTypeId === 1 && !data.identity.guest) {
              if(appliedTo && appliedTo[data.identity.id] && appliedTo[data.identity.id] >= promotion.timePerUser) {
                throw new Error('promo_used_limited');
                // return {
                //   id: undefined,
                //   discount: 0
                // };
              }
            } else {
              if(appliedTo && appliedTo[data.UserId] && appliedTo[data.UserId] >= promotion.timePerUser) {
                throw new Error('promo_used_limited');
                // return {
                //   id: undefined,
                //   discount: 0
                // };
              }
            }
          }
          return returnObj;
        }
      })
      .then(promotion => {
        // promotion = SequelizeHelper.convertSeque2Json(promotion);
        if(promotion.freeProductCountryIds) {
          return this.db.ProductCountry.findAll(
            {
              where: {id: {$in: promotion.freeProductCountryIds}},
              include: [this.db.Country]
            }
          )
          .then(productCountrys => {
            promotion.freeProductCountries = productCountrys;
            return promotion;
          });
        } else {
          return promotion;
        }
      });
  }

  makeOrder(data, t) {
    // get all product country
    let orderObject = {};
    let receiptObject = {
      totalPrice: 0
    };
    let updatedProducts = [];
    let discountAmount = 0;
    let currentChargeTotal = 0;
    let country;

    // init subscription data
    // data.subscription = {};

    // calculate total price
    receiptObject.currency = data.planCountry.Country.currencyDisplay;
    data.chargeCurrency = data.planCountry.Country.currencyCode;
    country = data.planCountry.Country;

    if(data.promotion && data.promotion.minSpend && (data.promotion.minSpend > currentChargeTotal)) {
      discountAmount = 0;
    } else if(data.promotion && data.promotion.maxDiscount && (discountAmount > data.promotion.maxDiscount)) {
      discountAmount = data.promotion.maxDiscount;
    } else if(data.promotion && data.promotion.promotionCodes && data.promotion.promotionCodes[0].code === 'EVENTRM1') {
      discountAmount = data.promotion.maxDiscount;
    }

    // set value for receive
    let price = 0;
    if(data.planCountry.planOption) {
      data.planCountry.planOption.planOptionProducts.forEach(opProduct => {
        price = parseFloat(price) + parseFloat(opProduct.ProductCountry.sellPrice);
      });
    } else {
      price = data.planCountry.sellPrice;
    }
    let trialkit_price = 0;
    //Malaysia
    if (country.id === 1) {
      trialkit_price = 6.5;
    } else if (country.id === 7) {
      //Singapore
      trialkit_price = 5.0;
    } else {
      trialkit_price = 0;
    }
    receiptObject.subTotalPrice = parseFloat(trialkit_price);
    receiptObject.discountAmount = 0;
    receiptObject.taxAmount = 0;
    receiptObject.totalPrice = parseFloat(trialkit_price);
    receiptObject.discountAmount = discountAmount;
    receiptObject.subTotalPrice =
    receiptObject.totalPrice - +receiptObject.discountAmount;

    // calculate shipping fee
    // let shippingFeeConfig = config.shippingFee[data.planCountry.Country.code.toUpperCase()];
    let shippingFeeConfig = {
      minAmount: data.planCountry.Country.shippingMinAmount,
      fee: data.planCountry.Country.shippingFee,
      trialFee: data.planCountry.Country.shippingTrialFee
    };
    if (data.params.userTypeId === 2) {
      if (
        data.country.code !== "MYS" &&
        data.country.code !== "SGP" &&
        data.country.code !== "KOR"
      ) {
        receiptObject.shippingFee = 0;
      } else if (shippingFeeConfig) {
        receiptObject.shippingFee = shippingFeeConfig.trialFee;
        receiptObject.totalPrice =
          shippingFeeConfig.trialFee - +receiptObject.discountAmount;
      }
    } else if (data.params.userTypeId === 1) {
      if (
        data.country.code !== "MYS" &&
        data.country.code !== "SGP" &&
        data.country.code !== "KOR"
      ) {
        receiptObject.shippingFee = 0;
      } else if (shippingFeeConfig) {
        receiptObject.shippingFee = 0;
        receiptObject.totalPrice =
          shippingFeeConfig.trialFee - +receiptObject.discountAmount;
      }
    } else if (shippingFeeConfig) {
      receiptObject.shippingFee = 0;
      receiptObject.totalPrice =
        shippingFeeConfig.trialFee - +receiptObject.discountAmount;
    }

    orderObject.email = data.params.email ? data.params.email : data.identity.email;
    orderObject.phone = data.params.phone;
    orderObject.fullName = data.params.fullName;
    orderObject.DeliveryAddressId = data.params.DeliveryAddressId;
    orderObject.BillingAddressId = data.params.BillingAddressId;
    orderObject.orderDetail = {
      qty: 1,
      price: data.planCountry.planOption ? data.planCountry.planOption.sellPrice : data.planCountry.sellPrice,
      currency: data.planCountry.Country.currencyDisplay,
      PlanCountryId: data.planCountry.id,
      // PlanOptionId: data.planCountry.planOption.id
    };
    if(data.promotion.freeProductCountryIds) {
      let orderDetails = [];
      orderDetails.push(orderObject.orderDetail);
      data.promotion.freeProductCountries.forEach(freeProductCountry => {
        orderDetails.push({
          qty: 1,
          price: freeProductCountry.sellPrice,
          currency: freeProductCountry.Country.currencyDisplay,
          ProductCountryId: freeProductCountry.id,
        });
      });
      orderObject.orderDetail = orderDetails;
    }
    orderObject.taxRate = country.taxRate;
    orderObject.taxName = country.taxName;
    orderObject.CountryId = country.id;
    orderObject.subscriptionIds = JSON.stringify([data.subscription.id]);
    orderObject.SubscriptionId = data.subscription.id;

    // build GA data
    orderObject.source = data.params.utmSource ? data.params.utmSource.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : data.params.utmSource;
    orderObject.medium = data.params.utmMedium;
    orderObject.campaign = data.params.utmCampaign;
    orderObject.term = data.params.utmTerm;
    orderObject.content = data.params.utmContent;

    if(discountAmount > 0 || data.promotion.freeProductCountryIds) {
      orderObject.PromotionId = data.promotion.id;
      orderObject.promoCode = data.params.promotionCode;
    }
    
    // process set value for order state and payment type
    if(data.params.userTypeId === 2) {
      if(data.country.code !== 'MYS' && data.country.code !== 'SGP') {
        orderObject.states = 'Completed';
      } else {
        orderObject.states = 'On Hold';
      }
    } else {
      orderObject.states = 'On Hold';
    }
    orderObject.paymentType = 'stripe';

    // process set value for userID or seller ID
    console.log(`make order dxata ====== ${JSON.stringify(data)} ===== `);
    if(data.params.userTypeId === 1 && !data.identity.guest) {
      orderObject.UserId = data.params.UserId ? data.params.UserId : data.identity.id;
    } else if(data.UserId) {
      orderObject.UserId = data.UserId;
    }
    if(data.params.userTypeId === 2) {
      orderObject.SellerUserId = data.identity.id;
      orderObject.channelType = data.params.channelType;
      orderObject.eventLocationCode = data.params.eventLocationCode;
      orderObject.MarketingOfficeId = data.identity.MarketingOfficeId;
    }

    console.log(`make order orderObject ====== ${JSON.stringify(orderObject)} ===== `);

    // build order history
    orderObject.isDirectTrial = data.params.directTrial;
    orderObject.orderHistories = [{
      message: orderObject.states
    }];

    // perform in server order
    return this.db.Order.create(orderObject, { include: ['orderDetail', 'orderHistories'], returning: true, transaction: t })
      .then(order => ({ order, updatedProducts, receiptObject, country }));
  }

  /**
   * make charge to stripe
   * @param {*} data 
   */
  makeChargeToStripe(data) {
    let chargeInfo;
    let collectedFrom = (data.params.userTypeId === 2) ? 'Sales App' : 'Website';
    let options = {where: {id: data.params.CardId, isRemoved: false}};
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    let type = data.identity.isSeller ? 'baWeb' : 'website';
    let errorMessage;
    if(data.identity.id && data.params.userTypeId === 1) {
      options.where = Object.assign(options.where, {UserId: data.params.UserId ? data.params.UserId : data.identity.id});
    } else {
      options.where = Object.assign(options.where, {UserId: data.UserId});
    }
    return this.db.Card.findOne(options)
    .then(card => {
      if(!card) {
        throw new Error('card_invalid');
      }
      // store last4 card number
      data.receiptObject.last4 = card.cardNumber;
      data.receiptObject.branchName = card.branchName;
      data.receiptObject.expiredYear = card.expiredYear;
      data.receiptObject.expiredMonth = card.expiredMonth;

      // make charge to stripe
      data.order.Country = {
        code: data.country.code
      };

      return taxInvoiceQueueService.addTask(data.order)
      .then(() => {
        if(data.receiptObject.totalPrice > 0) {
          console.log(`data.receiptObject.totalPrice ====== ${data.receiptObject.totalPrice}`);
          let chargeObject = {
            amount: data.receiptObject.totalPrice,
            currency: data.chargeCurrency.toLowerCase(),
            customer: card.customerId,
            description: `Shaves2u charge for orderId: ${OrderHelper.fromatOrderNumber(data.order, true)}`,
            metadata: {
              collectedFrom
            }
          };
          return stripeHelper.createCharge(chargeObject, data.country.code, type)
          .then(_chargeInfo => {
            chargeInfo = _chargeInfo;
            if((data.promotion && data.promotion.promotionCodes && data.promotion.promotionCodes[0].code === 'EVENTRM1') || data.params.directTrial) {
              return this.db.Order.update({states: 'Completed'}, {where: {id: data.order.id}})
              .then(() => {
                let startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.startFirstDelivery);
                return this.db.Subscription.update(
                  {
                    nextDeliverDate: startDeliverDate,
                    nextChargeDate: startDeliverDate,
                    startDeliverDate,
                  },
                  {where: {id: data.subscription.id}}
                );
              });
            } else {
              return this.db.Order.update({states: 'Payment Received'}, {where: {id: data.order.id}});
            }
          })
          .then(() => this.db.OrderHistory.create({
            OrderId: data.order.id,
            message: 'Payment Received'
          }))
          .then(() => {
            if((data.promotion && data.promotion.promotionCodes && data.promotion.promotionCodes[0].code === 'EVENTRM1') || data.params.directTrial) {
              this.db.OrderHistory.create({
                OrderId: data.order.id,
                message: 'Completed'
              });
            }
          })
          // .then(() => taxInvoiceQueueService.addTask(data.order))
          .then(() => stripeHelper.retrieveBalanceTransaction(chargeInfo.balance_transaction, data.country.code, type))
          .catch(err => {
            this.db.OrderHistory.create({
              OrderId: data.order.id,
              message: 'Payment Fail'
            })
            .then(() =>  {
              var S = err.message.toUpperCase();
            if(S.match(/INSUFFICIENT FUNDS/) && type =="baWeb")
            {
              errorMessage= "Cancelled due to user card has insufficient funds.";
            }
            else{
              errorMessage=  'Canceled due to payment failed';
            }
          }
          )
            .then(() => this.db.Order.update({states: 'Canceled'}, {where: {id: data.order.id}}))
            .then(() => this.db.Subscription.update({status: 'Canceled', cancellationReason: errorMessage}, {where: {id: data.subscription.id}}))
            .then(() => {
              
              this.db.SubscriptionHistory.create({
                message: 'Canceled',
                subscriptionId: data.subscription.id,
                detail: errorMessage
              });

              return this.db.OrderHistory.create({
                OrderId: data.order.id,
                message: 'Canceled',
                cancellationReason: errorMessage
              });
              
            })
            .then(() => {
              console.log(err.stack);
              let sendEmailQueueService = SendEmailQueueService.getInstance();
              let userId = data.params.userTypeId === 1 ? (data.params.UserId ? data.params.UserId : data.identity.id) : data.UserId;
              // send email to user
              sendEmailQueueService.addTask({method: 'sendPaymentFail', data: {userId}, langCode: data.country.defaultLang.toLowerCase()});
              throw err;
            });
            throw err;
          });
        } else {
          return {};
        }
      });
    });
  }

  createReceipt(data, t) {
    return this.db.Receipt.create(data.receiptObject, {returning: true, transaction: t});
  }

  /**
   * Create subscription
   * @param {*} data 
   */
  createSubscription(data, t) {
    // set value for subscription
    let startDeliverDate;
    let isOffline = true;
    if(data.params.isWeb) {
      startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery);
      console.log(`startDeliverDate Web ===== ${config.trialPlan.webFirstDelivery} === ${startDeliverDate}`);
      isOffline = false;
    } else {
      if(data.country.code === 'SGP') {
        startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery);
      } else {
        startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery);
      }
      console.log(`startDeliverDate BA ===== ${config.trialPlan.baFirstDelivery} === ${startDeliverDate}`);
    }

    let price = data.planCountry.planOption ? data.planCountry.planOption.sellPrice : data.planCountry.sellPrice;
    let subscription = {
      PlanCountryId: data.params.planCountryId,
      DeliveryAddressId: data.params.DeliveryAddressId,
      BillingAddressId: data.params.BillingAddressId,
      fullname: data.params.fullName,
      email: data.params.email,
      phone: data.params.phone,
      qty: 1,
      totalDeliverTimes: data.planCountry.Plan.PlanType.totalDeliverTimes,
      totalChargeTimes: data.planCountry.Plan.PlanType.totalChargeTimes,
      currentChargeNumber: 0,
      currentDeliverNumber: 0,
      // nextDeliverDate: startDeliverDate,
      // nextChargeDate: startDeliverDate,
      // startDeliverDate,
      price,
      pricePerCharge: price,
      isTrial: data.planCountry.Plan.isTrial,
      isOffline,
      PlanOptionId: data.params.PlanOptionId,
      MarketingOfficeId: data.identity.MarketingOfficeId,
      discountPercent: data.promotion.id ? data.promotion.discount : 0,
      promotionId: data.promotion.id ? data.promotion.id : null,
      promoCode: (data.promotion.id && data.promotion.promotionCodes && data.promotion.promotionCodes[0]) ? data.promotion.promotionCodes[0].code : null,
      isDirectTrial: data.params.directTrial
    };

    if(data.promotion.id) {
      data.hasUsedPromoCode = true;
    }

    // calculate expired date
    if(data.country.code === 'KOR') {
      subscription.expiredDateAt = moment(config.deliverDateForKorea).add(1, 'years').format('YYYY-MM-DD');
    } else {
      subscription.expiredDateAt = moment().add(1, 'years').format('YYYY-MM-DD');
    }

    if(data.params.CardId) {
      subscription.CardId = data.params.CardId;
    }
    // set identity
    if(data.params.userTypeId === 1 && !data.identity.guest) {
      subscription.UserId = data.params.UserId ? data.params.UserId : data.identity.id;
    } else if(data.identity.guest) {
      subscription.UserId = data.UserId;
    }
    if(data.params.userTypeId === 2) {
      subscription.SellerUserId = data.identity.id;
      subscription.UserId = data.UserId;
      subscription.channelType = data.params.channelType;
      subscription.eventLocationCode = data.params.eventLocationCode;
    }

    return new Promise((ok, fail) => {
      this.db.Subscription.create(subscription, {returning: true, transaction: t})
      .then((subs) => {
        this.db.SubscriptionHistory.create({
          message: 'On Hold',
          subscriptionId: subs.id
        });

        ok(subs);
      })
      .catch(error => {
        fail(error);
      });
    });
    
  }

  /** Perform process checkout for 
   * create order
   * @param {*} req 
   */
  processCheckout(req) {
    let params = req.body;
    params.userTypeId = params.userTypeId === 2 ? params.userTypeId : 1;

    let data = {
      params,
      country: req.country
    };

    // get identity for place order
    return this.getIdentityForCheckOut(req, data)
      .then(identity => {
        // check promo code
        data.identity = identity;
      })
      .then(() => 
        // create subscription if needed
        this.db.sequelize.transaction(t => this.db.PlanCountry.findOne({where: {id: data.params.planCountryId}, include: ['Country', {
          model: this.db.Plan,
          where: {isTrial: true},
          include: ['PlanType']
        }]}, {transaction: t})
          .then(planCountry => {
            if(!planCountry) {
              throw new Error('not_found');
            } else {
              data.planCountry = SequelizeHelper.convertSeque2Json(planCountry);
              return this.db.DeliveryAddress.findOne({where: {id: data.params.DeliveryAddressId}});
            }
          })
          .then(deliveryAddress => {
            data.deliveryAddress = deliveryAddress;
            return this.db.PlanOption.findOne(
              {
                where: {id: data.params.PlanOptionId},
                include: [
                  {
                    model: this.db.PlanOptionProduct,
                    as: 'planOptionProducts',
                    include: ['ProductCountry']
                  }, 'planOptionTranslate'
                ]
              }, {transaction: t}
            );
          })
          .then(planOption => {
            data.planCountry.planOption = SequelizeHelper.convertSeque2Json(planOption);
            // check email
            return this.db.User.findOne({where: {email: params.email}});
          })
          .then(user => {
            if(!user && ((params.userTypeId === 2) || (data.identity.guest))) {
              let _user;
              let firstName;
              let lastName;
              if(data.deliveryAddress) {
                firstName = data.deliveryAddress.firstName;
                lastName = data.deliveryAddress.lastName;
              } else if(params.fullName && params.fullName.indexOf(' ') > 0) {
                firstName = params.fullName.split(' ')[0];
                lastName = params.fullName.split(' ')[1];
              } else {
                firstName = params.fullName;
              }
              data.newCustomer = true;
              return this.db.User.create({
                email: params.email,
                firstName,
                lastName,
                CountryId: req.country.id,
                isGuest: true
              }, {returning: true, transaction: t})
                .then(result => {
                  _user = result;
                  return this.db.Cart.create({UserId: result.id}, {transaction: t})
                    .then(() => {
                      let utmDefaultData = this.mailchimp.getDefaultUtmData();
                      this.mailchimp.addMemberToRegisterList({
                        'email_address': params.email,
                        'status': 'subscribed',
                        'merge_fields': {
                          FNAME: firstName,
                          LNAME: lastName ? lastName : '',
                          COUNTRY: req.country.code.substr(0, 2),
                          SOURCE: params.userTypeId === 2 ? 'BA app' : 'Website',
                          USOURCE: params.utmSource ? params.utmSource.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : this.mailchimp.getDefaultUtmData('source'),
                          UMEDIUM: params.utmMedium ? params.utmMedium : utmDefaultData,
                          UCAMPAIGN: params.utmCampaign ? params.utmCampaign : utmDefaultData,
                          UCONTENT: params.utmContent ? params.utmContent : utmDefaultData,
                          UTERM: params.utmTerm ? params.utmTerm : utmDefaultData
                        }
                      });
                    });
                })
                .then(() => _user);
            } else if(user && !user.phone && params.userTypeId === 2) {
              // update user if order make from BA-app
              return this.db.User.update({phone: params.phone}, {where: {id: user.id}})
                .then(() => user);
            } else {
              return user;
            }
          })
          .then(user => {
            data.UserId = user.id;
            data.user = user;
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.DeliveryAddress.update({UserId: user.id}, {where: {id: data.params.DeliveryAddressId}, transaction: t});
            }
          })
          .then(() => {
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.DeliveryAddress.update({UserId: data.UserId}, {where: {id: data.params.BillingAddressId}, transaction: t});
            }
          })
          .then(() => {
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.Card.update({UserId: data.UserId}, {where: {id: data.params.CardId}, transaction: t});
            }
          })
          .then(() => this.proccessPromotion(data))
          .then(promotion => {
            data.promotion = promotion;
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.Subscription.findAll({where: {UserId: data.UserId}});
            } else {
              return this.db.Subscription.findAll({where: {UserId: data.params.UserId ? data.params.UserId : data.identity.id}});
            }
          })
          .then(subsciber => {
            data.isSubscriber = (subsciber.length > 0);
            return this.createSubscription(data, t);
          })
          .then(subscription => {
            data.subscription = SequelizeHelper.convertSeque2Json(subscription);
            // calculate price and create order
            return this.makeOrder(data, t);
          })
          .then(result => {
            data = Object.assign(data, result);
            data.receiptObject.OrderId = data.order.id;
            return this.createReceipt(data, t);
          })
          .then(receipt => {
            this.receiptObject = SequelizeHelper.convertSeque2Json(receipt);
          }))
          .then(() => this.makeChargeToStripe(data)) // make charge to stripe
          .then(balanceTransaction => {
            console.log(`data.receiptObject ${JSON.stringify(data.receiptObject)} `);
            if(balanceTransaction) {
              data.receiptObject.chargeId = balanceTransaction.source;
              data.receiptObject.chargeFee = balanceTransaction.fee;
              data.receiptObject.chargeCurrency = balanceTransaction.currency;
            }
            // create receipt
            return this.db.Receipt.update(data.receiptObject, {where: {OrderId: data.order.id}});
          })
          .then(() => this.db.Subscription.update({status: 'Processing'}, {where: {id: data.subscription.id}}))
          .then(() => {
            this.db.SubscriptionHistory.create({
              message: 'Processing',
              subscriptionId: data.subscription.id
            });
          })
          .then(() => {
            if((data.receiptObject.discountAmount !== 0 && !data.promotion.isGeneric) || (data.promotion.freeProductCountryIds && !data.promotion.isGeneric)) {
              return this.db.PromotionCode.update({isValid: false}, {where: {code: data.params.promotionCode}});
            } else if((data.receiptObject.discountAmount !== 0 || data.hasUsedPromoCode) && data.promotion.isGeneric) {
              let appliedTo = JSON.parse(data.promotion.appliedTo);
              if(!appliedTo) {
                appliedTo = {};
                if(data.params.userTypeId === 1 && !data.identity.guest) {
                  appliedTo[data.identity.id] = 1;
                } else {
                  appliedTo[data.UserId] = 1;
                }
              } else {
                if(data.params.userTypeId === 1 && !data.identity.guest) {
                  if(appliedTo[data.identity.id]) {
                    appliedTo[data.identity.id] = appliedTo[data.identity.id] + 1;
                  } else {
                    appliedTo[data.identity.id] = 1;
                  }
                } else {
                  if(appliedTo[data.UserId]) {
                    appliedTo[data.UserId] = appliedTo[data.UserId] + 1;
                  } else {
                    appliedTo[data.UserId] = 1;
                  }
                }
              }
              return this.db.Promotion.update({appliedTo: JSON.stringify(appliedTo)}, {where: {id: data.promotion.id}});
            }
          })
          .then(() => {
            data.user.Country = data.user.Country ? data.user.Country : data.order.Country;
            console.log(`create trial === ${JSON.stringify(data.user)}`);
            this.mailchimp.addMemberToTrialSubscriptionList(data);
            Reflect.deleteProperty(data, 'user');
          })
          .then(() => data)
      );
  }
}

export default TrialPlanController;
