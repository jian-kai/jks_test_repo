import Promise from 'bluebird';
// import jwt from 'jsonwebtoken';
// import _ from 'underscore';
import moment from 'moment';
// import config from '../config';
// import { stripeHelper } from '../helpers/stripe-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import OrderHelper from '../helpers/order-helper';
// import SendEmailQueueService from '../services/send-email.queue.service';
// import TaxInvoiceQueueService from '../services/invoice-number.service';
import { loggingHelper } from '../helpers/logging-helper';
import MailChimp from '../helpers/mailchimp';
import json2csv from 'json2csv';

// const CART_DISCOUNT_PERCENT = 10;

class DownloadController {

  constructor(db) {
    this.db = db;
    this.mailchimp = MailChimp.getInstance();
  }

  _getPlanInclude() {
    let include = [
      { 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: ['Country', {
          model: this.db.PlanDetail,
          as: 'planDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }, {
          model: this.db.PlanTrialProduct,
          as: 'planTrialProducts',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }]
      }
    ];
    return include;
  }

  downloadSubscribersReportForCronjob(options) {
    return this.db.Subscription.findAll(options)
    .then(subscriptions => {
      if(subscriptions.length === 0) return '';

      subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);

      // build order data
      let tmp = {};
      let subscriptionTmp = [];
      subscriptions.forEach(subscription => {
        let customSku = '';

        let cancellationReason = subscription.cancellationReason;
        let canceledDate = moment(subscription.updatedAt).format('YYYY-MM-DD');

        if(subscription.CustomPlan) {
          subscription.CustomPlan.customPlanDetail.forEach(detail => {
            customSku += `${detail.ProductCountry.Product.sku}, `;
          });
          customSku = customSku.replace(/^(.*)\,\s$/g, '$1');
        }

        let lastName = subscription.User.lastName || '';

        tmp = {
          customerName: `${subscription.User.firstName} ${lastName}`,
          email: subscription.email,
          country: subscription.User.Country.name,
          sku: subscription.PlanCountry ? subscription.PlanCountry.Plan.sku : '',
          customSku,
          category: subscription.PlanCountry ? subscription.PlanCountry.Plan.PlanType.name : (subscription.CustomPlan ? subscription.CustomPlan.PlanType.name : ''),
          isTrial: subscription.isTrial ? 'Yes' : 'No',
          status: subscription.status,
          cancellationReason,
          canceledDate,
          deliveryAddress: `${subscription.DeliveryAddress.address} ${subscription.DeliveryAddress.city} ${subscription.DeliveryAddress.portalCode} ${subscription.DeliveryAddress.state}`,
          billingAddress: `${subscription.BillingAddress.address} ${subscription.BillingAddress.city} ${subscription.BillingAddress.portalCode} ${subscription.BillingAddress.state}`,
          createdAt: moment(subscription.createdAt).format('YYYY-MM-DD'),
          startDeliverDate: moment(subscription.startDeliverDate).format('YYYY-MM-DD'),
          deliverNum: subscription.currentDeliverNumber,
          updatedAt: moment(subscription.updatedAt).format('YYYY-MM-DD'),
          nextChargeDate: moment(subscription.nextChargeDate).format('YYYY-MM-DD')
        };
        subscriptionTmp.push(tmp);
      });

      // create CSV and response file
      let fields = ['customerName', 'email', 'country', 'sku', 'customSku', 'category', 'isTrial', 'status', 'cancellationReason', 'canceledDate', 'deliveryAddress', 'billingAddress', 'createdAt', 'startDeliverDate', 'deliverNum', 'lastDeliverydate', 'nextChargeDate'];
      let fieldNames = ['Customer Name', 'Email', 'Country', 'Shave Plan (SKU)', 'Shave Plan w/o FT (SKU)', 'Category', 'Is Trial?', 'Status', 'Cancellation Reason', 'Canceled Date', 'Delivery Address', 'Billing Address', 'Created At', 'Start Delivery Date', 'No. of Delivery', 'Last Delivered', 'Next Charge'];
      return json2csv({
        data: subscriptionTmp,
        fields,
        fieldNames
      });
    });
  }

  downloadUsersReportForCronjob(options, dateDistance) {
    return this.db.Subscription.findAll(options)
    .then(subs => {
      if(subs.length === 0) return subs;

      subs = SequelizeHelper.convertSeque2Json(subs);

      return Promise.mapSeries(subs, sub => {
        return this.db.Order.findAll({
          where: {
            $or: [
              {subscriptionIds: {$like: `%[${sub.id}]%`}},
              {subscriptionIds: {$like: `%,${sub.id},%`}},
              {subscriptionIds: {$like: `%[${sub.id},%`}},
              {subscriptionIds: {$like: `%,${sub.id}]%`}},
              {subscriptionIds: {$like: `%"${sub.id}"%`}}
            ],
            $and: [
              {states: {$not: 'Pending'}},
              {states: {$not: 'On Hold'}}
            ]
          },
          include: ['Country'],
          order: [['id', 'DESC']]
        })
        .then(orders => {
          sub.orders = orders;
          return sub;
        });
      });
    })
    .then(subs => {
      if(subs.length === 0) return Promise.resolve('');

      let tmp = {};
      let data = [];
      let dateFormat = 'YYYY-MM-DD HH:mm';

      subs.forEach(sub => {
        let orders = sub.orders;

        if(orders.length > 0) {
          // if sub has orders
          orders = SequelizeHelper.convertSeque2Json(orders);

          let order = orders[0];

          if(sub.status != 'Canceled') {
            // order updated date must be greater than start selecting date
            // if not, skip current subs
            if(moment(order.updatedAt).format(dateFormat) <= moment().subtract(dateDistance, 'days').format(dateFormat)) {
              return;
            }
          }

          let lastName = sub.User.lastName || '';
          let customerName = sub.SellerUserId ? order.fullName : `${sub.User.firstName} ${lastName}`;
          
          let canceledDate = '';
          if(sub.status == 'Canceled') {
            canceledDate = sub.updatedAt;
            if(sub.subscriptionHistories) {
              let history = sub.subscriptionHistories.find(item => item.message == 'Canceled');
              if(history) {
                canceledDate = history.updatedAt;
              }
            }
            canceledDate = moment(canceledDate).format(dateFormat);
          }

          let subsStatus = '';
          if(sub.isTrial) {
            let expiredTrialDate = moment(sub.nextDeliverDate).format(dateFormat);
            if(sub.status == 'Canceled') {
              if(expiredTrialDate > canceledDate) {
                // currently trial
                subsStatus = 'Canceled (During Free Trial)';
              } else {
                // trial expired
                subsStatus = 'Canceled (After Free Trial)';
              }
            } else {
              let today = moment().format(dateFormat);
              if(today <= expiredTrialDate) {
                // currently trial
                subsStatus = 'Currently Trial Period';
              } else {
                // trial expired
                subsStatus = 'Active Customer (From Free Trial)';
              }
            }
          } else if(sub.CustomPlanId) {
            if(sub.status == 'Canceled') {
              subsStatus = 'Canceled (Without Free Trial)';
            } else {
              subsStatus = 'Active Customer (Without Free Trial)';
            }
          } else {
            // Old plans (not trial, not custom)
            if(sub.status == 'Canceled') {
              subsStatus = 'Canceled';
            } else {
              subsStatus = 'Active Customer';
            }
          }

          // get deliveried date and charged data
          let packageSentDate = '';
          let paidDate = '';
          orders.forEach(tmpOrder => {
            if(tmpOrder.states != 'Canceled') {
              // append charged date
              paidDate = paidDate ? `${paidDate}, ` : '';
              paidDate += moment(tmpOrder.createdAt).format(dateFormat);

              if(tmpOrder.states == 'Completed') {
                // append sent date
                packageSentDate = packageSentDate ? `${packageSentDate}, ` : '';
                packageSentDate += moment(tmpOrder.updatedAt).format(dateFormat);
              }
            }
          });

          let shavePlanSku = '';
          if(sub.CustomPlanId) {
            shavePlanSku = `Custom Plan: ${sub.CustomPlan.description}`;
          } else if(sub.PlanCountryId) {
            shavePlanSku = sub.PlanCountry.Plan.sku;
          }

          tmp = {
            location: order.Country.code,
            userSignupDate: sub.User.isActive ? moment(sub.User.createdAt).format(dateFormat) : '',
            email: sub.email || '',
            customerName,
            phone: (sub.SellerUserId ? order.phone : sub.DeliveryAddress.contactNumber) || '',
            packageSentDate,
            paidDate,
            subsStatus,
            orderDate: moment(order.createdAt).format(dateFormat),
            canceledDate,
            shavePlanSku,
            deliveryAddress: `${sub.DeliveryAddress.address}, ${sub.DeliveryAddress.city} ${sub.DeliveryAddress.portalCode}, ${sub.DeliveryAddress.state}`,
            source: sub.SellerUserId ? 'BA' : 'ECommerce',
            gaSource: order.source || '',
            gaMedium: order.medium || '',
            promoCode: order.promoCode || ''
          };
          data.push(tmp);
        } // end if sub has orders
      });

      // create CSV and response file
      let fields = ['location', 'userSignupDate', 'email', 'customerName', 'phone', 'packageSentDate', 'paidDate', 'subsStatus', 'orderDate', 'canceledDate', 'shavePlanSku'];
      fields.push('deliveryAddress', 'source', 'gaSource', 'gaMedium', 'promoCode');
      
      let fieldNames = ['Location', 'User signup Date', 'Email', 'Customer Name', 'Phone', 'Package Sent Date', 'Paid Date', 'User Status', 'Order Date', 'Canceled Date', 'Shave Plan SKU'];
      fieldNames.push('Delivery Address', 'Source', 'GA Source', 'GA Medium', 'Promo Code');

      return json2csv({
        data,
        fields,
        fieldNames
      });
    });
  }

  downloadSalesReport(options) {
    loggingHelper.log('info', 'downloadSalesReport controller start');
    loggingHelper.log('info', `param: options = ${options}`);
    
    let plans = [];
    let products = [];
    return this.db.Plan.findAll(
      {
        where: {$or: [
          {status: 'active'},
          {isTrial: true}
        ]},
        include: this._getPlanInclude()
      })
    .then(_plans => {
      plans = SequelizeHelper.convertSeque2Json(_plans);
    })
    .then(() => this.db.Product.findAll({where: {status: 'active'}, order: [['sku']]}))
    // .then(() => this.db.Product.findAll({order: [['sku']]}))
    .then(_products => {
      products = SequelizeHelper.convertSeque2Json(_products);
    })
    .then(() => this.db.SaleReport.findAll(options))
    .then(result => {
      if(!result) {
        return;
      }
      let orderTmp = [];
      let tmp = {};
      result.forEach(order => {
        // let productName = order.productName;
        // if(order.productName.slice(-2) === ', ') {
        //   productName = order.productName.slice(0, -2);
        // }
        let category = (order.category === 'sales app') ? 'BA' : ((order.category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
        let sku = order.sku;
        if(order.sku.slice(-2) === ', ') {
          sku = order.sku.slice(0, -2);
        }
        sku = sku.split(', ');

        let qty = order.qty;
        if(order.qty.slice(-2) === ', ') {
          qty = order.qty.slice(0, -2);
        }
        qty = qty.split(', ');

        tmp = {
          orderId: order.orderId ? OrderHelper.fromatOrderNumber(order) : OrderHelper.fromatBulkOrderNumber(order),
          badgeId: order.badgeId,
          agentName: order.agentName,
          channelType: order.channelType,
          eventLocationCode: order.eventLocationCode,
          moCode: order.moCode,
          customerName: order.customerName,
          email: order.email,
          deliveryAddress: order.deliveryAddress,
          deliveryContactNumber: order.deliveryContact,
          billingAddress: order.billingAddress,
          billingContactNumber: order.billingContact,
          trackingNumber: order.deliveryId,
          promoCode: order.promoCode,
          // productName,
          // sku,
          unitTotal: 0,
          productCategory: order.productCategory,
          region: order.region,
          category,
          currency: order.currency,
          createdAt: order.createdAt,
          saleDate: order.saleDate,
          paymentType: order.paymentType.toUpperCase(),
          states: order.states,
          taxInvoiceNo: order.region === 'MYS' ? order.taxInvoiceNo : '',
          totalPrice: order.totalPrice,
          subTotalPrice: order.subTotalPrice,
          discountAmount: order.discountAmount,
          taxAmount: order.taxAmount,
          shippingFee: order.shippingFee,
          grandTotalPriceBeforeTax: (order.Country.includedTaxToProduct && order.grandTotalPrice) ? (order.grandTotalPrice / (1 + (order.taxRate / 100))).toFixed(2) : order.grandTotalPrice,
          grandTotalPrice: order.grandTotalPrice,
          source: order.source,
          medium: order.medium,
          campaign: order.campaign,
          term: order.term,
          content: order.content,
          isDirectTrial: order.isDirectTrial ? 'Yes' : ((order.productCategory === 'Ala Carte' && order.category === 'sales app') ? 'Yes' : 'No')
        };

        plans.forEach(plan => {
          if(sku.includes(plan.sku) && qty[0]) {
            sku.splice(sku.indexOf(plan.sku), 1);
            qty.splice(sku.indexOf(plan.sku), 1);

            plan.planCountry.forEach(_planCountry => {
              if(_planCountry.Country.code.toUpperCase() === order.region.toUpperCase()) {
                // _planCountry.planDetails.forEach(planDetail => {
                //   sku.push(planDetail.ProductCountry.Product.sku);
                //   qty.push(JSON.parse(planDetail.deliverPlan)[1]);
                // });
                _planCountry.planTrialProducts.forEach(planTrialProduct => {
                  sku.push(planTrialProduct.ProductCountry.Product.sku);
                  qty.push(planTrialProduct.qty);
                });
              }
            });
          }
        });

        let unitTotal = 0;
        products.forEach(product => {
          if(sku.includes(product.sku) && qty[0]) {
            tmp[product.sku] = qty[sku.indexOf(product.sku)];
            unitTotal += +qty[sku.indexOf(product.sku)];
          } else {
            tmp[product.sku] = 0;
          }
        });

        tmp.unitTotal = unitTotal;

        orderTmp.push(tmp);
      });

      // create CSV and response file
      let fields;
      let fieldNames;
      fields = ['orderId', 'taxInvoiceNo', 'states', 'saleDate', 'region', 'category', 'badgeId', 'agentName', 'channelType', 'eventLocationCode', 'isDirectTrial', 'moCode', 'customerName', 'email', 'deliveryAddress', 'deliveryContactNumber', 'billingAddress', 'billingContactNumber', 'trackingNumber', 'productCategory'];
      // plans.forEach(plan => {
      //   fields.push(plan.sku);
      // });

      products.forEach(product => {
        fields.push(product.sku);
      });
      fields.push('unitTotal', 'paymentType', 'currency', 'promoCode', 'totalPrice', 'discountAmount', 'subTotalPrice', 'taxAmount', 'shippingFee', 'grandTotalPriceBeforeTax', 'grandTotalPrice', 'source', 'medium', 'campaign', 'term', 'content');
      
      fieldNames = ['Order', 'Tax Invoice number', 'Order Status', 'Sale Date', 'Region', 'Category', 'Badge Id', 'Agent Name', 'Channel Type', 'Event/Location Code', 'Direct Delivery', 'MO Code', 'Customer Name', 'Email', 'Delivery Address', 'Delivery Contact Number', 'Billing Address', 'Billing Contact Number', 'Tracking Number', 'Product Category'];
      // plans.forEach(plan => {
      //   fieldNames.push(plan.sku);
      // });

      products.forEach(product => {
        fieldNames.push(product.sku);
      });
      fieldNames.push('Unit Total', 'Payment Type', 'Currency', 'Promo Code', 'Total Price', 'Discount Amount', 'Sub Total Price', 'Tax Amount', 'Shipping Fee', 'Grand Total(Before tax)', 'Grand Total(Incl. tax)', 'Source', 'Medium', 'Campaign', 'Term', 'Content');

      return json2csv({
        data: orderTmp,
        fields,
        fieldNames
      });
    });
  }

  downloadSalesReportForAppco(options) {
    loggingHelper.log('info', 'downloadSalesReportForAppco controller start');
    loggingHelper.log('info', `param: options = ${options}`);

    let plans = [];
    let products = [];
    return this.db.Plan.findAll(
      {
        where: {$or: [
          {status: 'active'},
          {isTrial: true}
        ]},
        include: this._getPlanInclude()
      })
    .then(_plans => {
      plans = SequelizeHelper.convertSeque2Json(_plans);
    })
    .then(() => this.db.Product.findAll({where: {status: 'active'}, order: [['sku']]}))
    .then(_products => {
      products = SequelizeHelper.convertSeque2Json(_products);
    })
    .then(() => this.db.SaleReport.findAll(options))
    .then(result => {
      if(!result) {
        return;
      }
      let orderTmp = [];
      let tmp = {};
      result.forEach(order => {
        let sku = order.sku;
        if(order.sku.slice(-2) === ', ') {
          sku = order.sku.slice(0, -2);
        }
        sku = sku.split(', ');

        let qty = order.qty;
        if(order.qty.slice(-2) === ', ') {
          qty = order.qty.slice(0, -2);
        }
        qty = qty.split(', ');

        tmp = {
          badgeId: order.badgeId,
          agentName: order.agentName,
          customerName: order.customerName,
          orderId: order.orderId ? OrderHelper.fromatOrderNumber(order) : OrderHelper.fromatBulkOrderNumber(order),
          taxInvoiceNo: order.region === 'MYS' ? order.taxInvoiceNo : '',
          totalPrice: order.totalPrice,
          subTotalPrice: order.subTotalPrice,
          discountAmount: order.discountAmount,
          taxAmount: order.taxAmount,
          shippingFee: order.shippingFee,
          grandTotalPriceBeforeTax: (order.Country.includedTaxToProduct && order.grandTotalPrice) ? (order.grandTotalPrice / (1 + (order.taxRate / 100))).toFixed(2) : order.grandTotalPrice,
          grandTotalPrice: order.grandTotalPrice,
          unitTotal: 0,
          paymentType: order.paymentType.toUpperCase(),
          states: order.states,
          channelType: order.channelType,
          eventLocationCode: order.eventLocationCode,
          productCategory: order.productCategory,
          moCode: order.moCode,
          saleDate: order.saleDate,
          email: order.email,
          isDirectTrial: order.isDirectTrial ? 'Yes' : ((order.productCategory === 'Ala Carte' && order.category === 'sales app') ? 'Yes' : 'No')
        };

        plans.forEach(plan => {
          if(sku.includes(plan.sku) && qty[0]) {
            sku.splice(sku.indexOf(plan.sku), 1);
            qty.splice(sku.indexOf(plan.sku), 1);

            plan.planCountry.forEach(_planCountry => {
              if(_planCountry.Country.code.toUpperCase() === order.region.toUpperCase()) {
                // _planCountry.planDetails.forEach(planDetail => {
                //   sku.push(planDetail.ProductCountry.Product.sku);
                //   qty.push(JSON.parse(planDetail.deliverPlan)[1]);
                // });
                _planCountry.planTrialProducts.forEach(planTrialProduct => {
                  sku.push(planTrialProduct.ProductCountry.Product.sku);
                  qty.push(planTrialProduct.qty);
                });
              }
            });
          }
        });

        let unitTotal = 0;
        products.forEach(product => {
          if(sku.includes(product.sku) && qty[0]) {
            tmp[product.sku] = qty[sku.indexOf(product.sku)];
            unitTotal += +qty[sku.indexOf(product.sku)];
          } else {
            tmp[product.sku] = 0;
          }
        });

        tmp.unitTotal = unitTotal;

        orderTmp.push(tmp);
      });

      // create CSV and response file
      let fields;
      let fieldNames;
      fields = ['orderId', 'customerName', 'states', 'taxInvoiceNo', 'saleDate', 'badgeId', 'moCode', 'agentName', 'channelType', 'eventLocationCode', 'isDirectTrial', 'email', 'productCategory'];
      // plans.forEach(plan => {
      //   fields.push(plan.sku);
      // });

      products.forEach(product => {
        fields.push(product.sku);
      });
      fields.push('unitTotal', 'paymentType', 'totalPrice', 'discountAmount', 'subTotalPrice', 'taxAmount', 'shippingFee', 'grandTotalPriceBeforeTax', 'grandTotalPrice');
      
      fieldNames = ['Order', 'Customer Name', 'Order Status', 'Tax Invoice number', 'Sale Date', 'Badge Id', 'MO Code', 'Agent Name', 'Channel Type', 'Event/Location Code', 'Direct Delivery', 'Email', 'Product Category'];
      // plans.forEach(plan => {
      //   fieldNames.push(plan.sku);
      // });

      products.forEach(product => {
        fieldNames.push(product.sku);
      });
      fieldNames.push('Checkout size', 'Payment Type', 'Total Price', 'Discount Amount', 'Sub Total Price', 'Tax Amount', 'Shipping Fee', 'Grand Total(Before tax)', 'Grand Total(Incl. tax)');

      return json2csv({
        data: orderTmp,
        fields,
        fieldNames
      });
    });
  }

  downloadSalesReportForMO(options) {
    loggingHelper.log('info', 'downloadSalesReportForMO controller start');
    loggingHelper.log('info', `param: options = ${options}`);

    let plans = [];
    let products = [];
    return this.db.Plan.findAll(
      {
        where: {$or: [
          {status: 'active'},
          {isTrial: true}
        ]},
        include: this._getPlanInclude()
      })
    .then(_plans => {
      plans = SequelizeHelper.convertSeque2Json(_plans);
    })
    .then(() => this.db.Product.findAll({where: {status: 'active'}, order: [['sku']]}))
    .then(_products => {
      products = SequelizeHelper.convertSeque2Json(_products);
    })
    .then(() => this.db.SaleReport.findAll(options))
    .then(result => {
      if(!result) {
        return;
      }
      let orderTmp = [];
      let tmp = {};
      result.forEach(order => {
        let sku = order.sku;
        if(order.sku.slice(-2) === ', ') {
          sku = order.sku.slice(0, -2);
        }
        sku = sku.split(', ');

        let qty = order.qty;
        if(order.qty.slice(-2) === ', ') {
          qty = order.qty.slice(0, -2);
        }
        qty = qty.split(', ');

        tmp = {
          badgeId: order.badgeId,
          agentName: order.agentName,
          customerName: order.customerName,
          orderId: order.orderId ? OrderHelper.fromatOrderNumber(order) : OrderHelper.fromatBulkOrderNumber(order),
          taxInvoiceNo: order.region === 'MYS' ? order.taxInvoiceNo : '',
          totalPrice: order.totalPrice,
          subTotalPrice: order.subTotalPrice,
          discountAmount: order.discountAmount,
          taxAmount: order.taxAmount,
          shippingFee: order.shippingFee,
          grandTotalPriceBeforeTax: (order.Country.includedTaxToProduct && order.grandTotalPrice) ? (order.grandTotalPrice / (1 + (order.taxRate / 100))).toFixed(2) : order.grandTotalPrice,
          grandTotalPrice: order.grandTotalPrice,
          unitTotal: 0,
          paymentType: order.paymentType.toUpperCase(),
          states: order.states,
          channelType: order.channelType,
          eventLocationCode: order.eventLocationCode,
          productCategory: order.productCategory,
          moCode: order.moCode,
          saleDate: order.saleDate,
          email: order.email,
          isDirectTrial: order.isDirectTrial ? 'Yes' : ((order.productCategory === 'Ala Carte' && order.category === 'sales app') ? 'Yes' : 'No')
        };

        plans.forEach(plan => {
          if(sku.includes(plan.sku) && qty[0]) {
            sku.splice(sku.indexOf(plan.sku), 1);
            qty.splice(sku.indexOf(plan.sku), 1);

            plan.planCountry.forEach(_planCountry => {
              if(_planCountry.Country.code.toUpperCase() === order.region.toUpperCase()) {
                // _planCountry.planDetails.forEach(planDetail => {
                //   sku.push(planDetail.ProductCountry.Product.sku);
                //   qty.push(JSON.parse(planDetail.deliverPlan)[1]);
                // });
                _planCountry.planTrialProducts.forEach(planTrialProduct => {
                  sku.push(planTrialProduct.ProductCountry.Product.sku);
                  qty.push(planTrialProduct.qty);
                });
              }
            });
          }
        });

        let unitTotal = 0;
        products.forEach(product => {
          if(sku.includes(product.sku) && qty[0]) {
            tmp[product.sku] = qty[sku.indexOf(product.sku)];
            unitTotal += +qty[sku.indexOf(product.sku)];
          } else {
            tmp[product.sku] = 0;
          }
        });

        tmp.unitTotal = unitTotal;

        orderTmp.push(tmp);
      });

      // create CSV and response file
      let fields;
      let fieldNames;
      fields = ['orderId', 'customerName', 'states', 'taxInvoiceNo', 'saleDate', 'badgeId', 'moCode', 'agentName', 'channelType', 'eventLocationCode', 'isDirectTrial', 'email', 'productCategory'];
      // plans.forEach(plan => {
      //   fields.push(plan.sku);
      // });

      products.forEach(product => {
        fields.push(product.sku);
      });
      fields.push('unitTotal', 'paymentType', 'totalPrice', 'discountAmount', 'subTotalPrice', 'taxAmount', 'shippingFee', 'grandTotalPriceBeforeTax', 'grandTotalPrice');

      fieldNames = ['Order', 'Customer Name', 'Order Status', 'Tax Invoice number', 'Sale Date', 'Badge Id', 'MO Code', 'Agent Name', 'Channel Type', 'Event/Location Code', 'Direct Delivery', 'Email', 'Product Category'];
      // plans.forEach(plan => {
      //   fieldNames.push(plan.sku);
      // });

      products.forEach(product => {
        fieldNames.push(product.sku);
      });
      fieldNames.push('Checkout size', 'Payment Type', 'Total Price', 'Discount Amount', 'Sub Total Price', 'Tax Amount', 'Shipping Fee', 'Grand Total(Before tax)', 'Grand Total(Incl. tax)');

      return json2csv({
        data: orderTmp,
        fields,
        fieldNames
      });
    });
  }

  downloadUsersSaleReportForCronjob(options) {
    return this.db.Order.findAll(options)
    .then(orders => {
      if(!orders || orders.length === 0) return Promise.resolve('');

      orders = SequelizeHelper.convertSeque2Json(orders);

      let csvRows = [];

      orders.forEach(order => {
        let row = {
          orderNo: OrderHelper.fromatOrderNumber(order, true),
          id: order.id,
          states: order.states,
          sku: '',
          type: '',
          source: `${order.source}/${order.medium || 'none'}`,
          email: order.email,
          qty: '',
          price: '',
          total: '',
          currency: '',
          paymentType: order.paymentType,
          region: order.Country.currencyCode,
          category: order.SellerUserId ? 'BA App' : 'E-commerce',
          saleDate: moment(order.createdAt).format('MMM DD, YYYY') // Jul 02, 2018
        };

        if(order.orderDetail && order.orderDetail.length > 0) {
          order.orderDetail.forEach(detail => {
            let rowTmp = Object.assign({}, row);
            
            rowTmp.sku = detail.PlanCountry ? detail.PlanCountry.Plan.sku : (detail.ProductCountry ? detail.ProductCountry.Product.sku : '');
            rowTmp.type = detail.PlanCountry && detail.PlanCountry.Plan.isTrial ? 'Trial' : (detail.ProductCountry ? 'A La Carte' : 'Shave Plan');
            rowTmp.qty = detail.qty;
            rowTmp.price = detail.price;
            rowTmp.total = (parseInt(detail.qty) * parseFloat(detail.price)).toFixed(2);
            rowTmp.currency = detail.currency;

            csvRows.push(rowTmp);
          });
        }
      });

      // create CSV and response file
      let fields = ['orderNo', 'id', 'states', 'sku', 'type', 'source', 'email', 'qty', 'price', 'total', 'currency', 'paymentType', 'region', 'category', 'saleDate'];
      
      let fieldNames = ['Order No', 'ID', 'State', 'SKU', 'Type', 'Source', 'Email', 'Units', 'Unit Price', 'Total', 'Currency', 'Payment Type', 'Region', 'Category', 'Sale Date'];

      return json2csv({
        data: csvRows,
        fields,
        fieldNames
      });
    });
  }

  downloadSubscribersSaleReportForCronjob(options) {
    return this.db.Subscription.findAll(options)
    .then(items => {
      if(items.length === 0) return items;

      items = SequelizeHelper.convertSeque2Json(items);

      return Promise.mapSeries(items, item => {
        let subsOrdersPromise = this.db.Order.findAll({
          where: { subscriptionIds: `[${item.id}]` } 
        });

        let countOrdersPromise = this.db.Order.count({
          where: {
            $and: [
              { createdAt: { $gt: '2017-11-14' } },
              { UserId: item.UserId }
            ]
          }
        });

        return Promise.all([subsOrdersPromise, countOrdersPromise]).then(result => {
          item.orders = result[0];
          item.aLaCarteUser = parseInt(result[1]) > 0 ? 'True' : 'False';

          return item;
        });
      });
    })
    .then(items => {
      if(!items || items.length === 0) return Promise.resolve('');

      let csvRows = [];
      let dateFormat = 'YYYY-MM-DD HH:mm:ss';

      items.forEach(item => {
        if(item.status == 'Canceled') {
          if(moment(item.createdAt).add(5, 'seconds').format(dateFormat) >= moment(item.updatedAt).format(dateFormat)) {
            return;
          }
        }

        let userStatus = '';
        let customerName = item.fullname;
        let phone = item.phone;
        let source = 'BA';
        let canceledDate = '';
        let location = '';
        let shavePlanSku = '';
        let pkg = '';
        let paid = '';
        let badgeId = '';
        let referringChannel = '';

        if(!item.SellerUserId) {
          customerName = `${item.DeliveryAddress.firstName} ${item.DeliveryAddress.lastName}`;
          phone = item.DeliveryAddress.contactNumber;
          source = 'Ecommerce';
        } else {
          badgeId = item.SellerUser.badgeId;
        }

        if(item.PlanCountryId) {
          location = item.PlanCountry.Country.code;
          shavePlanSku = item.PlanCountry.Plan.sku;
        } else if(item.CustomPlanId) {
          location = item.CustomPlan.Country.code;
          shavePlanSku = item.CustomPlan.description;
        }

        if(item.status != 'Canceled') {
          if(item.isCustom) {
            userStatus = 'Active Customer (Without Free Trial)';
          } else if(item.isTrial) {
            if(item.currentDeliverNumber == 0) {
              userStatus = 'Currently Trial Period';
            } else if(item.currentDeliverNumber > 0) {
              userStatus = 'Active Customer (From Free Trial)';
            }
          }
        } else {
          canceledDate = moment(item.updatedAt).format(dateFormat);

          if(item.isCustom) {
            userStatus = 'Canceled (Without Free Trial)';
          } else if(item.isTrial) {
            if(item.currentDeliverNumber == 0) {
              userStatus = 'Canceled (During Free Trial)';
            } else if(item.currentDeliverNumber > 0) {
              userStatus = 'Canceled (After Free Trial)';
            }
          }
        }

        if(item.orders && item.orders.length > 0) {
          item.orders.forEach((order, index) => {
            if(index === 0) {
              referringChannel = `${order.source}/${order.medium}`;
            }

            pkg = pkg ? `${pkg}, ` : '';
            pkg += moment(order.updatedAt).format(dateFormat);

            if(item.isCustom) {
              paid = paid ? `${paid}, ` : '';
              paid += moment(order.createdAt).format(dateFormat);
            } else if(item.isTrial) {
              let orderCreatedDate = moment(order.createdAt).format(dateFormat);
              let subsCreatedDate1Hours = moment(item.createdAt).add(1, 'hours').format(dateFormat);

              if(orderCreatedDate > subsCreatedDate1Hours) {
                // subs must be created before order 1 hours or over
                paid = paid ? `${paid}, ` : '';
                paid += moment(order.createdAt).format(dateFormat);
              }
            }
          });
        }

        let row = {
          location,
          userSignupDate: item.User.isActive ? moment(item.User.createdAt).format(dateFormat) : '',
          email: item.email,
          customerName,
          phone,
          package: pkg,
          paid,
          userStatus,
          orderDate: moment(item.createdAt).format(dateFormat),
          canceledDate,
          shavePlanSku,
          address: item.DeliveryAddress.address,
          modeCode: item.MarketingOfficeId ? item.MarketingOffice.moCode : '',
          badgeId,
          source,
          referringChannel,
          aLaCarteUser: item.aLaCarteUser
        };

        csvRows.push(row);
      });

      // create CSV and response file
      let fields = ['location', 'userSignupDate', 'email', 'customerName', 'phone', 'package', 'paid', 'userStatus', 'orderDate', 'canceledDate', 'shavePlanSku', 'address'];
      fields.push('modeCode', 'badgeId', 'source', 'referringChannel', 'aLaCarteUser');

      let fieldNames = ['Location', 'User signup Date', 'Email', 'Customer Name', 'Phone', 'Package', 'Paid', 'User Status', 'Order Date', 'Canceled Date', 'Shave Plan SKU', 'Location'];
      fieldNames.push('MO Code', 'Badge ID', 'Source', 'Referring Channel (GA)', 'A La Carte User?');

      return json2csv({
        data: csvRows,
        fields,
        fieldNames
      });
    });
  }

  downloadStudentPromoCodeReportForCronjob(options) {
    return this.db.Subscription.findAll(options)
    .then(items => {
      if(items.length === 0) return items;

      items = SequelizeHelper.convertSeque2Json(items);

      return Promise.mapSeries(items, item => {
        return this.db.Order.count({
          where: { subscriptionIds: `[${item.id}]` } 
        })
        .then(count => {
          item.hasPaid = parseInt(count) > 1 ? 'Yes' : 'No';

          return item;
        });
      });
    })
    .then(items => {
      if(!items || items.length === 0) return Promise.resolve('');

      let csvRows = [];
      let dateFormat = 'YYYY-MM-DD HH:mm:ss';

      items.forEach(item => {
        let row = {
          email: item.email,
          customerName: `${item.DeliveryAddress.firstName} ${item.DeliveryAddress.lastName}`,
          discountPercent: item.discountPercent,
          price: item.pricePerCharge,
          promoCode: item.promoCode,
          subscribeDate: moment(item.createdAt).format(dateFormat),
          sku: item.PlanCountry ? item.PlanCountry.Plan.sku : '',
          status: item.status,
          nextChargeDate: item.nextChargeDate ? moment(item.nextChargeDate).format('YYYY-MM-DD') : '',
          hasPaid: item.hasPaid
        };

        csvRows.push(row);
      });

      // create CSV and response file
      let fieldCodes = ['email', 'customerName', 'discountPercent', 'price', 'promoCode', 'subscribeDate', 'sku', 'status', 'nextChargeDate', 'hasPaid'];

      let fieldNames = ['Email', 'Customer Name', 'Discount Percent', 'Price', 'Promo Code', 'Subscribe Date', 'Shave Plan SKU', 'Status', 'Next Charge Date', 'Has Paid'];

      return json2csv({
        data: csvRows,
        fieldCodes,
        fieldNames
      });
    });
  }
}

export default DownloadController;
