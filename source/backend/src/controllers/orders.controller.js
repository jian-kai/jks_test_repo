import Promise from 'bluebird';
import jwt from 'jsonwebtoken';
import _ from 'underscore';
import moment from 'moment';
import config from '../config';
import { stripeHelper } from '../helpers/stripe-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import OrderHelper from '../helpers/order-helper';
import SendEmailQueueService from '../services/send-email.queue.service';
import TaxInvoiceQueueService from '../services/invoice-number.service';
import { loggingHelper } from '../helpers/logging-helper';
import MailChimp from '../helpers/mailchimp';

const CART_DISCOUNT_PERCENT = 10;

class OrderController {

  constructor(db) {
    this.db = db;
    this.mailchimp = MailChimp.getInstance();
  }

  getIdentityForCheckOut(req, data) {
    loggingHelper.log('info', 'getIdentityForCheckOut start');
    loggingHelper.log('info', `param: req.headers = ${req.headers}`);
    return new Promise((resolved, reject) => {
      if(req.isRenew) {
        resolved({
          id: data.params.userId,
          email: data.params.email
        });
      } else {
        if(!req.headers['x-access-token'] && !data.params.email) {
          reject(new Error('missing_param'));
        } else if(!req.headers['x-access-token'] && data.params.email) {
          resolved({
            email: data.params.email,
            guest: true
          });
        } else {
          let token = req.headers['x-access-token'];
          jwt.verify(token, config.accessToken.secret, function(err, decoded) {
            if(err) {
              reject(new Error('token_expired'));
            } else {
              resolved(decoded);
            }
          });
        }
      }
    });
  }

  // proccess validate promotion code and update it if it's valided
  proccessPromotion(data) {
    loggingHelper.log('info', 'proccessPromotion start');
    loggingHelper.log('info', `param: data = ${data}`);
    return this.db.Promotion.findOne({where: {
      expiredAt: {
        $gte: moment().format('YYYY-MM-DD')
      },
      CountryId: data.country.id
    }, include: [
      {
        model: this.db.PromotionCode,
        as: 'promotionCodes',
        where: {
          code: data.params.promotionCode,
          isValid: true
        }
      }
    ]})
      .then(promotion => {
        if(!promotion) {
          return {
            id: undefined,
            discount: 0
          };
        } else {
          // check production ID
          let intersectionProduct;
          let intersectionPlan;
          let returnObj = promotion;
          // convert integer array to string array
          data.params.productCountryIds = data.params.productCountryIds.map(String);
          data.params.planCountryIds = data.params.planCountryIds.map(String);
          if(promotion.productCountryIds && promotion.productCountryIds !== 'all') {
            intersectionProduct = _.intersection(JSON.parse(promotion.productCountryIds), data.params.productCountryIds);
            returnObj.productCountryIds = intersectionProduct;
          }
          if(promotion.productCountryIds && promotion.planCountryIds !== 'all') {
            intersectionPlan = _.intersection(JSON.parse(promotion.planCountryIds), data.params.planCountryIds);
            returnObj.planCountryIds = intersectionPlan;
          }
          if(promotion.freeProductCountryIds) {
            returnObj.freeProductCountryIds = JSON.parse(promotion.freeProductCountryIds);
          }
          
          if(promotion.isGeneric && data.params.userTypeId) {
            let appliedTo = JSON.parse(promotion.appliedTo);
            if(data.params.userTypeId === 1 && !data.identity.guest) {
              if(appliedTo && appliedTo[data.identity.id] && appliedTo[data.identity.id] >= promotion.timePerUser) {
                throw new Error('promo_used_limited');
                // return {
                //   id: undefined,
                //   discount: 0
                // };
              }
            } else {
              if(appliedTo && appliedTo[data.UserId] && appliedTo[data.UserId] >= promotion.timePerUser) {
                throw new Error('promo_used_limited');
                // throw json({ ok: false, message: config.messages.promotion.usedLimited });
                // return {
                //   id: undefined,
                //   discount: 0
                // };
              }
            }
          }
          return returnObj;
        }
      })
      .then(promotion => {
        // promotion = SequelizeHelper.convertSeque2Json(promotion);
        if(promotion.freeProductCountryIds) {
          return this.db.ProductCountry.findAll(
            {
              where: {id: {$in: promotion.freeProductCountryIds}},
              include: [this.db.Country]
            }
          )
          .then(productCountrys => {
            promotion.freeProductCountries = productCountrys;
            return promotion;
          });
        } else {
          return promotion;
        }
      });
  }

  makeOrder(data, t) {
    loggingHelper.log('info', 'makeOrder start');
    loggingHelper.log('info', `param: data = ${data}`);
    // get all product country
    let orderObject = {};
    let receiptObject = {
      totalPrice: 0
    };
    let updatedProducts = [];
    let productCountry;
    let planCountry;
    let discountAmount = 0;
    let appliedDiscount = 0;
    let cartDiscountAmount = 0;
    let total = 0;
    let currentChargeTotal = 0;
    let subTotal = 0;
    let country;
    let hasUsedDecPromo = 0;
    let hasUsedTbcPromo = 0;

    // calculate card rule
    let checkoutRules = config.checkoutRules[data.country.code] || [];
    let availableRules = [];
    checkoutRules.forEach(checkoutRule => {
      if((data.params.userTypeId !== 2 && checkoutRule.isActiveWeb) || (data.params.userTypeId === 2 && checkoutRule.isActiveMobile)) {
        availableRules.push(checkoutRule);
      }
    });

    console.log(`availableRules ====== ${JSON.stringify(availableRules)}`);

    // calculate price for subscription items
    data.params.subsItems.forEach(subscription => {
      let pricePerTimes;
      // set price for order detail
      planCountry = _.findWhere(data.planCountries, {id: subscription.PlanCountryId});
      if(!planCountry) {
        throw new Error('not_found');
      }

      // calculate subTotal price
      total += planCountry.sellPrice * parseInt(subscription.qty);
      pricePerTimes = planCountry.sellPrice / +planCountry.Plan.PlanType.totalChargeTimes;
      currentChargeTotal += pricePerTimes * parseInt(subscription.qty);
      subscription.price = pricePerTimes;

      let subscriptionRule = _.findWhere(availableRules, {type: 'subscriptionRule'});
      if(subscriptionRule && !data.isSubscriber && (cartDiscountAmount > subscription.price * (subscriptionRule.discount / 100) || cartDiscountAmount === 0) && !subscription.isTrial) {
        cartDiscountAmount = subscription.price * (subscriptionRule.discount / 100);
      }

      // calculate discount price
      if(data.promotion.id && (data.promotion.planCountryIds === 'all' || (data.promotion.planCountryIds && data.promotion.planCountryIds.indexOf(`${planCountry.id}`) > -1))) {
        discountAmount += subscription.price * (data.promotion.discount / 100) * parseInt(subscription.qty);
        appliedDiscount += subscription.price * parseInt(subscription.qty);
      }

      // calculate delivery date
      let startDeliverDate = new Date(moment().format('YYYY-MM-DD'));
      if(!subscription.deliveryNow && data.params.userTypeId === 2) {
        startDeliverDate = new Date(moment().add(3, 'months').format('YYYY-MM-DD'));
      }
      subscription.startDeliverDate = startDeliverDate;

      // calculate total price
      subscription.currency = planCountry.Country.currencyDisplay;
      receiptObject.currency = planCountry.Country.currencyDisplay;
      data.chargeCurrency = planCountry.Country.currencyCode;
      country = planCountry.Country;
    });

    // calculate price for ala-carte items
    data.params.alaItems.forEach(alaItem => {
      // set price for order detail
      productCountry = _.findWhere(data.productCountries, {id: alaItem.ProductCountryId});
      // let itemInQueue = _.findWhere(data.itemInQueue, {id: alaItem.ProductCountryId});
      if(!productCountry) {
        throw new Error('not_found');
      }
      // itemInQueue = (itemInQueue) ? itemInQueue : {qty: 0};
      // if(productCountry.qty < alaItem.qty + itemInQueue.qty && data.params.userTypeId !== 2) {
      //   throw new Error('not_enough');
      // }
      updatedProducts.push({
        id: productCountry.id,
        qty: alaItem.qty
      });

      // calculate subTotal price
      total += productCountry.sellPrice * parseInt(alaItem.qty);
      currentChargeTotal += productCountry.sellPrice * parseInt(alaItem.qty);
      alaItem.price = productCountry.sellPrice;

      // calculate cart price
      if(availableRules.length > 0) {
        let specificProductSkus = _.pluck(_.where(availableRules, {type: 'specificProduct'}), 'productSKU');
        let cartRules = _.findWhere(availableRules, {type: 'cartRule'});
        let decemberPromo = _.findWhere(availableRules, {type: 'decemberPromo'});
        let tbcPromo = _.findWhere(availableRules, {type: 'tbcPromo'});
        if(data.isSubscriber && specificProductSkus.indexOf(productCountry.Product.sku) > 0) {
          cartDiscountAmount += alaItem.price * (50 / 100) * parseInt(alaItem.qty);
        } else if(data.params.subsItems.length > 0 && cartRules) {
          cartDiscountAmount += alaItem.price * (CART_DISCOUNT_PERCENT / 100) * parseInt(alaItem.qty);
        }

        // calculate free item
        console.log(`decemberDiscountProduct ${JSON.stringify(decemberPromo)} == ${JSON.parse(productCountry.id)} === ${JSON.stringify(data.params.alaItems)}`);
        loggingHelper.log('info', `decemberDiscountProduct ${JSON.stringify(decemberPromo)} == ${JSON.parse(productCountry.id)} === ${JSON.stringify(data.params.alaItems)}`);
        if(decemberPromo && hasUsedDecPromo !== JSON.parse(decemberPromo.freeProductIds).length && JSON.parse(decemberPromo.freeProductIds).find(id => id === productCountry.id)) {
          let decemberDiscountProduct = _.intersection(JSON.parse(decemberPromo.productIds), _.pluck(data.params.alaItems, 'ProductCountryId'));
          if(decemberDiscountProduct.length > 0 && alaItem.qty === 1) {
            total = total - alaItem.price;
            currentChargeTotal = currentChargeTotal - alaItem.price;
            alaItem.price = 0;
            hasUsedDecPromo += 1;
          }
        }
        if(tbcPromo && hasUsedTbcPromo !== JSON.parse(tbcPromo.freeProductIds).length && JSON.parse(tbcPromo.freeProductIds).find(id => id === productCountry.id)) {
          let decemberDiscountProduct = _.intersection(JSON.parse(tbcPromo.productIds), _.pluck(data.params.alaItems, 'ProductCountryId'));
          if(decemberDiscountProduct.length > 0 && alaItem.qty === 1) {
            total = total - alaItem.price;
            currentChargeTotal = currentChargeTotal - alaItem.price;
            alaItem.price = 0;
            hasUsedTbcPromo += 1;
          }
        }
      }
      
      // calculate promotion discount
      if(data.promotion.id && (data.promotion.productCountryIds === 'all' || (data.promotion.productCountryIds && data.promotion.productCountryIds.indexOf(`${productCountry.id}`) > -1))) {
        discountAmount += alaItem.price * (data.promotion.discount / 100) * parseInt(alaItem.qty);
        appliedDiscount += alaItem.price * parseInt(alaItem.qty);
      }

      // calculate total price
      alaItem.currency = productCountry.Country.currencyDisplay;
      receiptObject.currency = productCountry.Country.currencyDisplay;
      data.chargeCurrency = productCountry.Country.currencyCode;
      country = productCountry.Country;
    });

    if(data.params.subsItems.length > 0) {
      let subscriptionIds = _.pluck(data.subscriptions, 'id');
      orderObject.subscriptionIds = [];
      subscriptionIds.forEach(id => orderObject.subscriptionIds.push(id.toString()));
      orderObject.subscriptionIds = JSON.stringify(orderObject.subscriptionIds);
    }

    // process discount
    console.log(`discountAmount === ${discountAmount} ==== ${cartDiscountAmount}`);
    loggingHelper.log('info', `discountAmount === ${discountAmount} ==== ${cartDiscountAmount}`);
    if(discountAmount > 0 || data.promotion.freeProductCountryIds) {
      orderObject.PromotionId = data.promotion.id;
      orderObject.promoCode = data.params.promotionCode;
    }

    if(cartDiscountAmount > 0) {
      // perform cart discount
      discountAmount = cartDiscountAmount;
    } else if((data.promotion && data.promotion.minSpend && (data.promotion.minSpend > appliedDiscount) && !data.promotion.allowMinSpendForTotalAmount)
            || (data.promotion && data.promotion.minSpend && (data.promotion.minSpend > currentChargeTotal) && data.promotion.allowMinSpendForTotalAmount)) {
      discountAmount = 0;
    } else if(data.promotion && data.promotion.maxDiscount && (discountAmount > data.promotion.maxDiscount)) {
      discountAmount = data.promotion.maxDiscount;
    }

    // calculate subtotal charge
    receiptObject.originPrice = currentChargeTotal;
    subTotal = currentChargeTotal - discountAmount;
    receiptObject.totalPrice = subTotal;

    // calculating tax
    if(data.productCountries && data.productCountries.length > 0 && !country.includedTaxToProduct) {
      // let taxAmount = receiptObject.totalPrice * (data.productCountries[0].Country.taxRate / 100);
      // receiptObject.taxAmount = taxAmount;
      // receiptObject.totalPrice = receiptObject.totalPrice + taxAmount;
      // total = total + taxAmount;
    }

    // calculate shipping fee
    // let shippingFeeConfig = productCountry ? config.shippingFee[productCountry.Country.code.toUpperCase()] : config.shippingFee[planCountry.Country.code.toUpperCase()];
    let shippingFeeConfig = {};
    if(productCountry) {
      shippingFeeConfig = {
        minAmount: productCountry.Country.shippingMinAmount,
        fee: productCountry.Country.shippingFee,
        trialFee: productCountry.Country.shippingTrialFee
      };
    } else {
      shippingFeeConfig = {
        minAmount: planCountry.Country.shippingMinAmount,
        fee: planCountry.Country.shippingFee,
        trialFee: planCountry.Country.shippingTrialFee
      };
    }

    if(data.promotion && data.promotion.isFreeShipping) {
      data.hasUsedPromoCode = true;
      receiptObject.shippingFee = 0;
      orderObject.PromotionId = data.promotion.id;
      orderObject.promoCode = data.params.promotionCode;
    } else if(data.params.userTypeId === 2) {
      receiptObject.shippingFee = 0;
    } else if(shippingFeeConfig && total < shippingFeeConfig.minAmount) {
      receiptObject.shippingFee = shippingFeeConfig.fee;
      receiptObject.totalPrice = receiptObject.totalPrice + parseFloat(shippingFeeConfig.fee);
    }

    orderObject.email = data.params.email ? data.params.email : data.identity.email;
    orderObject.phone = data.params.phone;
    orderObject.fullName = data.params.fullName;
    orderObject.DeliveryAddressId = data.params.DeliveryAddressId;
    orderObject.BillingAddressId = data.params.BillingAddressId;
    orderObject.orderDetail = data.params.alaItems.concat(data.params.subsItems);

    if(data.promotion.freeProductCountryIds) {
      let orderDetails = [];
      orderDetails.push(orderObject.orderDetail);
      data.promotion.freeProductCountries.forEach(freeProductCountry => {
        orderDetails.push({
          qty: 1,
          price: freeProductCountry.sellPrice,
          currency: freeProductCountry.Country.currencyDisplay,
          ProductCountryId: freeProductCountry.id,
        });
      });
      orderObject.orderDetail = orderDetails;
    }

    orderObject.taxRate = country.taxRate;
    orderObject.taxName = country.taxName;
    orderObject.CountryId = country.id;

    // build GA data
    orderObject.source = data.params.utmSource ? data.params.utmSource.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : data.params.utmSource;
    orderObject.medium = data.params.utmMedium;
    orderObject.campaign = data.params.utmCampaign;
    orderObject.term = data.params.utmTerm;
    orderObject.content = data.params.utmContent;

    // process set value for order state and payment type
    if(data.params.paymentType === 'stripe') {
      // if(data.params.userTypeId === 2 && data.params.subsItems.length === 0) {
      //   orderObject.states = 'Completed';
      // } else {
      orderObject.states = 'On Hold';
      // }
      orderObject.paymentType = 'stripe';
    } else if(data.params.paymentType === 'iPay88') {
      orderObject.states = 'On Hold';
      orderObject.paymentType = 'iPay88';
    } else {
      if(data.params.subsItems.length > 0) {
        orderObject.states = 'Payment Received';
      } else {
        orderObject.states = 'Completed';
      }
      orderObject.paymentType = 'cash';
    }

    // process set value for userID or seller ID
    console.log(`make order dxata ====== ${JSON.stringify(data)} ===== `);
    loggingHelper.log('info', `make order dxata ====== ${JSON.stringify(data)} ===== `);
    if(data.params.userTypeId === 1 && !data.identity.guest) {
      orderObject.UserId = data.identity.id;
    } else if(data.UserId) {
      orderObject.UserId = data.UserId;
    }
    if(data.params.userTypeId === 2) {
      orderObject.SellerUserId = data.identity.id;
      orderObject.channelType = data.params.channelType;
      orderObject.eventLocationCode = data.params.eventLocationCode;
      orderObject.MarketingOfficeId = data.identity.MarketingOfficeId;
    }

    // set value for receive
    receiptObject.subTotalPrice = subTotal;
    receiptObject.discountAmount = discountAmount;

    console.log(`make order orderObject ====== ${JSON.stringify(orderObject)} ===== `);
    loggingHelper.log('info', `make order orderObject ====== ${JSON.stringify(orderObject)} ===== `);

    // build order history
    orderObject.orderHistories = [{
      message: orderObject.states
    }];

    // perform in server order
    return this.db.Order.create(orderObject, { include: ['orderDetail', 'orderHistories'], returning: true, transaction: t })
      .then(order => ({ order, updatedProducts, receiptObject, country }));
  }

  /**
   * make charge to stripe
   * @param {*} data 
   */
  makeChargeToStripe(data) {
    loggingHelper.log('info', 'makeChargeToStripe start');
    loggingHelper.log('info', `param: data = ${data}`);
    let chargeInfo;
    let collectedFrom = (data.params.userTypeId === 2) ? 'Sales App' : 'Website';
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    let type = data.identity.isSeller ? 'baWeb' : 'website';
    if(data.params.paymentType === 'stripe') {
      let options = {where: {id: data.params.CardId, isRemoved: false}};
      if(data.identity.id && data.params.userTypeId === 1) {
        options.where = Object.assign(options.where, {UserId: data.identity.id});
      } else {
        options.where = Object.assign(options.where, {UserId: data.UserId});
      }
      return this.db.Card.findOne(options)
      .then(card => {
        if(!card) {
          throw new Error('card_invalid');
        }
        // store last4 card number
        data.receiptObject.last4 = card.cardNumber;
        data.receiptObject.branchName = card.branchName;
        data.receiptObject.expiredYear = card.expiredYear;
        data.receiptObject.expiredMonth = card.expiredMonth;

        // make charge to stripe
        data.order.Country = {
          code: data.country.code
        };
        console.log(`data.receiptObject.totalPrice ====== ${data.receiptObject.totalPrice}`);
        let chargeObject = {
          amount: data.receiptObject.totalPrice,
          currency: data.chargeCurrency.toLowerCase(),
          customer: card.customerId,
          description: `Shaves2u charge for orderId: ${OrderHelper.fromatOrderNumber(data.order, true)}`,
          metadata: {
            collectedFrom
          }
        };
        return stripeHelper.createCharge(chargeObject, data.country.code, type);
      })
      .then(_chargeInfo => {
        chargeInfo = _chargeInfo;
        if(data.params.userTypeId === 2 && data.params.subsItems.length === 0) {
          return;
        } else {
          return this.db.Order.update({states: 'Payment Received'}, {where: {id: data.order.id}});
        }
      })
      .then(() => this.db.OrderHistory.create({
        OrderId: data.order.id,
        message: 'Payment Received'
      }))
      .then(() => {
        if(data.params.userTypeId === 2 && data.params.subsItems.length === 0) {
          return this.db.Order.update({states: 'Completed'}, {where: {id: data.order.id}})
          .then(() => 
            this.db.OrderHistory.create({
              OrderId: data.order.id,
              message: 'Completed'
            })
          );
        } else {
          return;
        }
      })
      .then(() => taxInvoiceQueueService.addTask(data.order))
      .then(() => stripeHelper.retrieveBalanceTransaction(chargeInfo.balance_transaction, data.country.code, type))
      .catch(err => {
        console.log(err.stack);
        loggingHelper.log('error', err.stack);
        let sendEmailQueueService = SendEmailQueueService.getInstance();
        let userId = data.params.userTypeId === 1 ? data.identity.id : data.UserId;
        // send email to user
        this.db.Order.update({states: 'On Hold'}, {where: {id: data.order.id}})
        .then(() => sendEmailQueueService.addTask({method: 'sendPaymentFail', data: {userId}, langCode: data.country.defaultLang.toLowerCase()}));
        throw err;
      });
    } else {
      return {};
    }
  }

  createReceipt(data, t) {
    loggingHelper.log('info', 'createReceipt start');
    loggingHelper.log('info', `param: data = ${data}`);
    return this.db.Receipt.create(data.receiptObject, {returning: true, transaction: t});
  }

  /**
   * Create subscription
   * @param {*} data 
   */
  createSubscription(data, t) {
    loggingHelper.log('info', 'createSubscription start');
    loggingHelper.log('info', `param: data = ${data}`);
    let subscriptions = [];
    data.params.subsItems.forEach(item => {
      let planCountry = _.findWhere(data.planCountries, {id: item.PlanCountryId});
      if(!planCountry) {
        throw new Error('not_found');
      }
      // if(planCountry.qty < item.qty) {
      //   throw new Error('not_enough');
      // }
      let startDeliverDate = new Date(moment().format('YYYY-MM-DD'));
      let currentDeliverNumber = 1;
      if(!item.deliveryNow && data.params.userTypeId === 2) {
        startDeliverDate = new Date(moment().add(3, 'months').format('YYYY-MM-DD'));
        currentDeliverNumber = 0;
      }

      let subscription = {
        PlanCountryId: item.PlanCountryId,
        DeliveryAddressId: data.params.DeliveryAddressId,
        BillingAddressId: data.params.BillingAddressId,
        fullname: data.params.fullName,
        email: data.params.email,
        phone: data.params.phone,
        qty: item.qty,
        totalDeliverTimes: planCountry.Plan.PlanType.totalDeliverTimes,
        totalChargeTimes: planCountry.Plan.PlanType.totalChargeTimes,
        currentChargeNumber: 1,
        currentDeliverNumber,
        startDeliverDate,
        price: planCountry.sellPrice,
        isTrial: item.isTrial,
        MarketingOfficeId: data.identity.MarketingOfficeId
      };

      // calculate next charge time
      subscription.nextChargeDate = moment().add(planCountry.Plan.PlanType.subsequentChargeDuration.split(' ')[0], planCountry.Plan.PlanType.subsequentChargeDuration.split(' ')[1]).format('YYYY-MM-DD');

      // calculate next charge time
      subscription.nextDeliverDate = moment().add(planCountry.Plan.PlanType.subsequentDeliverDuration.split(' ')[0], planCountry.Plan.PlanType.subsequentDeliverDuration.split(' ')[1]).format('YYYY-MM-DD');

      // calculate expired date
      subscription.expiredDateAt = moment().add(1, 'years').format('YYYY-MM-DD');

      if(data.params.CardId) {
        subscription.CardId = data.params.CardId;
      }
      // set identity
      if(data.params.userTypeId === 1 && !data.identity.guest) {
        subscription.UserId = data.identity.id;
      } else if(data.identity.guest) {
        subscription.UserId = data.UserId;
      }
      if(data.params.userTypeId === 2) {
        subscription.SellerUserId = data.identity.id;
        subscription.UserId = data.UserId;
        subscription.channelType = data.params.channelType;
        subscription.eventLocationCode = data.params.eventLocationCode;
      }
      // if(data.promotion.id && (data.promotion.planCountryIds === 'all' || data.promotion.planCountryIds.indexOf(item.PlanCountryId))) {
      // // check discount
      //   subscription.discountPercent = data.promotion.discount;
      // }

      // calculate price per charge
      subscription.pricePerCharge = planCountry.sellPrice / planCountry.Plan.PlanType.totalChargeTimes;

      subscriptions.push(subscription);
    });
    return this.db.Subscription.bulkCreate(subscriptions, {transaction: t});
  }

  /** Perform process checkout for 
   * create order
   * @param {*} req 
   */
  processCheckout(req) {
    loggingHelper.log('info', 'processCheckout start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let CountryId = req.country.id;
    params.userTypeId = params.userTypeId === 2 ? params.userTypeId : 1;
    // get product country IDs
    if(params.alaItems) {
      params.alaItems = (Array.isArray(params.alaItems)) ? params.alaItems : [params.alaItems];
      params.productCountryIds = _.pluck(params.alaItems, 'ProductCountryId');
    } else {
      params.alaItems = [];
      params.productCountryIds = [];
    }

    // get plan country ids
    if(params.subsItems) {
      params.subsItems = (Array.isArray(params.subsItems)) ? params.subsItems : [params.subsItems];
      params.planCountryIds = _.pluck(params.subsItems, 'PlanCountryId');
    } else {
      params.subsItems = [];
      params.planCountryIds = [];
    }

    let data = {
      params,
      country: req.country
    };

    // get identity for place order
    return this.getIdentityForCheckOut(req, data)
      .then(identity => {
        // check promo code
        data.identity = identity;
        // return this.proccessPromotion(data);
      })
      .then(() => 
        // data.promotion = promotion;
        // create subscription if needed
        this.db.sequelize.transaction(t => this.db.PlanCountry.findAll({where: {id: {$in: data.params.planCountryIds}}, include: ['Country', {
          model: this.db.Plan,
          include: ['PlanType']
        }]}, {transaction: t})
          .then(planCountries => {
            data.planCountries = planCountries;
            return this.db.DeliveryAddress.findOne({where: {id: data.params.DeliveryAddressId}});
          })
          .then(deliveryAddress => {
            data.deliveryAddress = deliveryAddress;
            // check email
            return this.db.User.findOne({where: {email: params.email}});
          })
          .then(user => {
            if(!user && ((params.userTypeId === 2) || (data.identity.guest))) {
              let _user;
              let firstName;
              let lastName;
              if(data.deliveryAddress) {
                firstName = data.deliveryAddress.firstName;
                lastName = data.deliveryAddress.lastName;
              } else if(params.fullName && params.fullName.indexOf(' ') > 0) {
                firstName = params.fullName.substr(0, params.fullName.indexOf(' '));
                lastName = params.fullName.substr(params.fullName.indexOf(' ') + 1);
              } else {
                firstName = params.fullName;
              }
              data.newCustomer = true;
              return this.db.User.create({
                email: params.email,
                firstName,
                lastName,
                CountryId: req.country.id,
                isGuest: true
              }, {returning: true, transaction: t})
                .then(result => {
                  _user = result;
                  return this.db.Cart.create({UserId: result.id}, {transaction: t})
                    .then(() => {
                      let utmDefaultData = this.mailchimp.getDefaultUtmData();
                      this.mailchimp.addMemberToRegisterList({
                        'email_address': params.email,
                        'status': 'subscribed',
                        'merge_fields': {
                          FNAME: firstName,
                          LNAME: lastName ? lastName : '',
                          COUNTRY: req.country.code.substr(0, 2),
                          SOURCE: params.userTypeId === 2 ? 'BA app' : 'Website',
                          USOURCE: params.utmSource ? params.utmSource.replace(/(http|ftp|https):\/\/([w]+\.)?([\w]+).*/g, '$3') : this.mailchimp.getDefaultUtmData('source'),
                          UMEDIUM: params.utmMedium ? params.utmMedium : utmDefaultData,
                          UCAMPAIGN: params.utmCampaign ? params.utmCampaign : utmDefaultData,
                          UCONTENT: params.utmContent ? params.utmContent : utmDefaultData,
                          UTERM: params.utmTerm ? params.utmTerm : utmDefaultData
                        }
                      });
                    });
                })
                .then(() => _user);
            } else if(user && !user.phone && params.userTypeId === 2) {
              // update user if order make from BA-app
              return this.db.User.update({phone: params.phone}, {where: {id: user.id}})
                .then(() => user);
            } else {
              return user;
            }
          })
          .then(user => {
            data.UserId = user.id;
            data.user = user;
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.DeliveryAddress.update({UserId: user.id}, {where: {id: data.params.DeliveryAddressId}, transaction: t});
            }
          })
          .then(() => {
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.DeliveryAddress.update({UserId: data.UserId}, {where: {id: data.params.BillingAddressId}, transaction: t});
            }
          })
          .then(() => {
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.Card.update({UserId: data.UserId}, {where: {id: data.params.CardId}, transaction: t});
            }
          })
          .then(() => this.proccessPromotion(data))
          .then(promotion => {
            data.promotion = promotion;
            if(data.identity.guest || params.userTypeId === 2) {
              return this.db.Subscription.findAll({where: {UserId: data.UserId}});
            } else {
              return this.db.Subscription.findAll({where: {UserId: data.identity.id}});
            }
          })
          .then(subsciber => {
            data.isSubscriber = (subsciber.length > 0);
            return this.createSubscription(data, t);
          })
          .then(subscriptions => {
            data.subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
            return this.db.ProductCountry.findAll({where: {
              id: {
                $in: data.params.productCountryIds
              }
            }, include: ['Country', {
              model: this.db.Product,
              include: ['ProductType']
            }]}, {transaction: t});
          })
          .then(productCountries => {
            data.productCountries = productCountries;

            // calculate price and create order
            return this.makeOrder(data, t);
          })
          .then(result => {
            data = Object.assign(data, result);
            data.receiptObject.OrderId = data.order.id;
            return this.createReceipt(data, t);
          })
          .then(receipt => {
            this.receiptObject = SequelizeHelper.convertSeque2Json(receipt);
            return this.db.Cart.findOne({where: {UserId: data.order.UserId}});
          })
          .then(cart => {
            if(cart) {
              return this.db.CartDetail.destroy({where: {cartId: cart.id, CountryId}});
            } else {
              return;
            }
          }))
          .then(() => this.makeChargeToStripe(data)) // make charge to stripe
          .then(balanceTransaction => {
            console.log(`data.receiptObject ${JSON.stringify(data.receiptObject)} `);
            loggingHelper.log('info', `data.receiptObject ${JSON.stringify(data.receiptObject)} `);
            data.receiptObject.chargeId = balanceTransaction.source;
            data.receiptObject.chargeFee = balanceTransaction.fee;
            data.receiptObject.chargeCurrency = balanceTransaction.currency;
            // create receipt
            this.db.Receipt.update(data.receiptObject, {where: {OrderId: data.order.id}});
          })
          .then(() => {
            if(data.subscriptions.length > 0) {

              let subscriptionIds = _.pluck(data.subscriptions, 'id');

              return this.db.Subscription.update({status: 'Processing'}, {where: {id: {$in: subscriptionIds}}});
            }
          })
          .then(() => {
            let subscriptionIds = _.pluck(data.subscriptions, 'id');
            let subsData = [];

            for (var i = 0; i < subscriptionIds.length; i++) {
              subsData.push({
                message: 'Processing',
                subscriptionId: subscriptionIds[i]
              });
            }

            this.db.SubscriptionHistory.bulkCreate(subsData);
          })
          .then(() => {
            if((data.receiptObject.discountAmount !== 0 && !data.promotion.isGeneric) || (data.promotion.freeProductCountryIds && !data.promotion.isGeneric)) {
              return this.db.PromotionCode.update({isValid: false}, {where: {code: data.params.promotionCode}});
            } else if((data.receiptObject.discountAmount !== 0 || data.hasUsedPromoCode) && data.promotion.isGeneric) {
              let appliedTo = JSON.parse(data.promotion.appliedTo);
              if(!appliedTo) {
                appliedTo = {};
                if(data.params.userTypeId === 1 && !data.identity.guest) {
                  appliedTo[data.identity.id] = 1;
                } else {
                  appliedTo[data.UserId] = 1;
                }
              } else {
                if(data.params.userTypeId === 1 && !data.identity.guest) {
                  if(appliedTo[data.identity.id]) {
                    appliedTo[data.identity.id] = appliedTo[data.identity.id] + 1;
                  } else {
                    appliedTo[data.identity.id] = 1;
                  }
                } else {
                  if(appliedTo[data.UserId]) {
                    appliedTo[data.UserId] = appliedTo[data.UserId] + 1;
                  } else {
                    appliedTo[data.UserId] = 1;
                  }
                }
              }
              return this.db.Promotion.update({appliedTo: JSON.stringify(appliedTo)}, {where: {id: data.promotion.id}});
            }
          })
          .then(() => {
            if(data.params.subsItems.length > 0 && data.identity.email) {
              data.user.Country = data.user.Country ? data.user.Country : data.order.Country;
              this.mailchimp.addMemberToSubscriptionList(data.user);
            }
            Reflect.deleteProperty(data, 'user');
          })
          .then(() => data)
      );
  }
}

export default OrderController;
