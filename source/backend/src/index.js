import shortId from 'shortid';
import config from './config';
import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import _ from 'underscore';

import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import errors from './components/errors';
import path from 'path';
import hbs from 'express-handlebars';
import paymentGateways from './payment-gateways';
import webhooks from './webhooks';
import views from './views';
import Schedule from './scheduler';
import TokenService from './services/token-service';
import AppContext from './services/app-context';
import JwtMiddleware from './middleware/jwt-middleware';
import jwt from 'jsonwebtoken';
import ResponseHelper from './helpers/response-helper';
import SequelizeHelper from './helpers/sequelize-helper';
import ipware from 'ipware';
// import iplocation from 'iplocation';
import request from 'request';
import moment from 'moment';

let app = express();
let getIP = ipware().get_ip;
app.server = http.createServer(app);

// view engine setup
app.engine('hbs', hbs({ extname: 'hbs' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
  origin: true,
  allowedHeaders: ['X-Requested-With, Accept, Content-Type, Origin, authorization, country-code, x-access-token, client-timezone-offset, lang-code']
}));

app.use(bodyParser.json({
  limit: config.bodyLimit
}));

// support encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

// connect to db
let db;
initializeDb()
  .then(_db => {
    db = _db;
    return db.Country.findAll();
  })
  .then(countries => {
    global.countries = SequelizeHelper.convertSeque2Json(countries);

    // sync database
    console.log(`Connection to database ${config.db.host} successful !!!`);
    //set app path
    app.set('appPath', path.join(config.root, 'public'));
    app.set('uploadPath', path.join(config.root, 'uploads'));

    // set static file
    app.use(express.static(app.get('appPath')));
    app.use('/uploads', express.static(app.get('uploadPath')));

    // internal middleware

    app.use(middleware({ db }));

    // // Temporate fake the Urbanfox API
    // let urbanfoxStatus = [];
    // let fulfillments = [];
    // app.post('/api/urbanfox/activity', (req, res) => {
    //   console.log(`Urban fox get activity ${JSON.stringify(req.body)}`);
    //   let refNo = req.body.query.replace(/.*referenceNumber1.+"(\w+)".*/g, '$1');
    //   let fakeData = _.findWhere(urbanfoxStatus, {referenceNumber1: refNo});
    //   if(fakeData) {
    //     fakeData.status = 'DELIVERED';
    //     fulfillments.push({
    //       referenceNumber1: refNo,
    //       status: 'DELIVERED'
    //     });
    //   } else {
    //     fakeData = {
    //       referenceNumber1: refNo,
    //       status: 'NEW_FULFILLMENT',
    //       trackingNumber: shortId.generate()
    //     };
    //     urbanfoxStatus.push(fakeData);
    //     fulfillments.push(fakeData);
    //   }
    //   res.json({
    //     data: {
    //       getOrder: {
    //         status: 'NEW_ORDER',
    //         remarks: 'test',
    //         referenceNumber1: refNo,
    //         fulfillments
    //       }
    //     }
    //   });
    // });

    // seller login without country code
    /** POST /sellers/login
     * @param: email
     * @param: password
     * @return: user object
     */
    app.post('/api/sellers/login', (req, res) => {
      let params = req.body;
      console.log('sellerLogin');

      return db.SellerUser.update(
        {
          lastLoginAt: moment(),
          channelType: params.channelType,
          eventLocationCode: params.eventLocationCode
        },
        {
          where: {
            badgeId: params.badgeId,
            icNumber: params.icNumber,
            isActive: true
          }
        }
      )
        .then(() => db.SellerUser.findOne({
          where: {
            badgeId: params.badgeId,
            icNumber: params.icNumber,
            isActive: true
          }, include: [
            'MarketingOffice',
            {
              model: db.Country,
              as: 'Country',
              include: [
                'SupportedLanguages',
                {
                  model: db.DirectoryCountry,
                  as: 'DirectoryCountries',
                  attributes: {
                    exclude: ['id', 'code', 'codeIso2', 'name', 'nativeName', 'currencyCode', 'languageCode', 'callingCode', 'CountryId']
                  }
                }
              ]
            }
          ]
        }))
        .then(result => {
          if(!result) {
            return ResponseHelper.responseError(res, 'Badge ID  or number i/C number do not match');
          } else {
            res.json(JwtMiddleware.generateToken(result, false, true));
          }
        })
        .catch(err => ResponseHelper.responseInternalServerError(res, err));
    });

    /** GET /sellers/channel-type
     * get channel-type of seller
     * @param {*} req 
     * @param {*} res 
     */
    app.get('/api/sellers/channel-type', (req, res) => {
      res.json(db.SellerUser.rawAttributes.channelType.values);
    });

    /**
     * GET /countries/current-country
     * get current country
     * @param {*} req 
     * @param {*} res 
     */
    app.get('/api/countries/current-country', (req, res) => {
      let ip = getIP(req);
      console.log(`ip ====== ${JSON.stringify(ip)}`);
      request(`http://api.ipstack.com/${ip.clientIp}?access_key=${config.ipStack.apiKey}&output=json&legacy=1`, { json: true }, (err, result, body) => {
        if(err) { 
          return ResponseHelper.responseError(res, 'can not get country code');
        }
        if(body.country_code) {
          db.DirectoryCountry.findOne({ where: { codeIso2: body.country_code } })
            .then(country => {
              res.json({ ok: true, data: country });
            })
            .catch(error => ResponseHelper.responseInternalServerError(res, error));
        } else {
          return ResponseHelper.responseError(res, 'can not get country code');
        }
      });
      
      // iplocation(ip.clientIp, (err, result) => {
      //   console.log(err, result);
      //   if(err) {
      //     return ResponseHelper.responseError(res, 'can not get country code');
      //   } else {
      //     db.DirectoryCountry.findOne({ where: { codeIso2: result.country_code } })
      //       .then(country => {
      //         res.json({ ok: true, data: country });
      //       })
      //       .catch(error => ResponseHelper.responseInternalServerError(res, error));
      //   }
      // });
    });

    /**
     * GET latest ondroid app version
     */
    app.get('/api/lastes-android-app', (req, res) => {
      db.AndroidVersion.findAll({
        limit: 1,
        order: [['createdAt', 'DESC']]
      })
        .then(androidVersions => {
          console.log(`androidVersions === ${JSON.stringify(androidVersions)}`);
          androidVersions = (androidVersions.length > 0) ? androidVersions[0] : null;
          res.json(androidVersions);
        })
        .catch(err => ResponseHelper.responseInternalServerError(res, err));
    });


    /**
     * GET build reset password link for old user
     */
    app.get('/api/old-users/reset-password', (req, res) => {
      let params = req.query;

      if(!params.token || !params.email) {
        return ResponseHelper.responseMissingParam(res);
      }

      JwtMiddleware.decodeTokenAsync(params.token)
        .then(() => db.User.findOne({ where: { email: params.email } })) // check user existing
        .then(user => {
          if(!user) {
            return res.redirect(`${config.webUrl}login?email=${params.email}`);
          } else {
            // regenerate new token and new link
            let expireAt = moment().add(config.email.expiredTime);
            // redirect user to website user reset password
            return res.redirect(`${config.webUrl}users/reset-password?email=${params.email}&token=${jwt.sign({ token: expireAt, email: params.email }, config.accessToken.secret)}&nextPage=${config.webUrl}`);
          }
        })
        .catch(error => {
          console.log(error);
          if(error.message === 'token_expired') {
            // redirect to login page with a message
            return res.redirect(`${config.webUrl}users/reset-password?email=${params.email}&message=token_expired`);
          } else {
            return res.redirect(`${config.webUrl}`);
          }
        });
    });

    /**
     * GET SEO config for web site
     */
    app.get('/api/SEO-configuration', (req, res) => {
      res.json(config.appSEO);
    });

    /**
     * GET deliver date for Korea
     */
    app.get('/api/deliver-date-for-korea', (req, res) => {
      res.json(config.deliverDateForKorea);
    });

    // api router
    app.use('/api', JwtMiddleware.verifyCountryCode.bind({ db }), api({ db }));

    // payment gateway router
    app.use('/payment-gateway', paymentGateways({ db }));

    // webhook
    app.use('/webhooks', webhooks({ db }));

    app.use('/', views({ db }));

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth|components|app|bower_components|assets|scripts)/*')
      .get(errors[404]);

    // All other routes should redirect to the index.html
    app.route('/*')
      .get((req, res) => {
        res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
      });

    app.server.listen(process.env.PORT || config.port, () => {
      console.log(`Started on port ${app.server.address().port}`);


      // init token service
      let tokenService = new TokenService(db);
      global.tokenService = tokenService;

      // init app-context
      global.appContext = new AppContext();

      // start scheduler service
      let scheduler = new Schedule(db);
      // start schedule jon
      scheduler.startJobs();
    });
  })
  .catch(err => {
    console.log(`can not connect to database: Host - ${config.db.host}; user - ${config.db.user}: ${err.stack}`);
  });

export default app;
