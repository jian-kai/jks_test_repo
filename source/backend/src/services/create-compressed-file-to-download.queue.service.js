/**
 * Queue process update product quantity
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const COMPRESSED_FILE_TO_DOWNLOAD_LIST_KEY = 'compressed-file-to-download-list-key';

class CompressedFileToDownloadQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new CompressedFileToDownloadQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(options) {
    return this.redisClient.saddAsync(COMPRESSED_FILE_TO_DOWNLOAD_LIST_KEY, JSON.stringify(options));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(COMPRESSED_FILE_TO_DOWNLOAD_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(COMPRESSED_FILE_TO_DOWNLOAD_LIST_KEY, key);
  }

}

export default CompressedFileToDownloadQueueService;
