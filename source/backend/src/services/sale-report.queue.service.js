/**
 * Queue process cancel order
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const SALE_REPORT_LIST_KEY = 'sale-report-list-key';

class SaleReportQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new SaleReportQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(options) {
    return this.redisClient.saddAsync(SALE_REPORT_LIST_KEY, JSON.stringify(options));
  }
  
  getKeysInQueue() {
    let returnValue = [];
    return this.redisClient.smembersAsync(SALE_REPORT_LIST_KEY)
      .then(redisData => {
        if(redisData) {
          redisData.forEach(data => returnValue.push(JSON.parse(data)));
          this.removeKeysFromQueue(returnValue);
        }
        return returnValue;
      });
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(JSON.stringify(key)));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(SALE_REPORT_LIST_KEY, key);
  }

}

export default SaleReportQueueService;
