import jwt from 'jsonwebtoken';
import config from '../config';
import moment from 'moment';

class TokenService {
  constructor(db) {
    this.db = db;
  }

  verifyToken(token, storeLater) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.accessToken.secret, (err, decoded) => {
        console.log(`decoded ==== ${moment().toString()} == ${moment(decoded.expiredAt).toString()}`);
        if(err) {
          reject(err);
        } else {
          // check token is used or not
          this.db.UsedToken.findOne({where: {token}})
            .then(usedToken => {
              console.log(`usedToken === ${JSON.stringify(usedToken)}`);
              if(usedToken) {
                reject(new Error('token_is_used'));
              } else {
                return;
              }
            })
            .then(() => {
              // check expired time
              if(moment().isAfter(moment(decoded.expiredAt))) {
                reject(new Error('token_expired'));
              } else if(!storeLater) {
                return this.db.UsedToken.create({token});
              } else {
                return;
              }
            })
            .then(() => resolve(decoded))
            .catch(err => reject(err));
        }
      });
    });
  }
}

export default TokenService;
