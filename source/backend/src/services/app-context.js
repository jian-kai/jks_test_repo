/**
 * Application Context that holds resources used through the system
 *
 * @author Kim Thi
 */

'use strict';

import config from '../config';
import bluebird from 'bluebird';
import redis from 'redis';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

/**
 * @prop _logger
 */
class AppContext {

  /**
   * @constructor
   */
  constructor() {
    this.redisClient = null;

    this.initialize();
  }

  initialize() {
    this.createRedisClient();
  }

  createRedisClient() {
    let options = {
      url: config.redisConnectionString,
      db: config.redisDbIndex
    };

    if(process.env.CONFIG_ENV === 'production') {
      options.password = config.redis.password;
    }

    this.redisClient = redis.createClient(options);

    this.redisClient.on('error', error => {
      console.log(`Connection redis has problem -  ${error.stack}`);
    });
  }

  getAppConfig() {
    return config;
  }

  getRedisClient() {
    return this.redisClient;
  }

  destroy() {
    if(this.redisClient) {
      this.redisClient.quit();
    }
  }

}

export default AppContext;
