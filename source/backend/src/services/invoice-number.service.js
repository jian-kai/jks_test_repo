/**
 * Queue process cancel order
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const TAX_INVOICE_LIST_KEY = 'tax-invoice-list-key';

class TaxInvoiceQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new TaxInvoiceQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(options) {
    return this.redisClient.saddAsync(TAX_INVOICE_LIST_KEY, JSON.stringify(options));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(TAX_INVOICE_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(TAX_INVOICE_LIST_KEY, key);
  }

}

export default TaxInvoiceQueueService;
