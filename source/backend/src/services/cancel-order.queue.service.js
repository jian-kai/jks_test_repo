/**
 * Queue process cancel order
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const CANCEL_ORDER_LIST_KEY = 'cancel-oder-list-key';

class CancelOrderQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new CancelOrderQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(orderData) {
    return this.redisClient.saddAsync(CANCEL_ORDER_LIST_KEY, JSON.stringify(orderData));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(CANCEL_ORDER_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(CANCEL_ORDER_LIST_KEY, key);
  }

}

export default CancelOrderQueueService;
