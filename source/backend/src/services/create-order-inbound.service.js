/**
 * Queue process update product quantity
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const ORDER_INBOUND_LIST_KEY = 'order-inbound-list-key';

class OrderInboundQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new OrderInboundQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(orderId, countryId) {
    let data = {};
    if(typeof orderId === 'string' || typeof orderId === 'number') {
      data = {
        orderId,
        countryId,
        retry: 0
      };
    }
    return this.redisClient.saddAsync(ORDER_INBOUND_LIST_KEY, JSON.stringify(data));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(ORDER_INBOUND_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(ORDER_INBOUND_LIST_KEY, key);
  }

}

export default OrderInboundQueueService;
