/**
 * Queue process update product quantity
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const EMAIL_LIST_KEY = 'send-email-list-key';

class SendEmailQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new SendEmailQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(options) {
    return this.redisClient.saddAsync(EMAIL_LIST_KEY, JSON.stringify(options));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(EMAIL_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(EMAIL_LIST_KEY, key);
  }

}

export default SendEmailQueueService;
