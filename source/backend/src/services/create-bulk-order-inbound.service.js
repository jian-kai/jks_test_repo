/**
 * Queue process update product quantity
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';

let instance = null;
const BULK_ORDER_INBOUND_LIST_KEY = 'bulk-order-inbound-list-key';

class BulkOrderInboundQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new BulkOrderInboundQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  addTask(orderId, countryId) {
    let data = {};
    if(typeof orderId === 'string' || typeof orderId === 'number') {
      data = {
        orderId,
        countryId,
        retry: 0
      };
    }
    return this.redisClient.saddAsync(BULK_ORDER_INBOUND_LIST_KEY, JSON.stringify(data));
  }
  
  getKeysInQueue() {
    return this.redisClient.smembersAsync(BULK_ORDER_INBOUND_LIST_KEY);
  }

  removeKeysFromQueue(keys) {
    return Promise.map(keys, key => this.removeKeyFromQueue(key));
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(BULK_ORDER_INBOUND_LIST_KEY, key);
  }

}

export default BulkOrderInboundQueueService;
