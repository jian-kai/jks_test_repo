/**
 * Queue process update product quantity
 *
 * @author Kim Thi
 */

'use strict';
import Promise from 'bluebird';
import shortId from 'shortid';
import _ from 'underscore';

let instance = null;
const UPDATE_PRODUCT_KEY = 'update-product';
const UPDATE_PRODUCT_LIST_KEY = 'update-product-list';

class UpdateProductQueueService {

  /**
   * @param {Object} options Service init configuration
   * @constructor
   */
  constructor(options) {
    if(!options.redisClient) {
      throw new Error('No Redis client defined');
    }
    this.redisClient = options.redisClient;
  }

  static getInstance() {
    if(!instance) {
      instance = new UpdateProductQueueService({ redisClient: global.appContext.getRedisClient() });
    }
    return instance;
  }

  /**
   * @param data: Object
   */
  addTask(items) {
    return Promise.map(items, item => {
      let key = shortId.generate();
      console.log(`add task ${key}`);
      return Promise.all([
        this.redisClient.setAsync(`${UPDATE_PRODUCT_KEY}-${key}`, JSON.stringify(item)),
        this.redisClient.saddAsync(UPDATE_PRODUCT_LIST_KEY, key)
      ]).then(() => {
        console.log(`Add ${JSON.stringify(item)} into update production queue`);
      });
    });
  }
  
  getKeyInQueue() {
    return this.redisClient.smembersAsync(UPDATE_PRODUCT_LIST_KEY);
  }

  getItems(keys) {
    return Promise.map(keys, key => this.redisClient.getAsync(`${UPDATE_PRODUCT_KEY}-${key}`)
      .then(data => JSON.parse(data))
    )
      .then(items => {
        // build item for update
        let updateItems = [];
        let tmp;
        items.forEach(item => {
          tmp = _.findWhere(updateItems, {id: item.id});
          if(tmp) {
            tmp.qty += item.qty;
          } else {
            tmp = item;
          }
          updateItems.push(Object.assign({}, tmp));
        });
        return updateItems;
      });
  }

  removeItemsFromQueue(keys) {
    return Promise.map(keys, key =>
      Promise.all([
        this.removeKeyFromQueue(key),
        this.removeItemFromQueue(key)
      ])
    );
  }

  removeKeyFromQueue(key) {
    return this.redisClient.sremAsync(UPDATE_PRODUCT_LIST_KEY, key);
  }

  removeItemFromQueue(key) {
    return this.redisClient.delAsync(`${UPDATE_PRODUCT_KEY}-${key}`);
  }

}

export default UpdateProductQueueService;
