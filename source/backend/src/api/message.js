import Promise from 'bluebird';
import _ from 'underscore';

import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import config from '../config';
import { loggingHelper } from '../helpers/logging-helper';

class MessageApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/message', this.getMessage.bind(this));
    this.api.post('/message', this.postMessage.bind(this));

    this.api.get('/admin-message', this.getAdminMessageList.bind(this));
    this.api.get('/admin-message/MessageGetList', this.getAdminAllMessageList.bind(this));
    this.api.get('/admin-message/total', this.getAdminAllMessageTotal.bind(this));
  }

  /** GET /countries
   * get all countries in DB
   */

  getAdminAllMessageList(req, res) {   
    loggingHelper.log('info', 'getAdminAllMessageList start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    // let countryCode = req.headers['country-code'];
    // let langCode = req.headers['lang-code'];

    // let langCodeget = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    // if(!countryCode) {
    //   return ResponseHelper.responseError(res, 'missing country Code');
    // }
    // if(!langCodeget) {
    //   return ResponseHelper.responseError(res, 'missing Language Code');
    // }
    this.db.messagestranslates.findAll()
    .then(messagestranslates =>{
     // console.log(`messagestranslates === ${JSON.stringify(messagestranslates)}`);

       res.json(messagestranslates)}
  )
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
 


  }
  //   getAdminAllMessageList(req, res) {   
  //   loggingHelper.log('info', 'getAdminAllMessageList start');
  //   loggingHelper.log('info', `param: req.query = ${req.query}`);
  //   // get param in request
  //   let params = req.query || {};
  //   // let countryCode = req.headers['country-code'];
  //   // let langCode = req.headers['lang-code'];

  //   // let langCodeget = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
  //   // if(!countryCode) {
  //   //   return ResponseHelper.responseError(res, 'missing country Code');
  //   // }
  //   // if(!langCodeget) {
  //   //   return ResponseHelper.responseError(res, 'missing Language Code');
  //   // }
  //   this.db.messagestranslates.findAll()
  //   .then(messagestranslates =>{
  //     console.log(`messagestranslates === ${JSON.stringify(messagestranslates)}`);

  //      res.json(messagestranslates)}
  // )
  //   .catch(error => ResponseHelper.responseInternalServerError(res, error));
 


  // }


  getAdminAllMessageTotal(req, res) {   
    loggingHelper.log('info', 'getAdminAllMessageTotal start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    // let countryCode = req.headers['country-code'];
    // let langCode = req.headers['lang-code'];

    // let langCodeget = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    // if(!countryCode) {
    //   return ResponseHelper.responseError(res, 'missing country Code');
    // }
    // if(!langCodeget) {
    //   return ResponseHelper.responseError(res, 'missing Language Code');
    // }
    this.db.cancellationjourneys.findAndCountAll()
    .then(cancellationjourneys =>{
     // console.log(`messagestranslates === ${JSON.stringify(cancellationjourneys)}`);

       res.json(cancellationjourneys)}
  )
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
 


  }

   /**
   * GET /admin-plans
   * get all plans
   * @param {*} req 
   * @param {*} res 
   */
  getAdminMessageList(req, res) {
    loggingHelper.log('info', 'getAdminMessageList start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
   // loggingHelper.log('info', `param testestststestes: req.query =  ${JSON.stringify(req.query)}`);
    let params = req.query || {};
    // let countryCode = req.headers['country-code'];
    // let langCode = req.headers['lang-code'];

    // let langCodeget = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    // if(!countryCode) {
    //   return ResponseHelper.responseError(res, 'missing country Code');
    // }
    // if(!langCodeget) {
    //   return ResponseHelper.responseError(res, 'missing Language Code');
    // }
    let options = {};
  
    options.include = [{
      model: this.db.User,
      as: 'User'
    }];
    options.include = [{
      model: this.db.Order,
      as: 'Order'
    }];
    options.include = [{
      model: this.db.Subscription,
      as: 'Subscription',
      include: [

        {
          model: this.db.PlanCountry,
          include: [{
            model: this.db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.Plan,
            include: ['planImages', 'planTranslate', 'PlanType']
          }]
        },
        {
          model: this.db.CustomPlan,
          as: 'CustomPlan',
          include: [
            'Country',
            'PlanType',
            {
              model: this.db.CustomPlanDetail,
              as: 'customPlanDetail',
             
            }
          ]
        }
      ]
    }];
  

    let include = [{
      model: this.db.User,
      as: 'User'
    },
   {
      model: this.db.Order,
      as: 'Order'
    },
    {
      model: this.db.Subscription,
      as: 'Subscription',
      include: [
     
        {
          model: this.db.PlanCountry,
          include: [{
            model: this.db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.Plan,
            include: ['planImages', 'planTranslate', 'PlanType']
          }]
        },
        {
          model: this.db.CustomPlan,
          as: 'CustomPlan',
          include: [
            'Country',
            'PlanType',
            {
              model: this.db.CustomPlanDetail,
              as: 'customPlanDetail',
              include: [{
                model: this.db.ProductCountry,
                as: 'ProductCountry',
                include: [{
                  model: this.db.Product,
                  as: 'Product',
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        }
      ]
    }];
    options.raws = true;
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.cancellationjourneys);


    if(!options.order) {
      options.order = ['createdAt'];
    }

    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where,{email: {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }


    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }
    
    // options.where = Object.assign(options.where, {
    //   CountryId: req.country.id,
    //   langCode: langCodeget
    // });


    SequelizeHelper.findAndCountAll(this.db.cancellationjourneys, options, include)
      .then(result => {res.json(result)
       //console.log(`Cancellation Journeys === ${JSON.stringify(result)}`);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));



  //   this.db.cancellationjourneys.findAll(options)
  //   .then(cancellationjourneys =>{
  //     console.log(`Cancellation Journeys === ${JSON.stringify(cancellationjourneys)}`);

  //      res.json(cancellationjourneys)}
  // )
  //   .catch(error => ResponseHelper.responseInternalServerError(res, error));
 

  }



  getMessage(req, res) {   
    loggingHelper.log('info', 'getMessage start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    let countryCode = req.headers['country-code'];
    // let langCode = req.headers['lang-code'];

    let langCodeget = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }
    if(!langCodeget) {
      return ResponseHelper.responseError(res, 'missing Language Code');
    }
    let options = RequestHelper.buildWhereCondition({}, params, this.db.messages);
    options.include = [{
      model: this.db.messagestranslates,
      as: 'messagestranslates'
    }];
  
    options.where = Object.assign(options.where, {
      CountryId: req.country.id,
      langCode: langCodeget
    });

    this.db.messages.findAll(options)
    .then(messages =>{
      console.log(`messages === ${JSON.stringify(messages)}`);

       res.json(messages)}
  )
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
 


  }


  postMessage(req, res) {
    loggingHelper.log('info', `postMessage === ${req.body}`);
    let params = req.body;

    // params.CountryId = req.country.id;

    console.log("postMessage message id = " + params.messageid);
    console.log("postMessage subcription id = " + params.subid);
    console.log("postMessage user id = " + params.userid);
    console.log("postMessage option reason = " + params.optionReason);
    console.log("postMessage email = " + params.email);
  
    // ('info', `param testestststestes: req.query =  ${JSON.stringify(params.testest)}`);
    // if(!countryCode) {
    //   return ResponseHelper.responseError(res, 'missing country Code');
    // }

   
    if(!params.userid) {
      return ResponseHelper.responseMissingParam(res);
    }

    let options = {};
    if(!params.messageid) {
    if(!params.optionReason) {
      options = {
        UserId: params.userid,
        email : params.email,
        SubscriptionId : params.subid
      };
    }
    else{
    options = {
      UserId: params.userid,
      email : params.email,
      SubscriptionId : params.subid,
      OtherReason : params.optionReason
    };
  }
}
else
{
  if(!params.optionReason) {
    options = {
      MessagestranslatesId: params.messageid,
      email : params.email,
      UserId: params.userid,
      SubscriptionId : params.subid
    };
  }
  else{
  options = {
    MessagestranslatesId: params.messageid,
    email : params.email,
    UserId: params.userid,
    SubscriptionId : params.subid,
    OtherReason : params.optionReason
  };
}
}

    this.db.cancellationjourneys.create(options)
      .then(() => res.json({ok: true}))
      .catch(err => {
        return ResponseHelper.responseInternalServerError(res, err);
    });;



    // this.db.NonEcommerce.findOne({where: {email: params.email}})
    //   .then(email => {
    //     if(email) {
    //       throw new Error('email_existed');
    //     } else {
    //       return this.db.NonEcommerce.create(params)
    //       .then(nonEcommerce => this.db.NonEcommerce.findOne({ where: {
    //         id: nonEcommerce.id
    //       }, include: ['Country']
    //       }))
    //       .then(data => this.mailchimp.addMemberToNewCustomerEmailForKoreaList(data));
    //     }
    //   })
    //   .then(() => ResponseHelper.responseSuccess(res))
    //   .catch(error => {
    //     if(error.message === 'email_existed') {
    //       return ResponseHelper.responseError(res, 'This email is already used');
    //     } else {
    //       return ResponseHelper.responseInternalServerError(res, error);
    //     }
    //   });
  }


}

export default function(...args) {
  return new MessageApi(...args);
}
