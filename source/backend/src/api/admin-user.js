import shortId from 'shortid';
import CryptoUtil from 'crypto-js';
import multipart from 'connect-multiparty';

import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import moment from 'moment';
import json2csv from 'json2csv';
import Validator from '../helpers/validator';
import { isNullOrUndefined } from 'util';
import { loggingHelper } from '../helpers/logging-helper';

let multipartMiddleware = multipart();

class AdminUserApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/admin-users', JwtMiddleware.verifyAdminToken, this.getUsers.bind(this));
    this.api.get('/admin-users/ba-guest', JwtMiddleware.verifyAdminToken, this.getBaGuest.bind(this));
    this.api.get('/admin-users/ba-guest/download', JwtMiddleware.verifyAdminToken, this.downloadBaGuest.bind(this));
    this.api.post('/admin-users/salesman', JwtMiddleware.verifyAdminToken, this.postSalesman.bind(this));
    this.api.put('/admin-users/salesman/:id', JwtMiddleware.verifyAdminToken, this.updateSalesman.bind(this));
    this.api.put('/admin-users/:id', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.updateUserStatus.bind(this));
    this.api.post('/admin-users/change-status', JwtMiddleware.verifyAdminToken, this.postUpdateStaffStatus.bind(this)); 
  }

  /** GET /admin-users
   * get all users in DB
   */
  getUsers(req, res) {
    loggingHelper.log('info', 'getUsers start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    
    if(!decoded.viewAllCountry) {
      options = {where: { CountryId: decoded.CountryId }, include: ['Country']};
    } else {
      options = {include: ['Country']};
    }

    if(params.email && params.email !== '') {
      options.where = Object.assign({email: {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Admin);

    this.db.User.findAndCountAll(options)
      .then(users => {
        res.json(users);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
  
  /** GET /users/ba-guest
   * get all guests of BA app
   */
  getBaGuest(req, res) {
    loggingHelper.log('info', 'getBaGuest start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {
      badgeId: !isNullOrUndefined
    }, include: ['Country']};

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.User);

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }
    
    this.db.User.findAndCountAll(options)
      .then(users => {
        res.json(users);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /users/ba-guest/download
   * download guests of BA app
   * @param {*} req 
   * @param {*} res 
   */
  downloadBaGuest(req, res) {
    loggingHelper.log('info', 'downloadBaGuest start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    
    let options = {where: {
      badgeId: !isNullOrUndefined
    }, include: ['Country']};

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    params.toDate = params.toDate ? params.toDate : moment().format('YYYY-MM-DD');
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.User);

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }
    this.db.User.findAll(options)
      .then(result => {
        // build order data
        let orderTmp = [];
        let tmp = {};

        result.forEach(user => {
          tmp = {
            fullName: user.firstName + (user.lastName ? (' ' + user.lastName) : ''),
            email: user.email,
            phone: (user.phone) ? ('\'' + user.phone) : '',
            badgeId: user.badgeId,
            region: user.Country.code,
            createdAt: user.createdAt.toISOString().replace(/T/, ' ').replace(/\..+/, '')
          };
          orderTmp.push(tmp);
        });

        // create CSV and response file
        let fields = ['fullName', 'email', 'phone', 'badgeId', 'region', 'createdAt'];
        let fieldNames = ['Full Name', 'Email', 'Phone', 'Badge Id', 'Region', 'Created At'];
        let data = json2csv({
          data: orderTmp,
          fields,
          fieldNames
        });
        // send CSV file
        res.attachment(`Guests report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  updateUserStatus(req, res) {
    console.log('updateUserStatus ======');
    loggingHelper.log('info', 'updateUserStatus start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    this.db.User.findOne({id: req.params.id})
      .then(user => {
        console.log(`User ==== ${JSON.stringify(user)}`);
        loggingHelper.log('info', `User ==== ${JSON.stringify(user)}`);
        if(!user) {
          throw new Error('old_password_invalid');
        } else {
          return this.db.User.update({isActive: params.isActive}, {
            where: {
              id: req.params.id,
            }
          });
        }
      })
    .then(() => this.db.User.findById(req.params.id))
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      if(error.message === 'old_password_invalid') {
        return ResponseHelper.responseError(res, 'old password is invalid');
      } else {
        return ResponseHelper.responseError(res, error);
      }
    });
  }

  /**
   * POST /admin-users/change-status
   * Allow admin update status of multiple staff
   * @param {*} req 
   * @param {*} res 
   */
  postUpdateStaffStatus(req, res) {
    loggingHelper.log('info', `postUpdateStaffStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if(!params.staffIds || !params.isActive) {
      return ResponseHelper.responseMissingParam(res, 'Params');
    }

    params.isActive = (params.isActive === 'true' || params.isActive === '1' || params.isActive === 1);
    params.staffIds = Array.isArray(params.staffIds) ? params.staffIds : [params.staffIds];

    this.db.Admin.update({isActive: params.isActive}, {where: {id: {$in: params.staffIds}}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** POST /admin-users/salesman
   * Creat salesman account
   */
  postSalesman(req, res) {
    loggingHelper.log('info', 'postSalesman start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let decoded = req.decoded;
    let role = decoded.adminRole;
    if(role === this.db.Admin.rawAttributes.role.values[0]) {
      if(!params.CountryId && !params.AdminId) {
        return ResponseHelper.responseMissingParam(res);
      }
    } else {
      params.CountryId = decoded.CountryId;
      if(role === this.db.Admin.rawAttributes.role.values[1] && !params.AdminId) {
        return ResponseHelper.responseMissingParam(res);
      } else if(role === this.db.Admin.rawAttributes.role.values[2]) {
        params.AdminId = decoded.id;
      }
    }

    // set role for account as a salesman
    params.isActive = true;

    // generate password for this account
    let password = shortId.generate();
    params.password = CryptoUtil.MD5(password).toString();

    // check email existing
    this.db.User.findOne({where: {
      email: params.email
    }})
      .then(_user => {
        if(_user) {
          throw new Error('email_existed');
        } else {
          return this.db.User.create(params, {returning: true});
        }
      })
      .then(user => res.json(user))
      .catch(error => {
        if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, 'email is existed');
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** PUT /admin-users/salesman/:id
   * Update saleman account
   */
  updateSalesman(req, res) {
    loggingHelper.log('info', 'updateSalesman start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let decoded = req.decoded;
    let role = decoded.adminRole;

    if(role !== this.db.Admin.rawAttributes.role.values[0]) {
      if(!params.CountryId && !params.AdminId) {
        Reflect.deleteProperty(params, 'AdminId');
        Reflect.deleteProperty(params, 'CountryId');
      }
    }

    // set role for account as a salesman
    params.isActive = true;

    // generate password for this account
    if(params.password) {
      Reflect.deleteProperty(params, 'password');
    }

    let hasChangeEmail = false;
    this.db.User.findOne({where: {
      id: req.params.id
    }})
      .then(_user => {
        if(!_user) {
          throw new Error('id_not_exist');
        } else if(params.email && _user.email !== params.email) {
          hasChangeEmail = true;
          return this.db.User.findOne({where: {
            email: params.email,
            $not: {
              id: req.params.id
            }
          }});
        }
      })
      .then(_user => {
        console.log(`_user ${_user}`);
        if(hasChangeEmail && _user) {
          throw new Error('email_existed');
        }
        return;
      })
      .then(() => this.db.User.update(params, {where: {id: req.params.id}, returning: true}))
      .then(() => this.db.User.findOne({where: {id: req.params.id}}))
      .then(user => res.json({ok: true, user}))
      .catch(error => {
        if(error.message === 'id_not_exist') {
          return ResponseHelper.responseNotFoundError(res, this.db.User.name);
        } else if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, 'email is existed');
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
}

export default function(...args) {
  return new AdminUserApi(...args);
}
