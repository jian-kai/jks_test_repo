import _ from 'underscore';
import moment from 'moment';
import jwt from 'jsonwebtoken';
import config from '../config';
import ResponseHelper from '../helpers/response-helper';
import Validator from '../helpers/validator';
import CryptoUtil from 'crypto-js';
import JwtMiddleware from '../middleware/jwt-middleware';
import SequelizeHelper from '../helpers/sequelize-helper';
import SendEmailQueueService from '../services/send-email.queue.service';
import { loggingHelper } from '../helpers/logging-helper';

class UserAuthenticationApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/users/login', this.userLogin.bind(this));
    this.api.post('/users/active', this.userActiveUser.bind(this));
    this.api.post('/users/active-login', this.userActiveLogin.bind(this));
    this.api.post('/users/signinFB', this.userSigninFB.bind(this));
    this.api.post('/users/check-email', this.checkEmail.bind(this));
    this.api.post('/users/send-active-email', this.sendActiveEmail.bind(this));
    this.api.post('/users/validate-token', this.validateToken.bind(this));
    this.api.post('/users/register', this.userRegister.bind(this));
    this.api.post('/users/reset-password', this.resetPassword.bind(this));
    this.api.post('/users/update-password', this.updatePassword.bind(this));
  }

  _getUserDetail(email, countryId, password) {
    let whereClause = {
      email
    };

    if(password) {
      whereClause = Object.assign(whereClause, { password });
    }
    console.log(`_getUserDetail == ${email} == ${countryId} == ${password}`);
    loggingHelper.log('info', `_getUserDetail == ${email} == ${countryId} == ${password}`);
    return this.db.User.findOne({where: whereClause, include: [
      'Country', {
        model: this.db.Subscription,
        as: 'subscriptions'
      },
      {
        model: this.db.DeliveryAddress,
        as: 'deliveryAddresses',
        where: { isRemoved: false },
        required: false,
        include: ['Country']
      },
      {
        model: this.db.Cart,
        include: [{
          require: false,
          model: this.db.CartDetail,
          as: 'cartDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [
              'Country',
              {
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
          }, {
            model: this.db.PlanCountry,
            include: [
              'Country',
              {
                model: this.db.Plan,
                include: ['planImages', 'planTranslate', 'PlanType']
              }]
          }]
        }]
      }
    ]})
    .then(result => {
      if(!result) {
        throw new Error('user_not_existed');
      }
      // build cart object
      result = SequelizeHelper.convertSeque2Json(result);
      let tmp = [];
      let cartDetails = _.filter(result.Cart.cartDetails, (cart => cart.CountryId === countryId));

      // build cart detail
      cartDetails.forEach(cartDetail => {
        console.log(`cartDetail ==== ${JSON.stringify(cartDetail)}`);
        if(cartDetail.ProductCountry) {
          let product = {};
          product.id = cartDetail.ProductCountry.Product.id;
          product.sku = cartDetail.ProductCountry.Product.sku;
          product.slug = cartDetail.ProductCountry.Product.slug;
          product.rating = cartDetail.ProductCountry.Product.rating;
          product.productTranslate = cartDetail.ProductCountry.Product.productTranslate;
          product.productImageURL = _.findWhere(cartDetail.ProductCountry.Product.productImages, {isDefault: true}).url;
          Reflect.deleteProperty(cartDetail.ProductCountry, 'Product');
          product.productCountry = cartDetail.ProductCountry;
          Reflect.deleteProperty(cartDetail, 'ProductCountry');
          cartDetail.product = product;
        }
        if(cartDetail.PlanCountry) {
          let plan = {};
          plan.id = cartDetail.PlanCountry.Plan.id;
          plan.sku = cartDetail.PlanCountry.Plan.sku;
          plan.slug = cartDetail.PlanCountry.Plan.slug;
          plan.rating = cartDetail.PlanCountry.Plan.rating;
          plan.planTranslate = cartDetail.PlanCountry.Plan.planTranslate;
          plan.PlanType = cartDetail.PlanCountry.Plan.PlanType;
          plan.planImageURL = _.findWhere(cartDetail.PlanCountry.Plan.planImages, {isDefault: true}).url;
          Reflect.deleteProperty(cartDetail.PlanCountry, 'Plan');
          plan.planCountry = cartDetail.PlanCountry;
          Reflect.deleteProperty(cartDetail, 'PlanCountry');
          cartDetail.plan = plan;
        }
        tmp.push(cartDetail);
      });
      result.Cart.cartDetails = tmp;

      // build subscription
      let tmpSubs = [];
      result.subscriptions.forEach(subcrription => {
        if(subcrription.status !== 'Canceled') {
          tmpSubs.push(subcrription);
        }
      });
      result.subscriptions = tmpSubs;
      return result;
    });
  }

  /** POST /users/checkEmail
   * check email is existed
   */
  checkEmail(req, res) {
    loggingHelper.log('info', 'checkEmail start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    if(!req.body.email) {
      return ResponseHelper.responseMissingParam(res);
    }
    this._getUserDetail(req.body.email, req.country.id)
      .then(result => res.json(JwtMiddleware.generateToken(result)))
      .catch(err => {
        if(err.message === 'user_not_existed') {
          return res.json({isExisted: false});
        }
        return ResponseHelper.responseInternalServerError(res, err);
      });
  }

  /** POST /users/sendActiveEmail
   * check email is existed
   */
  sendActiveEmail(req, res) {
    loggingHelper.log('info', 'sendActiveEmail start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let email = req.body.email;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    if(!req.body.email) {
      return ResponseHelper.responseMissingParam(res);
    }
    this.db.User.findOne({where: { email }, include: ['Country']})
      .then(user => {
        if(!user) {
          throw new Error('invalid_email');
        }
        // emailHelper.sendActiveUser(user);
        return sendEmailQueueService.addTask({method: 'sendActiveUser', data: user, langCode: req.country.defaultLang.toLowerCase()}, this.db);
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.message === 'invalid_email') {
          return ResponseHelper.responseError(res, config.messages[langCode].authentication.emailExisted.replace('{{email}}', email));
        }
        return ResponseHelper.responseInternalServerError(res, error);
      });
  }

  /**
   * POST /users/validate-token
   * check token is expired or not
   * @param {*} req 
   * @param {*} res 
   */
  validateToken(req, res) {
    loggingHelper.log('info', 'validateToken start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // check token
    let token = req.body.token;
    if(!token) {
      return ResponseHelper.responseMissingParam(res);
    }
    jwt.verify(token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseErrorParam(res, 'Token is invalid');
      } else if(moment(decoded.token).isBefore(moment())) {
        return ResponseHelper.responseErrorParam(res, 'Token is expired');
      } else {
        return res.json({ok: true});
      }
    });
  }

  /** POST /users/login
   * @param: email
   * @param: password
   * @return: user object
   */
  userLogin(req, res) {
    loggingHelper.log('info', 'userLogin start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let user = {};
    user.email = req.body.email;
    user.password = req.body.password;
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';

    // encrypt password
    user.password = CryptoUtil.MD5(user.password).toString();

    this.db.User.findOne({where: {email: user.email}})
      .then(result => {
        if(!result) {
          throw new Error('user_not_existed');
        } else if(!result.isActive) {
          throw new Error('user_inactive');
        } else {
          return this._getUserDetail(user.email, req.country.id, user.password);
        }
      })
      .then(result => res.json(JwtMiddleware.generateToken(result)))
      .catch(err => {
        if(err.message === 'user_not_existed') {
          return ResponseHelper.responseError(res, config.messages[langCode].authentication.loginFail);
        } else if(err.message === 'user_inactive') {
          return res.json({
            ok: false,
            code: 'inactive',
            message: config.messages[langCode].authentication.accountNotActive
          });
        }
        return ResponseHelper.responseInternalServerError(res, err);
      });
  }

  /** POST /users/active-login
   * @param: token
   * @return: user object
   */
  userActiveLogin(req, res) {
    loggingHelper.log('info', 'userActiveLogin start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let user;
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';

    if(!params.token) {
      return ResponseHelper.responseMissingParam(res, 'token is required');
    }

    // decode token
    jwt.verify(params.token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseErrorParam(res, err);
      } else {
        console.log(`userActiveLogin ==== ${JSON.stringify(decoded)}`);
        loggingHelper.log('info', `userActiveLogin ==== ${JSON.stringify(decoded)}`);
        this.db.User.update({isActive: true}, {where: {email: decoded.email}})
          .then(() => this._getUserDetail(decoded.email, req.country.id))
          .then(result => {
            user = result;
            return this.db.User.findOne({where: {id: user.id}, include: ['Country']});
          })
          .then(result => sendEmailQueueService.addTask({method: 'sendWelcomeEmail', data: result, langCode: req.country.defaultLang.toLowerCase()}))
          .then(() => res.json(JwtMiddleware.generateToken(user)))
          .catch(err => {
            if(err.message === 'user_not_existed') {
              return ResponseHelper.responseError(res, config.messages[langCode].authentication.emailNotExisted);
            }
            return ResponseHelper.responseInternalServerError(res, err);
          }); 
      }
    });
  }

  /**
   * POST /users/active
   * active user
   * @param {*} req 
   * @param {*} res 
   */
  userActiveUser(req, res) {
    loggingHelper.log('info', 'userActiveUser start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    if(!params.token) {
      return ResponseHelper.responseMissingParam(res, 'token is required');
    }

    // decode token
    jwt.verify(params.token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseErrorParam(res, err);
      } else {
        this.db.User.update({isActive: true}, {where: {email: decoded.email}, returning: true})
          .then(() => this.db.User.findOne({where: {email: decoded.email}}))
          .then(user => {
            if(!user) {
              throw new Error('user_not_existed');
            }
            return user;
          })
          .then(user => this.db.User.findOne({where: {id: user.id}, include: ['Country']}))
          .then(result => sendEmailQueueService.addTask({method: 'sendWelcomeEmail', data: result, langCode: req.country.defaultLang.toLowerCase()}))
          .then(() => {
            res.json({ok: true});
          })
          .catch(err => {
            if(err.message === 'user_not_existed') {
              return ResponseHelper.responseError(res, config.messages[langCode].authentication.emailNotExisted);
            }
            return ResponseHelper.responseInternalServerError(res, err);
          }); 
      }
    });
  }

  /**
   * POST /user/signinFB
   * login or register user via FB account
   * @param {*} req 
   * @param {*} res 
   */
  userSigninFB(req, res) {
    loggingHelper.log('info', 'userSigninFB start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    console.log(`userSigninFB ==== ${JSON.stringify(params)}`);
    loggingHelper.log('info', `userSigninFB params ==== ${JSON.stringify(params)}`);
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    // validate require field
    if(!params.email || !params.socialId) {
      return ResponseHelper.responseMissingParam(res);
    }

    // check existed email
    this.db.User.findOne({where: {email: params.email}})
      .then(user => {
        console.log(`user == ${JSON.stringify(user)}`);
        if(user) {
          // if(!user.socialId) {
          //   throw new Error('email_existed');
          // } else {
            // get user data
          return this._getUserDetail(user.email, req.country.id);
          // }
        } else {
          let returnValue = {};
          params.CountryId = req.country.id;
          params.isActive = true;
          // perform register
          // store user into database
          return this.db.User.create(params)
            .then(results => {
              let userId = results.get({ plain: true }).id;
              // create cart for user
              return this.db.Cart.create({ UserId: userId });
            })
            .then(() => this._getUserDetail(params.email, req.country.id))
            .then(result => {
              result = SequelizeHelper.convertSeque2Json(result);
              returnValue = result;
              // create reset link
              console.log(`returnValue == ${JSON.stringify(returnValue)}`);
              loggingHelper.log('info', `returnValue == ${JSON.stringify(returnValue)}`);
              // emailHelper.sendActiveUser(result);
              result.utmData = params.utmData;
              return sendEmailQueueService.addTask({method: 'sendWelcomeEmail', data: result, langCode: req.country.defaultLang.toLowerCase()}, this.db);
            })
            .then(() => returnValue);
        }
      })
      .then(result => res.json(JwtMiddleware.generateToken(result)))
      .catch(error => {
        if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, 'Email is existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** POST /users/register
   * @param: user object
   * @return: user object
   */
  userRegister(req, res) {
    loggingHelper.log('info', 'userRegister start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let countryCode = req.headers['country-code'];
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    if(!params.email || !params.password) {
      return ResponseHelper.responseMissingParam(res);
    }

    // validate format email and password
    if(!Validator.validateEmail(params.email) || !Validator.validatePassword(params.password)) {
      return ResponseHelper.responseError(res, 'email or password is incorrect!');
    }

    // validate date
    if(!Validator.valiadateDate(params.birthday)) {
      return ResponseHelper.responseError(res, 'Birthday is incorrect!');
    }

    // get country from country code
    let returnValue = {};

    // encrypt password
    params.password = CryptoUtil.MD5(params.password).toString();
    params.CountryId = req.country.id;

    if(params.birthday) {
      params.birthday = new Date(params.birthday);
    }

    if(!params.firstName && !params.lastName) {
      params.firstName = params.email.substring(0, params.email.lastIndexOf('@'));
    }

    this.db.User.findOne({ where: {
      email: params.email
    }})
      .then(user => {
        let userId = {};
        if(user) {
          if(params.isCheckout && !user.isActive) {
            return user;
          } else {
            throw new Error('email_existed');
          }
        } else {
          // store user into database
          return this.db.sequelize.transaction(t => this.db.User.create(params, {transaction: t})
            .then(results => {
              userId = results.get({ plain: true }).id;
              // create cart for user
              return this.db.Cart.create({ UserId: userId }, {transaction: t});
            }));
        }
      })
      .then(() => this._getUserDetail(params.email, req.country.id))
      .then(result => {
        returnValue.user = result;
        // create active link
        // emailHelper.sendActiveUser(result);
        result.utmData = params.utmData;
        return sendEmailQueueService.addTask({method: 'sendActiveUser', data: result, langCode: req.country.defaultLang.toLowerCase()}, this.db);
      })
      .then(() => res.json(JwtMiddleware.generateToken(returnValue.user)))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        }
        if(error.message === 'invalid_countryCode') {
          return ResponseHelper.responseError(res, `Country code (${countryCode} is invalid)`);
        }
        if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, config.messages[langCode].authentication.emailExisted.replace('{{email}}', params.email));
        }
        return ResponseHelper.responseInternalServerError(res, error);
      });
  }

  /** POST /user/reset-password 
   * @param: email String
   */
  resetPassword(req, res) {
    loggingHelper.log('info', 'resetPassword start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // check input data
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
    console.log(`resetPassword === ${req.method} == ${JSON.stringify(params)}`);
    loggingHelper.log('info', `resetPassword === ${req.method} == ${JSON.stringify(params)}`);
    if(!params.email) {
      return ResponseHelper.responseErrorParam(res);
    }

    // get user info
    this.db.User.findOne({ where: {
      email: params.email
    }, include: ['Country']})
      .then(user => {
        if(!user) {
          throw new Error('not_existed');
        } else {
          // do async
          // emailHelper.sendResetUserPassword(user);
          return sendEmailQueueService.addTask({method: 'sendResetUserPassword', data: user, langCode: req.country.defaultLang.toLowerCase()});
        }
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.message === 'not_existed') {
          return ResponseHelper.responseError(res, config.messages[langCode].authentication.emailNotExisted);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** POST /users/update-password
   * @param: new password
   */
  updatePassword(req, res) {
    loggingHelper.log('info', 'updatePassword start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // check token
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    if(!params.password || !params.token) {
      return ResponseHelper.responseMissingParam(res);
    }
    let password = CryptoUtil.MD5(params.password).toString();
    jwt.verify(params.token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseErrorParam(res, 'Token is invalid');
      } else if(moment(decoded.token).isBefore(moment())) {
        return ResponseHelper.responseErrorParam(res, 'Token is expired');
      } else {
        this.db.User.update({
          password,
          isActive: true
        }, {where: {
          email: decoded.email
        }})
          .then(() => this.db.User.findOne({where: {email: decoded.email}}))
          .then(user => sendEmailQueueService.addTask({method: 'sendPasswordUpdated', data: { email: decoded.email, firstName: user.firstName }, langCode: req.country.defaultLang.toLowerCase()}))
          .then(() => res.json({ok: true}))
          .catch(error => ResponseHelper.responseInternalServerError(res, error));
      }
    });
  }
}

export default function(...args) {
  return new UserAuthenticationApi(...args);
}
