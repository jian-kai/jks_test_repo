import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import { loggingHelper } from '../helpers/logging-helper';

class FaqAPI {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    // admin APIs
    this.api.get('/admin-faqs', JwtMiddleware.verifyAdminToken, this.adminGetFaqs.bind(this));
    this.api.post('/admin-faqs', JwtMiddleware.verifyAdminToken, this.postFaq.bind(this));
    this.api.put('/admin-faqs/:id', JwtMiddleware.verifyAdminToken, this.editFaq.bind(this));
    this.api.get('/admin-faqs/:id', JwtMiddleware.verifyAdminToken, this.adminGetFaqDetails.bind(this));
    this.api.delete('/admin-faqs/:id', JwtMiddleware.verifyAdminToken, this.deleteFaq.bind(this));
    this.api.delete('/admin-faqs/faq-translates/:id', JwtMiddleware.verifyAdminToken, this.deleteFaqTranslate.bind(this));

    // user APIs
    this.api.get('/faqs', this.getFaqs.bind(this));
    this.api.get('/faqs/types', this.getFaqTypes.bind(this));
  }

  /**
   * GET /faqs
   * get all faqs
   * @param {*} req 
   * @param {*} res 
   */
  getFaqs(req, res) {
    loggingHelper.log('info', 'getFaqs start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;

    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();

    let options = RequestHelper.buildWhereCondition({}, params, this.db.Faq);
    options.include = [{
      model: this.db.FaqTranslate,
      as: 'faqTranslate'
    }];

    options.where = Object.assign(options.where, {
      CountryId: req.country.id,
      langCode: params.langCode
    });

    this.db.Faq.findAll(options)
      .then(faqs => res.json(faqs))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /admin-faqs/:id
   * Allow admin get FAQ details
   * @param {*} req 
   * @param {*} res 
   */
  adminGetFaqDetails(req, res) {
    loggingHelper.log('info', 'adminGetFaqDetails start');
    this.db.Faq.findOne({
      where: {id: req.params.id},
      include: ['faqTranslate', 'Country']
    })
      .then(faq => ResponseHelper.responseSuccess(res, faq))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /faqs/types
   * get all FAQ type
   * @param {*} req 
   * @param {*} res 
   */
  getFaqTypes(req, res) {
    loggingHelper.log('info', 'getFaqTypes start');
    res.json(this.db.Faq.rawAttributes.type.values);
  }

  /**
   * GET /admin-faqs
   * allow admin get all FAQ
   * @param {*} req 
   * @param {*} res 
   */
  adminGetFaqs(req, res) {
    loggingHelper.log('info', `adminGetFaqs: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    if(params.CountryId) {
      options.where = {
        'CountryId': params.CountryId
      };
    }
    options.include = ['Country'];

    options.order = [['CountryId']];

    this.db.Faq.findAndCountAll(options)
      .then(faqs => res.json(faqs))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /admin-faqs
   * allow admin add new FAQ
   * @param {*} req 
   * @param {*} res 
   */
  postFaq(req, res) {
    loggingHelper.log('info', `postFaq: req.query = ${req.body}`);
    let params = req.body;

    // validate require fields
    if(!params.type || !params.CountryId || !params.langCode || !params.faqTranslate) {
      return ResponseHelper.responseMissingParam(res);
    }

    // edit data
    params.langCode = params.langCode.toUpperCase();
    params.isSubscription = (params.isSubscription && params.isSubscription !== '');
    params.isAlaCarte = (params.isAlaCarte && params.isAlaCarte !== '');

    // init transaction
    this.db.sequelize.transaction(t => this.db.Faq.create(params, {
      include: ['faqTranslate'],
      returning: true,
      transaction: t
    }))
      .then(faq => ResponseHelper.responseSuccess(res, faq)) // create FAQ
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * PUT /admin-faqs/:id
   * allow admin update FAQ
   * @param {*} req 
   * @param {*} res 
   */
  editFaq(req, res) {
    loggingHelper.log('info', `editFaq: req.query = ${req.body}`);
    let params = req.body;

    // init data for faqTranslate
    params.faqTranslate = Array.isArray(params.faqTranslate) ? params.faqTranslate : [];
    params.langCode = params.langCode ? params.langCode.toUpperCase() : null;
    params.isSubscription = (params.isSubscription && params.isSubscription !== '');
    params.isAlaCarte = (params.isAlaCarte && params.isAlaCarte !== '');

    // init transaction
    let updateFaqTranslate = [];
    let createFaqTranslate = [];
    this.db.sequelize.transaction(t => {
      params.faqTranslate.forEach(faqTranslate => {
        if(faqTranslate.id) {
          updateFaqTranslate.push(faqTranslate);
        } else {
          faqTranslate.FaqId = req.params.id;
          createFaqTranslate.push(faqTranslate);
        }
      });
      return SequelizeHelper.bulkUpdate(updateFaqTranslate, this.db, 'FaqTranslate', {transaction: t})
        .then(() => this.db.FaqTranslate.bulkCreate(createFaqTranslate, {returning: true, transaction: t}))
        .then(() => this.db.Faq.update(params, {where: {id: req.params.id}, transaction: t}));
    })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /admin-faqs/:id
   * allow admin delete FAQ
   * @param {*} req 
   * @param {*} res 
   */
  deleteFaq(req, res) {
    loggingHelper.log('info', `deleteFaq: req.query = ${req.body}`);
    this.db.Faq.destroy({where: {id: req.params.id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /
   * @param {*} req 
   * @param {*} res 
   */
  deleteFaqTranslate(req, res) {
    loggingHelper.log('info', `deleteFaqTranslate: req.query = ${req.body}`);
    this.db.FaqTranslate.destroy({where: {id: req.params.id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
}

export default function(...args) {
  return new FaqAPI(...args);
}
