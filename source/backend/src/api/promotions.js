import Promise from 'bluebird';
import moment from 'moment';
import _ from 'underscore';
import multipart from 'connect-multiparty';
import shortId from 'shortid';

import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import MailChimpHelper from '../helpers/mailchimp';
import Validator from '../helpers/validator';
import SequelizeHelper from '../helpers/sequelize-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import SendEmailQueueService from '../services/send-email.queue.service';
import { awsHelper } from '../helpers/aws-helper';
import config from '../config';
import { loggingHelper } from '../helpers/logging-helper';

let multipartMiddleware = multipart();

class PromotionApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
    this.mailchimp = MailChimpHelper.getInstance();
  }

  registerRoute() {
    this.api.get('/promotions', this.getPromotions.bind(this));
    this.api.get('/promotions/cart-rules', JwtMiddleware.decodeToken, this.getCartRules.bind(this));
    this.api.post('/promotions/check', JwtMiddleware.decodeToken, this.checkPromotionCode.bind(this));
    this.api.get('/admin-promotions/email-group', JwtMiddleware.verifyAdminToken, this.getEmailGroup.bind(this));
    this.api.post('/admin-promotions', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.postPromotionCode.bind(this));
    this.api.get('/admin-promotions', JwtMiddleware.verifyAdminToken, this.getAdminPromotions.bind(this));
    this.api.get('/admin-promotions/:id', JwtMiddleware.verifyAdminToken, this.getAdminPromotionDetails.bind(this));
    this.api.put('/admin-promotions/:id', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.putAdminPromotion.bind(this));
    this.api.delete('/admin-promotions/:id', JwtMiddleware.verifyAdminToken, this.removeAdminPromotions.bind(this));
  }

  /** GET /notification-types
   * get all notification-types in DB
   */
  getPromotions(req, res) {
    loggingHelper.log('info', 'getPromotions start');
    loggingHelper.log('info', `param: req.headers = ${req.headers}`);
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    this.db.Promotion.findAll({include: [
      'promotionTranslate',
      {
        model: this.db.Country,
        where: {code: countryCode}
      }
    ]})
      .then(promotions => {
        res.json(promotions);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /promotions/cart-rules
   * get user definition cart rules
   * @param {*} req
   * @param {*} res
   */
  getCartRules(req, res) {
    loggingHelper.log('info', 'getCartRules start');
    loggingHelper.log('info', `param: req.headers = ${req.headers}`);
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseMissingParam(res);
    } else {
      return res.json(config.checkoutRules[countryCode.toUpperCase()] ? config.checkoutRules[countryCode.toUpperCase()] : []);
    }
  }

  /**
   * GET /admin/promotions
   * get all promotions for admin
   * @param {*} req
   * @param {*} res
   */
  getAdminPromotions(req, res) {
    loggingHelper.log('info', 'getAdminPromotions start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    let options = {where: {
      isBaApp: false
    }};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, { CountryId: req.country.id });
    }

    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    options.distinct = true;

    options.include = [{
      model: this.db.PromotionTranslate,
      as: 'promotionTranslate',
      duplicating: false
    }, {
      model: this.db.Country,
      duplicating: false
    }];

    // update isGeneric field
    if(params.isGeneric === '1' || params.isGeneric === 1) {
      params.isGeneric = true;
    } else if(params.isGeneric && params.isGeneric !== '1' && params.isGeneric !== 1) {
      params.isGeneric = false;
    }

    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {'$promotionTranslate.name$': {$like: `%${params.s}%`}},
          {'$promotionTranslate.description$': {$like: `%${params.s}%`}}
        ]
      });
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Promotion);

    console.log(`params ===== ${JSON.stringify(options)}`);

    SequelizeHelper.findAndCountAll(this.db.Promotion, options, ['promotionTranslate', 'Country'])
      .then(result => res.json(result))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * PUT /admin-promotions/:id
   * update promotion
   * @param {*} req
   * @param {*} res
   */
  putAdminPromotion(req, res) {
    loggingHelper.log('info', 'putAdminPromotion start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);

    let params = req.body;
    let image = req.files.bannerUrl;
    let data = {};

    // validate images
    if(!image && !params.bannerUrl) {
      return ResponseHelper.responseMissingParam(res);
    }
    // validate expire date format
    if(!Validator.valiadateDate(params.expiredAt, true)) {
      return ResponseHelper.responseMissingParam(res, 'missing expired at date or  is invalid');
    }
    if(!Validator.valiadateDate(params.activeAt)) {
      return ResponseHelper.responseMissingParam(res, 'Active at date is invalid');
    }
    if(params.isGeneric && params.isGeneric === 'true' && !params.promoCode) {
      return ResponseHelper.responseMissingParam(res, 'Missing promo code');
    }
    if(!params.promotionTranslate) {
      return ResponseHelper.responseMissingParam(res, 'Missing promotion translate');
    }
    if(!params.CountryId) {
      return ResponseHelper.responseMissingParam(res, 'Missing Country Id');
    }
    if(!params.productCountryIds && !params.planCountryIds) {
      return ResponseHelper.responseMissingParam(res, 'You must be choose at least one product or plan for this promotion');
    }
    if(!params.isGeneric && !params.userIds && !params.mailChimpListId) {
      return ResponseHelper.responseMissingParam(res, 'You must be choose at least one user or mailchimp list for this promotion');
    }

    params.productCountryIds = (params.productCountryIds && params.productCountryIds !== 'all') ? JSON.stringify(params.productCountryIds) : params.productCountryIds;
    params.planCountryIds = (params.planCountryIds && params.planCountryIds !== 'all') ? JSON.stringify(params.planCountryIds) : params.planCountryIds;

    let uploadImagePromise = Promise.resolve({Location: params.bannerUrl});
    if(image) {
      // upload image to S3
      uploadImagePromise = awsHelper.upload(config.AWS.productImagesAlbum, image);
    }

    uploadImagePromise.then(uploadedImage => {
      params.bannerUrl = uploadedImage.Location;
      return this.db.sequelize.transaction(t => this.db.Promotion.update(params, {where: {id: req.params.id}}, {transaction: t})
        .then(() => {
          data.promotion = params;

          // get all user in params
          if(params.userIds) {
            return this.db.User.findAll({where: {id: {$in: params.userIds}}});
          } else {
            return Promise.resolve([]);
          }
        })
        .then(users => {
          data.users = users;
          // get all user from mailshimp group
          if(params.mailChimpListId) {
            return this.mailchimp.getMembers(params.mailChimpListId);
          } else {
            return Promise.resolve([]);
          }
        })
        .then(members => {
          if(members) {
            console.log(`members ==== ${JSON.stringify(members)}`);
            let emailAddresses = [];
            members.forEach(member => {
              if(member.status === 'subscribed') {
                emailAddresses.push(member.email_address);
              }
            });
            return this.db.User.findAll({where: {email: {$in: emailAddresses}}});
          }
        })
        .then(users => {
          if(params.isGeneric === 'true') {
            return this.db.PromotionCode.findAll({where: {
              PromotionId: data.promotion.id,
              code: {$like: params.promoCode}
            }}, {transaction: t})
            .then(promoCodes => {
              if(promoCodes.length === 0) {
                // promo code was changed

                // remove all old promo codes
                return this.db.PromotionCode.destroy({where: {
                  PromotionId: data.promotion.id
                }}, {transaction: t})
                .then(() => {
                  // add new generic code
                  let promoCode = params.promoCode;
                  return this.db.PromotionCode.create({
                    PromotionId: data.promotion.id,
                    code: promoCode
                  }, {transaction: t});
                });
              } else {
                // promo code was NOT changed
                return Promise.resolve({});
              }
            });
          } else {
            data.mailChimpUsers = users;

            // merge users and mailchimp users
            data.users = _.union(data.users, data.mailChimpUsers);

            return this.db.PromotionCode.findAll({where: {
              PromotionId: data.promotion.id,
              email: {$in: _.pluck(data.users, 'email')}
            }}, {transaction: t})
            .then(promoCodes => {
              let destroyPromise;

              if(promoCodes.length === 0) {
                // delete all user that removed or generic code
                destroyPromise = this.db.PromotionCode.destroy({where: {
                  PromotionId: data.promotion.id
                }}, {transaction: t});
              } else {
                promoCodes = SequelizeHelper.convertSeque2Json(promoCodes);

                let existedEmails = _.pluck(promoCodes, 'email');
                // filter to get all user that was added new
                data.users = data.users.filter(item => existedEmails.indexOf(item.email) < 0);

                // delete all user that removed or generic code
                destroyPromise = this.db.PromotionCode.destroy({where: {
                  PromotionId: data.promotion.id,
                  email: {$notIn: existedEmails}
                }}, {transaction: t});
              }

              return destroyPromise;
            });
          }
        })
        .then(() => Promise.mapSeries(data.promotion.promotionTranslate, item => {
          let translatePromise;

          if(item.id) {
            translatePromise = this.db.PromotionTranslate.update({
              name: item.name,
              description: item.description,
              langCode: item.langCode,
              PromotionId: data.promotion.id
            }, {where: {id: item.id}});
          } else {
            translatePromise = this.db.PromotionTranslate.create({
              name: item.name,
              description: item.description,
              langCode: item.langCode,
              PromotionId: data.promotion.id
            }, {returning: true});
          }

          return translatePromise.then(translateResult => {
            item.id = item.id ? item.id : translateResult.id;
            return item;
          });
        })
        // destroy all promotionTranslates that removed
        .then(promotionTranslates => this.db.PromotionTranslate.destroy({where: {
          PromotionId: data.promotion.id,
          id: {$notIn: _.pluck(promotionTranslates, 'id')}
        }})))
        .then(() => Promise.map(data.users, user => this.createPromotionAndSendEmail(data.promotion, user)))
      );
    })
    .then(() => res.json({ok: true}))
    .catch(error => {
      if(error.name === 'SequelizeValidationError') {
        return ResponseHelper.responseErrorParam(res, error);
      } else if(error.name === 'SequelizeUniqueConstraintError') {
        return ResponseHelper.responseError(res, error);
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }

  /**
   * GET /admin/promotions/:id
   * get promotion details
   * @param {*} req
   * @param {*} res
   */
  getAdminPromotionDetails(req, res) {
    loggingHelper.log('info', 'getAdminPromotionDetails start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let options = {
      where: {id: req.params.id},
      include: [
        'promotionTranslate', 'Country', 'promotionCodes'
      ]
    };

    let promotionDetails = {};
    this.db.Promotion.findOne(options)
      .then(result => {
        promotionDetails = SequelizeHelper.convertSeque2Json(result);
        // get product country Id
        let subOptions = {include: [{
          model: this.db.Product,
          include: ['productImages', 'productTranslate', 'productCountry', 'productDetails']
        }]};
        if(promotionDetails.productCountryIds && promotionDetails.productCountryIds !== 'all') {
          subOptions.where = {id: {$in: JSON.parse(promotionDetails.productCountryIds)}};
        }
        if(promotionDetails.productCountryIds) {
          return this.db.ProductCountry.findAll(subOptions);
        } else {
          return null;
        }
      })
      .then(result => {
        promotionDetails.productCountries = result;
        // get product country Id
        let subOptions = {include: [{
          model: this.db.Plan,
          include: ['planImages', 'planTranslate', 'planCountry']
        }]};
        if(promotionDetails.planCountryIds && promotionDetails.planCountryIds !== 'all') {
          subOptions.where = {id: {$in: JSON.parse(promotionDetails.planCountryIds)}};
        }
        if(promotionDetails.planCountryIds) {
          return this.db.PlanCountry.findAll(subOptions);
        } else {
          return null;
        }
      })
      .then(result => {
        promotionDetails.planCountries = result;

        if(promotionDetails.promotionCodes) {
          let userOptions = {
            where: {
              email: {$in: _.pluck(promotionDetails.promotionCodes, 'email')}
            }
          };
          return this.db.User.findAll(userOptions);
        }

        return null;
      })
      .then(result => {
        promotionDetails.users = result;
        res.json(promotionDetails);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /promotions/check
   * check promotion code and return discount percent
   * @param {*} req
   * @param {*} res
   */
  checkPromotionCode(req, res) {
    loggingHelper.log('info', 'checkPromotionCode start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    console.dir('Check Promo Code : ' + JSON.stringify(params));
    // if Custom Plan does not exist ---------------------------------------------------
    if (params.getCustomPlan != '') {
      console.dir('Custom Plan Section');
      let identity = req.decoded;
      let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
      if (!params.promotionCode || (!params.productCountryIds && !params.planCountryIds)) {
        return ResponseHelper.responseMissingParam(res);
      }
      this.db.Promotion.findOne({
          where: {
            expiredAt: {
              $gte: moment().format('YYYY-MM-DD')
            },
            CountryId: req.country.id
          },
          include: [{
              model: this.db.PromotionCode,
              as: 'promotionCodes',
              where: {
                code: params.promotionCode,
                $or: [{
                    promotion_type: 1
                  },
                  {
                    promotion_type: null
                  },
                ],
                isValid: true
              }
            },
            'promotionTranslate'
          ]
        })
        .then(promotion => {
          if (!promotion) {
            throw new Error('code_invalid');
          } else if ((!promotion.isOffline && params.userTypeId !== '1') || (!promotion.isOnline && params.userTypeId === '1')) {
            throw new Error('code_invalid');
          } else {
            // check product country ids
            let checkProductCountryIds = false;
            // convert integer array to string array
            if (params.productCountryIds) {
              params.productCountryIds = params.productCountryIds.map(String);
            }
            if (params.planCountryIds) {
              params.planCountryIds = params.planCountryIds.map(String);
            }
            // if(!promotion.productCountryIds && params.productCountryIds && params.productCountryIds.length > 0) {
            //   checkProductCountryIds = false;
            // }
            if (promotion.productCountryIds === 'all' && params.productCountryIds && params.productCountryIds.length > 0) {
              checkProductCountryIds = true;
            } else if (promotion.productCountryIds && promotion.productCountryIds !== 'all') {
              promotion.productCountryIds = JSON.parse(promotion.productCountryIds);
              if (params.productCountryIds) {
                params.productCountryIds.forEach(productCountryId => {
                  if (promotion.productCountryIds.find(id => id === productCountryId)) {
                    checkProductCountryIds = true;
                  }
                });
              }
            }
            // check plan country ids
            let checkPlanCountryIds = false;
            if (promotion.planCountryIds === 'all' && params.planCountryIds && params.planCountryIds.length > 0) {
              checkPlanCountryIds = true;
            } else if (promotion.planCountryIds && promotion.planCountryIds !== 'all') {
              promotion.planCountryIds = JSON.parse(promotion.planCountryIds);
              if (params.planCountryIds) {
                params.planCountryIds.forEach(planCountryId => {
                  if (promotion.planCountryIds.find(id => id === planCountryId)) {
                    checkPlanCountryIds = true;
                  }
                });
              }
            }

            console.log('params.planCountryIds', params.planCountryIds);
            console.log('params.planCountryIds', params.productCountryIds);
            console.log('promotion.productCountryIds', promotion.productCountryIds);
            console.log('checkProductCountryIds', checkProductCountryIds);
            console.log('promotion.planCountryIds', promotion.planCountryIds);
            console.log('checkPlanCountryIds', checkPlanCountryIds);

            if (!checkProductCountryIds && !checkPlanCountryIds) {
              throw new Error('code_invalid');
            } else {
              let intersectionProduct;
              let intersectionPlan;
              let returnObj = SequelizeHelper.convertSeque2Json(promotion);
              if (promotion.productCountryIds && promotion.productCountryIds !== 'all') {
                intersectionProduct = _.intersection(promotion.productCountryIds, params.productCountryIds);
                returnObj.productCountryIds = (intersectionProduct.length > 0) ? intersectionProduct : null;
              }
              if (promotion.productCountryIds && promotion.planCountryIds !== 'all') {
                intersectionPlan = _.intersection(promotion.planCountryIds, params.planCountryIds);
                returnObj.planCountryIds = (intersectionPlan.length > 0) ? intersectionPlan : null;
              }

              // check time per user
              if (promotion.isGeneric) {
                let appliedTo = JSON.parse(promotion.appliedTo);
                // console.log(`check promotion == ${promotion.timePerUser} === ${appliedTo[identity.id]}`);
                if (identity && appliedTo && appliedTo[identity.id] && appliedTo[identity.id] >= promotion.timePerUser) {
                  throw new Error('code_used');
                }
              }

              if (!returnObj.planCountryIds && !returnObj.productCountryIds) {
                throw new Error('code_invalid');
              } else {
                return returnObj;
              }
            }
          }
        })
        .then(promotion => {
          if (promotion.freeProductCountryIds) {
            return this.db.ProductCountry.findAll({
                where: {
                  id: {
                    $in: JSON.parse(promotion.freeProductCountryIds)
                  }
                },
                include: ['Country', {
                  model: this.db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              })
              .then(productCountrys => {
                promotion.freeProductCountries = productCountrys;
                return res.json(promotion);
              });
          } else {
            return res.json(promotion);
          }
        })
        .catch(error => {
          if (error.message === 'code_invalid') {
            return ResponseHelper.responseError(res, config.messages[langCode].promotion.codeInvalid);
          } else if (error.message === 'code_used') {
            return ResponseHelper.responseError(res, config.messages[langCode].promotion.usedLimited);
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
      // if Ala Cart ---------------------------------------------------
    } else {
      console.dir('Ala Cart Section');
      let identity = req.decoded;
      let langCode = ['en', 'ko'].indexOf(req.headers['lang-code'].toLowerCase()) > -1 ? req.headers['lang-code'].toLowerCase() : 'en';
      if (!params.promotionCode || (!params.productCountryIds && !params.planCountryIds)) {
        return ResponseHelper.responseMissingParam(res);
      }
      this.db.Promotion.findOne({
          where: {
            expiredAt: {
              $gte: moment().format('YYYY-MM-DD')
            },
            CountryId: req.country.id
          },
          include: [{
              model: this.db.PromotionCode,
              as: 'promotionCodes',
              where: {
                code: params.promotionCode,
                // promotion_type: {$not: 1},
                $or: [{
                    promotion_type: {
                      $not: 1
                    }
                  },
                  {
                    promotion_type: null
                  },
                ],
                isValid: true
              }
            },
            'promotionTranslate'
          ]
        })
        .then(promotion => {
          if (!promotion) {
            throw new Error('code_invalid');
          } else if ((!promotion.isOffline && params.userTypeId !== '1') || (!promotion.isOnline && params.userTypeId === '1')) {
            throw new Error('code_invalid');
          } else {
            // check product country ids
            let checkProductCountryIds = false;
            // convert integer array to string array
            if (params.productCountryIds) {
              params.productCountryIds = params.productCountryIds.map(String);
            }
            if (params.planCountryIds) {
              params.planCountryIds = params.planCountryIds.map(String);
            }
            // if(!promotion.productCountryIds && params.productCountryIds && params.productCountryIds.length > 0) {
            //   checkProductCountryIds = false;
            // }
            if (promotion.productCountryIds === 'all' && params.productCountryIds && params.productCountryIds.length > 0) {
              checkProductCountryIds = true;
            } else if (promotion.productCountryIds && promotion.productCountryIds !== 'all') {
              promotion.productCountryIds = JSON.parse(promotion.productCountryIds);
              if (params.productCountryIds) {
                params.productCountryIds.forEach(productCountryId => {
                  if (promotion.productCountryIds.find(id => id === productCountryId)) {
                    checkProductCountryIds = true;
                  }
                });
              }
            }
            // check plan country ids
            let checkPlanCountryIds = false;
            if (promotion.planCountryIds === 'all' && params.planCountryIds && params.planCountryIds.length > 0) {
              checkPlanCountryIds = true;
            } else if (promotion.planCountryIds && promotion.planCountryIds !== 'all') {
              promotion.planCountryIds = JSON.parse(promotion.planCountryIds);
              if (params.planCountryIds) {
                params.planCountryIds.forEach(planCountryId => {
                  if (promotion.planCountryIds.find(id => id === planCountryId)) {
                    checkPlanCountryIds = true;
                  }
                });
              }
            }

            console.log('params.planCountryIds', params.planCountryIds);
            console.log('params.planCountryIds', params.productCountryIds);
            console.log('promotion.productCountryIds', promotion.productCountryIds);
            console.log('checkProductCountryIds', checkProductCountryIds);
            console.log('promotion.planCountryIds', promotion.planCountryIds);
            console.log('checkPlanCountryIds', checkPlanCountryIds);

            if (!checkProductCountryIds && !checkPlanCountryIds) {
              throw new Error('code_invalid');
            } else {
              let intersectionProduct;
              let intersectionPlan;
              let returnObj = SequelizeHelper.convertSeque2Json(promotion);
              if (promotion.productCountryIds && promotion.productCountryIds !== 'all') {
                intersectionProduct = _.intersection(promotion.productCountryIds, params.productCountryIds);
                returnObj.productCountryIds = (intersectionProduct.length > 0) ? intersectionProduct : null;
              }
              if (promotion.productCountryIds && promotion.planCountryIds !== 'all') {
                intersectionPlan = _.intersection(promotion.planCountryIds, params.planCountryIds);
                returnObj.planCountryIds = (intersectionPlan.length > 0) ? intersectionPlan : null;
              }

              // check time per user
              if (promotion.isGeneric) {
                let appliedTo = JSON.parse(promotion.appliedTo);
                // console.log(`check promotion == ${promotion.timePerUser} === ${appliedTo[identity.id]}`);
                if (identity && appliedTo && appliedTo[identity.id] && appliedTo[identity.id] >= promotion.timePerUser) {
                  throw new Error('code_used');
                }
              }

              if (!returnObj.planCountryIds && !returnObj.productCountryIds) {
                throw new Error('code_invalid');
              } else {
                return returnObj;
              }
            }
          }
        })
        .then(promotion => {
          if (promotion.freeProductCountryIds) {
            return this.db.ProductCountry.findAll({
                where: {
                  id: {
                    $in: JSON.parse(promotion.freeProductCountryIds)
                  }
                },
                include: ['Country', {
                  model: this.db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              })
              .then(productCountrys => {
                promotion.freeProductCountries = productCountrys;
                return res.json(promotion);
              });
          } else {
            return res.json(promotion);
          }
        })
        .catch(error => {
          if (error.message === 'code_invalid') {
            return ResponseHelper.responseError(res, config.messages[langCode].promotion.codeInvalid);
          } else if (error.message === 'code_used') {
            return ResponseHelper.responseError(res, config.messages[langCode].promotion.usedLimited);
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
    }
  }

  /**
   * GET /promotions/email-group
   *
   * @param {*} req
   * @param {*} res
   */
  getEmailGroup(req, res) {
    loggingHelper.log('info', 'getEmailGroup start');
    this.mailchimp.getLists()
      .then(result => {
        let lists = _.reject(result.lists, (list => list.id === config.email.singleListId));
        return res.json(lists);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  createPromotionAndSendEmail(promotion, user) {
    loggingHelper.log('info', 'createPromotionAndSendEmail start');
    loggingHelper.log('info', `param: promotion = ${promotion}, user = ${user}`);
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let promoCodeObj = {
      PromotionId: promotion.id,
      code: shortId.generate(),
      email: user.email
    };
    return this.db.PromotionCode.create(promoCodeObj, {returning: true})
      .then(promoCode => {
        if(promotion.discount) {
          return sendEmailQueueService.addTask({method: 'sendPromotionEmail', data: {
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            promoCode: promoCode.code,
            discount: `${promotion.discount}%`,
            promotionName: promotion.promotionTranslate[0].name,
            startDate: moment(promotion.activeAt).format('YYYY-MM-DD'),
            expiredAt: moment(promotion.expiredAt).format('YYYY-MM-DD')
          }});
        }
      });
  }

  /**
   * DELETE /admin-promotion/:id
   * @param {*} req
   * @param {*} res
   */
  removeAdminPromotions(req, res) {
    loggingHelper.log('info', 'removeAdminPromotions start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.Promotion.destroy({where: {id: req.params.id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /promotions
   * Create promotion send email to user
   * @param {*} req
   * @param {*} res
   */
  postPromotionCode(req, res) {
    loggingHelper.log('info', 'postPromotionCode start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.image;
    let data = {};

    // validate images
    if(!image) {
      return ResponseHelper.responseMissingParam(res);
    }
    // validate expire date format
    if(!Validator.valiadateDate(params.expiredAt, true)) {
      return ResponseHelper.responseMissingParam(res, 'missing expired at date or  is invalid');
    }
    if(!Validator.valiadateDate(params.activeAt)) {
      return ResponseHelper.responseMissingParam(res, 'Active at date is invalid');
    }
    if(params.isGeneric && params.isGeneric === 'true' && !params.promoCode) {
      return ResponseHelper.responseMissingParam(res, 'Missing promo code');
    }
    if(!params.promotionTranslate) {
      return ResponseHelper.responseMissingParam(res, 'Missing promotion translate');
    }
    if(!params.CountryId) {
      return ResponseHelper.responseMissingParam(res, 'Missing Country Id');
    }
    if(!params.productCountryIds && !params.planCountryIds) {
      return ResponseHelper.responseMissingParam(res, 'You must be choose at least one product or plan for this promotion');
    }
    if(!params.isGeneric && !params.userIds && !params.mailChimpListId) {
      return ResponseHelper.responseMissingParam(res, 'You must be choose at least one user or mailchimp list for this promotion');
    }

    params.productCountryIds = (params.productCountryIds && params.productCountryIds !== 'all') ? JSON.stringify(params.productCountryIds) : params.productCountryIds;
    params.planCountryIds = (params.planCountryIds && params.planCountryIds !== 'all') ? JSON.stringify(params.planCountryIds) : params.planCountryIds;
    params.freeProductCountryIds = (params.freeProductCountryIds && params.freeProductCountryIds !== 'all') ? JSON.stringify(params.freeProductCountryIds) : params.freeProductCountryIds;

    // upload image to S3
    awsHelper.upload(config.AWS.productImagesAlbum, image)
      .then(uploadedImage => {
        params.bannerUrl = uploadedImage.Location;
        return this.db.sequelize.transaction(t => this.db.Promotion.create(params, {include: ['promotionTranslate'], returning: true, transaction: t})
          .then(promotion => {
            data.promotion = SequelizeHelper.convertSeque2Json(promotion);
            console.log(`data.promotion ==== ${JSON.stringify(data.promotion)}`);
            // get all user in params
            if(params.userIds) {
              return this.db.User.findAll({where: {id: {$in: params.userIds}}});
            } else {
              return;
            }
          })
          .then(users => {
            data.users = users;
            // get all user from mailshimp group
            if(params.mailChimpListId) {
              return this.mailchimp.getMembers(params.mailChimpListId);
            } else {
              return;
            }
          })
          .then(members => {
            if(members) {
              console.log(`members ==== ${JSON.stringify(members)}`);
              let emailAddresses = [];
              members.forEach(member => {
                if(member.status === 'subscribed') {
                  emailAddresses.push(member.email_address);
                }
              });
              return this.db.User.findAll({where: {email: {$in: emailAddresses}}});
            }
          })
          .then(users => {
            if(params.isGeneric === 'true') {
              let promoCodeObj = {
                PromotionId: data.promotion.id,
                code: params.promoCode
              };
              return this.db.PromotionCode.create(promoCodeObj, {transaction: t});
            } else {
              data.mailChimpUsers = users;

              // merge users and mailchimp users
              data.users = _.union(data.users, data.mailChimpUsers);
              // generate code and send email
              Promise.map(data.users, user => this.createPromotionAndSendEmail(data.promotion, user));
              return;
            }
          })
        );
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
}

export default function(...args) {
  return new PromotionApi(...args);
}
