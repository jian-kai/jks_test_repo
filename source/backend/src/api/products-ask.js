import Promise from "bluebird";
import _ from "underscore";
import ResponseHelper from "../helpers/response-helper";
import RequestHelper from "../helpers/request-helper";
import SequelizeHelper from "../helpers/sequelize-helper";
import JwtMiddleware from "../middleware/jwt-middleware";
import multipart from "connect-multiparty";
import { awsHelper } from "../helpers/aws-helper";
import config from "../config";
import { loggingHelper } from "../helpers/logging-helper";

let multipartMiddleware = multipart();

class ASKProductsAPI {
  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
    console.log("ASKProductsAPI");
  }

  registerRoute() {
    this.api.get("/products-ask", this.getASKProducts.bind(this));
  }

  /** GET /product-types
   * get all product-types in DB
   */
  getASKProducts(req, res) {
    loggingHelper.log("info", "getASKProducts start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    console.log("[info]", " getASKProducts start");
    // build query from inparams
    let params = req.query || {};
    let options = RequestHelper.buildWhereCondition(
      {},
      params,
      this.db.Product
    );
    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }

    // order by UpdatedAT
    options.order = [
      ["productCountry", "order", "DESC"],
      ["order", "ASC"]
    ];

    // filter active product
    options.where = Object.assign(options.where, { status: "active" });

    if (!params.userTypeId || +params.userTypeId === 1) {
      options.where = Object.assign(options.where, {
        "$productCountry.isOnline$": true
      });
    } else {
      options.where = Object.assign(options.where, {
        "$productCountry.isOffline$": true
      });
    }

    // select product
    this._findProduct(options, countryCode.toUpperCase())
      .then(products => {
        if (params.limit) {
          let offset = params.offset || 0;
          products = products.slice(offset, offset + parseInt(params.limit));
        }
        res.json(products);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  _findProduct(options, countryCode) {
    // build output value
    Object.assign(options, {
      attributes: {
        exclude: ["ProductTypeId"]
      },
      include: this._getProductInclude(countryCode)
    });

    return this.db.Product.findAll(options).then(products => {
      let returnValue = [];
      products.forEach(product => {
        product = SequelizeHelper.convertSeque2Json(product);

        // get unique product country
        if (product.productCountry && product.productCountry.length !== 0) {
          product.productCountry = product.productCountry[0];
          returnValue.push(product);
        }
      });
      return returnValue;
    });
  }

  _getProductInclude(countryCode, getRelated) {
    let include = [
      {
        model: this.db.ProductType,
        as: "ProductType",
        include: ["productTypeTranslate"]
      },
      "productImages",
      "productDetails",
      {
        model: this.db.ProductTranslate,
        as: "productTranslate",
        attributes: {
          exclude: ["ProductId", "createdAt", "updatedAt"]
        }
      }
    ];
    let relatedInclude = {};
    if (getRelated) {
      relatedInclude = {
        model: this.db.ProductRelated,
        as: "productRelateds",
        include: [
          {
            model: this.db.ProductCountry,
            as: "RelatedProductCountry",
            include: [
              {
                model: this.db.Product,
                as: "RelatedProducts",
                include: ["productImages", "productTranslate"]
              }
            ]
          }
        ]
      };
    } else {
      relatedInclude = {
        model: this.db.ProductRelated,
        as: "productRelateds"
      };
    }

    if (countryCode) {
      include.push({
        model: this.db.ProductCountry,
        as: "productCountry",
        attributes: {
          exclude: ["CountryId", "ProductId", "createdAt", "updatedAt"]
        },
        include: [
          {
            model: this.db.Country,
            where: { code: countryCode }
          },
          relatedInclude
        ]
      });
    } else {
      include.push({
        model: this.db.ProductCountry,
        as: "productCountry",
        attributes: {
          exclude: ["CountryId", "ProductId", "createdAt", "updatedAt"]
        },
        include: ["Country", relatedInclude]
      });
    }
    return include;
  }
}

export default function(...args) {
  return new ASKProductsAPI(...args);
}
