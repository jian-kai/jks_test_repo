import Promise from 'bluebird';
import _ from 'underscore';
import multipart from 'connect-multiparty';
import moment from 'moment';
import json2csv from 'json2csv';

import config from '../config';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import { awsHelper } from '../helpers/aws-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import Validator from '../helpers/validator';
import SendEmailQueueService from '../services/send-email.queue.service';
import { loggingHelper } from '../helpers/logging-helper';

let multipartMiddleware = multipart();

class SubscriptionApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/admin-plan-groups', JwtMiddleware.verifyAdminToken, this.getPlanGroupAdmin.bind(this));
    this.api.get('/admin-plan-groups/:id', JwtMiddleware.verifyAdminToken, this.getPlanGroupDetails.bind(this));
    this.api.post('/admin-plan-groups', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.postPlanGroup.bind(this));
    this.api.put('/admin-plan-groups/:id', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.putPlanGroup.bind(this));
    this.api.delete('/admin-plan-groups/:planGroupId/plan-group-translates/:id', JwtMiddleware.verifyAdminToken, this.deletePlanGroupTranslate.bind(this));
    this.api.get('/plan-types', this.getPlanType.bind(this));
    this.api.get('/plan-types/subscription', this.getPlanTypeForSubscription.bind(this));
    this.api.get('/admin-plan-types', this.getPlanTypeAdmin.bind(this));
    this.api.post('/plan-types', JwtMiddleware.verifyAdminToken, this.postPlanType.bind(this));
    this.api.put('/plan-types/:id', JwtMiddleware.verifyAdminToken, this.putPlanType.bind(this));
    this.api.get('/plans', this.getPlan.bind(this));
    this.api.get('/plans/free-trial', this.getPlanFreeTrial.bind(this));
    this.api.get('/plans/free-trial/ba', this.getPlanFreeTrialForBA.bind(this));
    this.api.get('/plans/by-payment-type', this.getPlanByPaymentType.bind(this));
    this.api.get('/plans/:slug', this.getPlanDetail.bind(this));
    this.api.get('/plans/sku/:sku', this.checkPlanSkuExist.bind(this));
    this.api.get('/plans/slug/:slug', this.checkPlanSlugExist.bind(this));
    this.api.get('/admin-plans', JwtMiddleware.verifyAdminToken, this.getPlanAdmin.bind(this));
    this.api.get('/admin-plans/select-list', JwtMiddleware.verifyAdminToken, this.getPlanAdminSelectList.bind(this));
    this.api.post('/plans', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.postPlan.bind(this));
    this.api.post('/plans/change-status', JwtMiddleware.verifyAdminToken, this.postUpdatePlanStatus.bind(this));
    this.api.put('/plans/:id', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.putPlan.bind(this));

    this.api.delete('/plans/plan-images/:id', JwtMiddleware.verifyAdminToken, this.deletePlanImage.bind(this));
    this.api.delete('/plans/plan-country/:id', JwtMiddleware.verifyAdminToken, this.deletePlanCountry.bind(this));
    this.api.delete('/plans/plan-translates/:id', JwtMiddleware.verifyAdminToken, this.deletePlanTranslate.bind(this));
    this.api.delete('/plans/plan-details/:id', JwtMiddleware.verifyAdminToken, this.deletePlanDetail.bind(this));
    this.api.delete('/plans/plan-trial-products/:id', JwtMiddleware.verifyAdminToken, this.deletePlanTrialProduct.bind(this));
    this.api.get('/subscribers/users', JwtMiddleware.verifyAdminToken, this.getUserSubscriber.bind(this));
    this.api.get('/subscribers/users/download', JwtMiddleware.verifyAdminToken, this.downloadSubscriber.bind(this));
    this.api.get('/subscribers/trial/users', JwtMiddleware.verifyAdminToken, this.getUserTrialSubscriber.bind(this));
    this.api.get('/subscribers/trial/users/download', JwtMiddleware.verifyAdminToken, this.downloadTrialSubscriber.bind(this));
    this.api.delete('/subscribers/trial/users/:id', JwtMiddleware.verifyAdminToken, this.cancelTrialSubscription.bind(this));
    this.api.get('/subscribers/sellers', JwtMiddleware.verifyAdminToken, this.getSellerSubscriber.bind(this));
    this.api.get('/subscriptions', JwtMiddleware.verifyAdminToken, this.getSubscription.bind(this));
    this.api.put('/subscriptions/revert/:id', JwtMiddleware.verifyUserToken, this.revertSubscription.bind(this));
    this.api.delete('/subscriptions/:id', JwtMiddleware.verifyUserToken, this.cancelSubscription.bind(this));
    this.api.post('/subscriptions/:id', JwtMiddleware.verifyAdminToken, this.updateSubscription.bind(this));

    // plantype translate
    this.api.get('/plan-types/:id', JwtMiddleware.verifyAdminToken, this.getPlanTypeDetails.bind(this));
    this.api.get('/plan-type-translates/:id', JwtMiddleware.verifyAdminToken, this.getPlanTypeTranslateDetails.bind(this));
    this.api.delete('/plan-types/:planTypeId/plan-type-translates/:id', JwtMiddleware.verifyAdminToken, this.deletePlanTypeTranslate.bind(this));

    // plan Option
    this.api.delete('/plans/plan-option/:id', JwtMiddleware.verifyAdminToken, this.deletePlanOption.bind(this));
    this.api.delete('/plans/plan-option-product/:id', JwtMiddleware.verifyAdminToken, this.deletePlanOptionProduct.bind(this));
    this.api.delete('/plans/plan-option-translate/:id', JwtMiddleware.verifyAdminToken, this.deletePlanOptionTranslate.bind(this));
  }

  _getPlanInclude(countryCode) {
    let include = [
      {
        model: this.db.PlanType,
        as: 'PlanType',
        include: ['planTypeTranslate']
      },
      'planImages',
      { 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      }
    ];
    if(countryCode) {
      include.push({ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: [
          {
            model: this.db.Country,
            where: {code: countryCode}
          }, 
          {
            model: this.db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }, 
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.PlanOption,
            as: 'planOptions',
            include: [
              {
                model: this.db.PlanOptionProduct,
                as: 'planOptionProducts',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }, 'planOptionTranslate'
            ]
          }
        ]
      });
    } else {
      include.push({ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: [
          {
            model: this.db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.PlanOption,
            as: 'planOptions',
            include: [
              {
                model: this.db.PlanOptionProduct,
                as: 'planOptionProducts',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }, 'planOptionTranslate'
            ]
          }
        ]
      });
    }
    return include;
  }

  /**
   * POST /plans
   * @param <plan properties>
   * @param files: plan images
   */
  postPlan(req, res) {
    loggingHelper.log('info', 'postPlan start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let images = req.files.images;

    // validate require params
    if(!params.PlanTypeId || !params.planTranslate || !params.planCountry || !params.images) {
      return ResponseHelper.responseMissingParam(res);
    }

    params.planCountry = Array.isArray(params.planCountry) ? params.planCountry : [params.planCountry];
    params.planCountry.forEach(planCountry => {
      if(!planCountry.planDetails) {
        return ResponseHelper.responseMissingParam(res);
      }
    });

    // check plan image
    params.images = Array.isArray(params.images) ? params.images : [params.images];
    images = _.pluck(req.files.images, 'image');

    if(images.length !== params.images.length) {
      return ResponseHelper.responseMissingParam(res, 'missing images');
    }

    if(params.PlanGroupId === 'null') {
      params.PlanGroupId = null;
    }

    if(params.isTrial === '') {
      params.isTrial = false;
    }

    let planOptionImages = [];
    let uploadedImages = [];
    let uploadedPlanOptionImages = [];
    let planOptionImagesHasImage = [];

    if(req.files.planCountry) {
      req.files.planCountry.forEach(_planCountry => {
        _planCountry.planOptions.forEach(_planOption => {
          _planOption = Array.isArray(_planOption) ? _planOption : [_planOption];
          let tmpImages = _.pluck(_planOption, 'url');
          planOptionImages = planOptionImages.concat(tmpImages);
        });
      });
      planOptionImages.forEach(image => {
        planOptionImagesHasImage.push(image.fieldName.replace(/[^\[]+\[(\d+)\].*?\[(\d+)\].*/g, '$1, $2'));
      });
    }

    // upload image to s3
    console.log('start up load image to S3');
    loggingHelper.log('info', 'start up load image to S3');
    Promise.mapSeries(images, image => awsHelper.upload(config.AWS.planPanelAlbum, image, true))
      .then(_uploadedImages => {
        uploadedImages = _uploadedImages;
        return Promise.mapSeries(planOptionImages, image => awsHelper.upload(config.AWS.productImagesAlbum, image, true));
      })
      .then(_uploadedImages => {
        console.log('finish up load image to S3');
        loggingHelper.log('info', 'finish up load image to S3');
        uploadedPlanOptionImages = _uploadedImages;
        // start transaction
        let plan;
        this.db.sequelize.transaction(t => this.db.Plan.create(params, {returning: true, transaction: t})
          .then(_plan => {
            plan = _plan;
            let planImages = [];
            let planImage;
            // build Plan Option image
            if(planOptionImagesHasImage) {
              planOptionImagesHasImage.forEach((image, idx) => {
                params.planCountry[parseInt(image.split(', ')[0])].planOptions[parseInt(image.split(', ')[1])].url = uploadedPlanOptionImages[idx].Location;
              });
            }
            // build PlanImages data
            params.images.forEach((image, idx) => {
              planImage = {
                PlanId: plan.id,
                url: uploadedImages[idx].Location,
                isDefault: (image.isDefault === 'true')
              };
              planImages.push(planImage);
            });
            
            // insert plan image data
            console.log(`postPlan === ${JSON.stringify(planImages)}`);
            return this.db.PlanImage.bulkCreate(planImages, {transaction: t});
          })
          .then(() => {
            // build plan details object
            params.planCountry.forEach(_planCountry => {
              // _planCountry.savePercent = ((parseFloat(_planCountry.actualPrice) - parseFloat(_planCountry.sellPrice)) / parseFloat(_planCountry.actualPrice)) * 100;
              _planCountry.PlanId = plan.id;
              if(_planCountry.planOptions) {
                console.log('planOptions ========= ============= ============== ', _planCountry.planOptions);
                _planCountry.planOptions.forEach(option => {
                  if(option.planOptionTranslate) {
                    let tmpTranslates = [];
                    option.planOptionTranslate.forEach(opTranslate => {
                      if(opTranslate) {
                        let tmpTranslate = {};
                        tmpTranslate.name = opTranslate['[name]'];
                        tmpTranslate.langCode = opTranslate['[langCode]'];
                        tmpTranslates.push(tmpTranslate);
                      }
                    });
                    option.planOptionTranslate = tmpTranslates;
                  }
                  if(option.planOptionProducts) {
                    console.log('planOptionProducts ========= ============= ============== ', option.planOptionProducts);
                    let tmpProducts = [];
                    option.planOptionProducts.forEach(opProduct => {
                      if(opProduct) {
                        let tmpProduct = {};
                        tmpProduct.qty = opProduct['[qty]'];
                        tmpProduct.ProductCountryId = opProduct['[ProductCountryId]'];
                        tmpProducts.push(tmpProduct);
                      }
                    });
                    option.planOptionProducts = tmpProducts;
                  }
                });
              }
            });
            // insert plan detail
            return Promise.mapSeries(params.planCountry, planCountry => this.db.PlanCountry.create(planCountry, {include: ['planDetails', 'planTrialProducts', {model: this.db.PlanOption, as: 'planOptions', include: ['planOptionProducts']}], returning: true, transaction: t}));
          })
          .then(() => {
            // build plan translate
            params.planTranslate = Array.isArray(params.planTranslate) ? params.planTranslate : [params.planTranslate];
            params.planTranslate.forEach(planTranslate => {
              planTranslate.PlanId = plan.id;
            });
            // insert plan translate
            return this.db.PlanTranslate.bulkCreate(params.planTranslate, {transaction: t});
          }))
        .then(() => res.json({ok: true}))
        .catch(error => {
          if(error.name === 'SequelizeValidationError') {
            return ResponseHelper.responseErrorParam(res, error);
          } else if(error.name === 'SequelizeUniqueConstraintError') {
            // return ResponseHelper.responseError(res, error);
            return ResponseHelper.responseError(res, 'sku or slug is existed');
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
      });
  }

  updatePlanDetail(planDetail, planCountryId, t) {
    loggingHelper.log('info', 'updatePlanDetail start');
    loggingHelper.log('info', `param: planDetail = ${planDetail}, planCountryId = ${planCountryId}`);
    // let planDetailUpdate = [];
    let planDetailCreate = [];
    if(planDetail) {
      planDetail = Array.isArray(planDetail) ? planDetail : [planDetail];
      planDetail.forEach(item => {
        item.PlanCountryId = planCountryId;
        // if(item.id) {
        //   planDetailUpdate.push(item);
        // } else {
        item.PlanCountryId = planCountryId;
        planDetailCreate.push(item);
        // }
      });
      return this.db.PlanDetail.destroy({where: {PlanCountryId: planCountryId}}, {transaction: t})
        // .then(() => SequelizeHelper.bulkUpdate(planDetailUpdate, this.db, 'PlanDetail', {transaction: t}))
        .then(() => this.db.PlanDetail.bulkCreate(planDetailCreate, {transaction: t}));
    } else {
      return;
    }
  }

  updatePlanTrialProduct(planTrialProduct, planCountryId, t) {
    // let planTrialProductUpdate = [];
    let planTrialProductCreate = [];
    if(planTrialProduct) {
      planTrialProduct = Array.isArray(planTrialProduct) ? planTrialProduct : [planTrialProduct];
      planTrialProduct.forEach(item => {
        item.PlanCountryId = planCountryId;
        // if(item.id) {
        //   planTrialProductUpdate.push(item);
        // } else {
        item.PlanCountryId = planCountryId;
        planTrialProductCreate.push(item);
        // }
      });
      if(planTrialProduct) {
        return this.db.PlanTrialProduct.destroy({where: {PlanCountryId: planCountryId}}, {transaction: t})
          // .then(() => SequelizeHelper.bulkUpdate(planTrialProductUpdate, this.db, 'PlanTrialProduct', {transaction: t}))
          .then(() => this.db.PlanTrialProduct.bulkCreate(planTrialProductCreate, {transaction: t}));
      }
    } else {
      return;
    }
  }

  updatePlanOption(planOption, planCountryId, t) {
    let planOptionUpdate = [];
    let planOptionCreate = [];
    if(planOption) {
      planOption = Array.isArray(planOption) ? planOption : [planOption];
      planOption.forEach(item => {
        item.planOptionTranslate = Array.isArray(item.planOptionTranslate) ? item.planOptionTranslate : [item.planOptionTranslate];
        if(item.planOptionTranslate) {
          let tmpTranslates = [];
          item.planOptionTranslate.forEach(translate => {
            if(translate) {
              let tmpTranslate = {};
              if(translate['[id]']) {
                tmpTranslate.id = translate['[id]'];
              }
              tmpTranslate.name = translate['[name]'];
              tmpTranslate.langCode = translate['[langCode]'];
              tmpTranslates.push(tmpTranslate);
            }
          });
          item.planOptionTranslate = tmpTranslates;
        }
        item.planOptionProducts = Array.isArray(item.planOptionProducts) ? item.planOptionProducts : [item.planOptionProducts];
        if(item.planOptionProducts) {
          let tmpProducts = [];
          item.planOptionProducts.forEach(product => {
            if(product) {
              let tmpProduct = {};
              if(product['[id]']) {
                tmpProduct.id = product['[id]'];
              }
              tmpProduct.qty = product['[qty]'];
              tmpProduct.ProductCountryId = product['[ProductCountryId]'];
              tmpProducts.push(tmpProduct);
            }
          });
          item.planOptionProducts = tmpProducts;
        }
        item.PlanCountryId = planCountryId;
        if(item.id) {
          item.planOptionProducts.forEach(product => {
            product.PlanOptionId = item.id;
          });
          planOptionUpdate.push(item);
        } else {
          item.PlanCountryId = planCountryId;
          planOptionCreate.push(item);
        }
      });
      if(planOption) {
        return Promise.map(planOptionUpdate, item => this.db.PlanOption.update(item, {where: {id: item.id}, transaction: t})
          .then(() => this.updatePlanOptionProduct(item.planOptionProducts, item.id, t))
          .then(() => this.updatePlanOptionTranslate(item.planOptionTranslate, item.id, t))
          // update sellPrice for subscription contains the edited shavePlan
          .then(() => this.db.Subscription.update({price: item.sellPrice, pricePerCharge: item.sellPrice}, {where: {PlanCountryId: item.PlanCountryId, PlanOptionId: item.id, status: {$in: ['Processing', 'On Hold']}}, transaction: t})))
          // .then(() => );
          .then(() => SequelizeHelper.bulkCreate(planOptionCreate, this.db, 'PlanOption', {include: ['planOptionProducts'], transaction: t}));
      }
    } else {
      return;
    }
  }

  updatePlanOptionTranslate(planOptionTranslate, planOptionId, t) {
    let planOptionTranslateUpdate = [];
    let planOptionTranslateCreate = [];
    if(planOptionTranslate) {
      console.log('planOptionTranslate === === === ', planOptionTranslate);
      planOptionTranslate = Array.isArray(planOptionTranslate) ? planOptionTranslate : [planOptionTranslate];
      planOptionTranslate.forEach(item => {
        item.PlanOptionId = planOptionId;
        if(item.id) {
          planOptionTranslateUpdate.push(item);
        } else {
          item.PlanOptionId = planOptionId;
          planOptionTranslateCreate.push(item);
        }
      });
      console.log('planOptionTranslateUpdate === === === ', planOptionTranslateUpdate);
      console.log('planOptionTranslateCreate === === === ', planOptionTranslateCreate);
      if(planOptionTranslate) {
        return Promise.map(planOptionTranslateUpdate, item => this.db.PlanOptionTranslate.update(item, {where: {id: item.id}, transaction: t}))
          .then(() => this.db.PlanOptionTranslate.bulkCreate(planOptionTranslateCreate, {transaction: t}));
      }
    } else {
      return;
    }
  }

  updatePlanOptionProduct(planOptionProduct, planOptionId, t) {
    let planOptionProductUpdate = [];
    let planOptionProductCreate = [];
    if(planOptionProduct) {
      planOptionProduct = Array.isArray(planOptionProduct) ? planOptionProduct : [planOptionProduct];
      planOptionProduct.forEach(item => {
        item.PlanOptionId = planOptionId;
        if(item.id) {
          planOptionProductUpdate.push(item);
        } else {
          item.PlanOptionId = planOptionId;
          planOptionProductCreate.push(item);
        }
      });
      if(planOptionProduct) {
        return Promise.map(planOptionProductUpdate, item => this.db.PlanOptionProduct.update(item, {where: {id: item.id}, transaction: t}))
          .then(() => this.db.PlanOptionProduct.bulkCreate(planOptionProductCreate, {transaction: t}));
      }
    } else {
      return;
    }
  }

  updatePlanCountry(planCountry, planId, t) {
    loggingHelper.log('info', 'updatePlanCountry start');
    loggingHelper.log('info', `param: planCountry = ${planCountry}, planId = ${planId}`);
    let planCountryUpdate = [];
    let planCountryCreate = [];
    if(planCountry) {
      planCountry = Array.isArray(planCountry) ? planCountry : [planCountry];
      planCountry.forEach(item => {
        // item.savePercent = ((parseFloat(item.actualPrice) - parseFloat(item.sellPrice)) / parseFloat(item.actualPrice)) * 100;
        if(item.id) {
          planCountryUpdate.push(item);
        } else {
          item.PlanId = planId;
          if(item.planOptions) {
            item.planOptions.forEach(option => {
              if(option.planOptionProducts) {
                let tmpProducts = [];
                option.planOptionProducts.forEach(opProduct => {
                  if(opProduct) {
                    let tmpProduct = {};
                    tmpProduct.qty = opProduct['[qty]'];
                    tmpProduct.ProductCountryId = opProduct['[ProductCountryId]'];
                    tmpProducts.push(tmpProduct);
                  }
                });
                option.planOptionProducts = tmpProducts;
              }
            });
          }
          planCountryCreate.push(item);
        }
      });
      return Promise.map(planCountryUpdate, item => this.db.PlanCountry.update(item, {where: {id: item.id}, transaction: t})
        .then(() => this.updatePlanDetail(item.planDetails, item.id, t))
        .then(() => this.updatePlanTrialProduct(item.planTrialProducts, item.id, t))
        .then(() => this.updatePlanOption(item.planOptions, item.id, t))
        // update sellPrice for subscription contains the edited shavePlan
        .then(() => this.db.Subscription.update({price: item.sellPrice, pricePerCharge: item.sellPrice}, {where: {PlanCountryId: item.id, PlanOptionId: null, status: {$in: ['Processing', 'On Hold']}}, transaction: t})))
        .then(() => SequelizeHelper.bulkCreate(planCountryCreate, this.db, 'PlanCountry', {include: ['planDetails', 'planTrialProducts', {model: this.db.PlanOption, as: 'planOptions', include: ['planOptionProducts']}], transaction: t}));
    } else {
      return;
    }
  }

  /**
   * PUT /plans
   * @param <plan properties>
   * @param files: plan images
   */
  putPlan(req, res) {
    loggingHelper.log('info', 'putPlan start');
    loggingHelper.log('info', `param: req.body = ${req.body}, req.params = ${req.params}`);
    let params = req.body;
    let planId = req.params.id;
    let images = [];
    let planOptionImages = [];
    let imageDefaultId;
    let uploadedImages = [];
    let uploadedPlanOptionImages = [];
    let planImagesHasImage = [];
    let planOptionImagesHasImage = [];
    let planImagesCreate = [];
    let planImagesDelete = [];
    let planImagesUpdate = [];
    let planTranslateUpdates = [];
    let planTranslateCreates = [];

    if(params.PlanGroupId === 'null') {
      params.PlanGroupId = null;
    }

    if(params.isTrial === '') {
      params.isTrial = false;
    }
    // let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check permisstion to update plan
    // if(role !== this.db.Admin.rawAttributes.role.values[0] && params.status === 'removed') {
    //   throw ResponseHelper.responsePermissonDenied(res);
    // }

    // check Length Of PlanDetail images
    if(params.images) {
      params.images = Array.isArray(params.images) ? params.images : [params.images];
      images = _.pluck(req.files.images, 'image');
      images.forEach(image => {
        planImagesHasImage.push(parseInt(image.fieldName.replace(/[^\[]+\[(\d+)\].*/g, '$1')));
      });
    }
    if(req.files.planCountry) {
      req.files.planCountry.forEach(_planCountry => {
        _planCountry.planOptions.forEach(_planOption => {
          _planOption = Array.isArray(_planOption) ? _planOption : [_planOption];
          let tmpImages = _.pluck(_planOption, 'url');
          planOptionImages = planOptionImages.concat(tmpImages);
        });
      });
      planOptionImages.forEach(image => {
        planOptionImagesHasImage.push(image.fieldName.replace(/[^\[]+\[(\d+)\].*?\[(\d+)\].*/g, '$1, $2'));
      });
    }

    console.log('start up load image to S3');
    loggingHelper.log('info', 'start up load image to S3');
    // upload plan image
    Promise.mapSeries(images, image => awsHelper.upload(config.AWS.planImagesAlbum, image, true))
    .then(_uploadedImages => {
      uploadedImages = _uploadedImages;
      return Promise.mapSeries(planOptionImages, image => awsHelper.upload(config.AWS.productImagesAlbum, image, true));
    })
    .then(_uploadedImages => {
      console.log('finish up load image to S3');
      loggingHelper.log('info', 'finish up load image to S3');
      uploadedPlanOptionImages = _uploadedImages;
      // start transaction
      this.db.sequelize.transaction(t => this.db.Plan.update(params, {where: {id: planId}}, {transaction: t})
        .then(() => {
          if(planOptionImagesHasImage) {
            planOptionImagesHasImage.forEach((image, idx) => {
              params.planCountry[parseInt(image.split(', ')[0])].planOptions[parseInt(image.split(', ')[1])].url = uploadedPlanOptionImages[idx].Location;
            });
          }
          if(params.images) {
            params.images.forEach((planImage, idx) => {
              if(planImagesHasImage.indexOf(idx) > -1) {
                planImage.url = uploadedImages[planImagesHasImage.indexOf(idx)].Location;
              }
              // determine which image is removed and which image is create
              if(planImage.isRemoved) {
                planImagesDelete.push(planImage.id);
              } else if(!planImage.id) {
                planImage.PlanId = planId;
                planImagesCreate.push(planImage);
              } else {
                planImagesUpdate.push(planImage);
              }
              // determine which image is default
              if(planImage.isDefault && planImage.isDefault === 'true' && planImage.id) {
                imageDefaultId = planImage.id;
              }
            });
            return this.db.PlanImage.update({isDefault: false}, {where: {PlanId: planId}, transaction: t});
          } else {
            return;
          }
        })
        .then(() => this.db.PlanImage.bulkCreate(planImagesCreate, {transaction: t}))
        .then(() => this.db.PlanImage.destroy({where: {id: {$in: planImagesDelete}}, transaction: t}))
        .then(() => SequelizeHelper.bulkUpdate(planImagesUpdate, this.db, 'PlanImage', {transaction: t}))
        .then(() => {
          if(imageDefaultId) {
            console.log(`imageDefaultId == ${imageDefaultId}`);
            return this.db.PlanImage.update({isDefault: true}, {where: {id: imageDefaultId}, transaction: t});
          } else {
            return;
          }
        })
        .then(() => {
          // determined plan translate update and create
          if(params.planTranslate) {
            params.planTranslate = Array.isArray(params.planTranslate) ? params.planTranslate : [params.planTranslate];
            params.planTranslate.forEach(planTranslate => {
              if(planTranslate.id) {
                planTranslateUpdates.push(planTranslate);
              } else {
                planTranslate.PlanId = planId;
                planTranslateCreates.push(planTranslate);
              }
            });
            return SequelizeHelper.bulkUpdate(planTranslateUpdates, this.db, 'PlanTranslate', {transaction: t});
          } else {
            return;
          }
        })
        .then(() => this.db.PlanTranslate.bulkCreate(planTranslateCreates, {transaction: t}))
        .then(() => this.updatePlanCountry(params.planCountry, planId, t))
        .then(() => res.json({ok: true}))
        .catch(error => {
          if(error.name === 'SequelizeValidationError') {
            return ResponseHelper.responseErrorParam(res, error);
          } else if(error.name === 'SequelizeUniqueConstraintError') {
            // return ResponseHelper.responseError(res, error);
            return ResponseHelper.responseError(res, 'sku or slug is existed');
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        }));
    })
    .catch(() => ResponseHelper.responseError(res, 'Cannot upload image to S3'));
  }

  /**
   * POST /plans/change-status
   * Allow admin change plan status of multiple plans
   * @param {*} req 
   * @param {*} res 
   */
  postUpdatePlanStatus(req, res) {
    loggingHelper.log('info', `postUpdatePlanStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if(!params.planIds || !params.status) {
      return ResponseHelper.responseMissingParam(res, 'Params');
    }

    params.planIds = Array.isArray(params.planIds) ? params.planIds : [params.planIds];

    this.db.Plan.update({status: params.status}, {where: {id: {$in: params.planIds}}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST plan-types
   * Create plan type
   * @param {*} req 
   * @param {*} res 
   */
  postPlanType(req, res) {
    loggingHelper.log('info', 'postPlanType start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    params.planTypeTranslate = params.planTypeTranslate ? params.planTypeTranslate : [];
    
    // validtate input params
    this.db.PlanType.create(params, {
      include: ['planTypeTranslate'],
      returning: true
    })
      .then(planType => res.json(planType))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'Name is existed.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT plan-types/:id
   * Create plan type
   * @param {*} req 
   * @param {*} res 
   */
  putPlanType(req, res) {
    loggingHelper.log('info', 'putPlanType start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validtate input params
    // this.db.PlanType.update(params, {where: {id: req.params.id}, returning: true})

    // init transaction
    let updateTranslates = [];
    let createTranslates = [];
    params.planTypeTranslate = params.planTypeTranslate ? params.planTypeTranslate : [];

    this.db.sequelize.transaction(t => {
      params.planTypeTranslate.forEach(translate => {
        if(translate.id) {
          updateTranslates.push(translate);
        } else {
          translate.ProductTypeId = req.params.id;
          createTranslates.push(translate);
        }
      });
      return SequelizeHelper.bulkUpdate(updateTranslates, this.db, 'PlanTypeTranslate', {transaction: t})
        .then(() => this.db.PlanTypeTranslate.bulkCreate(createTranslates, {returning: true, transaction: t}))
        .then(() => this.db.PlanType.update(params, {where: {id: req.params.id}, returning: true, transaction: t}));
    })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'Name is existed.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
  
  /**
   * GET /plans
   * @param PlanTypeId
   */
  getPlanFreeTrial(req, res) {
    let params = req.query || {};
    let CountryId = req.country.id;
    if(params.CountryId) {
      CountryId = parseInt(params.CountryId);
    }

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    if(options.order) {
      options.order.push(['order', 'DESC'], ['updatedAt', 'DESC']);
    } else {
      options.order = [['order', 'DESC'], ['updatedAt', 'DESC']];
    }

    // console.log(`options.order === ${JSON.stringify(options)}`);

    // filter active product
    options.where = Object.assign(options.where, {status: 'active', isTrial: true});

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }

    // console.log(`options ===== ${JSON.stringify(options)}`);

    let include = [
      {
        model: this.db.PlanType,
        as: 'PlanType',
        include: ['planTypeTranslate']
      },
      'planImages',
      { 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      },
      { 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId },
        include: [
          'Country',
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
            }]
          },
          {
            model: this.db.PlanOption,
            as: 'planOptions',
            include: [
              {
                model: this.db.PlanOptionProduct,
                as: 'planOptionProducts',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }, 'planOptionTranslate'
            ]
          }
        ]
      }
    ];

    this.db.Plan.findAll(options)
      .then(result => {
        console.log(`result == === == = = ${JSON.stringify(result)}`);
        result = SequelizeHelper.convertSeque2Json(result);
        let _options = {
          where: {
            '$Plans.id$': {$in: _.pluck(result, 'id')},
            '$Plans.status$': 'active',
          },
          include: [
            'planGroupTranslate',
            {
              model: this.db.Plan,
              as: 'Plans',
              include
            }
          ],
          order: [['Plans', 'planCountry', 'order', 'DESC'], ['Plans', 'updatedAt', 'DESC']]
        };
        return this.db.PlanGroup.findAll(_options);
      })
      .then(planGroups => {
        planGroups = SequelizeHelper.convertSeque2Json(planGroups);
        planGroups.forEach(planGroup => {
          planGroup.Plans.forEach(plan => {
            plan.planCountry = [_.findWhere(plan.planCountry, {CountryId})];
            // return plan;
            // let sku = plan.sku.replace(/^(.+)-([^-]+)$/g, '$2');
            // return this.db.Plan.findAll({where: {
            //   sku: {$like: `%${sku}`},
            //   status: 'active',
            //   isTrial: true
            // }, include: ['PlanType']})
            //   .then(result => {
            //     let planTypes = [];
            //     result.forEach(obj => {
            //       planTypes.push(obj.PlanType);
            //     });
            //     plan.planTypes = planTypes;
            //     return plan;
            //   });
          });
          // return planGroup;
        });
        res.json(planGroups);
      })
      // .then(result => res.json(result))
      .catch(error => {
        if(error.message === 'invalid_country_code') {
          return ResponseHelper.responseError(res, 'Invalid country code');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
  
  /**
   * GET /plans
   * @param PlanTypeId
   */
  getPlanFreeTrialForBA(req, res) {
    let params = req.query || {};
    let CountryId = req.country.id;

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    if(options.order) {
      options.order.push(['planCountry', 'order', 'DESC'], ['updatedAt', 'DESC']);
    } else {
      options.order = [['planCountry', 'order', 'DESC'], ['updatedAt', 'DESC']];
    }

    // console.log(`options.order === ${JSON.stringify(options)}`);

    // filter active product
    options.where = Object.assign(options.where, {status: 'active', isTrial: true});

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }

    // console.log(`options ===== ${JSON.stringify(options)}`);

    this.db.Plan.findAll(options)
      .then(result => {
        console.log(`result == === == = = ${JSON.stringify(result)}`);
        return Promise.mapSeries(result, plan => {
          let _options = {where: {
            id: plan.id
          }, include: this._getPlanInclude()};
          // console.log(`_getPlanInclude === ${JSON.stringify(this._getPlanInclude())}`);
          return this.db.Plan.findOne(_options);
        });
      })
      .then(plans => Promise.mapSeries(plans, plan => {
        plan = SequelizeHelper.convertSeque2Json(plan);
        // console.log(`result1 == === == = = ${JSON.stringify(plans)}`);
        plan.planCountry = [_.findWhere(plan.planCountry, {CountryId: req.country.id})];
        let sku = plan.sku.replace(/^(.+)-([^-]+)$/g, '$2');
        return this.db.Plan.findAll({where: {
          sku: {$like: `%${sku}`},
          status: 'active',
          isTrial: true
        }, include: ['PlanType']})
          .then(result => {
            let planTypes = [];
            result.forEach(obj => {
              planTypes.push(obj.PlanType);
            });
            plan.planTypes = planTypes;
            return plan;
          });
      }))
      .then(result => res.json(result))
      .catch(error => {
        if(error.message === 'invalid_country_code') {
          return ResponseHelper.responseError(res, 'Invalid country code');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * GET /plans
   * @param PlanTypeId
   */
  getPlan(req, res) {
    loggingHelper.log('info', 'getPlan start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    let CountryId = req.country.id;

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    if(options.order) {
      options.order.push(['order', 'DESC'], ['updatedAt', 'DESC']);
    } else {
      options.order = [['order', 'DESC'], ['updatedAt', 'DESC']];
    }

    console.log(`options.order === ${JSON.stringify(options)}`);

    // filter active product
    options.where = Object.assign(options.where, {status: 'active', isTrial: false});

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }

    console.log(`options ===== ${JSON.stringify(options)}`);

    this.db.Plan.findAll(options)
      .then(result => {
        console.log(`result == === == = = ${JSON.stringify(result)}`);
        result = SequelizeHelper.convertSeque2Json(result);
        return this.db.Plan.findAll({
          where: {id: {$in: _.pluck(result, 'id')}},
          include: this._getPlanInclude()
        });
      })
      .then(plans => Promise.mapSeries(plans, plan => {
        plan = SequelizeHelper.convertSeque2Json(plan);
        console.log(`result1 == === == = = ${JSON.stringify(plans)}`);
        plan.planCountry = [_.findWhere(plan.planCountry, {CountryId: req.country.id})];
        let sku = plan.sku.replace(/^(.+)-([^-]+)$/g, '$2');
        return this.db.Plan.findAll({where: {
          sku: {$like: `%${sku}`},
          status: 'active',
          isTrial: false
        }, include: ['PlanType']})
          .then(result => {
            let planTypes = [];
            result.forEach(obj => {
              planTypes.push(obj.PlanType);
            });
            plan.planTypes = planTypes;
            return plan;
          });
      }))
      .then(result => res.json(result))
      .catch(error => {
        if(error.message === 'invalid_country_code') {
          return ResponseHelper.responseError(res, 'Invalid country code');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * GET /plans/:id
   * get plan detail
   * @param {*} req 
   * @param {*} res 
   */
  getPlanDetail(req, res) {
    loggingHelper.log('info', 'getPlanDetail start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let options = {where: {slug: req.params.slug}};
    let plan;
    options.include = this._getPlanInclude();

    // get plan details
    this.db.Plan.findOne(options)
      .then(result => {
        if(result) {
          plan = SequelizeHelper.convertSeque2Json(result);
          let sku = result.sku.replace(/^(.+)-([^-]+)$/g, '$2');
          return this.db.Plan.findAll({where: {
            sku: {$like: `%${sku}`},
            status: 'active'
          }, include: ['PlanType']});
        } else {
          throw new Error('plan_not_existed');
        }
      })
      .then(plans => {
        let planTypes = [];
        plans.forEach(obj => {
          planTypes.push(obj.PlanType);
        });
        plan.planTypes = planTypes;
      })
      .then(() => res.json(plan))
      .catch(error => {
        if(error.message === 'plan_not_existed') {
          return ResponseHelper.responseNotFoundError(res, this.db.Plan.name);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * GET /plans/sku/:sku
   * get plan detail
   * @param {*} req 
   * @param {*} res 
   */
  checkPlanSkuExist(req, res) {
    loggingHelper.log('info', 'checkPlanSkuExist start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let options = {where: {sku: req.params.sku}};

    // get plan
    this.db.Plan.findOne(options)
      .then(result => res.json(result))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /plans/slug/:slug
   * get plan detail
   * @param {*} req 
   * @param {*} res 
   */
  checkPlanSlugExist(req, res) {
    loggingHelper.log('info', 'checkPlanSlugExist start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let options = {where: {slug: req.params.slug}};

    // get plan
    this.db.Plan.findOne(options)
      .then(result => res.json(result))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /plans/by-payment-type
   * get plan detail by payment type
   * @param {*} req 
   * @param {*} res 
   */
  getPlanByPaymentType(req, res) {
    loggingHelper.log('info', 'getPlanByPaymentType start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let CountryId = req.country.id;
    let plan;

    if(!params.sku) {
      return ResponseHelper.responseMissingParam(res, 'sku is required');
    }
    if(!params.PlanTypeId) {
      return ResponseHelper.responseMissingParam(res, 'plan type id is required');
    }

    let options = {};

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }
    this.db.PlanType.findOne({where: {id: params.PlanTypeId}})
      .then(planType => {
        if(!planType) {
          throw new Error('invalid_plantype');
        } else {
          let whereString = params.sku.replace(/^(.+)-([^-]+)$/g, `${planType.prefix}$2`);
          options.where = {sku: whereString, status: 'active'};
          return this.db.Plan.findOne(options);
        }
      })
      .then(result => {
        if(result) {
          let _options = {where: {
            id: result.id
          }, include: this._getPlanInclude()};
          return this.db.Plan.findOne(_options);
        }
      })
      .then(result => {
        if(result) {
          plan = SequelizeHelper.convertSeque2Json(result);
          let sku = result.sku.replace(/^(.+)-([^-]+)$/g, '$2');
          return this.db.Plan.findAll({where: {
            sku: {$like: `%${sku}`},
            status: 'active'
          }, include: ['PlanType']});
        } else {
          throw new Error('plan_not_existed');
        }
      })
      .then(plans => {
        let planTypes = [];
        plans.forEach(obj => {
          planTypes.push(obj.PlanType);
        });
        plan.planTypes = planTypes;
      })
      .then(() => res.json(plan))
      .catch(error => {
        if(error.message === 'plan_not_existed') {
          return ResponseHelper.responseNotFoundError(res, this.db.Plan.name);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * GET /admin-plans
   * get all plans
   * @param {*} req 
   * @param {*} res 
   */
  getPlanAdmin(req, res) {
    loggingHelper.log('info', 'getPlanAdmin start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    // build limit and off set
    let options = {};
    let returnValue;
    if(params.sku) {
      params.sku = {$like: `%${params.sku}%`};
    }

    options.include = [];
    options.where = {};

    // search function
    if(params.s && params.s !== '') {
      options.distinct = true;

      options.include.push({ 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        duplicating: false,
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      });

      options.where = Object.assign(options.where, {
        $or: [
          {sku: {$like: `%${params.s}%`}},
          {'$planTranslate.name$': {$like: `%${params.s}%`}}
        ]
      });
    }

    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    let planInclude = [
      {
        model: this.db.PlanType,
        as: 'PlanType',
        include: ['planTypeTranslate']
      },
      'planImages',
      'planCountry',
      { 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      }
    ];
    if(!options.order) {
      options.order = ['createdAt'];
    }

    if(!params.CountryId) {
      // find product base on limit and offset
      this.db.Plan.findAndCountAll(options)
      .then(result => {
        returnValue = SequelizeHelper.convertSeque2Json(result);

        return this.db.Plan.findAll({
          where: {id: {$in: _.pluck(returnValue.rows, 'id')}},
          order: options.order,
          include: planInclude,
        });
      })
      .then(result => {
        returnValue.rows = result;
        return res.json(returnValue);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
    } else {
      options.include.push({ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        duplicating: false,
        where: {CountryId: params.CountryId}
      });

      this.db.Plan.findAndCountAll(options)
        .then(result => {
          returnValue = SequelizeHelper.convertSeque2Json(result);

          return this.db.Plan.findAll({
            where: {id: {$in: _.pluck(returnValue.rows, 'id')}},
            order: options.order,
            include: planInclude
          });
        })
        .then(result => {
          returnValue.rows = result;
          return res.json(returnValue);
        })
        .catch(error => ResponseHelper.responseInternalServerError(res, error));
    }
  }

  /**
   * GET /admin-plans/select-list
   * get all plans
   * @param {*} req 
   * @param {*} res 
   */
  getPlanAdminSelectList(req, res) {
    loggingHelper.log('info', 'getPlanAdminSelectList start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    // build limit and off set
    let options = {};
    let returnValue;
    if(params.sku) {
      params.sku = {$like: `%${params.sku}%`};
    }
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    let planInclude = [
      {
        model: this.db.PlanType,
        as: 'PlanType',
        include: ['planTypeTranslate']
      },
      'planImages',
      {
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      }
    ];
    if(!options.order) {
      options.order = ['createdAt'];
    }

    if(!params.CountryId) {
      planInclude.push(
        {
          model: this.db.PlanCountry,
          as: 'planCountry',
          include: ['Country']
        }
      );
      // find product base on limit and offset
      this.db.Plan.findAndCountAll(options)
      .then(result => {
        returnValue = SequelizeHelper.convertSeque2Json(result);
        return this.db.Plan.findAll({
          where: {id: {$in: _.pluck(returnValue.rows, 'id')}},
          order: options.order,
          include: planInclude,
        });
      })
      .then(result => {
        returnValue.rows = result;
        return res.json(returnValue);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: {CountryId: params.CountryId}
      }];
      planInclude.push(
        {
          model: this.db.PlanCountry,
          as: 'planCountry',
          where: {CountryId: params.CountryId},
          include: ['Country']
        }
      );

      this.db.Plan.findAndCountAll(options)
        .then(result => {
          returnValue = SequelizeHelper.convertSeque2Json(result);
          return this.db.Plan.findAll({
            where: {id: {$in: _.pluck(returnValue.rows, 'id')}},
            order: options.order,
            include: planInclude
          });
        })
        .then(result => {
          returnValue.rows = result;
          return res.json(returnValue);
        })
        .catch(error => ResponseHelper.responseInternalServerError(res, error));
    }
  }

  /**
   * GET /plans-type
   */
  getPlanType(req, res) {
    loggingHelper.log('info', 'getPlanType start');
    let planTypes = [];
    this.db.PlanType.findAll({include: [{
      model: this.db.Plan,
      as: 'plans',
      where: {status: 'active'}
    }, 'planTypeTranslate']})
      .then(result => {
        result.forEach(planType => {
          if(planType.plans && planType.plans.length > 0) {
            planTypes.push(planType);
          }
        });
      })
      .then(() => res.json(planTypes))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /plan-types/subscription
   */
  getPlanTypeForSubscription(req, res) {
    loggingHelper.log('info', 'getPlanType start');
    let planTypes = [];
    this.db.PlanType.findAll({include: [{
      model: this.db.Plan,
      as: 'plans',
      where: {status: 'active', isTrial: false}
    }]})
      .then(result => {
        result.forEach(planType => {
          if(planType.plans && planType.plans.length > 0) {
            planTypes.push(planType);
          }
        });
      })
      .then(() => res.json(planTypes))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  getPlanTypeAdmin(req, res) {
    loggingHelper.log('info', 'getPlanTypeAdmin start');
    this.db.PlanType.findAll({
      include: ['planTypeTranslate']
    })
      .then(result => res.json(result))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /plans/plan-images/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanImage(req, res) {
    loggingHelper.log('info', 'deletePlanImage start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanImage.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
  
  /**
   * DELETE /plans/plan-images/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanCountry(req, res) {
    loggingHelper.log('info', 'deletePlanCountry start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanCountry.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /plans/plan-images/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanTranslate(req, res) {
    loggingHelper.log('info', 'deletePlanTranslate start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanTranslate.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /plans/plan-images/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanDetail(req, res) {
    loggingHelper.log('info', 'deletePlanDetail start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanDetail.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /plans/plan-trial-products/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanTrialProduct(req, res) {
    let id = req.params.id;
    this.db.PlanTrialProduct.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /subscription
   * get all subscriber
   * @param {*} req 
   * @param {*} res 
   */
  getUserSubscriber(req, res) {
    loggingHelper.log('info', 'getUserSubscriber start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'From date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'To date is invalid');
    }

    options.include = ['CustomPlan', {
      model: this.db.User,
      include: ['Country'],
    }];

    if(role === 'report') {
      options.where = Object.assign(options.where, {'$User.CountryId$': req.country.id});
    } else if(params.CountryId && params.CountryId !== 'all') {
      options.where = Object.assign(options.where, {'$User.CountryId$': params.CountryId});
    }
    Reflect.deleteProperty(params, 'CountryId');

    let include = [{
      model: this.db.User,
      include: ['Country']
    }, {
      model: this.db.PlanCountry,
      include: [{
        model: this.db.Plan,
        include: ['planImages', 'planTranslate', 'PlanType']
      }]
    },
    {
      model: this.db.CustomPlan,
      include: [
        'Country',
        'PlanType',
        {
          model: this.db.CustomPlanDetail,
          as: 'customPlanDetail',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }
      ]
    }];

    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {'$CustomPlan.description$': {$like: `%${params.s}%`}},
          {'$User.email$': {$like: `%${params.s}%`}}
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.userType === 'sellers') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.userType === 'users') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    options.where = Object.assign(options.where, {isTrial: false});

    console.log(`getUserSubscriber ==== ${JSON.stringify(options)}`);

    SequelizeHelper.findAndCountAll(this.db.Subscription, options, include)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /subscriptions/users/download
   * download subscriber list
   * @param {*} req 
   * @param {*} res 
   */
  downloadSubscriber(req, res) {
    loggingHelper.log('info', 'downloadSubscriber start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'From date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'To date is invalid');
    }

    if(role === 'report') {
      options.where = Object.assign(options.where, {'$User.CountryId$': req.country.id});
    } else if(params.CountryId && params.CountryId !== 'all') {
      options.where = Object.assign(options.where, {'$User.CountryId$': params.CountryId});
    }
    Reflect.deleteProperty(params, 'CountryId');

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    options.include = ['DeliveryAddress', 'BillingAddress', {
      model: this.db.User,
      include: ['Country']
    }, {
      model: this.db.PlanCountry,
      include: [{
        model: this.db.Plan,
        include: ['planImages', 'planTranslate', 'PlanType']
      }]
    }, {
      model: this.db.CustomPlan,
      include: [
        'PlanType',
        {
          model: this.db.CustomPlanDetail,
          as: 'customPlanDetail',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product
            }]
          }]
        }
      ]
    }];

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.userType === 'sellers') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.userType === 'users') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    options.where = Object.assign(options.where, {isTrial: false});

    this.db.Subscription.findAll(options)
      .then(subscriptions => {
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
        // build order data
        let subscriptionTmp = [];
        let tmp = {};
        subscriptions.forEach(subscription => {
          let customSku = '';

          let cancellationReason = '';
          let canceledDate = '';

          if(subscription.CustomPlan) {
            subscription.CustomPlan.customPlanDetail.forEach(detail => {
              customSku += `${detail.ProductCountry.Product.sku}, `;
            });
            customSku = customSku.replace(/^(.*)\,\s$/g, '$1');
          }

          if(subscription.status == 'Canceled') {
            cancellationReason = subscription.cancellationReason;
            canceledDate = moment(subscription.updatedAt).format('YYYY-MM-DD');
          }

          tmp = {
            customerName: subscription.User ? `${subscription.User.firstName ? subscription.User.firstName : ''} ${subscription.User.lastName ? subscription.User.lastName : ''}` : '',
            email: subscription.email,
            country: subscription.User.Country.name,
            sku: subscription.PlanCountry ? subscription.PlanCountry.Plan.sku : '',
            customSku,
            category: subscription.PlanCountry ? subscription.PlanCountry.Plan.PlanType.name : (subscription.CustomPlan ? subscription.CustomPlan.PlanType.name : ''),
            status: subscription.status,
            cancellationReason,
            canceledDate,
            deliveryAddress: `${subscription.DeliveryAddress.address} ${subscription.DeliveryAddress.city} ${subscription.DeliveryAddress.portalCode} ${subscription.DeliveryAddress.state}`,
            billingAddress: `${subscription.BillingAddress.address} ${subscription.BillingAddress.city} ${subscription.BillingAddress.portalCode} ${subscription.BillingAddress.state}`,
            createdAt: moment(subscription.createdAt).format('YYYY-MM-DD'),
            startDeliverDate: subscription.startDeliverDate ? moment(subscription.startDeliverDate).format('YYYY-MM-DD') : 'Pending Delivery Confirmation',
            deliverNum: subscription.currentDeliverNumber,
            updatedAt: moment(subscription.updatedAt).format('YYYY-MM-DD'),
            nextChargeDate: subscription.nextChargeDate ? moment(subscription.nextChargeDate).format('YYYY-MM-DD') : 'Pending Delivery Confirmation'
          };
          subscriptionTmp.push(tmp);
        });
        console.log(`subscriptionTmp ===== ${JSON.stringify(subscriptionTmp)}`);
        // create CSV and response file
        let fields = ['customerName', 'email', 'country', 'sku', 'customSku', 'category', 'status', 'cancellationReason', 'canceledDate', 'deliveryAddress', 'billingAddress', 'createdAt', 'startDeliverDate', 'deliverNum', 'lastDeliverydate', 'nextChargeDate'];
        let fieldNames = ['Customer Name', 'Email', 'Country', 'Shave Plan (SKU)', 'Shave Plan w/o FT (SKU)', 'Category', 'Status', 'Cancellation Reason', 'Canceled Date', 'Delivery Address', 'Billing Address', 'Created At', 'Start Delivery Date', 'No. of Delivery', 'Last Delivered', 'Next Charge'];
        let data = json2csv({
          data: subscriptionTmp,
          fields,
          fieldNames
        });
        // send CSV file
        res.attachment(`Subscripber report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /subscription
   * get all subscriber
   * @param {*} req 
   * @param {*} res 
   */
  getUserTrialSubscriber(req, res) {
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'From date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'To date is invalid');
    }

    options.distinct = true;

    options.include = [{
      model: this.db.User,
      duplicating: false,
      include: [{
        model: this.db.Country,
        duplicating: false,
      }],
    }, {
      model: this.db.PlanCountry,
      duplicating: false,
      include: [{
        model: this.db.Plan,
        duplicating: false,
        include: [{
          model: this.db.PlanTranslate,
          duplicating: false,
          as: 'planTranslate'
        }]
      }]
    }];

    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {'$User.email$': {$like: `%${params.s}%`}},
          {'$PlanCountry.Plan.planTranslate.name$': {$like: `%${params.s}%`}}
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {'$User.CountryId$': decoded.CountryId});
    } else if(params.CountryId && params.CountryId !== 'all') {
      options.where = Object.assign(options.where, {'$User.CountryId$': params.CountryId});
    }
    Reflect.deleteProperty(params, 'CountryId');

    let include = [{
      model: this.db.User,
      include: ['Country']
    }, {
      model: this.db.PlanCountry,
      include: [{
        model: this.db.Plan,
        include: ['planImages', 'planTranslate', 'PlanType']
      }]
    }];

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.userType === 'sellers') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.userType === 'users') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    options.where = Object.assign(options.where, {isTrial: true});

    console.log(`getUserTrialSubscriber ==== ${JSON.stringify(options)}`);

    SequelizeHelper.findAndCountAll(this.db.Subscription, options, include)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /subscriptions/users/download
   * download subscriber list
   * @param {*} req 
   * @param {*} res 
   */
  downloadTrialSubscriber(req, res) {
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'From date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'To date is invalid');
    }

    options.include = [{
      model: this.db.User,
      include: ['Country'],
    }];

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {'$User.CountryId$': req.country.id});
    } else if(params.CountryId && params.CountryId !== 'all') {
      options.where = Object.assign(options.where, {'$User.CountryId$': params.CountryId});
    }
    Reflect.deleteProperty(params, 'CountryId');

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    let include = ['DeliveryAddress', 'BillingAddress', {
      model: this.db.SellerUser,
      include: ['MarketingOffice']
    }, {
      model: this.db.User,
      include: ['Country']
    }, {
      model: this.db.PlanCountry,
      include: [{
        model: this.db.Plan,
        include: ['planImages', 'planTranslate', 'PlanType']
      }]
    }];

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.userType === 'sellers') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.userType === 'users') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }
    
    options.where = Object.assign(options.where, {isTrial: true});

    options.include = include;
    this.db.Subscription.findAll(options)
      .then(subscriptions => {
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
        // build order data
        let subscriptionTmp = [];
        let tmp = {};
        subscriptions.forEach(subscription => {
          let cancellationReason = '';
          let canceledDate = '';

          let badgeId = (subscription.SellerUser) ? subscription.SellerUser.badgeId : '';
          let moCode = (subscription.SellerUser && subscription.SellerUser.MarketingOffice) ? subscription.SellerUser.MarketingOffice.moCode : '';

          if(subscription.status == 'Canceled') {
            cancellationReason = subscription.cancellationReason;
            canceledDate = moment(subscription.updatedAt).format('YYYY-MM-DD');
          }

          tmp = {
            customerName: subscription.User ? `${subscription.User.firstName ? subscription.User.firstName : ''} ${subscription.User.lastName ? subscription.User.lastName : ''}` : '',
            email: subscription.email,
            country: subscription.User.Country.name,
            sku: subscription.PlanCountry.Plan.sku,
            category: subscription.PlanCountry.Plan.PlanType.name,
            moCode,
            badgeId,
            status: subscription.status,
            cancellationReason,
            canceledDate,
            deliveryAddress: `${subscription.DeliveryAddress.address} ${subscription.DeliveryAddress.city} ${subscription.DeliveryAddress.portalCode} ${subscription.DeliveryAddress.state}`,
            billingAddress: `${subscription.BillingAddress.address} ${subscription.BillingAddress.city} ${subscription.BillingAddress.portalCode} ${subscription.BillingAddress.state}`,
            createdAt: moment(subscription.createdAt).format('YYYY-MM-DD'),
            startDeliverDate: subscription.startDeliverDate ? moment(subscription.startDeliverDate).format('YYYY-MM-DD') : 'Pending Delivery Confirmation',
            deliverNum: subscription.currentDeliverNumber,
            updatedAt: moment(subscription.updatedAt).format('YYYY-MM-DD'),
            nextChargeDate: subscription.nextChargeDate ? moment(subscription.nextChargeDate).format('YYYY-MM-DD') : 'Pending Delivery Confirmation'
          };
          subscriptionTmp.push(tmp);
        });
        console.log(`subscriptionTmp ===== ${JSON.stringify(subscriptionTmp)}`);
        // create CSV and response file
        let fields = ['customerName', 'email', 'country', 'sku', 'category', 'moCode', 'badgeId', 'status', 'cancellationReason', 'canceledDate', 'deliveryAddress', 'billingAddress', 'createdAt', 'startDeliverDate', 'deliverNum', 'lastDeliverydate', 'nextChargeDate'];
        let fieldNames = ['Customer Name', 'Email', 'Country', 'Shave Plan (SKU)', 'Category', 'MO Code', 'Badge ID', 'Status', 'Cancellation Reason', 'Canceled Date', 'Delivery Address', 'Billing Address', 'Created At', 'Start Delivery Date', 'No. of Delivery', 'Last Delivered', 'Next Charge'];
        let data = json2csv({
          data: subscriptionTmp,
          fields,
          fieldNames
        });
        // send CSV file
        res.attachment(`Subscripber report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /subscription
   * get all subscriber
   * @param {*} req 
   * @param {*} res 
   */
  getSellerSubscriber(req, res) {
    loggingHelper.log('info', 'getSellerSubscriber start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    if(!decoded.viewAllCountry) {
      options = {
        where: { CountryId: decoded.CountryId },
      };
    }
    options.include = ['Country', {
      model: this.db.Subscription,
      as: 'subscriptions',
      required: true
    }];

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.SellerUser);

    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: new Date(moment(params.fromDate))}},
        {createdAt: {$lt: new Date(moment(params.toDate).add(1, 'day'))}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: new Date(moment(params.fromDate))}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: new Date(moment(params.toDate).add(1, 'day'))}});
    }

    SequelizeHelper.findAndCountAll(this.db.SellerUser, options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /subscription
   * get all subscriber
   * @param {*} req 
   * @param {*} res 
   */
  getSubscription(req, res) {
    loggingHelper.log('info', 'getSubscription start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    if(!decoded.viewAllCountry) {
      options = {
        where: {$or: [{ '$SellerUser.CountryId$': decoded.CountryId }, { '$User.CountryId$': decoded.CountryId }]},
      };
    }

    options.include = ['User', 'SellerUser',
      {
        model: this.db.PlanOption,
        include: [
          {
            model: this.db.PlanOptionProduct,
            as: 'planOptionProducts',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }, 'planOptionTranslate'
        ]
      },
      {
        model: this.db.PlanCountry,
        include: [{
          model: this.db.PlanDetail,
          as: 'planDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        },
        {
          model: this.db.PlanTrialProduct,
          as: 'planTrialProducts',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        },
        {
          model: this.db.Plan,
          include: ['planImages', 'planTranslate', 'PlanType']
        }]
      },
      {
        model: this.db.CustomPlan,
        include: [
          'Country',
          'PlanType',
          {
            model: this.db.CustomPlanDetail,
            as: 'customPlanDetail',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }
        ]
      },
      {
        model: this.db.SubscriptionHistory,
        as: 'subscriptionHistories',
      }
    ];
    options.raws = true;

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);
    this.db.Subscription.findAll(options)
      .then(subscriptions => {
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
        return Promise.mapSeries(subscriptions, subsctiption => {
          let orderOptions = {
            order: ['id'],
            where: {subscriptionIds: {
              $or: [
                {$like: `%\"${subsctiption.id}\"%`},
                {$like: `%[${subsctiption.id}]%`},
                {$like: `%[${subsctiption.id},%`},
                {$like: `%,${subsctiption.id},%`},
                {$like: `%,${subsctiption.id}]%`}
              ]
            }},
            include: [
              {
                model: this.db.OrderDetail,
                as: 'orderDetail',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productTranslate', 'productImages']
                  }]
                }, {
                  model: this.db.PlanCountry,
                  include: [{
                    model: this.db.Plan,
                    include: ['planImages', 'planTranslate']
                  }]
                }]
              },
              {
                model: this.db.Country,
                as: 'Country'
              }
            ]
          };
          return this.db.Order.findAll(orderOptions)
            .then(orders => {
              subsctiption.orders = orders;
              console.log(`subsctiption  ordersordersorders ===== ${JSON.stringify(subsctiption)}`);
              return subsctiption;
            });
        });
      })
      .then(subscriptions => res.json(subscriptions))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));

    // perform query
    // SequelizeHelper.findAndCountAll(this.db.Subscription, options)
    //   .then(result => res.json(result))
    //   .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
  
  cancelSubscription(req, res) {
    // let userTypeId = (req.query.userTypeId === 1 || req.query.userTypeId === '1') ? req.query.userTypeId : 2;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    
    let params = {status: 'Canceled'};

    if(req.query.isUserECommerce) {
      params.cancellationReason = 'Canceled by user from E-commerce site';
    }

    this.db.Subscription.findOne({where: {id: req.params.id}})
      .then(subscription => {
        if(!subscription) {
          throw new Error('not_found');
        } else {
          // if(moment(moment().format('YYYY-MM-DD')).isBefore(subscription.startDeliverDate)) {
          return sendEmailQueueService.addTask({method: 'sendCancelTrialEmail', data: {id: req.params.id}, langCode: req.country.defaultLang.toLowerCase()})
            .then(() => sendEmailQueueService.addTask({method: 'sendCancelTrialRequestEmail', data: {id: req.params.id, isCanceled: true}, langCode: req.country.defaultLang.toLowerCase()}));
          // } else {
          //   params = {status: 'On Hold', hasSendCancelRequest: true};
          //   return sendEmailQueueService.addTask({method: 'sendCancelTrialRequestEmail', data: {id: req.params.id}, langCode: req.country.defaultLang.toLowerCase()});
          // }
        }
      })
      .then(() => this.db.Subscription.update(params, {where: {id: req.params.id}}))
      .then(() => {
        if(params.cancellationReason) {
          this.db.SubscriptionHistory.create({
            message: 'Canceled',
            subscriptionId: req.params.id,
            detail: params.cancellationReason
          });
        }
      })
      .then(() => res.json({ok: true}))
      .catch(err => {
        if(err.message === 'not_found') {
          return ResponseHelper.responseError(res, 'Subscription not found');
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }
  
  revertSubscription(req, res) {
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    this.db.Subscription.findOne({where: {id: req.params.id}})
      .then(subscription => {
        if(!subscription) {
          throw new Error('not_found');
        } else {
          return subscription;
        }
      })
      .then(() => this.db.Subscription.update({status: 'Processing', hasSendCancelRequest: false}, {where: {id: req.params.id}}))
      .then(() => {
        this.db.SubscriptionHistory.create({
          message: 'Processing',
          subscriptionId: req.params.id
        });
      })
      .then(() => sendEmailQueueService.addTask({method: 'sendCancelTrialRequestEmail', data: {id: req.params.id, isUndo: true}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => res.json({ok: true}))
      .catch(err => {
        if(err.message === 'not_found') {
          return ResponseHelper.responseError(res, 'Subscription not found');
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }
  
  cancelTrialSubscription(req, res) {
    let sendEmailQueueService = SendEmailQueueService.getInstance();

    let params = {status: 'Canceled'};

    if(req.query.isUserECommerce) {
      params.cancellationReason = 'Canceled by user from E-commerce site';
    }

    this.db.Subscription.update(params, {where: {id: req.params.id}})
      .then(() => {
        if(params.cancellationReason) {
          this.db.SubscriptionHistory.findOne({
            where: {
              subscriptionId: req.params.id,
              message: 'Canceled'
            },
            order: [
              ['updatedAt', 'DESC']
            ]
          })
          .then(history => {
            if(history) {
              this.db.SubscriptionHistory.update({
                detail: params.cancellationReason
              }, {
                where: {
                  id: history.id
                }
              });
            } else {
              this.db.SubscriptionHistory.create({
                message: 'Canceled',
                subscriptionId: req.params.id,
                detail: params.cancellationReason
              });
            }
          });
        }
      })
      .then(() => sendEmailQueueService.addTask({method: 'sendCancelTrialEmail', data: {id: req.params.id}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => res.json({ok: true}))
      .catch(err => {
        if(err.message === 'order_not_found') {
          return ResponseHelper.responseError(res, 'order_not_found');
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }
  
  updateSubscription(req, res) {
    loggingHelper.log('info', 'updateSubscription start');
    let params = req.body;

    this.db.Subscription.findOne({where: {id: params.id}})
      .then(subscription => {
        if(!subscription) {
          throw new Error('not_found');
        }
      })
      .then(() => this.db.Subscription.update(params, {where: {id: params.id}}))
      .then(() => {
        if(params.cancellationReason) {
          this.db.SubscriptionHistory.findOne({
            where: {
              subscriptionId: params.id,
              message: 'Canceled'
            },
            order: [
              ['updatedAt', 'DESC']
            ]
          })
          .then(history => {
            if(history) {
              this.db.SubscriptionHistory.update({
                detail: params.cancellationReason
              }, {
                where: {
                  id: history.id
                }
              });
            } else {
              this.db.SubscriptionHistory.create({
                message: 'Canceled',
                subscriptionId: params.id,
                detail: params.cancellationReason
              });
            }
          });
        }
      })
      .then(() => res.json({ok: true}))
      .catch(err => {
        if(err.message === 'not_found') {
          return ResponseHelper.responseError(res, 'Subscription not found');
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }

  /**
   * GET /plan-types/:id
   * @param {*} req 
   * @param {*} res 
   */
  getPlanTypeDetails(req, res) {
    loggingHelper.log('info', `getPlanTypeDetails: req.query = ${req.params}`);
    this.db.PlanType.findOne({
      where: {id: req.params.id},
      include: 'planTypeTranslate'
    })
      .then(planType => ResponseHelper.responseSuccess(res, planType))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /plan-type-translates/:id
   * @param {*} req 
   * @param {*} res 
   */
  getPlanTypeTranslateDetails(req, res) {
    loggingHelper.log('info', `getPlanTypeTranslateDetails: req.query = ${req.params}`);
    this.db.PlanTypeTranslate.findOne({where: {id: req.params.id}})
      .then(planTypeTranslate => ResponseHelper.responseSuccess(res, planTypeTranslate))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * DELETE /plan-types/:planTypeId/plan-type-translates/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanTypeTranslate(req, res) {
    loggingHelper.log('info', `deletePlanTypeTranslate: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.PlanTypeTranslate.count({where: {PlanTypeId: req.params.planTypeId}})
      .then(count => {
        if(count <= 1) {
          throw new Error('can_not_delete');
        } else {
          return this.db.PlanTypeTranslate.destroy({where: {id}});
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'can_not_delete') {
          return ResponseHelper.responseError(res, 'can not delete.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
  
  /**
   * DELETE /plans/plan-option/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanOption(req, res) {
    loggingHelper.log('info', 'deletePlanOption start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanOption.destroy({where: {id}})
      .then(() => this.db.PlanOptionProduct.destroy({where: {PlanOptionId: id}}))
      .then(() => this.db.PlanOptionTranslate.destroy({where: {PlanOptionId: id}}))
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
  
  /**
   * DELETE /plans/plan-option-product/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanOptionProduct(req, res) {
    loggingHelper.log('info', 'deletePlanOptionProduct start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanOptionProduct.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
  
  /**
   * DELETE /plans/plan-option-translate/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanOptionTranslate(req, res) {
    loggingHelper.log('info', 'deletePlanOptionTranslate start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.PlanOptionTranslate.destroy({where: {id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET admin-plan-groups
   * get plan group
   * @param {*} req 
   * @param {*} res 
   */
  getPlanGroupAdmin(req, res) {
    loggingHelper.log('info', 'getPlanGroupAdmin start');
    this.db.PlanGroup.findAll({
      include: ['planGroupTranslate']
    })
      .then(result => res.json(result))
      .catch(error => {
        ResponseHelper.responseInternalServerError(res, error);
      });
  }

  /**
   * GET /plan-types/:id
   * @param {*} req 
   * @param {*} res 
   */
  getPlanGroupDetails(req, res) {
    loggingHelper.log('info', 'getPlanGroupDetails start');
    this.db.PlanGroup.findOne({
      where: {id: req.params.id},
      include: ['planGroupTranslate']
    })
      .then(planGroup => ResponseHelper.responseSuccess(res, planGroup))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST admin-plan-groups
   * Create plan group
   * @param {*} req 
   * @param {*} res 
   */
  postPlanGroup(req, res) {
    loggingHelper.log('info', 'postPlanGroup start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.url;
    params.planGroupTranslate = params.planGroupTranslate ? params.planGroupTranslate : [];
    
    // validate images
    if(!image) {
      return ResponseHelper.responseMissingParam(res);
    }

    // upload image to S3
    awsHelper.upload(config.AWS.productImagesAlbum, image)
    .then(uploadedImage => {
      params.url = uploadedImage.Location;
      // validtate input params
      this.db.PlanGroup.create(params, {
        include: ['planGroupTranslate'],
        returning: true
      })
      .then(planGroup => res.json(planGroup));
    })
    .catch(error => {
      if(error.name === 'SequelizeValidationError') {
        return ResponseHelper.responseErrorParam(res, error);
      } else if(error.name === 'SequelizeUniqueConstraintError') {
        return ResponseHelper.responseError(res, 'Name is existed.');
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }

  /**
   * PUT admin-plan-groups/:id
   * Create plan group
   * @param {*} req 
   * @param {*} res 
   */
  putPlanGroup(req, res) {
    loggingHelper.log('info', 'putPlanGroup start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.url;
    // this.db.ProductType.update(params, {where: {id: req.params.id}, returning: true})

    // validate images
    // if(!image) {
    //   return ResponseHelper.responseMissingParam(res);
    // }

    // init transaction
    let updateTranslates = [];
    let createTranslates = [];
    params.planGroupTranslate = params.planGroupTranslate ? params.planGroupTranslate : [];

    // upload product image
    if(image) {
      awsHelper.upload(config.AWS.planImagesAlbum, image)
      .then(uploadedImage => {
        params.url = uploadedImage.Location;
        // start transaction
        this.db.sequelize.transaction(t => {
          params.planGroupTranslate.forEach(translate => {
            if(translate.id) {
              updateTranslates.push(translate);
            } else {
              translate.PlanGroupId = req.params.id;
              createTranslates.push(translate);
            }
          });
          return SequelizeHelper.bulkUpdate(updateTranslates, this.db, 'PlanGroupTranslate', {transaction: t})
            .then(() => this.db.PlanGroupTranslate.bulkCreate(createTranslates, {returning: true, transaction: t}))
            .then(() => this.db.PlanGroup.update(params, {where: {id: req.params.id}, returning: true, transaction: t}));
        })
        .then(() => ResponseHelper.responseSuccess(res))
        .catch(error => {
          if(error.name === 'SequelizeValidationError') {
            return ResponseHelper.responseErrorParam(res, error);
          } else if(error.name === 'SequelizeUniqueConstraintError') {
            return ResponseHelper.responseError(res, 'Name is existed.');
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
      })
      .catch(() => ResponseHelper.responseError(res, 'Cannot upload image to S3'));
    } else {
      // start transaction
      this.db.sequelize.transaction(t => {
        params.planGroupTranslate.forEach(translate => {
          if(translate.id) {
            updateTranslates.push(translate);
          } else {
            translate.PlanGroupId = req.params.id;
            createTranslates.push(translate);
          }
        });
        return SequelizeHelper.bulkUpdate(updateTranslates, this.db, 'PlanGroupTranslate', {transaction: t})
          .then(() => this.db.PlanGroupTranslate.bulkCreate(createTranslates, {returning: true, transaction: t}))
          .then(() => this.db.PlanGroup.update(params, {where: {id: req.params.id}, returning: true, transaction: t}));
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'Name is existed.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
    }
  }

  /**
   * DELETE /admin-plan-groups/:planGroupId/plan-group-translates/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePlanGroupTranslate(req, res) {
    loggingHelper.log('info', `deletePlanGroupTranslate: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.PlanGroupTranslate.count({where: {PlanGroupId: req.params.planGroupId}})
      .then(count => {
        if(count <= 1) {
          throw new Error('can_not_delete');
        } else {
          return this.db.PlanGroupTranslate.destroy({where: {id}});
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'can_not_delete') {
          return ResponseHelper.responseError(res, 'can not delete.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
}

export default function(...args) {
  return new SubscriptionApi(...args);
}
