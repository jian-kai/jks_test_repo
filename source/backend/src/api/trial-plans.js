import Promise from 'bluebird';
import _ from 'underscore';
import config from '../config';
import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import SendEmailQueueService from '../services/send-email.queue.service';
import RequestHelper from '../helpers/request-helper';
import { loggingHelper } from '../helpers/logging-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import SubscriptionHelper from '../helpers/subscription-helper';

class TrialPlanApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/trial-plans', JwtMiddleware.verifyUserToken, this.getTrialPlan.bind(this));
    this.api.get('/trial-plans/groups', this.getTrialPlanGroups.bind(this));
    this.api.get('/trial-plans/groups/:id', this.getTrialPlanByGroupId.bind(this));
    this.api.put('/users/:userId/trial-plan/:subscriptionId', JwtMiddleware.verifyUserToken, this.updateTrialPlanDetails.bind(this));
  }

  _getPlanInclude(countryCode) {
    let include = [
      'PlanType',
      'planImages',
      { 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      }
    ];
    if(countryCode) {
      include.push({ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: ['Country', {
          model: this.db.Country,
          where: {code: countryCode}
        }, {
          model: this.db.PlanDetail,
          as: 'planDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }, {
          model: this.db.PlanTrialProduct,
          as: 'planTrialProducts',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }]
      });
    } else {
      include.push({ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: ['Country', {
          model: this.db.PlanDetail,
          as: 'planDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }, {
          model: this.db.PlanTrialProduct,
          as: 'planTrialProducts',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }]
      });
    }
    return include;
  }

  /**
   * GET /trial-plans/groups
   * @param PlanTypeId
   */
  getTrialPlanGroups(req, res) {
    loggingHelper.log('info', 'getTrialPlanGroups start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    let CountryId = req.country.id;
    if(params.CountryId) {
      CountryId = parseInt(params.CountryId);
    }

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    if(options.order) {
      options.order.push(['order', 'DESC'], ['updatedAt', 'DESC']);
    } else {
      options.order = [['order', 'DESC'], ['updatedAt', 'DESC']];
    }

    // filter active product
    options.where = Object.assign(options.where, {status: 'active', isTrial: true});

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }

    this.db.Plan.findAll(options)
      .then(result => {
        // console.log(`result == === == = = ${JSON.stringify(result)}`);
        result = SequelizeHelper.convertSeque2Json(result);
        let _options = {
          where: {
            '$Plans.id$': {$in: _.pluck(result, 'id')},
            '$Plans.status$': 'active',
          },
          include: [
            'planGroupTranslate',
            {
              model: this.db.Plan,
              as: 'Plans',
              // include
            }
          ],
          order: [['Plans', 'order', 'DESC'], ['Plans', 'updatedAt', 'DESC']]
        };
        return this.db.PlanGroup.findAll(_options);
      })
      .then(planGroups => {
        planGroups = SequelizeHelper.convertSeque2Json(planGroups);
        planGroups.forEach(planGroup => {
        //   planGroup.Plans.forEach(plan => {
        //     plan.planCountry = [_.findWhere(plan.planCountry, {CountryId})];
        //   });
        //   // return planGroup;
          Reflect.deleteProperty(planGroup, 'Plans');
        });
        res.json(planGroups);
      })
      // .then(result => res.json(result))
      .catch(error => {
        if(error.message === 'invalid_country_code') {
          return ResponseHelper.responseError(res, 'Invalid country code');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * GET /trial-plans/groups/:id
   * @param PlanTypeId
   */
  getTrialPlanByGroupId(req, res) {
    loggingHelper.log('info', 'getTrialPlanByGroupId start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    let CountryId = req.country.id;
    if(params.CountryId) {
      CountryId = parseInt(params.CountryId);
    }

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Plan);

    if(options.order) {
      options.order.push(['order', 'DESC'], ['updatedAt', 'DESC']);
    } else {
      options.order = [['order', 'DESC'], ['updatedAt', 'DESC']];
    }

    // filter active product
    options.where = Object.assign(options.where, {status: 'active', isTrial: true});

    if(!params.userTypeId || +params.userTypeId === 1) {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOnline: true }
      }];
    } else {
      options.include = [{ 
        model: this.db.PlanCountry,
        as: 'planCountry',
        where: { CountryId, isOffline: true }
      }];
    }

    let include = [
      {
        model: this.db.PlanType,
        as: 'PlanType',
        include: ['planTypeTranslate']
      },
      'planImages',
      { 
        model: this.db.PlanTranslate,
        as: 'planTranslate',
        attributes: {
          exclude: ['PlanId', 'createdAt', 'updatedAt']
        }
      },
      { 
        model: this.db.PlanCountry,
        as: 'planCountry',
        include: [
          'Country',
          {
            model: this.db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: this.db.ProductCountry,
            }]
          },
          {
            model: this.db.PlanOption,
            as: 'planOptions',
            include: [
              {
                model: this.db.PlanOptionProduct,
                as: 'planOptionProducts',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }, 'planOptionTranslate'
            ]
          }
        ]
      }
    ];

    this.db.Plan.findAll(options)
      .then(result => {
        result = SequelizeHelper.convertSeque2Json(result);
        let _options = {
          where: {
            'id': req.params.id,
            '$Plans.id$': {$in: _.pluck(result, 'id')},
            '$Plans.status$': 'active',
          },
          include: [
            'planGroupTranslate',
            {
              model: this.db.Plan,
              as: 'Plans',
              include
            }
          ],
          order: [['Plans', 'order', 'DESC'], ['Plans', 'updatedAt', 'DESC']]
        };
        return this.db.PlanGroup.findAll(_options);
      })
      .then(planGroups => {
        planGroups = SequelizeHelper.convertSeque2Json(planGroups);
        planGroups.forEach(planGroup => {
          planGroup.Plans.forEach(plan => {
            plan.planCountry = [_.findWhere(plan.planCountry, {CountryId})];
          });
          // return planGroup;
        });
        res.json(planGroups);
      })
      .catch(error => {
        if(error.message === 'invalid_country_code') {
          return ResponseHelper.responseError(res, 'Invalid country code');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /sellers/check-email
   * check existing user and return delivery address and billing address
   * @param {*} req 
   * @param {*} res 
   */
  getTrialPlan(req, res) {
    let params = req.body;
    let data = {};
    if(!params.email) {
      return ResponseHelper.responseMissingParam(res, 'missing email');
    }
    this.db.User.findOne({where: {email: params.email}})
      .then(user => {
        if(user) {
          data.user = user;
          return this.db.DeliveryAddress.findOne({where: {id: user.defaultShipping}});
        }
      })
      .then(deliveryAddress => {
        if(data.user) {
          data.deliveryAddress = deliveryAddress;
          return this.db.DeliveryAddress.findOne({where: {id: data.user.defaultBilling}});
        }
      })
      .then(billingAddress => {
        if(data.user) {
          data.billingAddress = billingAddress;
          return this.db.Subscription.findAll({where: { UserId: data.user.id }});
        }
      })
      .then(subscriptions => {
        data.subscriptions = subscriptions;
        return ResponseHelper.responseSuccess(res, data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * PUT /users/:userId/trial-plan/:orderId
   * update subscription details for trial plan
   * @param {*} req 
   * @param {*} res 
   */
  updateTrialPlanDetails(req, res) {
    let params = req.body;
    let options = {};
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    
    // check input data
    if(!params.planCountryId || !req.params.subscriptionId) {
      return ResponseHelper.responseMissingParam(res);
    }

    this.db.PlanCountry.findOne({where: {id: params.planCountryId}, include: ['Country', 'planOptions', {
      model: this.db.Plan,
      where: {isTrial: true},
      include: ['PlanType']
    }]})
    .then(planCountry => {
      if(!planCountry) {
        throw new Error('planCountry_not_found');
      }
      
      options = {
        PlanCountryId: params.planCountryId,
        price: planCountry.sellPrice,
        pricePerCharge: planCountry.pricePerCharge,
      };
      if(params.planOptionId) {
        options = {
          PlanCountryId: params.planCountryId,
          PlanOptionId: params.planOptionId,
          price: planCountry.planOptions.find(value => value.id === params.planOptionId).sellPrice,
          pricePerCharge: planCountry.planOptions.find(value => value.id === params.planOptionId).pricePerCharge,
        };
      }

      return this.db.Subscription.findOne({where: {id: req.params.subscriptionId, UserId: req.params.userId}});
    })
    .then(subscription => {
      if(!subscription) {
        throw new Error('subscription_not_found');
      }

      return this.db.Subscription.update(options, {where: {id: req.params.subscriptionId}})
      .then(() => 
        SubscriptionHelper.getDiffInfoWhenUpdate(subscription, options, this.db)
        .then(detail => {
          if(detail) {
            this.db.SubscriptionHistory.create({
              message: 'User Update',
              subscriptionId: req.params.subscriptionId,
              detail
            });
          }
        })
      );
    })
    .then(() => sendEmailQueueService.addTask({method: 'sendUpdateTrialEmail', data: {id: req.params.subscriptionId}, langCode: req.country.defaultLang.toLowerCase()}))
    .then(() => ResponseHelper.responseSuccess(res))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new TrialPlanApi(...args);
}
