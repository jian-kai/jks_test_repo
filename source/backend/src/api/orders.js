import Promise from 'bluebird';
import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import SequelizeHelper from '../helpers/sequelize-helper';
import CancelOrderQueueService from '../services/cancel-order.queue.service';
import OrderInboundQueueService from '../services/create-order-inbound.service';
import SendEmailQueueService from '../services/send-email.queue.service';
import CompressedFileToDownloadQueueService from '../services/create-compressed-file-to-download.queue.service';
import Validator from '../helpers/validator';
import OrderHelper from '../helpers/order-helper';
import _ from 'underscore';
import json2csv from 'json2csv';
import moment from 'moment';
import config from '../config';
import { loggingHelper } from '../helpers/logging-helper';
import DateTimeHelper from '../helpers/datetime-helpers';

class OrderApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    // User APIs
    this.api.get('/users/:userId/orders', JwtMiddleware.verifyUserToken, this.getOrdersUser.bind(this));
    this.api.get('/users/orders/:orderId', this.getGuestOrderDetails.bind(this));
    this.api.get('/users/:userId/orders/:orderId', JwtMiddleware.verifyUserToken, this.getOrderDetailsUser.bind(this));
    this.api.delete('/orders/:id', JwtMiddleware.verifyUserToken, this.cancelOrder.bind(this));

    // Seller APIs
    this.api.get('/sellers/:sellerId/orders', JwtMiddleware.verifyUserToken, this.getOrdersSeller.bind(this));
    this.api.get('/sellers/:sellerId/orders/:orderId', JwtMiddleware.verifyUserToken, this.getOrderDetailsSeller.bind(this));

    // Admin APIs
    this.api.get('/orders', JwtMiddleware.verifyAdminToken, this.getOrdersAdmin.bind(this));
    this.api.get('/orders/states', JwtMiddleware.verifyAdminToken, this.getOrderStates.bind(this));
    this.api.get('/orders/pending', JwtMiddleware.verifyAdminToken, this.getPendingOrders.bind(this));
    this.api.get('/orders/history', JwtMiddleware.verifyAdminToken, this.getHistoryOrders.bind(this));
    this.api.get('/orders/all', JwtMiddleware.verifyAdminToken, this.getAllOrders.bind(this));
    this.api.get('/orders/download', JwtMiddleware.verifyAdminToken, this.downloadOrderHistory.bind(this));
    this.api.get('/orders/download/tax-invoice', JwtMiddleware.verifyAdminToken, this.downloadOrderTaxInvoice.bind(this));
    this.api.put('/orders/:id', JwtMiddleware.verifyAdminToken, this.updateOrder.bind(this));
    this.api.get('/orders/:id', JwtMiddleware.verifyAdminToken, this.getOrderDetailsAdmin.bind(this));
    this.api.delete('/admin-orders/:id', JwtMiddleware.verifyAdminToken, this.cancelOrder.bind(this));
    this.api.post('/orders/:id/remarks', JwtMiddleware.verifyAdminToken, this.postOrderRemarks.bind(this));
    this.api.put('/orders/:id/remarks/:historyId', JwtMiddleware.verifyAdminToken, this.editOrderRemarks.bind(this));
    this.api.delete('/orders/:id/remarks/:historyId', JwtMiddleware.verifyAdminToken, this.deleteOrderRemarks.bind(this));
  }

  /**
   * get all associate fields of order
   */
  _getOrderInclude(long) {
    let includes = [
      'User',
      'SellerUser',
      'Receipt',
      'Country',
      'orderHistories',
      {
        model: this.db.OrderDetail,
        as: 'orderDetail',
        include: [
          {
            model: this.db.ProductCountry,
            include: [
              'Country',
              {
                model: this.db.Product,
                include: ['productTranslate', 'productImages']
              }
            ]
          },
          {
            model: this.db.PlanCountry,
            include: [
              'Country',
              {
                model: this.db.Plan,
                include: ['PlanType', 'planTranslate', 'planImages']
              }, {
                model: this.db.PlanDetail,
                as: 'planDetails',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }
            ]
          }
        ]
      }
    ];
    if(long) {
      includes.push('DeliveryAddress');
      includes.push('BillingAddress');
    }
    return includes;
  }

  /** GET /users/:userId/orders
   * Get order
   */
  getOrdersUser(req, res) {
    loggingHelper.log('info', 'getOrdersUser start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let data;
    let options = {where: {
      UserId: req.params.userId
    }, include: [
      {
        model: this.db.Receipt,
        where: {
          id: {$not: null},
          SubscriptionId: null
        }
      }
    ]};

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    // perform query
    this.db.Order.findAndCountAll(options)
    .then(result => {
      data = result;
      let orders = SequelizeHelper.convertSeque2Json(result.rows);
      let ids = _.pluck(orders, 'id');
      return this.db.Order.findAll({
        where: {id: {$in: ids}},
        order: options.order,
        include: this._getOrderInclude()
      });
    })
    .then(orders => {
      data.rows = orders;
      return res.json(data);
    })
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/orders
   * Get order
   */
  getOrdersSeller(req, res) {
    loggingHelper.log('info', 'getOrdersSeller start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let data;
    let options = {where: {
      SellerUserId: req.params.sellerId,
      isSubsequentOrder: false
      // createdAt: {$gte: new Date(moment().subtract(6, 'days').format('YYYY-MM-DD'))}
    }};

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    // perform query
    this.db.Order.findAndCountAll(options)
    .then(result => {
      data = result;
      let orders = SequelizeHelper.convertSeque2Json(result.rows);
      let ids = _.pluck(orders, 'id');
      return this.db.Order.findAll({
        where: {id: {$in: ids}},
        order: options.order,
        include: this._getOrderInclude()
      });
    })
    .then(orders => {
      data.rows = orders;
      return res.json(data);
    })
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /users/orders/:orderId
   * get order detail of guest
   * @param {*} req 
   * @param {*} res 
   */
  getGuestOrderDetails(req, res) {
    let orderResults;
    loggingHelper.log('info', 'getGuestOrderDetails start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.Order.findOne({where: {
      id: req.params.orderId,
    }, include: this._getOrderInclude(true)})
    .then(order => {
      orderResults = SequelizeHelper.convertSeque2Json(order);
      if(order.subscriptionIds) {
        return this.db.Subscription.findAll({where: {
          id: {$in: JSON.parse(order.subscriptionIds)}}
        });
      } else {
        return;
      }
    })
    .then(subscriptions => {
      orderResults.Subscriptions = subscriptions;
    })
    .then(() => res.json(orderResults))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/orders/:orderId
   * Get order detail
   */
  getOrderDetailsUser(req, res) {
    let orderResults;
    loggingHelper.log('info', 'getOrderDetailsUser start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.Order.findOne({where: {
      id: req.params.orderId,
      UserId: req.params.userId
    }, include: this._getOrderInclude(true)})
    .then(order => {
      orderResults = SequelizeHelper.convertSeque2Json(order);
      if(order && order.subscriptionIds) {
        return this.db.Subscription.findAll({where: {
          id: {$in: JSON.parse(order.subscriptionIds)}}
        });
      } else {
        return;
      }
    })
    .then(subscriptions => {
      if(orderResults) {
        orderResults.Subscriptions = subscriptions;
      } else {
        throw new Error('not_found');
      }
    })
    .then(() => res.json(orderResults))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/orders/:orderId
   * Get order detail
   */
  getOrderDetailsSeller(req, res) {
    loggingHelper.log('info', 'getOrderDetailsSeller start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let order;
    this.db.Order.findOne({where: {
      id: req.params.orderId,
      SellerUserId: req.params.sellerId
    }, include: this._getOrderInclude(true)})
    .then(_order => {
      order = SequelizeHelper.convertSeque2Json(_order);
      return this.db.Order.count({where: {
        email: order.email,
        createdAt: {
          $lt: order.createdAt
        }
      }});
    })
    .then(result => {
      order.isNewCustomer = true;
      if(result > 0) {
        order.isNewCustomer = false;
      }
      if(order.subscriptionIds) {
        return this.db.Subscription.findAll({where: {
          id: {$in: JSON.parse(order.subscriptionIds)}}
        });
      } else {
        return;
      }
    })
    .then(subscriptions => {
      order.Subscriptions = subscriptions;
    })
    .then(() => res.json(order))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /orders
   * get all orders in DB
   */
  getOrdersAdmin(req, res) {
    loggingHelper.log('info', 'getOrdersAdmin start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    let options = {include: ['Receipt']};
    if(!decoded.viewAllCountry) {
      options.where = {
        $or: [
          {'$SellerUser.CountryId$': decoded.CountryId},
          {'$User.CountryId$': decoded.CountryId}
        ]
      };
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    this.db.Order.findAndCountAll(options)
      .then(orders => res.json(orders))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /orders
   * get all orders in DB
   */
  getOrderStates(req, res) {
    loggingHelper.log('info', 'getOrderStates start');
    res.json(this.db.Order.rawAttributes.states.values);
  }

  /** GET /orders/:id
   * get all orders in DB
   */
  getOrderDetailsAdmin(req, res) {
    loggingHelper.log('info', 'getOrderDetailsAdmin start');
    let options = {
      include: this._getOrderInclude(true)};

    this.db.Order.findById(req.params.id, options)
      .then(order => res.json(order))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * PUT /orders/:id
   * update order
   */
  updateOrder(req, res) {
    loggingHelper.log('info', 'updateOrder start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let oldOrder;
    this.db.Order.findOne({where: {id: req.params.id}, include: ['Country']})
      .then(order => {
        oldOrder = SequelizeHelper.convertSeque2Json(order);
        if(order && params.states !== 'Processing') {
          params = Object.assign(oldOrder, params);
          return this.db.Order.update(params, {where: {
            id: req.params.id
          }, returning: true})
          .then(() => {
            if(params.states === 'Completed') {
              // update subscription start delivery date
              let arrPatt = new RegExp(/\[([^,]+,?)+\]/g);
              let startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.startFirstDelivery);
              if(order.subscriptionIds && arrPatt.test(order.subscriptionIds)) {
                return this.db.Subscription.update(
                  {
                    nextDeliverDate: startDeliverDate,
                    nextChargeDate: startDeliverDate,
                    startDeliverDate,
                  },
                  {
                    where: {
                      id: {$in: JSON.parse(order.subscriptionIds)},
                      startDeliverDate: null
                    }
                  }
                )
                .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()}));
              } else {
                return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
              }
            } else {
              return Promise.resolve();
            }
          });
        } else if(params.states === 'Processing' && order && order.states === 'Payment Received') {
          // if(order.subscriptionIds) {
          //   return this.db.Subscription.findAll({where: {
          //     id: {$in: JSON.parse(order.subscriptionIds)}}
          //   })
          //   .then(subscriptions => {
          //     subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
          //     if(subscriptions[0].isTrial) {
          //       return orderInboundQueueService.addTask(order.id, order.CountryId)
          //         .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id, langCode: order.Country.defaultLang.toLowerCase()}}));
          //     } else {
          //       return orderInboundQueueService.addTask(order.id, order.CountryId)
          //         .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()}));
          //     }
          //   });
          // } else {
          //   return orderInboundQueueService.addTask(order.id, order.CountryId)
          //     .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()}));
          // }
          if(order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
            return orderInboundQueueService.addTask(order.id, order.CountryId)
              .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()}));
          } else {
            return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
          }
        } else if(!order) {
          throw new Error('not_found');
        }
      })
      .then(() => {
        if(params.states && params.states !== '' && oldOrder && oldOrder.states !== params.states && params.states !== 'Processing') {
          return this.db.OrderHistory.create({
            OrderId: oldOrder.id,
            message: params.states
          });
        }
      })
      .then(() => this.db.Order.findOne({where: {id: req.params.id}, include: this._getOrderInclude()}))
      .then(order => res.json(order))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'email is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** GET /orders
   * get all orders in DB
   */
  getPendingOrders(req, res) {
    loggingHelper.log('info', 'getPendingOrders start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let options = {where: {}};
    let params = req.query;
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    
    options.include = [{
      model: this.db.User,
      duplicating: false
    }];

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'to date is invalid');
    }
    
    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where, {'$User.email$': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    // filter by states
    if(params.states && params.states !== '' && params.status !== 'all') {
      options.where = Object.assign(options.where, {
        states: {
          $and: [
            {$notIn: ['Completed', 'Canceled', 'On Hold']},
            {$like: `${params.states}`}
          ]
        }
      });
      Reflect.deleteProperty(params, 'states');
    } else {
      options.where = Object.assign(options.where, {states: {$notIn: ['Completed', 'Canceled', 'On Hold']}});
    }

    // search function
    if(params.s && params.s !== '') {
      options.include.push({
        model: this.db.SellerUser,
        duplicating: false
      }, {
        model: this.db.Receipt,
        duplicating: false
      });

      options.where = Object.assign(options.where, {
        $or: [
          {'$Receipt.totalPrice$': {$like: `%${params.s}%`}},
          {id: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}},
          {'$User.email$': {$like: `%${params.s}%`}},
          {
            $and: [
              {'$SellerUser.email$': {$like: `%${params.s}%`}},
              {UserId: null}
            ]
          }
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    options.where = Object.assign(options.where, {isBulkCreated: false});

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where.SellerUserId = {$not: null};
    } else if(params.type === 'customer') {
      options.where.SellerUserId = null;
    }
    
    let data = {};
    let includes = [
      'User',
      'SellerUser',
      'Receipt',
      'Country',
      {
        model: this.db.OrderDetail,
        as: 'orderDetail',
        include: [{
          model: this.db.ProductCountry,
          include: ['Product']
        }, {
          model: this.db.PlanCountry,
          include: ['Plan']
        }]
      }
    ];
    this.db.Order.findAndCountAll(options)
      .then(result => {
        data = SequelizeHelper.convertSeque2Json(result);

        if(!options.order) {
          options.order = ['createdAt'];
        }

        return this.db.Order.findAll({
          where: {id: {$in: _.pluck(data.rows, 'id')}},
          order: options.order,
          include: includes,
        });
      })
      .then(orders => {
        data.rows = orders;
        return res.json(data);
      })
      // .then(result => {
      //   console.log(`result === ${JSON.stringify(result)}`);
      //   data = SequelizeHelper.convertSeque2Json(result);
      //   return Promise.mapSeries(data.rows, order => this.db.Order.findOne({where: {id: order.id}, include: this._getOrderInclude()}));
      // })
      // .then(orders => {
      //   data.rows = orders;
      //   return res.json(data);
      // })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

    /** GET /orders
   * get all orders in DB
   */
  getHistoryOrders(req, res) {
    loggingHelper.log('info', 'getHistoryOrders start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    
    options.include = [{
      model: this.db.User,
      duplicating: false
    }];

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where, {'$User.email$': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    // filter by states
    if(params.states && params.states !== '' && params.states !== 'all') {
      options.where = Object.assign(options.where, {
        states: {
          $and: [
            ['Completed', 'Canceled', 'On Hold'],
            {$like: `${params.states}`}
          ]
        }
      });
      Reflect.deleteProperty(params, 'states');
    } else {
      options.where = Object.assign(options.where, {states: {$in: ['Completed', 'Canceled', 'On Hold']}});
    }

    // search function
    if(params.s && params.s !== '') {
      options.include.push({
        model: this.db.SellerUser,
        duplicating: false
      }, {
        model: this.db.Receipt,
        duplicating: false
      });
      
      options.where = Object.assign(options.where, {
        $or: [
          {'$Receipt.totalPrice$': {$like: `%${params.s}%`}},
          {id: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}},
          {'$User.email$': {$like: `%${params.s}%`}},
          {
            $and: [
              {'$SellerUser.email$': {$like: `%${params.s}%`}},
              {UserId: null}
            ]
          }
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    options.where = Object.assign(options.where, {isBulkCreated: false});

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where.SellerUserId = {$not: null};
    } else if(params.type === 'customer') {
      options.where.SellerUserId = null;
    }

    let data;
    let includes = [
      'User',
      'SellerUser',
      'Receipt',
      'Country',
      {
        model: this.db.OrderDetail,
        as: 'orderDetail',
        include: [{
          model: this.db.ProductCountry,
          include: ['Product']
        }, {
          model: this.db.PlanCountry,
          include: ['Plan']
        }]
      }
    ];
    this.db.Order.findAndCountAll(options)
      .then(result => {
        data = SequelizeHelper.convertSeque2Json(result);

        if(!options.order) {
          options.order = ['createdAt'];
        }

        return this.db.Order.findAll({
          where: {id: {$in: _.pluck(data.rows, 'id')}},
          order: options.order,
          include: includes,
        });
      })
      .then(orders => {
        data.rows = orders;
        return res.json(data);
      })
      // this.db.Order.findAndCountAll(options)
      //   .then(result => {
      //     console.log(`result === ${JSON.stringify(result)}`);
      //     data = SequelizeHelper.convertSeque2Json(result);
      //     return Promise.mapSeries(data.rows, order => this.db.Order.findOne({where: {id: order.id}, include: this._getOrderInclude()}));
      //   })
      //   .then(orders => {
      //     data.rows = orders;
      //     return res.json(data);
      //   })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
  
    /** GET /orders
   * get all orders in DB
   */
  getAllOrders(req, res) {
    loggingHelper.log('info', 'getAllOrders start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.email && params.email !== '') {
      options.include = ['User'];
      options.where = Object.assign(options.where, {'$User.email$': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }
    
    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where.SellerUserId = {$not: null};
    } else if(params.type === 'customer') {
      options.where.SellerUserId = null;
    }

    let data;
    this.db.Order.findAndCountAll(options)
      .then(result => {
        console.log(`result === ${JSON.stringify(result)}`);
        data = SequelizeHelper.convertSeque2Json(result);
        return Promise.mapSeries(data.rows, order => this.db.Order.findOne({where: {id: order.id}, include: this._getOrderInclude()}));
      })
      .then(orders => {
        data.rows = orders;
        return res.json(data);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /orders/download
   * download order sale
   * @param {*} req 
   * @param {*} res 
   */
  downloadOrderHistory(req, res) {
    loggingHelper.log('info', 'downloadOrderHistory start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'to date is invalid');
    }

    if(params.email && params.email !== '') {
      options.include = ['User'];
      options.where = Object.assign(options.where, {'$User.email$': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    if(params.historyType === 'pending') {
      options.where = Object.assign(options.where, {
        states: {
          $notIn: ['Completed', 'Canceled', 'On Hold']
        },
        isBulkCreated: false
      });
    } else if(params.historyType === 'history') {
      options.where = Object.assign(options.where, {
        states: {
          $in: ['Completed', 'Canceled', 'On Hold']
        },
        isBulkCreated: false
      });
    }

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.type === 'customer') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    options.include = this._getOrderInclude(true);

    let marketingOffices;
    this.db.MarketingOffice.findAll()
    .then(_marketingOffices => {
      marketingOffices = SequelizeHelper.convertSeque2Json(_marketingOffices);
    })
    .then(() => {
      this.db.Order.findAll(options)
      .then(orders => {
        orders = SequelizeHelper.convertSeque2Json(orders);
        // build order data
        let orderTmp = [];
        let tmp = {};
        orders.forEach(order => {
          let badgeId = (order.SellerUser) ? order.SellerUser.badgeId : '';
          let agentName = (order.SellerUser) ? order.SellerUser.agentName : '';
          let deliveryAddress = '';
          let deliveryContactNumber = '';
          let billingAddress = '';
          let billingContactNumber = '';
          let customerName = '';
          if(order.SellerUserId) {
            customerName = order.fullName;
          } else {
            customerName = order.User ? `${order.User.firstName ? order.User.firstName : ''} ${order.User.lastName ? order.User.lastName : ''}` : '';
          }

          if(order.DeliveryAddress) {
            deliveryAddress = `${order.DeliveryAddress.address} ${order.DeliveryAddress.city} ${order.DeliveryAddress.portalCode} ${order.DeliveryAddress.state}`;
            deliveryContactNumber = order.DeliveryAddress.contactNumber;
          }
          if(order.BillingAddress) {
            billingAddress = `${order.BillingAddress.address} ${order.BillingAddress.city} ${order.BillingAddress.portalCode} ${order.BillingAddress.state}`;
            billingContactNumber = order.BillingAddress.contactNumber;
          }
          if(order.SellerUserId) {
            deliveryContactNumber = order.phone;
            billingContactNumber = order.phone;
          }
          let trackingNumber = order.deliveryId;
          let promoCode = order.promoCode;
          let discountPrice = order.Receipt.discountAmount;
          let productName = '';
          let region = order.Country.code;
          let category = (order.SellerUser) ? 'Sales app' : 'ecommerce';
          let sku = '';
          let email;

          let cancellationReason = '';
          let canceledDate = '';

          if(order.User) {
            email = order.User.email;
          } else if(order.SellerUser) {
            email = order.SellerUser.email;
          } else {
            email = order.email;
          }
          let marketingOffice = _.findWhere(marketingOffices, {id: order.MarketingOfficeId});
          order.orderDetail.forEach((orderDetail, index) => {
            let prodName = (orderDetail.ProductCountry) ? _.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name : '';
            let planName = (orderDetail.PlanCountry) ? _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name : '';
            if(index !== 0) {
              productName += ', ';
            }
            productName += (prodName !== '' ? prodName : planName);

            if(index !== 0) {
              sku += ', ';
            }
            if(orderDetail.PlanCountry) {
              sku = sku + orderDetail.PlanCountry.Plan.sku;
            } else if(orderDetail.ProductCountry) {
              sku = sku + orderDetail.ProductCountry.Product.sku;
            }
          });

          // get cancellationReason if order was canceled from orderHistories
          if(order.states == 'Canceled') {
            for(var i = order.orderHistories.length - 1; i >= 0; i--) {
              let orderHistory = order.orderHistories[i];

              // get the if lastest history was canceled
              if(i == order.orderHistories.length - 1 && orderHistory.message == 'Canceled') {
                cancellationReason = orderHistory.cancellationReason;
                canceledDate = moment(orderHistory.createdAt).format('YYYY-MM-DD');
                break;
              }
            }
          }

          tmp = {
            orderId: OrderHelper.fromatOrderNumber(order),
            badgeId,
            agentName,
            channelType: order.channelType,
            eventLocationCode: order.eventLocationCode,
            moCode: marketingOffice ? marketingOffice.moCode : null,
            customerName,
            email,
            deliveryAddress,
            deliveryContactNumber,
            billingAddress,
            billingContactNumber,
            trackingNumber,
            promoCode,
            discountPrice,
            productName,
            sku,
            region,
            category,
            receiptId: order.Receipt.id,
            currency: order.Country.currencyCode,
            createdAt: order.createdAt,
            saleDate: moment(order.createdAt).format('YYYY-MM-DD'),
            paymentType: order.paymentType.toUpperCase(),
            states: order.states,
            cancellationReason,
            canceledDate,
            totalPrice: order.Receipt.originPrice,
            subTotalPrice: order.Receipt.subTotalPrice,
            discountAmount: order.Receipt.discountAmount,
            taxAmount: order.Receipt.taxAmount,
            shippingFee: order.Receipt.shippingFee,
            grandTotalPrice: order.Receipt.totalPrice,
            source: order.source,
            medium: order.medium,
            campaign: order.campaign,
            term: order.term,
            content: order.content,
            isDirectTrial: order.isDirectTrial ? 'Yes' : (((!order.subscriptionIds || order.subscriptionIds === '') && order.SellerUserId) ? 'Yes' : 'No')
          };
          orderTmp.push(tmp);
        });
        // create CSV and response file
        let fields;
        let fieldNames;
        fields = ['orderId', 'saleDate', 'region', 'states', 'canceledDate', 'cancellationReason', 'category', 'badgeId', 'agentName', 'channelType', 'eventLocationCode', 'isDirectTrial', 'moCode', 'customerName', 'email', 'deliveryAddress', 'deliveryContactNumber', 'billingAddress', 'billingContactNumber', 'trackingNumber', 'sku', 'productName', 'paymentType', 'currency', 'promoCode', 'totalPrice', 'discountAmount', 'subTotalPrice', 'taxAmount', 'shippingFee', 'grandTotalPrice', 'source', 'medium', 'campaign', 'term', 'content'];
        fieldNames = ['Order', 'Sale Date', 'Region', 'Order Status', 'Canceled Date', 'Cancellation Reason', 'Category', 'Badge Id', 'AgentName', 'Channel Type', 'Event/Location Code', 'Direct Delivery', 'MO Code', 'Customer Name', 'Email', 'Delivery Address', 'Delivery Contact Number', 'Billing Address', 'Billing Contact Number', 'Tracking Number', 'Sku', 'ProductName', 'Payment Type', 'Currency', 'Promo Code', 'Total Price', 'Discount Amount', 'Sub Total Price', 'Tax Amount', 'Shipping Fee', 'Grand Total(Incl. tax)', 'Source', 'Medium', 'Campaign', 'Term', 'Content'];
        let data = json2csv({
          data: orderTmp,
          fields,
          fieldNames
        });
        
        // send CSV file
        res.attachment(`Sales report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      });
    })
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /orders/download/tax-invoice
   * download tax invoice of order
   * @param {*} req 
   * @param {*} res 
   */
  downloadOrderTaxInvoice(req, res) {
    loggingHelper.log('info', 'downloadOrderTaxInvoice start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'to date is invalid');
    }

    if(params.email && params.email !== '') {
      options.include = ['User'];
      options.where = Object.assign(options.where, {'$User.email$': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Order);

    if(params.historyType === 'pending') {
      options.where = Object.assign(options.where, {
        states: {
          $notIn: ['Completed', 'Canceled', 'On Hold']
        },
        isBulkCreated: false
      });
    } else if(params.historyType === 'history') {
      options.where = Object.assign(options.where, {
        states: {
          $in: ['Completed', 'Canceled', 'On Hold']
        },
        isBulkCreated: false
      });
    }

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.type === 'customer') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    let fileExportName = 'Tax Invoice for Order';
    if(params.fromDate) {
      fileExportName += ` from ${params.fromDate}`;
    }
    if(params.toDate) {
      fileExportName += ` to ${params.toDate}`;
    }
    fileExportName += `(${moment().format('YYYY-MM-DD_HHmmssSSS')}).tar.gz`;
    console.log('fileExportName === === === ', fileExportName);
    return this.db.FileUpload.create(
      {
        name: fileExportName,
        // url: uploadedFile.Location,
        // bulkOrderId: order.id,
        isDownloaded: true,
        states: 'Processing',
        AdminId: decoded.id

      },
      {returning: true}
    )
    .then(result => {
      result = SequelizeHelper.convertSeque2Json(result);
      let queueOptions = {
        options,
        fileUploadId: result.id
      };
      return compressedFileToDownloadQueueService.addTask({method: 'downloadTaxInvoiceOrder', queueOptions});
    })
    .then(() => res.json({ok: true}))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  cancelOrder(req, res) {
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    loggingHelper.log('info', 'cancelOrder start');
    let userTypeId = (req.query.userTypeId === 1 || req.query.userTypeId === '1') ? req.query.userTypeId : 2;
    let cancelOrderQueueService = CancelOrderQueueService.getInstance();
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    // let isAdmin = req.decoded.hasOwnProperty('adminRole');
    // find order
    this.db.Order.findOne({where: {id: req.params.id}, include: ['User', 'Country']})
      .then(order => {
        // check order status
        if(!order) {
          throw new Error('order_not_existed');
        }
        // if(!isAdmin && moment(order.createdAt).add(1, 'day').isBefore(moment())) {
        //   throw new Error('can_not_cancel1');
        // } 
        if(order.status === 'Delivering' || (userTypeId === 1 && order.status === 'Completed')) {
          throw new Error('can_not_cancel2');
        } else if(userTypeId === 2 && order.paymentType === 'cash') {
          // Cancel from BA website and paymentType is cash
          return this.db.Order.update({states: 'Canceled'}, {where: {id: req.params.id}})
            .then(() => this.db.OrderHistory.create({
              OrderId: req.params.id,
              message: 'Canceled',
              cancellationReason: 'Canceled by user from BA web'
            }))
            .then(() => sendEmailQueueService.addTask({method: 'sendCancelOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()}));
        } else {
          // add cancel order to queue
          let canceledFrom = userTypeId == 1 ? 'admin' : 'BA';
          return cancelOrderQueueService.addTask({orderId: order.id, canceledFrom});
        }
      })
      .then(() => res.json({ok: true})) // return result to client
      .catch(error => {
        if(error.message === 'order_not_existed') {
          return ResponseHelper.responseError(res, 'Order not existed');
        } else if(error.message === 'can_not_cancel1') {
          return ResponseHelper.responseError(res, 'Can not cancel this order because it\'s over 24 hours');
        } else if(error.message === 'can_not_cancel2') {
          return ResponseHelper.responseError(res, 'Can not cancel this order because it\'s delivering');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /api/orders/:id/remarks
   * Add remark for order
   * @param {*} req 
   * @param {*} res 
   */
  postOrderRemarks(req, res) {
    loggingHelper.log('info', 'postOrderRemarks start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validate created at
    if(params.createdAt && !Validator.valiadateDate(params.createdAt)) {
      return ResponseHelper.responseError('created at incorrecft format');
    }

    if(params.updatedAt && !Validator.valiadateDate(params.updatedAt)) {
      return ResponseHelper.responseError('updated at incorrecft format');
    }

    params.OrderId = req.params.id;
    params.isRemark = true;
    this.db.OrderHistory.create(params, {returning: true})
      .then(orderHistory => ResponseHelper.responseSuccess(res, orderHistory))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * PUT /api/orders/:id/remarks/:historyId
   * Update remark
   * @param {*} req 
   * @param {*} res 
   */
  editOrderRemarks(req, res) {
    loggingHelper.log('info', 'editOrderRemarks start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validate created at
    if(params.createdAt && !Validator.valiadateDate(params.createdAt)) {
      return ResponseHelper.responseError('created at incorrecft format');
    }

    if(params.updatedAt && !Validator.valiadateDate(params.updatedAt)) {
      return ResponseHelper.responseError('updated at incorrecft format');
    }

    this.db.OrderHistory.update(params, {where: {id: req.params.historyId}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * DELETE /api/orders/:id/remarks/:historyId
   * Delete
   * @param {*} req 
   * @param {*} res 
   */
  deleteOrderRemarks(req, res) {
    loggingHelper.log('info', 'deleteOrderRemarks start');
    this.db.OrderHistory.destroy({where: {id: req.params.historyId}})
      .then(orderHistory => ResponseHelper.responseSuccess(res, orderHistory))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new OrderApi(...args);
}
