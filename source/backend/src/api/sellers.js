import jwt from 'jsonwebtoken';
import _ from 'underscore';

import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import { stripeHelper } from '../helpers/stripe-helper';
import { loggingHelper } from '../helpers/logging-helper';
import MailchimpHelper from '../helpers/mailchimp';
import config from '../config';

class SellerApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.mailchimp = MailchimpHelper.getInstance();
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/sellers/check-token', JwtMiddleware.verifyUserToken, this.checkToken.bind(this));
    this.api.post('/sellers/check-email', JwtMiddleware.verifyUserToken, this.checkEmailAddress.bind(this));
    this.api.post('/sellers/:sellerId/deliveries', JwtMiddleware.verifyUserToken, this.addDeliveryAddress.bind(this));
    this.api.post('/sellers/:sellerId/cards', JwtMiddleware.verifyUserToken, this.addStripeCard.bind(this));
  }

  checkToken(req, res) {
    loggingHelper.log('info', 'token checking');
    loggingHelper.log('info', 'token verified');

    return res.json({
      ok: true,
      message: 'token verified'
    });
  }

  /**
   * POST /sellers/check-email
   * check existing user and return delivery address and billing address
   * @param {*} req 
   * @param {*} res 
   */
  checkEmailAddress(req, res) {
    loggingHelper.log('info', 'checkEmailAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let data = {};
    if(!params.email) {
      return ResponseHelper.responseMissingParam(res, 'missing email');
    }
    this.db.User.findOne({where: {email: params.email}})
      .then(user => {
        if(user) {
          data.user = user;
          return this.db.DeliveryAddress.findOne({where: {id: user.defaultShipping}});
        }
      })
      .then(deliveryAddress => {
        if(data.user) {
          data.deliveryAddress = deliveryAddress;
          return this.db.DeliveryAddress.findOne({where: {id: data.user.defaultBilling}});
        }
      })
      .then(billingAddress => {
        if(data.user) {
          data.billingAddress = billingAddress;
          return this.db.Subscription.findAll({where: { UserId: data.user.id }});
        }
      })
      .then(subscriptions => {
        data.subscriptions = subscriptions;
        return ResponseHelper.responseSuccess(res, data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /users/:userId/deliveries
   * Add delivery detail
   */
  addDeliveryAddress(req, res) {
    loggingHelper.log('info', 'addDeliveryAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let data = {};
    if(!params.email) {
      return ResponseHelper.responseMissingParam(res, 'missing email');
    }

    params.fullname = params.fullname ? params.fullname : '';
    params.firstName = (params.fullname.indexOf(' ') > -1) ? params.fullname.substr(0, params.fullname.indexOf(' ')) : params.fullname;
    params.lastName = (params.fullname.indexOf(' ') > -1) ? params.fullname.substr(params.fullname.indexOf(' ') + 1) : null;

    // check email to create new account
    this.db.sequelize.transaction(t => this.db.User.findOne({where: {email: params.email}})
      .then(user => {
        if(!user) {
          let _user;
          return this.db.User.create({
            email: params.email,
            firstName: params.firstName,
            lastName: params.lastName,
            CountryId: req.country.id,
            isGuest: true
          }, {returning: true, transaction: t})
            .then(result => {
              _user = result;
              return this.db.Cart.create({UserId: result.id}, {transaction: t})
                .then(() => {
                  let utmDefaultData = this.mailchimp.getDefaultUtmData();
                  this.mailchimp.addMemberToRegisterList({
                    'email_address': params.email,
                    'status': 'subscribed',
                    'merge_fields': {
                      FNAME: params.firstName,
                      LNAME: params.lastName ? params.lastName : '',
                      COUNTRY: req.country.code.substr(0, 2),
                      SOURCE: 'BA app',
                      USOURCE: this.mailchimp.getDefaultUtmData('source'),
                      UMEDIUM: utmDefaultData,
                      UCAMPAIGN: utmDefaultData,
                      UCONTENT: utmDefaultData,
                      UTERM: utmDefaultData
                    }
                  });
                });
            })
            .then(() => _user);
        } else {
          return user;
        }
      })
      .then(user => {
        data.user = user;
        params.UserId = user.id;
        return this.db.DeliveryAddress.create(params, {returning: true, transaction: t});
      })
      .then(deliveryAddress => {
        data.deliveryAddress = deliveryAddress;
        if(!params.addressType) {
          return this.db.User.update({defaultShipping: deliveryAddress.id, defaultBilling: deliveryAddress.id}, {where: {id: data.user.id}, transaction: t});
        } else if(params.addressType === 'shipping') {
          return this.db.User.update({defaultShipping: deliveryAddress.id}, {where: {id: data.user.id}, transaction: t});
        } else {
          return this.db.User.update({defaultBilling: deliveryAddress.id}, {where: {id: data.user.id}, transaction: t});
        }
      })
    )
      .then(() => res.json({
        ok: true,
        data
      }))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

    /** POST /users/:userId/cards
   * Add stripe cart
   */
  addStripeCard(req, res) {
    loggingHelper.log('info', 'addStripeCard start');
    loggingHelper.log('info', `param: req.body = ${req.body}, req.decoded = ${req.decoded}`);
    let params = req.body;
    // let user = req.decoded;
    let cardInfo;
    let stripeCustomer;
    let token = req.headers['x-access-token'];
    let type = 'website';
    if(!params.stripeToken) {
      return ResponseHelper.responseMissingParam(res);
    }
    if(!token) {
      return ResponseHelper.responseError(res, 'missing access token');
    }

    jwt.verify(token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseError(res, 'token expired');
      } else {
        if(decoded.isSeller) {
          type = 'baWeb';
        }
      }
    });
    
    this.db.sequelize.transaction(t => stripeHelper.createCustomer(params.email, params.stripeToken, req.country.code, type)
      .then(_stripeCustomer => {
        // check card info
        stripeCustomer = _stripeCustomer;
        if(stripeCustomer.sources.data.length === 0) {
          throw new Error('stripe_token_invalid');
        } 
        
        return this.db.Card.findOne({where: {
          'cardNumber': stripeCustomer.sources.data[0].last4,
          'branchName': stripeCustomer.sources.data[0].brand,
          'expiredYear': stripeCustomer.sources.data[0].exp_year,
          'expiredMonth': stripeCustomer.sources.data[0].exp_month,
          type,
          '$User.email$': params.email,
          'CountryId': req.country.id
        }, include: ['User']});
      })
      .then(card => {
        if(!card) {
          // store card infor into database
          cardInfo = {
            customerId: stripeCustomer.id,
            cardNumber: stripeCustomer.sources.data[0].last4,
            branchName: stripeCustomer.sources.data[0].brand,
            cardName: stripeCustomer.sources.data[0].name,
            type,
            expiredYear: stripeCustomer.sources.data[0].exp_year,
            expiredMonth: stripeCustomer.sources.data[0].exp_month,
            CountryId: req.country.id
          };
          return this.db.Card.create(cardInfo, {returning: true, transaction: t});
        } else {
          return card;
        }
      })
      .then(card => res.json(card)))
      .catch(error => {
        if(error.message === 'stripe_token_invalid') {
          return ResponseHelper.responseErrorParam(res, 'stripe token invalid.');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

}

export default function(...args) {
  return new SellerApi(...args);
}
