// import Promise from 'bluebird';
// import _ from 'underscore';
// import multipart from 'connect-multiparty';
// import moment from 'moment';
// import json2csv from 'json2csv';

// import config from '../config';
import ResponseHelper from '../helpers/response-helper';
// import RequestHelper from '../helpers/request-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
// import { awsHelper } from '../helpers/aws-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
// import Validator from '../helpers/validator';
import SendEmailQueueService from '../services/send-email.queue.service';
import { loggingHelper } from '../helpers/logging-helper';

// let multipartMiddleware = multipart();

class CustomPlanApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/custom-plan', JwtMiddleware.verifyUserToken, this.postCustomPlan.bind(this));
    this.api.put('/users/:userId/custom-plan/:subscriptionId', JwtMiddleware.verifyUserToken, this.updateCustomPlanDetails.bind(this));
  }

  /**
   * POST /custom-plan
   * @param <plan properties>
   * @param files: plan images
   */
  postCustomPlan(req, res) {
    loggingHelper.log('info', 'postCustomPlan start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validate require params
    if(!params.UserId || !params.customPlanDetail) {
      return ResponseHelper.responseMissingParam(res);
    }
    this.db.User.findOne({where: {id: params.UserId}})
      .then(user => {
        let fullName = user.firstName ? (user.firstName + (user.lastName ? ` ${user.lastName}` : '')) : '';
        params.description = `Custom Plan of ${fullName}`;

        return this.db.sequelize.transaction(t => this.db.CustomPlan.create(params, {include: ['customPlanDetail'], returning: true, transaction: t}));
      })
    .then(() => res.json({ok: true}))
    .catch(error => {
      if(error.name === 'SequelizeValidationError') {
        return ResponseHelper.responseErrorParam(res, error);
      } else if(error.name === 'SequelizeUniqueConstraintError') {
        return ResponseHelper.responseError(res, error);
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }

  /**
   * PUT /users/:userId/custom-plan/:orderId
   * update subscription details for trial plan
   * @param {*} req 
   * @param {*} res 
   */
  updateCustomPlanDetails(req, res) {
    let params = req.body;
    let options = {};
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    
    // check input data
    if(!req.params.subscriptionId) {
      return ResponseHelper.responseMissingParam(res);
    }

    let countryIds = [];
    let sellPrice = 0.00;
    params.customPlanDetail.forEach(detail => 
      countryIds.push(detail.ProductCountryId)
    );

    // this.db.Subscription.findOne({where: {id: req.params.subscriptionId}})
    this.db.ProductCountry.findAll({where: {
      id: {
        $in: countryIds
      }
    }, include: ['Country', {
      model: this.db.Product,
      include: ['ProductType']
    }]})
    .then(productCountries => {
      productCountries = SequelizeHelper.convertSeque2Json(productCountries);
      productCountries.forEach(productCountry => {
        let qty = params.customPlanDetail.filter(detail => detail.ProductCountryId === productCountry.id)[0].qty;
        sellPrice = parseFloat(sellPrice) + parseFloat(productCountry.sellPrice * qty);
      });

      options = {
        CustomPlanId: params.CustomPlanId,
        price: sellPrice,
        pricePerCharge: sellPrice,
      };

      return this.db.Subscription.findOne({where: {id: req.params.subscriptionId, UserId: req.params.userId}});
    })
    .then(subscription => {
      if(!subscription) {
        throw new Error('subscription_not_found');
      }
      return this.db.CustomPlan.findOne({where: {id: params.CustomPlanId}});
    })
    .then(customPlan => {
      if(!customPlan) {
        throw new Error('CustomPlan_not_found');
      }

      let customPlanUpdate = {};
      // start transaction
      return this.db.sequelize.transaction(t => 
        this.db.Subscription.update(options, {where: {id: req.params.subscriptionId}, transaction: t})
        .then(() => {
          let productNameFirst = params.customPlanDetail[0].productName;
          let productNameSecond = params.customPlanDetail[1] ? params.customPlanDetail[1].productName : null;
          let duration = params.customPlanDetail[0].duration;
    
          customPlanUpdate.description = 'Custom Plan: 1 ' + productNameFirst + (productNameSecond ? (' and 1 ' + productNameSecond) : '') + ' every ' + duration;
          customPlanUpdate.PlanTypeId = params.PlanTypeId;
          
          return this.db.CustomPlan.update(customPlanUpdate, {where: {id: params.CustomPlanId}, transaction: t});
        })
        .then(() => this.db.CustomPlanDetail.destroy({where: {CustomPlanId: params.CustomPlanId}, transaction: t}))
        .then(() => this.db.CustomPlanDetail.bulkCreate(params.customPlanDetail, {returning: true, transaction: t}))
        .then(() => 
          this.db.SubscriptionHistory.create({
            message: 'User Update',
            subscriptionId: req.params.subscriptionId,
            detail: `${customPlan.description} -> ${customPlanUpdate.description}`
          },
          {returning: true, transaction: t})
        )
      );
    })
    .then(() => sendEmailQueueService.addTask({method: 'sendUpdateTrialEmail', data: {id: req.params.subscriptionId}, langCode: req.country.defaultLang.toLowerCase()}))
    .then(() => ResponseHelper.responseSuccess(res))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new CustomPlanApi(...args);
}
