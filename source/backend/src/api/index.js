import { version } from '../../package.json';
import { Router } from 'express';
import UserApi from './users';
import SellerApi from './sellers';
import UserAuthenticationApi from './user-authentication';
import AdminAuthenticationApi from './admin-authentication';
import PaymentApi from './payment';
import ProductionApi from './products';
import ArticleAPI from './articles';
import SubscriptionApi from './subscriptions';
import NotificationApi from './notifications';
import PromotionApi from './promotions';
import CountryApi from './countries';
import OrderApi from './orders';
import FaqAPI from './faqs';
import AdminUserApi from './admin-user';
import SalesReportApi from './sales-reports';
import BulkOrderApi from './bulk-orders';
import TrialPlanApi from './trial-plans';
import RoleApi from './roles';
import OrderUploadApi from './order-upload';
import TermPrivacyApi from './term-privacy';
import NonEcommerceApi from './non-ecommerce';
import MarketingOfficeApi from './marketing-offices';
import CustomPlanApi from './custom-plan';
import FileUploadApi from './file-upload';
import MessageApi from './message';
import UserOrderListApi from './user-order-list';
import ASKProductsAPI from './products-ask'

export default ({ db }) => {
  let api = Router();

  // register API
  UserAuthenticationApi(api, db);
  AdminAuthenticationApi(api, db);
  UserApi(api, db);
  PaymentApi(api, db);
  ProductionApi(api, db);
  ArticleAPI(api, db);
  SubscriptionApi(api, db);
  NotificationApi(api, db);
  PromotionApi(api, db);
  CountryApi(api, db);
  OrderApi(api, db);
  FaqAPI(api, db);
  AdminUserApi(api, db);
  SellerApi(api, db);
  SalesReportApi(api, db);
  BulkOrderApi(api, db);
  TrialPlanApi(api, db);
  RoleApi(api, db);
  OrderUploadApi(api, db);
  TermPrivacyApi(api, db);
  NonEcommerceApi(api, db);
  MarketingOfficeApi(api, db);
  CustomPlanApi(api, db);
  FileUploadApi(api, db);
  MessageApi(api, db);
  UserOrderListApi(api, db);
  ASKProductsAPI(api,db);
  // perhaps expose some API metadata at the root
  api.get('/', (req, res) => {
    res.json({ version });
  });

  return api;
};
