import ResponseHelper from '../helpers/response-helper';

class NotificationAPI {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/notification-types', this.getNotificationType.bind(this));
  }

  /** GET /notification-types
   * get all notification-types in DB
   */
  getNotificationType(req, res) {
    this.db.NotificationType.findAll()
      .then(notificationTypes => {
        res.json(notificationTypes);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new NotificationAPI(...args);
}
