import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import Validator from '../helpers/validator';
// import OrderHelper from '../helpers/order-helper';
import moment from 'moment';
// import json2csv from 'json2csv';
import { loggingHelper } from '../helpers/logging-helper';
import DownloadController from '../controllers/download.controller';
import CompressedFileToDownloadQueueService from '../services/create-compressed-file-to-download.queue.service';

class SalesReportApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
    this.downloadController = new DownloadController(db);
  }

  registerRoute() {
    this.api.get('/sales-reports', JwtMiddleware.verifyAdminToken, this.getSalesReport.bind(this));
    this.api.get('/sales-reports/download', JwtMiddleware.verifyAdminToken, this.downloadSalesReport.bind(this));
    this.api.get('/sales-reports/appco', JwtMiddleware.verifyAdminToken, this.getSalesReportForAppco.bind(this));
    this.api.get('/sales-reports/appco/download', JwtMiddleware.verifyAdminToken, this.downloadSalesReportForAppco.bind(this));
    this.api.get('/sales-reports/mo', JwtMiddleware.verifyAdminToken, this.getSalesReportForMO.bind(this));
    this.api.get('/sales-reports/mo/download', JwtMiddleware.verifyAdminToken, this.downloadSalesReportForMO.bind(this));
    this.api.get('/sales-reports/summary', JwtMiddleware.verifyAdminToken, this.getSaleReportSummary.bind(this));
    this.api.get('/sales-reports/location-code', JwtMiddleware.verifyAdminToken, this.getLocationCode.bind(this));
  }

  /**
   * GET /sales-reports
   * get sales report from start date to end date
   * @param {*} req 
   * @param {*} res 
   */
  getSalesReport(req, res) {
    loggingHelper.log('info', 'getSalesReport start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    
    // filter by states
    if(params.states && params.states !== '') {
      options.where = Object.assign(options.where, {states: {$like: `${params.states}`}});
    }
    
    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {sku: {$like: `%${params.s}%`}},
          {orderId: {$like: `%${params.s}%`}},
          {bulkOrderId: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}}
        ]
      });
    }

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'customer') {
      options.where = Object.assign(options.where, {category: 'client'});
    }
    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {category: 'sales app'});
    }
    if(params.type === 'bulkOrder') {
      options.where = Object.assign(options.where, {category: 'Bulk Order'});
    }

    this.db.SaleReport.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /sales-reports/download
   * download sales report
   * @param {*} req 
   * @param {*} res 
   */
  downloadSalesReport(req, res) {
    loggingHelper.log('info', 'downloadSalesReport start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    params.toDate = params.toDate ? params.toDate : moment().format('YYYY-MM-DD');
    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'customer') {
      options.where = Object.assign(options.where, {category: 'client'});
    }
    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {category: 'sales app'});
    }
    if(params.type === 'bulkOrder') {
      options.where = Object.assign(options.where, {category: 'Bulk Order'});
    }
    
    options.include = [{
      model: this.db.Country
    }];

    if(params.taxInvoice) {
      let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();
      let fileExportName = 'Tax Invoice for Sales report';
      if(params.fromDate) {
        fileExportName += ` from ${params.fromDate}`;
      }
      if(params.toDate) {
        fileExportName += ` to ${params.toDate}`;
      }
      fileExportName += `(${moment().format('YYYY-MM-DD_HHmmssSSS')}).tar.gz`;
      console.log('fileExportName === === === ', fileExportName);
      return this.db.FileUpload.create(
        {
          name: fileExportName,
          // url: uploadedFile.Location,
          // bulkOrderId: order.id,
          isDownloaded: true,
          states: 'Processing',
          AdminId: decoded.id
  
        },
        {returning: true}
      )
      .then(result => {
        result = SequelizeHelper.convertSeque2Json(result);
        Reflect.deleteProperty(options, 'include');
        let queueOptions = {
          options,
          fileUploadId: result.id
        };
        return compressedFileToDownloadQueueService.addTask({method: 'downloadTaxInvoiceSaleReport', queueOptions});
      })
      .then(() => res.json({ok: true}))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    } else {
      this.downloadController.downloadSalesReport(options)
      .then(data => {
        if(data) {
          // send CSV file
          res.attachment(`Sales report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
          res.status(200).send(data);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    }
  }

  /**
   * GET /sales-report/summary
   * get summary of sales-report
   * @param {*} req 
   * @param {*} res 
   */
  getSaleReportSummary(req, res) {
    loggingHelper.log('info', 'getSaleReportSummary start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let type = params.type;
    let decoded = req.decoded;
    let options = {};
    if(!params.countryCode) {
      return ResponseHelper.responseError(res, 'missing country');
    }

    // build condition
    if(type === 'year') {
      options.where = {createdAt: {$gt: RequestHelper.convertServerTimezone2UTC(new Date(moment().format('YYYY')), 0, req)}};
    } else if(type === 'month') {
      options.where = {
        $and: [
          {createdAt: {$gt: RequestHelper.convertServerTimezone2UTC(new Date(moment().subtract(1, 'month').format('YYYY-MM')), 0, req)}},
          {createdAt: {$lt: RequestHelper.convertServerTimezone2UTC(new Date(moment().format('YYYY-MM')), 0, req)}},
        ]
      };
    } else {
      options.where = {createdAt: {$gte: RequestHelper.convertServerTimezone2UTC(new Date(moment().subtract(7, 'days').format('YYYY-MM-DD')), 0, req)}};
    }

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // filter by country
    options.where = Object.assign(options.where, {region: params.countryCode.toUpperCase()});

    if(params.reportType === 'appco') {
      options.where = Object.assign(options.where, {category: 'sales app'});
    }
    
    if(params.reportType === 'mo') {
      options.where = Object.assign(options.where, {category: 'sales app'});
      if(params.MarketingOfficeId) {
        options.where = Object.assign(options.where, {MarketingOfficeId: params.MarketingOfficeId});
      }
    }

    options.where = Object.assign(options.where, {states: {$notIn: ['Canceled', 'On Hold']}});

    // let resultAla = [];
    // let resultSubs = [];
    // let currentReportAla;
    // let currentReportSubs;
    let revenues = [];
    let labels = [];
    let totalRevenue = 0;
    let currentReportAt;
    let revenue;
    this.db.SaleReport.findAll(options)
      .then(reports => {
        reports = SequelizeHelper.convertSeque2Json(reports);
        // sort report
        reports.sort((a, b) => a.createdAt.localeCompare(b.createdAt));
        
        // build report
        reports.forEach(report => {
          totalRevenue += +report.subTotalPrice;

          if(currentReportAt !== moment(RequestHelper.buildClientTimezone(moment(report.createdAt), 0, req)).format('YYYY-MM-DD')) {
            revenue = +report.subTotalPrice;
            revenues.push(revenue);
          
            labels.push(moment(RequestHelper.buildClientTimezone(moment(report.createdAt), 0, req)).format('YYYY-MM-DD'));
            currentReportAt = moment(RequestHelper.buildClientTimezone(moment(report.createdAt), 0, req)).format('YYYY-MM-DD');
          } else {
            revenue += +report.subTotalPrice;
            revenues.splice(-1, 1);
            revenues.push(revenue);
          }
          // if(type === 'year' && report.type === 'ala-carte') {
          //   if(currentReportAla && moment(report.createdAt).format('YYYY-MM') !== currentReportAla.label) {
          //     resultAla.push(Object.assign({}, currentReportAla));
          //     currentReportAla = {
          //       label: moment(report.createdAt).format('YYYY-MM'),
          //       productUnits: report.productUnits
          //     };
          //   } else if(currentReportAla) {
          //     currentReportAla.productUnits += report.productUnits;
          //   } else {
          //     currentReportAla = {
          //       label: moment(report.createdAt).format('YYYY-MM'),
          //       productUnits: report.productUnits
          //     };
          //   }
          // } else if((type === 'month' || type === 'day') && report.type === 'ala-carte') {
          //   if(currentReportAla && moment(report.createdAt).format('YYYY-MM-DD') !== currentReportAla.label) {
          //     resultAla.push(Object.assign({}, currentReportAla));
          //     currentReportAla = {
          //       label: moment(report.createdAt).format('YYYY-MM-DD'),
          //       productUnits: report.productUnits
          //     };
          //   } else if(currentReportAla) {
          //     currentReportAla.productUnits += report.productUnits;
          //   } else {
          //     currentReportAla = {
          //       label: moment(report.createdAt).format('YYYY-MM-DD'),
          //       productUnits: report.productUnits
          //     };
          //   }
          // } else if(type === 'year' && report.type === 'subscription') {
          //   if(currentReportSubs && moment(report.createdAt).format('YYYY-MM') !== currentReportSubs.label) {
          //     resultSubs.push(Object.assign({}, currentReportSubs));
          //     currentReportSubs = {
          //       label: moment(report.createdAt).format('YYYY-MM'),
          //       productUnits: report.productUnits
          //     };
          //   } else if(currentReportSubs) {
          //     currentReportSubs.productUnits += report.productUnits;
          //   } else {
          //     currentReportSubs = {
          //       label: moment(report.createdAt).format('YYYY-MM'),
          //       productUnits: report.productUnits
          //     };
          //   }
          // } else {
          //   if(currentReportSubs && moment(report.createdAt).format('YYYY-MM-DD') !== currentReportSubs.label) {
          //     resultSubs.push(Object.assign({}, currentReportSubs));
          //     currentReportSubs = {
          //       label: moment(report.createdAt).format('YYYY-MM-DD'),
          //       productUnits: report.productUnits
          //     };
          //   } else if(currentReportSubs) {
          //     currentReportSubs.productUnits += report.productUnits;
          //   } else {
          //     currentReportSubs = {
          //       label: moment(report.createdAt).format('YYYY-MM-DD'),
          //       productUnits: report.productUnits
          //     };
          //   }
          // }
        });
        // resultAla.push(Object.assign({}, currentReportAla));
        // resultSubs.push(Object.assign({}, currentReportSubs));
        return res.json({revenues: revenues, totalRevenue, labels: labels});
      })
      // .then(() => {
      //   // build result value
      //   let alaCarteLabels = (resultAla) ? _.pluck(resultAla, 'label') : [];
      //   let subsLabels = (resultSubs) ? _.pluck(resultSubs, 'label') : [];
      //   let labels = _.union(alaCarteLabels, subsLabels);
      //   let alaItems = [];
      //   let subsItems = [];
      //   let outputLables = [];
      //   labels.forEach(label => {
      //     if(label) {
      //       let alarItem = _.findWhere(resultAla, {label});
      //       alaItems.push((alarItem) ? alarItem.productUnits : 0);
      //       let subsItem = _.findWhere(resultSubs, {label});
      //       subsItems.push((subsItem) ? subsItem.productUnits : 0);
      //       outputLables.push(label);
      //     }
      //   });
      //   return res.json({alaCarteItems: alaItems, subscriptionItems: subsItems, totalRevenue, labels: outputLables});
      // })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /sales-reports/appco
   * get sales report from start date to end date
   * @param {*} req 
   * @param {*} res 
   */
  getSalesReportForAppco(req, res) {
    loggingHelper.log('info', 'getSalesReportForAppco start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    
    // filter by states
    if(params.states && params.states !== '') {
      options.where = Object.assign(options.where, {states: {$like: `${params.states}`}});
    }
    
    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {sku: {$like: `%${params.s}%`}},
          {orderId: {$like: `%${params.s}%`}},
          {bulkOrderId: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}}
        ]
      });
    }

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'customer') {
      options.where = Object.assign(options.where, {category: 'client'});
    }
    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {category: 'sales app'});
    }
    if(params.type === 'bulkOrder') {
      options.where = Object.assign(options.where, {category: 'Bulk Order'});
    }

    // options.where = Object.assign(options.where, 
    //   {$and: [
    //     {badgeId: {$not: null}},
    //     {badgeId: {$not: ''}}
    //   ]});
    options.where = Object.assign(options.where, {category: 'sales app'});

    this.db.SaleReport.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /sales-reports/appco/download
   * download sales report for Appco
   * @param {*} req 
   * @param {*} res 
   */
  downloadSalesReportForAppco(req, res) {
    loggingHelper.log('info', 'downloadSalesReportForAppco start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    params.toDate = params.toDate ? params.toDate : moment().format('YYYY-MM-DD');
    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'customer') {
      options.where = Object.assign(options.where, {category: 'client'});
    }
    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {category: 'sales app'});
    }
    if(params.type === 'bulkOrder') {
      options.where = Object.assign(options.where, {category: 'Bulk Order'});
    }

    options.where = Object.assign(options.where, {category: 'sales app'});
    
    options.include = [{
      model: this.db.Country
    }];

    if(params.taxInvoice) {
      let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();
      let fileExportName = 'Tax Invoice for Sales report Appco';
      if(params.fromDate) {
        fileExportName += ` from ${params.fromDate}`;
      }
      if(params.toDate) {
        fileExportName += ` to ${params.toDate}`;
      }
      fileExportName += `(${moment().format('YYYY-MM-DD_HHmmssSSS')}).tar.gz`;
      console.log('fileExportName === === === ', fileExportName);
      return this.db.FileUpload.create(
        {
          name: fileExportName,
          // url: uploadedFile.Location,
          // bulkOrderId: order.id,
          isDownloaded: true,
          states: 'Processing',
          AdminId: decoded.id
  
        },
        {returning: true}
      )
      .then(result => {
        result = SequelizeHelper.convertSeque2Json(result);
        Reflect.deleteProperty(options, 'include');
        let queueOptions = {
          options,
          fileUploadId: result.id
        };
        return compressedFileToDownloadQueueService.addTask({method: 'downloadTaxInvoiceSaleReport', queueOptions});
      })
      .then(() => res.json({ok: true}))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    } else {
      this.downloadController.downloadSalesReportForAppco(options)
      .then(data => {
        if(data) {
          // send CSV file
          res.attachment(`Sales report Appco for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
          res.status(200).send(data);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    }
  }

  /**
   * GET /sales-reports/mo
   * get sales report from start date to end date
   * @param {*} req 
   * @param {*} res 
   */
  getSalesReportForMO(req, res) {
    loggingHelper.log('info', 'getSalesReportForMO start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // filter by states
    if(params.states && params.states !== '') {
      options.where = Object.assign(options.where, {states: {$like: `${params.states}`}});
    }
    
    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {sku: {$like: `%${params.s}%`}},
          {orderId: {$like: `%${params.s}%`}},
          {bulkOrderId: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}}
        ]
      });
    }

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    options.where = Object.assign(options.where, {category: 'sales app'});
    
    if(decoded.MarketingOfficeId) {
      options.where = Object.assign(options.where, {MarketingOfficeId: decoded.MarketingOfficeId});
    }

    this.db.SaleReport.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /sales-reports/mo/download
   * download sales report for MO
   * @param {*} req 
   * @param {*} res 
   */
  downloadSalesReportForMO(req, res) {
    loggingHelper.log('info', 'downloadSalesReportForMO start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    params.toDate = params.toDate ? params.toDate : moment().format('YYYY-MM-DD');
    options = RequestHelper.buildWhereCondition(options, params, this.db.SaleReport);
    options = RequestHelper.buildSequelizeOptions(options, params);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    options.where = Object.assign(options.where, {category: 'sales app'});
    
    if(decoded.MarketingOfficeId) {
      options.where = Object.assign(options.where, {MarketingOfficeId: decoded.MarketingOfficeId});
    }
    
    options.include = [{
      model: this.db.Country
    }];

    if(params.taxInvoice) {
      let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();
      let fileExportName = 'Tax Invoice for Sales report Marketing Office';
      if(params.fromDate) {
        fileExportName += ` from ${params.fromDate}`;
      }
      if(params.toDate) {
        fileExportName += ` to ${params.toDate}`;
      }
      fileExportName += `(${moment().format('YYYY-MM-DD_HHmmssSSS')}).tar.gz`;
      console.log('fileExportName === === === ', fileExportName);
      return this.db.FileUpload.create(
        {
          name: fileExportName,
          // url: uploadedFile.Location,
          // bulkOrderId: order.id,
          isDownloaded: true,
          states: 'Processing',
          AdminId: decoded.id
  
        },
        {returning: true}
      )
      .then(result => {
        result = SequelizeHelper.convertSeque2Json(result);
        Reflect.deleteProperty(options, 'include');
        let queueOptions = {
          options,
          fileUploadId: result.id
        };
        return compressedFileToDownloadQueueService.addTask({method: 'downloadTaxInvoiceSaleReport', queueOptions});
      })
      .then(() => res.json({ok: true}))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    } else {
      this.downloadController.downloadSalesReportForMO(options)
      .then(data => {
        if(data) {
          // send CSV file
          res.attachment(`Sales report MO_${decoded.moCode} for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
          res.status(200).send(data);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
    }
  }

  /**
   * GET /sales-reports/location-code
   * get all location code of sales report
   * @param {*} req 
   * @param {*} res 
   */
  getLocationCode(req, res) {
    loggingHelper.log('info', 'getLocationCode start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // let params = req.query;
    let options = {where: {}};
    // let role = decoded.adminRole;

    options.where = Object.assign(options.where, {eventLocationCode: {$not: null}});

    this.db.SaleReport.findAll(options)
      .then(result => {
        let tempResult = [];
        if(result) {
          result.forEach(element => {
            if(!tempResult.filter(tempRes => tempRes === element.eventLocationCode)[0]) {
              tempResult.push(element.eventLocationCode);
            }
          });
        }
        res.json(tempResult);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new SalesReportApi(...args);
}
