import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import { loggingHelper } from '../helpers/logging-helper';

class SellerApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    // term
    this.api.get('/terms', this.getTerm.bind(this));
    this.api.get('/admin-terms', JwtMiddleware.verifyAdminToken, this.getAdminTerms.bind(this));
    this.api.post('/admin-terms', JwtMiddleware.verifyAdminToken, this.postTerm.bind(this));
    this.api.put('/admin-terms/:id', JwtMiddleware.verifyAdminToken, this.editTerm.bind(this));
    this.api.get('/admin-terms/:id', JwtMiddleware.verifyAdminToken, this.getTermDetail.bind(this));

    // privacy
    this.api.get('/privacy', this.getPrivacy.bind(this));
    this.api.get('/admin-privacy', JwtMiddleware.verifyAdminToken, this.getAdminPrivacys.bind(this));
    this.api.post('/admin-privacy', JwtMiddleware.verifyAdminToken, this.postPrivacy.bind(this));
    this.api.put('/admin-privacy/:id', JwtMiddleware.verifyAdminToken, this.editPrivacy.bind(this));
    this.api.get('/admin-privacy/:id', JwtMiddleware.verifyAdminToken, this.getPrivacyDetail.bind(this));
  }

  /**
   * GET /terms
   * get term and condition of specify country
   * @param {*} req 
   * @param {*} res 
   */
  getTerm(req, res) {
    loggingHelper.log('info', 'getTerm');
    let params = req.query;
    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();
    this.db.Term.findOne({where: {
      CountryId: req.country.id,
      langCode: params.langCode
    }})
      .then(term => ResponseHelper.responseSuccess(res, term))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /admin-term
   * get all term condition
   * @param {*} req 
   * @param {*} res 
   */
  getAdminTerms(req, res) {
    loggingHelper.log('info', 'getTerm');
    this.db.Term.findAll({include: ['Country']})
      .then(terms => ResponseHelper.responseSuccess(res, terms))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /terms
   * allow admin create term for a country
   * @param {*} req 
   * @param {*} res 
   */
  postTerm(req, res) {
    loggingHelper.log('info', `postTerm === ${req.body}`);
    let params = req.body;
    params.langCode = params.langCode ? params.langCode.toUpperCase() : null;
    this.db.Term.findOne({where: {
      CountryId: params.CountryId,
      langCode: params.langCode
    }})
      .then(term => {
        if(term) {
          throw new Error('existed');
        } else {
          return this.db.Term.create(params);
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'existed') {
          return ResponseHelper.responseError(res, 'This country already have term');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /terms/:id
   * allow admin update term
   * @param {*} req 
   * @param {*} res 
   */
  editTerm(req, res) {
    loggingHelper.log('info', `postTerm === ${req.body}`);
    let params = req.body;
    params.langCode = params.langCode ? params.langCode.toUpperCase() : null;
    this.db.Term.update(params, {where: {id: req.params.id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /terms/:id
   * allow admin get term detail
   * @param {*} req 
   * @param {*} res 
   */
  getTermDetail(req, res) {
    loggingHelper.log('info', 'getTermDetail');
    this.db.Term.findOne({where: {id: req.params.id}})
      .then(term => ResponseHelper.responseSuccess(res, term))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /privacy
   * get privacy policy of specify country
   * @param {*} req 
   * @param {*} res 
   */
  getPrivacy(req, res) {
    loggingHelper.log('info', 'getPrivacy');
    let params = req.query;
    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();
    this.db.Privacy.findOne({where: {
      CountryId: req.country.id,
      langCode: params.langCode
    }})
      .then(privacy => ResponseHelper.responseSuccess(res, privacy))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /admin-privacy
   * get all privacy condition
   * @param {*} req 
   * @param {*} res 
   */
  getAdminPrivacys(req, res) {
    loggingHelper.log('info', 'getAdminPrivacys');
    this.db.Privacy.findAll({include: ['Country']})
      .then(privacy => ResponseHelper.responseSuccess(res, privacy))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /privacy
   * allow admin create privacy for a country
   * @param {*} req 
   * @param {*} res 
   */
  postPrivacy(req, res) {
    loggingHelper.log('info', `postPrivacy === ${req.body}`);
    let params = req.body;
    params.langCode = params.langCode ? params.langCode.toUpperCase() : null;
    this.db.Privacy.findOne({where: {
      CountryId: params.CountryId,
      langCode: params.langCode
    }})
      .then(privacy => {
        if(privacy) {
          throw new Error('existed');
        } else {
          return this.db.Privacy.create(params);
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'existed') {
          return ResponseHelper.responseError(res, 'This country already have privacy');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /privacy/:id
   * allow admin update privacy
   * @param {*} req 
   * @param {*} res 
   */
  editPrivacy(req, res) {
    loggingHelper.log('info', `postPrivacy === ${req.body}`);
    let params = req.body;
    params.langCode = params.langCode ? params.langCode.toUpperCase() : null;
    this.db.Privacy.update(params, {where: {id: req.params.id}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /admin-privacy/:id
   * allow admin get privacy detail
   * @param {*} req 
   * @param {*} res 
   */
  getPrivacyDetail(req, res) {
    loggingHelper.log('info', 'getPrivacyDetail');
    this.db.Privacy.findOne({where: {id: req.params.id}})
      .then(privacy => ResponseHelper.responseSuccess(res, privacy))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
}

export default function(...args) {
  return new SellerApi(...args);
}
