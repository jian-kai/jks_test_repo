import Promise from 'bluebird';
import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import { loggingHelper } from '../helpers/logging-helper';
import SequelizeHelper from '../helpers/sequelize-helper';

class MarketingOfficeApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/marketing-offices', JwtMiddleware.verifyAdminToken, this.getMarketingOffices.bind(this));
    // this.api.get('/marketing-offices/:roleId', JwtMiddleware.verifyAdminToken, this.getMarketingOfficesDetails.bind(this));
    // this.api.post('/marketing-offices', JwtMiddleware.verifyAdminToken, this.postMarketingOffices.bind(this));
    // this.api.put('/marketing-offices/:id', JwtMiddleware.verifyAdminToken, this.putMarketingOffices.bind(this));
  }

  /**
   * GET /marketing-offices
   * get all marketing offices
   * @param {*} req 
   * @param {*} res 
   */
  getMarketingOffices(req, res) {
    loggingHelper.log('info', 'getMarketingOffices start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    // build limit and off set
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Product);
    
    // find role base on limit and offset
    this.db.MarketingOffice.findAll(options)
    .then(marketingOffices => res.json(marketingOffices))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /roles/:roleId
   * get role details in DB
   */
  getAdminRoleDetails(req, res) {
    loggingHelper.log('info', 'getAdminRoleDetails start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    let includeClause = this._getRoleInclude();
    
    // perform query
    this.db.Role.findOne({where: {
      id: req.params.roleId
    }, include: includeClause})
      .then(role => {
        if(role) {
          res.json(role);
        } else {
          ResponseHelper.responseNotFoundError(res, `Role(${params.productId})`);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /roles
   * @param {*} req 
   * @param {*} res 
   */
  postAdminRole(req, res) {
    loggingHelper.log('info', 'postAdminRole start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    
    let role;
    this.db.sequelize.transaction(t => this.db.Role.create(params, {returning: true, transaction: t})
      .then(_role => {
        role = _role;
        let roleDetails = [];
        // build ProductImages data
        params.roleDetails.forEach(roleDetail => {
          roleDetail.RoleId = role.id;
          roleDetails.push(roleDetail);
        });

        // insert Role data
        return this.db.RoleDetail.bulkCreate(roleDetails, {transaction: t});
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      }));
  }

  /**
   * POST /roles/:id
   * @param {*} req 
   * @param {*} res 
   */
  putAdminRole(req, res) {
    loggingHelper.log('info', 'putAdminRole start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let roleDetailsToUpdate;
    let pathIdList = [];
    params.roleDetails.forEach(element => {
      pathIdList.push(element.PathId);
    });

    this.db.RoleDetail.findAll({where: {PathId: {$in: pathIdList}, RoleId: req.params.id}})
    .then(result => {
      roleDetailsToUpdate = SequelizeHelper.convertSeque2Json(result);
      return this.db.sequelize.transaction(t => this.db.Role.update(params, {where: {id: req.params.id}}, {returning: true, transaction: t})
      .then(() => {
        let roleDetailsUpdate = [];
        let roleDetailsCreate = [];
        // build ProductImages data
        params.roleDetails.forEach(roleDetail => {
          roleDetail.RoleId = req.params.id;
          if(roleDetailsToUpdate.filter(item => item.PathId === roleDetail.PathId)[0]) {
            roleDetailsUpdate.push(roleDetail);
          } else {
            roleDetailsCreate.push(roleDetail);
          }
        });
        
        // update Role data
        // return this.db.RoleDetail.bulkCreate(roleDetails, {transaction: t});
        return Promise.map(roleDetailsUpdate, item => {
          let _option = Object.assign({where: {PathId: item.PathId, RoleId: req.params.id}}, {transaction: t});
          return this.db.RoleDetail.update(item, _option);
        })
        .then(() => this.db.RoleDetail.bulkCreate(roleDetailsCreate, {transaction: t}));
      }))
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
    });
  }

}

export default function(...args) {
  return new MarketingOfficeApi(...args);
}
