import CryptoUtil from 'crypto-js';
import moment from 'moment';
import jwt from 'jsonwebtoken';

import config from '../config';
import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import SendEmailQueueService from '../services/send-email.queue.service';
import { loggingHelper } from '../helpers/logging-helper';

class UserAuthenticationApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/admin/login', this.adminLogin.bind(this));
    this.api.post('/admin/reset-password', this.resetPassword.bind(this));
    this.api.post('/admin/update-password', this.updatePasswordReset.bind(this));
    this.api.post('/admin/validate-token', this.validateToken.bind(this));
    this.api.get('/admin/roles', JwtMiddleware.verifyAdminToken, this.getAdminRoles.bind(this));
    this.api.post('/admin', JwtMiddleware.verifyAdminToken, this.postAdmin.bind(this));
    this.api.get('/admin', JwtMiddleware.verifyAdminToken, this.getAdmins.bind(this));
    this.api.get('/admin/:id', JwtMiddleware.verifyAdminToken, this.getAdminDetails.bind(this));
    this.api.put('/admin/:id', JwtMiddleware.verifyAdminToken, this.putAdmin.bind(this));
    this.api.post('/admin/:id/password', JwtMiddleware.verifyAdminToken, this.updatePassword.bind(this));
  }

  /** POST /admin/login
   * @param: email
   * @param: password
   * @return: admin object
   */
  adminLogin(req, res) {
    loggingHelper.log('info', 'adminLogin start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    if(!params.email || !params.password) {
      return ResponseHelper.responseMissingParam(res);
    }

    // encrypt password
    params.password = CryptoUtil.MD5(params.password).toString();

    this.db.Admin.findOne({
      where: {
        email: params.email,
        password: params.password
      }, include: [
        'Country',
        'MarketingOffice',
        {
          model: this.db.Role,
          as: 'Role',
          include: [
            {
              model: this.db.RoleDetail,
              as: 'roleDetails',
              include: [
                'Path',
              ]
            }
          ]
        }
      ]
    })
      .then(result => {
        if(!result) {
          ResponseHelper.responseError(res, 'Email or password do not match');
        } else if(!result.isActive) {
          ResponseHelper.responseError(res, 'You account is not activated.');
        } else {
          return res.json(JwtMiddleware.generateToken(result, true));
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /admin/login
   * @param: req
   * @param: res
   * @return: admin object
   */
  postAdmin(req, res) {
    loggingHelper.log('info', 'postAdmin start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let decoded = req.decoded;
    let role = decoded.adminRole;

    // validation role
    // if(role === this.db.Admin.rawAttributes.role.values[1] && params.role && params.role !== this.db.Admin.rawAttributes.role.values[2]) {
    //   return ResponseHelper.responsePermissonDenied(res);
    // } else if(role === this.db.Admin.rawAttributes.role.values[2]) {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }
    console.log(`postAdmin   ${role}`);
    if(role.indexOf('admin') === -1) {
      return ResponseHelper.responsePermissonDenied(res);
    }

    // set value for CountryId
    // if(!decoded.viewAllCountry) {
    //   params.CountryId = decoded.CountryId;
    // }

    // encrypt password
    params.password = CryptoUtil.MD5(params.password).toString();

    // check viewAllCountry field value for null or undefined ...
    params.viewAllCountry = params.viewAllCountry ? params.viewAllCountry : false;

    this.db.Admin.build(params).validate()
      .then(() => this.db.Admin.findOne({where: {email: params.email}}))
      .then(result => {
        if(!result) {
          return this.db.Admin.create(params, {returning: true});
        } else {
          throw new Error('email_existed');
        }
      })
      .then(admin => res.json(admin))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, 'email is existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** GET /admin
   * @param: req
   * @param: res
   * @return: list of admin object
   */
  getAdmins(req, res) {
    loggingHelper.log('info', 'getAdmins start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    let params = req.query;
    let options = {include: ['Country', 'Role']};

    if(params.email && params.email !== '') {
      options.where = {email: {$like: `%${params.email}%`}};
      Reflect.deleteProperty(params, 'email');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Admin);
    
    // if(role === this.db.Admin.rawAttributes.role.values[1]) {
    //   options.where = {CountryId: decoded.CountryId, role: this.db.Admin.rawAttributes.role.values[2]};
    // } else if(role === this.db.Admin.rawAttributes.role.values[2]) {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }
    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {CountryId: decoded.CountryId});
    }

    console.log(`option ${JSON.stringify(options)}`);
    loggingHelper.log('info', `option ${JSON.stringify(options)}`);

    this.db.Admin.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /admin:/:id
   * @param: req
   * @param: res
   * @return: get admin detail info
   */
  getAdminDetails(req, res) {
    loggingHelper.log('info', 'getAdminDetails start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    let options = {where: {id: req.params.id}, include: ['Country']};
    // if(role === this.db.Admin.rawAttributes.role.values[1] && decoded.id !== +req.params.id) {
    //   options.where = Object.assign(options.where, {CountryId: decoded.CountryId, role: this.db.Admin.rawAttributes.role.values[2]});
    // } else if(role === this.db.Admin.rawAttributes.role.values[2] && decoded.id !== req.params.id) {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }
    // if(role !== 'super admin') {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }

    // set value for CountryId
    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {CountryId: decoded.CountryId});
    }

    this.db.Admin.findOne(options)
      .then(result => {
        if(result) {
          res.json(result);
        } else {
          ResponseHelper.responseNotFoundError(res, this.db.Admin.name);
        }
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /admin:/:id
   * @param: req
   * @param: res
   * @return: get all role of admin
   */
  getAdminRoles(req, res) {
    loggingHelper.log('info', 'getAdminRoles start');
    // let role = req.decoded.adminRole;
    // if(role === 'super admin') {
    this.db.Role.findAll()
    .then(roles => res.json(roles));
    // } else {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }
  }

  /** POST /admin/login
   * @param: req
   * @param: res
   * @return: admin object
   */
  putAdmin(req, res) {
    loggingHelper.log('info', 'putAdmin start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let decoded = req.decoded;
    let role = decoded.adminRole;
    let options = {where: {id: req.params.id}, raw: true};

    // validate role
    // if(role === this.db.Admin.rawAttributes.role.values[1] && decoded.id !== +req.params.id) {
    //   options.where = Object.assign(options.where, {CountryId: decoded.CountryId, role: this.db.Admin.rawAttributes.role.values[2]});
    // } else if(role === this.db.Admin.rawAttributes.role.values[2] && decoded.id !== req.params.id) {
    //   return ResponseHelper.responsePermissonDenied(res);
    // }
    if(role.indexOf('admin') === -1 && decoded.id !== +req.params.id) {
      return ResponseHelper.responsePermissonDenied(res);
    }

    // // set value for CountryId
    // if(!decoded.viewAllCountry) {
    //   params.CountryId = decoded.CountryId;
    // }

    // remove password
    if(params.password) {
      Reflect.deleteProperty(params, 'password');
    }
    // if(params.CountryId) {
    //   Reflect.deleteProperty(params, 'CountryId');
    // }

    console.log(`options === ${JSON.stringify(options)} === ${JSON.stringify(decoded)}`);
    loggingHelper.log('info', `options === ${JSON.stringify(options)} === ${JSON.stringify(decoded)}`);

    this.db.Admin.findOne(options)
      .then(admin => {
        if(admin) {
          return this.db.Admin.update(params, {where: {
            id: req.params.id
          }, returning: true});
        } else {
          throw new Error('not_found');
        }
      })
      .then(() => this.db.Admin.findById(req.params.id))
      .then(admin => res.json(admin))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          console.log(`eerrooror == ${JSON.stringify(error)}`);
          loggingHelper.log('error', `eerrooror == ${JSON.stringify(error)}`);
          return ResponseHelper.responseError(res, 'email is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** POST /admin/login
   * @param: req
   * @param: res
   * @return: admin object
   */
  updatePassword(req, res) {
    loggingHelper.log('info', 'updatePassword start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // remove password
    if(!params.oldPassword || !params.newPassword) {
      return ResponseHelper.responseMissingParam(res);
    }

    // encrypt password
    params.oldPassword = CryptoUtil.MD5(params.oldPassword).toString();
    params.newPassword = CryptoUtil.MD5(params.newPassword).toString();

    this.db.Admin.findOne({where: {
      id: req.params.id,
      password: params.oldPassword
    }, raw: true})
      .then(admin => {
        if(admin) {
          return this.db.Admin.update({password: params.newPassword}, {where: {
            id: req.params.id
          }});
        } else {
          throw new Error('password_incorrect');
        }
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        console.log(`error == ${error.name} == ${error.stack || error}`);
        loggingHelper.log('error', `error == ${error.name} == ${error.stack || error}`);
        if(error.message === 'password_incorrect') {
          return ResponseHelper.responseError(res, 'old password is incorrect');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** POST /admin/login
   * @param: req
   * @param: res
   * @return: admin object
   */
  resetPassword(req, res) {
    loggingHelper.log('info', 'resetPassword start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;    
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    this.db.Admin.findOne({where: {
      email: params.email
    }, include: ['Country']})
      .then(admin => {
        if(admin) {
          // send reset password email
          // return emailHelper.sendResetAdminPassword(admin);
          return sendEmailQueueService.addTask({method: 'sendResetAdminPassword', data: admin});
        } else {
          throw new Error('email_not_existed');
        }
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        console.log(`error == ${error.name} == ${error.stack || error}`);
        loggingHelper.log('error', `error == ${error.name} == ${error.stack || error}`);
        if(error.message === 'email_not_existed') {
          return ResponseHelper.responseError(res, 'email does not existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /admin/validate-token
   * check token is expired or not
   * @param {*} req 
   * @param {*} res 
   */
  validateToken(req, res) {
    loggingHelper.log('info', 'validateToken start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // check token
    let token = req.body.token;
    if(!token) {
      return ResponseHelper.responseMissingParam(res);
    }
    jwt.verify(token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseErrorParam(res, 'Token is invalid');
      } else if(moment(decoded.token).isBefore(moment())) {
        return ResponseHelper.responseErrorParam(res, 'Token is expired');
      } else {
        res.json({ok: true});
      }
    });
  }

  /** POST /admin/update-password
   * update password depend on reset password token
   * @param: new password
   */
  updatePasswordReset(req, res) {
    loggingHelper.log('info', 'updatePasswordReset start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // check token
    let params = req.body;
    if(!params.password || !params.token) {
      return ResponseHelper.responseMissingParam(res);
    }
    let password = CryptoUtil.MD5(params.password).toString();
    jwt.verify(params.token, config.accessToken.secret, (err, decoded) => {
      if(err) {
        return ResponseHelper.responseError(res, 'Token is invalid');
      } else if(moment(decoded.token).isBefore(moment())) {
        return ResponseHelper.responseError(res, 'Token is expired');
      } else {
        this.db.Admin.update({
          password
        }, {where: {
          email: decoded.email
        }})
          .then(() => {
            res.json({ok: true});
          })
          .catch(error => ResponseHelper.responseInternalServerError(res, error));
      }
    });
  }
}

export default function(...args) {
  return new UserAuthenticationApi(...args);
}
