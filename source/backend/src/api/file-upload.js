import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import Validator from '../helpers/validator';
import moment from 'moment';
// import json2csv from 'json2csv';
import { loggingHelper } from '../helpers/logging-helper';

class FileUploadApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/file-upload/check-tax-invoice-exists', JwtMiddleware.verifyAdminToken, this.checkTaxInvoiceExists.bind(this));
    this.api.get('/file-upload/download/tax-invoice/:id', JwtMiddleware.verifyAdminToken, this.downloadTaxInvoice.bind(this));
    this.api.get('/file-upload/download/bulk-tax-invoice/:id', JwtMiddleware.verifyAdminToken, this.downloadBulkTaxInvoice.bind(this));
    this.api.get('/file-upload/download/:id', JwtMiddleware.verifyAdminToken, this.downloadFile.bind(this));
    
    this.api.get('/file-upload/:adminId', JwtMiddleware.verifyAdminToken, this.getFileToDownload.bind(this));
  }

  /**
   * GET /file-upload
   * get all file can be download
   * @param {*} req 
   * @param {*} res 
   */
  getFileToDownload(req, res) {
    loggingHelper.log('info', 'getFileToDownload start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {isDownloaded: true}};
    // let decoded = req.decoded;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!req.params.adminId) {
      return ResponseHelper.responseMissingParam(res);
    }
    options.where = Object.assign(options.where, {AdminId: req.params.adminId});

    if(params.name && params.name !== '') {
      options.where = Object.assign(options.where, {name: {$like: `%${params.name}%`}});
      Reflect.deleteProperty(params, 'name');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.FileUpload);

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: new Date(moment(params.fromDate))}},
        {createdAt: {$lt: new Date(moment(params.toDate).add(1, 'day'))}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: new Date(moment(params.fromDate))}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: new Date(moment(params.toDate).add(1, 'day'))}});
    }

    this.db.FileUpload.findAndCountAll(options)
    .then(result => res.json(result))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /file-upload/check-tax-invoice-exists
   * check tax invoice exists
   * @param {*} req 
   * @param {*} res 
   */
  checkTaxInvoiceExists(req, res) {
    loggingHelper.log('info', 'checkTaxInvoiceExists start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};

    options = RequestHelper.buildWhereCondition(options, params, this.db.FileUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);

    this.db.FileUpload.findOne(options)
    .then(result => {
      if(result) {
        return res.json({ok: true});
      }
      return res.json({ok: false});
    })
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /file-upload/download/tax-invoice/:id
   * download tax invoice of order
   * @param {*} req 
   * @param {*} res 
   */
  downloadTaxInvoice(req, res) {
    loggingHelper.log('info', 'downloadTaxInvoice start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {orderId: req.params.id}};

    options = RequestHelper.buildWhereCondition(options, params, this.db.FileUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);
    
    this.db.FileUpload.findOne(options)
    .then(result => res.json(result))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /file-upload/download/bulk-tax-invoice/:id
   * download bulk tax invoice of order
   * @param {*} req 
   * @param {*} res 
   */
  downloadBulkTaxInvoice(req, res) {
    loggingHelper.log('info', 'downloadBulkTaxInvoice start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {bulkOrderId: req.params.id}};

    options = RequestHelper.buildWhereCondition(options, params, this.db.FileUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);
    
    this.db.FileUpload.findOne(options)
    .then(result => res.json(result))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /file-upload/download/:id
   * download file
   * @param {*} req 
   * @param {*} res 
   */
  downloadFile(req, res) {
    loggingHelper.log('info', 'downloadFile start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {id: req.params.id}};

    options = RequestHelper.buildWhereCondition(options, params, this.db.FileUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);
    
    this.db.FileUpload.findOne(options)
    .then(result => res.json(result))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new FileUploadApi(...args);
}
