import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import Validator from '../helpers/validator';
import moment from 'moment';
import json2csv from 'json2csv';
import { loggingHelper } from '../helpers/logging-helper';
import OrderHelper from '../helpers/order-helper';

class OrderUploadApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/order-upload', JwtMiddleware.verifyAdminToken, this.getOrderUpload.bind(this));
    this.api.get('/order-upload/download', JwtMiddleware.verifyAdminToken, this.downloadOrderUpload.bind(this));
  }

  /**
   * GET /order-upload
   * get order upload from start date to end date
   * @param {*} req 
   * @param {*} res 
   */
  getOrderUpload(req, res) {
    loggingHelper.log('info', 'getOrderUpload start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(params.orderNumber && params.orderNumber !== '') {
      options.where = Object.assign(options.where, {'orderNumber': {$like: `%${params.orderNumber}%`}});
      Reflect.deleteProperty(params, 'orderNumber');
    }

    // filter by status
    if(params.states && params.states !== '' && params.states !== 'all') {
      options.where = Object.assign(options.where, {status: {$like: `${params.states}`}});
      Reflect.deleteProperty(params, 'states');
    }

    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {orderNumber: {$like: `%${params.s}%`}},
          {sku: {$like: `%${params.s}%`}}
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    options = RequestHelper.buildWhereCondition(options, params, this.db.OrderUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    this.db.Country.findOne({where: {
      id: req.decoded.CountryId}
    })
    .then(country => {
      if(!decoded.viewAllCountry) {
        options.where = Object.assign(options.where, {$and: [{ region: country.code }]});
      }
      return this.db.OrderUpload.findAndCountAll(options);
    })
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /order-upload/download
   * download sales report
   * @param {*} req 
   * @param {*} res 
   */
  downloadOrderUpload(req, res) {
    loggingHelper.log('info', 'downloadOrderUpload start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }

    if(!decoded.viewAllCountry) {
      options.where = { CountryId: req.decoded.CountryId };
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    params.toDate = params.toDate ? params.toDate : moment().format('YYYY-MM-DD');
    options = RequestHelper.buildWhereCondition(options, params, this.db.OrderUpload);
    options = RequestHelper.buildSequelizeOptions(options, params);

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }
    
    this.db.Country.findOne({where: {
      id: req.decoded.CountryId}
    })
    .then(country => {
      if(!decoded.viewAllCountry) {
        options.where = Object.assign(options.where, {$and: [{ region: country.code }]});
      }
      return this.db.OrderUpload.findAll(options);
    })
    // this.db.OrderUpload.findAll(options)
      .then(result => {
        // result.forEach(order => {
        //   order.orderNumber = OrderHelper.fromatOrderNumber(order, true);
        // });
        // create CSV and response file
        let fields;
        let fieldNames;
        fields = ['orderNumber', 'doFileName', 'wareHouseFileName', 'sku', 'qty', 'deliveryId', 'carrierAgent', 'status', 'createdAt'];
        fieldNames = ['Order Number', 'DO File Name', 'WareHouse File Name', 'Sku', 'Qty', 'Tracking Number', 'Carrier Agent', 'Order Status', 'Order Date'];
        let data = json2csv({
          data: result,
          fields,
          fieldNames
        });
        // send CSV file
        res.attachment(`Sales report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new OrderUploadApi(...args);
}
