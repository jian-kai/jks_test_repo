import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import { loggingHelper } from '../helpers/logging-helper';
import { awsHelper } from '../helpers/aws-helper';
import multipart from 'connect-multiparty';
import config from '../config';

let multipartMiddleware = multipart();

class ArticleAPI {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/article-types', this.getArticleType.bind(this));
    this.api.get('/admin-article-types', JwtMiddleware.verifyAdminToken, this.getAdminArticleTypes.bind(this));
    this.api.post('/admin-article-types', JwtMiddleware.verifyAdminToken, this.postArticleType.bind(this));
    this.api.put('/admin-article-types/:id', JwtMiddleware.verifyAdminToken, this.updateArticleType.bind(this));
    this.api.delete('/admin-article-types/:id', JwtMiddleware.verifyAdminToken, this.deleteArticleType.bind(this));
    this.api.get('/articles', this.getArticles.bind(this));
    this.api.get('/articles/:articleId', this.getArticleDetails.bind(this));
    this.api.get('/articles-by-type/:articleTypeId', this.getArticleByType.bind(this));
    this.api.get('/articles-featured', this.getArticleFeatured.bind(this));
    this.api.get('/admin-articles', JwtMiddleware.verifyAdminToken, this.getAdminArticle.bind(this));
    this.api.get('/admin-articles/:id', JwtMiddleware.verifyAdminToken, this.getAdminArticleDetails.bind(this));
    this.api.post('/admin-articles', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.postArticle.bind(this));
    this.api.put('/admin-articles/:id', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.updateArticle.bind(this));
    this.api.delete('/admin-articles/:id', JwtMiddleware.verifyAdminToken, this.deleteArticle.bind(this));
  }

  /**
   * Find all article in DB depend on the options arguments
   * @param options <optional>
   */
  _findArticles(options) {
    // build output value
    Object.assign(options, {attributes: {
      exclude: ['ArticleTypeId']
    }, include: [
      {
        model: this.db.ArticleType,
        as: 'ArticleType',
        include: ['Country']
      }
    ]});

    // perform query
    return this.db.Article.findAll(options);
  }

  /** GET /article-types
   * get all article-types in DB
   */
  getArticleType(req, res) {
    loggingHelper.log('info', 'getArticleType start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.query;

    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();

    let options = RequestHelper.buildWhereCondition({}, params, this.db.Faq);
    options.include = ['Country'];

    options.where = Object.assign(options.where, {
      CountryId: req.country.id,
      langCode: params.langCode
    });
    
    this.db.ArticleType.findAll(options)
      .then(articleTypes => {
        res.json(articleTypes);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-article-types
   * get all article-types in DB
   */
  getAdminArticleTypes(req, res) {
    loggingHelper.log('info', 'getAdminArticleTypes start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;

    let options = {};

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.ArticleType);
    options.include = ['Country'];

    this.db.ArticleType.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /admin-article-types
   * create new article type
   */
  postArticleType(req, res) {
    loggingHelper.log('info', 'postArticleType start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.ArticleType.build(params).validate()
      .then(() => this.db.ArticleType.create(params, {returning: true}))
      .then(articleType => res.json(articleType))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.message === 'name_existed') {
          return ResponseHelper.responseError(res, 'name is existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** PUT /admin-article-types/:id
   * update article type
   */
  updateArticleType(req, res) {
    loggingHelper.log('info', 'updateArticleType start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;    
    this.db.ArticleType.update(params, {where: {
      id: req.params.id
    }})
      .then(() => this.db.ArticleType.findById(req.params.id))
      .then(result => res.json(result))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'code or name is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** DELETE /admin-article-types/:id
   * delete article type
   */
  deleteArticleType(req, res) {
    loggingHelper.log('info', 'deleteArticleType start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.Article.findAll({where: {ArticleTypeId: id}})
    .then(articles => {
      if(articles) {
        throw new Error('can_not_delete');
      } else {
        return this.db.ArticleType.destroy({where: {id}})
        .then(() => ResponseHelper.responseSuccess(res));
      }
    })
    .catch(error => {
      if(error.message === 'can_not_delete') {
        return ResponseHelper.responseError(res, 'can not delete. Article Type has article.');
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }

  /** GET /articles
   * get all articles in DB
   */
  getArticles(req, res) {
    loggingHelper.log('info', 'getArticles start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.query || {};
    let options = RequestHelper.buildSequelizeOptions({}, params, this.db.Article);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Article);

    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();

    options.where = Object.assign(options.where, {
      '$ArticleType.CountryId$': req.country.id,
      '$ArticleType.langCode$': params.langCode
    });

    this._findArticles(options)
      .then(articles => res.json(articles))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /articles-by-type/:articleTypeId
   * get all articles by Type in DB
   */
  getArticleByType(req, res) {
    loggingHelper.log('info', 'getArticles start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.query || {};
    let options = RequestHelper.buildSequelizeOptions({}, params, this.db.Article);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Article);

    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();

    options.where = Object.assign(options.where, {
      '$ArticleType.CountryId$': req.country.id,
      '$ArticleType.langCode$': params.langCode,
      'ArticleTypeId': req.params.articleTypeId
    });

    this._findArticles(options)
      .then(articles => res.json(articles))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /articles-featured
   * get all articles featured in DB
   */
  getArticleFeatured(req, res) {
    loggingHelper.log('info', 'getArticleFeatured start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.query || {};
    let options = RequestHelper.buildSequelizeOptions({}, params, this.db.Article);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Article);

    // init data for langcode, set EN as default
    params.langCode = params.langCode ? params.langCode.toUpperCase() : req.country.defaultLang.toUpperCase();

    options.where = Object.assign(options.where, {
      '$ArticleType.CountryId$': req.country.id,
      '$ArticleType.langCode$': params.langCode,
      '$isFeatured$': true
    });

    this._findArticles(options)
      .then(articles => res.json(articles))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /articles/:articleId
   * get article details in DB
   */
  getArticleDetails(req, res) {
    loggingHelper.log('info', 'getArticleDetails start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);

    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }
    // perform query
    this.db.Article.findOne({where: {
      id: req.params.articleId
    }, attributes: {
      exclude: ['ArticleTypeId']
    }, include: [
      { all: true }
    ]}, { raw: true })
      .then(article => this._findArticles({where: {
        ArticleTypeId: article.ArticleType.id,
        id: { $ne: article.id }
      }}, countryCode)
          .then(articles => {
            article = JSON.parse(JSON.stringify(article));
            article.relatedArticles = articles;
            return article;
          })
      )
      .then(article => {
        res.json(article);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-articles
   * get all articles in DB
   */
  getAdminArticle(req, res) {
    loggingHelper.log('info', 'getAdminArticle start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.query || {};
    let options = RequestHelper.buildSequelizeOptions({}, params, this.db.Article);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Article);

    Object.assign(options, {attributes: {
      exclude: ['ArticleTypeId']
    }, include: [
      {
        model: this.db.ArticleType,
        as: 'ArticleType',
        include: ['Country']
      }
    ]});

    this.db.Article.findAndCountAll(options)
      .then(articles => res.json(articles))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-articles/:id
   * get article details in DB
   */
  getAdminArticleDetails(req, res) {
    loggingHelper.log('info', 'getAdminArticleDetails start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    // let params = req.query || {};
    let options = {
      where: {id: req.params.id},
      include: [
        {
          model: this.db.ArticleType,
          as: 'ArticleType',
          include: ['Country']
        }
      ]
    };

    this.db.Article.findOne(options)
      .then(articles => res.json(articles))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /admin-articles
   * create new article
   */
  postArticle(req, res) {
    loggingHelper.log('info', 'postArticle start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.bannerUrl;
    
    // validate images
    if(!image) {
      return ResponseHelper.responseMissingParam(res);
    }

    // upload image to S3
    awsHelper.upload(config.AWS.productImagesAlbum, image)
    .then(uploadedImage => {
      params.bannerUrl = uploadedImage.Location;
      this.db.Article.build(params).validate()
        .then(() => this.db.Article.create(params, {returning: true}))
        .then(article => res.json(article));
    })
    .catch(error => {
      if(error.name === 'SequelizeValidationError') {
        return ResponseHelper.responseErrorParam(res, error);
      } else if(error.name === 'SequelizeUniqueConstraintError') {
        return ResponseHelper.responseErrorParam(res, error);
      } else if(error.message === 'name_existed') {
        return ResponseHelper.responseError(res, 'name is existed');
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }

  /** PUT /admin-articles/:id
   * update article
   */
  updateArticle(req, res) {
    loggingHelper.log('info', 'updateArticle start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.bannerUrl;
    
    // validate images
    // if(!image) {
    //   return ResponseHelper.responseMissingParam(res);
    // }

    if(image) {
      // upload image to S3
      awsHelper.upload(config.AWS.productImagesAlbum, image)
      .then(uploadedImage => {
        params.bannerUrl = uploadedImage.Location;
        this.db.Article.findOne({where: {
          id: req.params.id
        }, raw: true})
          .then(article => {
            if(article) {
              // merge current article data
              params = Object.assign(article, params);
              return this.db.Article.build(params).validate();
            } else {
              throw new Error('not_found');
            }
          })
          .then(() => 
            this.db.Article.update(params, {where: {
              id: req.params.id
            }})
          )
          .then(() => this.db.Article.findById(req.params.id))
          .then(result => res.json(result));
      })
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'code or name is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
    } else {
      this.db.Article.findOne({where: {
        id: req.params.id
      }, raw: true})
        .then(article => {
          if(article) {
            // merge current article data
            params = Object.assign(article, params);
            return this.db.Article.build(params).validate();
          } else {
            throw new Error('not_found');
          }
        })
        .then(() => 
          this.db.Article.update(params, {where: {
            id: req.params.id
          }})
        )
        .then(() => this.db.Article.findById(req.params.id))
        .then(result => res.json(result))
        .catch(error => {
          if(error.name === 'SequelizeValidationError') {
            return ResponseHelper.responseErrorParam(res, error);
          } else if(error.name === 'SequelizeUniqueConstraintError') {
            return ResponseHelper.responseError(res, 'code or name is existed');
          } else if(error.message === 'not_found') {
            return ResponseHelper.responseNotFoundError(res, 'Admin');
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
    }
  }

  /** DELETE /admin-articles/:id
   * delete article
   */
  deleteArticle(req, res) {
    loggingHelper.log('info', 'deleteArticle start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.Article.destroy({where: {id}})
    .then(() => ResponseHelper.responseSuccess(res))
    .catch(error => {
      if(error.message === 'can_not_delete') {
        return ResponseHelper.responseError(res, 'can not delete.');
      } else {
        return ResponseHelper.responseInternalServerError(res, error);
      }
    });
  }
}

export default function(...args) {
  return new ArticleAPI(...args);
}
