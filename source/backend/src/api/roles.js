import Promise from 'bluebird';
import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import { loggingHelper } from '../helpers/logging-helper';
import SequelizeHelper from '../helpers/sequelize-helper';

class RoleApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/path', JwtMiddleware.verifyAdminToken, this.getPath.bind(this));
    this.api.post('/path', JwtMiddleware.verifyAdminToken, this.postPath.bind(this));
    this.api.put('/path/:id', JwtMiddleware.verifyAdminToken, this.putPath.bind(this));
    this.api.delete('/path/:id', JwtMiddleware.verifyAdminToken, this.deletePath.bind(this));
    this.api.get('/roles', JwtMiddleware.verifyAdminToken, this.getAdminRole.bind(this));
    this.api.get('/roles/:roleId', JwtMiddleware.verifyAdminToken, this.getAdminRoleDetails.bind(this));
    this.api.post('/roles', JwtMiddleware.verifyAdminToken, this.postAdminRole.bind(this));
    this.api.put('/roles/:id', JwtMiddleware.verifyAdminToken, this.putAdminRole.bind(this));
  }

  _getRoleInclude() {
    let includes = [
      {
        model: this.db.RoleDetail,
        as: 'roleDetails',
        include: [
          'Path',
        ]
      }
    ];
    return includes;
  }

  /**
   * GET /path
   * get all path
   * @param {*} req 
   * @param {*} res 
   */
  getPath(req, res) {
    loggingHelper.log('info', 'getPath start');
    this.db.Path.findAll()
      .then(paths => res.json(paths))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /path
   * @param {*} req 
   * @param {*} res 
   */
  postPath(req, res) {
    loggingHelper.log('info', 'postPath start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.Path.findOne({ where: { pathValue: params.pathValue } })
      .then(result => {
        if(!result) {
          return this.db.Path.create(params, {returning: true});
        } else {
          throw new Error('path_existed');
        }
      })
      .then(path => {
        res.json(path);
      })
      .catch(err => {
        if(err.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, err);
        } else if(err.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, err);
        } else if(err.message === 'path_existed') {
          return ResponseHelper.responseError(res, 'path is existed'); 
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }

  /**
   * PUT /path/:id
   * @param {*} req 
   * @param {*} res 
   */
  putPath(req, res) {
    loggingHelper.log('info', 'putPath start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.Path.update(params, {where: {id: req.params.id}, returning: true})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(err => {
        if(err.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, err);
        } else if(err.name === 'SequelizeUniqueConstraintError') {
          // return ResponseHelper.responseError(res, err);
          return ResponseHelper.responseError(res, 'path is existed'); 
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }

  /**
   * DELETE /path/:id
   * @param {*} req 
   * @param {*} res 
   */
  deletePath(req, res) {
    loggingHelper.log('info', 'deletePath start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);

    this.db.RoleDetail.destroy({where: {PathId: req.params.id}})
      .then(() => this.db.Path.destroy({where: {id: req.params.id}}))
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /roles
   * get all roles
   * @param {*} req 
   * @param {*} res 
   */
  getAdminRole(req, res) {
    loggingHelper.log('info', 'getAdminRole start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query || {};
    // build limit and off set
    let options = {};
    let returnValue;
    // if(params.sku) {
    //   params.sku = {$like: `%${params.sku}%`};
    // }
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Product);
    
    // find role base on limit and offset
    this.db.Role.findAndCountAll(options)
    .then(result => {
      returnValue = result;
      return Promise.mapSeries(result.rows, role => {
        let _options = {where: {
          id: role.id
        }, include: this._getRoleInclude()};
        return this.db.Role.findOne(_options);
      });
    })
    .then(result => {
      returnValue.rows = result;
      return res.json(returnValue);
    })
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /roles/:roleId
   * get role details in DB
   */
  getAdminRoleDetails(req, res) {
    loggingHelper.log('info', 'getAdminRoleDetails start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    let includeClause = this._getRoleInclude();
    
    // perform query
    this.db.Role.findOne({where: {
      id: req.params.roleId
    }, include: includeClause})
      .then(role => {
        if(role) {
          res.json(role);
        } else {
          ResponseHelper.responseNotFoundError(res, `Role(${params.productId})`);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /roles
   * @param {*} req 
   * @param {*} res 
   */
  postAdminRole(req, res) {
    loggingHelper.log('info', 'postAdminRole start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    
    let role;
    this.db.sequelize.transaction(t => this.db.Role.create(params, {returning: true, transaction: t})
      .then(_role => {
        role = _role;
        let roleDetails = [];
        // build ProductImages data
        params.roleDetails.forEach(roleDetail => {
          roleDetail.RoleId = role.id;
          roleDetails.push(roleDetail);
        });

        // insert Role data
        return this.db.RoleDetail.bulkCreate(roleDetails, {transaction: t});
      })
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          // return ResponseHelper.responseError(res, error);
          return ResponseHelper.responseError(res, 'roleName is existed'); 
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      }));
  }

  /**
   * POST /roles/:id
   * @param {*} req 
   * @param {*} res 
   */
  putAdminRole(req, res) {
    loggingHelper.log('info', 'putAdminRole start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let roleDetailsToUpdate;
    let pathIdList = [];
    params.roleDetails.forEach(element => {
      pathIdList.push(element.PathId);
    });

    this.db.RoleDetail.findAll({where: {PathId: {$in: pathIdList}, RoleId: req.params.id}})
    .then(result => {
      roleDetailsToUpdate = SequelizeHelper.convertSeque2Json(result);
      return this.db.sequelize.transaction(t => this.db.Role.update(params, {where: {id: req.params.id}}, {returning: true, transaction: t})
      .then(() => {
        let roleDetailsUpdate = [];
        let roleDetailsCreate = [];
        // build ProductImages data
        params.roleDetails.forEach(roleDetail => {
          roleDetail.RoleId = req.params.id;
          if(roleDetailsToUpdate.filter(item => item.PathId === roleDetail.PathId)[0]) {
            roleDetailsUpdate.push(roleDetail);
          } else {
            roleDetailsCreate.push(roleDetail);
          }
        });
        
        // update Role data
        // return this.db.RoleDetail.bulkCreate(roleDetails, {transaction: t});
        return Promise.map(roleDetailsUpdate, item => {
          let _option = Object.assign({where: {PathId: item.PathId, RoleId: req.params.id}}, {transaction: t});
          return this.db.RoleDetail.update(item, _option);
        })
        .then(() => this.db.RoleDetail.bulkCreate(roleDetailsCreate, {transaction: t}));
      }))
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          // return ResponseHelper.responseError(res, error);
          return ResponseHelper.responseError(res, 'roleName is existed'); 
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
    });
  }

}

export default function(...args) {
  return new RoleApi(...args);
}
