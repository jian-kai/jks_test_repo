import Promise from 'bluebird';
import _ from 'underscore';

import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import config from '../config';
import { loggingHelper } from '../helpers/logging-helper';

class CountryApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/countries/stripe-pubkey', this.getStripePubkey.bind(this));
    this.api.get('/countries', this.getCountries.bind(this));
    this.api.get('/admin-country-lists', JwtMiddleware.verifyAdminToken, this.getAdminCountryLists.bind(this));
    this.api.get('/directory-countries', this.getDirectoryCountries.bind(this));
    this.api.get('/admin-countries', JwtMiddleware.verifyAdminToken, this.getAdminCountries.bind(this));
    this.api.get('/admin-countries/:id', JwtMiddleware.verifyAdminToken, this.getAdminCountryDetails.bind(this));
    this.api.post('/countries', this.postCountries.bind(this));
    this.api.put('/countries/:id', this.updateCountry.bind(this));
    this.api.get('/countries/states', this.getCountryState.bind(this));
    this.api.get('/countries/shipping-fee', this.getShippingFee.bind(this));
    this.api.get('/countries/subscriber', JwtMiddleware.verifyAdminToken, this.getSubscriberCountries.bind(this));
    this.api.get('/admin-states', JwtMiddleware.verifyAdminToken, this.getAdminStates.bind(this));
    this.api.post('/admin-states', this.postStates.bind(this));
    this.api.post('/admin-countries/change-status', JwtMiddleware.verifyAdminToken, this.postUpdateCountriesStatus.bind(this));
    this.api.put('/admin-states/:id', this.updateState.bind(this));
    this.api.delete('/admin-states/:id', this.deleteState.bind(this));
    // this.api.get('/admin-supported-languages', JwtMiddleware.verifyAdminToken, this.getAdminSupportedLanguages.bind(this));
    this.api.delete('/admin-supported-languages/:id', this.deleteSupportedLanguage.bind(this));
  }

  /** GET /countries
   * get all countries in DB
   */
  getStripePubkey(req, res) {
    loggingHelper.log('info', 'getStripePubkey start');
    let countryCode = req.headers['country-code'];
    let params = req.query;
    let type = params.type;
    if(!type) {
      return ResponseHelper.responseMissingParam(res);
    }

    if(!countryCode || !config.keyStripe.hasOwnProperty(countryCode.toUpperCase())) {
      return res.json({ stripePub: config.keyStripe.default[type].publishableKey });
    } else {
      return res.json({ stripePub: config.keyStripe[countryCode.toUpperCase()][type].publishableKey });
    }
  }

  /** GET /countries
   * get all countries in DB
   */
  getCountries(req, res) {
    loggingHelper.log('info', 'getCountries start');
    this.db.Country.findAll({
      where: {
        isActive: true
      },
      include: [
        {
          model: this.db.DirectoryCountry,
          as: 'DirectoryCountries',
          attributes: {
            exclude: ['id', 'code', 'codeIso2', 'name', 'nativeName', 'currencyCode', 'languageCode', 'callingCode', 'CountryId']
          }
        },
        {
          model: this.db.SupportedLanguage,
        }
      ],
      order: ['order', 'id']
    })
      .then(countries => res.json(countries))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-country-lists
   * get all countries in DB for admin
   */
  getAdminCountryLists(req, res) {
    loggingHelper.log('info', 'getAdminCountryLists start');
    let params = req.query;
    let options = {};
    if(params.CountryId) {
      params.id = params.CountryId;
      Reflect.deleteProperty(params, 'CountryId');
    }
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Country);
    // options.where = Object.assign(options.where, {isActive: true});
    this.db.Country.findAll({
      where: options.where,
      include: [
        {
          model: this.db.DirectoryCountry,
          as: 'DirectoryCountries',
          attributes: {
            exclude: ['id', 'code', 'codeIso2', 'name', 'nativeName', 'currencyCode', 'languageCode', 'callingCode', 'CountryId']
          }
        },
        {
          model: this.db.SupportedLanguage,
        }
      ],
      order: ['order', 'id']
    })
      .then(countries => res.json(countries))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /directory-countries
   * get all directory countries in DB
   */
  getDirectoryCountries(req, res) {
    loggingHelper.log('info', 'getDirectoryCountries start');
    this.db.Country.findAll()
      .then(countries => {
        let names = _.pluck(countries, 'code');
        let options = {
          where: {
            code: { $notIn: [names] }
          }
        };
        return this.db.DirectoryCountry.findAll(options);
      })
      .then(countries => res.json(countries))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-countries
   * get all countries in DB
   */
  getAdminCountries(req, res) {
    loggingHelper.log('info', 'getAdminCountries start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    let responseObj;
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Country);

    options.include = ['States'];
    this.db.Country.findAndCountAll(options)
      .then(result => {
        responseObj = SequelizeHelper.convertSeque2Json(result);
        return Promise.mapSeries(result.rows, country => this.db.Admin.count({ where: { CountryId: country.id } }));
      })
      .then(totalStaffs => {
        let countries = [];
        responseObj.rows.forEach((country, idx) => {
          country.totalStaffs = totalStaffs[idx];
          countries.push(country);
        });
        responseObj.rows = countries;
        return res.json(responseObj);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-countries/:id
   * get country details in DB
   */
  getAdminCountryDetails(req, res) {
    loggingHelper.log('info', 'getAdminCountryDetails start');

    this.db.Country.findOne({
      where: { id: req.params.id },
      include: ['States', 'SupportedLanguages']
    })
      .then(country => ResponseHelper.responseSuccess(res, country))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /countries/states
   * get all countries in DB
   */
  getCountryState(req, res) {
    loggingHelper.log('info', 'getCountryState start');
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseMissingParam(res);
    } else {
      // res.json(config.countryStates[countryCode.toUpperCase()]);

      this.db.State.findAll({
        include: [{
          model: this.db.Country,
          as: 'Country',
          where: { code: countryCode.toUpperCase() }
        }]
      })
        .then(states => res.json(states));
    }
  }

  /** GET /countries/shipping-fee
   * get config for shipping fee
   */
  getShippingFee(req, res) {
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseMissingParam(res);
    } else {
      this.db.Country.findOne({
        where: {
          code: countryCode.toUpperCase()
        }
      })
        .then(country => {
          // let shippingFee = config.shippingFee[countryCode.toUpperCase()];
          let shippingFee = {};
          if(country) {
            shippingFee = {
              minAmount: country.shippingMinAmount,
              fee: country.shippingFee,
              trialFee: country.shippingTrialFee
            };
          }
          res.json(shippingFee || {});
        });
    }
  }

  /** POST /countries
   * create new country
   */
  postCountries(req, res) {
    loggingHelper.log('info', 'postCountries start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.Country.build(params).validate()
      .then(() => this.db.Country.findOne({ where: { code: params.code } }))
      .then(country => {
        if(country) {
          throw new Error('code_existed');
        } else {
          return this.db.Country.findOne({ where: { name: params.name } });
        }
      })
      .then(result => {
        if(!result) {
          params.States.forEach(state => {
            if(!state.isDefault || state.isDefault === '') {
              state.isDefault = false;
            }
          });
          return this.db.sequelize.transaction(t => this.db.Country.create(params, {
            include: ['States', 'SupportedLanguages'],
            returning: true,
            transaction: t
          })
            .then(country => this.db.DirectoryCountry.update({ CountryId: country.id }, { where: { code: country.code }, transaction: t }))
          );
        } else {
          throw new Error('name_existed');
        }
      })
      .then(admin => res.json(admin))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.message === 'code_existed') {
          return ResponseHelper.responseError(res, 'code is existed');
        } else if(error.message === 'name_existed') {
          return ResponseHelper.responseError(res, 'name is existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
  /**
   * POST /countries/change-status
   * Allow admin change status of multiple countries
   * @param {*} req 
   * @param {*} res 
   */
  postUpdateCountriesStatus(req, res) {
    loggingHelper.log('info', `postUpdateCountriesStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if(!params.countriesIds || !params.isActive) {
      return ResponseHelper.responseMissingParam(res, 'Params');
    }
    params.isActive = (params.isActive === 'true' || params.isActive === '1' || params.isActive === 1);
    params.countriesIds = Array.isArray(params.countriesIds) ? params.countriesIds : [params.countriesIds];

    this.db.Country.update({ isActive: params.isActive }, { where: { id: { $in: params.countriesIds } } })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }
  /** PUT /countries/:id
   * update country
   */
  updateCountry(req, res) {
    loggingHelper.log('info', 'updateCountry start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // init transaction
    let updateStates = [];
    let createStates = [];
    let updateSupportedLanguage = [];
    let createSupportedLanguage = [];
    params.States = params.States ? params.States : [];
    params.SupportedLanguages = params.SupportedLanguages ? params.SupportedLanguages : [];

    this.db.sequelize.transaction(t => {
      params.States.forEach(state => {
        if(!state.isDefault || state.isDefault === '') {
          state.isDefault = false;
        }
        if(state.id) {
          updateStates.push(state);
        } else {
          state.CountryId = req.params.id;
          createStates.push(state);
        }
      });
      params.SupportedLanguages.forEach(supportedLanguage => {
        if(supportedLanguage.id) {
          updateSupportedLanguage.push(supportedLanguage);
        } else {
          supportedLanguage.CountryId = req.params.id;
          createSupportedLanguage.push(supportedLanguage);
        }
      });
      return SequelizeHelper.bulkUpdate(updateStates, this.db, 'State', { transaction: t })
        .then(() => this.db.State.bulkCreate(createStates, { returning: true, transaction: t }))
        .then(() => SequelizeHelper.bulkUpdate(updateSupportedLanguage, this.db, 'SupportedLanguage', { transaction: t }))
        .then(() => this.db.SupportedLanguage.bulkCreate(createSupportedLanguage, { returning: true, transaction: t }))
        .then(() => this.db.Country.update(params, { where: { id: req.params.id }, transaction: t }));
    })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'code or name is existed');
        } else {
          ResponseHelper.responseInternalServerError(res, error);
        }
      });

    // this.db.Country.findOne({where: {
    //   id: req.params.id
    // }, raw: true})
    //   .then(country => {
    //     if(country) {
    //       // merge current country data
    //       params = Object.assign(country, params);
    //       return this.db.Country.build(params).validate();
    //     } else {
    //       throw new Error('not_found');
    //     }
    //   })
    //   .then(() => this.db.Country.update(params, {where: {
    //     id: req.params.id
    //   }}))
    //   .then(() => this.db.Country.findById(req.params.id))
    //   .then(country => res.json(country))
    //   .catch(error => {
    //     if(error.name === 'SequelizeValidationError') {
    //       return ResponseHelper.responseErrorParam(res, error);
    //     } else if(error.name === 'SequelizeUniqueConstraintError') {
    //       return ResponseHelper.responseError(res, 'code or name is existed');
    //     } else if(error.message === 'not_found') {
    //       return ResponseHelper.responseNotFoundError(res, 'Admin');
    //     } else {
    //       return ResponseHelper.responseInternalServerError(res, error);
    //     }
    //   });
  }

  /** GET /admin-states
   * get all states in DB
   */
  getAdminStates(req, res) {
    loggingHelper.log('info', 'getAdminStates start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {};
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.State);

    this.db.State.findAndCountAll(options)
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /admin-states
   * create new state
   */
  postStates(req, res) {
    loggingHelper.log('info', 'postStates start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.State.build(params).validate()
      .then(() => this.db.State.findOne({ where: { name: params.name } }))
      .then(result => {
        if(!result) {
          return this.db.State.update({ isDefault: false }, { where: { CountryId: params.CountryId } })
            .then(() => this.db.State.create(params, { returning: true }));
        } else {
          throw new Error('name_existed');
        }
      })
      .then(state => res.json(state))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.message === 'name_existed') {
          return ResponseHelper.responseError(res, 'name is existed');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** PUT /admin-states/:id
   * update state
   */
  updateState(req, res) {
    loggingHelper.log('info', 'updateState start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    this.db.State.findOne({
      where: {
        id: req.params.id
      }, raw: true
    })
      .then(state => {
        if(state) {
          // merge current state data
          params = Object.assign(state, params);
          return this.db.State.build(params).validate();
        } else {
          throw new Error('not_found');
        }
      })
      .then(() => {
        if(params.isDefault) {
          return this.db.State.update({ isDefault: false }, { where: { CountryId: params.CountryId } });
        } else {
          return;
        }
      })
      .then(() =>
        this.db.State.update(params, {
          where: {
            id: req.params.id
          }
        })
      )
      .then(() => this.db.State.findById(req.params.id))
      .then(result => res.json(result))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'code or name is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** DELETE /admin-states/:id
   * delete state
   */
  deleteState(req, res) {
    loggingHelper.log('info', 'deleteStates start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.State.destroy({ where: { id } })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'can_not_delete') {
          return ResponseHelper.responseError(res, 'can not delete.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** DELETE /admin-supported-languages/:id
   * delete supported language
   */
  deleteSupportedLanguage(req, res) {
    loggingHelper.log('info', 'deleteSupportedLanguage start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.SupportedLanguage.destroy({ where: { id } })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'can_not_delete') {
          return ResponseHelper.responseError(res, 'can not delete.');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** GET /countries/subscriber
   * get all countries has subscriber
   */
  getSubscriberCountries(req, res) {
    loggingHelper.log('info', 'getSubscriberCountries start');
    this.db.Country.findAll({
      where: {
        isActive: true
      },
      include: [
        {
          model: this.db.PlanCountry
        }
      ]
    })
      .then(countries => {
        countries = SequelizeHelper.convertSeque2Json(countries);
        let result = [];
        countries.forEach(country => {
          if(country.PlanCountries[0]) {
            Reflect.deleteProperty(country, 'PlanCountries');
            result.push(country);
          }
        });
        res.json(result);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
}

export default function(...args) {
  return new CountryApi(...args);
}
