import Promise from 'bluebird';
import CryptoUtil from 'crypto-js';
import multipart from 'connect-multiparty';
import _ from 'underscore';
import jwt from 'jsonwebtoken';
import config from '../config';

import JwtMiddleware from '../middleware/jwt-middleware';
import ResponseHelper from '../helpers/response-helper';
import RequestHelper from '../helpers/request-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import SubscriptionHelper from '../helpers/subscription-helper';
import SendEmailQueueService from '../services/send-email.queue.service';
import Validator from '../helpers/validator';
import { stripeHelper } from '../helpers/stripe-helper';
import { loggingHelper } from '../helpers/logging-helper';

let multipartMiddleware = multipart();

class UserApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    // admin APIs
    this.api.post('/admins/users/change-status', JwtMiddleware.verifyAdminToken, this.postChangeUserStatus.bind(this));

    // guest API
    this.api.post('/users/deliveries', this.addGuestDeliveryAddress.bind(this));
    this.api.put('/users/deliveries/:deliveryId', this.updateGuestDeliveryAddress.bind(this));
    this.api.delete('/users/deliveries/:deliveryId', this.deleteGuestDeliveryAddress.bind(this));
    this.api.post('/users/cards', this.addSGuestStripeCard.bind(this));
    this.api.put('/users/cards/:cardId', this.updateGuestStripeCard.bind(this));
    this.api.delete('/users/cards/:cardId', this.deleteGuestStripeCard.bind(this));

    // logged in user API
    this.api.get('/users', JwtMiddleware.verifyUserToken, this.getUsers.bind(this));
    this.api.get('/users/check-subscriber', JwtMiddleware.verifyUserToken, this.checkSubscriber.bind(this));
    this.api.get('/users/:id', JwtMiddleware.verifyUserToken, this.getUserDetail.bind(this));
    this.api.post('/users/:id', JwtMiddleware.verifyUserToken, multipartMiddleware, this.updateUser.bind(this));
    this.api.post('/users/:userId/carts', JwtMiddleware.verifyUserToken, this.syncCartDetail.bind(this));
    this.api.get('/users/:userId/carts', JwtMiddleware.verifyUserToken, this.getCartDetails.bind(this));
    this.api.post('/users/:userId/deliveries', JwtMiddleware.verifyUserToken, this.addDeliveryAddress.bind(this));
    this.api.post('/users/:userId/deliveries/:deliveryId', JwtMiddleware.verifyUserToken, this.updateDeliveryAddress.bind(this));    
    this.api.delete('/users/:userId/deliveries/:deliveryId', JwtMiddleware.verifyUserToken, this.deleteDeliveryAddress.bind(this));
    this.api.get('/users/:userId/deliveries', JwtMiddleware.verifyUserToken, this.getDeliveryAddress.bind(this));
    this.api.get('/users/:userId/cards', JwtMiddleware.verifyUserToken, this.getCards.bind(this));
    this.api.get('/users/:userId/ba-cards', JwtMiddleware.verifyUserToken, this.getCardsForBa.bind(this));
    this.api.get('/admin/users/:userId/cards', JwtMiddleware.verifyAdminToken, this.getUserCardsForAdmin.bind(this));
    this.api.get('/admin/users/:id', JwtMiddleware.verifyAdminToken, this.getUserDetailsForAdmin.bind(this));
    this.api.post('/users/:userId/cards', JwtMiddleware.verifyUserToken, this.addStripeCard.bind(this));
    this.api.put('/users/:userId/cards/:cardId', JwtMiddleware.verifyUserToken, this.updateStripeCard.bind(this));
    this.api.post('/users/:userId/cards/set-default', JwtMiddleware.verifyUserToken, this.setCardDefault.bind(this));
    this.api.delete('/users/:userId/cards/:cardId', JwtMiddleware.verifyUserToken, this.deleteCard.bind(this));
    this.api.get('/users/:userId/subscriptions', JwtMiddleware.verifyUserToken, this.getSubscription.bind(this));
    this.api.get('/users/:userId/subscriptions/:subsId', JwtMiddleware.verifyUserToken, this.getSubscriptionDetails.bind(this));
    this.api.put('/users/:userId/subscriptions/:subsId', JwtMiddleware.verifyUserToken, this.updateSubscriptionDetails.bind(this));
  }

  /**
   * POST /admins/users/change-status
   * Allow admin update status for multiple user
   * @param {*} req 
   * @param {*} res 
   */
  postChangeUserStatus(req, res) {
    loggingHelper.log('info', `postChangeUserStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if(!params.userIds || !params.isActive) {
      return ResponseHelper.responseMissingParam(res, 'Params');
    }

    params.isActive = (params.isActive === 'true' || params.isActive === '1' || params.isActive === 1);
    params.userIds = Array.isArray(params.userIds) ? params.userIds : [params.userIds];

    this.db.User.update({isActive: params.isActive}, {where: {id: {$in: params.userIds}}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users
   * get all users in DB
   */
  getUsers(req, res) {
    loggingHelper.log('info', 'getUsers start');
    this.db.User.findAll()
      .then(users => {
        res.json(users);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /users/:id
   * get User details
   */
  getUserDetail(req, res) {
    loggingHelper.log('info', 'getUserDetail start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.User.findOne({ where: {
      id: req.params.id
    }, include: ['Country']
    })
      .then(user => {
        res.json(user);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin/users/:id
   * get User details for admin
   */
  getUserDetailsForAdmin(req, res) {
    loggingHelper.log('info', 'getUserDetailsForAdmin start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);

    this.db.User.findOne({
      where: {
        id: req.params.id
      },
      include: [
        'Country', 'deliveryAddresses',
        {
          model: this.db.Card,
          as: 'cards',
          include: ['Country']
        }
      ]
    })
    .then(user => this.db.Subscription.findAll({
        where: {
          status: {$notLike: 'Canceled'},
          userId: user.id
        }
      })
      .then(subs => {
        user = SequelizeHelper.convertSeque2Json(user);
        user.subscriptions = subs;
        return user;
      }))
    .then(user => {
      res.json(user);
    })
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /user/:id
   * update user info
   */
  updateUser(req, res) {
    loggingHelper.log('info', 'updateUser start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let options = {};
    let hasChangePassword = false;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let data = {};

    if((params.password && !params.oldPassword) || (params.oldPassword && !params.password)) {
      return ResponseHelper.responseMissingParam(res);
    }

    if(params.password) {
      params.password = CryptoUtil.MD5(params.password).toString();
      params.oldPassword = CryptoUtil.MD5(params.oldPassword).toString();
      options.where = {password: params.oldPassword, id: req.params.id};
      hasChangePassword = true;
    }

    // validate date
    if(!Validator.valiadateDate(params.birthday)) {
      return ResponseHelper.responseError(res, 'Birthday is incorrect!');
    }

    params.birthday = params.birthday === '' ? null : params.birthday;
    if(params.birthday) {
      params.birthday = new Date(params.birthday);
    }

    this.db.User.findOne(options)
      .then(user => {
        console.log(`User ==== ${JSON.stringify(user)}`);
        loggingHelper.log('info', `User ==== ${JSON.stringify(user)}`);
        if(!user) {
          throw new Error('old_password_invalid');
        } else {
          return this.db.User.update(params, {
            where: {
              id: req.params.id,
            }
          });
        }
      })
      .then(() => this.db.User.findById(req.params.id))
      .then(user => {
        data.user = user;
        if(hasChangePassword) {
          return sendEmailQueueService.addTask({method: 'sendPasswordUpdated', data: { email: user.email, firstName: user.firstName }, langCode: req.country.defaultLang.toLowerCase()});
        }
      })
      .then(() => {
        res.json(data.user);
      })
      .catch(error => {
        if(error.message === 'old_password_invalid') {
          return ResponseHelper.responseError(res, 'Current password is incorrect');
        } else {
          return ResponseHelper.responseError(res, error);
        }
      });
  }

  /** POST /users/:userId/carts
   * Sync local carts with server
   */
  syncCartDetail(req, res) {
    loggingHelper.log('info', 'syncCartDetail start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let countryId = req.country.id;

    console.log(`params == ${params}`);
    loggingHelper.log('info', `params == ${params}`);

    if(!params || !Array.isArray(params.carts)) {
      ResponseHelper.responseErrorParam(res);
      return;
    }

    // get cart ID
    this.db.Cart.findOne({where: {userId: req.params.userId}})
      .then(cart => { 
        // build cart detail
        params.carts.forEach(param => {
          param.CartId = cart.id;
          param.CountryId = countryId;
        });
        return this.db.CartDetail.destroy({where: {
          cartId: cart.id,
          CountryId: countryId
        }});
      }) // remove all card detail
      .then(() => this.db.CartDetail.bulkCreate(params.carts))
      .then(() => res.json({ok: true}))
      .catch(error => ResponseHelper.responseError(res, `${error.message || error}`));
    // update cart detail
  }

  /**
   * GET /users/:userId/carts
   * get all item in cart of user by country
   * @param {*} req 
   * @param {*} res 
   */
  getCartDetails(req, res) {
    loggingHelper.log('info', 'getCartDetails start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let countryId = req.country.id;
    this.db.Cart.findOne({where: {userId: req.params.userId}})
      .then(cart => this.db.CartDetail.findAll({
        where: {CartId: cart.id},
        include: [{
          model: this.db.ProductCountry,
          include: [
            'Country',
            {
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
        }, {
          model: this.db.PlanCountry,
          include: [
            'Country',
            {
              model: this.db.Plan,
              include: ['planImages', 'planTranslate', 'PlanType']
            }]
        }]
      }))
      .then(cartDetails => {
        cartDetails = SequelizeHelper.convertSeque2Json(cartDetails);
        cartDetails = _.filter(cartDetails, (cart => cart.CountryId === countryId));
        let tmp = [];
        cartDetails.forEach(cartDetail => {
          if(cartDetail.ProductCountry) {
            let product = {};
            product.id = cartDetail.ProductCountry.Product.id;
            product.sku = cartDetail.ProductCountry.Product.sku;
            product.rating = cartDetail.ProductCountry.Product.rating;
            product.productTranslate = cartDetail.ProductCountry.Product.productTranslate;
            product.productImageURL = _.findWhere(cartDetail.ProductCountry.Product.productImages, {isDefault: true}).url;
            Reflect.deleteProperty(cartDetail.ProductCountry, 'Product');
            product.productCountry = cartDetail.ProductCountry;
            Reflect.deleteProperty(cartDetail, 'ProductCountry');
            cartDetail.product = product;
          }
          if(cartDetail.PlanCountry) {
            let plan = {};
            plan.id = cartDetail.PlanCountry.Plan.id;
            plan.sku = cartDetail.PlanCountry.Plan.sku;
            plan.rating = cartDetail.PlanCountry.Plan.rating;
            plan.planTranslate = cartDetail.PlanCountry.Plan.planTranslate;
            plan.PlanType = cartDetail.PlanCountry.Plan.PlanType;
            plan.planImageURL = _.findWhere(cartDetail.PlanCountry.Plan.planImages, {isDefault: true}).url;
            Reflect.deleteProperty(cartDetail.PlanCountry, 'Plan');
            plan.planCountry = cartDetail.PlanCountry;
            Reflect.deleteProperty(cartDetail, 'PlanCountry');
            cartDetail.plan = plan;
          }
          tmp.push(cartDetail);
        });
        return tmp;
      })
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** POST /users/:userId/deliveries
   * Add delivery detail
   */
  addDeliveryAddress(req, res) {
    loggingHelper.log('info', 'addDeliveryAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    console.log(`addDeliveryAddress ==== ${req.method}`);
    loggingHelper.log('info', `addDeliveryAddress ==== ${req.method}`);
    let deliveryAddressses = req.body;
    let defaultBilling;
    let defaultShipping;

    deliveryAddressses = Array.isArray(deliveryAddressses) ? deliveryAddressses : [deliveryAddressses];
    
    this.db.sequelize.transaction(t => Promise.mapSeries(deliveryAddressses, deliveryAddress => {
      deliveryAddress.UserId = req.params.userId;
      deliveryAddress.CountryId = req.country.id;
      return this.db.DeliveryAddress.create(deliveryAddress, {returning: true, transaction: t});
    })
      .then(addresses => {
        deliveryAddressses.forEach((address, idx) => {
          defaultBilling = (address.defaultBilling) ? addresses[idx].id : defaultBilling;
          defaultShipping = (address.defaultShipping) ? addresses[idx].id : defaultShipping;
        });
        deliveryAddressses = addresses;
        let options = {};
        if(defaultBilling) {
          options.defaultBilling = defaultBilling;
        }
        if(defaultShipping) {
          options.defaultShipping = defaultShipping;
        }
        return this.db.User.update(options, {where: {id: req.params.userId}, transaction: t});
      }))
    .then(() => res.json({
      ok: true,
      data: deliveryAddressses
    }))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /users/deliveries
   * Add delivery address for guest
   * @param {*} req 
   * @param {*} res 
   */
  addGuestDeliveryAddress(req, res) {
    loggingHelper.log('info', 'addGuestDeliveryAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let deliveryAddressses = req.body;

    deliveryAddressses = Array.isArray(deliveryAddressses) ? deliveryAddressses : [deliveryAddressses];
    Promise.mapSeries(deliveryAddressses, deliveryAddress => {
      deliveryAddress.CountryId = req.country.id;
      return this.db.DeliveryAddress.create(deliveryAddress, {returning: true});
    })
    .then(addresses => res.json({
      ok: true,
      data: addresses
    }))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /users/deliveries/:deliveryId
   * delete delivery address of guest
   * @param {*} req 
   * @param {*} res 
   */
  deleteGuestDeliveryAddress(req, res) {
    loggingHelper.log('info', 'deleteGuestDeliveryAddress start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.DeliveryAddress.update({isRemoved: true}, {where: {
      id: req.params.deliveryId,
      UserId: null
    }})
      .then(() => res.json({ok: true}))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/deliveries
   * Get delivery detail
   */
  getDeliveryAddress(req, res) {
    loggingHelper.log('info', 'getDeliveryAddress start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.DeliveryAddress.findAll({where: {
      UserId: req.params.userId,
      isRemoved: false
    }})
    .then(deliveryAddresses => res.json(deliveryAddresses))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  checkSubscriber(req, res) {
    loggingHelper.log('info', 'checkSubscriber start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    if(!params.email) {
      return ResponseHelper.responseMissingParam(res, 'missing email');
    }
    // get user by email
    this.db.User.findOne({where: {email: params.email}})
      .then(user => {
        if(!user) {
          throw new Error('not_existed');
        } else {
          // find subscriber 
          return this.db.Subscription.findAll({where: { UserId: user.id }});
        }
      })
      .then(subscriptions => res.json({ok: true, subscriptions}))
      .catch(error => {
        if(error.message === 'not_existed') {
          res.json({ok: true});
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** GET /users/:userId/deliveries
   * Get delivery detail
   */
  getSubscription(req, res) {
    loggingHelper.log('info', 'getSubscription start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {
      UserId: req.params.userId
    }, include: ['User', 'Card',
      { 
        model: this.db.PlanCountry,
        include: ['Country', {
          model: this.db.Plan,
          include: ['planImages', 'planTranslate', 'PlanType']
        }, {
          model: this.db.PlanDetail,
          as: 'planDetails',
          include: [{
            model: this.db.ProductCountry,
            include: [{
              model: this.db.Product,
              include: ['productImages', 'productTranslate']
            }]
          }]
        }]
      },
      {
        model: this.db.CustomPlan,
        include: [
          'Country',
          'PlanType',
          {
            model: this.db.CustomPlanDetail,
            as: 'customPlanDetail',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }
        ]
      }
    ]};
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    // order by created at
    params.orderBy = JSON.stringify(['createdAt_DESC']);

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Subscription);

    this.db.Subscription.findAll(options)
      .then(subscriptions => {
        // build next delivery items
        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
        subscriptions.forEach(subscription => {
          let deliveryDetails = SubscriptionHelper.buildDeliveryProducts(subscription);
          let nextDeliveryProducts = _.findWhere(deliveryDetails, {time: `${subscription.currentDeliverNumber + 1}`});
          subscription.nextDeliveryProducts = nextDeliveryProducts ? nextDeliveryProducts.productNames : null;
        });
        return subscriptions;
      })
      .then(subscriptions => res.json(subscriptions))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/deliveries
   * Get delivery detail
   */
  getSubscriptionDetails(req, res) {
    loggingHelper.log('info', 'getSubscriptionDetails start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let countryCode = req.headers['country-code'];
    let subscription;
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }
    this.db.Subscription.findOne({where: {
      id: req.params.subsId
    }, include: ['User', 'DeliveryAddress', 'BillingAddress', 'Card', 
      {
        model: this.db.PlanOption,
        as: 'PlanOption',
        include: ['planOptionTranslate']
      },
      {
        model: this.db.PlanCountry,
        include: ['Country', 
          {
            model: this.db.PlanOption,
            as: 'planOptions',
            include: ['planOptionTranslate']
          },
          {
            model: this.db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          },
          {
            model: this.db.Plan,
            include: [
              'planImages', 'planTranslate', 
              {
                model: this.db.PlanType,
                as: 'PlanType',
                include: ['planTypeTranslate']
              },
              'PlanGroup',
              {
                model: this.db.PlanCountry,
                as: 'planCountry',
                include: ['Country']
              }
            ]
          }
        ]
      },
      {
        model: this.db.CustomPlan,
        include: [
          'Country',
          {
            model: this.db.PlanType,
            as: 'PlanType',
            include: ['planTypeTranslate']
          },
          {
            model: this.db.CustomPlanDetail,
            as: 'customPlanDetail',
            include: [{
              model: this.db.ProductCountry,
              include: [{
                model: this.db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }
        ]
      }
    ]})
    .then(result => {
      subscription = SequelizeHelper.convertSeque2Json(result);
      return this.db.Order.findAll({
        where: {$or: [
          {subscriptionIds: {$like: `%${result.id}%`}},
          {'$Receipt.SubscriptionId$': result.id}
        ]},
        include: ['Receipt', 'Country']
      });
    })
    .then(result => {
      subscription.orders = result;
      let deliveryDetails = SubscriptionHelper.buildDeliveryProducts(subscription);
      let nextDeliveryProducts = _.findWhere(deliveryDetails, {time: `${subscription.currentDeliverNumber + 1}`});
      subscription.nextDeliveryProducts = nextDeliveryProducts ? nextDeliveryProducts.productNames : null;
      return res.json(subscription);
    })
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** POST /users/:userId/deliveries/:deliveryId
   * Update delivery detail
   */
  updateDeliveryAddress(req, res) {
    loggingHelper.log('info', 'updateDeliveryAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.sequelize.transaction(t => this.db.DeliveryAddress.update(params, {
      where: {
        id: req.params.deliveryId,
        UserId: req.params.userId
      },
      returning: true,
      transaction: t
    })
      .then(() => {
        let options = {};
        if(params.defaultBilling) {
          options.defaultBilling = req.params.deliveryId;
        }
        if(params.defaultShipping) {
          options.defaultShipping = req.params.deliveryId;
        }
        return this.db.User.update(options, {where: {id: req.params.userId}, transaction: t});
      }))
      .then(() => this.db.DeliveryAddress.findOne({where: {id: req.params.deliveryId}}))
      .then(result => res.json({ok: true, data: SequelizeHelper.convertSeque2Json(result)}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** DELETE /users/:userId/deliveries/:deliveryId
   * Update delivery detail
   */
  deleteDeliveryAddress(req, res) {
    loggingHelper.log('info', 'deleteDeliveryAddress start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let user;
    this.db.sequelize.transaction(t => this.db.Subscription.count({where: {
      CardId: req.params.cardId,
      status: 'Processing'
    }})
    .then(count => {
      if(count > 0) {
        throw new Error('can_remove');
      } else {
        return this.db.DeliveryAddress.update({isRemoved: true}, {where: {
          id: req.params.deliveryId,
          UserId: req.decoded.id
        }, transaction: t});
      }
    })
    .then(() => this.db.User.findOne({where: {id: req.decoded.id}}))
    .then(_user => {
      user = _user;
      if(user.defaultShipping === req.params.deliveryId || user.defaultBilling === req.params.deliveryId) {
        return this.db.DeliveryAddress.findOne({where: {UserId: user.id, isRemoved: false}});
      }
    })
    .then(address => {
      if((user.defaultShipping === req.params.deliveryId || user.defaultBilling === req.params.deliveryId) && address) {
        let options = {};
        if(user.defaultShipping === req.params.deliveryId) {
          options.where = {defaultShipping: address.id};
        }
        if(user.defaultBilling === req.params.deliveryId) {
          options.where = options.where ? Object.assign(options.where, {defaultBilling: address.id}) : {defaultBilling: address.id};
        }
        return this.db.User.update(options, {where: {id: user.id}});
      } else if((user.defaultShipping === req.params.deliveryId || user.defaultBilling === req.params.deliveryId) && !address) {
        return this.db.User.update({defaultBilling: null, defaultShipping: null}, {where: {id: user.id}});
      }
    })
    )
    .then(() => res.json({ok: true}))
    .catch(error => {
      if(error.message === 'can_remove') {
        return ResponseHelper.responseError(res, 'This delivery address are using for a subscription, So you can not remove it.');
      } else {
        return ResponseHelper.responseError(res, error);
      }
    });
  }

  /** GET /users/:userId/cards
   * Get card list
   */
  getCards(req, res) {
    loggingHelper.log('info', 'getCards start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    let options = {where: {
      UserId: req.params.userId,
      isRemoved: false,
      // type: 'website'
    }};
    let params = req.query;

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Card);

    this.db.Card.findAll(options)
    .then(cards => res.json(cards))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/ba-cards
   * Get card list
   */
  getCardsForBa(req, res) {
    loggingHelper.log('info', 'getCards start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);

    let options = {where: {
      UserId: req.params.userId,
      isRemoved: false,
      type: 'baWeb'
    }};
    let params = req.params;

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.Card);

    this.db.Card.findAll(options)
    .then(cards => res.json(cards))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** GET /users/:userId/cards
   * Get card list
   */
  getUserCardsForAdmin(req, res) {
    loggingHelper.log('info', 'getUserCardsForAdmin start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.Card.findAll({where: {
      UserId: req.params.userId,
      isRemoved: false,
      type: 'website'
    }})
    .then(cards => res.json(cards))
    .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** POST /users/:userId/cards
   * Add stripe cart
   */
  addStripeCard(req, res) {
    loggingHelper.log('info', 'addStripeCard start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let user = req.decoded;
    let cardInfo;
    let card;
    let stripeCustomer;
    let isDefault = params.isDefault || false;
    let token = req.headers['x-access-token'];
    let type = 'website';

    if(!params.stripeToken || !params.CountryId) {
      return ResponseHelper.responseMissingParam(res);
    }
    if(!token) {
      return ResponseHelper.responseError(res, 'missing access token');
    }

    jwt.verify(token, config.accessToken.secret, function(err, decoded) {
      if(err) {
        return ResponseHelper.responseError(res, 'token expired');
      } else {
        if(decoded.isSeller) {
          type = 'baWeb';
        }
      }
    });

    this.db.sequelize.transaction(t => stripeHelper.createCustomer(user.email, params.stripeToken, req.country.code, type)
      .then(_stripeCustomer => {
        stripeCustomer = _stripeCustomer;
        console.log(`_stripeCustomer ==== ${JSON.stringify(_stripeCustomer)}`);
        loggingHelper.log('info', `_stripeCustomer ==== ${JSON.stringify(_stripeCustomer)}`);
        if(stripeCustomer.sources.data.length === 0) {
          throw new Error('stripe_token_invalid');
        }
        return this.db.Card.findOne({where: {
          UserId: user.id,
          cardNumber: stripeCustomer.sources.data[0].last4,
          branchName: stripeCustomer.sources.data[0].brand,
          expiredYear: stripeCustomer.sources.data[0].exp_year,
          expiredMonth: stripeCustomer.sources.data[0].exp_month,
          type,
          isRemoved: false,
          CountryId: params.CountryId
        }});
      })
      .then(result => {
        if(!result) {
          // store card infor into database
          cardInfo = {
            isDefault,
            UserId: user.id,
            customerId: stripeCustomer.id,
            cardNumber: stripeCustomer.sources.data[0].last4,
            branchName: stripeCustomer.sources.data[0].brand,
            cardName: stripeCustomer.sources.data[0].name,
            type,
            expiredYear: stripeCustomer.sources.data[0].exp_year,
            expiredMonth: stripeCustomer.sources.data[0].exp_month,
            CountryId: params.CountryId
          };
          return this.db.Card.create(cardInfo, {returning: true, transaction: t});
        } else {
          return result;
        }
      })
      .then(_card => {
        card = _card;
        // update isDefault field
        if(isDefault) {
          return this.db.Card.update({isDefault: false}, {where: {
            UserId: user.id,
            id: {$ne: card.id},
            CountryId: params.CountryId
          }, transaction: t});
        }
      })
      .then(() => res.json(card)))
      .catch(error => {
        if(error.message === 'stripe_token_invalid') {
          return ResponseHelper.responseErrorParam(res, 'stripe token invalid.');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /users/:userId/cards/:cardId
   * update card token
   * @param {*} req 
   * @param {*} res 
   */
  updateStripeCard(req, res) {
    loggingHelper.log('info', 'updateStripeCard start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let user = req.decoded;
    let cardInfo;
    let isDefault = params.isDefault || false;
    let token = req.headers['x-access-token'];
    let type = 'website';

    if(!params.stripeToken) {
      return ResponseHelper.responseMissingParam(res);
    }
    if(!token) {
      return ResponseHelper.responseError(res, 'missing access token');
    }

    jwt.verify(token, config.accessToken.secret, function(err, decoded) {
      if(err) {
        return ResponseHelper.responseError(res, 'token expired');
      } else {
        if(decoded.isSeller) {
          type = 'baWeb';
        }
      }
    });

    this.db.sequelize.transaction(t => stripeHelper.createCustomer(user.email, params.stripeToken, req.country.code, type)
      .then(stripeCustomer => {
        if(stripeCustomer.sources.data.length === 0) {
          throw new Error('stripe_token_invalid');
        }
        // store card infor into database
        cardInfo = {
          UserId: req.params.userId,
          customerId: stripeCustomer.id,
          cardNumber: stripeCustomer.sources.data[0].last4,
          branchName: stripeCustomer.sources.data[0].brand,
          cardName: stripeCustomer.sources.data[0].name,
          expiredYear: stripeCustomer.sources.data[0].exp_year,
          expiredMonth: stripeCustomer.sources.data[0].exp_month,
          isDefault
        };
        return this.db.Card.update(cardInfo, {where: {UserId: user.id, id: req.params.cardId}, transaction: t});
      })
      .then(() => {
        if(isDefault) {
          return this.db.Card.update({isDefault: false}, {where: {
            UserId: user.id,
            id: {$ne: req.params.cardId}
          }, transaction: t});
        }
      })
      .then(() => res.json({ok: true})))
      .catch(error => {
        if(error.message === 'stripe_token_invalid') {
          return ResponseHelper.responseErrorParam(res, 'stripe token invalid.');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /users/:userId/cards/set-default
   * set card default for user
   * @param {*} req 
   * @param {*} res 
   */
  setCardDefault(req, res) {
    loggingHelper.log('info', 'setCardDefault start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    if(!params.cardId && !params.CountryId) {
      return ResponseHelper.responseMissingParam(res, 'missing cardId');
    }
    this.db.sequelize.transaction(t => this.db.Card.update({isDefault: false}, {where: {
      id: {$not: params.cardId},
      UserId: req.decoded.id,
      CountryId: params.CountryId
    }, transaction: t})
      .then(() => this.db.Card.update({isDefault: true}, {where: {
        UserId: req.params.userId,
        id: params.cardId
      }, transaction: t})))
      .then(() => res.json({ok: true}))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /users/cards
   * Add casd for guest checkout
   * @param {*} req 
   * @param {*} res 
   */
  addSGuestStripeCard(req, res) {
    loggingHelper.log('info', 'addSGuestStripeCard start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let cardInfo;
    let stripeCustomer;
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }
    if(!params.stripeToken || !params.type) {
      return ResponseHelper.responseMissingParam(res);
    }
    this.db.sequelize.transaction(t => stripeHelper.createCustomer(null, params.stripeToken, req.country.code, params.type)
      .then(_stripeCustomer => {
        stripeCustomer = _stripeCustomer;
        if(stripeCustomer.sources.data.length === 0) {
          throw new Error('stripe_token_invalid');
        }
        return this.db.Card.findOne({where: {
          'cardNumber': stripeCustomer.sources.data[0].last4,
          'branchName': stripeCustomer.sources.data[0].brand,
          'expiredYear': stripeCustomer.sources.data[0].exp_year,
          'expiredMonth': stripeCustomer.sources.data[0].exp_month,
          'type': params.type,
          '$User.email$': params.email,
          'CountryId': req.country.id
        }, include: ['User']});
      })
      .then(card => {
        if(!card) {
          // store card infor into database
          cardInfo = {
            customerId: stripeCustomer.id,
            cardNumber: stripeCustomer.sources.data[0].last4,
            branchName: stripeCustomer.sources.data[0].brand,
            cardName: stripeCustomer.sources.data[0].name,
            type: params.type,
            expiredYear: stripeCustomer.sources.data[0].exp_year,
            expiredMonth: stripeCustomer.sources.data[0].exp_month,
            CountryId: req.country.id
          };
          return this.db.Card.create(cardInfo, {returning: true, transaction: t});
        } else {
          return card;
        }
      })
      .then(card => res.json(card)))
      .catch(error => {
        if(error.message === 'stripe_token_invalid') {
          return ResponseHelper.responseErrorParam(res, 'stripe token invalid.');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** POST /users/deliveries/:deliveryId
   * Update delivery detail
   */
  updateGuestDeliveryAddress(req, res) {
    loggingHelper.log('info', 'updateGuestDeliveryAddress start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    this.db.sequelize.transaction(t => this.db.DeliveryAddress.update(params, {
      where: {
        id: req.params.deliveryId,
        UserId: null
      },
      returning: true,
      transaction: t
    }))
      .then(() => res.json({ok: true}))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /users/:userId/cards/:cardId
   * update card token
   * @param {*} req 
   * @param {*} res 
   */
  updateGuestStripeCard(req, res) {
    loggingHelper.log('info', 'updateGuestStripeCard start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let cardInfo;
    let isDefault = params.isDefault || false;

    if(!params.stripeToken || !params.email || !params.type) {
      return ResponseHelper.responseMissingParam(res);
    }
    this.db.sequelize.transaction(t => stripeHelper.createCustomer(params.email, params.stripeToken, req.country.code, params.type)
      .then(stripeCustomer => {
        if(stripeCustomer.sources.data.length === 0) {
          throw new Error('stripe_token_invalid');
        }
        // store card infor into database
        cardInfo = {
          id: req.params.cardId,
          UserId: req.params.userId,
          customerId: stripeCustomer.id,
          cardNumber: stripeCustomer.sources.data[0].last4,
          branchName: stripeCustomer.sources.data[0].brand,
          cardName: stripeCustomer.sources.data[0].name,
          expiredYear: stripeCustomer.sources.data[0].exp_year,
          expiredMonth: stripeCustomer.sources.data[0].exp_month,
          isDefault
        };
        return this.db.Card.update(cardInfo, {where: {UserId: null, id: req.params.cardId}, transaction: t});
      })
      .then(() => res.json({ok: true, data: cardInfo})))
      .catch(error => {
        if(error.message === 'stripe_token_invalid') {
          return ResponseHelper.responseErrorParam(res, 'stripe token invalid.');
        } else if(error.type === 'StripeCardError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'StripeInvalidRequestError') {
          return ResponseHelper.responseError(res, error);
        } else if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * DELETE /user/cards/:cardId
   * delete card for guest
   * @param {*} req 
   * @param {*} res 
   */
  deleteGuestStripeCard(req, res) {
    loggingHelper.log('info', 'deleteGuestStripeCard start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.Card.destroy({where: {
      id: req.params.cardId,
      UserId: null
    }})
      .then(() => res.json({ok: true}))
      .then(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /** DELETE /users/:userId/cards/:cardId
   * Delete Card
   */
  deleteCard(req, res) {
    loggingHelper.log('info', 'deleteCard start');
    loggingHelper.log('info', `param: req.params = ${req.params}`);
    this.db.sequelize.transaction(t => this.db.Subscription.count({where: {
      CardId: req.params.cardId,
      status: 'Processing'
    }})
      .then(count => {
        if(count > 0) {
          throw new Error('can_remove');
        } else {
          return this.db.Card.update({isRemoved: true}, {where: {
            id: req.params.cardId
          }, transaction: t, returning: true});
        }
      })
      .then(() => this.db.Card.findOne({where: {UserId: req.params.userId, isDefault: true}, transaction: t}))
      .then(card => {
        if(!card) {
          return this.db.Card.findOne({where: {UserId: req.params.userId, type: 'website'}, order: [['createdAt', 'DESC']], transaction: t});
        }
      })
      .then(card => {
        if(card) {
          return this.db.Card.update({isDefault: true}, {where: {id: card.id}, transaction: t});
        }
      }))
    .then(() => res.json({ok: true}))
    .catch(error => {
      if(error.message === 'can_remove') {
        return ResponseHelper.responseError(res, 'Oh no! This card can\'t be removed as it is currently connected to an active Shave Plan.');
      } else {
        return ResponseHelper.responseError(res, error);
      }
    });
  }

  /**
   * PUT /users/:userId/subscriptions/:subsId
   * update card info and delivery info for subscription
   * @param {*} req 
   * @param {*} res 
   */
  updateSubscriptionDetails(req, res) {
    loggingHelper.log('info', 'updateSubscriptionDetails start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let params = req.body;
    let options = {};

    // check input data
    if(!params.cardId && !params.deliveryAddressId && !params.shipingAddressId) {
      return ResponseHelper.responseSuccess(res);
    }

    // build update data
    if(params.cardId) {
      options = Object.assign(options, {CardId: params.cardId});
    }
    if(params.deliveryAddressId) {
      options = Object.assign(options, {DeliveryAddress: params.deliveryAddressId});
    }
    if(params.shipingAddressId) {
      options = Object.assign(options, {BillingAddress: params.shipingAddressId});
    }

    // perform update data
    this.db.Subscription.update(options, {where: {id: req.params.subsId}})
      .then(() => SubscriptionHelper.getSubscription(req.params.subsId, this.db))
      .then(subs1 => {
        subs1 = SequelizeHelper.convertSeque2Json(subs1);
        if(subs1.status === 'On Hold') {
          return SubscriptionHelper.performCharge(subs1)
            .then(data =>
              SubscriptionHelper.updateSubscriptionStatus(SequelizeHelper.convertSeque2Json(data), this.db)
                .then(() => SubscriptionHelper.getSubscription(req.params.subsId, this.db))
                .then(subs2 => SubscriptionHelper.performDeliver(SequelizeHelper.convertSeque2Json(subs2), this.db))
            )
            .catch(error => {
              console.log(`Charge subscription [${req.params.subsId}] error: ${error.stack}`);
              loggingHelper.log('error', `Charge subscription [${req.params.subsId}] error: ${error.stack}`);
              // update subscription status
              return this.db.Subscription.update({status: 'On Hold'}, {where: {id: req.params.subsId}})
                .then(() => sendEmailQueueService.addTask({method: 'sendChargeSubscriptionFail', data: {subscriptionId: req.params.subsId}, langCode: req.country.defaultLang.toLowerCase()})); // send email to user
            });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

}

export default function(...args) {
  return new UserApi(...args);
}
