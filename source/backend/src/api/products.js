import Promise from "bluebird";
import _ from "underscore";
import ResponseHelper from "../helpers/response-helper";
import RequestHelper from "../helpers/request-helper";
import SequelizeHelper from "../helpers/sequelize-helper";
import JwtMiddleware from "../middleware/jwt-middleware";
import multipart from "connect-multiparty";
import { awsHelper } from "../helpers/aws-helper";
import config from "../config";
import { loggingHelper } from "../helpers/logging-helper";

let multipartMiddleware = multipart();

class ProductAPI {
  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get("/product-types", this.getProductType.bind(this));
    this.api.get("/admin-product-types", this.adminGetProductType.bind(this));
    this.api.post(
      "/product-types",
      JwtMiddleware.verifyAdminToken,
      multipartMiddleware,
      this.postProductType.bind(this)
    );
    this.api.put(
      "/product-types/:id",
      JwtMiddleware.verifyAdminToken,
      multipartMiddleware,
      this.putProductType.bind(this)
    );
    this.api.get(
      "/product-types/:id",
      JwtMiddleware.verifyAdminToken,
      this.getAdminProductTypeDetails.bind(this)
    );
    this.api.post(
      "/product-types/change-status",
      JwtMiddleware.verifyAdminToken,
      this.postUpdateProductTypeStatus.bind(this)
    );
    this.api.get("/products/panel", this.getProductPanel.bind(this));
    this.api.get("/products/:slug", this.getProductDetails.bind(this));
    this.api.get("/products", this.getProducts.bind(this));
    this.api.get("/product-relateds", this.getProductRelateds.bind(this));
    this.api.get("/products/awesome-shave-kits", this.getASKItems.bind(this));

    // admin APIs
    this.api.get(
      "/admin-products",
      JwtMiddleware.verifyAdminToken,
      this.getAdminProducts.bind(this)
    );
    this.api.get(
      "/admin-products/:productId",
      JwtMiddleware.verifyAdminToken,
      this.getAdminProductDetails.bind(this)
    );
    this.api.post(
      "/admin-products",
      JwtMiddleware.verifyAdminToken,
      multipartMiddleware,
      this.postAdminProduct.bind(this)
    );
    this.api.put(
      "/admin-products/:id",
      JwtMiddleware.verifyAdminToken,
      multipartMiddleware,
      this.putAdminProduct.bind(this)
    );

    // manage product related
    this.api.delete(
      "/admin-products/product-relateds/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductRelated.bind(this)
    );
    this.api.post(
      "/admin-products/change-status",
      JwtMiddleware.verifyAdminToken,
      this.postUpdateProductStatus.bind(this)
    );

    // manage product translate
    this.api.post(
      "/admin-products/:productId/product-translates",
      JwtMiddleware.verifyAdminToken,
      this.postProductTranslate.bind(this)
    );
    this.api.put(
      "/admin-products/:productId/product-translates/:id",
      JwtMiddleware.verifyAdminToken,
      this.putProductTranslate.bind(this)
    );
    this.api.delete(
      "/admin-products/:productId/product-translates/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductTranslate.bind(this)
    );

    // manage product country
    this.api.post(
      "/admin-products/:productId/product-countries",
      JwtMiddleware.verifyAdminToken,
      this.postProductCountry.bind(this)
    );
    this.api.put(
      "/admin-products/:productId/product-countries/:id",
      JwtMiddleware.verifyAdminToken,
      this.putProductCountry.bind(this)
    );
    this.api.delete(
      "/admin-products/:productId/product-countries/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductCountry.bind(this)
    );

    // manage product detail
    this.api.delete(
      "/admin-products/:productId/product-details/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductDetail.bind(this)
    );

    // manage product detail
    this.api.delete(
      "/admin-products/:productId/product-images/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductImage.bind(this)
    );
    this.api.delete(
      "/product-types/:productTypeId/translate/:id",
      JwtMiddleware.verifyAdminToken,
      this.deleteProductTypeTranslate.bind(this)
    );
  }

  _getProductInclude(countryCode, getRelated) {
    let include = [
      {
        model: this.db.ProductType,
        as: "ProductType",
        include: ["productTypeTranslate"]
      },
      "productImages",
      "productDetails",
      {
        model: this.db.ProductTranslate,
        as: "productTranslate",
        attributes: {
          exclude: ["ProductId", "createdAt", "updatedAt"]
        }
      }
    ];
    let relatedInclude = {};
    if (getRelated) {
      relatedInclude = {
        model: this.db.ProductRelated,
        as: "productRelateds",
        include: [
          {
            model: this.db.ProductCountry,
            as: "RelatedProductCountry",
            include: [
              {
                model: this.db.Product,
                // as: "RelatedProducts",
                include: ["productImages", "productTranslate"]
              }
            ]
          }
        ]
      };
    } else {
      relatedInclude = {
        model: this.db.ProductRelated,
        as: "productRelateds"
      };
    }

    if (countryCode) {
      include.push({
        model: this.db.ProductCountry,
        as: "productCountry",
        attributes: {
          exclude: ["CountryId", "ProductId", "createdAt", "updatedAt"]
        },
        include: [
          {
            model: this.db.Country,
            where: { code: countryCode }
          },
          relatedInclude
        ]
      });
    } else {
      include.push({
        model: this.db.ProductCountry,
        as: "productCountry",
        attributes: {
          exclude: ["CountryId", "ProductId", "createdAt", "updatedAt"]
        },
        include: ["Country", relatedInclude]
      });
    }
    return include;
  }

  //------------------------------------------------------------------------------------------
  getASKItems() {
    loggingHelper.log("info", "getASKProducts start");
    let params = req.query || {};
    let options = RequestHelper.buildWhereCondition(
      {},
      params,
      this.db.Product
    );
    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }

    // order by UpdatedAT
    options.order = [
      ["productCountry", "order", "DESC"],
      ["updatedAt", "DESC"]
    ];

    // filter active product
    options.where = Object.assign(options.where, { status: "active" });

    if (!params.userTypeId || +params.userTypeId === 1) {
      options.where = Object.assign(options.where, {
        "$productCountry.isOnline$": true
      });
    } else {
      options.where = Object.assign(options.where, {
        "$productCountry.isOffline$": true
      });
    }

    // select product
    this._findProduct(options, countryCode.toUpperCase())
      .then(products => {
        if (params.limit) {
          let offset = params.offset || 0;
          products = products.slice(offset, offset + parseInt(params.limit));
        }
        res.json(products);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  //------------------------------------------------------------------------------------------
  /** GET /product-types
   * get all product-types in DB
   */
  getProductType(req, res) {
    loggingHelper.log("info", "getProductType start");

    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }

    this.db.ProductType.findAndCountAll({
      include: [
        {
          model: this.db.Product,
          as: "products",
          include: {
            model: this.db.ProductCountry,
            as: "productCountry",
            attributes: {
              exclude: ["CountryId", "ProductId", "createdAt", "updatedAt"]
            },
            include: [
              {
                model: this.db.Country,
                where: { code: countryCode.toUpperCase() }
              }
            ]
          }
        },
        "productTypeTranslate"
      ],
      order: [["order", "DESC"], ["updatedAt", "DESC"]]
    })
      .then(result => {
        let returnValue = [];
        result = SequelizeHelper.convertSeque2Json(result);
        let productTypes = result.rows;
        productTypes.forEach(productType => {
          if (productType.products.length > 0) {
            productType.products.forEach(product => {
              if (
                product.productCountry &&
                product.productCountry[0] &&
                !returnValue.filter(type => type.id === productType.id)[0]
              ) {
                Reflect.deleteProperty(productType, "products");
                returnValue.push(productType);
              }
            });
          }
        });
        result.rows = returnValue;
        return res.json(result);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  adminGetProductType(req, res) {
    loggingHelper.log("info", "getProductType start");
    this.db.ProductType.findAndCountAll({
      include: ["productTypeTranslate"],
      order: [["order", "DESC"], ["updatedAt", "DESC"]]
    })
      .then(result => res.json(result))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /products/panel
   * get product will be display as a panel
   * @param {*} req
   * @param {*} res
   */
  getProductPanel(req, res) {
    loggingHelper.log("info", "getProductPanel start");
    loggingHelper.log("info", `param: req.country.code = ${req.country.code}`);
    console.log(`req.country.code ${req.country.code}`);
    let productPanel = Object.assign({}, config.productPanel[req.country.code]);
    this.db.ProductCountry.findOne({
      where: { id: productPanel.productCountryId },
      include: [
        "Country",
        {
          model: this.db.Product,
          include: [
            "productImages",
            "productDetails",
            "productTranslate",
            "ProductType"
          ]
        }
      ]
    })
      .then(result => {
        if (result) {
          let product = SequelizeHelper.convertSeque2Json(result.Product);
          product.productCountry = result;
          productPanel.Product = product;
          productPanel.description = productPanel.description.replace(
            "{{price}}",
            `${result.Country.currencyDisplay} ${result.sellPrice}`
          );
          return res.json(productPanel);
        } else {
          return res.json([]);
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /product-types
   * @param {*} req
   * @param {*} res
   */
  postProductType(req, res) {
    loggingHelper.log("info", "postProductType start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.url;
    params.productTypeTranslate = params.productTypeTranslate
      ? params.productTypeTranslate
      : [];

    // validate images
    if (!image) {
      return ResponseHelper.responseMissingParam(res);
    }

    // upload image to S3
    awsHelper
      .upload(config.AWS.productImagesAlbum, image)
      .then(uploadedImage => {
        params.url = uploadedImage.Location;
        this.db.ProductType.create(params, {
          include: ["productTypeTranslate"],
          returning: true
        }).then(productTypes => {
          res.json(productTypes);
        });
      })
      .catch(err => {
        if (err.name === "SequelizeValidationError") {
          return ResponseHelper.responseErrorParam(res, err);
        } else if (err.name === "SequelizeUniqueConstraintError") {
          // return ResponseHelper.responseError(res, err);
          return ResponseHelper.responseError(res, "name is existed");
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }

  /**
   * PUT /product-types/:id
   * @param {*} req
   * @param {*} res
   */
  putProductType(req, res) {
    loggingHelper.log("info", "putProductType start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let image = req.files.url;
    // this.db.ProductType.update(params, {where: {id: req.params.id}, returning: true})

    // validate images
    // if(!image) {
    //   return ResponseHelper.responseMissingParam(res);
    // }

    // init transaction
    let updateTranslates = [];
    let createTranslates = [];
    params.productTypeTranslate = params.productTypeTranslate
      ? params.productTypeTranslate
      : [];

    if (image) {
      // upload product image
      awsHelper
        .upload(config.AWS.productImagesAlbum, image)
        .then(uploadedImage => {
          params.url = uploadedImage.Location;
          // start transaction
          this.db.sequelize
            .transaction(t => {
              params.productTypeTranslate.forEach(translate => {
                if (translate.id) {
                  updateTranslates.push(translate);
                } else {
                  translate.ProductTypeId = req.params.id;
                  createTranslates.push(translate);
                }
              });
              return SequelizeHelper.bulkUpdate(
                updateTranslates,
                this.db,
                "ProductTypeTranslate",
                { transaction: t }
              )
                .then(() =>
                  this.db.ProductTypeTranslate.bulkCreate(createTranslates, {
                    returning: true,
                    transaction: t
                  })
                )
                .then(() =>
                  this.db.ProductType.update(params, {
                    where: { id: req.params.id },
                    returning: true,
                    transaction: t
                  })
                );
            })
            .then(() => ResponseHelper.responseSuccess(res))
            .catch(err => {
              if (err.name === "SequelizeValidationError") {
                return ResponseHelper.responseErrorParam(res, err);
              } else if (err.name === "SequelizeUniqueConstraintError") {
                // return ResponseHelper.responseError(res, err);
                return ResponseHelper.responseError(res, "name is existed");
              } else {
                return ResponseHelper.responseInternalServerError(res, err);
              }
            });
        })
        .catch(() =>
          ResponseHelper.responseError(res, "Cannot upload image to S3")
        );
    } else {
      // start transaction
      this.db.sequelize
        .transaction(t => {
          params.productTypeTranslate.forEach(translate => {
            if (translate.id) {
              updateTranslates.push(translate);
            } else {
              translate.ProductTypeId = req.params.id;
              createTranslates.push(translate);
            }
          });
          return SequelizeHelper.bulkUpdate(
            updateTranslates,
            this.db,
            "ProductTypeTranslate",
            { transaction: t }
          )
            .then(() =>
              this.db.ProductTypeTranslate.bulkCreate(createTranslates, {
                returning: true,
                transaction: t
              })
            )
            .then(() =>
              this.db.ProductType.update(params, {
                where: { id: req.params.id },
                returning: true,
                transaction: t
              })
            );
        })
        .then(() => ResponseHelper.responseSuccess(res))
        .catch(err => {
          if (err.name === "SequelizeValidationError") {
            return ResponseHelper.responseErrorParam(res, err);
          } else if (err.name === "SequelizeUniqueConstraintError") {
            // return ResponseHelper.responseError(res, err);
            return ResponseHelper.responseError(res, "name is existed");
          } else {
            return ResponseHelper.responseInternalServerError(res, err);
          }
        });
    }
  }

  _findProduct(options, countryCode) {
    // build output value
    Object.assign(options, {
      attributes: {
        exclude: ["ProductTypeId"]
      },
      include: this._getProductInclude(countryCode)
    });

    return this.db.Product.findAll(options).then(products => {
      let returnValue = [];
      products.forEach(product => {
        product = SequelizeHelper.convertSeque2Json(product);

        // get unique product country
        if (product.productCountry && product.productCountry.length !== 0) {
          product.productCountry = product.productCountry[0];
          returnValue.push(product);
        }
      });
      return returnValue;
    });
  }

  /** GET /product-types
   * get all product-types in DB
   */
  getProducts(req, res) {
    loggingHelper.log("info", "getProducts start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    console.log("getproduct");
    // build query from inparams
    let params = req.query || {};
    let options = RequestHelper.buildWhereCondition(
      {},
      params,
      this.db.Product
    );
    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }

    // order by UpdatedAT
    options.order = [
      ["productCountry", "order", "DESC"],
      ["updatedAt", "DESC"]
    ];

    // filter active product
    options.where = Object.assign(options.where, { status: "active" });

    if (!params.userTypeId || +params.userTypeId === 1) {
      options.where = Object.assign(options.where, {
        "$productCountry.isOnline$": true
      });
    } else {
      options.where = Object.assign(options.where, {
        "$productCountry.isOffline$": true
      });
    }

    // select product
    this._findProduct(options, countryCode.toUpperCase())
      .then(products => {
        if (params.limit) {
          let offset = params.offset || 0;
          products = products.slice(offset, offset + parseInt(params.limit));
        }
        res.json(products);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
  /**PRODUCT RELATED START -------------------------------------------- */

  /** GET /product-relateds
   * get all product-relateds in DB
   */
  getProductRelateds(req, res) {
    loggingHelper.log("info", "getProductRelateds start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    console.log("getProductRelateds");
    // build query from inparams
    let params = req.query || {};
    let options = RequestHelper.buildWhereCondition(
      {},
      params,
      this.db.Product
    );
    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }
    if (
      params.productCountryIds &&
      JSON.parse(params.productCountryIds).length >= 0
    ) {
      params.productCountryIds = JSON.parse(params.productCountryIds);
    } else {
      return ResponseHelper.responseMissingParam(res);
    }

    // order by sellPrice
    options.order = [
      ["productCountry", "sellPrice", "DESC"],
      ["createdAt", "DESC"]
    ];

    // filter active product
    options.where = Object.assign(options.where, { status: "active" });

    // limit 3 product
    // options.limit = 3;

    if (!params.userTypeId || +params.userTypeId === 1) {
      options.where = Object.assign(options.where, {
        "$productCountry.isOnline$": true
      });
    } else {
      options.where = Object.assign(options.where, {
        "$productCountry.isOffline$": true
      });
    }

    let relatedProductIds = [];
    return this.db.ProductCountry.findAll({
      where: { id: { $in: params.productCountryIds } },
      include: ["productRelateds"]
    })
      .then(productCountrys => {
        productCountrys.forEach(productCountry => {
          productCountry = SequelizeHelper.convertSeque2Json(productCountry);
          if (productCountry.productRelateds.length > 0) {
            productCountry.productRelateds.forEach(proRelated => {
              if (
                !params.productCountryIds.find(
                  id => id === proRelated.RelatedProductCountryId
                )
              ) {
                relatedProductIds.push(proRelated.RelatedProductCountryId);
              }
            });
          }
        });
        return relatedProductIds;
      })
      .then(() => {
        options.where = { "$productCountry.id$": { $in: relatedProductIds } };
        options.include = this._getProductInclude(countryCode.toUpperCase());
        return this.db.Product.findAll(options);
      })
      .then(products => {
        products = SequelizeHelper.convertSeque2Json(products);
        let result = [];
        products.forEach(product => {
          if (product.productCountry.length > 0) {
            product.productCountry = product.productCountry[0];
          }
          if (result.length < 3) {
            result.push(product);
          }
        });
        return res.json(result);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }
  /**PRODUCT RELATED END -------------------------------------------- */

  /** GET /product-types
   * get all product-types in DB
   */
  getProductDetails(req, res) {
    loggingHelper.log("info", "getProductDetails start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    let countryCode = req.headers["country-code"];
    if (!countryCode) {
      return ResponseHelper.responseError(res, "missing country Code");
    }
    let includeClause = this._getProductInclude(countryCode.toUpperCase());
    includeClause.push({
      model: this.db.ProductReview,
      as: "productReviews",
      attributes: {
        exclude: ["ProductId", "createdAt"]
      }
    });

    console.log(`includeClause ${includeClause}`);

    // perform query
    this.db.Product.findOne({
      where: {
        slug: req.params.slug
      },
      include: includeClause
    })
      .then(product => {
        product = SequelizeHelper.convertSeque2Json(product);
        if (product && product.productCountry.length !== 0) {
          product.productCountry = product.productCountry[0];
          res.json(product);
        } else {
          ResponseHelper.responseNotFoundError(
            res,
            `Product(${params.productId})`
          );
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /** GET /admin-product
   * get all product without the country
   */
  getAdminProducts(req, res) {
    loggingHelper.log("info", "getAdminProducts start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    let params = req.query || {};
    // build limit and off set
    let options = {};
    let returnValue;
    if (params.sku) {
      params.sku = { $like: `%${params.sku}%` };
    }

    options.include = [];
    options.where = {};

    // search function
    if (params.s && params.s !== "") {
      options.distinct = true;

      options.include.push({
        model: this.db.ProductTranslate,
        duplicating: false,
        as: "productTranslate"
      });

      options.where = Object.assign(options.where, {
        $or: [
          { sku: { $like: `%${params.s}%` } },
          { "$productTranslate.name$": { $like: `%${params.s}%` } }
        ]
      });
    }

    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(
      options,
      params,
      this.db.Product
    );

    if (!params.CountryId) {
      // find product base on limit and offset
      this.db.Product.findAndCountAll(options)
        .then(result => {
          returnValue = result;
          return Promise.mapSeries(result.rows, product => {
            let _options = {
              where: {
                id: product.id
              },
              include: this._getProductInclude()
            };
            return this.db.Product.findOne(_options);
          });
        })
        .then(result => {
          returnValue.rows = result;
          return res.json(returnValue);
        })
        .catch(error => ResponseHelper.responseInternalServerError(res, error));
    } else {
      options.include.push({
        model: this.db.ProductCountry,
        as: "productCountry",
        duplicating: false,
        where: { CountryId: params.CountryId },
        include: ["Country"]
      });

      this.db.Product.findAndCountAll(options)
        .then(result => {
          returnValue = result;
          return Promise.mapSeries(result.rows, product => {
            let _options = {
              where: {
                id: product.id
              },
              include: this._getProductInclude(
                product.productCountry[0].Country.code
              )
            };
            return this.db.Product.findOne(_options);
          });
        })
        .then(result => {
          returnValue.rows = result;
          return res.json(returnValue);
        })
        .catch(error => ResponseHelper.responseInternalServerError(res, error));
    }
  }

  /** GET /product-types
   * get all product-types in DB
   */
  getAdminProductDetails(req, res) {
    loggingHelper.log("info", "getAdminProductDetails start");
    loggingHelper.log("info", `param: req.query = ${req.query}`);
    // get param in request
    let params = req.query || {};
    let includeClause = this._getProductInclude(false, true);
    includeClause.push({
      model: this.db.ProductReview,
      as: "productReviews",
      attributes: {
        exclude: ["ProductId", "createdAt"]
      }
    });

    // perform query
    this.db.Product.findOne({
      where: {
        id: req.params.productId
      },
      include: includeClause
    })
      .then(product => {
        if (product) {
          res.json(product);
        } else {
          ResponseHelper.responseNotFoundError(
            res,
            `Product(${params.productId})`
          );
        }
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /admin-products
   * @param {*} req
   * @param {*} res
   */
  postAdminProduct(req, res) {
    loggingHelper.log("info", "postAdminProduct start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let images;
    let detailImages;
    let uploadedImages = [];
    let uploadedDetailImages = [];

    // validate require params
    if (
      !params.ProductTypeId ||
      !params.productTranslate ||
      !params.productCountry ||
      !params.images
    ) {
      return ResponseHelper.responseMissingParam(res);
    }

    // check Length Of ProductDetail images
    if (params.productDetails) {
      params.productDetails = Array.isArray(params.productDetails)
        ? params.productDetails
        : [params.productDetails];
      detailImages = _.pluck(req.files.productDetails, "image");
    }

    if (detailImages && detailImages.length !== params.productDetails.length) {
      return ResponseHelper.responseMissingParam(res, "missing detail images");
    }

    // check Length Of ProductDetail images
    params.images = Array.isArray(params.images)
      ? params.images
      : [params.images];
    images = _.pluck(req.files.images, "image");

    if (images.length !== params.images.length) {
      return ResponseHelper.responseMissingParam(res, "missing images");
    }

    console.log("start up load image to S3");
    loggingHelper.log("info", "start up load image to S3");
    Promise.mapSeries(images, image =>
      awsHelper.upload(config.AWS.productImagesAlbum, image, true)
    )
      .then(_uploadedImages => {
        uploadedImages = _uploadedImages;
        // upload detail images
        if (detailImages) {
          return Promise.mapSeries(detailImages, image =>
            awsHelper.upload(config.AWS.productImagesAlbum, image)
          );
        }
      })
      .then(_uploadedImages => {
        console.log("finish up load image to S3");
        loggingHelper.log("info", "finish up load image to S3");
        uploadedDetailImages = _uploadedImages;
        // start transaction
        let product;
        this.db.sequelize.transaction(t =>
          this.db.Product.create(params, { returning: true, transaction: t })
            .then(_product => {
              product = _product;
              let productImages = [];
              let productImage;
              // build ProductImages data
              params.images.forEach((image, idx) => {
                image.isDefault = image.isDefault ? image.isDefault : false;
                productImage = {
                  ProductId: product.id,
                  url: uploadedImages[idx].Location,
                  isDefault: image.isDefault
                };
                productImages.push(productImage);
              });

              // insert product image data
              return this.db.ProductImage.bulkCreate(productImages, {
                transaction: t
              });
            })
            .then(() => {
              // build product translate object
              params.productTranslate = Array.isArray(params.productTranslate)
                ? params.productTranslate
                : [params.productTranslate];
              params.productTranslate.forEach(productTranslate => {
                productTranslate.ProductId = product.id;
              });
              // insert product translate
              return this.db.ProductTranslate.bulkCreate(
                params.productTranslate,
                { transaction: t }
              );
            })
            .then(() => console.log("#3 ---> " + params.productCountry))
            .then(() =>
              this.updateProductCountry(params.productCountry, product.id, t)
            )
            // .then(() => {
            //   // build product country
            //   params.productCountry = Array.isArray(params.productCountry) ? params.productCountry : [params.productCountry];
            //   params.productCountry.forEach(productCountry => {
            //     productCountry.ProductId = product.id;
            //   });
            //   // insert product country
            //   console.log('params.productCountry ===== ==== ===== ======= ', params.productCountry);
            //   return this.db.ProductCountry.bulkCreate(params.productCountry, {include: ['productRelateds'], transaction: t});
            // })
            .then(() => {
              if (params.productDetails) {
                // build product detail
                params.productDetails.forEach((detail, idx) => {
                  detail.ProductId = product.id;
                  detail.imageUrl = uploadedDetailImages[idx].Location;
                });
                // insert product deatil
                return this.db.ProductDetail.bulkCreate(params.productDetails, {
                  transaction: t
                });
              }
            })
            .then(() => res.json({ ok: true }))
            .catch(error => {
              if (error.name === "SequelizeValidationError") {
                return ResponseHelper.responseErrorParam(res, error);
              } else if (error.name === "SequelizeUniqueConstraintError") {
                // return ResponseHelper.responseError(res, error);
                return ResponseHelper.responseError(
                  res,
                  "sku or slug is existed"
                );
              } else {
                return ResponseHelper.responseInternalServerError(res, error);
              }
            })
        );
      });
  }

  /**
   * PUT /admin-products:/:id
   * Update admin product
   * @param {*} req
   * @param {*} res
   */
  putAdminProduct(req, res) {
    loggingHelper.log("info", "putAdminProduct start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let productId = req.params.id;
    let images = [];
    let imageDefaultId;
    let detailImages = [];
    let uploadedImages = [];
    let uploadedDetailImages = [];
    let productImagesHasImage = [];
    let productImagesCreate = [];
    let productImagesDelete = [];
    let productImagesUpdate = [];
    let productTranslateUpdates = [];
    let productTranslateCreates = [];
    // let productCountryUpdates = [];
    // let productCountryCreates = [];
    let productDetailsUpdates = [];
    let productDetailsCreates = [];
    let productDetailHasImage = [];
    // let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check permisstion to update product
    // if(role !== this.db.Admin.rawAttributes.role.values[0] && params.status === 'removed') {
    //   throw ResponseHelper.responsePermissonDenied(res);
    // }

    // check Length Of ProductDetail images
    if (params.images) {
      params.images = Array.isArray(params.images)
        ? params.images
        : [params.images];
      images = _.pluck(req.files.images, "image");
      images.forEach(image => {
        productImagesHasImage.push(
          parseInt(image.fieldName.replace(/[^\[]+\[(\d+)\].*/g, "$1"))
        );
      });
    }

    console.log(`productImagesHasImage === ${productImagesHasImage}`);
    loggingHelper.log(
      "info",
      `productImagesHasImage === ${productImagesHasImage}`
    );

    // check Length Of ProductDetail images
    if (params.productDetails) {
      params.productDetails = Array.isArray(params.productDetails)
        ? params.productDetails
        : [params.productDetails];
      detailImages = _.pluck(req.files.productDetails, "image");
      detailImages.forEach(detail => {
        productDetailHasImage.push(
          parseInt(detail.fieldName.replace(/[^\[]+\[(\d+)\].*/g, "$1"))
        );
      });
    }

    console.log("start up load image to S3");
    loggingHelper.log("info", "start up load image to S3");
    // upload product image
    Promise.mapSeries(images, image =>
      awsHelper.upload(config.AWS.productImagesAlbum, image, true)
    )
      .then(_uploadedImages => {
        uploadedImages = _uploadedImages;
        // upload detail images
        return Promise.mapSeries(detailImages, image =>
          awsHelper.upload(config.AWS.productImagesAlbum, image)
        );
      })
      .then(_uploadedImages => {
        console.log("finish up load image to S3");
        loggingHelper.log("info", "finish up load image to S3");
        uploadedDetailImages = _uploadedImages;
        // start transaction
        this.db.sequelize
          .transaction(t =>
            this.db.Product.update(
              params,
              { where: { id: productId } },
              { transaction: t }
            )
              .then(() => {
                if (params.images) {
                  params.images.forEach((productImage, idx) => {
                    if (productImagesHasImage.indexOf(idx) > -1) {
                      productImage.url =
                        uploadedImages[
                          productImagesHasImage.indexOf(idx)
                        ].Location;
                    }
                    // determine which image is removed and which image is create
                    if (productImage.isRemoved) {
                      productImagesDelete.push(productImage.id);
                    } else if (!productImage.id) {
                      productImage.ProductId = productId;
                      productImagesCreate.push(productImage);
                    } else {
                      productImagesUpdate.push(productImage);
                    }
                    // determine which image is default
                    if (
                      productImage.isDefault &&
                      productImage.isDefault === "true" &&
                      productImage.id
                    ) {
                      imageDefaultId = productImage.id;
                    }
                  });
                  return this.db.ProductImage.update(
                    { isDefault: false },
                    { where: { ProductId: productId }, transaction: t }
                  );
                } else {
                  return;
                }
              })
              .then(() =>
                this.db.ProductImage.bulkCreate(productImagesCreate, {
                  transaction: t
                })
              )
              .then(() =>
                this.db.ProductImage.destroy({
                  where: { id: { $in: productImagesDelete } },
                  transaction: t
                })
              )
              .then(() =>
                SequelizeHelper.bulkUpdate(
                  productImagesUpdate,
                  this.db,
                  "ProductImage",
                  { transaction: t }
                )
              )
              .then(() => {
                if (imageDefaultId) {
                  console.log(`imageDefaultId == ${imageDefaultId}`);
                  loggingHelper.log(
                    "info",
                    `imageDefaultId == ${imageDefaultId}`
                  );
                  return this.db.ProductImage.update(
                    { isDefault: true },
                    { where: { id: imageDefaultId }, transaction: t }
                  );
                } else {
                  return;
                }
              })
              .then(() => {
                // determined product translate update and create
                if (params.productTranslate) {
                  params.productTranslate = Array.isArray(
                    params.productTranslate
                  )
                    ? params.productTranslate
                    : [params.productTranslate];
                  params.productTranslate.forEach(productTranslate => {
                    if (productTranslate.id) {
                      productTranslateUpdates.push(productTranslate);
                    } else {
                      productTranslate.ProductId = productId;
                      productTranslateCreates.push(productTranslate);
                    }
                  });
                  return SequelizeHelper.bulkUpdate(
                    productTranslateUpdates,
                    this.db,
                    "ProductTranslate",
                    { transaction: t }
                  );
                } else {
                  return;
                }
              })
              .then(() =>
                this.db.ProductTranslate.bulkCreate(productTranslateCreates, {
                  transaction: t
                })
              )
              .then(() =>
                console.log("#3 ---> " + JSON.stringify(params.productCountry))
              )
              .then(() =>
                this.updateProductCountry(params.productCountry, productId, t)
              )
              .then(() => {
                // determined product country update and create
                if (params.productCountry) {
                  let productCountryUpdates = [];
                  params.productCountry = Array.isArray(params.productCountry)
                    ? params.productCountry
                    : [params.productCountry];
                  params.productCountry.forEach(productCountry => {
                    if (productCountry.id) {
                      productCountryUpdates.push(productCountry);
                    } else {
                      productCountry.ProductId = productId;
                      // productCountryCreates.push(productCountry);
                    }
                  });
                  // update sellPrice for subscription contains the edited product
                  return Promise.map(productCountryUpdates, item => {
                    let subscriptionUpdates = [];
                    let productCountries;
                    return this.db.ProductCountry.findAll()
                      .then(_productCountries => {
                        productCountries = SequelizeHelper.convertSeque2Json(
                          _productCountries
                        );
                        return this.db.CustomPlanDetail.findAll({
                          where: { ProductCountryId: item.id }
                        })
                          .then(data => {
                            data = SequelizeHelper.convertSeque2Json(data);
                            let customPlanIds = _.pluck(data, "CustomPlanId");
                            return this.db.CustomPlanDetail.findAll({
                              where: { CustomPlanId: { $in: customPlanIds } },
                              order: ["CustomPlanId"]
                            });
                          })
                          .then(customPlanDetails => {
                            customPlanDetails = SequelizeHelper.convertSeque2Json(
                              customPlanDetails
                            );
                            let customPlanId;
                            let price = 0;

                            customPlanDetails.forEach((element, index) => {
                              if (element.CustomPlanId !== customPlanId) {
                                if (customPlanId) {
                                  let itemUpdate = {
                                    CustomPlanId: customPlanId,
                                    price,
                                    pricePerCharge: price
                                  };
                                  subscriptionUpdates.push(itemUpdate);
                                }

                                if (
                                  parseInt(element.ProductCountryId) ===
                                  parseInt(item.id)
                                ) {
                                  console.log("#5 Else ---------");
                                  price = parseFloat(
                                    item.sellPrice * element.qty
                                  );
                                } else {
                                  console.log("#5 Else ---------");
                                  price = parseFloat(
                                    productCountries.find(
                                      productCountry =>
                                        productCountry.id ===
                                        element.ProductCountryId
                                    ).sellPrice * element.qty
                                  );
                                }
                              } else {
                                if (
                                  parseInt(element.ProductCountryId) ===
                                  parseInt(item.id)
                                ) {
                                  price =
                                    parseFloat(price) +
                                    parseFloat(item.sellPrice * element.qty);
                                } else {
                                  price =
                                    parseFloat(price) +
                                    parseFloat(
                                      productCountries.find(
                                        productCountry =>
                                          productCountry.id ===
                                          element.ProductCountryId
                                      ).sellPrice * element.qty
                                    );
                                }
                              }

                              if (index + 1 === customPlanDetails.length) {
                                let itemUpdate = {
                                  CustomPlanId: element.CustomPlanId,
                                  price,
                                  pricePerCharge: price
                                };
                                subscriptionUpdates.push(itemUpdate);
                              }
                              customPlanId = element.CustomPlanId;
                            });
                          });
                      })
                      .then(() =>
                        Promise.map(subscriptionUpdates, subscriptionUpdate =>
                          this.db.Subscription.update(
                            subscriptionUpdate,
                            {
                              where: {
                                CustomPlanId: subscriptionUpdate.CustomPlanId,
                                PlanCountryId: null,
                                status: { $in: ["Processing", "On Hold"] }
                              }
                            },
                            { transaction: t }
                          )
                        )
                      );
                  });
                } else {
                  return;
                }
              })
              .then(() => {
                // determined product details update and create
                if (params.productDetails) {
                  params.productDetails = Array.isArray(params.productDetails)
                    ? params.productDetails
                    : [params.productDetails];
                  params.productDetails.forEach((productDetails, idx) => {
                    if (productDetailHasImage.indexOf(idx) > -1) {
                      productDetails.imageUrl =
                        uploadedDetailImages[
                          productDetailHasImage.indexOf(idx)
                        ].Location;
                    }
                    if (productDetails.id) {
                      productDetailsUpdates.push(productDetails);
                    } else {
                      productDetails.ProductId = productId;
                      productDetailsCreates.push(productDetails);
                    }
                  });
                  return SequelizeHelper.bulkUpdate(
                    productDetailsUpdates,
                    this.db,
                    "ProductDetail",
                    { transaction: t }
                  );
                } else {
                  return;
                }
              })
              .then(() =>
                this.db.ProductDetail.bulkCreate(productDetailsCreates, {
                  transaction: t
                })
              )
          )
          .then(() => res.json({ ok: true }))
          .catch(error => {
            if (error.name === "SequelizeValidationError") {
              return ResponseHelper.responseErrorParam(res, error);
            } else if (error.name === "SequelizeUniqueConstraintError") {
              // return ResponseHelper.responseError(res, error);
              return ResponseHelper.responseError(
                res,
                "sku or slug is existed"
              );
            } else {
              return ResponseHelper.responseInternalServerError(res, error);
            }
          });
      })
      .catch(() =>
        ResponseHelper.responseError(res, "Cannot upload image to S3")
      );
  }

  updateProductCountry(productCountry, productId, t) {
    loggingHelper.log("info", "updateProductCountry start");
    loggingHelper.log(
      "info",
      `param: productCountry = ${productCountry}, productId = ${productId}`
    );
    let productCountryUpdate = [];
    let productCountryCreate = [];
    if (productCountry) {
      productCountry = Array.isArray(productCountry)
        ? productCountry
        : [productCountry];
      productCountry.forEach(item => {
        // item.savePercent = ((parseFloat(item.actualPrice) - parseFloat(item.sellPrice)) / parseFloat(item.actualPrice)) * 100;
        if (item.id) {
          productCountryUpdate.push(item);
        } else {
          item.ProductId = productId;
          // if(item.planOptions) {
          //   item.planOptions.forEach(option => {
          //     if(option.planOptionProducts) {
          //       let tmpProducts = [];
          //       option.planOptionProducts.forEach(opProduct => {
          //         if(opProduct) {
          //           let tmpProduct = {};
          //           tmpProduct.qty = opProduct['[qty]'];
          //           tmpProduct.ProductCountryId = opProduct['[ProductCountryId]'];
          //           tmpProducts.push(tmpProduct);
          //         }
          //       });
          //       option.planOptionProducts = tmpProducts;
          //     }
          //   });
          // }
          productCountryCreate.push(item);
        }
        console.log(
          "item.productRelateds ======= ===== ========= ========== ",
          item.productRelateds
        );
      });
      return Promise.map(productCountryUpdate, item =>
        this.db.ProductCountry.update(item, {
          where: { id: item.id },
          transaction: t
        })
          .then(() =>
            console.log("updateProductRelated -> " + JSON.stringify(item))
          )
          .then(() =>
            this.updateProductRelated(item.productRelateds, item.id, t)
          )
      ).then(() =>
        SequelizeHelper.bulkCreate(
          productCountryCreate,
          this.db,
          "ProductCountry",
          { include: ["productRelateds"], transaction: t }
        )
      );
    } else {
      return;
    }
  }

  updateProductRelated(productRelated, productCountryId, t) {
    loggingHelper.log("info", "updateProductRelated start");
    loggingHelper.log(
      "info",
      `param: productRelated = ${JSON.stringify(
        productRelated
      )}, productCountryId = ${productCountryId}`
    );
    // let planDetailUpdate = [];
    let productRelatedCreate = [];
    if (productRelated) {
      productRelated = Array.isArray(productRelated)
        ? productRelated
        : [productRelated];
      productRelated.forEach(item => {
        // item.ProductCountryId = productCountryId;
        // if(item.id) {
        //   planDetailUpdate.push(item);
        // } else {
        productRelatedCreate.push(item);
        // }
      });
      return (
        this.db.ProductRelated.destroy(
          { where: { ProductCountryId: productCountryId } },
          { transaction: t }
        )
          // .then(() => SequelizeHelper.bulkUpdate(planDetailUpdate, this.db, 'PlanDetail', {transaction: t}))
          .then(() =>
            this.db.ProductRelated.bulkCreate(productRelatedCreate, {
              transaction: t
            })
          )
      );
    } else {
      return;
    }
  }

  /**
   * DELETE /admin-products/product-relateds/:id
   * @param {*} req
   * @param {*} res
   */
  deleteProductRelated(req, res) {
    loggingHelper.log("info", "deleteProductRelated start");
    loggingHelper.log("info", `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.ProductRelated.destroy({ where: { id } })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /admin-products/change-status
   * Allow admin change status of multiple product
   * @param {*} req
   * @param {*} res
   */
  postUpdateProductStatus(req, res) {
    loggingHelper.log("info", `postUpdateProductStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if (!params.productIds || !params.status) {
      return ResponseHelper.responseMissingParam(res, "Params");
    }

    params.productIds = Array.isArray(params.productIds)
      ? params.productIds
      : [params.productIds];

    this.db.Product.update(
      { status: params.status },
      { where: { id: { $in: params.productIds } } }
    )
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /admin-product/productTranslate
   * add product translate
   * @param {*} req
   * @param {*} res
   */
  postProductTranslate(req, res) {
    loggingHelper.log("info", "postProductTranslate start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let productId = req.params.productId;
    this.db.Product.findOne({ where: { id: productId } })
      .then(product => {
        if (!product) {
          throw new Error("not_found");
        } else {
          params.ProductId = productId;
          return this.db.ProductTranslate.create(params, { returning: true });
        }
      })
      .then(productTranslate =>
        ResponseHelper.responseSuccess(res, productTranslate)
      )
      .catch(error => {
        if (error.message === "not_found") {
          return ResponseHelper.responseNotFoundError(
            res,
            this.db.Product.name
          );
        } else if (error.name === "SequelizeValidationError") {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /admin-product/:productId/product-translates
   * @param {*} req
   * @param {*} res
   */
  putProductTranslate(req, res) {
    loggingHelper.log("info", "putProductTranslate start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let id = req.params.id;
    this.db.ProductTranslate.findOne({ where: { id } })
      .then(productTranslate => {
        if (!productTranslate) {
          throw new Error("not_found");
        } else {
          return this.db.ProductTranslate.update(params, { where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "not_found") {
          return ResponseHelper.responseNotFoundError(
            res,
            this.db.ProductTranslate.name
          );
        } else if (error.name === "SequelizeValidationError") {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /admin-product/:productId/product-translates
   * @param {*} req
   * @param {*} res
   */
  deleteProductTranslate(req, res) {
    loggingHelper.log("info", "deleteProductTranslate start");
    loggingHelper.log("info", `param: req.params = ${req.params}`);
    let id = req.params.id;
    this.db.ProductTranslate.destroy({ where: { id } })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /admin-product/productCountry
   * add product countries
   * @param {*} req
   * @param {*} res
   */
  postProductCountry(req, res) {
    loggingHelper.log("info", "postProductCountry start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let productId = req.params.productId;
    this.db.Product.findOne({ where: { id: productId } })
      .then(product => {
        if (!product) {
          throw new Error("not_found");
        } else {
          params.ProductId = productId;
          return this.db.ProductCountry.create(params, { returning: true });
        }
      })
      .then(productCountry =>
        ResponseHelper.responseSuccess(res, productCountry)
      )
      .catch(error => {
        if (error.message === "not_found") {
          return ResponseHelper.responseNotFoundError(
            res,
            this.db.Product.name
          );
        } else if (error.name === "SequelizeValidationError") {
          return ResponseHelper.responseErrorParam(res, error);
        } else if (error.name === "SequelizeUniqueConstraintError") {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * PUT /admin-product/:productId/product-countries
   * @param {*} req
   * @param {*} res
   */
  putProductCountry(req, res) {
    loggingHelper.log("info", "putProductCountry start");
    loggingHelper.log("info", `param: req.body = ${req.body}`);
    let params = req.body;
    let id = req.params.id;
    this.db.ProductCountry.findOne({ where: { id } })
      .then(productCountry => {
        if (!productCountry) {
          throw new Error("not_found");
        } else {
          return this.db.ProductCountry.update(params, { where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "not_found") {
          return ResponseHelper.responseNotFoundError(
            res,
            this.db.ProductCountry.name
          );
        } else if (error.name === "SequelizeValidationError") {
          return ResponseHelper.responseErrorParam(res, error);
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * DELETE /admin-product/:productId/product-countries
   * @param {*} req
   * @param {*} res
   */
  deleteProductCountry(req, res) {
    loggingHelper.log("info", "deleteProductCountry start");
    loggingHelper.log("info", `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.ProductCountry.count({ where: { ProductId: req.params.productId } })
      .then(count => {
        if (count <= 1) {
          throw new Error("can_not_delete");
        } else {
          return this.db.ProductCountry.destroy({ where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "can_not_delete") {
          return ResponseHelper.responseError(res, "can not delete.");
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * DELETE /admin-product/:productId/product-details
   * @param {*} req
   * @param {*} res
   */
  deleteProductDetail(req, res) {
    loggingHelper.log("info", "deleteProductDetail start");
    loggingHelper.log("info", `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.ProductDetail.count({ where: { ProductId: req.params.productId } })
      .then(count => {
        if (count <= 1) {
          throw new Error("can_not_delete");
        } else {
          return this.db.ProductDetail.destroy({ where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "can_not_delete") {
          return ResponseHelper.responseError(res, "can not delete.");
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * DELETE /admin-product/:productId/product-images
   * @param {*} req
   * @param {*} res
   */
  deleteProductImage(req, res) {
    loggingHelper.log("info", "deleteProductImage start");
    loggingHelper.log("info", `param: req.params = ${req.params}`);
    let id = +req.params.id;
    this.db.ProductImage.count({ where: { ProductId: req.params.productId } })
      .then(count => {
        if (count <= 1) {
          throw new Error("can_not_delete");
        } else {
          return this.db.ProductImage.destroy({ where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "can_not_delete") {
          return ResponseHelper.responseError(res, "can not delete.");
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /** GET /product-types/:id
   * get product types details in DB
   */
  getAdminProductTypeDetails(req, res) {
    loggingHelper.log("info", "getAdminProductTypeDetails start");

    this.db.ProductType.findOne({
      where: { id: req.params.id },
      include: ["productTypeTranslate"]
    })
      .then(productType => ResponseHelper.responseSuccess(res, productType))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * POST /product-types/change-status
   * Allow admin change status of product type
   * @param {*} req
   * @param {*} res
   */
  postUpdateProductTypeStatus(req, res) {
    loggingHelper.log("info", `postUpdateProductTypeStatus param: ${req.body}`);
    let params = req.body;

    // check require field
    if (!params.productTypeIds || !params.status) {
      return ResponseHelper.responseMissingParam(res, "Params");
    }

    params.productTypeIds = Array.isArray(params.productTypeIds)
      ? params.productTypeIds
      : [params.productTypeIds];

    this.db.ProductType.update(
      { status: params.status },
      { where: { id: { $in: params.productTypeIds } } }
    )
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * DELETE /product-type-translate
   * allow admin delete product type translate
   * @param {*} req
   * @param {*} res
   */
  deleteProductTypeTranslate(req, res) {
    loggingHelper.log(
      "info",
      `deleteProductTypeTranslate: req.params = ${req.params}`
    );
    let id = +req.params.id;
    this.db.ProductTypeTranslate.count({
      where: { ProductTypeId: req.params.productTypeId }
    })
      .then(count => {
        if (count <= 1) {
          throw new Error("can_not_delete");
        } else {
          return this.db.ProductTypeTranslate.destroy({ where: { id } });
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if (error.message === "can_not_delete") {
          return ResponseHelper.responseError(res, "can not delete.");
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }
}

export default function(...args) {
  return new ProductAPI(...args);
}
