import ResponseHelper from '../helpers/response-helper';
import MailchimpHelper from '../helpers/mailchimp';
import { loggingHelper } from '../helpers/logging-helper';
import Validator from '../helpers/validator';

class NonEcommerceApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
    this.mailchimp = MailchimpHelper.getInstance();
  }

  registerRoute() {
    // term
    this.api.get('/non-ecommerce', this.getNonEcommerce.bind(this));
    this.api.post('/non-ecommerce', this.postNonEcommerce.bind(this));
    // this.api.put('/admin-terms/:id', this.editTerm.bind(this));
  }

  /**
   * GET /non-ecommerce
   * get non ecommerce email
   * @param {*} req 
   * @param {*} res 
   */
  getNonEcommerce(req, res) {
    loggingHelper.log('info', 'getNonEcommerce');
    this.db.NonEcommerce.findAll()
      .then(email => ResponseHelper.responseSuccess(res, email))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * POST /non-ecommerce
   * add non ecommerce email
   * @param {*} req 
   * @param {*} res 
   */
  postNonEcommerce(req, res) {
    loggingHelper.log('info', `postNonEcommerce === ${req.body}`);
    let params = req.body;
    let countryCode = req.headers['country-code'];
    // let sendEmailQueueService = SendEmailQueueService.getInstance();
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }

    if(!params.email) {
      return ResponseHelper.responseMissingParam(res);
    }

    // validate format email and password
    if(!Validator.validateEmail(params.email)) {
      return ResponseHelper.responseError(res, 'email is incorrect!');
    }

    params.CountryId = req.country.id;

    this.db.NonEcommerce.findOne({where: {email: params.email}})
      .then(email => {
        if(email) {
          throw new Error('email_existed');
        } else {
          return this.db.NonEcommerce.create(params)
          .then(nonEcommerce => this.db.NonEcommerce.findOne({ where: {
            id: nonEcommerce.id
          }, include: ['Country']
          }))
          .then(data => this.mailchimp.addMemberToNewCustomerEmailForKoreaList(data));
        }
      })
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(error => {
        if(error.message === 'email_existed') {
          return ResponseHelper.responseError(res, 'This email is already used');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  // /**
  //  * PUT /terms/:id
  //  * allow admin update term
  //  * @param {*} req 
  //  * @param {*} res 
  //  */
  // editTerm(req, res) {
  //   loggingHelper.log('info', `postTerm === ${req.body}`);
  //   let params = req.body;
  //   this.db.Term.update(params, {where: {id: req.params.id}})
  //     .then(() => this.db.Term.update(params))
  //     .then(() => ResponseHelper.responseSuccess(res))
  //     .catch(error => ResponseHelper.responseInternalServerError(res, error));
  // }
}

export default function(...args) {
  return new NonEcommerceApi(...args);
}
