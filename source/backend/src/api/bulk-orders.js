import Promise from 'bluebird';
import fs from 'fs';
import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import SequelizeHelper from '../helpers/sequelize-helper';
import WareHouseHelper from '../helpers/warehouse-helper';
import SendEmailQueueService from '../services/send-email.queue.service';
import CompressedFileToDownloadQueueService from '../services/create-compressed-file-to-download.queue.service';
import BulkOrderInboundQueueService from '../services/create-bulk-order-inbound.service';
import SaleReportQueueService from '../services/sale-report.queue.service';
import TaxInvoiceBulkOrderQueueService from '../services/invoice-number_bulk-order.service';
import Validator from '../helpers/validator';
import OrderHelper from '../helpers/order-helper';
// import { awsHelper } from '../helpers/aws-helper';
import _ from 'underscore';
import json2csv from 'json2csv';
import moment from 'moment';
import multipart from 'connect-multiparty';
import { loggingHelper } from '../helpers/logging-helper';
import UrbanfoxHelper from '../helpers/urbanfox-helpers';

import config from '../config';

let multipartMiddleware = multipart();
const BULK_ORDER_FORMAT = ['email', 'vendor', 'countryCode', 'sku', 'qty', 'price', 'dFirstName', 'dLastName', 'dAddress', 'dCity', 'dPortalCode', 'dContactNumber', 'bFirstName', 'bLastName', 'bAddress', 'bCity', 'bPortalCode', 'bContactNumber', 'taxInvoice'];

class BulkOrderApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/bulk-orders', JwtMiddleware.verifyAdminToken, multipartMiddleware, this.postBulkOrder.bind(this));
    this.api.get('/bulk-orders', JwtMiddleware.verifyAdminToken, this.getBulkOrders.bind(this));
    this.api.get('/bulk-orders/download', JwtMiddleware.verifyAdminToken, this.downloadBulkOrder.bind(this));
    this.api.get('/bulk-orders/download/tax-invoice', JwtMiddleware.verifyAdminToken, this.downloadBulkOrderTaxInvoice.bind(this));
    this.api.put('/bulk-orders/:id', JwtMiddleware.verifyAdminToken, this.updateBulkOrder.bind(this));
    this.api.get('/bulk-orders/:id', JwtMiddleware.verifyAdminToken, this.getBulkOrderDetailsAdmin.bind(this));
    this.api.delete('/bulk-orders/:id', JwtMiddleware.verifyAdminToken, this.cancelBulkOrder.bind(this));
    this.api.post('/bulk-orders/:id/remarks', JwtMiddleware.verifyAdminToken, this.postBulkOrderRemarks.bind(this));
    this.api.put('/bulk-orders/:id/remarks/:historyId', JwtMiddleware.verifyAdminToken, this.editBulkOrderRemarks.bind(this));
    this.api.delete('/bulk-orders/:id/remarks/:historyId', JwtMiddleware.verifyAdminToken, this.deleteBulkOrderRemarks.bind(this));
  }

  /**
   * get all associate fields of order
   */
  _getBulkOrderInclude(long) {
    let includes = [
      'Country',
      'bulkOrderHistories',
      {
        model: this.db.BulkOrderDetail,
        as: 'bulkOrderDetail',
        include: [
          {
            model: this.db.ProductCountry,
            include: [
              'Country',
              {
                model: this.db.Product,
                include: ['productTranslate', 'productImages']
              }
            ]
          },
          {
            model: this.db.PlanCountry,
            include: [
              'Country',
              {
                model: this.db.Plan,
                include: ['PlanType', 'planTranslate', 'planImages']
              }, {
                model: this.db.PlanDetail,
                as: 'planDetails',
                include: [{
                  model: this.db.ProductCountry,
                  include: [{
                    model: this.db.Product,
                    include: ['productImages', 'productTranslate']
                  }]
                }]
              }
            ]
          }
        ]
      }
    ];
    if(long) {
      includes.push('DeliveryAddress');
      includes.push('BillingAddress');
    }
    return includes;
  }

  /** GET /bulk-orders/:id
   * get bulk order detail
   */
  getBulkOrderDetailsAdmin(req, res) {
    loggingHelper.log('info', 'getBulkOrderDetailsAdmin start');
    let options = {
      include: this._getBulkOrderInclude(true)};

    this.db.BulkOrder.findById(req.params.id, options)
      .then(order => res.json(order))
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /orders/bulk-orders
   * @param {*} req 
   * @param {*} res 
   */
  getBulkOrders(req, res) {
    loggingHelper.log('info', 'getBulkOrders start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'to date is invalid');
    }

    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where, {'email': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    // filter by states
    if(params.states && params.states !== '' && params.status !== 'all') {
      options.where = Object.assign(options.where, {states: {$like: `${params.states}`}});
      Reflect.deleteProperty(params, 'states');
    }

    // search function
    if(params.s && params.s !== '') {
      options.where = Object.assign(options.where, {
        $or: [
          {id: {$like: `%${params.s}%`}},
          {email: {$like: `%${params.s}%`}},
          {taxInvoiceNo: {$like: `%${params.s}%`}}
        ]
      });

      Reflect.deleteProperty(params, 's');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.BulkOrder);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    let data;
    this.db.BulkOrder.findAndCountAll(options)
      .then(result => {
        console.log(`result === ${JSON.stringify(result)}`);
        loggingHelper.log('info', `BulkOrder result === ${JSON.stringify(result)}`);
        data = SequelizeHelper.convertSeque2Json(result);
        return Promise.mapSeries(data.rows, order => this.db.BulkOrder.findOne({where: {id: order.id}, include: this._getBulkOrderInclude(true)}));
      })
      .then(orders => {
        data.rows = orders;
        return res.json(data);
      })
      .catch(error => ResponseHelper.responseInternalServerError(res, error));
  }

  /**
   * GET /orders/download
   * download order sale
   * @param {*} req 
   * @param {*} res 
   */
  downloadBulkOrder(req, res) {
    loggingHelper.log('info', 'downloadBulkOrder start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    
    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where, {'email': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.BulkOrder);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.type === 'customer') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }

    this.db.BulkOrder.findAll(options)
      .then(result => Promise.mapSeries(result, order => this.db.BulkOrder.findOne({where: {id: order.id}, include: this._getBulkOrderInclude()})))
      .then(orders => {
        orders = SequelizeHelper.convertSeque2Json(orders);
        // build order data
        let orderTmp = [];
        let tmp = {};
        orders.forEach(order => {
          order.bulkOrderDetail.forEach(bulkOrderDetail => {
            let agentName = (order.SellerUser) ? order.SellerUser.agentName : '';
            let productName = (bulkOrderDetail.ProductCountry) ? _.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name : '';
            let planName = (bulkOrderDetail.PlanCountry) ? _.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name : '';
            productName = productName !== '' ? productName : planName;
            let region = (bulkOrderDetail.PlanCountry) ? bulkOrderDetail.PlanCountry.Country.code : bulkOrderDetail.ProductCountry.Country.code;
            let type = (bulkOrderDetail.PlanCountry) ? 'subscription' : 'ala-carte';
            let category = (order.SellerUser) ? 'sales app' : 'client';
            let sku = (bulkOrderDetail.PlanCountry) ? `'${bulkOrderDetail.PlanCountry.Plan.sku}'` : `'${bulkOrderDetail.ProductCountry.Product.sku}'`;
            tmp = {
              orderId: OrderHelper.fromatBulkOrderNumber(order),
              agentName,
              email: order.email,
              productName,
              sku,
              region,
              category,
              type,
              receiptId: order.Receipt ? order.Receipt.id : '',
              productUnits: +bulkOrderDetail.qty,
              productPrice: +bulkOrderDetail.price,
              totalPrice: +bulkOrderDetail.price * +bulkOrderDetail.qty,
              currency: bulkOrderDetail.currency,
              createdAt: order.createdAt,
              saleDate: moment(order.createdAt).format('YYYY-MM-DD'),
              paymentType: order.paymentType.toUpperCase(),
              states: order.states
            };
            orderTmp.push(tmp);
          });
        });
        console.log(`orderTmp ===== ${JSON.stringify(orderTmp)}`);
        // create CSV and response file
        let fields = ['orderId', 'sku', 'agentName', 'email', 'productName', 'productUnits', 'productPrice', 'totalPrice', 'currency', 'paymentType', 'region', 'category', 'states', 'saleDate'];
        let fieldNames = ['Order', 'Sku', 'AgentName', 'Email', 'ProductName', 'Product Units', 'Product Price', 'Total Price', 'Currency', 'Payment Type', 'Region', 'Category', 'State', 'Sale Date'];
        let data = json2csv({
          data: orderTmp,
          fields,
          fieldNames
        });
        // send CSV file
        res.attachment(`Sales report for ${moment(params.fromDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')} to ${moment(params.toDate, 'YYYY-MM-DD').format('DD-MMM-YYYY')}.csv`);
        res.status(200).send(data);
      })
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * GET /bulk-orders/download/tax-invoice
   * download tax invoice of bulk order
   * @param {*} req 
   * @param {*} res 
   */
  downloadBulkOrderTaxInvoice(req, res) {
    loggingHelper.log('info', 'downloadBulkOrderTaxInvoice start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let params = req.query;
    let options = {where: {}};
    let decoded = req.decoded;
    // let role = decoded.adminRole;
    let compressedFileToDownloadQueueService = CompressedFileToDownloadQueueService.getInstance();

    // check date format
    if(!Validator.valiadateDate(params.fromDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    if(!Validator.valiadateDate(params.toDate)) {
      return ResponseHelper.responseError(res, 'from date is invalid');
    }
    
    if(params.email && params.email !== '') {
      options.where = Object.assign(options.where, {'email': {$like: `%${params.email}%`}});
      Reflect.deleteProperty(params, 'email');
    }

    if(params.id && params.id !== '') {
      options.where = Object.assign(options.where, {id: {$like: `%${params.id}%`}});
      Reflect.deleteProperty(params, 'id');
    }

    if(params.limit) {
      Reflect.deleteProperty(params, 'limit');
    }

    if(params.page) {
      Reflect.deleteProperty(params, 'page');
    }

    // build limit and off set
    options = RequestHelper.buildSequelizeOptions(options, params);
    options = RequestHelper.buildWhereCondition(options, params, this.db.BulkOrder);

    if(!decoded.viewAllCountry) {
      options.where = Object.assign(options.where, {$and: [{ CountryId: req.decoded.CountryId }]});
    }

    // build where string
    if(params.fromDate && params.toDate) {
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}},
        {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}}
      ]});
    } else if(params.fromDate) {
      options.where = Object.assign(options.where, {createdAt: {$gt: RequestHelper.buildClientTimezone(params.fromDate, 0, req)}});
    } else if(params.toDate) {
      options.where = Object.assign(options.where, {createdAt: {$lt: RequestHelper.buildClientTimezone(params.toDate, 1, req)}});
    }

    if(params.type === 'seller') {
      options.where = Object.assign(options.where, {SellerUserId: {$not: null}});
    } else if(params.type === 'customer') {
      options.where = Object.assign(options.where, {SellerUserId: null});
    }
          
    let fileExportName = 'Tax Invoice for Bulk Order';
    if(params.fromDate) {
      fileExportName += ` from ${params.fromDate}`;
    }
    if(params.toDate) {
      fileExportName += ` to ${params.toDate}`;
    }
    fileExportName += `(${moment().format('YYYY-MM-DD_HHmmssSSS')}).tar.gz`;
    console.log('fileExportName === === === ', fileExportName);
    return this.db.FileUpload.create(
      {
        name: fileExportName,
        // url: uploadedFile.Location,
        // bulkOrderId: order.id,
        isDownloaded: true,
        states: 'Processing',
        AdminId: decoded.id

      },
      {returning: true}
    )
    .then(result => {
      result = SequelizeHelper.convertSeque2Json(result);
      let queueOptions = {
        options,
        fileUploadId: result.id
      };
      return compressedFileToDownloadQueueService.addTask({method: 'downloadTaxInvoiceBulkOrder', queueOptions});
    })
    .then(() => res.json({ok: true}))
    .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * PUT /orders/:id
   * update order
   */
  updateBulkOrder(req, res) {
    loggingHelper.log('info', 'updateBulkOrder start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let bulkOrderInboundQueueService = BulkOrderInboundQueueService.getInstance();
    let params = req.body;
    let oldOrder;
    this.db.BulkOrder.findById(req.params.id, {raw: true})
      .then(order => {
        oldOrder = SequelizeHelper.convertSeque2Json(order);
        if(order && params.states !== 'Processing') {
          params = Object.assign(order, params);
          console.log(`params == ${JSON.stringify(params)}`);
          loggingHelper.log('info', `updateBulkOrder params == ${JSON.stringify(params)}`);
          return this.db.BulkOrder.update(params, {where: {
            id: req.params.id
          }, returning: true});
        } else if(params.states === 'Processing' && order && order.states === 'Payment Received') {
          if(order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
            return bulkOrderInboundQueueService.addTask(order.id, order.CountryId);
          }
          // return WareHouseHelper.createBulkOrderInbound(order.id, this.db);
        } else if(!order) {
          throw new Error('not_found');
        }
      })
      .then(() => {
        if(params.states && params.states !== '' && oldOrder && oldOrder.states !== params.states) {
          return this.db.BulkOrderHistory.create({
            BulkOrderId: oldOrder.id,
            message: params.states
          });
        }
      })
      .then(() => this.db.BulkOrder.findById(req.params.id, {include: this._getBulkOrderInclude()}))
      .then(order => res.json(order))
      .catch(error => {
        if(error.name === 'SequelizeValidationError') {
          return ResponseHelper.responseErrorParam(res, error);
        } else if(error.name === 'SequelizeUniqueConstraintError') {
          return ResponseHelper.responseError(res, 'email is existed');
        } else if(error.message === 'not_found') {
          return ResponseHelper.responseNotFoundError(res, 'Admin');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  cancelBulkOrder(req, res) {
    loggingHelper.log('info', 'cancelBulkOrder start');
    loggingHelper.log('info', `param: req.query = ${req.query}`);
    let userTypeId = (req.query.userTypeId === 1 || req.query.userTypeId === '1') ? req.query.userTypeId : 2;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    // find order
    this.db.BulkOrder.findOne({where: {id: req.params.id}, include: ['Country']})
      .then(order => {
        // check order status
        if(!order) {
          throw new Error('order_not_existed');
        } 
        // if(moment(order.createdAt).add(1, 'day').isBefore(moment())) {
        //   throw new Error('can_not_cancel1');
        // } 
        if(order.status === 'Delivering' || (userTypeId === 1 && order.status === 'Completed')) {
          throw new Error('can_not_cancel2');
        } else {
          return this.db.BulkOrder.update({states: 'Canceled'}, {where: {id: req.params.id}})
            .then(() => this.db.BulkOrderHistory.create({
              BulkOrderId: req.params.id,
              message: 'Canceled'
            }))
            .then(() => {
              if(!order.carrierAgent || (order.carrierAgent && order.carrierAgent !== 'courex')) {
                return sendEmailQueueService.addTask({method: 'sendCancelBulkOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()});
              } else {
                return UrbanfoxHelper.cancelOrder(order, true);
              }
            });
        }
      })
      .then(() => res.json({ok: true})) // return result to client
      .catch(error => {
        if(error.message === 'order_not_existed') {
          return ResponseHelper.responseError(res, 'Order not existed');
        } else if(error.message === 'can_not_cancel1') {
          return ResponseHelper.responseError(res, 'Can not cancel this order because it\'s over 24 hours');
        } else if(error.message === 'can_not_cancel2') {
          return ResponseHelper.responseError(res, 'Can not cancel this order because it\'s delivering');
        } else {
          return ResponseHelper.responseInternalServerError(res, error);
        }
      });
  }

  /**
   * POST /api/orders/:id/remarks
   * Add remark for order
   * @param {*} req 
   * @param {*} res 
   */
  postBulkOrderRemarks(req, res) {
    loggingHelper.log('info', 'postBulkOrderRemarks start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validate created at
    if(params.createdAt && !Validator.valiadateDate(params.createdAt)) {
      return ResponseHelper.responseError('created at incorrecft format');
    }

    if(params.updatedAt && !Validator.valiadateDate(params.updatedAt)) {
      return ResponseHelper.responseError('updated at incorrecft format');
    }

    params.BulkOrderId = req.params.id;
    params.isRemark = true;
    this.db.BulkOrderHistory.create(params, {returning: true})
      .then(orderHistory => ResponseHelper.responseSuccess(res, orderHistory))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * PUT /api/orders/:id/remarks/:historyId
   * Update remark
   * @param {*} req 
   * @param {*} res 
   */
  editBulkOrderRemarks(req, res) {
    loggingHelper.log('info', 'editBulkOrderRemarks start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;

    // validate created at
    if(params.createdAt && !Validator.valiadateDate(params.createdAt)) {
      return ResponseHelper.responseError('created at incorrect format');
    }

    if(params.updatedAt && !Validator.valiadateDate(params.updatedAt)) {
      return ResponseHelper.responseError('updated at incorrecft format');
    }

    this.db.BulkOrderHistory.update(params, {where: {id: req.params.historyId}})
      .then(() => ResponseHelper.responseSuccess(res))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  /**
   * DELETE /api/orders/:id/remarks/:historyId
   * Delete
   * @param {*} req 
   * @param {*} res 
   */
  deleteBulkOrderRemarks(req, res) {
    loggingHelper.log('info', 'deleteBulkOrderRemarks start');
    this.db.BulkOrderHistory.destroy({where: {id: req.params.historyId}})
      .then(orderHistory => ResponseHelper.responseSuccess(res, orderHistory))
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

  readFileAsync(file) {
    loggingHelper.log('info', 'readFileAsync start');
    loggingHelper.log('info', `param: file = ${file}`);
    let orders = [];
    return new Promise((resolve, reject) => {
      // read csv file
      fs.readFile(file.path, (err, fileContent) => {
        if(err) {
          reject(new Error('can_not_read_file'));
        } else {
          let dataArray = fileContent.toString().replace(/\r/g, '').split(/\r?\n/);
          dataArray.forEach((v, i) => {
            if(i !== 0 && v !== '') {
              let values = v.replace(/”/g, '"').replace(/“/g, '"').split(/\,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);
              console.log(`readFileAsync ${JSON.stringify(values)} === ${values.length}`);
              loggingHelper.log('info', `readFileAsync ${JSON.stringify(values)} === ${values.length}`);
              // validate info
              if(values.length !== 19) {
                reject(new Error('data_wrong'));
              } else {
                let order = {};
                values.forEach((value, idx) => {
                  order[BULK_ORDER_FORMAT[idx]] = value.replace(/"/g, '');
                });
                orders.push(order);
              }
            }          
          });
          resolve(orders);
        }
      });
    });
  }

  /**
   * POST bulk order create
   * create order from uploaded file
   * @param {*} req 
   * @param {*} res 
   */
  postBulkOrder(req, res) {
    loggingHelper.log('info', 'postBulkOrder start');
    loggingHelper.log('info', `param: req.files = ${req.files}`);
    let file = req.files.file;
    let bulkOrderInboundQueueService = BulkOrderInboundQueueService.getInstance();
    let saleReportQueueService = SaleReportQueueService.getInstance();
    let taxInvoiceBulkOrderQueueService = TaxInvoiceBulkOrderQueueService.getInstance();
    let sendEmailQueueService = SendEmailQueueService.getInstance();

    // check file format
    console.log(`file === ${JSON.stringify(file)}`);
    loggingHelper.log('info', `postBulkOrder file === ${JSON.stringify(file)}`);
    if(!file) {
      return ResponseHelper.responseError(res, 'Missing file');
    } else if(file.name.indexOf('.csv') === -1) {
      return ResponseHelper.responseError(res, 'File format is not support');
    }
    this.readFileAsync(file)
      .then(orders => {
        orders.forEach((order, index) => {
          if(!order.email || order.email === '' ||
          !order.vendor || order.vendor === '' ||
          !order.countryCode || order.countryCode === '' ||
          !order.sku || order.sku === '' ||
          !order.qty || order.qty === '' ||
          !order.dFirstName || order.dFirstName === '' ||
          !order.dLastName || order.dLastName === '' ||
          !order.dAddress || order.dAddress === '' ||
          !order.dCity || order.dCity === '' ||
          !order.dPortalCode || order.dPortalCode === '' ||
          !order.dContactNumber || order.dContactNumber === '' ||
          !order.bFirstName || order.bFirstName === '' ||
          !order.bLastName || order.bLastName === '' ||
          !order.bAddress || order.bAddress === '' ||
          !order.bCity || order.bCity === '' ||
          !order.bPortalCode || order.bPortalCode === '' ||
          !order.bContactNumber || order.bContactNumber === '') {
            throw new Error(`invalid_row ${index + 2}`);
          }
        });
  
        orders.sort((a, b) => a.email.localeCompare(b.email));
        // sort order by address
        orders.sort((a, b) => a.dFirstName.localeCompare(b.dFirstName));
        orders.sort((a, b) => a.dLastName.localeCompare(b.dLastName));
        orders.sort((a, b) => a.dAddress.localeCompare(b.dAddress));
        orders.sort((a, b) => a.dCity.localeCompare(b.dCity));
        orders.sort((a, b) => a.dPortalCode.localeCompare(b.dPortalCode));
        orders.sort((a, b) => a.dContactNumber.localeCompare(b.dContactNumber));
        // sort order by billing
        orders.sort((a, b) => a.bFirstName.localeCompare(b.bFirstName));
        orders.sort((a, b) => a.bLastName.localeCompare(b.bLastName));
        orders.sort((a, b) => a.bAddress.localeCompare(b.bAddress));
        orders.sort((a, b) => a.bCity.localeCompare(b.bCity));
        orders.sort((a, b) => a.bPortalCode.localeCompare(b.bPortalCode));
        orders.sort((a, b) => a.bContactNumber.localeCompare(b.bContactNumber));

        console.log(`order === ${JSON.stringify(orders)}`);
        
        // groupd order
        let orderGroup = [];
        let currentAddress = {};
        let addressList = [];
        let countryCodes = [];
        let productSkus = [];
        let currentOrder = {bulkOrderDetail: []};
        orders.forEach(order => {
          // add to address list
          addressList = addressList.concat([{
            firstName: order.dFirstName,
            lastName: order.dLastName,
            address: order.dAddress,
            city: order.dCity,
            portalCode: order.dPortalCode,
            contactNumber: order.dContactNumber,
            countryCode: order.countryCode
          }, {
            firstName: order.bFirstName,
            lastName: order.bLastName,
            address: order.bAddress,
            city: order.bCity,
            portalCode: order.bPortalCode,
            contactNumber: order.bContactNumber,
            countryCode: order.countryCode
          }]);
          
          // add to product sku list
          if(productSkus.indexOf(order.sku) === -1) {
            productSkus.push(order.sku);
          }

          if(countryCodes.indexOf(order.countryCode) === -1) {
            countryCodes.push(order.countryCode);
          }
          // re-group the order
          if(currentAddress.email !== order.email || currentAddress.vendor !== order.vendor || currentAddress.countryCode !== order.countryCode ||
            currentAddress.dFirstName !== order.dFirstName || currentAddress.dLastName !== order.dLastName || currentAddress.dAddress !== order.dAddress ||
            currentAddress.dCity !== order.dCity || currentAddress.dPortalCode !== order.dPortalCode || currentAddress.dContactNumber !== order.dContactNumber ||
            currentAddress.bFirstName !== order.bFirstName || currentAddress.bLastName !== order.bLastName || currentAddress.bAddress !== order.bAddress ||
            currentAddress.bCity !== order.bCity || currentAddress.bPortalCode !== order.bPortalCode || currentAddress.bContactNumber !== order.bContactNumber) {
            currentOrder = {bulkOrderDetail: []};
            currentAddress = {
              email: order.email,
              vendor: order.vendor,
              countryCode: order.countryCode,
              dFirstName: order.dFirstName,
              dLastName: order.dLastName,
              dAddress: order.dAddress,
              dCity: order.dCity,
              dPortalCode: order.dPortalCode,
              dContactNumber: order.dContactNumber,
              bFirstName: order.bFirstName,
              bLastName: order.bLastName,
              bAddress: order.bAddress,
              bCity: order.bCity,
              bPortalCode: order.bPortalCode,
              bContactNumber: order.bContactNumber
            };
            currentOrder.email = order.email;
            currentOrder.vendor = order.vendor;
            currentOrder.countryCode = order.countryCode;
            currentOrder.taxInvoice = order.taxInvoice;
            currentOrder.bulkOrderDetail.push({
              qty: order.qty,
              sku: order.sku,
              price: order.price
            });
            currentOrder.address = currentAddress;
            orderGroup.push(Object.assign({}, currentOrder));
            console.log(`create order ==2222 = ${JSON.stringify(currentAddress)} === ${JSON.stringify(order)}`);
          } else {
            currentOrder.bulkOrderDetail.push({
              qty: order.qty,
              sku: order.sku,
              price: order.price
            });
          }
        });

        console.log(`create order ==11111 = ${JSON.stringify(orderGroup)}`);

        // create address
        let uniqAddressList = [];
        currentAddress = {};
        addressList.sort((a, b) => a.firstName.localeCompare(b.firstName));
        addressList.sort((a, b) => a.lastName.localeCompare(b.lastName));
        addressList.sort((a, b) => a.address.localeCompare(b.address));
        addressList.sort((a, b) => a.city.localeCompare(b.city));
        addressList.sort((a, b) => a.portalCode.localeCompare(b.portalCode));
        addressList.sort((a, b) => a.contactNumber.localeCompare(b.contactNumber));
        addressList.forEach(address => {
          if(currentAddress.firstName !== address.firstName || currentAddress.lastName !== address.lastName || currentAddress.address !== address.address ||
            currentAddress.city !== address.city || currentAddress.portalCode !== address.portalCode || currentAddress.contactNumber !== address.contactNumber) {
            uniqAddressList.push(address);
            currentAddress = address;
          }
        });

        // create address
        let data = {};
        return this.db.Country.findAll({where: {code: {$in: countryCodes}}})
          .then(result => {
            data.countries = SequelizeHelper.convertSeque2Json(result);
            return Promise.mapSeries(uniqAddressList, address => {
              // get Country
              let country = _.findWhere(data.countries, {code: address.countryCode});
              console.log(`address === ${JSON.stringify(uniqAddressList)} == ${JSON.stringify(data.countries)} == ${JSON.stringify(address)}`);
              if(country) {
                return this.db.DeliveryAddress.findOne({where: {
                  firstName: address.firstName,
                  lastName: address.lastName,
                  address: address.address,
                  city: address.city,
                  portalCode: address.portalCode,
                  contactNumber: address.contactNumber,
                  isBulkAddress: true,
                  CountryId: country.id
                }})
                  .then(_address => {
                    if(_address) {
                      return _address;
                    } else {
                      address.isBulkAddress = true;
                      address.CountryId = country.id;
                      return this.db.DeliveryAddress.create(address, {returning: true});
                    }
                  });
              }
            });
          })
          .then(result => {
            console.log(`address === ${JSON.stringify(result)}`);
            loggingHelper.log('info', `address === ${JSON.stringify(result)}`);
            data.addresses = SequelizeHelper.convertSeque2Json(result);
            // get country list
            return this.db.PlanCountry.findAll({where: {'$Plan.sku$': {$in: productSkus}}, include: ['Plan', 'Country']});
          })
          .then(planCountries => {
            data.planCountries = planCountries;
            // get country list
            return this.db.ProductCountry.findAll({where: {'$Product.sku$': {$in: productSkus}}, include: ['Product', 'Country']});
          })
          .then(productCountries => {
            // build order
            data.productCountries = SequelizeHelper.convertSeque2Json(productCountries);
            let createOrders = [];
            orderGroup.forEach(order => {
              // check delivery address
              let deliverAddress = _.find(data.addresses, obj => (obj.firstName.toUpperCase() === order.address.dFirstName.toUpperCase() && obj.lastName.toUpperCase() === order.address.dLastName.toUpperCase() && obj.address.toUpperCase() === order.address.dAddress.toUpperCase() &&
                obj.city.toUpperCase() === order.address.dCity.toUpperCase() && obj.portalCode.toUpperCase() === order.address.dPortalCode.toUpperCase() && obj.contactNumber.toUpperCase() === order.address.dContactNumber.toUpperCase()));
              let billingAddress = _.find(data.addresses, obj => (obj.firstName.toUpperCase() === order.address.bFirstName.toUpperCase() && obj.lastName.toUpperCase() === order.address.bLastName.toUpperCase() && obj.address.toUpperCase() === order.address.bAddress.toUpperCase() &&
                obj.city.toUpperCase() === order.address.bCity.toUpperCase() && obj.portalCode.toUpperCase() === order.address.bPortalCode.toUpperCase() && obj.contactNumber.toUpperCase() === order.address.bContactNumber.toUpperCase()));

              console.log(`deliverAddress === ${JSON.stringify(Object.assign({}, order.address))}`);
              console.log(`deliverAddress === ${JSON.stringify(Object.assign({}, deliverAddress))}`);
  
              // get product Country
              let bulkOrderDetail = [];
              order.bulkOrderDetail.forEach(details => {
                let productCountry = _.find(data.productCountries, obj => (obj.Product.sku === details.sku && obj.Country.code === order.countryCode));
                // let planCountry = _.find(data.planCountries, obj => (obj.Plan.sku === details.sku && obj.Country.code === order.countryCode));
                if(productCountry) {
                  bulkOrderDetail.push({
                    qty: details.qty,
                    price: details.price,
                    currency: productCountry.Country.currencyCode,
                    ProductCountryId: productCountry.id 
                  });
                } else {
                  throw new Error(`product_not_found ${details.sku}`);
                }
                // if(planCountry) {
                //   bulkOrderDetail.push({
                //     qty: details.qty,
                //     price: details.price,
                //     currency: planCountry.Country.currencyCode,
                //     PlanCountryId: planCountry.id 
                //   });
                // }
              });
              order.bulkOrderDetail = bulkOrderDetail;

              // get Country
              let country = _.findWhere(data.countries, {code: order.countryCode});

              // validate portal code
              console.log(`country ${country.code} ==== ${deliverAddress.portalCode}`);
              if(country.code === 'SGP' && !/^\d{6}$/g.test(deliverAddress.portalCode)) {
                throw new Error('portalcode_invalid');
              }

              if(deliverAddress && billingAddress && country) {
                createOrders.push({
                  states: 'Payment Received',
                  paymentType: 'cash',
                  email: order.email,
                  vendor: order.vendor,
                  isBulkCreated: true,
                  DeliveryAddressId: deliverAddress.id,
                  BillingAddressId: billingAddress.id,
                  CountryId: country.id,
                  bulkOrderDetail: order.bulkOrderDetail,
                  bulkOrderHistories: [{
                    message: 'Payment Received'
                  }],
                  taxInvoice: order.taxInvoice
                });
              } else {
                throw new Error('invalid_param');
              }
            });
            console.log(`create order === ${JSON.stringify(createOrders)}`);
            loggingHelper.log('info', `create order === ${JSON.stringify(createOrders)}`);
            // create order
            let result;
            return Promise.mapSeries(createOrders, order => this.db.BulkOrder.create(order, {include: ['bulkOrderDetail', 'bulkOrderHistories'], returning: true})
              .then(_result => {
                result = _result;
                if(result.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
                  bulkOrderInboundQueueService.addTask(result.id, result.CountryId);
                }
              })
              .then(() => saleReportQueueService.addTask({
                id: result.id,
                bulkOrder: true
              }))
              .then(() => {
                if(result.CountryId === 1 && order.taxInvoice.toUpperCase() === 'Y') {
                  return taxInvoiceBulkOrderQueueService.addTask(result);
                } else {
                  return;
                }
              })
              .then(() => {
                if(result.CountryId === 1 && order.taxInvoice.toUpperCase() === 'Y') {
                  sendEmailQueueService.addTask({method: 'sendReceiptBulkOrdersEmail', data: {orderId: result.id, langCode: 'en'}});
                }
              })
            );
          })
          .then(result => ResponseHelper.responseSuccess(res, result));
      })
      .catch(err => {
        if(err.message === 'invalid_param') {
          return ResponseHelper.responseError(res, 'Something incorrect in the data.');
        } else if(err.message === 'can_not_read_file') {
          return ResponseHelper.responseError(res, 'Can not read file');
        } else if(err.message === 'data_wrong') {
          return ResponseHelper.responseError(res, 'Data content is not correct, require 19 columns per rows');
        } else if(err.message.indexOf('invalid_row') > -1) {
          return ResponseHelper.responseError(res, `Data content is not correct, row ${err.message.split(' ')[1]} is invalid.`);
        } else if(err.message === 'portalcode_invalid') {
          return ResponseHelper.responseError(res, 'Please check delivery postal code of SG again. One or more postal codes are invalid, it should be 6 digits.');
        } else if(err.message.indexOf('product_not_found') > -1) {
          return ResponseHelper.responseError(res, `Product SKU(${err.message.replace('product_not_found ', '')}) is not found.`);
        } else {
          return ResponseHelper.responseInternalServerError(res, err);
        }
      });
  }

}

export default function(...args) {
  return new BulkOrderApi(...args);
}
