import Promise from 'bluebird';
import _ from 'underscore';
import RequestHelper from '../helpers/request-helper';
import ResponseHelper from '../helpers/response-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import JwtMiddleware from '../middleware/jwt-middleware';
import config from '../config';
import { loggingHelper } from '../helpers/logging-helper';

class UserOrderListApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.get('/user-order-list', this.getLastOrders.bind(this));
  
  }

  getLastOrders(req, res) {   
    loggingHelper.log('info', 'getLastOrders start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    loggingHelper.log('info', `param: req.query = ${req.query}`);

    let params = req.query || {};
    let options={};

    options = {
      where: {UserId: params.userId},
      order: [ [ 'createdAt', 'DESC' ]],
    };

    this.db.Order.findOne(options)
      .then(getorder =>{
       // console.log(`Get Current User Order List === ${JSON.stringify(getorder)}`);
        return res.json(getorder)
        
      }
    )
      .catch(err => ResponseHelper.responseInternalServerError(res, err));
  }

}
export default function(...args) {
  return new UserOrderListApi(...args);
}
