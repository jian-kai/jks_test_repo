import _ from 'underscore';
import jwt from 'jsonwebtoken';

import ResponseHelper from '../helpers/response-helper';
import OrderController from '../controllers/orders.controller';
import TrialPlanController from '../controllers/trial-plan.controller';
import ASKController from '../controllers/awesome-shave-kits.controller';
import CustomPlanController from '../controllers/custom-plan.controller';
import OrderInboundQueueService from '../services/create-order-inbound.service';
import SendEmailQueueService from '../services/send-email.queue.service';
import JwtMiddleware from '../middleware/jwt-middleware';
import SequelizeHelper from '../helpers/sequelize-helper';
import SaleReportQueueService from '../services/sale-report.queue.service';
import { loggingHelper } from '../helpers/logging-helper';
import TaxInvoiceQueueService from '../services/invoice-number.service';
import config from '../config';
import moment from 'moment';

class PaymentApi {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.orderController = new OrderController(db);
    this.trialPlanController = new TrialPlanController(db);
	this.askController = new ASKController(db);
    this.customPlanController = new CustomPlanController(db);
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/payment/orders/stripe', this.placeOrderStripe.bind(this));
    this.api.post('/payment/orders/ipay88', this.placeOrderIpay88.bind(this));
    this.api.post('/payment/orders/cash', JwtMiddleware.verifyUserToken.bind(this), this.placeOrderCash.bind(this));
    this.api.post('/payment/orders/sample', JwtMiddleware.verifyUserToken.bind(this), this.placeOrderSample.bind(this));
    this.api.post('/payment/trial-plan/stripe', this.placeTrialPlan.bind(this));
    this.api.post('/payment/custom-plan/stripe', this.placeCustomPlan.bind(this));
	this.api.post('/payment/awesome-shave-kit/stripe', this.placeAskProduct.bind(this));
  }

  _handlerError(error, res) {
    if(error.message === 'missing_param') {
      return ResponseHelper.responseMissingParam(res, error);
    } else if(error.message === 'not_enough') {
      return ResponseHelper.responseError(res, 'Insufficient quantity');
    } else if(error.message === 'card_invalid') {
      return ResponseHelper.responseError(res, 'Card information is invalid. Please try again.');
    } else if(error.message === 'not_found') {
      return ResponseHelper.responseError(res, 'product or plan does not exited');
    } else if(error.message === 'promo_used_limited') {
      return ResponseHelper.responseError(res, config.messages.promotion.usedLimited);
    } else if(error.name === 'SequelizeValidationError') {
      return ResponseHelper.responseErrorParam(res, error);
    } else if(error.name === 'SequelizeForeignKeyConstraintError') {
      return ResponseHelper.responseErrorParam(res, error);
    } else if(error.type === 'StripeInvalidRequestError') {
      return ResponseHelper.responseErrorParam(res, error);
    } else if(error.type === 'StripeCardError') {
      return ResponseHelper.responseErrorParam(res, error);
    } else {
      return ResponseHelper.responseInternalServerError(res, error);
    }
  }

  /** POST /payment/orders/stripe
   * create order and charge 
   */
  placeOrderStripe(req, res) {
    loggingHelper.log('info', 'placeOrderStripe start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let saleReportQueueService = SaleReportQueueService.getInstance();
    console.log(`params === ${JSON.stringify(params)}`);
    if((params.subsItems && (!params.DeliveryAddressId || !params.BillingAddressId)) || !params.CardId) {
      return ResponseHelper.responseMissingParam(res);
    } else if(!params.alaItems && !params.subsItems) {
      return ResponseHelper.responseMissingParam(res);
    }

    // set payment type as stripe
    req.body.paymentType = 'stripe';

    let result;
    let orderInboundQueueService = OrderInboundQueueService.getInstance();

    this.orderController.processCheckout(req)
      .then(_result => {
        result = _result;
        if(result.order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
          return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => saleReportQueueService.addTask({
        id: result.order.id,
        source: params.utmSource,
        medium: params.utmMedium,
        campaign: params.utmCampaign,
        term: params.utmTerm,
        content: params.utmContent
      }))
      .then(() => res.json({ok: true, result}))
      .catch(error => this._handlerError(error, res));
  }
  
	
	    /**		
	   * POST /payment/awesome-shave-kit/stripe		
	   * ala carte awesome-shave-kit		
	   * @param {*} req 		
	   * @param {*} res 		
	   */		
	  placeAskProduct(req, res) {		
	    console.log(`start placeAskProduct`);		
	    let params = req.body;		
	    let sendEmailQueueService = SendEmailQueueService.getInstance();		
	    console.log(`params === ${JSON.stringify(params)}`);		
	    if(!params.productCountryId || !params.DeliveryAddressId || !params.BillingAddressId || !params.CardId) {		
	      return ResponseHelper.responseMissingParam(res);		
	    }		
			
     let result;		
	    let orderInboundQueueService = OrderInboundQueueService.getInstance();		
	    let saleReportQueueService = SaleReportQueueService.getInstance();		
			
	    this.askController.processCheckout(req)		
	    .then(_result => {		
	      result = _result;		
	      if(result.order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {		
	        return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);		
	      } else {		
	        return Promise.resolve();		
	      }		
	    })		
	    .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))		
	    .then(() => saleReportQueueService.addTask({		
	      id: result.order.id,		
	      source: params.utmSource,		
	      medium: params.utmMedium,		
	      campaign: params.utmCampaign,		
	      term: params.utmTerm,		
      content: params.utmContent		
	    }))		
	    .then(() => res.json({ok: true, result}))		
	    .catch(error => this._handlerError(error, res));		
	  }

  /**
   * POST /payment/trial-plan/stripe
   * subscribe for trial plan
   * @param {*} req 
   * @param {*} res 
   */
  placeTrialPlan(req, res) {
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    console.log(`params === ${JSON.stringify(params)}`);
    if(!params.planCountryId || !params.DeliveryAddressId || !params.BillingAddressId || !params.CardId) {
      return ResponseHelper.responseMissingParam(res);
    }

    let result;
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    let saleReportQueueService = SaleReportQueueService.getInstance();

    this.trialPlanController.processCheckout(req)
      .then(_result => {
        result = _result;
        if(result.order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
          return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsTrialEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => saleReportQueueService.addTask({
        id: result.order.id,
        source: params.utmSource,
        medium: params.utmMedium,
        campaign: params.utmCampaign,
        term: params.utmTerm,
        content: params.utmContent
      }))
      .then(() => res.json({ok: true, result}))
      .catch(error => this._handlerError(error, res));
  }

  /**
   * POST /payment/custom-plan/stripe
   * subscribe for custom plan
   * @param {*} req 
   * @param {*} res 
   */
  placeCustomPlan(req, res) {
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    console.log(`params === ${JSON.stringify(params)}`);
    if(!params.customPlanDetail || !params.DeliveryAddressId || !params.BillingAddressId || !params.CardId) {
      return ResponseHelper.responseMissingParam(res);
    }

    let result;
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    let saleReportQueueService = SaleReportQueueService.getInstance();

    this.customPlanController.processCheckout(req)
      .then(_result => {
        result = _result;
        if(result.order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
          return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => saleReportQueueService.addTask({
        id: result.order.id,
        source: params.utmSource,
        medium: params.utmMedium,
        campaign: params.utmCampaign,
        term: params.utmTerm,
        content: params.utmContent
      }))
      .then(() => res.json({ok: true, result}))
      .catch(error => this._handlerError(error, res));
  }

  /**
   * POST /payment/orders/ipay88
   * create order by iPay88
   * @param {*} req 
   * @param {*} res 
   */
  placeOrderIpay88(req, res) {
    loggingHelper.log('info', 'placeOrderIpay88 start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let saleReportQueueService = SaleReportQueueService.getInstance();
    let params = req.body;
    if(!params.DeliveryAddressId || !params.BillingAddressId) {
      return ResponseHelper.responseMissingParam(res);
    } else if(!params.alaItems) {
      return ResponseHelper.responseMissingParam(res);
    } else if(params.subsItems) {
      return ResponseHelper.responseErrorParam(res, 'Payment method is not supported');
    }

    // get product country IDs
    if(params.alaItems) {
      params.alaItems = (Array.isArray(params.alaItems)) ? params.alaItems : [params.alaItems];
      params.productCountryIds = _.pluck(params.alaItems, 'ProductCountryId');
    }

    params.subsItems = [];
    params.planCountryIds = [];
    params.paymentType = 'iPay88';

    let data = {
      params,
      country: req.country
    };
    this.db.sequelize.transaction(t => this.orderController.getIdentityForCheckOut(req, data)
    .then(identity => {
      data.identity = identity;
      return this.db.DeliveryAddress.findOne({where: {id: data.params.DeliveryAddressId}});
    })
    .then(deliveryAddress => {
      data.deliveryAddress = deliveryAddress;
      return this.db.User.findOne({where: {email: params.email}});
    })
    .then(user => {
      if(!user && ((params.userTypeId === 2) || (data.identity.guest))) {
        let _user;
        data.newCustomer = true;
        return this.db.User.create({
          email: params.email,
          firstName: data.deliveryAddress.firstName,
          lastName: data.deliveryAddress.lastName,
          CountryId: req.country.id,
          isGuest: true
        }, {returning: true, transaction: t})
          .then(_result => {
            _user = _result;
            return this.db.Cart.create({UserId: _result.id}, {transaction: t});
          })
          .then(() => _user);
      } else {
        return user;
      }
    })
    .then(user => {
      data.UserId = user.id;
      if(data.identity.guest) {
        return this.db.DeliveryAddress.update({UserId: user.id}, {where: {id: data.params.DeliveryAddressId}, transaction: t});
      }
    })
    .then(() => {
      if(data.identity.guest) {
        return this.db.DeliveryAddress.update({UserId: data.UserId}, {where: {id: data.params.DeliveryAddressId}, transaction: t});
      }
    })
    .then(() => {
      if(data.identity.guest) {
        return this.db.Card.update({UserId: data.UserId}, {where: {id: data.params.CardId}, transaction: t});
      }
    })
    .then(() => this.orderController.proccessPromotion(data))
    .then(promotion => {
      data.promotion = promotion;
      return this.db.ProductCountry.findAll({where: {
        id: {
          $in: data.params.productCountryIds
        }
      }, include: ['Country', {
        model: this.db.Product,
        include: ['ProductType']
      }]});
    })
    .then(productCountries => {
      data.productCountries = productCountries;
      let result;
      return this.orderController.makeOrder(data, t)
      .then(_result => {
        result = _result;
        data = Object.assign(data, result);
        data.receiptObject.OrderId = data.order.id;
        return this.orderController.createReceipt(data, t);
      })
      .then(() => saleReportQueueService.addTask({
        id: result.order.id,
        source: params.utmSource,
        medium: params.utmMedium,
        campaign: params.utmCampaign,
        term: params.utmTerm,
        content: params.utmContent
      }));
    }))
      .then(() => res.json({ok: true, result: data}))
      .catch(error => this._handlerError(error, res));
  }

  /**
   * POST /payment/orders/cash
   * create order wby cash
   * @param {*} req 
   * @param {*} res 
   */
  placeOrderCash(req, res) {
    loggingHelper.log('info', 'placeOrderCash start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let saleReportQueueService = SaleReportQueueService.getInstance();
    let taxInvoiceQueueService = TaxInvoiceQueueService.getInstance();
    console.log(`params === ${JSON.stringify(params)}`);
    if(params.subsItems && (!params.DeliveryAddressId || !params.BillingAddressId)) {
      return ResponseHelper.responseMissingParam(res);
    } else if(!params.alaItems && !params.subsItems) {
      return ResponseHelper.responseMissingParam(res);
    }

    // set payment type as stripe
    req.body.paymentType = 'cash';

    let result;
    let orderInboundQueueService = OrderInboundQueueService.getInstance();
    this.orderController.processCheckout(req)
      .then(_result => {
        result = _result;
        if(result.order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
          return orderInboundQueueService.addTask(result.order.id, result.order.CountryId);
        } else {
          return Promise.resolve();
        }
      })
      .then(() => taxInvoiceQueueService.addTask(result.order))
      .then(() => sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: result.order.id}, langCode: req.country.defaultLang.toLowerCase()}))
      .then(() => saleReportQueueService.addTask({
        id: result.order.id,
        source: params.utmSource,
        medium: params.utmMedium,
        campaign: params.utmCampaign,
        term: params.utmTerm,
        content: params.utmContent
      }))
      .then(() => res.json({ok: true, result}))
      .catch(error => this._handlerError(error, res));
  }

  /**
   * POST /payment/orders/sample
   * collect customer infor
   * @param {*} req 
   * @param {*} res 
   */
  placeOrderSample(req, res) {
    loggingHelper.log('info', 'placeOrderSample start');
    loggingHelper.log('info', `param: req.body = ${req.body}`);
    let params = req.body;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    let returnData;
    if(!params.email) {
      return ResponseHelper.responseMissingParam(res, 'missing email');
    }

    let token = req.body.accessToken || req.query.accessToken || req.headers['x-access-token'];
    let sellerId = '';
    sellerId = jwt.decode(token).id;

    this.db.SellerUser.findOne({where: {id: sellerId}})
    .then(seller => {
      // check email to create new account
      this.db.sequelize.transaction(t => this.db.User.findOne({where: {email: params.email}})
        .then(user => {
          if(!user) {
            let _user;
            return this.db.User.create({
              email: params.email,
              firstName: (params.fullname.indexOf(' ') > -1) ? params.fullname.substr(0, params.fullname.indexOf(' ')) : params.fullname,
              lastName: (params.fullname.indexOf(' ') > -1) ? params.fullname.substr(params.fullname.indexOf(' ') + 1) : '',
              CountryId: req.country.id,
              isGuest: true,
              badgeId: seller.badgeId,
              phone: params.phone ? params.phone : params.callingCode + params.contactNumber
            }, {returning: true, transaction: t})
              .then(result => {
                _user = result;
                return this.db.Cart.create({UserId: result.id}, {transaction: t});
              })
              .then(() => _user);
          } else {
            return user;
          }
        })
        .then(user => {
          console.log(`sdfsdf === ${JSON.stringify(user)}`);
          return this.db.User.findOne({where: {id: user.id}, include: ['Country'], transaction: t});
        })
        .then(user => {
          console.log(`sdfsdf === ${JSON.stringify(user)}`);
          returnData = SequelizeHelper.convertSeque2Json(user);
          returnData.setPassAndActive = true;
          return sendEmailQueueService.addTask({method: 'sendActiveUserSample', data: user, langCode: req.country.defaultLang.toLowerCase()});
        }))
        .then(() => res.json({
          ok: true,
          returnData
        }))
        .catch(error => {
          if(error.name === 'SequelizeValidationError') {
            return ResponseHelper.responseErrorParam(res, error);
          } else {
            return ResponseHelper.responseInternalServerError(res, error);
          }
        });
    });
  }
}

export default function(...args) {
  return new PaymentApi(...args);
}
