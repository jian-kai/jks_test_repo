import fs from 'fs';

function getFilesAsync() {
  return new Promise((resolve, reject) => {
    fs.readdir(`${__dirname}/`, (err, files) => {
      if(err) {
        reject(err);
      } else {
        resolve(files);
      }
    });
  });
}

class In18 {
  constructor() {
    this.in18 = {};

    // get language file config
    getFilesAsync()
      .then(files => {
        files.forEach(file => {
          if(file !== 'index.js' && file !== '.DS_Store') {
            this.in18[file.replace(/^(.*)\.js$/g, '$1')] = require(`./${file}`).default;
          }
        });
      });
  }

  /**
   * Return message depended on key and langCode
   * @param {*} key 
   * @param {*} langCode 
   */
  getMessage(type, key, langCode) {
    return this.in18[langCode][type][key];
  }
}

export let in18 = new In18();
