import jwt from 'jsonwebtoken';
import moment from 'moment';
import _ from 'underscore';

import config from '../config';
import ResponseHelper from '../helpers/response-helper';
import { loggingHelper } from '../helpers/logging-helper';

class JwtMiddleware {
  

  static decodeTokenAsync(token) {
    loggingHelper.log('info', 'decodeTokenAsync start');
    loggingHelper.log('info', `param: token = ${token}`);
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.accessToken.secret, (err, decoded) => {
        if(err) {
          reject(new Error('token_invalid'));
        } else if(moment().isAfter(moment(decoded.expiredAt))) {
          reject(new Error('token_expired'));
        } else {
          resolve(decoded);
        }
      });
    });
  }

  static verifyCountryCode(req, res, next) {
    loggingHelper.log('info', 'verifyCountryCode start');
    loggingHelper.log('info', `param: req = ${req}`);
    let countryCode = req.headers['country-code'];
    if(!countryCode) {
      return ResponseHelper.responseError(res, 'missing country Code');
    }
    let country = _.findWhere(global.countries, {code: countryCode});
    if(country) {
      req.country = country;
      return next();
    } else {
      ResponseHelper.responseError(res, 'country code is invalid');
    }
  }

  static generateToken(user, isAdmin, isSeller) {
    loggingHelper.log('info', 'generateToken start');
    loggingHelper.log('info', `param: user = ${user}, isAdmin = ${isAdmin}, isSeller = ${isSeller}`);
    let token;
    if(isAdmin) {
      token = jwt.sign({ id: user.id, email: user.email, adminRole: user.Role.roleName, CountryId: user.CountryId, viewAllCountry: user.viewAllCountry, expireIn: moment().add(1, 'days'), MarketingOfficeId: user.MarketingOffice ? user.MarketingOffice.id : null }, config.accessToken.secret);
    } else if(isSeller) {
      token = jwt.sign({ id: user.id, email: user.email, isSeller: true, MarketingOfficeId: user.MarketingOffice ? user.MarketingOffice.id : null, expireIn: moment().add(12, 'hours') }, config.accessToken.secret);
    } else {
      token = jwt.sign({ id: user.id, email: user.email }, config.accessToken.secret);
    }
    return {token, user};
  }

  static decodeToken(req, res, next) {
    loggingHelper.log('info', 'decodeToken start');
    loggingHelper.log('info', `param: req = ${req}`);
    // check header or url parameters or post parameters for token
    let token = req.body.accessToken || req.query.accessToken || req.headers['x-access-token'];
    // decode token
    if(token) {
      // verifies secret and checks exp
      jwt.verify(token, config.accessToken.secret, function(err, decoded) {
        console.log(`decoded === ${decoded}`);
        loggingHelper.log('info', `decoded === ${decoded}`);
        if(err || decoded.hasOwnProperty('adminRole')) {
          loggingHelper.log('error', `Failed to authenticate token. ${err}`);
          return res.status(403).json({ ok: false, message: `Failed to authenticate token. ${err}` });
        } else if(decoded.expireIn && !moment(decoded.expireIn).isAfter(moment())) {
          return res.status(401).json({ ok: false, message: 'Your account has been expire.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;    
          return next();
        }
      });
    } else {
      return next();
    }
  }

  static verifyUserToken(req, res, next) {
    loggingHelper.log('info', 'verifyUserToken start');
    loggingHelper.log('info', `param: req = ${req}`);
    // check header or url parameters or post parameters for token
    let token = req.body.accessToken || req.query.accessToken || req.headers['x-access-token'];
    // decode token
    if(token) {
      // verifies secret and checks exp
      jwt.verify(token, config.accessToken.secret, function(err, decoded) {
        console.log(`decoded === ${decoded}`);
        loggingHelper.log('info', `decoded === ${decoded}`);
        if(err || decoded.hasOwnProperty('adminRole')) {
          loggingHelper.log('error', `Failed to authenticate token. ${err}`);
          return res.status(403).json({ ok: false, message: `Failed to authenticate token. ${err}` });
        } else if(decoded.isSeller) {
          if(!decoded.expireIn) {
            return res.status(401).json({ ok: false, message: 'Your account has been expire.' });
          } else if(decoded.expireIn && !moment(decoded.expireIn).isAfter(moment())) {
            return res.status(401).json({ ok: false, message: 'Your account has been expire.' });
          } else {
            req.decoded = decoded;    
            return next();
          }
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;    
          return next();
        }
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({ 
        ok: false, 
        message: 'No token provided.' 
      });
    }
  }
  static verifyAdminToken(req, res, next) {
    loggingHelper.log('info', 'verifyAdminToken start');
    loggingHelper.log('info', `param: req = ${req}`);
    // check header or url parameters or post parameters for token
    let token = req.body.token || req.query.token || req.headers['x-access-token'];
    // decode token
    if(token) {
      // verifies secret and checks exp
      jwt.verify(token, config.accessToken.secret, function(err, decoded) {
        if(err || !decoded.hasOwnProperty('adminRole')) {
          return res.status(403).json({ ok: false, message: `Failed to authenticate token. ${err}` });
        } else if(!moment(decoded.expireIn).isAfter(moment())) {
          return res.status(401).json({ ok: false, message: 'Your account has been expire.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          if(!decoded.viewAllCountry && req.method === 'GET') {
            req.query.CountryId = decoded.CountryId;
          }

          return next();
        }
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({ 
        ok: false, 
        message: 'No token provided.' 
      });
    }
  }
  static verifyIsSuperAmin(req, res, next) {
    loggingHelper.log('info', 'verifyIsSuperAmin start');
    loggingHelper.log('info', `param: req = ${req}`);
    if(req.decoded.adminRole === 'super admin') {
      return res.status(403).json({ ok: false, message: 'require super admin permission' });
    } else {
      return next();
    }
  }
}

export default JwtMiddleware;
