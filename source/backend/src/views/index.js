import jwt from 'jsonwebtoken';
import moment from 'moment';
import { Router } from 'express';
import config from '../config';
// import JwtMiddleware from '../middleware/jwt-middleware';

export default ({ db }) => {
  let api = Router();

  // api.get('/users/reset-password', JwtMiddleware.verifyUserToken, (req, res) => {
  //   // check token
  //   let token = req.query.token;
  //   let email = req.query.email;
  //   let valid = true;
  //   let errorMessage = '';
  //   if(!token) {
  //     valid = false;
  //     errorMessage = 'Missing token';
  //     return res.render('reset-password-user', { valid, errorMessage });
  //   }
  //   jwt.verify(token, config.accessToken.secret, (err, decoded) => {
  //     console.log(`decoded.expireAt ${decoded.token}`);
  //     if(err) {
  //       valid = false;
  //       errorMessage = 'Invalid token';
  //     } else if(moment(decoded.token).isBefore(moment())) {
  //       valid = false;
  //       errorMessage = 'Token is expired';
  //     } else {
  //       valid = true;
  //       token = jwt.sign({ token: moment }, config.accessToken.secret);
  //     }
  //     return res.render('reset-password-user', { valid, errorMessage, token, email });
  //   });
  // });

  api.get('/users/active', (req, res) => {
    // check token
    let token = req.query.token;
    let valid = true;
    let errorMessage = '';
    if(!token) {
      valid = false;
      errorMessage = 'Missing token';
      return res.render('active-user', { valid, errorMessage });
    }
    jwt.verify(token, config.accessToken.secret, (err, decoded) => {
      console.log(`decoded.expireAt ${decoded.token}`);
      if(err) {
        valid = false;
        errorMessage = 'Invalid token';
        res.render('active-user', { valid, errorMessage, token, email: decoded.email });
      } else if(moment(decoded.token).isBefore(moment())) {
        valid = false;
        errorMessage = 'Token is expired';
        res.render('active-user', { valid, errorMessage, token, email: decoded.email });
      } else {
        valid = true;
        token = jwt.sign({ token: moment }, config.accessToken.secret);
        // update user status
        db.User.update({isActive: true}, {where: {
          email: decoded.email
        }})
          .then(() => res.render('active-user', { valid, errorMessage, token, email: decoded.email }));
      }
    });
  });

  return api;
};
