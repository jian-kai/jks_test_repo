/**
 * Product Country model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductCountry = sequelize.define('ProductCountry', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    basedPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    sellPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: true
      }
    },
    maxQty: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: true
      }
    },
    isOnline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isOffline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
  
  ProductCountry.associate = function(models) {
    ProductCountry.belongsTo(models.Product);
    ProductCountry.belongsTo(models.Country);
	ProductCountry.hasMany(models.ProductRelated, {as: 'productRelateds'});
  };
  
  return ProductCountry;
}
