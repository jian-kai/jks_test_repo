/**
 * OrderDetail Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var OrderDetail = sequelize.define('OrderDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    price: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    currency: {
      type: DataTypes.STRING(5),
      allowNull: false,
      set(val) {
        this.setDataValue('currency', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    startDeliverDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
  });
  
  OrderDetail.associate = function(models) {
    OrderDetail.belongsTo(models.Order);
    OrderDetail.belongsTo(models.ProductCountry);
    OrderDetail.belongsTo(models.PlanCountry);
  };
  
  return OrderDetail;
}
