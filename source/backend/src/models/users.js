/**
 * USERS model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 50]
      }
    },
    password: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    firstName: {
      type: DataTypes.STRING(20),
      allowNull: true,
      validate: {
        len: [0, 100]
      }
    },
    lastName: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 100]
      }
    },
    socialId: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    birthday: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      validate: {
        isDate: true
      }
    },
    gender: {
      type: DataTypes.ENUM,
      values: ['male', 'female'],
      allowNull: true,
      validate: {
        isIn: [['male', 'female']]
      }
    },
    phone: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [1, 30]
      }
    },
    defaultLanguage: {
      type: DataTypes.STRING(3),
      defaultValue: 'en',
      validate: {
        len: [1, 3]
      },
      set(val) {
        this.setDataValue('defaultLanguage', val.toUpperCase());
      }
    },
    defaultDateFormat: {
      type: DataTypes.STRING(20),
      defaultValue: 'yyyy/MM/dd',
      validate: {
        len: [1, 20]
      }
    },
    avatarUrl: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    notificationSettings: {
      type: DataTypes.STRING,
      defaultValue: JSON.stringify([])
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    defaultShipping: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    defaultBilling: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    hasReceiveOffer: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isGuest: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    badgeId: {
      type: DataTypes.STRING(10),
      allowNull: true,
      validate: {
        len: [1, 10]
      }
    }
  });

  User.associate = function(models) {
    User.hasOne(models.Cart);
    User.belongsTo(models.Country);
    User.hasMany(models.DeliveryAddress, {as: 'deliveryAddresses'});
    User.hasMany(models.Order, {as: 'orders'});
    User.hasMany(models.RecentlyViewed, {as: 'recentlyVieweds'});
    User.hasMany(models.Notification, {as: 'notifications'});
    User.hasMany(models.Subscription, {as: 'subscriptions'});
    User.hasMany(models.Card, {as: 'cards'});
    User.hasMany(models.CustomPlan);
    User.hasMany(models.cancellationjourneys);
  };

  User.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'password');
    return values;
  };
  
  return User;
}
