/**
 * Role Details model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var RoleDetail = sequelize.define('RoleDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    canView: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    canCreate: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    canEdit: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    canDelete: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    canUpload: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    canDownload: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {
    timestamps: false
  });

  RoleDetail.associate = function(models) {
    RoleDetail.belongsTo(models.Role);
    RoleDetail.belongsTo(models.Path);
  };

  return RoleDetail;
}
