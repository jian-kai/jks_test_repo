/**
 * Role model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Path = sequelize.define('Path', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    pathValue: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 50]
      }
    },
  });

  Path.associate = function(models) {
    Path.hasMany(models.RoleDetail, {as: 'roleDetails'});
  };
  
  Path.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    return values;
  };

  return Path;
}
