/**
 * Delivery Address model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Card = sequelize.define('Card', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    customerId: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    cardNumber: {
      type: DataTypes.STRING(4),
      allowNull: false,
      validate: {
        len: [1, 4]
      }
    },
    branchName: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        len: [1, 20]
      }
    },
    cardName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      validate: {
        len: [1, 200]
      }
    },
    expiredYear: {
      type: DataTypes.STRING(4),
      allowNull: false,
      validate: {
        len: [1, 4]
      }
    },
    expiredMonth: {
      type: DataTypes.STRING(2),
      allowNull: false,
      validate: {
        len: [1, 2]
      }
    },
    isDefault: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isRemoved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    type: {
      type: DataTypes.ENUM,
      values: ['website', 'baWeb'],
      allowNull: true,
      validate: {
        isIn: [['website', 'baWeb']]
      }
    }
  });
  
  Card.associate = function(models) {
    Card.belongsTo(models.User);
    Card.belongsTo(models.Country);
    Card.hasMany(models.Subscription);
  };

  Card.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    Reflect.deleteProperty(values, 'UserId');
    return values;
  };
  
  return Card;
}
