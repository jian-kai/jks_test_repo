/**
 * Plan Images model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var FileUpload = sequelize.define('FileUpload', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      unique: true,
      allowNull: false
    },
    states: {
      type: DataTypes.ENUM,
      values: ['Processing', 'Completed', 'Canceled'],
      defaultValue: 'Completed',
      validate: {
        isIn: [['Processing', 'Completed', 'Canceled']]
      }
    },
    url: {
      type: DataTypes.STRING(400),
      allowNull: true
    },
    orderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
    },
    bulkOrderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
    },
    isDownloaded: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  FileUpload.associate = function(models) {
    FileUpload.belongsTo(models.Admin);
  };
  
  return FileUpload;
}
