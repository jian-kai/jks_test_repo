/**
 * Plan Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanTranslate = sequelize.define('PlanTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    shortDescription: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    subsequentDescription: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  }, {
    timestamps: false
  });
  
  PlanTranslate.associate = function(models) {
    PlanTranslate.belongsTo(models.Plan);
  };

  return PlanTranslate;
}
