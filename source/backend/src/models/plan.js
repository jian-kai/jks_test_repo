/**
 * Plan model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Plan = sequelize.define('Plan', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    sku: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 50]
      }
    },
    rating: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    isFeatured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    status: {
      type: DataTypes.ENUM,
      values: ['active', 'removed'],
      allowNull: false,
      defaultValue: 'active',
      validate: {
        isIn: [['active', 'removed']]
      }
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    slug: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    },
    isTrial: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  Plan.associate = function(models) {
    Plan.belongsTo(models.PlanType);
    Plan.belongsTo(models.PlanGroup);
    Plan.hasMany(models.PlanImage, {as: 'planImages'});
    Plan.hasMany(models.Subscription);
    Plan.hasMany(models.PlanTranslate, {as: 'planTranslate'});
    Plan.hasMany(models.PlanCountry, {as: 'planCountry'});
  };
  
  Plan.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    Reflect.deleteProperty(values, 'PlanTypeId');
    return values;
  };
  
  return Plan;
}
