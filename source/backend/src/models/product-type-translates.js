/**
 * Product Type Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductTypeTranslate = sequelize.define('ProductTypeTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      unique: true,
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      },
      validate: {
        len: [1, 2]
      }
    }
  });
  
  ProductTypeTranslate.associate = function(models) {
    ProductTypeTranslate.belongsTo(models.ProductType);
  };
  
  return ProductTypeTranslate;
}
