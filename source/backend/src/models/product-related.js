/**
 * Product Related model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
    var ProductRelated = sequelize.define('ProductRelated', {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      }
    }, {
      timestamps: false
    });
    
    ProductRelated.associate = function(models) {
      ProductRelated.belongsTo(models.ProductCountry);
      ProductRelated.belongsTo(models.ProductCountry, {as: 'RelatedProductCountry'});
    };
    
    return ProductRelated;
  }
  