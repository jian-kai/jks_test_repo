/**
 * Custom Plan Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var CustomPlanDetail = sequelize.define('CustomPlanDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: true
      }
    }
  }, {
    timestamps: false
  });
  
  CustomPlanDetail.associate = function(models) {
    CustomPlanDetail.belongsTo(models.CustomPlan);
    CustomPlanDetail.belongsTo(models.ProductCountry);
  };
  
  return CustomPlanDetail;
}
