/**
 * BulkOrderHistory Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var BulkOrderHistory = sequelize.define('BulkOrderHistory', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    message: {
      type: DataTypes.STRING(300),
      allowNull: false,
    },
    isRemark: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  });

  BulkOrderHistory.associate = function(models) {
    BulkOrderHistory.belongsTo(models.BulkOrder);
  };

  return BulkOrderHistory;
}
