/**
 * Plan Trial Product model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanTrialProduct = sequelize.define('PlanTrialProduct', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: true
      }
    }
  }, {
    timestamps: false
  });
  
  PlanTrialProduct.associate = function(models) {
    PlanTrialProduct.belongsTo(models.PlanCountry);
    PlanTrialProduct.belongsTo(models.ProductCountry);
  };
  
  return PlanTrialProduct;
}
