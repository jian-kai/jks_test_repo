/**
 * Plan Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanDetail = sequelize.define('PlanDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    deliverPlan: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps: false
  });
  
  PlanDetail.associate = function(models) {
    PlanDetail.belongsTo(models.PlanCountry);
    PlanDetail.belongsTo(models.ProductCountry);
  };
  
  return PlanDetail;
}
