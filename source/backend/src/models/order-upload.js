/**
 * Order Upload model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var OrderUpload = sequelize.define('OrderUpload', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    doFileName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    wareHouseFileName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    orderNumber: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    orderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
      validate: {
        len: [0, 10]
      }
    },
    bulkOrderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
      validate: {
        len: [0, 10]
      }
    },
    sku: {
      type: DataTypes.STRING(64),
      allowNull: true,
      validate: {
        len: [0, 10]
      }
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    deliveryId: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    carrierAgent: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM,
      values: ['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled'],
      defaultValue: 'On Hold',
      validate: {
        isIn: [['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled']]
      }
    },
    region: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
  });
  
  // OrderUpload.associate = function(models) {
  //   OrderUpload.belongsTo(models.Order);
  //   OrderUpload.belongsTo(models.ProductCountry);
  //   OrderUpload.belongsTo(models.PlanCountry);
  // };
  
  return OrderUpload;
}
