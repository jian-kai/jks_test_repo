/**
 * SaleReport model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var SaleReport = sequelize.define('SaleReport', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    badgeId: {
      type: DataTypes.STRING(10),
      allowNull: true,
      validate: {
        len: [1, 10]
      }
    },
    agentName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      validate: {
        len: [0, 200]
      }
    },
    channelType: {
      type: DataTypes.ENUM,
      values: ['Event', 'Streets', 'B2B', 'RES'],
      allowNull: true,
      validate: {
        isIn: [['Event', 'Streets', 'B2B', 'RES']]
      }
    },
    eventLocationCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    states: {
      type: DataTypes.ENUM,
      values: ['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled'],
      defaultValue: 'On Hold',
      validate: {
        isIn: [['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled']]
      }
    },
    deliveryId: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    carrierAgent: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    customerName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    deliveryAddress: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    deliveryContact: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    billingAddress: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    billingContact: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    productCategory: {
      type: DataTypes.ENUM,
      values: ['Ala Carte', 'Subscription'],
      allowNull: true
    },
    sku: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    qty: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    productName: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    currency: {
      type: DataTypes.STRING(5),
      allowNull: true,
      set(val) {
        this.setDataValue('currency', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    paymentType: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    region: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    category: {
      type: DataTypes.ENUM,
      values: ['client', 'sales app'],
      defaultValue: 'client'
    },
    saleDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    orderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
    },
    bulkOrderId: {
      type: DataTypes.INTEGER(10).ZEROFILL,
      allowNull: true,
    },
    promoCode: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    totalPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    subTotalPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    discountAmount: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    shippingFee: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    taxAmount: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    grandTotalPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    source: {
      type: DataTypes.STRING(50),
      defaultValue: 'direct'
    },
    medium: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    campaign: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    term: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    content: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    CountryId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    taxInvoiceNo: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    moCode: {
      type: DataTypes.STRING(5),
      allowNull: true,
      validate: {
        len: [1, 5]
      }
    },
    taxRate: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    isDirectTrial: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  SaleReport.associate = function(models) {
    SaleReport.belongsTo(models.Country);
    SaleReport.belongsTo(models.MarketingOffice);
  };

  return SaleReport;
}
