/**
 * SubscriptionHistory Detail model
 * @author PhuPT
 */
export default function(sequelize, DataTypes) {
  var SubscriptionHistory = sequelize.define('SubscriptionHistory', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    message: {
      type: DataTypes.STRING(300),
      allowNull: false
    },
    detail: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    subscriptionId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  });

  SubscriptionHistory.associate = function(models) {
    SubscriptionHistory.belongsTo(models.Subscription);
  };

  return SubscriptionHistory;
}
