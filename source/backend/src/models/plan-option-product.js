/**
 * Plan Trial Product model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanOptionProduct = sequelize.define('PlanOptionProduct', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: true
      }
    }
  }, {
    timestamps: false
  });
  
  PlanOptionProduct.associate = function(models) {
    PlanOptionProduct.belongsTo(models.PlanOption);
    PlanOptionProduct.belongsTo(models.ProductCountry);
  };
  
  return PlanOptionProduct;
}
