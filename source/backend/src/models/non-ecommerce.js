/**
 * Non Ecommerce model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var NonEcommerce = sequelize.define('NonEcommerce', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 50]
      }
    }
  });

  NonEcommerce.associate = function(models) {
    NonEcommerce.belongsTo(models.Country);
  };

  NonEcommerce.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'CountryId');
    return values;
  };
  
  return NonEcommerce;
}
