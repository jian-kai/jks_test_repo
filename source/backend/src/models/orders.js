/**
 * Order model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Order = sequelize.define('Order', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER(10).ZEROFILL
    },
    states: {
      type: DataTypes.ENUM,
      values: ['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled'],
      defaultValue: 'On Hold',
      validate: {
        isIn: [['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled']]
      }
    },
    deliveryId: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    phone: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    fullName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      validate: {
        len: [0, 200]
      }
    },
    paymentType: {
      type: DataTypes.ENUM,
      values: ['stripe', 'iPay88', 'cash'],
      allowNull: true,
      validate: {
        isIn: [['stripe', 'iPay88', 'cash']]
      }
    },
    promoCode: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    subscriptionIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    taxRate: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    taxName: {
      type: DataTypes.STRING(20),
      defaultValue: 'GST'
    },
    carrierKey: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    carrierAgent: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    vendor: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    isBulkCreated: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    source: {
      type: DataTypes.STRING(50),
      defaultValue: 'direct'
    },
    medium: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    campaign: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    term: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    content: {
      type: DataTypes.STRING(50),
      defaultValue: 'none'
    },
    taxInvoiceNo: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    channelType: {
      type: DataTypes.ENUM,
      values: ['Event', 'Streets', 'B2B', 'RES'],
      allowNull: true,
      validate: {
        isIn: [['Event', 'Streets', 'B2B', 'RES']]
      }
    },
    eventLocationCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    isDirectTrial: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isSubsequentOrder: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  Order.associate = function(models) {
    Order.belongsTo(models.Country);
    Order.belongsTo(models.DeliveryAddress);
    Order.belongsTo(models.DeliveryAddress, {as: 'BillingAddress'});
    Order.belongsTo(models.Promotion);
    Order.belongsTo(models.User);
    Order.belongsTo(models.SellerUser);
    Order.hasMany(models.OrderDetail, {as: 'orderDetail'});
    Order.hasOne(models.Receipt);
    Order.belongsTo(models.MarketingOffice);
    Order.hasMany(models.UsedToken, {as: 'usedTokens'});
    Order.hasMany(models.OrderHistory, {as: 'orderHistories'});
    Order.hasMany(models.cancellationjourneys);
  };

  Order.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'PromotionId');
    Reflect.deleteProperty(values, 'UserId');
    Reflect.deleteProperty(values, 'SubscriptionId');
    return values;
  };
  
  return Order;
}
