/**
 * Product Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PromotionTranslate = sequelize.define('PromotionTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  }, {
    timestamps: false
  });
  
  PromotionTranslate.associate = function(models) {
    PromotionTranslate.belongsTo(models.Promotion);
  };
  
  return PromotionTranslate;
}
