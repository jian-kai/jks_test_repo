/**
 * Administrator model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Admin = sequelize.define('Admin', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 50]
      }
    },
    password: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    firstName: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        len: [0, 100]
      }
    },
    lastName: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        len: [0, 100]
      }
    },
    staffId: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    // role: {
    //   type: DataTypes.STRING(50),
    //   allowNull: true,
    //   validate: {
    //     len: [0, 50]
    //   }
    // },
    phone: {
      type: DataTypes.STRING(20),
      allowNull: true,
      validate: {
        len: [0, 20]
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    viewAllCountry: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    timestamps: false
  });

  Admin.associate = function(models) {
    Admin.belongsTo(models.Country);
    Admin.belongsTo(models.Role);
    Admin.belongsTo(models.MarketingOffice);
    Admin.hasMany(models.Article, {foreignKey: 'postBy'});
    Admin.hasMany(models.FileUpload);
  };
  
  Admin.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'password');
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    Reflect.deleteProperty(values, 'CountryId');
    return values;
  };

  return Admin;
}
