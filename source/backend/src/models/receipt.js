/**
 * Receipt model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Receipt = sequelize.define('Receipt', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    chargeId: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    last4: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    branchName: {
      type: DataTypes.STRING(20),
      allowNull: true,
      validate: {
        len: [0, 20]
      }
    },
    expiredYear: {
      type: DataTypes.STRING(4),
      allowNull: true,
      validate: {
        len: [0, 4]
      }
    },
    expiredMonth: {
      type: DataTypes.STRING(2),
      allowNull: true,
      validate: {
        len: [0, 2]
      }
    },
    chargeFee: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    chargeCurrency: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    subTotalPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    discountAmount: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    shippingFee: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    taxAmount: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    totalPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    },
    currency: {
      type: DataTypes.STRING(5),
      allowNull: false,
      validate: {
        len: [1, 5]
      }
    },
    originPrice: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0
    }
  });
  
  Receipt.associate = function(models) {
    Receipt.belongsTo(models.Order);
    Receipt.belongsTo(models.Subscription);
  };
  
  return Receipt;
}
