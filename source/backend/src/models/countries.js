/**
 * COUNTRY model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Country = sequelize.define('Country', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(5),
      allowNull: false,
      unique: true,
      set(val) {
        this.setDataValue('code', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 20]
      }
    },
    currencyCode: {
      type: DataTypes.STRING(5),
      allowNull: false,
      set(val) {
        this.setDataValue('currencyCode', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    currencyDisplay: {
      type: DataTypes.STRING(5),
      allowNull: false,
      set(val) {
        this.setDataValue('currencyDisplay', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    defaultLang: {
      type: DataTypes.STRING(2),
      allowNull: false,
      validate: {
        len: [1, 2]
      },
      set(val) {
        this.setDataValue('defaultLang', val.toUpperCase());
      },
    },
    callingCode: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    companyName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    companyRegistrationNumber: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    companyAddress: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        len: [1, 100]
      }
    },
    taxRate: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    taxName: {
      type: DataTypes.STRING(20),
      defaultValue: 'GST'
    },
    taxCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    includedTaxToProduct: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    taxNumber: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        len: [1, 20]
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    phone: {
      type: DataTypes.STRING(20),
      allowNull: true,
      validate: {
        len: [0, 20]
      }
    },
    isBaEcommerce: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isBaSubscription: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isBaAlaCarte: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isBaStripe: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isWebEcommerce: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isWebSubscription: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isWebAlaCarte: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isWebStripe: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    shippingMinAmount: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    shippingFee: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    shippingTrialFee: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    // customOneDayShippingFee: {
    //   type: DataTypes.DECIMAL(10, 2),
    //   defaultValue: 0,
    //   validate: {
    //     isDecimal: true
    //   }
    // },
    // customTwoDayShippingFee: {
    //   type: DataTypes.DECIMAL(10, 2),
    //   defaultValue: 0,
    //   validate: {
    //     isDecimal: true
    //   }
    // },
    // customGroundShippingFee: {
    //   type: DataTypes.DECIMAL(10, 2),
    //   defaultValue: 0,
    //   validate: {
    //     isDecimal: true
    //   }
    // },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
  }, {
    timestamps: false
  });
  
  Country.associate = function(models) {
    Country.hasMany(models.ProductCountry);
    Country.hasMany(models.PlanCountry);
    Country.hasMany(models.User);
    Country.hasMany(models.SellerUser);
    Country.hasMany(models.DeliveryAddress);
    Country.hasMany(models.CartDetail);
    Country.hasMany(models.Admin, {as: 'admins'});
    Country.hasMany(models.State);
    Country.hasMany(models.DirectoryCountry);
    Country.hasMany(models.SupportedLanguage);
    Country.hasMany(models.CustomPlan);
    Country.hasMany(models.SaleReport);
  };
  
  return Country;
}
