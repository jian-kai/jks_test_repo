/**
 * FAQ model
 * @author VS
 */
export default function(sequelize, DataTypes) {
  var messages = sequelize.define('messages', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    meassage_type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toLowerCase());
      }
    }
  }, {
    timestamps: false
  });
  
  messages.associate = function(models) {
    messages.hasMany(models.messagestranslates);
    messages.belongsTo(models.Country);
  };

  return messages;
}
