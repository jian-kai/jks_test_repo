/**
 * Notification Type model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var NotificationType = sequelize.define('NotificationType', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    serviceImageUrl: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  }, {
    timestamps: false
  });
  
  NotificationType.associate = function(models) {
    NotificationType.hasMany(models.Notification, {as: 'notifications'});
    NotificationType.hasMany(models.User, {as: 'users'});
  };
  
  return NotificationType;
}
