/**
 * Product Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductTranslate = sequelize.define('ProductTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    shortDescription: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    seoTitle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      },
      validate: {
        len: [1, 2]
      }
    }
  }, {
    timestamps: false
  });
  
  ProductTranslate.associate = function(models) {
    ProductTranslate.belongsTo(models.Product);
  };
  
  return ProductTranslate;
}
