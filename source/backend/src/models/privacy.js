/**
 * FAQ Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Privacy = sequelize.define('Privacy', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  }, {
    timestamps: false
  });
  
  Privacy.associate = function(models) {
    Privacy.belongsTo(models.Country);
  };
  
  return Privacy;
}
