/**
 * BulkOrder model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var BulkOrder = sequelize.define('BulkOrder', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER(8).ZEROFILL
    },
    states: {
      type: DataTypes.ENUM,
      values: ['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled'],
      defaultValue: 'On Hold',
      validate: {
        isIn: [['Pending', 'On Hold', 'Payment Received', 'Processing', 'Delivering', 'Completed', 'Canceled']]
      }
    },
    deliveryId: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [0, 50]
      }
    },
    phone: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [0, 30]
      }
    },
    fullName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      validate: {
        len: [0, 200]
      }
    },
    paymentType: {
      type: DataTypes.ENUM,
      values: ['stripe', 'iPay88', 'cash'],
      allowNull: true,
      validate: {
        isIn: [['stripe', 'iPay88', 'cash']]
      }
    },
    promoCode: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    subscriptionIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    taxRate: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    taxName: {
      type: DataTypes.STRING(20),
      defaultValue: 'GST'
    },
    carrierKey: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    carrierAgent: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    vendor: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    taxInvoiceNo: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  });
  
  BulkOrder.associate = function(models) {
    BulkOrder.belongsTo(models.Country);
    BulkOrder.belongsTo(models.DeliveryAddress);
    BulkOrder.belongsTo(models.DeliveryAddress, {as: 'BillingAddress'});
    BulkOrder.hasMany(models.BulkOrderDetail, {as: 'bulkOrderDetail'});
    BulkOrder.hasMany(models.BulkOrderHistory, {as: 'bulkOrderHistories'});
  };
  
  return BulkOrder;
}
