/**
 * STATES model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var SupportedLanguage = sequelize.define('SupportedLanguage', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    languageCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      validate: {
        len: [1, 2]
      },
      set(val) {
        this.setDataValue('languageCode', val.toUpperCase());
      }
    },
    languageName: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  });

  SupportedLanguage.associate = function(models) {
    SupportedLanguage.belongsTo(models.Country);
  };

  SupportedLanguage.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    // Reflect.deleteProperty(values, 'CountryId');
    return values;
  };
  
  return SupportedLanguage;
}
