/**
 * FAQ model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Faq = sequelize.define('Faq', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    isSubscription: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isAlaCarte: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  });
  
  Faq.associate = function(models) {
    Faq.hasMany(models.FaqTranslate, {as: 'faqTranslate'});
    Faq.belongsTo(models.Country);
  };
  
  return Faq;
}
