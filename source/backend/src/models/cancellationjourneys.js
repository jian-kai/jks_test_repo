/**
 * FAQ model
 * @author VS
 */
export default function(sequelize, DataTypes) {
  var cancellationjourneys = sequelize.define('cancellationjourneys', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email:{
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    MessagestranslatesId: {
      type: DataTypes.STRING(50),
      allowNull: true,
  
      },
      OtherReason: {
        type: DataTypes.STRING(100),
        allowNull: true,
      }
  }, {
    timestamps: true
  });
  
  cancellationjourneys.associate = function(models) {
    cancellationjourneys.belongsTo(models.User);
    cancellationjourneys.belongsTo(models.Order);
    cancellationjourneys.belongsTo(models.Subscription);
  };

  return cancellationjourneys;
}
