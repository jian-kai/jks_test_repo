/**
 * FAQ model
 * @author VS
 */
export default function(sequelize, DataTypes) {
  var messagestranslates = sequelize.define('messagestranslates', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    content: {
      type: DataTypes.TEXT,
      allowNul: false
    }
    
  }, {
    timestamps: false
  });
  
  messagestranslates.associate = function(models) {
    messagestranslates.belongsTo(models.messages);
  };

  return messagestranslates;
}
