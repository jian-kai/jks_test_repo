/**
 * STATES model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var State = sequelize.define('State', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    isDefault: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });

  State.associate = function(models) {
    State.belongsTo(models.Country);
  };

  State.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    // Reflect.deleteProperty(values, 'CountryId');
    return values;
  };
  
  return State;
}
