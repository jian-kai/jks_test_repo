/**
 * Notification model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Notification = sequelize.define('Notification', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    happenedDate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  });
  
  Notification.associate = function(models) {
    Notification.belongsTo(models.User);
    Notification.belongsTo(models.NotificationType);
  };
  
  return Notification;
}
