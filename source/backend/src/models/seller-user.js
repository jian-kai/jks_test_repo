/**
 * USERS model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var SellerUser = sequelize.define('SellerUser', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    badgeId: {
      type: DataTypes.STRING(10),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 10]
      }
    },
    icNumber: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        len: [1, 20]
      }
    },
    agentName: {
      type: DataTypes.STRING(200),
      allowNull: false,
      validate: {
        len: [0, 200]
      }
    },
    startDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      validate: {
        isDate: true
      }
    },
    campaign: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [0, 50]
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    latestUpdatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      validate: {
        isDate: true
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    channelType: {
      type: DataTypes.ENUM,
      values: ['Event', 'Streets', 'B2B', 'RES'],
      allowNull: true,
      validate: {
        isIn: [['Event', 'Streets', 'B2B', 'RES']]
      }
    },
    eventLocationCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    lastLoginAt: {
      type: DataTypes.DATE,
      allowNull: true,
    }
  });

  SellerUser.associate = function(models) {
    SellerUser.belongsTo(models.Country);
    SellerUser.belongsTo(models.MarketingOffice);
    SellerUser.hasMany(models.Order, {as: 'orders'});
    SellerUser.hasMany(models.Subscription, {as: 'subscriptions'});
  };

  SellerUser.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    Reflect.deleteProperty(values, 'CountryId');
    return values;
  };
  
  return SellerUser;
}
