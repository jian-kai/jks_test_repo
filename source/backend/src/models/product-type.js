/**
 * Product Type model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductType = sequelize.define('ProductType', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    status: {
      type: DataTypes.ENUM,
      values: ['active', 'removed'],
      allowNull: false,
      defaultValue: 'active',
      validate: {
        isIn: [['active', 'removed']]
      }
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  });
  
  ProductType.associate = function(models) {
    ProductType.hasMany(models.Product, {as: 'products'});
    ProductType.hasMany(models.ProductTypeTranslate, {as: 'productTypeTranslate'});
  };
  
  return ProductType;
}
