/**
 * Article Type model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  let AndroidVersion = sequelize.define('AndroidVersion', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    version: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apkUrl: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    size: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  });
  
  return AndroidVersion;
}
