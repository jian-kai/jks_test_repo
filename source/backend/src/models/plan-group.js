/**
 * Plan Group model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanGroup = sequelize.define('PlanGroup', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    timestamps: false
  });
  
  PlanGroup.associate = function(models) {
    PlanGroup.hasMany(models.Plan);
    PlanGroup.hasMany(models.PlanGroupTranslate, {as: 'planGroupTranslate'});
  };
  
  return PlanGroup;
}
