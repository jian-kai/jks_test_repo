/**
 * Article model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Article = sequelize.define('Article', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    bannerUrl: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    hashTag: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    views: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    title: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    isFeatured: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  });
  
  Article.associate = function(models) {
    Article.belongsTo(models.ArticleType);
    Article.belongsTo(models.Admin, {foreignKey: 'postBy'});
  };
  
  return Article;
}
