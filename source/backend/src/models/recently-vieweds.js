/**
 * Recently Viewed model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var RecentlyViewed = sequelize.define('RecentlyViewed', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }
  }, {
    timestamps: false
  });
  
  RecentlyViewed.associate = function(models) {
    RecentlyViewed.belongsTo(models.User);
    RecentlyViewed.belongsTo(models.ProductCountry);
  };
  
  return RecentlyViewed;
}
