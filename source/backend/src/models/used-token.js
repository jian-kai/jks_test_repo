/**
 * Used Token model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var UsedToken = sequelize.define('UsedToken', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    token: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  });
  
  UsedToken.associate = function(models) {
    UsedToken.belongsTo(models.Order);
  };
  
  return UsedToken;
}
