/**
 * FAQ Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Term = sequelize.define('Term', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    content: {
      type: DataTypes.TEXT,
      allowNul: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  }, {
    timestamps: false
  });
  
  Term.associate = function(models) {
    Term.belongsTo(models.Country);
  };
  
  return Term;
}
