import moment from 'moment';

/**
 * Subscription model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Subscription = sequelize.define('Subscription', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    nextDeliverDate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      // defaultValue: moment().add(1, 'day').format('YYYY-MM-DD')
    },
    startDeliverDate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      // defaultValue: moment().add(1, 'day').format('YYYY-MM-DD')
    },
    expiredDateAt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    currentDeliverNumber: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    totalDeliverTimes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nextChargeDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    currentChargeNumber: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    totalChargeTimes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pricePerCharge: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false
    },
    currentOrderId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM,
      values: ['Processing', 'On Hold', 'Finished', 'Canceled'],
      defaultValue: 'On Hold',
      validate: {
        isIn: [['Processing', 'On Hold', 'Finished', 'Canceled']]
      }
    },
    fullname: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    discountPercent: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    lastDeliverydate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    price: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    isTrial: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isCustom: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    hasSendFollowUp1: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    hasSendFollowUp2: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    hasSendFollowUp5Days: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    hasSendCancelRequest: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isOffline: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    cancellationReason: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    channelType: {
      type: DataTypes.ENUM,
      values: ['Event', 'Streets', 'B2B', 'RES'],
      allowNull: true,
      validate: {
        isIn: [['Event', 'Streets', 'B2B', 'RES']]
      }
    },
    eventLocationCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    promoCode: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    promotionId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    isDirectTrial: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  Subscription.associate = function(models) {
    Subscription.belongsTo(models.User);
    Subscription.belongsTo(models.SellerUser);
    Subscription.belongsTo(models.Card);
    Subscription.belongsTo(models.DeliveryAddress);
    Subscription.belongsTo(models.MarketingOffice);
    Subscription.belongsTo(models.DeliveryAddress, {as: 'BillingAddress'});
    Subscription.belongsTo(models.PlanCountry);
    Subscription.belongsTo(models.CustomPlan);
    Subscription.belongsTo(models.PlanOption);
    Subscription.hasMany(models.SubscriptionHistory, {as: 'subscriptionHistories'});
    Subscription.hasMany(models.cancellationjourneys);
  };
  
  return Subscription;
}
