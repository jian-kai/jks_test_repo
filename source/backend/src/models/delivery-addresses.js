/**
 * Delivery Address model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var DeliveryAddress = sequelize.define('DeliveryAddress', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    firstName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      validate: {
        len: [0, 100]
      }
    },
    lastName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      validate: {
        len: [0, 100]
      }
    },
    fullName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      validate: {
        len: [1, 200]
      }
    },
    contactNumber: {
      type: DataTypes.STRING(30),
      allowNull: true,
      validate: {
        len: [1, 30]
      }
    },
    state: {
      type: DataTypes.STRING(100),
      allowNull: true,
      validate: {
        len: [1, 100]
      }
    },
    address: {
      type: DataTypes.STRING(150),
      allowNull: false,
      validate: {
        len: [1, 150]
      }
    },
    portalCode: {
      type: DataTypes.STRING(10),
      allowNull: false,
      validate: {
        len: [1, 10]
      }
    },
    city: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: [1, 50]
      }
    },
    company: {
      type: DataTypes.STRING(50),
      allowNull: true,
      validate: {
        len: [1, 50]
      }
    },
    isRemoved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isBulkAddress: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  
  DeliveryAddress.associate = function(models) {
    DeliveryAddress.belongsTo(models.Country);
    DeliveryAddress.belongsTo(models.User);
    DeliveryAddress.belongsTo(models.SellerUser);
    DeliveryAddress.hasMany(models.Order);
  };

  DeliveryAddress.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'CountryId');
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    return values;
  };
  
  return DeliveryAddress;
}
