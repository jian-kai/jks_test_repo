/**
 * Plan Type Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanOptionTranslate = sequelize.define('PlanOptionTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      },
      validate: {
        len: [1, 2]
      }
    }
  });
  
  PlanOptionTranslate.associate = function(models) {
    PlanOptionTranslate.belongsTo(models.PlanOption);
  };
  
  return PlanOptionTranslate;
}
