/**
 * Cart Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var CartDetail = sequelize.define('CartDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    qty: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
  
  CartDetail.associate = function(models) {
    CartDetail.belongsTo(models.Cart);
    CartDetail.belongsTo(models.ProductCountry);
    CartDetail.belongsTo(models.PlanCountry);
    CartDetail.belongsTo(models.Country);
  };
  
  return CartDetail;
}
