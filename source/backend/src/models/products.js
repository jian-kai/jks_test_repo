/**
 * Product model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Product = sequelize.define('Product', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    sku: {
      type: DataTypes.STRING(64),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 64]
      }
    },
    rating: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    isFeatured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
	isFeaturedAsk: {
		type: DataTypes.BOOLEAN,
		defaultValue: false
	},
	isFeaturedProduct: {
		type: DataTypes.BOOLEAN,
		defaultValue: false
	},
    status: {
      type: DataTypes.ENUM,
      values: ['active', 'removed'],
      allowNull: false,
      defaultValue: 'active',
      validate: {
        isIn: [['active', 'removed']]
      }
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    slug: {
      type: DataTypes.STRING(50),
      allowNull: true,
      unique: true
    },
    long: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    width: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    hight: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    weight: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    }
  });
  
  Product.associate = function(models) {
    Product.belongsTo(models.ProductType);
    Product.hasMany(models.ProductReview, {as: 'productReviews'});
    Product.hasMany(models.ProductCountry, {as: 'productCountry'});
    Product.hasMany(models.ProductTranslate, {as: 'productTranslate'});
    Product.hasMany(models.ProductImage, {as: 'productImages'});
    Product.hasMany(models.ProductDetail, {as: 'productDetails'});
  };

  Product.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    // Reflect.deleteProperty(values, 'ProductTypeId');
    return values;
  };
  
  return Product;
}
