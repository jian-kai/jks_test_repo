/**
 * FAQ Translate model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var FaqTranslate = sequelize.define('FaqTranslate', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    question: {
      type: DataTypes.TEXT,
      allowNul: false
    },
    answer: {
      type: DataTypes.TEXT,
      allowNul: false
    }
  }, {
    timestamps: false
  });
  
  FaqTranslate.associate = function(models) {
    FaqTranslate.belongsTo(models.Faq);
  };
  
  return FaqTranslate;
}
