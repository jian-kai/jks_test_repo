/**
 * OrderHistory Detail model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var OrderHistory = sequelize.define('OrderHistory', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    message: {
      type: DataTypes.STRING(300),
      allowNull: false,
    },
    isRemark: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    cancellationReason: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  });

  OrderHistory.associate = function(models) {
    OrderHistory.belongsTo(models.Order);
  };

  return OrderHistory;
}
