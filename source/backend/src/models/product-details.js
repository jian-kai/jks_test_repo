/**
 * Product Review model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductDetail = sequelize.define('ProductDetail', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    type: {
      type: DataTypes.ENUM,
      values: ['left', 'center', 'right'],
      defaultValue: 'center',
      validate: {
        isIn: [['left', 'center', 'right']]
      }
    },
    imageUrl: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    details: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      },
      validate: {
        len: [1, 2]
      }
    }
  }, {
    timestamps: false
  });
  
  ProductDetail.associate = function(models) {
    ProductDetail.belongsTo(models.Product);
  };
  
  return ProductDetail;
}
