/**
 * Role model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var MarketingOffice = sequelize.define('MarketingOffice', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    moCode: {
      type: DataTypes.STRING(5),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 5]
      }
    },
    organization: {
      type: DataTypes.TEXT,
      allowNull: false
    },
  });

  MarketingOffice.associate = function(models) {
    MarketingOffice.hasMany(models.Admin);
    MarketingOffice.hasMany(models.SellerUser);
    MarketingOffice.hasMany(models.Order);
    MarketingOffice.hasMany(models.SaleReport);
    MarketingOffice.hasMany(models.Subscription);
  };

  return MarketingOffice;
}
