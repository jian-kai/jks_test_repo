/**
 * Plan Images model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanImage = sequelize.define('PlanImage', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    isDefault: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    timestamps: false
  });
  
  PlanImage.associate = function(models) {
    PlanImage.belongsTo(models.Plan);
  };
  
  return PlanImage;
}
