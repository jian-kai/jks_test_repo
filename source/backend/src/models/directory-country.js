/**
 * COUNTRY model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var DirectoryCountry = sequelize.define('DirectoryCountry', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(5),
      allowNull: false,
      unique: true,
      set(val) {
        this.setDataValue('code', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    codeIso2: {
      type: DataTypes.STRING(5),
      allowNull: false,
      unique: true,
      set(val) {
        this.setDataValue('codeIso2', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      validate: {
        len: [1, 200]
      }
    },
    nativeName: {
      type: DataTypes.STRING(200),
      allowNull: false,
      validate: {
        len: [1, 200]
      }
    },
    currencyCode: {
      type: DataTypes.STRING(5),
      allowNull: false,
      set(val) {
        this.setDataValue('currencyCode', val.toUpperCase());
      },
      validate: {
        len: [1, 5]
      }
    },
    languageCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      validate: {
        len: [1, 2]
      },
      set(val) {
        this.setDataValue('languageCode', val.toUpperCase());
      }
    },
    languageName: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    flag: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    callingCode: {
      type: DataTypes.STRING(5),
      allowNull: true
    }
  }, {
    timestamps: false
  });
  
  DirectoryCountry.associate = function(models) {
    DirectoryCountry.belongsTo(models.Country);
  };

  return DirectoryCountry;
}
