/**
 * Product Review model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductReview = sequelize.define('ProductReview', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    comment: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    rating: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
  
  ProductReview.associate = function(models) {
    ProductReview.belongsTo(models.Product);
  };
  
  return ProductReview;
}
