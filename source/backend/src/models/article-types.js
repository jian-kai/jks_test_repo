/**
 * Article Type model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ArticleType = sequelize.define('ArticleType', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    type: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    langCode: {
      type: DataTypes.STRING(2),
      allowNull: false,
      set(val) {
        this.setDataValue('langCode', val.toUpperCase());
      }
    }
  }, {
    timestamps: false
  });
  
  ArticleType.associate = function(models) {
    ArticleType.hasMany(models.Article);
    ArticleType.belongsTo(models.Country);
  };
  
  return ArticleType;
}
