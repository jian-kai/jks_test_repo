/**
 * Custom Plan model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var CustomPlan = sequelize.define('CustomPlan', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  });
  
  CustomPlan.associate = function(models) {
    CustomPlan.belongsTo(models.User);
    CustomPlan.belongsTo(models.Country);
    CustomPlan.belongsTo(models.PlanType);
    CustomPlan.hasMany(models.CustomPlanDetail, {as: 'customPlanDetail'});
  };

  return CustomPlan;
}
