/**
 * Product Country model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanCountry = sequelize.define('PlanCountry', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    actualPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    sellPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    savePercent: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    isOnline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isOffline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    pricePerCharge: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
  
  PlanCountry.associate = function(models) {
    PlanCountry.belongsTo(models.Plan);
    PlanCountry.belongsTo(models.Country);
    PlanCountry.hasMany(models.PlanDetail, {as: 'planDetails'});
    PlanCountry.hasMany(models.PlanTrialProduct, {as: 'planTrialProducts'});
    PlanCountry.hasMany(models.PlanOption, {as: 'planOptions'});
  };
  
  return PlanCountry;
}
