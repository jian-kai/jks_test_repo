/**
 * Product Review model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var ProductImage = sequelize.define('ProductImage', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    isDefault: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    timestamps: false
  });
  
  ProductImage.associate = function(models) {
    ProductImage.belongsTo(models.Product);
  };
  
  return ProductImage;
}
