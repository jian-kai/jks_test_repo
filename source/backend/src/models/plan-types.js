/**
 * Plan Type model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanType = sequelize.define('PlanType', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      unique: true,
      validate: {
        len: [1, 200]
      }
    },
    totalDeliverTimes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    subsequentDeliverDuration: {
      type: DataTypes.STRING(15),
      defaultValue: '1 month'
    },
    totalChargeTimes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    subsequentChargeDuration: {
      type: DataTypes.STRING(15),
      defaultValue: '1 month'
    },
    prefix: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: ''
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    timestamps: false
  });
  
  PlanType.associate = function(models) {
    PlanType.hasMany(models.Plan, {as: 'plans'});
    PlanType.hasMany(models.PlanTypeTranslate, {as: 'planTypeTranslate'});
    PlanType.hasMany(models.CustomPlan);
  };

  PlanType.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'createdAt');
    Reflect.deleteProperty(values, 'updatedAt');
    return values;
  };
  
  return PlanType;
}
