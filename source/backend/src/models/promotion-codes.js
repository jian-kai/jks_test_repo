/**
 * PromotionCode model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PromotionCode = sequelize.define('PromotionCode', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    isValid: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  });
  
  PromotionCode.associate = function(models) {
    PromotionCode.belongsTo(models.Promotion);
  };
  
  return PromotionCode;
}
