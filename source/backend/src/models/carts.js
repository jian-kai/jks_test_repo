/**
 * CART model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Cart = sequelize.define('Cart', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }
  });
  
  Cart.associate = function(models) {
    Cart.belongsTo(models.User);
    Cart.hasMany(models.CartDetail, {as: 'cartDetails'});
  };
  
  return Cart;
}
