/**
 * Plan Option model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PlanOption = sequelize.define('PlanOption', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    actualPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    sellPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    pricePerCharge: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
      validate: {
        isDecimal: true
      }
    },
    savePercent: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true
      }
    },
    hasShaveCream: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    timestamps: false
  });
  
  PlanOption.associate = function(models) {
    PlanOption.belongsTo(models.PlanCountry);
    // PlanOption.belongsTo(models.ProductCountry);
    PlanOption.hasMany(models.PlanOptionProduct, {as: 'planOptionProducts'});
    PlanOption.hasMany(models.PlanOptionTranslate, {as: 'planOptionTranslate'});
  };
  
  return PlanOption;
}
