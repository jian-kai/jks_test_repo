/**
 * Promotion product model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var PromotionProduct = sequelize.define('PromotionProduct', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    planIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    productCountryIds: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    timestamps: false
  });
  
  PromotionProduct.associate = function(models) {
    PromotionProduct.belongsTo(models.Promotion);
  };
  
  return PromotionProduct;
}
