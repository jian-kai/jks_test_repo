/**
 * Promotion model
 * @author KimThi
 */
export default function(sequelize, DataTypes) {
  var Promotion = sequelize.define('Promotion', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    discount: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    bannerUrl: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    emailTemplateId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    emailTemplateOptions: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    expiredAt: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: new Date()
    },
    activeAt: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: new Date()
    },
    minSpend: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: true,
      validate: {
        isDecimal: true
      }
    },
    allowMinSpendForTotalAmount: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    maxDiscount: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: true,
      validate: {
        isDecimal: true
      }
    },
    planIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    productCountryIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    planCountryIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    freeProductCountryIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    isGeneric: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    timePerUser: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    totalUsage: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    appliedTo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    isFreeShipping: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isBaApp: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isOnline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isOffline: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });
  
  Promotion.associate = function(models) {
    Promotion.belongsTo(models.Country);
    Promotion.hasMany(models.Order, {as: 'orders'});
    Promotion.hasMany(models.PromotionCode, {as: 'promotionCodes'});
    Promotion.hasMany(models.PromotionTranslate, {as: 'promotionTranslate'});
  };
  
  Promotion.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
    Reflect.deleteProperty(values, 'updatedAt');
    Reflect.deleteProperty(values, 'CountryId');
    return values;
  };

  return Promotion;
}
