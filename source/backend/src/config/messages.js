'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
export default {
  en: {
    authentication: {
      loginFail: 'Email or password do not match',
      emailNotExisted: 'The email address you provided is not associated with an activated Shaves2U account.',
      emailExisted: 'email {{email}} is in use.',
      accountNotActive: 'You account is not activated. Please check your mailbox.'
    },
    promotion: {
      codeInvalid: 'Promo code is invalid',
      usedLimited: 'Promo code is already used.'
    }
  },
  ko: {
    authentication: {
      loginFail: '이메일 또는 비밀번호가 맞지 않습니다',
      emailNotExisted: '입력하신 이메일 주소는 활성화 된 Shaves2U 계정과 관련이 없습니다.',
      emailExisted: '{{email}} 이메일을 사용 중입니다.',
      accountNotActive: '고객님의 계정이 비활성화된 상태입니다.'
    },
    promotion: {
      codeInvalid: '프로모션 코드가 잘못되었습니다.',
      usedLimited: '프로모션 코드가 이미 사용되었습니다.'
    }
  }
};
