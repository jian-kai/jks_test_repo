'use strict';
/*eslint no-process-env:0*/

import path from 'path';
import _ from 'lodash';

/*function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}*/


process.env.NODE_ENV = process.env.NODE_ENV || 'local';
process.env.CONFIG_ENV = process.env.CONFIG_ENV || 'local'; // WORK AROUND

console.log(`process.env.NODE_ENV == ${process.env.NODE_ENV}`);
console.log(`process.env.CONFIG_ENV == ${process.env.CONFIG_ENV}`);

// All configurations will extend these options
// ============================================
let all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(`${__dirname}/../`),

  // Server port
  port: process.env.PORT || 8080,

  // Server IP
  ip: process.env.IP || '0.0.0.0',

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'shave-project'
  },

  // limit http request body
  bodyLimit: '100kb',

  corsHeaders: ['Link']
};

// Export the config object based on the NODE_ENV
// ==============================================

let messages = require('./messages').default;
let baseConfig = require('./environment/base').default;
let envConfig = require(`./environment/${process.env.CONFIG_ENV}.js`).default;
let keyConfig = require(envConfig.configPath);
let checkoutRules = require(envConfig.checkoutRulesPath);

// Config new relic
process.env.NEW_RELIC_NO_CONFIG_FILE = true;
process.env.NEW_RELIC_APP_NAME = envConfig.newrelic.addName;
process.env.NEW_RELIC_LICENSE_KEY = envConfig.newrelic.key; 
process.env.NEW_RELIC_LOG_LEVEL = envConfig.newrelic.logLevel;

console.log(`CONFIGS ===: ${JSON.stringify(envConfig)} == ${JSON.stringify(keyConfig)}`);

export default _.merge(
  all,
  baseConfig,
  require('./environment/shared'),
  envConfig,
  { checkoutRules },
  { messages },
  keyConfig);
