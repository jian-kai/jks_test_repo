'use strict';
/*eslint no-process-env:0*/

// Test specific configuration
// ===========================
export default {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/angularfullstack-test'
  },
  sequelize: {
    uri: 'sqlite://',
    options: {
      logging: false,
      storage: 'test.sqlite',
      define: {
        timestamps: false
      }
    }
  }
};
