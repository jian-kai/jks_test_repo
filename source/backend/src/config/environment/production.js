'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
export default {

  db: {
    host: 'shaves2u.mysql.database.azure.com',
    user: 'shaves2u@shaves2u',
    password: 'Y}K7G{Sd(75/<Vq!',
    database: 'shave2u',
    pool: {
      max: 100,
      min: 0,
      acquire: 60000,
    }
  },

  configPath: '/home/shaves2u/config/config.json',
  checkoutRulesPath: '/home/shaves2u/config/checkout-rules.json',
  webUrl: 'https://shaves2u.com/',
  adminUrl: 'http://52.230.31.233:82/',
  serverUrl: 'https://shaves2u.com/',
  redisConnectionString: 'redis://shaves2u.redis.cache.windows.net:6379',  
  redisDbIndex: 0,
  oneTimeToken: {
    secret: 'peCOytG6QSY=',
    expireIn: 24 * 60 * 60 * 1000
  },

    // API access token configuration
  accessToken: {
    secret: 'peCOytG6QSY=',
    expireIn: 24 * 60 * 60 * 1000
  },
  // API access token configuration
  AWS: {
    keyPath: '/home/shaves2u/config/aws-key.json',
    userAvatarAlbum: 'user-avatars',
    version: '2012-10-17',
    bucketName: 'smart-shaves-development',
    planPanelAlbum: 'plan-panel',
    productImagesAlbum: 'product-images',
    planImagesAlbum: 'plan-images',
    promotionImagesAlbum: 'promotion-images',
    filesUploadAlbum: 'files-upload-production',
    localDownloadFolder: '/home/shaves2u/aws-output-files',
    limitDownload: 2000,
  },
  email: {
    expiredTime: 24 * 60 * 60 * 1000,
    singleListId: '2244989605',
    registerListId: 'a319039f76',
    subscriberListId: '52973b4ecb',
    trialSubscriberList: 'a45289f880',
    customPlanList: '1a9cb4162f',
    newCustomerEmailForKoreaList: '4700479ad7',
    replyToEmail: 'no-reply@shaves2u.com',
    fromEmail: 'no-reply@shaves2u.com',
    fromName: 'No-reply shaves2u',
    ssAdminEmail: 'no-reply@shaves2u.com',
    baseTmpl: 33457,
    activeAccountTmpl: 24521,
    accountActivedTmpl: 29269,
    resetPasswordTmpl: 24573,
    promotionTmpl: 24921,
    receiptTmpl: 30385,
    cancelOrderList: [
      'help@shaves2u.com',
      'shipping@shaves2u.com',
      'warehouse@shaves2u.com'
    ],
    cancelTrialCSList: [
      'help@shaves2u.com',
      'shipping@shaves2u.com'
    ],
    SubscribersReportList: [
      'desmond@dci-digital.com',
      'jason@dci-digital.com',
      'vietsoon@dci-digital.com'
    ],
    cronjobUsersSaleReportEmail: [
      'desmond@dci-digital.com',
      'jason@dci-digital.com',
      'vietsoon@dci-digital.com'
    ],
    cronjobUsersReportEmail: [
      'desmond@dci-digital.com',
      'jason@dci-digital.com',
      'vietsoon@dci-digital.com',
      'chantal@dci-digital.com',
      'Halah.Alhajahjah@shaves2u.com'
    ],
    cronjobSubscribersSaleReportConfig: {
      emails: [
        'desmond@dci-digital.com',
        'jason@dci-digital.com',
        'vietsoon@dci-digital.com'
      ],
      fromDate: '2018-04-16'
    },
    studentPromoCodeReportEmail: [
      'Halah.Alhajahjah@shaves2u.com',
      'desmond@dci-digital.com',
      'jason@dci-digital.com',
      'vietsoon@dci-digital.com',
      'chantal@dci-digital.com'
    ],
    saleReportEmail: {
      master: [
        'help@shaves2u.com',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      appcoMYS: [
        'AppcoReport_MY@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      appcoSGP: [
        'AppcoReport_SG@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moIM: [
        'MOReport_IM@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moLO: [
        'MOReport_LO@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moBO: [
        'MOReport_BO@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moCAM: [
        'MOReport_CAM@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moOPS: [
        'MOReport_OPS@appcogroup.asia',
        'desmond@dci-digital.com',
        'chantal@dci-digital.com'
      ],
      moAP: [
        'Sherry@appcogroup.asia',
        'Megan.balyk@appcogroup.asia',
        'desmond@dci-digital.com',
        'jason@dci-digital.com',
        'vietsoon@dci-digital.com',
        'chantal@dci-digital.com'
      ]
    }
  },
  scheduler: {
    short: 1000,
    normal: 30 * 1000,
    long: 60 * 1000,
    min2: 2 * 60 * 1000,
    min3: 3 * 60 * 1000,
    min10: 10 * 60 * 1000,
    min20: 20 * 60 * 1000,
    everyDay: 24 * 60 * 60 * 1000,
    everyWeek: 7 * 24 * 60 * 60 * 1000
  },
  ipay88: {
    responseURL: 'https://shaves2u.com/payment-gateway/responseUrlHandler',
    backendURL: 'https://shaves2u.com/payment-gateway/backendUrlHandler'
  },
  ongroundApp: {
    localBadgePath: '/home/ss-system/ftp/sales-app-ba/',
    ftpBadgePath: '/sales-app-ba'
  },
  warehouse: {
    uploadFolder: '/home/shaves2u/warehouse-upload-files',
    ftpUploadFolder: '/shaves01-my-sftp-prod/outbox',
    ftpResultFolder: '/shaves01-my-sftp-prod/inbox',
    localDownloadFolder: '/home/shaves2u/warehouse-output-files',
    retry: 5,
    maxFiles: 10
  },
  aftership: {
    trackingUrl: 'https://shaves2u.aftership.com',
    originUrl: ''
  },
  saleApp: {
    discountPercent: 10,
    maxTimes: 1000
  },
  newrelic: {
    addName: 'Shaves2u-production',
    key: 'd4070ece4eed72cc9cf648d6772845ab2723d04f',
    logLevel: 'info'
  },
  trialPlan: {
    baFirstDelivery: '13 days',
    webFirstDelivery: '20 days',
    startFirstDelivery: '13 days',
    followUpEmail1: '10 days',
    followUpEmail2: '7 days',
    followUpEmail5Days: '5 days'
  },
  taxInvoicePath: '/home/shaves2u/taxInvoice-upload-files',
  warehouseInfo: {
    name: 'S2u',
    companyName: 'S2u',
    contactNumber: '841659612968',
    email: 'kimtn129@gmail.com',
    country: 'Singapore',
    postCode: '550000',
    address: '128 Hillview Ave, Singapore 669595'
  },
  urbanFox: {
    host: 'https://ims.urbanfox.asia/graphiql',
    trackingUrl: 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number='
  },
  deliverDateForKorea: '2018-10-01'
};

