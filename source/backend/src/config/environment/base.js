'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
export default {
  groupPlanTrial: {
    S3: {
      description: 'Men’s',
      image: 'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/catridge-s3.png'
    },
    S5: {
      description: 'Men’s',
      image: 'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/catridge-s5.png'
    },
    S6: {
      description: 'Men’s',
      image: 'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/catridge-s6.png'
    },
    F5: {
      description: 'Women’s',
      image: 'https://d8pi1a7qa1dfv.cloudfront.net/assets/images/catridge-f5.png'
    },
  },
  shippingFee: {
    MYS: {
      minAmount: 75,
      fee: 6.5,
      trialFee: 6.5
    },
    SGP: {
      minAmount: 30,
      fee: 5,
      trialFee: 5
    },
    HKG: {
      minAmount: 0,
      fee: 0
    },
    KOR: {
      minAmount: 0,
      fee: 0
    }
  },
  countryStates: {
    MYS: [
      {
        name: 'Selangor'
      },
      {
        name: 'Kuala Lumpur',
        isDefault: true
      },
      {
        name: 'Sarawak'
      },
      {
        name: 'Johor'
      },
      {
        name: 'Pulau Pinang'
      },
      {
        name: 'Sabah'
      },
      {
        name: 'Perak'
      },
      {
        name: 'Pahang'
      },
      {
        name: 'Negeri Sembilan'
      },
      {
        name: 'Kedah'
      },
      {
        name: 'Melaka'
      },
      {
        name: 'Terengganu'
      },
      {
        name: 'Kelantan'
      },
      {
        name: 'Labuan'
      },
      {
        name: 'Perlis'
      }
    ],
    SGP: [
      {
        name: 'Singapore',
        isDefault: true
      }
    ]
  },
  productPanel: {
    MYS: {
      title: 'Get a taste of Awesome',
      description: 'Start your Shaves2U journey with our Awesome Shave Kit at only {{price}}!',
      productCountryId: 5,
      planCountryId: null 
    },
    SGP: {
      title: 'Get a taste of Awesome',
      description: 'Start your Shaves2U journey with our Awesome Shave Kit at only {{price}}!',
      productCountryId: 16,
      planCountryId: null
    }
  },
  appSEO: [
    {
      url: '/shave-plans/custom-plan',
      title: 'Shaves2U | Custom Shave Plan',
      description: 'Tailor your Shave Plan the way you like it! Choose from 3, 5 or 6-blade razors, after shave cream, and your preferred frequency.'
    },
    {
      url: '/shave-plans/free-trial',
      title: 'Shaves2U | Select A Shave Plan',
      description: 'What\'s your shaver flavour? 3, 5 or even 6-blade razors? Take your pick, and choose your payment plan. Easy peasy!'
    },
    {
      url: '/shave-plans',
      title: 'Shaves2U | Shave Plans',
      description: 'Customise your shave plan with Shaves2U. You can change or cancel the plan at anytime. Sign up for free trial and get our quality shave delivered to your door.'
    },
    {
      url: '/plans',
      title: 'Shaves2U | Select A Shave Plan',
      description: 'What\'s your shaver flavour? 3, 5 or even 6-blade razors? Take your pick, and choose your payment plan. Easy peasy!'
    },
    {
      url: '/products/H1S3-2018',
      title: 'Premium Handle with 3-Blade Razor Cartridge | Shaves2U',
      description: 'Shaves2U Men\'s Premium Handle with 3-Blade is perfect for shaping facial hair. Subscribe to our shave plan to get a free trial now.'
    },
    {
      url: '/products/H1S5',
      title: 'Premium Handle with 5-Blade Razor Cartridge | Shaves2U',
      description: 'Shaves2U Men\'s Premium Handle with 5-Blade is ideal for those who don\'t shave daily. Sign up for free trial and experience the difference now.'
    },
    {
      url: '/products/H1S6-2018',
      title: 'Premium Handle with 6-Blade Razor Cartridge | Shaves2U',
      description: 'Shaves2U Men\'s Premium Handle with 6-Blade is ideal for hairy guys who shave daily. Sign up for free trial or customise your own shave plan.'
    },
    {
      url: '/products/H3S6-2018',
      title: 'Men\'s Swivel Handle with 6-Blade Razor Cartridge | Shaves2U',
      description: 'Shaves2U Men\'s Swivel Handle with 6-Blade is ideal for guys who just shave everything off. Sign up to get your free trial and experience the difference.'
    },
    {
      url: '/products/mens_3blade_cartridge_pack_2',
      title: 'Shaves2U | 3-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 3-blade razor cartridges.'
    },
    {
      url: '/products/mens_3blade_cartridge_pack_2018',
      title: '3-Blade Razor Cartridge Pack | Shaves2U',
      description: 'Shaves2U 3-Blade Cartridge Pack is ideal for upper lip and chin areas. Discover a new experience in men\'s grooming. Sign up for free trial now!'
    },
    {
      url: '/products/womens_starter_pack_5',
      title: 'Shaves2U | Women\'s Starter Pack',
      description: 'The perfect place to start - try out the 5-blade cartridges.'
    },
    {
      url: '/products/womens_starter_pack_4',
      title: 'Women\'s Starter Pack | Shaves2U',
      description: 'Women\'s Started Pack includes a women\'s razor handle and 2 women\'s 5-blade cartridges. Ladies, purchase now and feel the difference.'
    },
    {
      url: '/products/women\'s_5blade_cartridge_pack_7',
      title: 'Shaves2U | Women\'s 5-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 5-blade razor cartridges.'
    },
    {
      url: '/products/womens_razor_handle_6',
      title: 'Shaves2U | Women\'s Razor Handle',
      description: 'The handle, for her. Its soft ruberised grip and ergonomic handle allows for a smooth shave.'
    },
    {
      url: '/products/men\'s_premium_handle_0',
      title: 'Shaves2U | Men\'s Premium Handle',
      description: 'The Premium Handle. Keep it simple, with this beautifully crafted, weighty metal handle.'
    },
    {
      url: '/products/men\'s_swivel_handle_23',
      title: 'Shaves2U | Men\'s Swivel Handle',
      description: 'The Swivel Handle. Achieve perfect manoeuvrability with its rubberised grip.'
    },
    {
      url: '/products/womens_razor_handle_7',
      title: 'Women\'s Premium Handle | Shaves2U',
      description: 'Women\'s premium handle comes with soft rubberised grip with precision balance and a convenient push button release. Click here to buy and feel the difference.'
    },
    {
      url: '/products/mens_starter_pack_3',
      title: 'Shaves2U | Men\'s Starter Pack',
      description: 'The perfect place to start for the perfect shave if you\'re on the fence. You get to try out both, the 3 and 5-blade cartridges to see which suits you best.'
    },
    {
      url: '/products/womens_starter_pack_4',
      title: 'Shaves2U | Women\'s Starter Pack',
      description: 'The perfect place to start - try out the 5-blade cartridges.'
    },
    {
      url: '/products/awesome_shave_kit_5',
      title: 'Awesome Shaving Kit | Shaves2U',
      description: 'Shaves2U Awesome Shaving Kit includes a premium handle, a 3-blade cartridge, a 5-blade cartridge, a shaving cream and an aftershave cream. Sign up now!'
    },
    {
      url: '/products/mens_3blade_cartridge_pack_1',
      title: 'Shaves2U | 3-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 3-blade razor cartridges.'
    },
    {
      url: '/products/mens_5blade_cartridge_pack_2',
      title: 'Shaves2U | 5-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 5-blade razor cartridges.'
    },
    {
      url: '/products/mens_6blade_cartridge_pack_22',
      title: 'Shaves2U | 6-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 6-blade razor cartridges.'
    },
    {
      url: '/products/mens_6blade_cartridge_pack_2018',
      title: '6-Blade Razor Cartridge Pack | Shaves2U',
      description: 'Shaves2U 6-Blade Cartridge Pack is made with our patented I.C.E. Diamond Coating for durability and sharpness. Purchase and experience the difference now.'
    },
    {
      url: '/products/women\'s_5blade_cartridge_pack_6',
      title: 'Women\'s 5-Blade Razor Cartridge Pack | Shaves2U',
      description: 'Women\'s 5-Blade cartridges Pack includes 4 women\'s 5-blade cartridges that come with a larger aloe vera lubricating strip to soothe sensitive skin. Purchase now!'
    },
    {
      url: '/products/mens_5blade_cartridge_pack_3',
      title: 'Shaves2U | 5-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 5-blade razor cartridges.'
    },
    {
      url: '/products/mens_5blade_cartridge_pack_2018',
      title: '5-Blade Razor Cartridge Pack | Shaves2U',
      description: 'Shaves2U 5-Blade Cartridge Pack is features with Accublade® Trimmer system for goatee and sideburn shaving needs. Purchase and experience the difference now.'
    },
    {
      url: '/products/women\'s_5blade_cartridge_pack_7',
      title: 'Shaves2U | Women\'s 5-Blade Razor',
      description: 'Razor sharp yet affordable. 1 cartridge pack includes 4 5-blade razor cartridges.'
    },
    {
      url: '/products/shaving_foam_9',
      title: 'Shaves2U | Shaving Foam',
      description: 'Quality skin care provided by Arko'
    },
    {
      url: '/products/after_shave_cream_11',
      title: 'After Shave Cream | Shaves2U',
      description: 'Shaves2U Aftershave Cream replenishes skin\'s lost moisture with conditioning ingredients. Subscribe to our shave plan now to get a free trial.'
    },
    {
      url: '/products/shaving_soap_18',
      title: 'Shaving Soap | Shaves2U',
      description: 'Shaves2U shaving soap has a rich lather to help protect and hydrates the skin. Subscribe to our shave plan to get a free trial now.'
    },
    {
      url: '/products/shaving_gel_10',
      title: 'Shaves2U | Shaving Gel',
      description: 'Quality skin care provided by Arko'
    },
    {
      url: '/products/shave_cream_24',
      title: 'Shaving Cream | Shaves2U',
      description: 'Start to shave with Shaves2U Shave Cream to prevent redness. Sign up and get a free trial that includes a premium handle with blade cartridge and a shave cream.'
    },
    {
      url: '/products/shave-cream',
      title: 'Shaves2U | Shave Cream',
      description: 'Quality skin care provided by Arko. Pair your shave with the perfect Shaving Cream.'
    },
    {
      url: '/products',
      title: 'Shaver, Razor, Handle, Cream & Shaving Products | Shaves2U',
      description: 'Get a better shave today! Shop Shaves2U razors, handles, cartridges, shaving kit, shaving cream, aftershave cream, shaving soap and customise shave plan now.'
    },
    {
      url: '/login',
      title: 'Shaves2U | Welcome',
      description: 'Log in now or create a new account'
    },
    {
      url: '/cart',
      title: 'Shaves2U | Cart',
      description: 'Review your shopping cart'
    },
    {
      url: '/checkout',
      title: 'Shaves2U | Check Out',
      description: 'Check out your shaving products'
    },
    {
      url: '/help',
      title: 'Help | Shaves2U',
      description: 'Please do not hesitate to contact us if you have any question. We are always here to help you. Call us at 03-27732882 or email us at help.my@shaves2u.com'
    },
    {
      url: '/terms-conditions',
      title: 'Terms & Conditions | Shaves2U',
      description: 'Read terms and conditions on Shaves2U. Additional terms and conditions may be subjected from time to time. Visit here to learn more.'
    },
    {
      url: '/privacy-policy',
      title: 'Privacy Policy | Shaves2U',
      description: 'The privacy policy describes the policies and procedures of Shaves2U. Please read our privacy policy carefully. Visit here to learn more.'
    }
  ],

  countryCodes: {
    AFG: 'AF',
    ALA: 'AX',
    ALB: 'AL',
    DZA: 'DZ',
    ASM: 'AS',
    AND: 'AD',
    AGO: 'AO',
    AIA: 'AI',
    ATA: 'AQ',
    ATG: 'AG',
    ARG: 'AR',
    ARM: 'AM',
    ABW: 'AW',
    AUS: 'AU',
    AUT: 'AT',
    AZE: 'AZ',
    BHS: 'BS',
    BHR: 'BH',
    BGD: 'BD',
    BRB: 'BB',
    BLR: 'BY',
    BEL: 'BE',
    BLZ: 'BZ',
    BEN: 'BJ',
    BMU: 'BM',
    BTN: 'BT',
    BOL: 'BO',
    BES: 'BQ',
    BIH: 'BA',
    BWA: 'BW',
    BVT: 'BV',
    BRA: 'BR',
    IOT: 'IO',
    UMI: 'UM',
    VIR: 'VI',
    BRN: 'BN',
    BGR: 'BG',
    BFA: 'BF',
    BDI: 'BI',
    KHM: 'KH',
    CMR: 'CM',
    CAN: 'CA',
    CPV: 'CV',
    CYM: 'KY',
    CAF: 'CF',
    TCD: 'TD',
    CHL: 'CL',
    CHN: 'CN',
    CXR: 'CX',
    CCK: 'CC',
    COL: 'CO',
    COM: 'KM',
    COG: 'CG',
    COD: 'CD',
    COK: 'CK',
    CRI: 'CR',
    HRV: 'HR',
    CUB: 'CU',
    CUW: 'CW',
    CYP: 'CY',
    CZE: 'CZ',
    DNK: 'DK',
    DJI: 'DJ',
    DMA: 'DM',
    DOM: 'DO',
    ECU: 'EC',
    EGY: 'EG',
    SLV: 'SV',
    GNQ: 'GQ',
    ERI: 'ER',
    EST: 'EE',
    ETH: 'ET',
    FLK: 'FK',
    FRO: 'FO',
    FJI: 'FJ',
    FIN: 'FI',
    FRA: 'FR',
    GUF: 'GF',
    PYF: 'PF',
    GAB: 'GA',
    GMB: 'GM',
    GEO: 'GE',
    DEU: 'DE',
    GHA: 'GH',
    GIB: 'GI',
    GRC: 'GR',
    GRL: 'GL',
    GRD: 'GD',
    GLP: 'GP',
    GUM: 'GU',
    GTM: 'GT',
    GGY: 'GG',
    GIN: 'GN',
    GNB: 'GW',
    GUY: 'GY',
    HTI: 'HT',
    HMD: 'HM',
    VAT: 'VA',
    HND: 'HN',
    HKG: 'HK',
    HUN: 'HU',
    ISL: 'IS',
    IND: 'IN',
    IDN: 'ID',
    CIV: 'CI',
    IRN: 'IR',
    IRQ: 'IQ',
    IRL: 'IE',
    IMN: 'IM',
    ISR: 'IL',
    ITA: 'IT',
    JAM: 'JM',
    JPN: 'JP',
    JEY: 'JE',
    JOR: 'JO',
    KAZ: 'KZ',
    KEN: 'KE',
    KIR: 'KI',
    KWT: 'KW',
    KGZ: 'KG',
    LAO: 'LA',
    LVA: 'LV',
    LBN: 'LB',
    LSO: 'LS',
    LBR: 'LR',
    LBY: 'LY',
    LIE: 'LI',
    LTU: 'LT',
    LUX: 'LU',
    MAC: 'MO',
    MKD: 'MK',
    MDG: 'MG',
    MWI: 'MW',
    MYS: 'MY',
    MDV: 'MV',
    MLI: 'ML',
    MLT: 'MT',
    MHL: 'MH',
    MTQ: 'MQ',
    MRT: 'MR',
    MUS: 'MU',
    MYT: 'YT',
    MEX: 'MX',
    MDA: 'MD',
    MCO: 'MC',
    MNG: 'MN',
    MNE: 'ME',
    MSR: 'MS',
    MAR: 'MA',
    MOZ: 'MZ',
    MMR: 'MM',
    NAM: 'NA',
    NRU: 'NR',
    NPL: 'NP',
    NLD: 'NL',
    NCL: 'NC',
    NZL: 'NZ',
    NIC: 'NI',
    NER: 'NE',
    NGA: 'NG',
    NIU: 'NU',
    NFK: 'NF',
    PRK: 'KP',
    MNP: 'MP',
    NOR: 'NO',
    OMN: 'OM',
    PAK: 'PK',
    PSE: 'PS',
    PAN: 'PA',
    PNG: 'PG',
    PRY: 'PY',
    PER: 'PE',
    PHL: 'PH',
    PCN: 'PN',
    POL: 'PL',
    PRT: 'PT',
    PRI: 'PR',
    QAT: 'QA',
    KOS: 'XK',
    REU: 'RE',
    ROU: 'RO',
    RUS: 'RU',
    RWA: 'RW',
    BLM: 'BL',
    SHN: 'SH',
    KNA: 'KN',
    LCA: 'LC',
    MAF: 'MF',
    SPM: 'PM',
    VCT: 'VC',
    WSM: 'WS',
    SMR: 'SM',
    STP: 'ST',
    SAU: 'SA',
    SEN: 'SN',
    SRB: 'RS',
    SYC: 'SC',
    SLE: 'SL',
    SGP: 'SG',
    SXM: 'SX',
    SVK: 'SK',
    SVN: 'SI',
    SLB: 'SB',
    SOM: 'SO',
    ZAF: 'ZA',
    SGS: 'GS',
    KOR: 'KR',
    SSD: 'SS',
    ESP: 'ES',
    LKA: 'LK',
    SDN: 'SD',
    SUR: 'SR',
    SJM: 'SJ',
    SWZ: 'SZ',
    SWE: 'SE',
    CHE: 'CH',
    SYR: 'SY',
    TWN: 'TW',
    TJK: 'TJ',
    TZA: 'TZ',
    THA: 'TH',
    TLS: 'TL',
    TGO: 'TG',
    TKL: 'TK',
    TON: 'TO',
    TTO: 'TT',
    TUN: 'TN',
    TUR: 'TR',
    TKM: 'TM',
    TCA: 'TC',
    TUV: 'TV',
    UGA: 'UG',
    UKR: 'UA',
    ARE: 'AE',
    USA: 'US',
    URY: 'UY',
    UZB: 'UZ',
    VUT: 'VU',
    VEN: 'VE',
    VNM: 'VN',
    WLF: 'WF',
    ESH: 'EH',
    YEM: 'YE',
    ZMB: 'ZM',
    ZWE: 'ZW'
  }
};
