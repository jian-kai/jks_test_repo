'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
export default {

  db: {
    host: 'localhost',
    user: 'shave2u',
    password: 'Admin@123',
    database: 'shave2u',
    pool: {
      max: 100,
      min: 0,
      idle: 10000,
      acquire: 10000,
    }
  },

  configPath: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/config/config.json',
  checkoutRulesPath: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/config/checkout-rules.json',
  webUrl: 'http://128.199.201.237:8080/',
  adminUrl: 'http://128.199.201.237:9000/',
  serverUrl: 'http://128.199.201.237:8080/',
  redisConnectionString: 'redis://127.0.0.1:6379',
  redisDbIndex: 0,
  oneTimeToken: {
    secret: 'peCOytG6QSY=',
    expireIn: 24 * 60 * 60 * 1000
  },

    // API access token configuration
  accessToken: {
    secret: 'peCOytG6QSY=',
    expireIn: 24 * 60 * 60 * 1000
  },
  // API access token configuration
  AWS: {
    keyPath: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/config/aws-key.json',
    userAvatarAlbum: 'user-avatars',
    version: '2012-10-17',
    bucketName: 'smart-shaves-development',
    planPanelAlbum: 'plan-panel',
    productImagesAlbum: 'product-images',
    planImagesAlbum: 'plan-images',
    promotionImagesAlbum: 'promotion-images',
    filesUploadAlbum: 'files-upload-development',
    localDownloadFolder: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/aws-output-files',
    limitDownload: 2000,
  },
  email: {
    expiredTime: 24 * 60 * 60 * 1000,
    singleListId: '4c7c2e0680',
    registerListId: '9ca4e13452',
    subscriberListId: '551d06a9be',
    trialSubscriberList: '24cef6bc70',
    customPlanList: '70156695d9',
    newCustomerEmailForKoreaList: '4700479ad7',
    replyToEmail: 'kim129@gmail.com',
    fromEmail: 'smartshave@bjdev.net',
    fromName: 'Smart Shave',
    ssAdminEmail: 'kim@bonseyjaden.com',
    baseTmpl: 109311,
    resetPasswordTmpl: 24573,
    promotionTmpl: 24921,
    cancelOrderList: [
      'kim@bonseyjaden.com',
    ],
    cancelTrialCSList: [
      'kim@bonseyjaden.com',
      'ngoclan@bonseyjaden.com'
    ],
    SubscribersReportList: [
      // abc@abc.com
    ],
    cronjobUsersSaleReportEmail: [
      'phu@bonseyjaden.com',
      'thien@bonseyjaden.com'
    ],
    cronjobUsersReportEmail: [
      // abc@abc.com
    ],
    cronjobSubscribersSaleReportConfig: {
      emails: [
        'thien@bonseyjaden.com',
        'phu@bonseyjaden.com'
      ],
      fromDate: '2018-04-16'
    },
    studentPromoCodeReportEmail: [
      'thien@bonseyjaden.com',
      'phu@bonseyjaden.com'
    ],
    saleReportEmail: {
      master: [
        'ngoclan@bonseyjaden.com'
      ],
      appcoMYS: [
        'ngoclan@bonseyjaden.com'
      ],
      appcoSGP: [
        'ngoclan@bonseyjaden.com'
      ],
      moIM: [
        'ngoclan@bonseyjaden.com'
      ],
      moLO: [
        'ngoclan@bonseyjaden.com'
      ],
      moBO: [
        'ngoclan@bonseyjaden.com'
      ],
      moCAM: [
        'ngoclan@bonseyjaden.com'
      ],
      moOPS: [
        'ngoclan@bonseyjaden.com'
      ],
      moAP: [
        'ngoclan@bonseyjaden.com'
      ]
    }
  },
  scheduler: {
    short: 1000,
    normal: 30 * 1000,
    long: 60 * 1000,
    min2: 2 * 60 * 1000,
    min3: 3 * 60 * 1000,
    min10: 10 * 60 * 1000,
    min20: 20 * 60 * 1000,
    everyDay: 24 * 60 * 60 * 1000,
    everyWeek: 7 * 24 * 60 * 60 * 1000
  },
  ipay88: {
    responseURL: 'http://128.199.201.237:8080/payment-gateway/responseUrlHandler',
    backendURL: 'http://128.199.201.237:8080/payment-gateway/backendUrlHandler'
  },
  ongroundApp: {
    localBadgePath: '/home/ss-system/ftp/sales-app-ba/',
    ftpBadgePath: '/badge-upload-file'
  },
  warehouse: {
    uploadFolder: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/warehouse-upload-files',
    ftpUploadFolder: '/shaves01-my-sftp-test/outbox',
    ftpResultFolder: '/shaves01-my-sftp-test/inbox',
    localDownloadFolder: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/warehouse-output-files',
    retry: 5,
    maxFiles: 10
  },
  aftership: {
    trackingUrl: 'https://shaves2u.aftership.com',
    originUrl: ''
  },
  saleApp: {
    discountPercent: 10,
    maxTimes: 1000
  },
  newrelic: {
    addName: 'Shaves2u-develop',
    key: 'd4070ece4eed72cc9cf648d6772845ab2723d04f',
    logLevel: 'info'
  },
  trialPlan: {
    baFirstDelivery: '13 days',
    webFirstDelivery: '20 days',
    startFirstDelivery: '13 days',
    followUpEmail1: '10 days',
    followUpEmail2: '7 days',
    followUpEmail5Days: '5 days'
  },
  taxInvoicePath: '/Volumes/Transcend/project/bonsey-jaden/shaves2u/source/backend/src/taxInvoice-upload-files',
  warehouseInfo: {
    name: 'S2u',
    companyName: 'S2u',
    contactNumber: '841659612968',
    email: 'kimtn129@gmail.com',
    country: 'Singapore',
    postCode: '550000',
    address: '128 Hillview Ave, Singapore 669595'
  },
  urbanFox: {
    host: 'https://staging-ims.urbanfox.asia/graphiql',
    trackingUrl: 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number='
  },
  deliverDateForKorea: '2018-10-01'
};

