import Sequelize from 'sequelize';
import fs from 'fs';
import path from 'path';
import config from './config';

process.env.CONFIG_ENV = process.env.CONFIG_ENV || 'local';

function readFilePromise() {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/config/certs/BaltimoreCyberTrustRoot.crt.pem`, (err, data) => {
      if(err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export default function() {
  return readFilePromise()
    .then(ca => {
      let db = {};
      const MODEL_PATH = `${__dirname}/models`;
      let connectionConfig = {
        host: config.db.host,
        dialect: 'mysql',
        pool: config.db.pool,
        define: {
          timestamps: true
        }
      }
      if(process.env.CONFIG_ENV === 'production') {
        connectionConfig.dialectOptions = {
          encrypt: true,
          ssl: {
            ca
          }
        }
      }
      let sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, connectionConfig);
      return new Promise((resolved, reject) => {
        fs.readdir(MODEL_PATH, (err, files) => {
          if(err) {
            reject(err);
          } else {
            // fetch all files in models folder
            files.forEach(file => {
              if(file.indexOf('.') !== 0 && file !== 'index.js' && file.slice(-3) === '.js') {
                let model = sequelize.import(path.join(`${__dirname}/models`, file));
                db[model.name] = model;
              }
            });
    
            // make constrain for model
            Object.keys(db).forEach(function(modelName) {
              if('associate' in db[modelName]) {
                db[modelName].associate(db);
              }
            });
    
            db.sequelize = sequelize;
            db.Sequelize = Sequelize;
            resolved(db);
          }
        });
      });
    });
}
