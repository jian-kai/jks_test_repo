import SendEmailQueueService from '../services/send-email.queue.service';
import config from '../config';
import OrderHelper from '../helpers/order-helper';
import SequelizeHelper from '../helpers/sequelize-helper';
import DateTimeHelper from '../helpers/datetime-helpers';

class AftershipWebhook {

  constructor(api, db) {
    this.api = api;
    this.db = db;
    this.registerRoute();
  }

  registerRoute() {
    this.api.post('/aftership', this.postUpdateOrder.bind(this));
  }

  postUpdateOrder(req, res) {
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    // check origin IP
    let params = req.body;
    let order;
    let key = req.query.secretKey;
    console.log(`aftership hook : ${JSON.stringify(params)} : ${key}`);
    // check event name
    if(key === config.keyAftership.secretKey && params.event === 'tracking_update' && params.msg && params.msg.tag === 'Delivered' && params.msg.tracking_number) {
      // find order fit with this hook 
      // check order type
      if(OrderHelper.checkOrderNumberType(params.msg.order_id) === 'order') {
        this.db.Order.findOne({where: {deliveryId: params.msg.tracking_number}, include: ['Country']})
          .then(_order => {
            order = _order;
            return this.db.Order.update({states: 'Completed'}, {where: {id: order.id}});
          })
          .then(() => {
            if(order) {
              return this.db.OrderHistory.create({
                OrderId: order.id,
                message: 'Completed'
              });
            }
          })
          .then(() => {
            if(order) {
              if(order.subscriptionIds) {
                return this.db.Subscription.findAll({where: {
                  id: {$in: JSON.parse(order.subscriptionIds)}}
                })
                .then(subscriptions => {
                  subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
                  if(subscriptions[0].isTrial) {
                    // send email update order status
                    return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
                  } else {
                    // send email update order status
                    return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
                  }
                });
              } else {
                // send email update order status
                return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: order.Country.defaultLang.toLowerCase()});
              }
            }
          })
          .then(() => {
            // update subscription last delivery date
            let arrPatt = new RegExp(/\[([^,]+,?)+\]/g);
            if(order && order.subscriptionIds && arrPatt.test(order.subscriptionIds)) {
              return this.db.Subscription.update({lastDeliverydate: new Date()}, {where: {id: {$in: JSON.parse(order.subscriptionIds)}}})
              .then(() => {
                // update subscription start delivery date
                let startDeliverDate = DateTimeHelper.getNextWorkingDays(config.trialPlan.startFirstDelivery);
                return this.db.Subscription.update(
                  {
                    nextDeliverDate: startDeliverDate,
                    nextChargeDate: startDeliverDate,
                    startDeliverDate,
                  },
                  {
                    where: {
                      id: {$in: JSON.parse(order.subscriptionIds)},
                      startDeliverDate: null
                    }
                  }
                );
              });
            }
          })
          .then(() => res.json({ok: true}))
          .catch(err => {
            console.log(`Aftership webhook error ${err}`);
            res.json({ok: false});
          });
      } else {
        this.db.BulkOrder.findOne({where: {deliveryId: params.msg.tracking_number}, include: ['Country']})
          .then(_order => {
            order = _order;
            return this.db.BulkOrder.update({states: 'Completed'}, {where: {id: order.id}});
          })
          .then(() => {
            if(order) {
              return this.db.BulkOrderHistory.create({
                BulkOrderId: order.id,
                message: 'Completed'
              });
            }
          })
          .then(() => res.json({ok: true}))
          .catch(err => {
            console.log(`Aftership webhook wrror ${err}`);
            res.json({ok: false});
          });
      }
    } else {
      res.json({ok: false});
    }
  }
}

export default function(...args) {
  return new AftershipWebhook(...args);
}
