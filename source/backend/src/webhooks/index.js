import { Router } from 'express';
import Aftership from './aftership';

export default ({ db }) => {
  let api = Router();

  // register API
  Aftership(api, db);

  return api;
};
