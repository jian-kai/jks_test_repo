import fs from 'fs';
// import AWS from 'aws-sdk';
import config from '../config';
// import uuidv1 from 'uuid/v1';
// import { resize } from 'easyimage';
import { loggingHelper } from './logging-helper';
import SequelizeHelper from './sequelize-helper';
import { awsHelper } from './aws-helper';
import _ from 'underscore';
import Promise from 'bluebird';

import childProcess from 'child_process';
// import zipdir from 'zip-dir';
// import zip from 'node-zip';
// var zip = new require('node-zip')();

// import path from 'path';

// const albumPattern = /^[0-9a-zA-Z]$/g;

class CompressedFileHelper {
  // constructor() {
  //   AWS.config.loadFromPath(config.AWS.keyPath);
  //   this.S3 = new AWS.S3({
  //     apiVersion: config.AWS.version,
  //     params: {Bucket: config.AWS.bucketName}
  //   });
  // }

  downloadTaxInvoiceBulkOrder(options, db) {
    let fileProcessing;

    return db.FileUpload.findOne({where: {id: options.fileUploadId}})
      .then(data => {
        fileProcessing = SequelizeHelper.convertSeque2Json(data);
        return db.BulkOrder.findAll(options.options);
      })
      .then(result => 
        this.doProcessExportTaxInvoice(fileProcessing, _.pluck(result, 'id'), [], db)
      )
      .catch(error => {
        db.FileUpload.update({states: 'Canceled'}, {where: {id: fileProcessing.id}});
        loggingHelper.log('error', `downloadTaxInvoiceBulkOrder: ${error}`); 
      });
  }

  downloadTaxInvoiceOrder(options, db) {
    let fileProcessing;

    return db.FileUpload.findOne({where: {id: options.fileUploadId}})
      .then(data => {
        fileProcessing = SequelizeHelper.convertSeque2Json(data);
        return db.Order.findAll(options.options);
      })
      .then(result => 
        this.doProcessExportTaxInvoice(fileProcessing, [], _.pluck(result, 'id'), db)
      )
      .catch(error => {
        db.FileUpload.update({states: 'Canceled'}, {where: {id: fileProcessing.id}});
        loggingHelper.log('error', `downloadTaxInvoiceBulkOrder: ${error}`); 
      });
  }

  downloadTaxInvoiceSaleReport(options, db) {
    let fileProcessing;

    return db.FileUpload.findOne({where: {id: options.fileUploadId}})
      .then(data => {
        fileProcessing = SequelizeHelper.convertSeque2Json(data);
        console.log('options.options ==== === === ', options.options);
        return db.SaleReport.findAll(options.options);
      })
      .then(result => 
        this.doProcessExportTaxInvoice(fileProcessing, _.pluck(result, 'bulkOrderId'), _.pluck(result, 'orderId'), db)
      )
      .catch(error => {
        db.FileUpload.update({states: 'Canceled'}, {where: {id: fileProcessing.id}});
        loggingHelper.log('error', `downloadTaxInvoiceSaleReport: ${error}`); 
      });
  }

  doProcessExportTaxInvoice(fileProcessing, bulkOrderIds, orderIds, db) {
    let files;
    let folderName = fileProcessing.name.replace('.tar.gz', '');

    return db.FileUpload.findAll({
      where: {
        $or: [
          {bulkOrderId: {$in: bulkOrderIds}},
          {orderId: {$in: orderIds}}
        ]
      },
      limit: config.AWS.limitDownload
    })
    .then(_files => {
      files = SequelizeHelper.convertSeque2Json(_files);

      // download file from S3
      return Promise.mapSeries(files, file => awsHelper.doDownload(config.AWS.filesUploadAlbum, file.name, folderName))
      .then(() => 
        // compression file
        new Promise((resolve, reject) => 
          childProcess.exec(`cd ${config.AWS.localDownloadFolder} && tar -zcvf '${fileProcessing.name}' '${folderName}'`, {maxBuffer: 10 * 1024 * 1024}, (err, res) => {
            if(err) reject(err);
            resolve(res);
          })
        )

        // let zipFile = new zip();
        // files.forEach(file => {
        //   zipFile.file(file.name, fs.readFileSync(`${config.AWS.localDownloadFolder}/${file.name}`));
        // });
        
        // let data = zipFile.generate({ base64: false, compression: 'DEFLATE' });

        // // it's important to use *binary* encode
        // return fs.writeFileSync(`${config.AWS.localDownloadFolder}/${fileProcessing.name}`, data, 'binary');

        // new Promise((resolve, reject) => 
        //   zipdir(config.AWS.localDownloadFolder, { saveTo: `${config.AWS.localDownloadFolder}/${fileProcessing.name}` }, function(err, buffer) {
        //     if(err) reject(err);
        //     // `buffer` is the buffer of the zipped file
        //     // And the buffer was saved to `~/myzip.zip
        //     resolve(buffer);
        //   })
        // )
      )
      // // delete file after compression
      // .then(() => 
      //   files.forEach(file => {
      //     fs.unlink(`${config.AWS.localDownloadFolder}/${folderName}/${file.name}`, err => {
      //       if(err) throw err;
      //       console.log(`successfully deleted ${config.AWS.localDownloadFolder}/${folderName}/${file.name}`);
      //       return Promise.resolve();
      //     });
      //   })
      // )
      // delete folder after compression
      .then(() => 
        // fs.rmdir(`${config.AWS.localDownloadFolder}/${folderName}`, err => {
        //   if(err) throw err;
        //   console.log(`successfully deleted ${config.AWS.localDownloadFolder}/${folderName}`);
        //   return Promise.resolve();
        // })
        new Promise((resolve, reject) => 
          childProcess.exec(`rm -rf '${config.AWS.localDownloadFolder}/${folderName}'`, {maxBuffer: 10 * 1024 * 1024}, (err, res) => {
            if(err) reject(err);
            console.log(`successfully deleted ${config.AWS.localDownloadFolder}/${folderName}`);
            resolve(res);
          })
        )
      )
      .then(() => {
        // upload file to S3
        let fileUpload = {
          fieldName: 'url',
          originalFilename: fileProcessing.name,
          path: `${config.AWS.localDownloadFolder}/${fileProcessing.name}`,
          name: fileProcessing.name,
          type: 'application/pdf'
        };
        return awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileProcessing.name)
        .then(uploadedFile => {
          fileProcessing.url = uploadedFile.Location.replace(/\+/g, '%20');
          fileProcessing.states = 'Completed';

          // update status for FileUpload
          db.FileUpload.update(fileProcessing, {where: {id: fileProcessing.id}})
          // delete file after upload
          .then(() => 
            fs.unlink(`${config.AWS.localDownloadFolder}/${fileProcessing.name}`, err => {
              if(err) throw err;
              console.log(`successfully deleted ${config.AWS.localDownloadFolder}/${fileProcessing.name}`);
              return Promise.resolve();
            })
          );
        });
      });
    })
    .catch(error => {
      db.FileUpload.update({states: 'Canceled'}, {where: {id: fileProcessing.id}});
      loggingHelper.log('error', `doProcessExportTaxInvoice: ${error}`); 
    });
  }
}

export let compressedFileHelper = new CompressedFileHelper();
