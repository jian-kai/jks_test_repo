import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import moment from 'moment';
import _ from 'underscore';
import config from '../config';
import { ftpHelper } from './warehouse-ftp-helper';
import { aftership } from './aftership';
import OrderHelper from './order-helper';
import SequelizeHelper from './sequelize-helper';
import CancelOrderQueueService from '../services/cancel-order.queue.service';
import SendEmailQueueService from '../services/send-email.queue.service';
import { loggingHelper } from './logging-helper';

let fileCache = {};

function _readFileAsync(fileName, dir) {
  loggingHelper.log('info', '_readFileAsync start');
  loggingHelper.log('info', `param: fileName = ${fileName}, dir = ${dir}`);
  let dirName = dir ? dir : `${__dirname}/../config/warehouse-interface/`;
  return new Promise((resolve, reject) => {
    if(fileCache.fileName) {
      resolve(fileCache.fileName);
    } else {
      fs.readFile(path.resolve(dirName, fileName), (err, data) => {
        if(err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    }
  });
}
function _addSpace(len, value) {
  value = (value) ? value : '';
  for(let i = value.length; i < len; i++) {
    value = `${value} `;
  }
  if(value.length > len) {
    value = value.substring(0, len);
  }
  return value;
}

function _fillZero(len, value) {
  value = (value) ? value : '';
  for(let i = value.length; i < len; i++) {
    value = `0${value}`;
  }
  if(value.length > len) {
    value = value.substring(0, len);
  }
  return value;
}

function _createData(options) {
  loggingHelper.log('info', '_createData start');
  loggingHelper.log('info', `param = ${options}`);
  let returnObj = '';
  return _readFileAsync(options.fileName)
    .then(data => {
      let dataArray = data.toString().split(/\r?\n/);
      dataArray.forEach(object => {
        let values = object.split(',');
        returnObj += _addSpace(values[4], options.data[values[1]]);
      });
      return returnObj;
    });
}

function _readData(options) {
  loggingHelper.log('info', '_readData start');
  loggingHelper.log('info', `param = ${options}`);
  let returnObj = {};
  return _readFileAsync(options.fileName)
    .then(data => {
      let dataArray = data.toString().split(/\r?\n/);
      dataArray.forEach(object => {
        let keys = object.split(',');
        returnObj[keys[1]] = options.values.substr(keys[5] - 1, keys[4]).trim();
      });
      return returnObj;
    });
}

class WareHouseHelper {
  static createOrderInbound(id, db) {
    loggingHelper.log('info', 'createOrderInbound start');
    loggingHelper.log('info', `param: id = ${id}`);
    let data;
    let dataOrderUpload = [];
    // get order infor
    return db.Order.findOne({
      where: {id, states: 'Payment Received'},
      include: [
        'Country',
        {
          model: db.DeliveryAddress,
          include: ['Country']
        },
        {
          model: db.DeliveryAddress,
          as: 'BillingAddress',
          include: ['Country']
        },
        {
          model: db.OrderDetail,
          as: 'orderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: ['Product']
            },
            {
              model: db.PlanCountry,
              include: [
                'Plan',
                {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                },
                {
                  model: db.PlanTrialProduct,
                  as: 'planTrialProducts',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(order => {
        order = SequelizeHelper.convertSeque2Json(order);
        if(!order) {
          throw new Error('skip');
        }
        // build order header data
        let hasPlan = false;
        data = {};
        data.order = order;

        // collect product
        let items = [];
        order.orderDetail.forEach(details => {
          if(details.ProductCountryId && (!order.SellerUserId || (order.SellerUserId && order.subscriptionIds))) {
            let item = _.findWhere(items, {sku: details.ProductCountry.Product.sku});
            if(item) {
              item.qty += details.qty;
              _.reject(items, (obj => obj.sku === item.sku));
              items.push(item);
            } else {
              items.push({
                sku: details.ProductCountry.Product.sku,
                qty: details.qty
              });
            }
          } else if(details.PlanCountryId && !details.PlanCountry.Plan.isTrial) {
            hasPlan = true;
            if(moment(details.startDeliverDate).isBefore(moment().add(2, 'days'))) {
              details.PlanCountry.planDetails.forEach(planDetail => {
                let item = _.findWhere(items, {sku: planDetail.ProductCountry.Product.sku});
                let deliverPlan = JSON.parse(planDetail.deliverPlan);
                if(item) {
                  item.qty += details.qty * deliverPlan['1'];
                  _.reject(items, (obj => obj.sku === item.sku));
                  items.push(item);
                } else {
                  items.push({
                    sku: planDetail.ProductCountry.Product.sku,
                    qty: details.qty * deliverPlan['1']
                  });
                }
              });
            }
          } else if(details.PlanCountryId && details.PlanCountry.Plan.isTrial) {
            if(order.Country.code === 'MYS') {
              hasPlan = true;
            }
            details.PlanCountry.planTrialProducts.forEach(trialProductDetail => {
              items.push({
                sku: trialProductDetail.ProductCountry.Product.sku,
                qty: trialProductDetail.qty
              });
            });
          }
        });

        if(order.SellerUserId && !order.subscriptionIds && !hasPlan) {
          throw new Error('skip');
        } else {
          data.orderHeader = {
            HeaderFlag: 'ORDHD',
            InterfaceActionFlag: 'A',
            StorerKey: 'SHAVES2U',
            ExternOrderKey: OrderHelper.fromatOrderNumber(order, true),
            OrderDate: moment().format('YYYYMMDDHHmmss'),
            DeliveryDate: moment().format('YYYYMMDDHHmmss'),
            C_contact1: order.SellerUserId ? order.fullName : `${order.DeliveryAddress.firstName} ${order.DeliveryAddress.lastName}`,
            C_Address1: order.DeliveryAddress.address.replace(/\n/g, ' '),
            C_City: order.DeliveryAddress.city.replace(/\n/g, ' '),
            // C_State: order.DeliveryAddress.state,
            C_Zip: order.DeliveryAddress.portalCode,
            C_Country: order.DeliveryAddress.Country.name,
            C_ISOCntryCode: order.DeliveryAddress.Country.code,
            C_Phone1: order.SellerUserId ? order.phone : order.DeliveryAddress.contactNumber,
            B_contact1: order.SellerUserId ? order.fullName : `${order.BillingAddress.firstName} ${order.BillingAddress.lastName}`,
            B_Address1: order.BillingAddress.address.replace(/\n/g, ' '),
            B_City: order.BillingAddress.city.replace(/\n/g, ' '),
            // B_State: order.BillingAddress.state,
            B_Zip: order.BillingAddress.portalCode,
            B_Country: order.BillingAddress.Country.name,
            B_ISOCntryCode: order.BillingAddress.Country.code,
            B_Phone1: order.SellerUserId ? order.phone : order.BillingAddress.contactNumber,
            Status: '0',
            Type: '0',
            SOStatus: '0'
          };
        }

        // build order detail
        data.orderDetails = [];
        items.forEach((item, idx) => {
          data.orderDetails.push({
            HeaderFlag: 'ORDDT',
            InterfaceActionFlag: 'A',
            ExternOrderKey: OrderHelper.fromatOrderNumber(order, true),
            ExternLineNo: `${idx + 1}`,
            Sku: item.sku,
            StorerKey: 'SHAVES2U',
            OriginalQty: `${item.qty}`,
            OpenQty: `${item.qty}`,
            ShippedQty: '0',
            UOM: 'EA',
            Lottable03: 'UR'
          });

          dataOrderUpload.push({
            orderNumber: OrderHelper.fromatOrderNumber(order, true),
            orderId: order.orderId ? `${order.orderId}` : `${order.id}`,
            sku: item.sku,
            qty: `${item.qty}`,
            status: order.states,
            region: order.Country ? order.Country.code : order.region
          });
        });
      })
      .then(() => _createData({
        fileName: 'order-inbound/file-header.csv',
        data: {
          TableID: 'WMSORD',
          InterfaceType: 'I',
          CreateDateTime: moment().format('YYYYMMDDHHmmss'),
          ClientID: 'SHAVES2U',
          ClientCountry: 'MY',
          InterfaceName: 'SO Inbound'
        }
      }))
      .then(orderFileHeaderData => {
        data.orderFileHeaderData = orderFileHeaderData;
        return _createData({
          fileName: 'order-inbound/header.csv',
          data: data.orderHeader
        });
      })
      .then(orderHeaderData => {
        data.orderHeaderData = orderHeaderData;
        return Promise.mapSeries(data.orderDetails, details => _createData({
          fileName: 'order-inbound/details.csv',
          data: details
        }));
      })
      .then(orderDetailsData => {
        data.orderDetailsData = orderDetailsData;
        return _createData({
          fileName: 'order-inbound/file-footer.csv',
          data: {
            FileFooter: 'ORDTR',
            TotalRecordsCount: _fillZero(10, `${data.orderDetailsData.length + 1}`)
          }
        });
      })
      .then(orderFooterData => {
        data.orderFooterData = orderFooterData;

        // upload file to warehouse's FTP server
        data.inboundFileName = `WMSORD_${data.order.Country.code}_${moment().format('YYYYMMDDHHmmss')}.txt`;
        dataOrderUpload.forEach(element => {
          element.doFileName = data.inboundFileName;
        });
        let stream = fs.createWriteStream(`${config.warehouse.uploadFolder}/${data.inboundFileName}`, { encoding: 'latin1' });
        stream.write(`${data.orderFileHeaderData}\n`, 'latin1');
        stream.write(`${data.orderHeaderData}\n`, 'latin1');
        data.orderDetailsData.forEach(details => {
          stream.write(`${details}\n`, 'latin1');
        });
        stream.write(`${data.orderFooterData}\n`, 'latin1');
        stream.end();
      })
      .then(() => ftpHelper.putFile(`${config.warehouse.uploadFolder}/${data.inboundFileName}`, `${config.warehouse.ftpUploadFolder}/${data.inboundFileName}`)) // put file to ftp server
      .then(() => db.OrderUpload.bulkCreate(dataOrderUpload)) // insert info into Order Upload
      .then(() => db.Order.update({states: 'Processing'}, {where: {id: data.order.id}})) // update order states to 'Processing'
      .then(() => db.OrderHistory.create({
        OrderId: data.order.id,
        message: 'Processing'
      }))
      .catch(err => {
        console.log(`Create outbound job error ${err.stack}`);
        if(err.message !== 'skip') {
          throw err;
        }
      });
  }

  static createBulkOrderInbound(id, db) {
    loggingHelper.log('info', 'createBulkOrderInbound start');
    loggingHelper.log('info', `param: id = ${id}`);
    let data;
    let dataOrderUpload = [];
    // get order infor
    return db.BulkOrder.findOne({
      where: {id, states: 'Payment Received'},
      include: [
        'Country',
        {
          model: db.DeliveryAddress,
          include: ['Country']
        },
        {
          model: db.DeliveryAddress,
          as: 'BillingAddress',
          include: ['Country']
        },
        {
          model: db.BulkOrderDetail,
          as: 'bulkOrderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: ['Product']
            },
            {
              model: db.PlanCountry,
              include: [
                {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(order => {
        if(!order) {
          throw new Error('skip');
        }
        // build order header data
        data = {};
        data.order = order;

        // collect product
        let items = [];
        order.bulkOrderDetail.forEach(details => {
          if(details.ProductCountryId && !order.SellerUserId) {
            let item = _.findWhere(items, {sku: details.ProductCountry.Product.sku});
            if(item) {
              item.qty += details.qty;
              _.reject(items, (obj => obj.sku === item.sku));
              items.push(item);
            } else {
              items.push({
                sku: details.ProductCountry.Product.sku,
                qty: details.qty
              });
            }
          } else if(details.PlanCountryId) {
            if(moment(details.startDeliverDate).isBefore(moment().add(2, 'days'))) {
              details.PlanCountry.planDetails.forEach(planDetail => {
                let item = _.findWhere(items, {sku: planDetail.ProductCountry.Product.sku});
                let deliverPlan = JSON.parse(planDetail.deliverPlan);
                if(item) {
                  item.qty += details.qty * deliverPlan['1'];
                  _.reject(items, (obj => obj.sku === item.sku));
                  items.push(item);
                } else {
                  items.push({
                    sku: planDetail.ProductCountry.Product.sku,
                    qty: details.qty * deliverPlan['1']
                  });
                }
              });
            }
          }
        });
        
        // update delivery address
        let deliveryAddress = order.DeliveryAddress.address.replace(/\n/g, ' ').split(',');
        let billingAddress = order.BillingAddress.address.replace(/\n/g, ' ').split(',');

        data.orderHeader = {
          HeaderFlag: 'ORDHD',
          InterfaceActionFlag: 'A',
          StorerKey: 'SHAVES2U',
          ExternOrderKey: OrderHelper.fromatBulkOrderNumber(order, true),
          OrderDate: moment().format('YYYYMMDDHHmmss'),
          DeliveryDate: moment().format('YYYYMMDDHHmmss'),
          C_contact1: `${order.DeliveryAddress.firstName} ${order.DeliveryAddress.lastName}`,
          C_Address1: deliveryAddress[0],
          C_Address2: deliveryAddress.length > 1 ? deliveryAddress[1] : '',
          C_Address3: deliveryAddress.length > 2 ? deliveryAddress[2] : '',
          C_Address4: deliveryAddress.length > 3 ? deliveryAddress[3] : '',
          C_City: order.DeliveryAddress.city.replace(/\n/g, ' '),
          // C_State: order.DeliveryAddress.state,
          C_Zip: order.DeliveryAddress.portalCode,
          C_Country: order.DeliveryAddress.Country.name,
          C_ISOCntryCode: order.DeliveryAddress.Country.code,
          C_Phone1: order.DeliveryAddress.contactNumber,
          B_contact1: `${order.BillingAddress.firstName} ${order.BillingAddress.lastName}`,
          B_Address1: billingAddress[0],
          B_Address2: billingAddress.length > 1 ? billingAddress[1] : '',
          B_Address3: billingAddress.length > 2 ? billingAddress[2] : '',
          B_Address4: billingAddress.length > 3 ? billingAddress[3] : '',
          B_City: order.BillingAddress.city.replace(/\n/g, ' '),
          // B_State: order.BillingAddress.state,
          B_Zip: order.BillingAddress.portalCode,
          B_Country: order.BillingAddress.Country.name,
          B_ISOCntryCode: order.BillingAddress.Country.code,
          B_Phone1: order.BillingAddress.contactNumber,
          Status: '0',
          Type: '0',
          SOStatus: '0'
        };

        // build order detail
        data.bulkOrderDetails = [];
        items.forEach((item, idx) => {
          data.bulkOrderDetails.push({
            HeaderFlag: 'ORDDT',
            InterfaceActionFlag: 'A',
            ExternOrderKey: OrderHelper.fromatBulkOrderNumber(order, true),
            ExternLineNo: `${idx + 1}`,
            Sku: item.sku,
            StorerKey: 'SHAVES2U',
            OriginalQty: `${item.qty}`,
            OpenQty: `${item.qty}`,
            ShippedQty: '0',
            UOM: 'EA',
            Lottable03: 'UR'
          });

          dataOrderUpload.push({
            orderNumber: OrderHelper.fromatBulkOrderNumber(order, true),
            bulkOrderId: order.orderId ? `${order.orderId}` : `${order.id}`,
            sku: item.sku,
            qty: `${item.qty}`,
            status: order.states,
            region: order.Country ? order.Country.code : order.region
          });
        });
      })
      .then(() => _createData({
        fileName: 'order-inbound/file-header.csv',
        data: {
          TableID: 'WMSORD',
          InterfaceType: 'I',
          CreateDateTime: moment().format('YYYYMMDDHHmmss'),
          ClientID: 'SHAVES2U',
          ClientCountry: 'MY',
          InterfaceName: 'SO Inbound'
        }
      }))
      .then(orderFileHeaderData => {
        data.orderFileHeaderData = orderFileHeaderData;
        return _createData({
          fileName: 'order-inbound/header.csv',
          data: data.orderHeader
        });
      })
      .then(orderHeaderData => {
        data.orderHeaderData = orderHeaderData;
        return Promise.mapSeries(data.bulkOrderDetails, details => _createData({
          fileName: 'order-inbound/details.csv',
          data: details
        }));
      })
      .then(bulkOrderDetailsData => {
        data.bulkOrderDetailsData = bulkOrderDetailsData;
        return _createData({
          fileName: 'order-inbound/file-footer.csv',
          data: {
            FileFooter: 'ORDTR',
            TotalRecordsCount: _fillZero(10, `${data.bulkOrderDetailsData.length + 1}`)
          }
        });
      })
      .then(orderFooterData => {
        data.orderFooterData = orderFooterData;

        // upload file to warehouse's FTP server
        data.inboundFileName = `WMSORD_${data.order.Country.code}_${moment().format('YYYYMMDDHHmmss')}.txt`;
        dataOrderUpload.forEach(element => {
          element.doFileName = data.inboundFileName;
        });
        let stream = fs.createWriteStream(`${config.warehouse.uploadFolder}/${data.inboundFileName}`, { encoding: 'latin1' });
        stream.write(`${data.orderFileHeaderData}\n`, 'latin1');
        stream.write(`${data.orderHeaderData}\n`, 'latin1');
        data.bulkOrderDetailsData.forEach(details => {
          stream.write(`${details}\n`, 'latin1');
        });
        stream.write(`${data.orderFooterData}\n`, 'latin1');
        stream.end();
      })
      .then(() => {
        loggingHelper.log('info', `Upload inound file: ${data.inboundFileName}, ${data.order.id}`);
        return ftpHelper.putFile(`${config.warehouse.uploadFolder}/${data.inboundFileName}`, `${config.warehouse.ftpUploadFolder}/${data.inboundFileName}`);
      }) // put file to ftp server
      .then(() => db.OrderUpload.bulkCreate(dataOrderUpload)) // insert info into Order Upload
      .then(() => db.BulkOrder.update({states: 'Processing'}, {where: {id: data.order.id}})) // update order states to 'Processing'
      .then(() => db.BulkOrderHistory.create({
        BulkOrderId: data.order.id,
        message: 'Processing'
      }))
      .catch(err => {
        if(err.message !== 'skip') {
          throw err;
        }
      });
  }

  static readOrderConfirmation(fileName, db) {
    loggingHelper.log('info', 'readOrderConfirmation start');
    loggingHelper.log('info', `param: fileName = ${fileName}`);
    // read outbound file
    let outBoundData = {detailsStrs: [], headerStrs: []};
    let dirName = `${__dirname}/../warehouse-output-files/`;
    let sendEmailQueueService = SendEmailQueueService.getInstance();
    return _readFileAsync(fileName, dirName)
      .then(data => {
        let dataArray = data.toString().split(/\r?\n/);
        dataArray.forEach((obj, idx) => {
          if(idx === 0) {
            outBoundData.fileHeaderStr = obj;
          } else if(obj.substr(0, 5) === 'SHPHD') {
            outBoundData.headerStrs.push(obj);
          }
        });
      })
      .then(() => _readData({fileName: 'order-confirmation-outbound/file-header.csv', values: outBoundData.fileHeaderStr}))
      .then(fileHeaderData => {
        outBoundData.fileHeaderData = fileHeaderData;
        return Promise.mapSeries(outBoundData.headerStrs, headerStr => _readData({fileName: 'order-confirmation-outbound/header.csv', values: headerStr}));
      })
      .then(headersData => {
        outBoundData.headersData = headersData;
        // find order match with the outbound file
        // update order status
        return Promise.mapSeries(outBoundData.headersData, headerData => {
          let orderId;
          if(headerData.ExternOrderKey[2] === 'B') {
            orderId = OrderHelper.getBulkOrderNumber(headerData.ExternOrderKey);
            if(headerData.SOStatus === 'CANC') {
              return db.BulkOrder.update({states: 'Canceled'}, {where: {id: orderId}})
                .then(() => db.BulkOrderHistory.create({
                  BulkOrderId: orderId,
                  message: 'Canceled'
                }))
                .then(() => db.BulkOrder.findOne({where: {id: orderId}, include: ['Country']}))
                .then(order => sendEmailQueueService.addTask({method: 'sendCancelBulkOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()}));
            } else {
              let order;
              return db.BulkOrder.update({
                states: 'Delivering',
                deliveryId: headerData.OtherReference,
                carrierKey: headerData.CarrierKey,
                carrierAgent: headerData.Carrieragent
              }, {where: {id: orderId, states: {$in: ['Processing', 'Delivering']}}})
                .then(() => db.BulkOrder.findOne({
                  where: {id: orderId},
                  include: ['Country', 'DeliveryAddress']
                }))
                .then(_order => {
                  order = _order;
                  if(order) {
                    return db.BulkOrderHistory.create({
                      BulkOrderId: orderId,
                      message: 'Delivering'
                    });
                  }
                })
                .then(() => 
                  db.OrderUpload.update(
                    {
                      wareHouseFileName: fileName.substr(fileName.lastIndexOf('/') + 1)
                    },
                    {where: {orderNumber: orderId}}
                  )
                )
                .then(() => {
                  if(order) {
                    return aftership.postTracking(order, true);
                  }
                })
                .catch(err => loggingHelper.log('error', `Read order outbound ERROR: ${fileName} ==== ${err}`));
            }
          } else {
            orderId = OrderHelper.getOrderNumber(headerData.ExternOrderKey);
            if(headerData.SOStatus === 'CANC') {
              let cancelOrderQueueService = CancelOrderQueueService.getInstance();
              return cancelOrderQueueService.addTask({orderId, canceledFrom: 'warehouse'});
            } else {
              let order;
              return db.Order.update({
                states: 'Delivering',
                deliveryId: headerData.OtherReference,
                carrierKey: headerData.CarrierKey,
                carrierAgent: headerData.Carrieragent
              }, {where: {id: orderId, states: {$in: ['Processing', 'Delivering']}}})
                .then(() => db.Order.findOne({
                  where: {id: orderId},
                  include: ['Country', 'DeliveryAddress']
                }))
                .then(_order => {
                  order = _order;
                  if(order) {
                    return db.OrderHistory.create({
                      OrderId: orderId,
                      message: 'Delivering'
                    });
                  }
                })
                .then(() => 
                  db.OrderUpload.update(
                    {
                      wareHouseFileName: fileName.substr(fileName.lastIndexOf('/') + 1)
                    },
                    {where: {orderNumber: orderId}}
                  )
                )
                .then(() => {
                  if(order) {
                    return aftership.postTracking(order);
                  }
                })
                .then(() => {
                  if(order) {
                    if(order.subscriptionIds) {
                      return db.Subscription.findAll({where: {
                        id: {$in: JSON.parse(order.subscriptionIds)}}
                      })
                      .then(subscriptions => {
                        subscriptions = SequelizeHelper.convertSeque2Json(subscriptions);
                        if(subscriptions[0].isTrial) {
                          return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}});
                        } else {
                          return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}});
                        }
                      });
                    } else {
                      return sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}});
                    }
                  }
                })
                .catch(err => loggingHelper.log('error', `Read order outbound ERROR: ${fileName} ==== ${err}`));
            }
          }
        });
      })
      .catch(err => {
        console.log(`Read order outbound ERROR: ${err}`);
        loggingHelper.log('error', `Read order outbound ERROR: ${fileName} ==== ${err}`);
      });
  }
}

export default WareHouseHelper;
