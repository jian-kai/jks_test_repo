import Promise from 'bluebird';
import PromiseFtp from 'promise-ftp';
import config from '../config';
import fs from 'fs';
import { loggingHelper } from './logging-helper';

class FtpHelper {
  constructor() {
    // init FTP object
    this.options = {
      host: config.keyWarehouse.ftp.host,
      user: config.keyWarehouse.ftp.username,
      password: config.keyWarehouse.ftp.password
    };
    this.ftp = new PromiseFtp();
  }

  connect() {
    loggingHelper.log('info', 'connect start');
    let status = this.ftp.getConnectionStatus();
    if(status !== PromiseFtp.STATUSES.CONNECTED && status !== PromiseFtp.STATUSES.CONNECTING) {
      return this.ftp.connect(this.options);
    } else {
      return Promise.resolve();
    }
  }

  disconnect() {
    loggingHelper.log('info', 'disconnect start');
    if(this.canEnd) {
      return this.ftp.end();
    } else {
      return Promise.resolve();
    }
  }

  /**
   * List all file in remote path
   * @param {*} remotePath 
   */
  listFiles(remotePath) {
    loggingHelper.log('info', 'listFiles start');
    loggingHelper.log('info', `param: remotePath = ${remotePath}`);
    let outputFiles = [];
    return this.connect()
      .then(() => {
        this.canEnd = false;
        return this.ftp.list(remotePath);
      })
      .then(files => {
        files.forEach(file => {
          if(file.type === '-') {
            outputFiles.push(file);
          }
        });
      })
      .then(() => {
        this.canEnd = true;
        return this.disconnect();
      })
      .then(() => outputFiles);
  }

  /**
   * Download and delete file on FTP server
   * @param {*} remotePath 
   * @param {*} localPath 
   */
  getFile(remotePath, localPath) {
    loggingHelper.log('info', 'getFile start');
    loggingHelper.log('info', `param: remotePath = ${remotePath}`);
    return this.connect()
      .then(() => {
        this.canEnd = false;
        return this.ftp.get(remotePath);
      })
      .then(stream => new Promise((resolve, reject) => {
        stream.once('close', resolve);
        stream.once('error', reject);
        stream.pipe(fs.createWriteStream(localPath));
      }))
      .then(() => {
        this.canEnd = true;
        return this.disconnect();
      });
  }

  putFile(source, remotePath) {
    loggingHelper.log('info', 'putFile start');
    loggingHelper.log('info', `param: remotePath = ${remotePath}`);
    return this.connect()
      .then(() => {
        this.canEnd = false;
        return this.ftp.put(source, remotePath);
      })
      .then(() => {
        this.canEnd = true;
        return this.disconnect();
      });
  }

  deleteFile(remotePath) {
    loggingHelper.log('info', 'deleteFile start');
    loggingHelper.log('info', `param: remotePath = ${remotePath}`);
    return this.connect()
    .then(() => {
      this.canEnd = false;
      return this.ftp.delete(remotePath);
    })
    .then(() => {
      this.canEnd = true;
      return this.disconnect();
    });
  }
}

export let ftpHelper = new FtpHelper();
