// using mailchimp's v3 Library
import fs from 'fs';
import pdf from 'html-pdf';
import moment from 'moment';
import jwt from 'jsonwebtoken';
import MailChimp from './mailchimp';
import Handlebars from 'handlebars';
import _ from 'underscore';
import config from '../config';
import SequelizeHelper from './sequelize-helper';
import OrderHelper from './order-helper';
import DatetimeHelper from './datetime-helpers';
import { loggingHelper } from './logging-helper';
import { awsHelper } from './aws-helper';
import { in18 } from '../in18';
import DownloadController from '../controllers/download.controller';

Handlebars.registerHelper('displayCurrency', value => {
  value = parseFloat(value);
  return value.toLocaleString('us-EN', {minimumFractionDigits: 2, maximumFractionDigits: 2});
});

class EmailHelper {
  constructor(langCode) {
    this.langCode = langCode.toUpperCase();
    this.moment = moment;
    this.moment.locale('en');
    if(langCode === 'ko') {
      this.dateFormat = 'LL';
      this.dateFormatWithSpace = 'LL';
    } else {
      this.dateFormat = 'DD-MMM-YYYY';
      this.dateFormatWithSpace = 'DD MMM YYYY';
    }
    // create mailchimp object
    this.mailChimp = MailChimp.getInstance();

    // loading template
    this._readFile(`${__dirname}/../views/email-templates/base/base.hbs`)
    .then(sText => {
      this.baseTmp = sText;
    });

    this._readFile(`${__dirname}/../views/email-templates/base/left-logo.hbs`)
      .then(sText => {
        this.leftLogoTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/base/center-logo.hbs`)
      .then(sText => {
        this.centerLogoTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/receipt-body.hbs`)
      .then(sText => {
        this.receiptBodyEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/receipt-body-pre-order.hbs`)
      .then(sText => {
        this.receiptBodyPreOrderEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/receipt-bulk-order.hbs`)
      .then(sText => {
        this.receiptBulkOrderEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/active-account.hbs`)
      .then(sText => {
        this.activeAccountEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/welcome-user.hbs`)
      .then(sText => {
        this.welcomeEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/reset-password.hbs`)
      .then(sText => {
        this.resetPasswordEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/password-updated.hbs`)
      .then(sText => {
        this.passwordUpdatedEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/cancel-order.hbs`)
      .then(sText => {
        this.cancelOrderEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/cancel-order-client.hbs`)
      .then(sText => {
        this.cancelOrderClientEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/auto-renewal-error.hbs`)
      .then(sText => {
        this.autoRenewalErrorTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/payment-fail.hbs`)
      .then(sText => {
        this.paymentFailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/shopping-cart-remind.hbs`)
      .then(sText => {
        this.pendingCartTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/renewal-confirm.hbs`)
      .then(sText => {
        this.renewalConfirmTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/product_sampling.hbs`)
      .then(sText => {
        this.samplingTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/promotion.hbs`)
      .then(sText => {
        this.promotionTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/trial-plan-receipt.hbs`)
      .then(sText => {
        this.trialPlanTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/trial-plan-receipt-pre-order.hbs`)
      .then(sText => {
        this.trialPlanPreOrderTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/cancel-trial-plan.hbs`)
      .then(sText => {
        this.cancelTrialPlanTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/cancel-trial-plan-request.hbs`)
      .then(sText => {
        this.cancelTrialPlanRequestTmp = sText;
      });
    
    this._readFile(`${__dirname}/../views/email-templates/${langCode}/update-trial-plan.hbs`)
      .then(sText => {
        this.updateTrialPlanTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/follow-up-trial-email.hbs`)
      .then(sText => {
        this.followUpTrialPlanTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/sale-report.hbs`)
      .then(sText => {
        this.saleReportEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/subscribers-report.hbs`)
      .then(sText => {
        this.subscribersReportEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/users-sale-report.hbs`)
      .then(sText => {
        this.usersSaleReportEmailTmp = sText; //
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/users-report.hbs`)
      .then(sText => {
        this.usersReportEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/subscribers-sale-report.hbs`)
      .then(sText => {
        this.subscribersSaleReportEmailTmp = sText;
      });

    this._readFile(`${__dirname}/../views/email-templates/${langCode}/student-promo-code-report.hbs`)
      .then(sText => {
        this.studentPromoCodeReportEmailTmp = sText;
      });
  }

  _readFile(path) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, 'UTF8', (oErr, sText) => {
        if(!oErr) {
          resolve(sText);
        } else {
          reject(oErr);
        }
      });
    });
  }

  _getOrderData(orderId, db) {
    // get order detail
    let order;
    return db.Order.findOne({
      where: {id: orderId},
      include: [
        'User',
        'SellerUser',
        'Receipt',
        'DeliveryAddress',
        'BillingAddress',
        'Country',
        {
          model: db.OrderDetail,
          as: 'orderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: [
                {
                  model: db.Product,
                  include: ['productTranslate', 'productImages']
                }
              ]
            },
            {
              model: db.PlanCountry,
              include: [
                {
                  model: db.Plan,
                  include: ['PlanType', 'planTranslate', 'planImages']
                }, {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [{
                    model: db.ProductCountry,
                    include: [{
                      model: db.Product,
                      include: ['productImages', 'productTranslate']
                    }]
                  }]
                }, {
                  model: db.PlanTrialProduct,
                  as: 'planTrialProducts',
                  include: [{
                    model: db.ProductCountry,
                    include: [{
                      model: db.Product,
                      include: ['productImages', 'productTranslate']
                    }]
                  }]
                }
              ]
            }
          ]
        },
        {
          model: db.Promotion,
          as: 'Promotion',
          include: ['promotionCodes']
        }
      ]
    })
      .then(_order => {
        order = SequelizeHelper.convertSeque2Json(_order);
        if(order.subscriptionIds) {
          return db.Subscription.findAll({where: {
            id: {$in: JSON.parse(order.subscriptionIds)}}
          });
        } else {
          return;
        }
      })
      .then(_subscriptions => {
        order.Subscriptions = _subscriptions;

        // detemine language
        let lang = order.Country.defaultLang;

        // build order
        order.displayId = OrderHelper.fromatOrderNumber(order);
        // order.invoiceNumber = OrderHelper.formatInvoiceNumber(order);

        // if(order.User) {
        //   lang = order.User.defaultLanguage.toUpperCase();
        // } else if(order.SellerUser) {
        //   lang = order.SellerUser.defaultLanguage.toUpperCase();
        // }

        if(order.User && order.User.firstName) {
          order.displayName = order.User.firstName;
        } else if(order.fullName) {
          order.displayName = order.fullName;
        } else {
          order.displayName = 'Guest';
        }

        console.log(`lang ===== ${lang} === ${JSON.stringify(order)}`);
        // build email template data
        order.orderDetail.forEach(orderDetail => {
          orderDetail.totalPrice = 0;
          orderDetail.currency = order.Country.currencyDisplay;

          if(orderDetail.ProductCountry) {
            console.log('lang ====== ====== ===== ', lang);
            console.log('orderDetail.ProductCountry.Product.productTranslate ====== ====== ===== ', orderDetail.ProductCountry.Product.productTranslate);
            orderDetail.ProductCountry.Product.productTranslate = _.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: lang}) ?
                                                                  _.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: lang}) :
                                                                  _.findWhere(orderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
            orderDetail.ProductCountry.Product.productImages = _.findWhere(orderDetail.ProductCountry.Product.productImages, {isDefault: true});
            orderDetail.totalPrice = (orderDetail.price * orderDetail.qty).toFixed(2);
            orderDetail.type = 'ALA-CARTE';
            orderDetail.isProduct = true;
            orderDetail.isFreeProduct = order.Promotion ? (order.Promotion.freeProductCountryIds) : false;
            // orderDetail.isProductOfTrial = (order.Subscriptions && order.Subscriptions.length > 0) ? order.Subscriptions[0].isTrial : false;
            orderDetail.isProductOfTrial = false;
            if(order.Subscriptions && order.Subscriptions.length > 0) {
              if(order.Subscriptions[0].isCustom) {
                orderDetail.isProductOfTrial = false;
              } else {
                orderDetail.isProductOfTrial = true;
              }
            }
          }
          if(orderDetail.PlanCountry) {
            orderDetail.PlanCountry.Plan.planTranslate = _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: lang}) ?
                                                         _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: lang}) :
                                                         _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
            orderDetail.PlanCountry.Plan.planImages = _.findWhere(orderDetail.PlanCountry.Plan.planImages, {isDefault: true});
            orderDetail.unitPrice = (orderDetail.price * orderDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
            orderDetail.totalPrice = (orderDetail.price * orderDetail.qty).toFixed(2);
            orderDetail.type = 'SUBSCRIPTION';
            orderDetail.isPlan = true;

            // build delivery product
            orderDetail.deliveryDetails = [];
            let deliveryDetail;
            
            orderDetail.PlanCountry.planTrialProducts.forEach(planTrialProduct => {
              let productTranslates = planTrialProduct.ProductCountry.Product.productTranslate;
              let productName = _.findWhere(productTranslates, {langCode: lang}) ?
                                _.findWhere(productTranslates, {langCode: lang}).name :
                                _.findWhere(productTranslates, {langCode: 'EN'}).name;
              deliveryDetail = {
                time: 0,
                isTrial: true,
                productNames: [{productName, qty: planTrialProduct.qty}],
              };
              orderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
            });
            
            orderDetail.PlanCountry.planDetails.forEach(planDetail => {
              let deliverPlan = JSON.parse(planDetail.deliverPlan);
              let times = Object.keys(deliverPlan);
              times.forEach(time => {
                time = parseInt(time);
                let productTranslates = planDetail.ProductCountry.Product.productTranslate;
                let productName = _.findWhere(productTranslates, {langCode: lang}) ?
                                  _.findWhere(productTranslates, {langCode: lang}).name :
                                  _.findWhere(productTranslates, {langCode: 'EN'}).name;
                let productImgUrl = _.findWhere(planDetail.ProductCountry.Product.productImages, {isDefault: true}).url;
                deliveryDetail = _.findWhere(orderDetail.deliveryDetails, {time});
                if(deliveryDetail) {
                  let product = _.findWhere(deliveryDetail.productNames, {productName});
                  if(product) {
                    product.qty += deliverPlan[time];
                  } else if(deliverPlan[time] !== 0) {
                    deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                  }
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail = {
                    time,
                    productNames: [{
                      productName,
                      qty: deliverPlan[time],
                      imgUrl: productImgUrl
                    }],
                  };
                  if(orderDetail.PlanCountry.Plan.isTrial) {
                    deliveryDetail.isFirstTrial = true;
                  }
                  orderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
                }
              });
            });
            
            // TODO work around for design
            let tmp = [];
            orderDetail.deliveryDetails.forEach(obj => {
              if(obj.time === 1) {
                obj.isFirst = true;
              } else {
                obj.notFirst = true;
              }
              if(obj.time < 3) {
                tmp.push(obj);
              }
            });
            orderDetail.deliveryDetails = tmp;
            console.log(`orderDetail.deliveryDetails ==== ${JSON.stringify(orderDetail.deliveryDetails)}`);
          }
        });

        // build extra data
        order.hasDiscount = order.Receipt.discountAmount > 0;
        order.hasShippingFee = order.Receipt.shippingFee > 0;
        order.isFreeShipping = parseFloat(order.Receipt.shippingFee) === 0;
        order.hasTaxAmount = order.Receipt.taxAmount > 0;
        order.isPayByStripe = order.paymentType === 'stripe';
        order.isPayByIpay88 = order.paymentType === 'iPay88';
        order.isPayByCash = order.paymentType === 'cash';

        this.moment = DatetimeHelper.setMomentLocale(this.moment, order.Country.code.toLowerCase() === 'kor' ? 'ko' : 'en');
        if(order.Country.code.toLowerCase() === 'kor') {
          order.shippingDate = this.moment(new Date(config.deliverDateForKorea)).add(1, 'days').format(this.dateFormatWithSpace);
        } else {
          order.shippingDate = this.moment(new Date(order.createdAt)).add(1, 'days').format(this.dateFormatWithSpace);
        }
        order.createdAt = this.moment(new Date(order.createdAt)).format(this.dateFormatWithSpace);
        order.hasShippingAddress = order.DeliveryAddress !== null;
        order.hasBillingAddress = order.BillingAddress !== null;
        order.countryCode = order.Country.code.toLowerCase();

        // if(order.countryCode === 'kor') {
        //   order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery, config.deliverDateForKorea).format('DD-MMM-YYYY');
        // } else {
        //   if(!order.SellerUserId) {
        //     order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery, order.createdAt).format('DD-MMM-YYYY');
        //   } else {
        //     if(order.countryCode !== 'mys') {
        //       order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery, order.createdAt).format('DD-MMM-YYYY');
        //     } else {
        //       order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery, order.createdAt).format('DD-MMM-YYYY');
        //     }
        //   }
        // }
        order.trialShippingDate = order.startDeliverDate;

        // detemate ongound order
        order.isOnground = !!order.SellerUser;
        return order;
      });
  }

  _getBulkOrderData(orderId, db) {
    // get order detail
    let order;
    return db.BulkOrder.findOne({
      where: {id: orderId},
      include: [
        'DeliveryAddress',
        'BillingAddress',
        'Country',
        {
          model: db.BulkOrderDetail,
          as: 'bulkOrderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: [
                {
                  model: db.Product,
                  include: ['productTranslate', 'productImages']
                }
              ]
            },
            {
              model: db.PlanCountry,
              include: [
                {
                  model: db.Plan,
                  include: ['PlanType', 'planTranslate', 'planImages']
                }, {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [{
                    model: db.ProductCountry,
                    include: [{
                      model: db.Product,
                      include: ['productImages', 'productTranslate']
                    }]
                  }]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(_order => {
        order = SequelizeHelper.convertSeque2Json(_order);
        // detemine language
        let lang = order.Country.defaultLang;

        // build order
        order.displayId = OrderHelper.fromatBulkOrderNumber(order);
        order.displayName = 'Guest';

        console.log(`lang ===== ${lang} === ${JSON.stringify(order)}`);
        // build email template data
        order.bulkOrderDetail.forEach(bulkOrderDetail => {
          bulkOrderDetail.totalPrice = 0;

          if(bulkOrderDetail.ProductCountry) {
            bulkOrderDetail.ProductCountry.Product.productTranslate = _.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: lang}) ?
                                                                      _.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: lang}) :
                                                                      _.findWhere(bulkOrderDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
            bulkOrderDetail.ProductCountry.Product.productImages = _.findWhere(bulkOrderDetail.ProductCountry.Product.productImages, {isDefault: true});
            bulkOrderDetail.totalPrice = (bulkOrderDetail.price * bulkOrderDetail.qty).toFixed(2);
            bulkOrderDetail.type = 'ALA-CARTE';
            bulkOrderDetail.isProduct = true;
          }
          if(bulkOrderDetail.PlanCountry) {
            bulkOrderDetail.PlanCountry.Plan.planTranslate = _.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: lang}) ?
                                                             _.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: lang}) :
                                                             _.findWhere(bulkOrderDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
            bulkOrderDetail.PlanCountry.Plan.planImages = _.findWhere(bulkOrderDetail.PlanCountry.Plan.planImages, {isDefault: true});
            bulkOrderDetail.unitPrice = (bulkOrderDetail.price * bulkOrderDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
            bulkOrderDetail.totalPrice = (bulkOrderDetail.price * bulkOrderDetail.qty).toFixed(2);
            bulkOrderDetail.type = 'SUBSCRIPTION';
            bulkOrderDetail.isPlan = true;

            // build delivery product
            bulkOrderDetail.deliveryDetails = [];
            let deliveryDetail;
            bulkOrderDetail.PlanCountry.planDetails.forEach(planDetail => {
              let deliverPlan = JSON.parse(planDetail.deliverPlan);
              let times = Object.keys(deliverPlan);
              times.forEach(time => {
                time = parseInt(time);
                let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: lang}) ?
                                  _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: lang}).name :
                                  _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
                deliveryDetail = _.findWhere(bulkOrderDetail.deliveryDetails, {time});
                if(deliveryDetail) {
                  let product = _.findWhere(deliveryDetail.productNames, {productName});
                  if(product) {
                    product.qty += deliverPlan[time];
                  } else if(deliverPlan[time] !== 0) {
                    deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                  }
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail = {
                    time,
                    productNames: [{productName, qty: deliverPlan[time]}],
                  };
                  bulkOrderDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
                }
              });
            });
            // TODO work around for design
            let tmp = [];
            bulkOrderDetail.deliveryDetails.forEach(obj => {
              if(obj.time === 1) {
                obj.isFirst = true;
              } else {
                obj.notFirst = true;
              }
              if(obj.time < 3) {
                tmp.push(obj);
              }
            });
            bulkOrderDetail.deliveryDetails = tmp;
            console.log(`bulkOrderDetail.deliveryDetails ==== ${JSON.stringify(bulkOrderDetail.deliveryDetails)}`);
          }
        });

        // build extra data
        order.isPayByCash = order.paymentType === 'cash';

        this.moment = DatetimeHelper.setMomentLocale(this.moment, order.Country.code.toLowerCase() === 'kor' ? 'ko' : 'en');
        if(order.Country.code.toLowerCase() === 'kor') {
          order.shippingDate = this.moment(new Date(config.deliverDateForKorea)).add(1, 'days').format(this.dateFormatWithSpace);
        } else {
          order.shippingDate = this.moment(new Date(order.createdAt)).add(1, 'days').format(this.dateFormatWithSpace);
        }
        order.createdAt = this.moment(new Date(order.createdAt)).format(this.dateFormatWithSpace);
        order.hasShippingAddress = order.DeliveryAddress !== null;
        order.hasBillingAddress = order.BillingAddress !== null;
        order.countryCode = order.Country.code.toLowerCase();

        return order;
      });
  }

  sendResetAdminPassword(options) {
    loggingHelper.log('info', 'sendResetAdminPassword start');
    loggingHelper.log('info', `param: options = ${options}`);
    // create reset link
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.resetPasswordEmailTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let expireAt = moment().add(config.email.expiredTime);
    let resetLink = `${config.adminUrl}admin/reset-password?email=${options.email}&token=${jwt.sign({ token: expireAt, email: options.email }, config.accessToken.secret)}`;
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let resetPasswordBodyHtml = template({
      firstName: options.firstName,
      email: options.email,
      link: resetLink,
      countryCode: options.Country.code.toLowerCase()
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: resetPasswordBodyHtml
    });

    return this.mailChimp.sendTransactionEmail({
      subject: '[Shaves2U] Reset password',
      html: emailHtml,
      to: options.email
    });
  }

  sendResetUserPassword(options) {
    loggingHelper.log('info', `sendResetUserPassword start: options = ${JSON.stringify(options)}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.resetPasswordEmailTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let expireAt = moment().add(config.email.expiredTime);
    let resetLink = `${config.webUrl}users/reset-password?email=${options.email}&token=${jwt.sign({ token: expireAt, email: options.email }, config.accessToken.secret)}&utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_forgotpassword&utm_content=S2U_html_forgotpassword&utm_term=_forgotpassword_web`;
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let resetPasswordBodyHtml = template({
      firstName: options.firstName,
      email: options.email,
      link: resetLink,
      countryCode: options.Country.code.toLowerCase()
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: resetPasswordBodyHtml
    });

    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_reset_password', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  sendPasswordUpdated(options) {
    loggingHelper.log('info', 'sendPasswordUpdated start');
    loggingHelper.log('info', `param: options = ${options}`);
    // create reset link
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.passwordUpdatedEmailTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let passwordUpdateBodyHtml = template({
      email: options.email,
      firstName: options.firstName
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: passwordUpdateBodyHtml
    });

    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_reset_password_succeed', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  sendActiveUser(options, db) {
    loggingHelper.log('info', 'sendActiveUser start');
    loggingHelper.log('info', `param: options = ${options}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.activeAccountEmailTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let expireAt = moment().add(config.email.expiredTime);
    let activeLink;

    return db.User.findOne({where: {email: options.email}})
      .then(user => {
        if(user) {
          if(user.password) {
            activeLink = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: options.email }, config.accessToken.secret)}?utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          } else {
            activeLink = `${config.webUrl}users/reset-password?email=${options.email}&token=${jwt.sign({ token: expireAt, email: options.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/subscriptions&utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          }
      
          let logoHtml = logoTemplate({
            homepage: config.webUrl
          });
          let activeUserBodyHtml = template({
            firstName: options.firstName,
            link: activeLink
          });
          let emailHtml = baseTemplate({
            header: logoHtml,
            bodyContent: activeUserBodyHtml
          });
          return this.mailChimp.sendTransactionEmail({
            subject: in18.getMessage('email', 'user_active', options.langCode),
            html: emailHtml,
            to: options.email
          })
          .then(() => {
            if(options.utmData) {
              // only push if send active email on the first time (that has UTM data attached)
              let utmDefaultData = this.mailChimp.getDefaultUtmData();
              this.mailChimp.addMemberToRegisterList({
                'email_address': options.email,
                'status': 'subscribed',
                'merge_fields': {
                  FNAME: options.firstName,
                  LNAME: options.lastName ? options.lastName : '',
                  COUNTRY: options.Country.code.substr(0, 2),
                  SOURCE: options.badgeId ? 'BA app' : 'Website',
                  USOURCE: options.utmData.source ? options.utmData.source : this.mailChimp.getDefaultUtmData('source'),
                  UMEDIUM: options.utmData.medium ? options.utmData.medium : utmDefaultData,
                  UCAMPAIGN: options.utmData.campaign ? options.utmData.campaign : utmDefaultData,
                  UCONTENT: options.utmData.content ? options.utmData.content : utmDefaultData,
                  UTERM: options.utmData.term ? options.utmData.term : utmDefaultData
                }
              });
            }

            return Promise.resolve();
          });
        }
      });
  }

  sendActiveUserSample(options) {
    loggingHelper.log('info', 'sendActiveUserSample start');
    loggingHelper.log('info', `param: options = ${options}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.samplingTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let expireAt = moment().add(config.email.expiredTime);
    let activeLink;
    activeLink = `${config.webUrl}users/reset-password?email=${options.email}&token=${jwt.sign({ token: expireAt, email: options.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/subscriptions&utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;

    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let activeUserBodyHtml = template({
      firstName: options.firstName,
      link: activeLink
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: activeUserBodyHtml
    });
    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_sample_active', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  sendWelcomeEmail(options) {
    loggingHelper.log('info', 'sendWelcomeEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.welcomeEmailTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let welcomeBodyHtml = template({
      firstName: options.firstName,
      countryCode: options.Country.code.toLowerCase()
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: welcomeBodyHtml
    });
    return this.mailChimp.sendTransactionEmail({
      html: emailHtml,
      subject: in18.getMessage('email', 'user_welcome', options.langCode),
      to: options.email
    })
      .then(() => {
        if(options.utmData) {
          // only push if login with FB (that has UTM data attached)
          let utmDefaultData = this.mailChimp.getDefaultUtmData();
          this.mailChimp.addMemberToRegisterList({
            'email_address': options.email,
            'status': 'subscribed',
            'merge_fields': {
              FNAME: options.firstName,
              LNAME: options.lastName ? options.lastName : '',
              COUNTRY: options.Country.code.substr(0, 2),
              SOURCE: options.badgeId ? 'BA app' : 'Website',
              USOURCE: options.utmData.source ? options.utmData.source : this.mailChimp.getDefaultUtmData('source'),
              UMEDIUM: options.utmData.medium ? options.utmData.medium : utmDefaultData,
              UCAMPAIGN: options.utmData.campaign ? options.utmData.campaign : utmDefaultData,
              UCONTENT: options.utmData.content ? options.utmData.content : utmDefaultData,
              UTERM: options.utmData.term ? options.utmData.term : utmDefaultData
            }
          });
        }

        return Promise.resolve();
      });
  }

  sendCancelOrderEmail(options, db) {
    loggingHelper.log('info', 'sendCancelOrderEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    let order;
    let payByCash;
    let payByCredit;
    let baseTemplate;
    let logoHtml;
    return this._getOrderData(options.id, db)
      .then(_order => {
        order = _order;

        // check e-commerce
        if(order.Country.isWebEcommerce) {
          order.isEcommerce = true;
        }

        baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.cancelOrderEmailTmp);
        let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    
        payByCash = (options.paymentType === 'cash');
        payByCredit = (options.paymentType === 'stripe');
    
        logoHtml = logoTemplate({
          homepage: config.webUrl
        });
        let cancelOrderHtml = template({
          firstName: options.User.firstName,
          payByCash,
          payByCredit,
          order,
          countryCode: options.Country.code.toLowerCase()
        });
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: cancelOrderHtml
        });
        let sendOptions = {
          html: emailHtml,
          subject: in18.getMessage('email', 'user_cancel_order', options.langCode),
          to: options.email
        };
        return this.mailChimp.sendTransactionEmail(sendOptions);
      })
      .then(() => {
        if(order.carrierAgent === 'courex') {
          return Promise.resolve();
        } else {
          let template = Handlebars.compile(this.cancelOrderClientEmailTmp);

          let cancelOrderHtml = template({
            firstName: options.User.firstName,
            payByCash,
            payByCredit,
            order,
            countryCode: options.Country.code.toLowerCase()
          });

          let emailHtml = baseTemplate({
            header: logoHtml,
            bodyContent: cancelOrderHtml
          });

          let sendOptions = {
            html: emailHtml,
            subject: in18.getMessage('email', 'admin_cancel_order', options.langCode).replace('{value}', order.displayId),
            to: config.email.cancelOrderList
          };
          return this.mailChimp.sendTransactionEmail(sendOptions);
        }
      });
  }

  sendCancelBulkOrderEmail(options, db) {
    loggingHelper.log('info', 'sendCancelBulkOrderEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    let payByCash;
    let baseTemplate;
    let logoHtml;
    return this._getBulkOrderData(options.id, db)
      .then(order => {
        order.orderDetail = order.bulkOrderDetail;
        baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.cancelOrderClientEmailTmp);
        let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    
        payByCash = true;
    
        logoHtml = logoTemplate({
          homepage: config.webUrl
        });

        let cancelOrderHtml = template({
          payByCash,
          order
        });

        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: cancelOrderHtml
        });

        let sendOptions = {
          html: emailHtml,
          subject: in18.getMessage('email', 'admin_cancel_bulk_order', options.langCode).replace('{value}', order.displayId),
          to: config.email.cancelOrderList
        };
        return this.mailChimp.sendTransactionEmail(sendOptions);
      });
  }

  sendPromotionEmail(options) {
    loggingHelper.log('info', 'sendPromotionEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    // create reset link
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.promotionTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let promotionBodyHtml = template({
      firstName: options.firstName,
      promoCode: options.promoCode,
      discount: options.discount,
      startDate: options.startDate,
      expiredAt: options.expiredAt
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: promotionBodyHtml
    });

    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_promo_code', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  // send receipts email
  sendReceiptsEmail(options, db) {
    loggingHelper.log('info', 'sendReceiptsEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    // get order detail
    return this._getOrderData(options.orderId, db)
      .then(order => {
        let emailTitle;
        let emailSubject;
        let isTaxInvoice;
        // get link to order detail
        let expireAt = moment().add(config.email.expiredTime);
        if(order.User.isActive) {
          order.link = `${config.webUrl}user/orders/${order.id}?utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          order.isOldUser = true;
        } else if(!order.User.isGuest) {
          order.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          order.isActiveUser = true;
        } else {
          let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${order.User.email}&token=${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
          order.link = activeAndViewOrderLink;
          order.isNewUser = true;
        }

        // check e-commerce
        if(order.Country.isWebEcommerce) {
          order.isEcommerce = true;
        } else {
          order.link = `${config.webUrl}`;
        }

        // process status
        order.checkMysConfirm = order.countryCode === 'mys';
        if(order.states === 'Payment Received' || order.states === 'Processing') {
          if(order.checkMysConfirm) {
            emailTitle = in18.getMessage('email', 'user_order_confirm_title_mys', options.langCode);
            isTaxInvoice = true;
            if(order.SellerUser) {
              order.isBaSale = true;
              order.isBaSubSaleMys = true;
              order.link = `${config.webUrl}`;
            }
          } else {
            emailTitle = in18.getMessage('email', 'user_order_confirm_title', options.langCode);
          }
          if(order.subscriptionIds) {
            if(order.checkMysConfirm) {
              emailSubject = in18.getMessage('email', 'user_order_tax_invoice_subject', options.langCode).replace('{value}', order.displayId);
            } else {
              emailSubject = in18.getMessage('email', 'user_order_confirm_subject', options.langCode).replace('{value}', order.displayId);
            }
          } else {
            emailSubject = in18.getMessage('email', 'user_order_confirm_subject', options.langCode).replace('{value}', order.displayId);
          }
          order.orderConfirmed = true;
        } else if(order.states === 'Delivering') {
          emailTitle = in18.getMessage('email', 'user_order_delivering_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_order_delivering_subject', options.langCode);
          order.orderDelivering = true;
          order.trackOrderLink = order.carrierAgent === 'courex' ? `${config.urbanFox.trackingUrl}${order.deliveryId}` : `${config.aftership.trackingUrl}/${order.deliveryId}`;
        } else if(order.states === 'Completed') {
          order.orderCompleted = true;
          if(order.SellerUser) {
            if(order.checkMysConfirm && (order.Promotion && order.Promotion.promotionCodes && order.Promotion.promotionCodes[0].code === 'EVENTRM1')) {
              emailTitle = in18.getMessage('email', 'user_order_confirm_title_mys', options.langCode);
              emailSubject = in18.getMessage('email', 'user_order_tax_invoice_subject', options.langCode).replace('{value}', order.displayId);
              isTaxInvoice = true;
              order.isBaAlaSaleMys = true;
              order.link = `${config.webUrl}`;
            } else {
              emailTitle = in18.getMessage('email', 'user_order_completed_title', options.langCode);
              emailSubject = in18.getMessage('email', 'user_order_completed_subject', options.langCode);
              if(!order.Subscriptions || order.Subscriptions.length <= 0 || (order.Subscriptions && order.Subscriptions.length > 0 && order.isDirectTrial)) {
                isTaxInvoice = true;
              }
            }
            // if(order.subscriptionIds) {
            //   emailSubject = in18.getMessage('email', 'user_order_tax_invoice_subject', options.langCode).replace('{value}', order.displayId);
            // } else {
            //   emailSubject = in18.getMessage('email', 'user_order_completed_subject', options.langCode);
            // }
            order.isBaSale = true;
          } else {
            emailTitle = in18.getMessage('email', 'user_order_completed_title', options.langCode);
            emailSubject = in18.getMessage('email', 'user_order_completed_subject', options.langCode);
          }
        }

        if(options.isOld) {
          console.log('isOld ================');
          emailTitle = 'Tax Invoice';
          emailSubject = '[Shaves2U] Order {value}: Your S2U Tax Invoice'.replace('{value}', order.displayId);
          isTaxInvoice = true;
        }
        
        if(order.countryCode === 'kor' && moment().isBefore(moment(config.deliverDateForKorea))) {
          emailTitle = in18.getMessage('email', 'pre_order_user_order_confirm_title', options.langCode);
          emailSubject = in18.getMessage('email', 'pre_order_user_order_confirm_subject', options.langCode).replace('{value}', order.displayId);
        }

        order.orderDetail.forEach(orderDetail => {
          orderDetail.currency = order.Country.currencyDisplay;
          if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan && orderDetail.PlanCountry.Plan.isTrial) {
            order.isTrial = true;
          }

          orderDetail.taxCode = order.Country.taxCode;
          orderDetail.taxRate = order.Country.taxRate;
          orderDetail.taxName = order.Country.taxName;
          
          orderDetail.priceExcludeTax = (0.00).toFixed(2);
          orderDetail.taxPrice = (0.00).toFixed(2);
          if(order.Country.includedTaxToProduct) {
            orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
            orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
          }
        });

        order.Receipt.priceExcludeTax = (0.00).toFixed(2);
        order.Receipt.taxPrice = (0.00).toFixed(2);
        order.Receipt.cashRebate = (0.00).toFixed(2);
        order.Receipt.totalPayable = (order.Receipt.totalPrice - order.Receipt.cashRebate).toFixed(2);
        if(order.hasShippingFee) {
          order.Receipt.priceExcludeTax = (order.Receipt.shippingFee / (1 + (order.Country.taxRate / 100))).toFixed(2);
          order.Receipt.taxPrice = (order.Receipt.shippingFee - order.Receipt.priceExcludeTax).toFixed(2);
        }
        
        order.priceExcludeTax = (order.Receipt.totalPrice / (1 + (order.Country.taxRate / 100))).toFixed(2);
        order.taxPrice = (order.Receipt.totalPrice - order.priceExcludeTax).toFixed(2);

        order.isOffline = !!order.SellerUserId;
        // if(order.countryCode === 'kor') {
        //   order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery, config.deliverDateForKorea).format('DD-MMM-YYYY');
        // } else {
        //   if(!order.isOffline) {
        //     order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
        //   } else {
        //     if(order.countryCode !== 'mys') {
        //       order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery).format('DD-MMM-YYYY');
        //     } else {
        //       order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
        //     }
        //   }
        // }
        order.trialShippingDate = order.startDeliverDate;

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.receiptBodyEmailTmp);
        if(order.countryCode === 'kor' && moment().isBefore(moment(config.deliverDateForKorea))) {
          template = Handlebars.compile(this.receiptBodyPreOrderEmailTmp);
        }
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(order);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml,
          countryCode: order.countryCode,
          checkMysConfirm: order.checkMysConfirm
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: order.email
        })
        .then(() => {
          if(isTaxInvoice) {
            let fileName = `TaxInvoice_${order.displayId}_${moment().format('YYYY-MM-DD_HHmmssSSS')}.pdf`;
            return new Promise((resolve, reject) => {
              pdf.create(emailHtml, { height: '2043px', width: '1444px' }).toFile(`${config.taxInvoicePath}/${fileName}`, (err, res) => {
                if(err) reject(err);
          
                // upload file to S3
                let fileUpload = {
                  fieldName: 'url',
                  originalFilename: fileName,
                  path: res.filename,
                  name: fileName,
                  type: 'application/pdf'
                };
                
                awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
                  .then(uploadedFile => 
                    db.FileUpload.create(
                      {
                        name: fileName,
                        url: uploadedFile.Location,
                        orderId: order.id,
                        isDownloaded: false
            
                      },
                      {returning: true})
                  )
                  .then(() => 
                    fs.unlink(res.filename, err => {
                      if(err) reject(err);
                      resolve();
                    })
                  )
                  .catch(error => reject(error));
              });
            });
          } else {
            return;
          }
        });
      })
      .catch(err => {
        console.log(`Send receipt email error : ${err.stack || JSON.stringify(err)}`);
        loggingHelper.log('error', `Send receipt email error : ${err.stack || JSON.stringify(err)}`);
      });
  }

  // send receipts email
  sendReceiptsTrialEmail(options, db) {
    loggingHelper.log('info', 'sendReceiptsTrialEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    // get order detail
    let checkMysConfirm;
    let isBaSaleMys;
    let orderState;
    return this._getOrderData(options.orderId, db)
      .then(order => {
        orderState = order.states;
        let emailTitle;
        let emailSubject;
        // get link to order detail
        let expireAt = moment().add(config.email.expiredTime);
        if(order.User.isActive) {
          order.link = `${config.webUrl}user/shave-plans?utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          order.isOldUser = true;
        } else if(!order.User.isGuest) {
          order.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}?nextPage=/user/shave-plans&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          order.isActiveUser = true;
        } else {
          let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${order.User.email}&token=${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/shave-plans&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
          order.link = activeAndViewOrderLink;
          order.isNewUser = true;
        }

        order.isOffline = !!order.SellerUserId;

        if(order.states === 'Payment Received' || order.states === 'Processing') {
          checkMysConfirm = order.countryCode === 'mys';
        }
        if(order.SellerUser && order.countryCode === 'mys') {
          isBaSaleMys = true;
        }
        // process status
        // if(order.states === 'Payment Received' || order.states === 'Processing') {
        //   emailTitle = 'Enjoy your smooth, fuzz-free shave.';
        //   emailSubject = order.isOffline ? '[Shaves2U] Your trial pack is on the way!' : '[Shaves2U] Thanks for your order!';
        //   order.orderConfirmed = true;
        // } else if(order.states === 'Delivering') {
        //   emailTitle = 'Your order is on its way!';
        //   emailSubject = '[Shaves2U] Keep track of your delivery here!';
        //   order.orderDelivering = true;
        //   // order.trackOrderLink = `${config.aftership.trackingUrl}/${order.deliveryId}`;
        // } else if(order.states === 'Completed') {
        //   emailTitle = 'Delivery Confirmation';
        //   emailSubject = '[Shaves2U] Your order has been delivered';
        //   order.orderCompleted = true;
        // }
        // emailTitle = order.isOffline ? 'Enjoy your smooth, fuzz-free shave.' : 'Welcome to the club!';
        emailTitle = in18.getMessage('email', 'user_trial_confirm_title', options.langCode);
        emailSubject = in18.getMessage('email', 'user_trial_confirm_subject', options.langCode);
        if(order.countryCode === 'kor' && moment().isBefore(moment(config.deliverDateForKorea))) {
          emailTitle = in18.getMessage('email', 'pre_order_user_trial_confirm_title', options.langCode);
          emailSubject = in18.getMessage('email', 'pre_order_user_trial_confirm_subject', options.langCode);
        }

        if(order.User.isActive) {
          order.link = `${config.webUrl}user/orders/${order.id}?utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          order.linkDashboard = `${config.webUrl}user/shave-plans?utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          order.isOldUser = true;
        } else if(!order.User.isGuest) {
          order.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          order.linkDashboard = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          order.isActiveUser = true;
        } else {
          let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${order.User.email}&token=${jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/orders/${order.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${order.Country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
          order.link = activeAndViewOrderLink;
          order.linkDashboard = activeAndViewOrderLink;
          order.isNewUser = true;
        }

        // process data to display
        if(order.orderDetail.length === 0 || !order.orderDetail[0].PlanCountry) {
          throw new Error('order detail or plan does not existed');
        }
        
        order.orderDetail.forEach(orderDetail => {
          orderDetail.currency = order.Country.currencyDisplay;
          if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan && orderDetail.PlanCountry.Plan.isTrial) {
            order.isTrial = true;
          }

          orderDetail.taxCode = order.Country.taxCode;
          orderDetail.taxRate = order.Country.taxRate;
          orderDetail.taxName = order.Country.taxName;
          
          orderDetail.priceExcludeTax = (0.00).toFixed(2);
          orderDetail.taxPrice = (0.00).toFixed(2);
          if(order.Country.includedTaxToProduct) {
            orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
            orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
          }
        });

        let orderDetail = order.orderDetail[0];

        // orderDetail.taxCode = order.Country.taxCode;
        // orderDetail.taxRate = order.Country.taxRate;
        // orderDetail.taxName = order.Country.taxName;
        
        // orderDetail.priceExcludeTax = (0.00).toFixed(2);
        // orderDetail.taxPrice = (0.00).toFixed(2);
        // if(order.Country.includedTaxToProduct) {
        //   orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
        //   orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
        // }

        order.Receipt.priceExcludeTax = (0.00).toFixed(2);
        order.Receipt.taxPrice = (0.00).toFixed(2);
        order.Receipt.cashRebate = (0.00).toFixed(2);
        order.Receipt.totalPayable = (order.Receipt.totalPrice - order.Receipt.cashRebate).toFixed(2);
        if(order.hasShippingFee) {
          order.Receipt.priceExcludeTax = (order.Receipt.shippingFee / (1 + (order.Country.taxRate / 100))).toFixed(2);
          order.Receipt.taxPrice = (order.Receipt.shippingFee - order.Receipt.priceExcludeTax).toFixed(2);
        }
        
        order.priceExcludeTax = (order.Receipt.totalPrice / (1 + (order.Country.taxRate / 100))).toFixed(2);
        order.taxPrice = (order.Receipt.totalPrice - order.priceExcludeTax).toFixed(2);
        // order.checkMysConfirm = order.countryCode === 'mys';

        
        order.periodTime = orderDetail.PlanCountry.Plan.PlanType.subsequentDeliverDuration;
        order.planTrialProducts = orderDetail.PlanCountry.planTrialProducts;
        order.planTrialProducts.forEach(product => {
          product.imgUrl = _.findWhere(product.ProductCountry.Product.productImages, {isDefault: true}).url;
          product.productName = _.findWhere(product.ProductCountry.Product.productTranslate, {langCode: order.Country.defaultLang.toUpperCase()}) ?
                                _.findWhere(product.ProductCountry.Product.productTranslate, {langCode: order.Country.defaultLang.toUpperCase()}).name :
                                _.findWhere(product.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
          product.currency = order.Country.currencyDisplay;
          product.totalPrice = (product.ProductCountry.sellPrice * product.qty).toFixed(2);
        });
        
        order.planDetails = orderDetail.PlanCountry.planDetails;
        order.planDetails.forEach(detail => {
          detail.imgUrl = _.findWhere(detail.ProductCountry.Product.productImages, {isDefault: true}).url;
          detail.productName = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: order.Country.defaultLang.toUpperCase()}) ?
                               _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: order.Country.defaultLang.toUpperCase()}).name :
                               _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
          detail.currency = order.Country.currencyDisplay;
          detail.qty = JSON.parse(detail.deliverPlan)[1];
        });

        this.moment = DatetimeHelper.setMomentLocale(this.moment, options.langCode);
        order.receiveDate = this.moment().format(this.dateFormat);
        order.createdAt = this.moment(order.createdAt).format(this.dateFormat);
        // if(order.countryCode === 'kor') {
        //   order.nextReceiveDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery, config.deliverDateForKorea).format('DD-MMM-YYYY');
        // } else {
        //   if(!order.isOffline) {
        //     order.nextReceiveDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
        //   } else {
        //     if(order.countryCode !== 'mys') {
        //       order.nextReceiveDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery).format('DD-MMM-YYYY');
        //     } else {
        //       order.nextReceiveDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
        //     }
        //   }
        // }
        order.nextReceiveDate = order.startDeliverDate;

        order.pricePerCharge = orderDetail.PlanCountry.pricePerCharge;

        console.log(`order === ${JSON.stringify(order)}`);

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.trialPlanTmp);
        if(order.countryCode === 'kor' && moment().isBefore(moment(config.deliverDateForKorea))) {
          template = Handlebars.compile(this.trialPlanPreOrderTmp);
        }
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(order);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml,
          countryCode: order.countryCode
          // checkMysConfirm: order.checkMysConfirm
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: order.email
        });
      })
      .then(() => {
        if(checkMysConfirm || isBaSaleMys) {
          if(orderState !== 'Canceled') {
            this.sendReceiptsEmail(options, db);
          }
        }
      })
      .catch(err => console.log(`Send receipt trial email error : ${err.stack || JSON.stringify(err)}`));
  }

  // send receipts email
  sendUpdateTrialEmail(options, db) {
    // get trial plan details
    return db.Subscription.findOne({
      where: {id: options.id},
      include: ['User',
        {
          model: db.PlanCountry,
          include: [
            'Country',
            {
              model: db.Plan,
              include: [
                {
                  model: db.PlanType,
                  as: 'PlanType',
                  include: ['planTypeTranslate']
                },
                'planTranslate', 'planImages'
              ]
            }, {
              model: db.PlanDetail,
              as: 'planDetails',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }, {
              model: db.PlanTrialProduct,
              as: 'planTrialProducts',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        },
        {
          model: db.CustomPlan,
          include: [
            'Country',
            {
              model: db.PlanType,
              as: 'PlanType',
              include: ['planTypeTranslate']
            },
            {
              model: db.CustomPlanDetail,
              as: 'customPlanDetail',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        }
      ]
    })
      .then(subscription => {
        subscription = SequelizeHelper.convertSeque2Json(subscription);
        let emailTitle = in18.getMessage('email', 'user_update_trial_confirm_title', options.langCode);
        let emailSubject = in18.getMessage('email', 'user_update_trial_confirm_subject', options.langCode);

        if(subscription.User && subscription.User.firstName) {
          subscription.displayName = subscription.User.firstName;
        } else if(subscription.fullName) {
          subscription.displayName = subscription.fullName;
        }
        
        let country;
        // build plan details data
        if(subscription.PlanCountry) {
          country = subscription.PlanCountry.Country;
          subscription.PlanCountry.planDetails.forEach(detail => {
            detail.imgUrl = _.findWhere(detail.ProductCountry.Product.productImages, {isDefault: true}).url;
            let productTranslate = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()});
            if(!productTranslate) {
              productTranslate = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
            }
            detail.productName = productTranslate.name;
            detail.currency = subscription.PlanCountry.Country.currencyDisplay;
            detail.qty = JSON.parse(detail.deliverPlan)[1];
          });
        }
        if(subscription.CustomPlan) {
          country = subscription.CustomPlan.Country;
          let description = '';
          subscription.CustomPlan.customPlanDetail.forEach((detail, index) => {
            let productTranslate = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.CustomPlan.Country.defaultLang.toUpperCase()});
            if(!productTranslate) {
              productTranslate = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
            }
            detail.productName = productTranslate.name;
            description = index !== 0 ? `${description}, ` : description;
            description = `${description}${detail.qty} x ${detail.productName} (${detail.ProductCountry.Product.sku})`;
          });
          subscription.CustomPlan.customPlanDetail = [{description}];
        }
        
        // get link to order detail
        let expireAt = moment().add(config.email.expiredTime);
        if(subscription.User.isActive) {
          subscription.link = `${config.webUrl}user/shave-plans?utm_source=mandrill&utm_medium=email&utm_campaign=_${country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          subscription.isNewUser = false;
        } else if(!subscription.User.isGuest) {
          subscription.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: subscription.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${subscription.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          subscription.isActiveUser = true;
        } else {
          let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${subscription.User.email}&token=${jwt.sign({ token: expireAt, email: subscription.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/orders/${subscription.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
          subscription.link = activeAndViewOrderLink;
          subscription.isNewUser = true;
        }

        this.moment = DatetimeHelper.setMomentLocale(this.moment, options.langCode);
        // build email data
        subscription.startDate = this.moment(subscription.startDeliverDate).format(this.dateFormat);
        subscription.requestOn = this.moment().format(this.dateFormat);
        if(subscription.PlanCountry) {
          let planTranslate = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()});
          if(!planTranslate) {
            _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
          }
          subscription.planName = planTranslate.name;
          let planTypeTranslate = _.findWhere(subscription.PlanCountry.Plan.PlanType.planTypeTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()});
          if(!planTypeTranslate) {
            _.findWhere(subscription.PlanCountry.Plan.PlanType.planTypeTranslate, {langCode: 'EN'});
          }
          subscription.periodTime = planTypeTranslate.name;
          subscription.currency = subscription.PlanCountry.Country.currencyDisplay;
          subscription.countryCode = subscription.PlanCountry.Country.code.toLowerCase();
        }
        if(subscription.CustomPlan) {
          subscription.planName = subscription.CustomPlan.description;
          let planTypeTranslate = _.findWhere(subscription.CustomPlan.PlanType.planTypeTranslate, {langCode: subscription.CustomPlan.Country.defaultLang.toUpperCase()});
          if(!planTypeTranslate) {
            _.findWhere(subscription.CustomPlan.PlanType.planTypeTranslate, {langCode: 'EN'});
          }
          subscription.periodTime = planTypeTranslate.name;
          subscription.currency = subscription.CustomPlan.Country.currencyDisplay;
          subscription.countryCode = subscription.CustomPlan.Country.code.toLowerCase();
        }

        if(subscription.nextDeliverDate) {
          subscription.nextDeliverDate = this.moment(subscription.nextDeliverDate).format(this.dateFormat);
        } else {
          let currDate = new Date(subscription.createdAt);
          subscription.nextDeliverDate1 = this.moment(currDate.setDate(currDate.getDate() + 13)).format(this.dateFormat);
          subscription.nextDeliverDate2 = this.moment(currDate.setDate(currDate.getDate() + 3)).format(this.dateFormat);
        }

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.updateTrialPlanTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(subscription);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: subscription.email
        });
      })
      .catch(err => console.log(`Send update trial email error : ${err.stack || JSON.stringify(err)}`));
  }

  // send receipts email
  sendFollowUpTrialEmail(options, db) {
    // get trial plan details
    return db.Subscription.findOne({
      where: {id: options.id},
      include: ['User', {
        model: db.PlanCountry,
        include: [
          'Country',
          {
            model: db.Plan,
            include: ['PlanType', 'planTranslate', 'planImages']
          }, {
            model: db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: db.ProductCountry,
              include: [{
                model: db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }, {
            model: db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: db.ProductCountry,
              include: [{
                model: db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }
        ]
      }]
    })
      .then(subscription => {
        subscription = SequelizeHelper.convertSeque2Json(subscription);
        let emailTitle;
        let emailSubject;
        if(options.type === '5days') {
          emailTitle = in18.getMessage('email', 'user_trial_follow_1_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_trial_follow_1_subject', options.langCode);
          subscription.is5Days = true;
        } else if(options.type === 1 && subscription.isOffline) {
          emailTitle = in18.getMessage('email', 'user_trial_follow_2_title_ol', options.langCode);
          emailSubject = in18.getMessage('email', 'user_trial_follow_2_subject_ol', options.langCode);
          subscription.isFirst = true;
        } else if(options.type === 1 && !subscription.isOffline) {
          emailTitle = in18.getMessage('email', 'user_trial_follow_2_title_off', options.langCode);
          emailSubject = in18.getMessage('email', 'user_trial_follow_2_subject_off', options.langCode);
          subscription.isFirst = true;
        } else {
          emailTitle = in18.getMessage('email', 'user_trial_follow_3_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_trial_follow_3_subject', options.langCode);
        }

        if(subscription.User && subscription.User.firstName) {
          subscription.displayName = subscription.User.firstName;
        } else if(subscription.fullName) {
          subscription.displayName = subscription.fullName;
        }
        
        // build plan details data
        subscription.PlanCountry.planDetails.forEach(detail => {
          detail.imgUrl = _.findWhere(detail.ProductCountry.Product.productImages, {isDefault: true}).url;
          detail.productName = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                               _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name : 
                               _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
          detail.currency = subscription.PlanCountry.Country.currencyDisplay;
          detail.qty = JSON.parse(detail.deliverPlan)[1];
        });
        
        // get link to order detail
        let expireAt = moment().add(config.email.expiredTime);
        if(subscription.User.isActive) {
          subscription.link = `${config.webUrl}user/shave-plans?utm_source=mandrill&utm_medium=email&utm_campaign=_${subscription.PlanCountry.Country.code.toLowerCase()}_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web`;
          subscription.isNewUser = false;
        } else if(!subscription.User.isGuest) {
          subscription.link = `${config.webUrl}login/${jwt.sign({ token: expireAt, email: subscription.User.email }, config.accessToken.secret)}?nextPage=/user/orders/${subscription.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${subscription.PlanCountry.Country.code.toLowerCase()}_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web`;
          subscription.isActiveUser = true;
        } else {
          let activeAndViewOrderLink = `${config.webUrl}users/reset-password?email=${subscription.User.email}&token=${jwt.sign({ token: expireAt, email: subscription.User.email }, config.accessToken.secret)}&isActive=true&nextPage=/user/orders/${subscription.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${subscription.PlanCountry.Country.code.toLowerCase()}_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web`;
          subscription.link = activeAndViewOrderLink;
          subscription.isNewUser = true;
        }

        this.moment = DatetimeHelper.setMomentLocale(this.moment, options.langCode);
        // build email data
        subscription.planName = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name;
        subscription.startDate = this.moment(subscription.startDeliverDate).format(this.dateFormat);
        subscription.periodTime = subscription.PlanCountry.Plan.PlanType.subsequentDeliverDuration;
        subscription.currency = subscription.PlanCountry.Country.currencyDisplay;

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.followUpTrialPlanTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(subscription);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: subscription.email
        });
      })
      .catch(err => console.log(`Send Follow Up Trial email error : ${err.stack || JSON.stringify(err)}`));
  }

  // send cancel free trial
  sendCancelTrialEmail(options, db) {
    loggingHelper.log('info', 'sendCancelTrialEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    // get trial plan details
    return db.Subscription.findOne({
      where: {id: options.id},
      include: [
        'User', 'PlanOption', 
        {
          model: db.PlanCountry,
          include: [
            'Country',
            {
              model: db.Plan,
              include: ['PlanType', 'planTranslate', 'planImages']
            }, {
              model: db.PlanDetail,
              as: 'planDetails',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }, {
              model: db.PlanTrialProduct,
              as: 'planTrialProducts',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        },
        {
          model: db.CustomPlan,
          include: [
            'Country',
            {
              model: db.PlanType,
              as: 'PlanType',
              include: ['planTypeTranslate']
            },
            {
              model: db.CustomPlanDetail,
              as: 'customPlanDetail',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        }
      ]
    })
      .then(subscription => {
        subscription = SequelizeHelper.convertSeque2Json(subscription);
        let emailTitle = in18.getMessage('email', 'user_cancel_trial_title', options.langCode);
        let emailSubject = in18.getMessage('email', 'user_cancel_trial_subject', options.langCode);
        if(subscription.CustomPlan) {
          emailTitle = in18.getMessage('email', 'user_cancel_custom_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_cancel_custom_subject', options.langCode);
        }
        
        this.moment = DatetimeHelper.setMomentLocale(this.moment, options.langCode);
        // build email data
        subscription.startDate = this.moment(subscription.startDeliverDate).format(this.dateFormat);
        subscription.createdAt = this.moment(subscription.createdAt).format(this.dateFormat);

        if(subscription.PlanCountry) {
          // build plan details data
          subscription.PlanCountry.planDetails.forEach(detail => {
            detail.imgUrl = _.findWhere(detail.ProductCountry.Product.productImages, {isDefault: true}).url;
            detail.productName = _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                 _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                 _.findWhere(detail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
            detail.currency = subscription.PlanCountry.Country.currencyDisplay;
            detail.qty = JSON.parse(detail.deliverPlan)[1];
            detail.totalPrice = (detail.ProductCountry.sellPrice * detail.qty).toFixed(2);
            detail.planName = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                              _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                              _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name;
          });
          
          // build email data
          subscription.planName = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                  _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                  _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name;
          subscription.periodTime = subscription.PlanCountry.Plan.PlanType.subsequentDeliverDuration;
          subscription.currency = subscription.PlanCountry.Country.currencyDisplay;
          
          subscription.PlanCountry.Plan.planImages = _.findWhere(subscription.PlanCountry.Plan.planImages, {isDefault: true});
          subscription.PlanCountry.Plan.planTranslate = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                                        _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) :
                                                        _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
        }

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.cancelTrialPlanTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(subscription);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: subscription.email
        });
      })
      .catch(err => console.log(`Send cancel trial error : ${err.stack || JSON.stringify(err)}`));
  }

  // send cancel free trial
  sendCancelTrialRequestEmail(options, db) {
    // get trial plan details
    return db.Subscription.findOne({
      where: {id: options.id},
      include: ['User', {
        model: db.PlanCountry,
        include: [
          'Country',
          {
            model: db.Plan,
            include: ['PlanType', 'planTranslate', 'planImages']
          }, {
            model: db.PlanDetail,
            as: 'planDetails',
            include: [{
              model: db.ProductCountry,
              include: [{
                model: db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }, {
            model: db.PlanTrialProduct,
            as: 'planTrialProducts',
            include: [{
              model: db.ProductCountry,
              include: [{
                model: db.Product,
                include: ['productImages', 'productTranslate']
              }]
            }]
          }
        ]
      }]
    })
      .then(subscription => {
        subscription = SequelizeHelper.convertSeque2Json(subscription);
        let emailTitle;
        let emailSubject;

        if(options.isUndo) {
          emailTitle = in18.getMessage('email', 'user_cancel_trial_follow_1_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_cancel_trial_follow_1_subject', options.langCode);
          subscription.isUndo = true;
        } else if(options.isCanceled) {
          emailTitle = in18.getMessage('email', 'user_cancel_trial_follow_2_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_cancel_trial_follow_2_subject', options.langCode).replace('{value}', subscription.id);
          subscription.isCanceled = true;
        } else {
          emailTitle = in18.getMessage('email', 'user_cancel_trial_follow_3_title', options.langCode);
          emailSubject = in18.getMessage('email', 'user_cancel_trial_follow_3_subject', options.langCode);
          subscription.isRequestCancel = true;
        }
        
        this.moment = DatetimeHelper.setMomentLocale(this.moment, options.langCode);
        // build email data
        subscription.lastDeliverydate = moment(subscription.lastDeliverydate).isValid() ? this.moment(subscription.lastDeliverydate).format(this.dateFormat) : 'NA';
        subscription.nextDeliverDate = subscription.nextDeliverDate ? this.moment(subscription.nextDeliverDate).format(this.dateFormat) : 'NA';
        subscription.requestDate = this.moment().format(this.dateFormat);
        subscription.planName = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'}).name;

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.cancelTrialPlanRequestTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(subscription);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: config.email.cancelTrialCSList
        });
      })
      .catch(err => console.log(`Send cancel trial request email error : ${err.stack || JSON.stringify(err)}`));
  }

  // send subscription-renewal-failed
  sendAutoRenewalError(options, db) {
    loggingHelper.log('info', 'sendAutoRenewalError start');
    loggingHelper.log('info', `param: options = ${options}`);
    return db.Subscription.findOne({
      where: {id: options.subscriptionId},
      include: [
        'Card',
        {
          model: db.User,
          include: ['Country']
        },
        {
          model: db.PlanCountry,
          include: [
            'Country',
            {
              model: db.Plan,
              include: ['PlanType', 'planTranslate', 'planImages']
            }, {
              model: db.PlanDetail,
              as: 'planDetails',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        }
      ]
    })
      .then(subscription => {
        subscription = SequelizeHelper.convertSeque2Json(subscription);
        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.autoRenewalErrorTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);

        // build delivery detail
        console.log(`orderDetail.deliveryDetails ==== ${JSON.stringify(subscription)}`);
        subscription.PlanCountry.Plan.planTranslate = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.User.Country.defaultLang.toUpperCase()}) ?
                                                      _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.User.Country.defaultLang.toUpperCase()}) :
                                                      _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
        subscription.PlanCountry.Plan.planImages = _.findWhere(subscription.PlanCountry.Plan.planImages, {isDefault: true});
        subscription.chargePerTime = (subscription.PlanCountry.sellPrice / subscription.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);

        // build delivery product
        subscription.deliveryDetails = [];
        let deliveryDetail;
        subscription.PlanCountry.planDetails.forEach(planDetail => {
          let deliverPlan = JSON.parse(planDetail.deliverPlan);
          let times = Object.keys(deliverPlan);
          times.forEach(time => {
            time = parseInt(time);
            let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: subscription.User.Country.defaultLang.toUpperCase()}) ?
                              _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: subscription.User.Country.defaultLang.toUpperCase()}).name :
                              _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
            deliveryDetail = _.findWhere(subscription.deliveryDetails, {time});
            if(deliveryDetail) {
              let product = _.findWhere(deliveryDetail.productNames, {productName});
              if(product) {
                product.qty += deliverPlan[time];
              } else if(deliverPlan[time] !== 0) {
                deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
              }
            } else if(deliverPlan[time] !== 0) {
              deliveryDetail = {
                time,
                productNames: [{productName, qty: deliverPlan[time]}],
              };
              subscription.deliveryDetails.push(Object.assign({}, deliveryDetail));
            }
          });
        });
        // TODO work around for design
        let tmp = [];
        subscription.deliveryDetails.forEach(obj => {
          if(obj.time === 1) {
            obj.isFirst = true;
          } else {
            obj.notFirst = true;
          }
          if(obj.time < 3) {
            tmp.push(obj);
          }
        });
        subscription.deliveryDetails = tmp;


        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: in18.getMessage('email', 'user_auto_renew_error_title', options.langCode)
        });
        let bodyHtml = template({
          firstName: subscription.User.firstName,
          link: `${config.webUrl}user/subscriptions?id=${subscription.id}&utm_source=mandrill&utm_medium=email&utm_campaign=_${subscription.User.Country.code.toLowerCase()}_renewerror&utm_content=S2U_html_renewerror&utm_term=_renewerror_web`,
          subscription
        });
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: in18.getMessage('email', 'user_auto_renew_error_subject', options.langCode),
          html: emailHtml,
          to: subscription.User.email
        });
      })
      .catch(err => {
        console.log(`Send subscription renewal error : ${err.stack || JSON.stringify(err)}`);
        loggingHelper.log('error', `Send subscription renewal error : ${err.stack || JSON.stringify(err)}`);
      });
  }

  sendPaymentFail(options, db) {
    loggingHelper.log('info', 'sendPaymentFail start');
    loggingHelper.log('info', `param: options = ${options}`);
    let user;
    return db.User.findOne({
      where: { id: options.userId },
      include: [
        'Country',
        {
          model: db.Cart,
          include: [
            {
              model: db.CartDetail,
              as: 'cartDetails',
              include: [
                {
                  model: db.ProductCountry,
                  include: [
                    'Country',
                    {
                      model: db.Product,
                      include: ['productTranslate', 'productImages']
                    }
                  ]
                },
                {
                  model: db.PlanCountry,
                  include: [
                    'Country',
                    {
                      model: db.Plan,
                      include: ['PlanType', 'planTranslate', 'planImages']
                    }, {
                      model: db.PlanDetail,
                      as: 'planDetails',
                      include: [{
                        model: db.ProductCountry,
                        include: [{
                          model: db.Product,
                          include: ['productImages', 'productTranslate']
                        }]
                      }]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(_user => {
        user = SequelizeHelper.convertSeque2Json(_user);
        let items = [];
        user.Cart.cartDetails.forEach(cartDetail => {
          cartDetail.totalPrice = 0;

          if(cartDetail.ProductCountry) {
            cartDetail.ProductCountry.Product.productTranslate = _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.ProductCountry.Country.defaultLang.toUpperCase()}) ?
                                                                 _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.ProductCountry.Country.defaultLang.toUpperCase()}) :
                                                                 _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
            cartDetail.ProductCountry.Product.productImages = _.findWhere(cartDetail.ProductCountry.Product.productImages, {isDefault: true});
            cartDetail.totalPrice = (cartDetail.ProductCountry.sellPrice * cartDetail.qty).toFixed(2);
            cartDetail.type = 'ALA-CARTE';
            cartDetail.isProduct = true;
            cartDetail.currency = cartDetail.ProductCountry.Country.currencyDisplay;
          }
          if(cartDetail.PlanCountry) {
            cartDetail.PlanCountry.Plan.planTranslate = _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                                        _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLang.toUpperCase()}) :
                                                        _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
            cartDetail.PlanCountry.Plan.planImages = _.findWhere(cartDetail.PlanCountry.Plan.planImages, {isDefault: true});
            cartDetail.unitPrice = (cartDetail.PlanCountry.sellPrice * cartDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
            cartDetail.totalPrice = (cartDetail.PlanCountry.sellPrice * cartDetail.qty).toFixed(2);
            cartDetail.type = 'SUBSCRIPTION';
            cartDetail.isPlan = true;
            cartDetail.currency = cartDetail.PlanCountry.Country.currencyDisplay;

            // build delivery product
            cartDetail.deliveryDetails = [];
            let deliveryDetail;
            cartDetail.PlanCountry.planDetails.forEach(planDetail => {
              let deliverPlan = JSON.parse(planDetail.deliverPlan);
              let times = Object.keys(deliverPlan);
              times.forEach(time => {
                time = parseInt(time);
                let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                  _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                  _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
                deliveryDetail = _.findWhere(cartDetail.deliveryDetails, {time});
                if(deliveryDetail) {
                  let product = _.findWhere(deliveryDetail.productNames, {productName});
                  if(product) {
                    product.qty += deliverPlan[time];
                  } else if(deliverPlan[time] !== 0) {
                    deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                  }
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail = {
                    time,
                    productNames: [{productName, qty: deliverPlan[time]}],
                  };
                  cartDetail.deliveryDetails.push(Object.assign({}, deliveryDetail));
                }
              });
            });
            // TODO work around for design
            let tmp = [];
            cartDetail.deliveryDetails.forEach(obj => {
              if(obj.time === 1) {
                obj.isFirst = true;
              } else {
                obj.notFirst = true;
              }
              if(obj.time < 3) {
                tmp.push(obj);
              }
            });
            cartDetail.deliveryDetails = tmp;
          }
          items.push(cartDetail);
        });
        return items;
      })
      .then(data => {
        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.paymentFailTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: in18.getMessage('email', 'user_payment_fail_title', options.langCode)
        });
        let bodyHtml = template({
          firstName: user.firstName,
          link: `${config.webUrl}user/settings?utm_source=mandrill&utm_medium=email&utm_campaign=_${user.Country.code.toLowerCase()}_paymentfail&utm_content=S2U_html_paymentfail&utm_term=_paymentfail_web`,
          data
        });
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: in18.getMessage('email', 'user_payment_fail_subject', options.langCode),
          html: emailHtml,
          to: user.email
        });
      })
      .catch(err => {
        console.log(`Send payment fail error : ${err.stack || JSON.stringify(err)}`);
        loggingHelper.log('error', `Send payment fail error : ${err.stack || JSON.stringify(err)}`);
      });
  }

  sendChargeSubscriptionFail(options, db) {
    loggingHelper.log('info', 'sendChargeSubscriptionFail start');
    loggingHelper.log('info', `param: options = ${options}`);
    let subscription;
    let country;
    return db.Subscription.findOne({
      where: { id: options.subscriptionId },
      include: [
        'User',
        {
          model: db.PlanCountry,
          include: [
            'Country',
            {
              model: db.Plan,
              include: ['PlanType', 'planTranslate', 'planImages']
            }, {
              model: db.PlanDetail,
              as: 'planDetails',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        },
        {
          model: db.CustomPlan,
          include: [
            'Country',
            {
              model: db.PlanType,
              as: 'PlanType',
              include: ['planTypeTranslate']
            },
            {
              model: db.CustomPlanDetail,
              as: 'customPlanDetail',
              include: [{
                model: db.ProductCountry,
                include: [{
                  model: db.Product,
                  include: ['productImages', 'productTranslate']
                }]
              }]
            }
          ]
        }
      ]
    })
      .then(_subscription => {
        subscription = SequelizeHelper.convertSeque2Json(_subscription);
        let item = { 
          PlanCountry: 
          subscription.PlanCountry ? 
          subscription.PlanCountry : 
          {
            Plan: {
              planTranslate: {name: ''},
              planImages: {},
              PlanType: {}
            },
            sellPrice: 0
          }
        };
        let items = [];
        country = subscription.PlanCountry ? subscription.PlanCountry.Country : subscription.CustomPlan.Country;

        if(subscription.PlanCountry) {
          item.PlanCountry.Plan.planTranslate = _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}) :
                                                _.findWhere(subscription.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
          item.PlanCountry.Plan.planImages = _.findWhere(subscription.PlanCountry.Plan.planImages, {isDefault: true});
        } else if(subscription.CustomPlan) {
          item.PlanCountry.Plan.planTranslate.name = subscription.CustomPlan.description;
          item.PlanCountry.Plan.PlanType = subscription.CustomPlan.PlanType;
          item.PlanCountry.sellPrice = subscription.pricePerCharge;
        }
        item.unitPrice = subscription.CustomPlan ? subscription.pricePerCharge : (subscription.PlanCountry.sellPrice * subscription.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
        item.totalPrice = subscription.CustomPlan ? subscription.pricePerCharge : (subscription.PlanCountry.sellPrice * subscription.qty).toFixed(2);
        item.type = 'SUBSCRIPTION';
        item.isPlan = true;
        item.qty = subscription.qty;
        item.currency = subscription.CustomPlan ? subscription.CustomPlan.Country.currencyDisplay : subscription.PlanCountry.Country.currencyDisplay;

        // build delivery product
        item.deliveryDetails = [];
        let deliveryDetail;
        if(subscription.PlanCountry) {
          subscription.PlanCountry.planDetails.forEach(planDetail => {
            let deliverPlan = JSON.parse(planDetail.deliverPlan);
            let times = Object.keys(deliverPlan);
            times.forEach(time => {
              time = parseInt(time);
              let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: item.PlanCountry.Country.defaultLang.toUpperCase()}) ?
                                _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: item.PlanCountry.Country.defaultLang.toUpperCase()}).name :
                                _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'}).name;
              deliveryDetail = _.findWhere(item.deliveryDetails, {time});
              if(deliveryDetail) {
                let product = _.findWhere(deliveryDetail.productNames, {productName});
                if(product) {
                  product.qty += deliverPlan[time];
                } else if(deliverPlan[time] !== 0) {
                  deliveryDetail.productNames.push({productName, qty: deliverPlan[time]});
                }
              } else if(deliverPlan[time] !== 0) {
                deliveryDetail = {
                  time,
                  productNames: [{productName, qty: deliverPlan[time]}],
                };
                item.deliveryDetails.push(Object.assign({}, deliveryDetail));
              }
            });
          });
        }
        // TODO work around for design
        let tmp = [];
        item.deliveryDetails.forEach(obj => {
          if(obj.time === 1) {
            obj.isFirst = true;
          } else {
            obj.notFirst = true;
          }
          if(obj.time < 3) {
            tmp.push(obj);
          }
        });
        item.deliveryDetails = tmp;
        items.push(item);
        return items;
      })
      .then(data => {
        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.paymentFailTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: in18.getMessage('email', 'user_charge_subscription_fail_title', options.langCode)
        });
        let bodyHtml = template({
          firstName: subscription.User.firstName,
          link: `${config.webUrl}user/settings?utm_source=mandrill&utm_medium=email&utm_campaign=_${country.code.toLowerCase()}_paymentfail&utm_content=S2U_html_paymentfail&utm_term=_paymentfail_web`,
          data
        });
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml
        });
        return this.mailChimp.sendTransactionEmail({
          subject: in18.getMessage('email', 'user_charge_subscription_fail_subject', options.langCode),
          html: emailHtml,
          to: subscription.User.email
        });
      })
      .catch(err => {
        console.log(`Send subscription renewal error : ${err.stack || JSON.stringify(err)}`);
        loggingHelper.log('error', `Send Charge Subscription Fail error : ${err.stack || JSON.stringify(err)}`);
      });
  }

  sendRenewalConfirm(options) {
    loggingHelper.log('info', 'sendRenewalConfirm start');
    loggingHelper.log('info', `param: options = ${options}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.renewalConfirmTmp);
    let logoTemplate = Handlebars.compile(this.centerLogoTmp);
    let logoHtml = logoTemplate({
      homepage: config.webUrl
    });
    let bodyHtml = template({
      firstName: options.firstName,
      header: `Congrats, ${options.firstName}! Your Shave Plan has been renewed.`,
      link: `${config.webUrl}user/shave-plans?utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_renewalconfirm&utm_content=S2U_html_renewalconfirm&utm_term=_renewalconfirm_web`
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: bodyHtml
    });
    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_renew_confirm', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  sendPendingCart(options) {
    loggingHelper.log('info', 'sendPendingCart start');
    loggingHelper.log('info', `param: user = ${options}`);
    let baseTemplate = Handlebars.compile(this.baseTmp);
    let template = Handlebars.compile(this.pendingCartTmp);
    let logoTemplate = Handlebars.compile(this.leftLogoTmp);
    let logoHtml = logoTemplate({
      homepage: config.webUrl,
      title: 'Complete Your Order'
    });

    // build cartDetails
    options.Cart.cartDetails.forEach(cartDetail => {
      if(cartDetail.ProductCountry) {
        cartDetail.translate = _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.ProductCountry.Country.defaultLanguage}) ?
                               _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: cartDetail.ProductCountry.Country.defaultLanguage}) :
                               _.findWhere(cartDetail.ProductCountry.Product.productTranslate, {langCode: 'EN'});
        cartDetail.image = _.findWhere(cartDetail.ProductCountry.Product.productImages, {isDefault: true});
        cartDetail.unitPrice = cartDetail.ProductCountry.sellPrice;
        cartDetail.totalPrice = (cartDetail.ProductCountry.sellPrice * cartDetail.qty).toFixed(2);
        cartDetail.sku = cartDetail.ProductCountry.Product.sku;
        cartDetail.currency = cartDetail.ProductCountry.Country.currencyDisplay;
      }
      if(cartDetail.PlanCountry) {
        cartDetail.translate = _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLanguage}) ?
                               _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: cartDetail.PlanCountry.Country.defaultLanguage}) :
                               _.findWhere(cartDetail.PlanCountry.Plan.planTranslate, {langCode: 'EN'});
        cartDetail.image = _.findWhere(cartDetail.PlanCountry.Plan.planImages, {isDefault: true});
        cartDetail.unitPrice = (cartDetail.PlanCountry.sellPrice * cartDetail.PlanCountry.Plan.PlanType.totalChargeTimes).toFixed(2);
        cartDetail.totalPrice = (cartDetail.PlanCountry.sellPrice * cartDetail.qty).toFixed(2);
        cartDetail.sku = cartDetail.PlanCountry.Plan.sku;
        cartDetail.currency = cartDetail.PlanCountry.Country.currencyDisplay;
      }
    });

    console.log(JSON.stringify(options.Cart.cartDetails));
    let bodyHtml = template({
      firstName: options.firstName,
      link: `${config.webUrl}cart?utm_source=mandrill&utm_medium=email&utm_campaign=_${options.Country.code.toLowerCase()}_cartremind&utm_content=S2U_html_cartremind&utm_term=_cartremind_web`,
      cartDetails: options.Cart.cartDetails
    });
    let emailHtml = baseTemplate({
      header: logoHtml,
      bodyContent: bodyHtml
    });
    return this.mailChimp.sendTransactionEmail({
      subject: in18.getMessage('email', 'user_pendding_cart', options.langCode),
      html: emailHtml,
      to: options.email
    });
  }

  // send receipts bulk order email
  sendReceiptBulkOrdersEmail(options, db) {
    loggingHelper.log('info', 'sendReceiptBulkOrdersEmail start');
    loggingHelper.log('info', `param: options = ${options}`);
    // get order detail
    return this._getBulkOrderData(options.orderId, db)
      .then(order => {
        let emailTitle;
        let emailSubject;
        // get link to order detail
        
        order.link = `${config.webUrl}`;

        // process status
        emailTitle = in18.getMessage('email', 'bulk_order_receipt_title', options.langCode);
        emailSubject = in18.getMessage('email', 'bulk_order_receipt_subject', options.langCode).replace('{value}', order.displayId);

        order.totalPrice = 0.00;
        order.bulkOrderDetail.forEach(orderDetail => {
          orderDetail.currency = order.Country.currencyDisplay;
          if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan && orderDetail.PlanCountry.Plan.isTrial) {
            order.isTrial = true;
          }

          orderDetail.taxCode = order.Country.taxCode;
          orderDetail.taxRate = order.Country.taxRate;
          orderDetail.taxName = order.Country.taxName;
          
          orderDetail.priceExcludeTax = (0.00).toFixed(2);
          orderDetail.taxPrice = (0.00).toFixed(2);
          if(order.Country.includedTaxToProduct) {
            orderDetail.priceExcludeTax = (orderDetail.price / (1 + (order.Country.taxRate / 100))).toFixed(2);
            orderDetail.taxPrice = (orderDetail.price - orderDetail.priceExcludeTax).toFixed(2);
          }
          order.totalPrice = parseFloat(order.totalPrice) + (parseFloat(orderDetail.price) * orderDetail.qty);
        });

        order.Receipt = {};
        order.Receipt.originPrice = order.totalPrice.toFixed(2);
        order.Receipt.discountAmount = (0.00).toFixed(2);
        order.Receipt.totalPrice = order.totalPrice.toFixed(2);
        order.Receipt.priceExcludeTax = (0.00).toFixed(2);
        order.Receipt.taxPrice = (0.00).toFixed(2);
        order.Receipt.cashRebate = (0.00).toFixed(2);
        order.Receipt.currency = order.Country.currencyDisplay;
        order.Receipt.totalPayable = (order.totalPrice - order.Receipt.cashRebate).toFixed(2);
        // if(order.hasShippingFee) {
        //   order.Receipt.priceExcludeTax = (order.Receipt.shippingFee / (1 + (order.Country.taxRate / 100))).toFixed(2);
        //   order.Receipt.taxPrice = (order.Receipt.shippingFee - order.Receipt.priceExcludeTax).toFixed(2);
        // }
        
        order.priceExcludeTax = (order.totalPrice / (1 + (order.Country.taxRate / 100))).toFixed(2);
        order.taxPrice = (order.totalPrice - order.priceExcludeTax).toFixed(2);

        // order.isOffline = !!order.SellerUserId;
        // if(!order.isOffline) {
        //   order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.webFirstDelivery).format('DD-MMM-YYYY');
        // } else {
        //   order.trialShippingDate = DatetimeHelper.getNextWorkingDays(config.trialPlan.baFirstDelivery).format('DD-MMM-YYYY');
        // }

        let baseTemplate = Handlebars.compile(this.baseTmp);
        let template = Handlebars.compile(this.receiptBulkOrderEmailTmp);
        let logoTemplate = Handlebars.compile(this.leftLogoTmp);
        let logoHtml = logoTemplate({
          homepage: config.webUrl,
          title: emailTitle
        });
        let bodyHtml = template(order);
        let emailHtml = baseTemplate({
          header: logoHtml,
          bodyContent: bodyHtml,
          countryCode: order.countryCode
        });
        return this.mailChimp.sendTransactionEmail({
          subject: emailSubject,
          html: emailHtml,
          to: order.email
        })
        .then(() => {
          let fileName = `TaxInvoice_${order.displayId}_${moment().format('YYYY-MM-DD_HHmmssSSS')}.pdf`;

          // return pdf.create(emailHtml, { format: 'Letter' }).toFile(`${config.taxInvoicePath}/${fileName}`, function(err, res) {
          //   if(err) return console.log('err ===== ', err);

          //   // upload file to S3
          //   let fileUpload = {
          //     fieldName: 'url',
          //     originalFilename: fileName,
          //     path: res.filename,
          //     name: fileName,
          //     type: 'application/pdf'
          //   };
            
          //   awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
          //   .then(uploadedFile => 
          //     db.FileUpload.create(
          //       {
          //         name: fileName,
          //         url: uploadedFile.Location,
          //         bulkOrderId: order.id,
          //         isDownloaded: false
  
          //       },
          //       {returning: true}
          //     )
          //     .then(() => 
          //       fs.unlink(res.filename, err => {
          //         if(err) throw err;
          //         console.log(`successfully deleted ${res.filename}`);
          //       })
          //     )
          //   );
          //   return res; // { filename: '/app/businesscard.pdf' }
          // });
          return new Promise((resolve, reject) => {
            pdf.create(emailHtml, { height: '2043px', width: '1444px' }).toFile(`${config.taxInvoicePath}/${fileName}`, (err, res) => {
              if(err) reject(err);
        
              // upload file to S3
              let fileUpload = {
                fieldName: 'url',
                originalFilename: fileName,
                path: res.filename,
                name: fileName,
                type: 'application/pdf'
              };
              
              awsHelper.upload(config.AWS.filesUploadAlbum, fileUpload, false, fileName)
                .then(uploadedFile => 
                  db.FileUpload.create(
                    {
                      name: fileName,
                      url: uploadedFile.Location,
                      bulkOrderId: order.id,
                      isDownloaded: false
          
                    },
                    {returning: true})
                )
                .then(() => 
                  fs.unlink(res.filename, err => {
                    if(err) reject(err);
                    resolve();
                  })
                )
                .catch(error => reject(error));
            });
          });
        });
      })
      .catch(err => {
        console.log(`Send receipt bulk order email error : ${err.stack || JSON.stringify(err)}`);
        loggingHelper.log('error', `Send receipt bulk order email error : ${err.stack || JSON.stringify(err)}`);
      });
  }

  // send sale report email
  sendSaleReportEmail(data, db) {
    loggingHelper.log('info', 'sendSaleReportEmail start');
    loggingHelper.log('info', `param: data = ${data}`);
    this.downloadController = new DownloadController(db);

    console.log('config.email.saleReportEmail', config.email.saleReportEmail[data.reportType.replace('_', '')]);
    let mailTo = config.email.saleReportEmail[data.reportType.replace('_', '')];
    let options = {where: {}};
    options.include = [{
      model: db.Country
    }];
    let reportType = data.reportType.split('_');

    // let emailTitle = in18.getMessage('email', 'bulk_order_receipt_title', options.langCode);
    let emailSubject = in18.getMessage('email', 'sale_report_subject', data.langCode).replace('{date}', moment().subtract(1, 'day').format('YYYY-MM-DD'));

    if(!reportType[1]) {
      // get sale report
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: new Date(moment(moment().subtract(1, 'day').format('YYYY-MM-DD')))}},
        {createdAt: {$lt: new Date(moment(moment().format('YYYY-MM-DD')))}}
      ]});
      return this.downloadController.downloadSalesReport(options)
        .then(result => {
          // make csv file
          let attachments = [{   // utf-8 string as an attachment
            filename: `Sales report for ${moment().subtract(1, 'day').format('YYYY-MM-DD')}.csv`,
            content: result
          }];
  
          let bodyData = {};
          bodyData.date = moment().subtract(1, 'day').format('YYYY-MM-DD');

          let baseTemplate = Handlebars.compile(this.baseTmp);
          let template = Handlebars.compile(this.saleReportEmailTmp);
          let logoTemplate = Handlebars.compile(this.leftLogoTmp);
          let logoHtml = logoTemplate({
            homepage: config.webUrl,
            // title: emailTitle
          });
          let bodyHtml = template(bodyData);
          let emailHtml = baseTemplate({
            header: logoHtml,
            bodyContent: bodyHtml
          });
          return this.mailChimp.sendTransactionEmail({
            subject: emailSubject,
            html: emailHtml,
            to: mailTo,
            attachments
          });
        })
        .catch(err => {
          console.log(`Send sale report email error : ${err.stack || JSON.stringify(err)}`);
          loggingHelper.log('error', `Send sale report email error : ${err.stack || JSON.stringify(err)}`);
        });
    } else if(reportType[1] && reportType[0] === 'appco') {
      // get sale report
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: new Date(moment(moment().subtract(1, 'day').format('YYYY-MM-DD')))}},
        {createdAt: {$lt: new Date(moment(moment().format('YYYY-MM-DD')))}},
        {region: reportType[1]},
        {category: 'sales app'}
      ]});
      return this.downloadController.downloadSalesReportForAppco(options)
        .then(result => {
          // make csv file
          let attachments = [{   // utf-8 string as an attachment
            filename: `Sales report Appco_${reportType[1]} for ${moment().subtract(1, 'day').format('YYYY-MM-DD')}.csv`,
            content: result
          }];
  
          let bodyData = {};
          bodyData.date = moment().subtract(1, 'day').format('YYYY-MM-DD');

          let baseTemplate = Handlebars.compile(this.baseTmp);
          let template = Handlebars.compile(this.saleReportEmailTmp);
          let logoTemplate = Handlebars.compile(this.leftLogoTmp);
          let logoHtml = logoTemplate({
            homepage: config.webUrl,
            // title: emailTitle
          });
          let bodyHtml = template(bodyData);
          let emailHtml = baseTemplate({
            header: logoHtml,
            bodyContent: bodyHtml
          });
          return this.mailChimp.sendTransactionEmail({
            subject: emailSubject,
            html: emailHtml,
            to: mailTo,
            attachments
          });
        })
        .catch(err => {
          console.log(`Send sale report(Appco) email error : ${err.stack || JSON.stringify(err)}`);
          loggingHelper.log('error', `Send sale report(Appco) email error : ${err.stack || JSON.stringify(err)}`);
        });
    } else if(reportType[1] && reportType[0] === 'mo') {
      // get sale report
      options.where = Object.assign(options.where, {$and: [
        {createdAt: {$gt: new Date(moment(moment().subtract(1, 'day').format('YYYY-MM-DD')))}},
        {createdAt: {$lt: new Date(moment(moment().format('YYYY-MM-DD')))}},
        {moCode: reportType[1]},
        {category: 'sales app'}
      ]});
      return this.downloadController.downloadSalesReportForMO(options)
        .then(result => {
          // make csv file
          let attachments = [{   // utf-8 string as an attachment
            filename: `Sales report Marketing Office(${reportType[1]}) for ${moment().subtract(1, 'day').format('YYYY-MM-DD')}.csv`,
            content: result
          }];
  
          let bodyData = {};
          bodyData.date = moment().subtract(1, 'day').format('YYYY-MM-DD');

          let baseTemplate = Handlebars.compile(this.baseTmp);
          let template = Handlebars.compile(this.saleReportEmailTmp);
          let logoTemplate = Handlebars.compile(this.leftLogoTmp);
          let logoHtml = logoTemplate({
            homepage: config.webUrl,
            // title: emailTitle
          });
          let bodyHtml = template(bodyData);
          let emailHtml = baseTemplate({
            header: logoHtml,
            bodyContent: bodyHtml
          });
          return this.mailChimp.sendTransactionEmail({
            subject: emailSubject,
            html: emailHtml,
            to: mailTo,
            attachments
          });
        })
        .catch(err => {
          console.log(`Send sale report(MO) email error : ${err.stack || JSON.stringify(err)}`);
          loggingHelper.log('error', `Send sale report(MO) email error : ${err.stack || JSON.stringify(err)}`);
        });
    }
  }

  sendSubscribersReportEmail(data, db) {
    this.downloadController = new DownloadController(db);

    let emailSubject = in18.getMessage('email', 'subscribers_report_subject', data.langCode).replace('{date}', moment().subtract(1, 'days').format('YYYY-MM-DD'));
    
    let options = {
      where: {
        $and: [
          { status: { $like: 'Canceled' } },
          { updatedAt: { $gte: new Date(moment(moment().subtract(1, 'days').format('YYYY-MM-DD'))) } },
          { updatedAt: { $lt: new Date(moment(moment().format('YYYY-MM-DD'))) } }
        ]
      },
      order: [
        ['updatedAt', 'DESC']
      ]
    };
    
    options.include = ['DeliveryAddress', 'BillingAddress', {
      model: db.User,
      include: ['Country']
    }, {
      model: db.PlanCountry,
      include: [{
        model: db.Plan,
        include: ['planImages', 'planTranslate', 'PlanType']
      }]
    }, {
      model: db.CustomPlan,
      include: [
        'PlanType',
        {
          model: db.CustomPlanDetail,
          as: 'customPlanDetail',
          include: [{
            model: db.ProductCountry,
            include: [{
              model: db.Product
            }]
          }]
        }
      ]
    }];

    return this.downloadController.downloadSubscribersReportForCronjob(options)
    .then(result => {
      // no results -> continue job on next time
      if(!result) return Promise.resolve();

      // make csv file
      let attachments = [{   // utf-8 string as an attachment
        filename: `Canceled Subscribers Report for ${moment().subtract(1, 'days').format('YYYY-MM-DD')}.csv`,
        content: result
      }];

      let bodyData = {};
      bodyData.date = moment().subtract(1, 'days').format('YYYY-MM-DD');

      let baseTemplate = Handlebars.compile(this.baseTmp);
      let template = Handlebars.compile(this.subscribersReportEmailTmp);
      let logoTemplate = Handlebars.compile(this.leftLogoTmp);
      let logoHtml = logoTemplate({
        homepage: config.webUrl,
        // title: emailTitle
      });
      let bodyHtml = template(bodyData);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml
      });
      return this.mailChimp.sendTransactionEmail({
        subject: emailSubject,
        html: emailHtml,
        to: config.email.SubscribersReportList,
        attachments
      });
    })
    .catch(err => {
      console.log(`Send Canceled Subscribers report email error : ${err.stack || JSON.stringify(err)}`);
      loggingHelper.log('error', `Send Canceled Subscribers report email error : ${err.stack || JSON.stringify(err)}`);
    });
  }

  sendUsersSaleReportEmail(data, db) {
    this.downloadController = new DownloadController(db);

    let emailSubject = in18.getMessage('email', 'users_sale_report_subject', data.langCode).replace('{date}', moment().subtract(1, 'days').format('YYYY-MM-DD'));
    
    let dateDistance = 1;

    let options = {
      where: {
        $and: [
          { updatedAt: { $gte: new Date(moment(moment().subtract(dateDistance, 'days').format('YYYY-MM-DD'))) } },
          { updatedAt: { $lt: new Date(moment(moment().format('YYYY-MM-DD'))) } }
        ]
      },
      order: [
        ['createdAt', 'DESC']
      ]
    };

    options.include = [
      'Country',
      {
        model: db.OrderDetail,
        as: 'orderDetail',
        include: [{
          model: db.ProductCountry,
          include: ['Product']
        }, {
          model: db.PlanCountry,
          include: ['Plan']
        }]
      }
    ];

    return this.downloadController.downloadUsersSaleReportForCronjob(options)
    .then(result => {
      if(!result) return Promise.resolve();

      // make csv file
      let attachments = [{
        filename: `Users_sale_report_${moment().subtract(1, 'days').format('YYYYMMDD')}.csv`,
        content: result
      }];

      let bodyData = {};
      bodyData.date = moment().subtract(1, 'days').format('YYYY-MM-DD');

      let baseTemplate = Handlebars.compile(this.baseTmp);
      let template = Handlebars.compile(this.usersSaleReportEmailTmp);
      let logoTemplate = Handlebars.compile(this.leftLogoTmp);
      let logoHtml = logoTemplate({
        homepage: config.webUrl
      });
      let bodyHtml = template(bodyData);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml
      });
      return this.mailChimp.sendTransactionEmail({
        subject: emailSubject,
        html: emailHtml,
        to: config.email.cronjobUsersSaleReportEmail,
        attachments
      });
    })
    .catch(err => {
      console.log(`Send Users sale report email error : ${err.stack || JSON.stringify(err)}`);
      loggingHelper.log('error', `Send Users sale report email error : ${err.stack || JSON.stringify(err)}`);
    });
  }

  sendUsersReportEmail(data, db) {
    this.downloadController = new DownloadController(db);

    let emailSubject = in18.getMessage('email', 'users_report_subject', data.langCode).replace('{date}', moment().subtract(1, 'days').format('YYYY-MM-DD'));
    
    let dateDistance = 1;

    let options = {
      where: {
        $and: [
          { status: { $not: 'On Hold' } },
          { updatedAt: { $gte: new Date(moment(moment().subtract(dateDistance, 'days').format('YYYY-MM-DD'))) } },
          { updatedAt: { $lt: new Date(moment(moment().format('YYYY-MM-DD'))) } }
        ]
      },
      order: [
        ['createdAt', 'DESC']
      ]
    };
    
    options.include = [
      'User',
      'CustomPlan',
      'DeliveryAddress',
      {
        model: db.PlanCountry,
        include: ['Plan']
      }
    ];

    if(db.SubscriptionHistory) {
      options.include.push({
        model: db.SubscriptionHistory,
        as: 'subscriptionHistories'
      });
    }

    return this.downloadController.downloadUsersReportForCronjob(options, dateDistance)
    .then(result => {
      if(!result) return Promise.resolve();

      // make csv file
      let attachments = [{
        filename: `Users Report for ${moment().subtract(1, 'days').format('YYYY-MM-DD')}.csv`,
        content: result
      }];

      let bodyData = {};
      bodyData.date = moment().subtract(1, 'days').format('YYYY-MM-DD');

      let baseTemplate = Handlebars.compile(this.baseTmp);
      let template = Handlebars.compile(this.usersReportEmailTmp);
      let logoTemplate = Handlebars.compile(this.leftLogoTmp);
      let logoHtml = logoTemplate({
        homepage: config.webUrl
      });
      let bodyHtml = template(bodyData);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml
      });
      return this.mailChimp.sendTransactionEmail({
        subject: emailSubject,
        html: emailHtml,
        to: config.email.cronjobUsersReportEmail,
        attachments
      });
    })
    .catch(err => {
      console.log(`Send Users report email error : ${err.stack || JSON.stringify(err)}`);
      loggingHelper.log('error', `Send Users report email error : ${err.stack || JSON.stringify(err)}`);
    });
  }

  sendSendSubscribersSaleReportEmail(data, db) {
    this.downloadController = new DownloadController(db);

    // get day of week index, sunday -> saturday: 0 -> 6
    let dayOfWeek = parseInt(moment().day());
    // set sunday index, if 0 set to 7
    dayOfWeek = (dayOfWeek != 0) ? dayOfWeek : 7;

    // get distance from lastest monday to today
    let distanceFromLastestMonday = (dayOfWeek > 1) ? dayOfWeek - 1 : 0;
    // get fromDate from config
    let fromDateConfig = config.email.cronjobSubscribersSaleReportConfig.fromDate;

    // if fromDate from config not presented -> set fromDate = lastest monday - 7days (second lastest moday)
    let fromDate = fromDateConfig ? moment(fromDateConfig) : moment().subtract(distanceFromLastestMonday + 7, 'days');
    // set toDate = lastest monday
    let toDate = moment().subtract(distanceFromLastestMonday, 'days');

    // format dates
    let fromDateFormated = fromDate.format('YYYY-MM-DD');
    let toDateFormated = toDate.format('YYYY-MM-DD');

    // toDate to display is always sunday -> so back 1 day from lastest monday
    let toDateDisplay = toDate.subtract(1, 'days');
    let toDateDisplayFormated = toDateDisplay.format('YYYY-MM-DD');

    let emailSubject = in18.getMessage('email', 'subscribers_sale_report_subject', data.langCode).replace('{fromDate}', fromDateFormated);
    emailSubject = emailSubject.replace('{toDate}', toDateDisplayFormated);

    let options = {
      where: {
        $and: [
          { createdAt: { $gte: new Date(moment(fromDateFormated)) } },
          { createdAt: { $lt: new Date(moment(toDateFormated)) } },
          { status: { $not: 'On Hold' } }
        ]
      },
      order: [
        ['createdAt', 'DESC']
      ]
    };

    options.include = [
      'DeliveryAddress', 'User', 'SellerUser', 'MarketingOffice',
      {
        model: db.CustomPlan,
        include: ['Country']
      },
      {
        model: db.PlanCountry,
        include: ['Plan', 'Country']
      }
    ];

    return this.downloadController.downloadSubscribersSaleReportForCronjob(options)
    .then(result => {
      if(!result) return Promise.resolve();

      // make csv file
      let attachments = [{
        filename: `weekly_subscribers_sale_report_${fromDate.format('YYYYMMDD')}_to_${toDateDisplay.format('YYYYMMDD')}.csv`,
        content: result
      }];

      let bodyData = {};
      bodyData.fromDate = fromDateFormated;
      bodyData.toDate = toDateDisplayFormated;

      let baseTemplate = Handlebars.compile(this.baseTmp);
      let template = Handlebars.compile(this.subscribersSaleReportEmailTmp);
      let logoTemplate = Handlebars.compile(this.leftLogoTmp);
      let logoHtml = logoTemplate({
        homepage: config.webUrl
      });
      let bodyHtml = template(bodyData);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml
      });
      return this.mailChimp.sendTransactionEmail({
        subject: emailSubject,
        html: emailHtml,
        to: config.email.cronjobSubscribersSaleReportConfig.emails,
        attachments
      });
    })
    .catch(err => {
      console.log(`Send weekly subscribers sale report email error : ${err.stack || JSON.stringify(err)}`);
      loggingHelper.log('error', `Send weekly subscribers sale report email error : ${err.stack || JSON.stringify(err)}`);
    });
  }

  sendStudentPromoCodeReportEmail(data, db) {
    this.downloadController = new DownloadController(db);

    // get day of week index, sunday -> saturday: 0 -> 6
    let dayOfWeek = parseInt(moment().day());
    // set sunday index, if 0 set to 7
    dayOfWeek = (dayOfWeek != 0) ? dayOfWeek : 7;

    // get distance from lastest monday to today
    let distanceFromLastestMonday = (dayOfWeek > 1) ? dayOfWeek - 1 : 0;

    // set fromDate = lastest monday - 7days (second lastest moday)
    let fromDate = moment().subtract(distanceFromLastestMonday + 7, 'days');
    // set toDate = lastest monday
    let toDate = moment().subtract(distanceFromLastestMonday, 'days');

    // format dates
    let fromDateFormated = fromDate.format('YYYY-MM-DD');
    let toDateFormated = toDate.format('YYYY-MM-DD');

    // toDate to display is always sunday -> so back 1 day from lastest monday
    let toDateDisplay = toDate.subtract(1, 'days');
    let toDateDisplayFormated = toDateDisplay.format('YYYY-MM-DD');

    let promoCodesIncluded = [
      'S2USPCQDR', 'S2USPCPOP', 'S2USPCPPP', 'S2USPCOPL', 'S2USPCLOL', 'S2USPCLOO', 'S2USPCASD', 'S2USPCQWE', 'S2USPCMNB', 'S2USPCFGH', 'S2USPCICP', 'S2USPCTUP',
      'S2USPCDNP', 'S2USPCTTY', 'S2USPCMSE', 'S2USPCOBW', 'S2USPCIII', 'S2USPCWGC', 'S2USPCNTM', 'S2USPCNIM', 'S2USPCVMT', 'S2USPCFEH', 'S2USPCHLC', 'S2USPCGOB',
      'S2USPCHLK', 'S2USPCVBN', 'S2USPCTYJ', 'S2USPCZMQ', 'S2USPCQPG', 'S2USPCGKD'
    ];

    let options = {
      where: {
        $and: [
          { createdAt: { $gte: new Date(moment(fromDateFormated)) } },
          { createdAt: { $lt: new Date(moment(toDateFormated)) } },
          { promoCode: { $in: promoCodesIncluded } },
          { status: { $not: 'On Hold' } }
        ]
      },
      order: [
        ['createdAt', 'DESC']
      ]
    };

    options.include = [
      {
        model: db.DeliveryAddress,
        required: true
      },
      {
        model: db.User,
        required: true
      },
      {
        model: db.PlanCountry,
        required: true,
        include: [{
          model: db.Plan,
          required: true
        }]
      }
    ];

    return this.downloadController.downloadStudentPromoCodeReportForCronjob(options)
    .then(result => {
      if(!result) return Promise.resolve();

      // make csv file
      let attachments = [{
        filename: `weekly_student_promo_code_report_${fromDateFormated}_to_${toDateDisplayFormated}.csv`,
        content: result
      }];

      let emailSubject = in18.getMessage('email', 'student_promo_code_report_subject', data.langCode).replace('{fromDate}', fromDateFormated);
      emailSubject = emailSubject.replace('{toDate}', toDateDisplayFormated);

      let bodyData = {};
      bodyData.fromDate = fromDateFormated;
      bodyData.toDate = toDateDisplayFormated;

      let baseTemplate = Handlebars.compile(this.baseTmp);
      let template = Handlebars.compile(this.studentPromoCodeReportEmailTmp);
      let logoTemplate = Handlebars.compile(this.leftLogoTmp);
      let logoHtml = logoTemplate({
        homepage: config.webUrl
      });
      let bodyHtml = template(bodyData);
      let emailHtml = baseTemplate({
        header: logoHtml,
        bodyContent: bodyHtml
      });
      return this.mailChimp.sendTransactionEmail({
        subject: emailSubject,
        html: emailHtml,
        to: config.email.studentPromoCodeReportEmail,
        attachments
      });
    })
    .catch(err => {
      console.log(`Send weekly student promo code report email error : ${err.stack || JSON.stringify(err)}`);
      loggingHelper.log('error', `Send weekly student promo code report email error : ${err.stack || JSON.stringify(err)}`);
    });
  }
}

export default EmailHelper;
