import PromiseFtp from 'promise-ftp';
import config from '../config';
import fs from 'fs';
import { loggingHelper } from './logging-helper';

class FtpHelper {
  constructor() {
    // init FTP object
    this.options = {
      host: config.badgeFiles.ftp.host,
      user: config.badgeFiles.ftp.username,
      password: config.badgeFiles.ftp.password
    };
    this.ftp = new PromiseFtp();
  }

    /**
   * List all file in remote path
   * @param {*} remotePath 
   */
  listFiles(remotePath) {
    loggingHelper.log('info', `bulkUpdate ${remotePath} start`);
    let outputFiles = [];
    return this.ftp.connect(this.options)
      .then(serverMessage => {
        console.log(`Server message: serverMessage ${serverMessage}`);
        loggingHelper.log('info', `Server message: serverMessage ${serverMessage}`);
        return this.ftp.list(remotePath);
      })
      .then(files => {
        files.forEach(file => {
          if(file.type === '-') {
            outputFiles.push(file);
          }
        });
      })
      .then(() => this.ftp.end())
      .then(() => outputFiles);
  }

  /**
   * Download and delete file on FTP server
   * @param {*} remotePath 
   * @param {*} localPath 
   */
  getFile(remotePath, localPath) {
    loggingHelper.log('info', `getFile ${localPath} start`);
    return this.ftp.connect(this.options)
      .then(serverMessage => {
        console.log(`Server message: serverMessage ${serverMessage}`);
        loggingHelper.log('info', `Server message: serverMessage ${serverMessage}`);
        return this.ftp.get(remotePath);
      })
      .then(stream => new Promise((resolve, reject) => {
        stream.once('close', resolve);
        stream.once('error', reject);
        stream.pipe(fs.createWriteStream(localPath));
      }))
      .then(() => this.ftp.end());
  }
}

export let ssFtpHelper = new FtpHelper();
