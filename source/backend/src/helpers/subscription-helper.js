import _ from 'underscore';
import moment from 'moment';
import { stripeHelper } from './stripe-helper';
import SequelizeHelper from './sequelize-helper';
import OrderInboundQueueService from '../services/create-order-inbound.service';
import { loggingHelper } from './logging-helper';
import config from '../config';

class SubscriptionHelper {

  static getSubscription(_id, db) {
    return db.Subscription.findOne({where: {
      id: _id,
    }, include: [
      'Card',
      {
        model: db.PlanCountry,
        include: ['Country', 'planDetails',
          {
            model: db.Plan,
            include: ['PlanType']
          }
        ]
      },
      {
        model: db.CustomPlan,
        include: ['Country', 'PlanType', 'customPlanDetail']
      },
      {
        model: db.PlanOption,
        include: ['planOptionProducts']
      }
    ]});
  }

  static getDiffInfoWhenUpdate(beforeObj, afterObj, db) {
    let planOptionPromise = '';
    let planCountryPromise = '';

    // change PlanOptionId
    if(beforeObj.PlanOptionId != afterObj.PlanOptionId) {
      planOptionPromise = new Promise((ok, fail) => {
        db.PlanOption.findOne({where: {id: beforeObj.PlanOptionId}})
        .then(before => {
          db.PlanOption.findOne({where: {id: afterObj.PlanOptionId}})
          .then(after => {
            ok([before, after]);
          });
        });
      });
    }

    // change PlanCountryId
    if(beforeObj.PlanCountryId != afterObj.PlanCountryId) {
      planCountryPromise = new Promise((ok, fail) => {
        let planCountryOptions = {
          where: {},
          include: [
            {
              model: db.Plan,
              include: [
                'PlanType',
                {
                  model: db.PlanGroup,
                  include: [{
                    model: db.PlanGroupTranslate,
                    as: 'planGroupTranslate'
                  }]
                }
              ]
            }
          ]
        };

        planCountryOptions.where = {id: beforeObj.PlanCountryId};
        db.PlanCountry.findOne(planCountryOptions)
        .then(before => {
          planCountryOptions.where = {id: afterObj.PlanCountryId};
          db.PlanCountry.findOne(planCountryOptions)
          .then(after => {
            ok([before, after]);
          });
        });
      });
    }

    return new Promise((ok, fail) => {
      let detail = '';

      if(planCountryPromise !== '') {
        planCountryPromise.then(data => {
          let valueBefore = data[0].Plan.PlanGroup.planGroupTranslate.find(element => element.langCode === 'EN').name;
          let valueAfter = data[1].Plan.PlanGroup.planGroupTranslate.find(element => element.langCode === 'EN').name;
          if(valueBefore !== valueAfter) {
            detail += `Blade: ${valueBefore} -> ${valueAfter}`;
          }

          valueBefore = data[0].Plan.PlanType.name;
          valueAfter = data[1].Plan.PlanType.name;
          if(valueBefore !== valueAfter) {
            if(detail.length > 0) {
              detail += '<br>';
            }
            detail += `Usage: ${valueBefore} -> ${valueAfter}`;
          }

          if(planOptionPromise !== '') {
            planOptionPromise.then(dataArr => {
              valueBefore = dataArr[0] ? (dataArr[0].hasShaveCream ? 'Yes' : 'No') : 'No Addon';
              valueAfter = dataArr[1] ? (dataArr[1].hasShaveCream ? 'Yes' : 'No') : 'No Addon';
              if(valueBefore !== valueAfter) {
                if(detail.length > 0) {
                  detail += '<br>';
                }
                detail += `Addon Shave Cream: ${valueBefore} -> ${valueAfter}`;
              }
              ok(detail);
            });
          } else {
            ok(detail);
          }
        });
      } else if(planOptionPromise !== '') {
        planOptionPromise.then(dataArr => {
          let valueBefore = dataArr[0] ? (dataArr[0].hasShaveCream ? 'Yes' : 'No') : 'No Addon';
          let valueAfter = dataArr[1] ? (dataArr[1].hasShaveCream ? 'Yes' : 'No') : 'No Addon';
          if(valueBefore !== valueAfter) {
            detail += `Addon Shave Cream: ${valueBefore} -> ${valueAfter}`;
          }
          ok(detail);
        });
      } else {
        ok(detail);
      }
    });
  }

  static buildDeliveryProducts(subscription) {
    loggingHelper.log('info', 'buildDeliveryProducts start');
    loggingHelper.log('info', `param = ${subscription}`);
    let deliveryDetails = [];
    if(subscription.PlanCountry) {
      subscription.PlanCountry.planDetails.forEach(planDetail => {
        let deliverPlan = JSON.parse(planDetail.deliverPlan);
        let times = Object.keys(deliverPlan);
        let deliveryDetail;
        times.forEach(time => {
          let productName = _.findWhere(planDetail.ProductCountry.Product.productTranslate, {langCode: subscription.PlanCountry.Country.defaultLang.toUpperCase()}).name;
          deliveryDetail = _.findWhere(deliveryDetails, {time});
          if(deliveryDetail) {
            let product = _.findWhere(deliveryDetail.productNames, {productName});
            if(product) {
              product.qty += deliverPlan[time];
            } else if(deliverPlan[time] !== 0) {
              deliveryDetail.productNames.push({productName: planDetail.ProductCountry.Product.productTranslate, qty: deliverPlan[time]});
            }
          } else if(deliverPlan[time] !== 0) {
            deliveryDetail = {
              time,
              productNames: [{productName: planDetail.ProductCountry.Product.productTranslate, qty: deliverPlan[time]}],
            };
            deliveryDetails.push(Object.assign({}, deliveryDetail));
          }
        });
      });
    }
    return deliveryDetails;
  }

  static performCharge(subscription) {
    loggingHelper.log('info', 'performCharge start');
    loggingHelper.log('info', `param = ${subscription}`);
    // calculate charge amount

    let result = { subscription, receiptObject: {}};
    // let amount = (subscription.price / subscription.PlanCountry.Plan.PlanType.totalChargeTimes) * subscription.qty;
    let amount = 0;
    if((subscription.PlanCountry && !subscription.PlanCountry.Plan.isTrial) || subscription.CustomPlan) {
      amount = subscription.pricePerCharge * subscription.qty;
    } else {
      amount = subscription.pricePerCharge;
    }

    // calculate discount amount
    result.receiptObject.discountAmount = 0;
    if(subscription.discountPercent !== 0 && subscription.currentChargeNumber === 0 && subscription.isTrial) {
      result.receiptObject.discountAmount = amount * (subscription.discountPercent / 100);
      amount = amount - result.receiptObject.discountAmount;
    }

    // calculate discount amount
    result.receiptObject.discountAmount = 0;
    // result.receiptObject.discountAmount = amount * (subscription.discountPercent / 100);
    // amount = amount - result.receiptObject.discountAmount;

    // calculate tax amount
    // if(!subscription.PlanCountry.Country.includedTaxToProduct) {
      // let taxAmount = result.receiptObject.totalPrice * (subscription.PlanCountry.Country.taxRate / 100);
      // result.receiptObject.taxAmount = taxAmount;
      // amount = amount + taxAmount;
    // }

    // calculate shipping fee
    result.receiptObject.shippingFee = 0;
    result.receiptObject.totalPrice = amount;

    let country = subscription.PlanCountry ? subscription.PlanCountry.Country : subscription.CustomPlan.Country;
    let chargeObject = {
      amount: amount,
      currency: country.currencyCode.toLowerCase(),
      customer: subscription.Card.customerId,
      description: `Shave2u charge for subscription: ${subscription.currentOrderId}`
    };
    // perform charge to stripe
    return stripeHelper.createCharge(chargeObject, country.code, subscription.Card.type)
      .then(chargeObj => {
        result.chargeObj = chargeObj;
        return stripeHelper.retrieveBalanceTransaction(chargeObj.balance_transaction, country.code, subscription.Card.type);
      })
      .then(balanceTransaction => {
        result.chargeObj.chargeFee = balanceTransaction.fee;
        result.chargeObj.chargeCurrency = balanceTransaction.currency;
        return result;
      });
  }

  static updateSubscriptionStatus(data, db) {
    loggingHelper.log('info', 'updateSubscriptionStatus start');
    loggingHelper.log('info', `param = ${data}`);
    return db.sequelize.transaction(t => db.Receipt.create({ // create receipt
      SubscriptionId: data.subscription.id,
      chargeId: data.chargeObj.id,
      chargeFee: data.chargeObj.chargeFee / 100,
      chargeCurrency: data.chargeObj.chargeCurrency,
      last4: data.subscription.Card.cardNumber,
      branchName: data.subscription.Card.branchName,
      expiredYear: data.subscription.Card.expiredYear,
      expiredMonth: data.subscription.Card.expiredMonth,
      subTotalPrice: data.subscription.price,
      discountAmount: data.receiptObject.discountAmount,
      shippingFee: data.receiptObject.shippingFee,
      taxAmount: data.receiptObject.taxAmount,
      totalPrice: data.receiptObject.totalPrice,
      currency: data.subscription.PlanCountry ? data.subscription.PlanCountry.Country.currencyDisplay : data.subscription.CustomPlan.Country.currencyDisplay
      // taxAmount: 
    }, {transaction: t})
    .then(() => {
      let planType = data.subscription.PlanCountry ? data.subscription.PlanCountry.Plan.PlanType : data.subscription.CustomPlan.PlanType;
      let subsequentChargeDuration = planType.subsequentChargeDuration;
      let nextChargeDate = moment().add(subsequentChargeDuration.split(' ')[0], subsequentChargeDuration.split(' ')[1]);
      let status = 'Processing';
      status = data.subscription.currentChargeNumber + 1 === planType.totalChargeTimes && data.subscription.currentDeliverNumber === planType.totalDeliverTimes ? 'Finished' : status;

      let subscriptionChangeInfo = {
        nextChargeDate,
        status,
        currentChargeNumber: data.subscription.currentChargeNumber + 1,
        totalChargeTimes: planType.totalChargeTimes
      };
      
      // update subscription info
      if(status == 'Finished') {
        db.SubscriptionHistory.create({
          message: status,
          subscriptionId: data.subscription.id
        });
      }

      return db.Subscription.update(subscriptionChangeInfo, {where: {id: data.subscription.id}, transaction: t});
    }));
  }

  static performDeliver(subscription, db) {
    loggingHelper.log('info', 'performDeliver start');
    loggingHelper.log('info', `param = ${subscription}`);
    let country;
    if(subscription.PlanCountry) {
      country = subscription.PlanCountry.Country;
    }
    if(subscription.CustomPlan) {
      country = subscription.CustomPlan.Country;
    }
    let order = {
      subscriptionIds: JSON.stringify([subscription.id]),
      UserId: subscription.UserId,
      DeliveryAddressId: subscription.DeliveryAddressId,
      BillingAddressId: subscription.BillingAddressId,
      currency: country.currency,
      email: subscription.email,
      paymentType: 'stripe',
      states: 'Payment Received',
      CountryId: country.id
    };

    let updateItems = [];

    // build order details object
    let orderDetails = [];
    if(subscription.PlanCountry && !subscription.PlanCountry.Plan.isTrial) {
      subscription.PlanCountry.planDetails.forEach(planDetail => {
        let deliverPlan = JSON.parse(planDetail.deliverPlan);
        let deliverAt = Object.keys(deliverPlan);
        if(deliverAt.includes(`${subscription.currentDeliverNumber + 1}`) && deliverPlan[subscription.currentDeliverNumber + 1] > 0) {
          orderDetails.push({
            ProductCountryId: planDetail.ProductCountryId,
            qty: deliverPlan[subscription.currentDeliverNumber + 1] * subscription.qty
          });

          // create update items
          updateItems.push({
            id: planDetail.ProductCountryId,
            qty: deliverPlan[subscription.currentDeliverNumber + 1] * subscription.qty
          });
        }
      });
    } else if(subscription.CustomPlan) {
      subscription.CustomPlan.customPlanDetail.forEach(detail => {
        // let deliverPlan = JSON.parse(detail.deliverPlan);
        let qty = detail.qty;
        if(qty > 0) {
          orderDetails.push({
            ProductCountryId: detail.ProductCountryId,
            qty: qty * subscription.qty
          });
  
          // create update items
          updateItems.push({
            id: detail.ProductCountryId,
            qty: qty * subscription.qty
          });
        }
      });
    } else {
      if(subscription.PlanOption) {
        subscription.PlanOption.planOptionProducts.forEach(optionProduct => {
          let qty = optionProduct.qty;
          if(qty > 0) {
            orderDetails.push({
              ProductCountryId: optionProduct.ProductCountryId,
              qty: qty * subscription.qty
            });
    
            // create update items
            updateItems.push({
              id: optionProduct.ProductCountryId,
              qty: qty * subscription.qty
            });
          }
        });
      } else {
        subscription.PlanCountry.planDetails.forEach(planDetail => {
          let deliverPlan = JSON.parse(planDetail.deliverPlan);
          let qty = deliverPlan[1];
          if(qty > 0) {
            orderDetails.push({
              ProductCountryId: planDetail.ProductCountryId,
              qty: qty * subscription.qty
            });
    
            // create update items
            updateItems.push({
              id: planDetail.ProductCountryId,
              qty: qty * subscription.qty
            });
          }
        });
      }
    }
    order.orderDetail = orderDetails;

    // console.log(`order.orderDetail === ${JSON.stringify(order.orderDetail)} == ${JSON.stringify(subscription.PlanCountry.planDetails)}`);
    // build order history
    order.orderHistories = [{
      message: order.states
    }];

    // get product country
    return db.ProductCountry.findAll({where: {
      id: {$in: _.pluck(updateItems, 'id')},
    }, include: ['Country']})
      .then(productCountries => {
        productCountries = SequelizeHelper.convertSeque2Json(productCountries);
        let tmp = [];
        order.orderDetail.forEach(orderDetail => {
          let productCountry = _.findWhere(productCountries, {id: orderDetail.ProductCountryId});
          if(productCountry) {
            orderDetail.price = productCountry.sellPrice;
            orderDetail.currency = productCountry.Country.currencyCode;
            tmp.push(orderDetail);
          }
        });
        order.orderDetail = tmp;
      })
      .then(() => db.sequelize.transaction(t => db.Order.create(order, { include: ['orderDetail', 'orderHistories'], returning: true, transaction: t})
          .then(_order => {
            order = _order;
            // calculate next delivery date
            let planType = subscription.PlanCountry ? subscription.PlanCountry.Plan.PlanType : subscription.CustomPlan.PlanType;
            let subsequentDeliverDuration = planType.subsequentDeliverDuration;
            let nextDeliverDate = moment().add(subsequentDeliverDuration.split(' ')[0], subsequentDeliverDuration.split(' ')[1]);
            
            // determined status
            let status = subscription.status;
            console.log('status ================== ', status);
            console.log('subscription.currentDeliverNumber + 1 ================== ', subscription.currentDeliverNumber + 1);
            console.log('planType.totalDeliverTimes ================== ', planType.totalDeliverTimes);
            console.log('planType.currentChargeNumber ================== ', planType.currentChargeNumber);
            console.log('planType.totalChargeTimes ================== ', planType.totalChargeTimes);
            status = subscription.currentDeliverNumber + 1 === planType.totalDeliverTimes && subscription.currentChargeNumber === planType.totalChargeTimes ? 'Finished' : status;
            console.log('status ================== ', status);

            let subscriptionChangeInfo = {
              nextDeliverDate,
              status,
              currentDeliverNumber: subscription.currentDeliverNumber + 1,
              totalDeliverTimes: planType.totalDeliverTimes,
              currentOrderId: _order.id
            };
            // update subscription info
            if(status == 'Finished') {
              db.SubscriptionHistory.create({
                message: status,
                subscriptionId: subscription.id
              });
            }
              
            return db.Subscription.update(subscriptionChangeInfo, {where: {id: subscription.id}, transaction: t});
          })
          .then(() => db.Receipt.findOne({where: {SubscriptionId: subscription.id, OrderId: null}}))
          .then(receipt => {
            if(receipt) {
              // update receipt
              return db.Receipt.update({OrderId: order.id}, {where: {SubscriptionId: subscription.id, OrderId: null}, transaction: t});
            } else {
              return db.Receipt.create({
                OrderId: order.id,
                SubscriptionId: subscription.id,
                last4: subscription.Card.cardNumber,
                branchName: subscription.Card.branchName,
                expiredYear: subscription.Card.expiredYear,
                expiredMonth: subscription.Card.expiredMonth,
                currency: country.currencyDisplay
              }, {transaction: t});
            }
          }))
          .then(() => {
            let orderInboundQueueService = OrderInboundQueueService.getInstance();
            if(order.CountryId !== 8 || moment().isSameOrAfter(moment(config.deliverDateForKorea))) {
              return orderInboundQueueService.addTask(order.id, order.CountryId);
            } else {
              return Promise.resolve();
            }
          })
        );
  }
}

export default SubscriptionHelper;
