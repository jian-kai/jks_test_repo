class Validator {
  static validateEmail(email) {
    let exp = /^(([^<>()\[\]\\.,;:\s@“]+(\.[^<>()\[\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return exp.test(email);
  }

  /**
   * validate password
   * At least one upper case English letter, (?=.*?[A-Z])
   * At least one lower case English letter, (?=.*?[a-z])
   * At least one digit, (?=.*?[0-9])
   * At least one special character, (?=.*?[#?!@$%^&*-])
   * Minimum eight in length .{8,} (with the anchors)
   * @param {sting} password 
   * @return {boolean}
   */
  static validatePassword(password) {
    let exp = /^(?=.*?[a-z])(?=.*?[0-9]).{6,}$/;
    return exp.test(password);
  }

  static valiadateDate(date, isRequired) {
    let exp = /^\d{4}\/\d{1,2}\/\d{1,2}$|^\d{4}-\d{1,2}-\d{1,2}(\s\d{1,2}:\d{1,2}:\d{1,2})?$/;
    if(isRequired && !date) {
      return false;
    }
    if(date) {
      return exp.test(date);
    }
    return true;
  }
}

export default Validator;
