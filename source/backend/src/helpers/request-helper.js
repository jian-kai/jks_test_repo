import moment from 'moment';
import { loggingHelper } from './logging-helper';

class RequestHelper {
  static buildSequelizeOptions(options, _params) {
    loggingHelper.log('info', 'buildSequelizeOptions start');
    let params = {};
    Object.assign(params, _params);

    // buidl order option
    console.log(`params.orderBy ${JSON.stringify(params.orderBy)}`);
    loggingHelper.log('info', `params.orderBy ${JSON.stringify(params.orderBy)}`);
    if(params.orderBy) {
      options.order = [];
      let orderFields;
      try {
        orderFields = JSON.parse(params.orderBy) || [];
      } catch(err) {
        orderFields = [params.orderBy];
      }
      orderFields.forEach(value => {
        if(value.indexOf('_DESC') > -1) {
          options.order.push([value.replace('_DESC', ''), 'DESC']);
        } else {
          options.order.push(value);
        }
      });
    }

    if(!isNaN(Number.parseInt(params.limit))) {
      options.limit = parseInt(params.limit);

      let page = !isNaN(Number.parseInt(params.page)) ? Number.parseInt(params.page) : 1;
      page = page < 1 ? 1 : page;
      if(page) {
        let offset = page * options.limit - options.limit;
        options.offset = parseInt(offset);
      }
    }

    // remove not column DB
    return options;
  }

  static buildWhereCondition(options, _params, model) {
    loggingHelper.log('info', 'buildWhereCondition start');
    let params = {};
    Object.assign(params, _params);

    let paramKeys = Object.keys(params);
    let modelColumns = Object.keys(model.rawAttributes);
    paramKeys.forEach(key => {
      if(modelColumns.indexOf(key) === -1) {
        // remove country
        Reflect.deleteProperty(params, key);
      } else if(params[key] && model.tableAttributes[key].type.constructor.key === 'BOOLEAN') {
        params[key] = (params[key] == 1 || params[key].toLowerCase() == 'true' || params[key].toLowerCase() == '1');
      }
    });
    if(options.where) {
      options.where = Object.assign(options.where, params);
    } else {
      options.where = params;
    }
    return options;
  }

  static buildClientTimezone(date, dayAdded, req) {
    let clientTimezone = req.headers['client-timezone-offset'];
    let serverTimezone = ((new Date()).getTimezoneOffset() * -1) / 60;

    if(!clientTimezone && clientTimezone != '0') {
      clientTimezone = serverTimezone;
    } else {
      clientTimezone = parseInt(clientTimezone);
    }

    let hoursOffsetServerClient = (serverTimezone - clientTimezone) + (dayAdded * 24);

    console.log(`serverTimezone === ${serverTimezone} === ${clientTimezone} === ${hoursOffsetServerClient}`);

    return new Date(moment(date).add(hoursOffsetServerClient, 'hours'));
  }

  static convertServerTimezone2UTC(date) {
    let serverTimezone = ((new Date()).getTimezoneOffset() * -1) / 60;
    return new Date(moment(date).subtract(serverTimezone, 'hours'));
  }
}

export default RequestHelper;
