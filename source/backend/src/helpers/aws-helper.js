import fs from 'fs';
import AWS from 'aws-sdk';
import config from '../config';
import uuidv1 from 'uuid/v1';
import { resize } from 'easyimage';
import { loggingHelper } from './logging-helper';

const albumPattern = /^[0-9a-zA-Z]$/g;

class AWSHelper {
  constructor() {
    AWS.config.loadFromPath(config.AWS.keyPath);
    this.S3 = new AWS.S3({
      apiVersion: config.AWS.version,
      params: {Bucket: config.AWS.bucketName}
    });
  }

  createAlbum(albumName) {
    loggingHelper.log('info', 'createAlbum start');
    loggingHelper.log('info', `param: albumName = ${albumName}`);
    return new Promise((resolved, reject) => {
      if(!albumPattern.test(albumName.trim())) {
        reject('Album name incorrect!');
      } else {
        this.S3.headObject({Key: `${albumName}/`}, err => {
          if(!err) {
            reject('Album already exists.');
          } else if(err.code !== 'NotFound') {
            reject(`There was an error creating your album: ${err.message}`);
          } else {
            this.S3.putObject({Key: `${albumName}/`}, function(err) {
              if(err) {
                reject(`There was an error creating your album: ${err.message}`);
              } else {
                resolved();
              }
            });
          }
        });
      }
    });
  }

  doUpload(album, file, fileName) {
    loggingHelper.log('info', `upload start param: album = ${album}, file = ${file}`);
    return new Promise((resolved, reject) => {
      fs.readFile(file.path, (err, fileContent) => {
        console.log(`file.path ERROR ${err}`);
        if(err) reject(err);
        let key = `${album}/${uuidv1()}.${file.originalFilename.split('.').pop()}`;
        if(fileName) {
          key = `${album}/${fileName}`;
        }
        this.S3.upload({
          Key: key,
          Body: fileContent,
          ACL: 'public-read'
        }, function(err, data) {
          console.log(`file.path ERROR ${err}`);
          if(err) {
            reject(err);
          } else {
            resolved(data);
          }
        });
      });
    });
  }

  upload(album, file, isResize, fileName) {
    loggingHelper.log('info', `upload S3 start param: album = ${album}, file = ${file}`);
    // resize image before upload to S3
    if(isResize) {
      return resize({
        src: file.path,
        width: 1800
      })
        .then(croped => {
          console.log(`resized == ${JSON.stringify(croped)}`);
          croped.originalFilename = croped.name;
          return this.doUpload(album, croped, fileName);
        })
        .catch(error => {
          console.log('error ===== === ', error);
        });
    } else {
      return this.doUpload(album, file, fileName);
    }
  }
  
  doDownload(album, filename, folderName) {
    let folderPath = config.AWS.localDownloadFolder;
    if(!fs.existsSync(config.AWS.localDownloadFolder)) {
      fs.mkdirSync(config.AWS.localDownloadFolder);
    }
    if(folderName) {
      if(!fs.existsSync(`${config.AWS.localDownloadFolder}/${folderName}`)) {
        fs.mkdirSync(`${config.AWS.localDownloadFolder}/${folderName}`);
      }
      folderPath = `${config.AWS.localDownloadFolder}/${folderName}`;
    }

    let file = fs.createWriteStream(`${folderPath}/${filename}`);
    let params = {
      Bucket: config.AWS.bucketName,
      Key: `${album}/${filename}`
    };
    return new Promise((resolve, reject) => {
      this.S3.getObject(params).createReadStream()
      .on('end', () => resolve())
      .on('error', error => reject(error))
      .pipe(file);
    });
  }
}

export let awsHelper = new AWSHelper();
