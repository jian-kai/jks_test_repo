import Promise from 'bluebird';
import { loggingHelper } from './logging-helper';
import _ from 'underscore';

class SequelizeHelper {
  static convertSeque2Json(result) {
    return JSON.parse(JSON.stringify(result));
  }

  /**
   * Update multiple record at the same time
   * @param {*} data 
   * @param {*} db 
   * @param {*} modelName 
   * @return Promise
   */
  static bulkUpdate(data, db, modelName, options) {
    loggingHelper.log('info', `bulkUpdate ${modelName} start`);
    return Promise.map(data, item => {
      let _option = Object.assign({where: {id: item.id}}, options);
      return db[modelName].update(item, _option);
    });
  }

  /**
   * Update multiple record at the same time
   * @param {*} data 
   * @param {*} db 
   * @param {*} modelName 
   * @return Promise
   */
  static bulkCreate(data, db, modelName, options) {
    loggingHelper.log('info', `bulkCreate ${modelName} start`);
    return Promise.map(data, item => db[modelName].create(item, options));
  }

  static findAndCountAll(model, options, include) {
    loggingHelper.log('info', `findAndCountAll ${model} start`);
    let result;
    return model.findAndCountAll(options)
      .then(_result => {
        result = this.convertSeque2Json(_result);
        // return Promise.mapSeries(result.rows, row => model.findOne({where: {id: row.id}, include}))
        //   .then(data => {
        //     result.rows = this.convertSeque2Json(data);
        //     return result;
        //   });
        if(!options.order) {
          options.order = ['createdAt'];
        }

        return model.findAll({
          where: {id: {$in: _.pluck(result.rows, 'id')}},
          order: options.order,
          include,
        })
        .then(data => {
          result.rows = this.convertSeque2Json(data);
          return result;
        });
      });
  }
}

export default SequelizeHelper;
