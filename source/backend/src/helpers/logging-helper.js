class LoggingHelper {
  constructor() {
    var winston = require('winston');
    winston.emitErrs = true;

    this.logger = new (winston.Logger)({
      transports: [
        new (winston.transports.File)({
          name: 'info-file',
          filename: 'shaves2u_logging.log',
          level: 'info',
          handleExceptions: true,
          json: true,
        }),
        new (winston.transports.File)({
          name: 'error-file',
          filename: 'shaves2u_error.log',
          level: 'error',
          handleExceptions: true,
          json: true,
        }),
        new (winston.transports.Console)()
      ]
    });
  }

  log(level, message) {
    this.logger.log(level, message);
  }
}

export let loggingHelper = new LoggingHelper();
