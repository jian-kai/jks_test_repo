import stripePackage from 'stripe';
import config from '../config';
import { loggingHelper } from './logging-helper';

class StripeHelper {
  constructor() {
    this.StripeCountry = {
      MYS: {
        website: stripePackage(config.keyStripe.MYS.website.secretKey),
        baWeb: stripePackage(config.keyStripe.MYS.baWeb.secretKey)
      },
      SGP: {
        website: stripePackage(config.keyStripe.SGP.website.secretKey),
        baWeb: stripePackage(config.keyStripe.SGP.baWeb.secretKey)
      },
      KOR: {
        website: stripePackage(config.keyStripe.KOR.website.secretKey),
        baWeb: stripePackage(config.keyStripe.KOR.baWeb.secretKey)
      },
      default: {
        website: stripePackage(config.keyStripe.default.website.secretKey),
        baWeb: stripePackage(config.keyStripe.default.baWeb.secretKey)
      }
    };

    this.countryEditAmount = ['MYS', 'SGP'];
  }

  getStripeService(countryCode, type) {
    loggingHelper.log('info', `getStripeService countryCode = ${countryCode} start`);
    if(this.StripeCountry.hasOwnProperty(countryCode)) {
      return this.StripeCountry[countryCode][type];
    } else {
      return this.StripeCountry.default[type];
    }
  }

  createCustomer(email, source, countryCode, type) {
    loggingHelper.log('info', `createCustomer email = ${email}, source = ${source}, countryCode = ${countryCode} start`);
    return new Promise((resolved, reject) => {
      email = email ? email : 'smart shaves guest';
      let description = `Customer for ${email}`;
      this.getStripeService(countryCode, type).customers.create({ description, source }, (err, customer) => {
        if(err) {
          reject(err);
        } else {
          resolved(customer);
        }
      });
    });
  }

  createCharge(chargeOptions, countryCode, type) {
    loggingHelper.log('info', `createCharge chargeOptions = ${chargeOptions}, countryCode = ${countryCode} start`);
    if(this.countryEditAmount.find(code => code === countryCode.toUpperCase())) {
      chargeOptions.amount = parseInt(chargeOptions.amount * 100);
    }
    return new Promise((resolved, reject) => {
      this.getStripeService(countryCode, type).charges.create(chargeOptions, (err, charge) => {
        if(err) {
          reject(err);
        } else {
          resolved(charge);
        }
      });
    });
  }

  retrieveBalanceTransaction(txId, countryCode, type) {
    loggingHelper.log('info', `retrieveBalanceTransaction countryCode = ${countryCode} start`);
    return new Promise((resolved, reject) => {
      this.getStripeService(countryCode, type).balance.retrieveTransaction(txId, (err, balanceTransaction) => {
        if(err) {
          reject(err);
        } else {
          resolved(balanceTransaction);
        }
      });
    });
  }

  createRefund(chargeId, refundAmount, countryCode, type) {
    loggingHelper.log('info', `createRefund chargeId = ${countryCode}, countryCode = ${countryCode} start`);
    if(this.countryEditAmount.find(code => code === countryCode.toUpperCase())) {
      refundAmount = refundAmount * 100;
    }
    return new Promise((resolved, reject) => {
      this.getStripeService(countryCode, type).refunds.create({
        charge: chargeId,
        amount: parseInt(refundAmount)
      }, (err, refund) => {
        if(err) {
          reject(err);
        } else {
          resolved(refund);
        }
      });
    });
  }

  // create token for testing
  createToken(countryCode, type) {
    loggingHelper.log('info', `createToken countryCode = ${countryCode} start`);
    this.getStripeService(countryCode, type).tokens.create({
      card: {
        name: 'Thi Nhuoc Kim',
        number: '5555555555554444',
        exp_month: 12,
        exp_year: 2018,
        cvc: '123'
      }
    }, (err, token) => {
      if(err) {
        console.log(`error: --- ${err.message}`);
        loggingHelper.log('error', `error: --- ${err.message}`);
      } else {
        console.log(`success token: --- ${JSON.stringify(token)}`);
        loggingHelper.log('info', `success token: --- ${JSON.stringify(token)}`);
      }
    });
  }
}

export let stripeHelper = new StripeHelper();
