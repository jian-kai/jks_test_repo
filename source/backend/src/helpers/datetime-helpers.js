import moment from 'moment';

class DateTimeHelper {

  /**
   * Get next working days after a priod time
   * @param {*} duration 
   */
  static getNextWorkingDays(period, date) {
    let nextDate = moment();
    if(date) {
      nextDate = moment(date);
    }
    if(typeof period === 'string' && period.indexOf(' ') > -1) {
      nextDate = nextDate.add(period.split(' ')[0], period.split(' ')[1]);
    } else if(typeof period === 'object') {
      nextDate = nextDate.add(period.number, period.string);
    }
    // if(nextDate.day() === 6) {
    //   nextDate = nextDate.add(2, 'days');
    // }
    // if(nextDate.day() === 0) {
    //   nextDate = nextDate.add(1, 'days');
    // }
    return nextDate;
  }

  static setMomentLocale(_moment, _langCode) {
    if(_langCode === 'ko') {
      _moment.locale('ko');
    } else {
      _moment.locale('en');
    }
    return _moment;
  }
}

export default DateTimeHelper;
