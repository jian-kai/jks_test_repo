
import { loggingHelper } from './logging-helper';

class ResponseHelper {
  static responseError(res, error, data) {
    console.log(error);
    loggingHelper.log('error', error);
    error = error.message ? error.message : error;
    return res.status(400).json({ ok: false, message: error, data });
  }
  static responsePermissonDenied(res) {
    return res.status(403).json({ ok: false, message: 'permission denied' });
  }
  static responseNotFoundError(res, modelName) {
    return res.status(404).json({ ok: false, message: `${modelName} not found` });
  }
  static responseInternalServerError(res, error) {
    // log error to log file
    console.log(error);
    loggingHelper.log('error', error);
    if(error.name === 'SequelizeValidationError') {
      return res.status(400).json({ ok: false, message: error.message || error });
    } else {
      return res.status(500).json({ ok: false, message: 'Internal server error' });
    }
  }
  static responseErrorParam(res, error) {
    console.log(error);
    loggingHelper.log('error', error);
    return res.status(400).json({ ok: false, message: error.message || error });
  }
  static responseMissingParam(res, error) {
    console.log(error);
    loggingHelper.log('error', error);
    return res.status(400).json({ ok: false, message: error || 'missing parameters' });
  }
  static responseSuccess(res, data) {
    return res.json({ ok: true, data });
  }
}

export default ResponseHelper;
