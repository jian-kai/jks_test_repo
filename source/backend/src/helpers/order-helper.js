import moment from 'moment';
import config from '../config';
import { loggingHelper } from './logging-helper';

const ORDER_LENGTH = 9;
const BULK_ORDER_LENGTH = 8;

class OrderHelper {

  /**
   * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
   * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
   * @param {*} order 
   */
  static fromatOrderNumber(order, isFull) {
    loggingHelper.log('info', 'fromatOrderNumber start');
    let code = order.Country ? order.Country.code : order.region;
    let countryCode = config.countryCodes[code] || code;
    let orderId = order.orderId ? `${order.orderId}` : `${order.id}`;
    let returnValue = '';
    for(let i = 0; i < ORDER_LENGTH - orderId.length; i++) {
      returnValue += '0';
    }

    if(!isFull) {
      return `${countryCode}${returnValue}${orderId}`;
    } else {
      return `${countryCode}${returnValue}${orderId}-${moment(order.createdAt).format('YYYYMMDD')}`;
    }
  }

  /**
   * Extract order number for display format
   * Example MY000000001-20171127 -> 000000001
   * @param {*} orderNumber 
   */
  static getOrderNumber(orderNumber) {
    loggingHelper.log('info', 'getOrderNumber start');
    loggingHelper.log('info', `param: orderNumber = ${orderNumber}`);
    return orderNumber.substr(2, ORDER_LENGTH);
  }

  /**
   * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
   * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
   * @param {*} order 
   */
  static formatInvoiceNumber(order) {
    loggingHelper.log('info', 'formatInvoiceNumber start');
    let code = order.Country ? order.Country.code : order.region;
    let countryCode = config.countryCodes[code] || code;
    let orderId = order.orderId ? `${order.orderId}` : `${order.id}`;
    let returnValue = '';
    for(let i = 0; i < ORDER_LENGTH - orderId.length; i++) {
      returnValue += '0';
    }

    return `S2U-${countryCode}-${returnValue}${orderId}`;
  }

  /**
   * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
   * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
   * @param {*} order 
   */
  static fromatBulkOrderNumber(order, isFull) {
    loggingHelper.log('info', 'fromatBulkOrderNumber start');
    let countryCode = config.countryCodes[order.Country.code] || order.Country.code;
    let orderId = order.bulkOrderId ? `${order.bulkOrderId}` : `${order.id}`;
    let returnValue = '';
    for(let i = 0; i < BULK_ORDER_LENGTH - orderId.length; i++) {
      returnValue += '0';
    }

    if(!isFull) {
      return `${countryCode}B${returnValue}${orderId}`;
    } else {
      return `${countryCode}B${returnValue}${orderId}-${moment(order.createdAt).format('YYYYMMDD')}`;
    }
  }

  /**
   * Extract order number for display format
   * Example MY000000001-20171127 -> 000000001
   * @param {*} orderNumber 
   */
  static getBulkOrderNumber(orderNumber) {
    loggingHelper.log('info', 'getBulkOrderNumber start');
    loggingHelper.log('info', `param: orderNumber = ${orderNumber}`);
    if(orderNumber.indexOf('-') > -1) {
      return orderNumber.substr(3, orderNumber.indexOf('-') - 3);
    } else {
      return orderNumber.substr(3, BULK_ORDER_LENGTH);
    }
  }

  /**
   * Check type of order base on the order number
   * MY0000000001-20170101 -> order
   * MYB000000001-20170101 -> bulkOrder
   * @param {*} orderNumber 
   */
  static checkOrderNumberType(orderNumber) {
    loggingHelper.log('info', 'checkOrderNumberType start');
    loggingHelper.log('info', `param: orderNumber = ${orderNumber}`);
    if(orderNumber[2] === 'B') {
      return 'bulkOrder';
    } else {
      return 'order';
    }
  }
}

export default OrderHelper;
