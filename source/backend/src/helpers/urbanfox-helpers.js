import Promise from 'bluebird';
import request from 'request';
import moment from 'moment';
import _ from 'underscore';
import { jsonToGraphQLQuery, EnumType } from 'json-to-graphql-query';

import config from '../config';
import SequelizeHelper from './sequelize-helper';
import { loggingHelper } from './logging-helper';
import OrderHelper from './order-helper';

const CONFIG = {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${config.urbanFoxKey.apiKey}`
  }
};

class UrbanFoxHelper {
  static sendRequest(_options) {
    let method = _options.method || 'get';
    let body = _options.body || {};
    let query = _options.query || {};
    let options = {
      url: _options.url ? _options.url : config.urbanFox.host,
      method,
      headers: CONFIG.headers,
      json: body,
      qs: query
    };

    return new Promise((resolve, reject) => {
      request(options, (error, response, data) => {
        console.log(`send requets ${error}, ${JSON.stringify(data)}, ${JSON.stringify(response)}`);
        if(!error && !data.errors) {
          resolve(data);
        } else {
          loggingHelper.log('error', `URBANFOX error: ${JSON.stringify(options)} == ${data.errors ? JSON.stringify(data.errors) : error}`);
          if(data.errors && data.errors.length > 0) {
            let err = new Error(data.errors[0].message);
            err.type = 'urbanfox_error';
            return reject(err);
          } else {
            return reject(error);
          }
        }
      });
    });
  }

  /**
   * Submit transaction to urban fox and get the tracking code
   * @param {*} options 
   */
  static createTransaction(options, db, isBulk) {
    loggingHelper.log('info', `create transaction to urbanfox job ${JSON.stringify(options)}`);

    // build phone number
    let deliveryPhone = options.SellerUserId ? options.phone : options.DeliveryAddress.contactNumber;
    deliveryPhone = /^\d{8}$/g.test(deliveryPhone) ? `65${deliveryPhone}` : deliveryPhone;
    if(!deliveryPhone || (deliveryPhone && !/^\+?\s?65\d{8}$/g.test(deliveryPhone))) {
      deliveryPhone = 'NA';
    }

    let billingPhone = options.SellerUserId ? options.phone : options.BillingAddress.contactNumber;
    billingPhone = /^\d{8}$/g.test(billingPhone) ? `65${billingPhone}` : billingPhone;
    if(!billingPhone || (billingPhone && !/^\+?\s?65\d{8}$/g.test(billingPhone))) {
      billingPhone = 'NA';
    }

    // build name of customer
    let billingName;
    let deliveryName;
    if(!isBulk) {
      deliveryName = options.SellerUserId ? options.fullName : `${options.DeliveryAddress.firstName} ${options.DeliveryAddress.lastName}`;
      billingName = options.SellerUserId ? options.fullName : `${options.BillingAddress.firstName} ${options.BillingAddress.lastName}`;
    } else {
      deliveryName = `${options.DeliveryAddress.firstName} ${options.DeliveryAddress.lastName}`;
      billingName = `${options.BillingAddress.firstName} ${options.BillingAddress.lastName}`;
    }
    deliveryName = deliveryName.replace('null', '');
    billingName = deliveryName.replace('null', '');

    // build input data
    let transaction = {
      'referenceNumber1': `${isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true)}`,
      'remarks': `Fullfills for order ${isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true)}`,
      'payment_type': new EnumType('CREDIT_CARD'),
      'shippingAddress': {
        name: deliveryName,
        addressLine1: options.DeliveryAddress.address.replace(/\n/g, ' '),
        city: options.DeliveryAddress.city,
        country: options.DeliveryAddress.Country.name,
        postalCode: options.DeliveryAddress.portalCode,
        phone: deliveryPhone
      },
      'billingAddress': {
        name: billingName,
        addressLine1: options.BillingAddress.address.replace(/\n/g, ' '),
        city: options.BillingAddress.city,
        country: options.BillingAddress.Country.name,
        postalCode: options.BillingAddress.portalCode,
        phone: billingPhone
      },
      'customerAddress': {
        name: deliveryName,
        addressLine1: options.DeliveryAddress.address.replace(/\n/g, ' '),
        city: options.DeliveryAddress.city,
        country: options.DeliveryAddress.Country.name,
        postalCode: options.DeliveryAddress.portalCode,
        phone: deliveryPhone
      }
    };

    // create transaction
    transaction.orderItems = [];
    options.orderDetails.forEach(details => {
      transaction.orderItems.push({
        sku: details.sku,
        quantity: parseInt(details.qty)
      });
    });

    // build param
    let params = {
      mutation: {
        createOrder: {
          __args: transaction,
          id: true, status: true
        }
      }
    };
    return this.sendRequest({
      method: 'POST',
      body: {
        query: jsonToGraphQLQuery(params)
      }
    })
      .then(result => {
        result.orderId = transaction.billing_uid;
        return result;
      })
      .catch(error => {
        if(error.type === 'urbanfox_error') {
          if(isBulk) {
            return db.BulkOrderHistory.create({
              BulkOrderId: options.id,
              message: `Urbanfox Error: ${error.message}`
            })
              .then(() => {
                throw error;
              });
          } else {
            return db.OrderHistory.create({
              OrderId: options.id,
              message: `Urbanfox Error: ${error.message}`
            })
              .then(() => {
                throw error;
              });
          }
        } else {
          throw error;
        }
      });
  }

  /**
   * Allow create multiple transaction to urbanfox
   * @param {*} options 
   */
  static createTransactions(orderId, db) {
    // get order infor
    let data = {};
    return db.Order.findOne({
      where: {id: orderId, states: 'Payment Received'},
      include: [
        'User',
        'Country',
        'Receipt',
        {
          model: db.DeliveryAddress,
          include: ['Country']
        },
        {
          model: db.DeliveryAddress,
          as: 'BillingAddress',
          include: ['Country']
        },
        {
          model: db.OrderDetail,
          as: 'orderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: ['Product']
            },
            {
              model: db.PlanCountry,
              include: [
                'Plan',
                {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                },
                {
                  model: db.PlanTrialProduct,
                  as: 'planTrialProducts',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(order => {
        order = SequelizeHelper.convertSeque2Json(order);
        if(!order) {
          throw new Error('skip');
        }
        // build order header data
        let hasPlan = false;
        data.order = order;

        // collect product
        let items = [];
        data.order.orderDetail.forEach(details => {
          if(details.ProductCountryId && (!order.SellerUserId || (order.SellerUserId && order.subscriptionIds))) {
            let item = _.findWhere(items, {sku: details.ProductCountry.Product.sku});
            if(item) {
              item.qty += details.qty;
              items = _.reject(items, (obj => obj.sku === item.sku));
              items.push(item);
            } else {
              items.push({
                sku: details.ProductCountry.Product.sku,
                qty: details.qty,
                long: details.ProductCountry.Product.long,
                width: details.ProductCountry.Product.width,
                hight: details.ProductCountry.Product.hight,
                weight: details.ProductCountry.Product.weight
              });
            }
          } else if(details.PlanCountryId && !details.PlanCountry.Plan.isTrial) {
            hasPlan = true;
            if(moment(details.startDeliverDate).isBefore(moment().add(2, 'days'))) {
              details.PlanCountry.planDetails.forEach(planDetail => {
                let item = _.findWhere(items, {sku: planDetail.ProductCountry.Product.sku});
                let deliverPlan = JSON.parse(planDetail.deliverPlan);
                if(item) {
                  item.qty += details.qty * deliverPlan['1'];
                  _.reject(items, (obj => obj.sku === item.sku));
                  items.push(item);
                } else {
                  items.push({
                    sku: planDetail.ProductCountry.Product.sku,
                    qty: details.qty * deliverPlan['1'],
                    long: planDetail.ProductCountry.Product.long,
                    width: planDetail.ProductCountry.Product.width,
                    hight: planDetail.ProductCountry.Product.hight,
                    weight: planDetail.ProductCountry.Product.weight,
                  });
                }
              });
            }
          } else if(details.PlanCountryId && details.PlanCountry.Plan.isTrial) {
            if(order.Country.code === 'MYS') {
              hasPlan = true;
            }
            details.PlanCountry.planTrialProducts.forEach(trialProductDetail => {
              items.push({
                sku: trialProductDetail.ProductCountry.Product.sku,
                qty: trialProductDetail.qty,
                long: trialProductDetail.ProductCountry.Product.long,
                width: trialProductDetail.ProductCountry.Product.width,
                hight: trialProductDetail.ProductCountry.Product.hight,
                weight: trialProductDetail.ProductCountry.Product.weight,
              });
            });
          }
        });

        data.order.orderDetails = items;

        // check BA-order
        if(order.SellerUserId && !order.subscriptionIds && !hasPlan) {
          throw new Error('skip');
        }
        return this.createTransaction(data.order, db);
      })
      .then(() => db.Order.update({
        states: 'Processing',
        carrierAgent: 'courex'
      }, {where: {id: orderId}})
        .then(() => db.OrderHistory.create({
          OrderId: orderId,
          message: 'Processing'
        }))
      )
      .catch(err => {
        if(err.message !== 'skip') {
          throw err;
        }
      });
  }

  /**
   * Allow create multiple transaction to urbanfox
   * @param {*} options 
   */
  static createBulkTransactions(orderId, db) {
    // get order infor
    let data = {};
    return db.BulkOrder.findOne({
      where: {id: orderId, states: 'Payment Received'},
      include: [
        'Country',
        {
          model: db.DeliveryAddress,
          include: ['Country']
        },
        {
          model: db.DeliveryAddress,
          as: 'BillingAddress',
          include: ['Country']
        },
        {
          model: db.BulkOrderDetail,
          as: 'bulkOrderDetail',
          include: [
            {
              model: db.ProductCountry,
              include: ['Product']
            },
            {
              model: db.PlanCountry,
              include: [
                {
                  model: db.PlanDetail,
                  as: 'planDetails',
                  include: [
                    {
                      model: db.ProductCountry,
                      include: ['Product']
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
      .then(order => {
        if(!order) {
          throw new Error('skip');
        }
        // build order header data
        data.order = SequelizeHelper.convertSeque2Json(order);

        // collect product
        let items = [];
        let totalPrice = 0;
        data.order.bulkOrderDetail.forEach(details => {
          let item = _.findWhere(items, {sku: details.ProductCountry.Product.sku});
          totalPrice += parseFloat(details.price);
          if(item) {
            item.qty += details.qty;
            items = _.reject(items, (obj => obj.sku === item.sku));
            items.push(item);
          } else {
            items.push({
              sku: details.ProductCountry.Product.sku,
              qty: details.qty,
              long: details.ProductCountry.Product.long,
              width: details.ProductCountry.Product.width,
              hight: details.ProductCountry.Product.hight,
              weight: details.ProductCountry.Product.weight
            });
          }
        });
        data.order.totalPrice = totalPrice;
        data.order.orderDetails = items;
        return this.createTransaction(data.order, db, true);
      })
      .then(() => db.BulkOrder.update({
        states: 'Processing',
        carrierAgent: 'courex'
      }, {where: {id: orderId}})
        .then(() => db.BulkOrderHistory.create({
          BulkOrderId: orderId,
          message: 'Processing'
        }))
      )
      .catch(err => {
        if(err.message !== 'skip') {
          loggingHelper.log('error', `Create transaction to URBANFOX error: ${err.message}`);
          throw err;
        }
      });
  }

  /**
   * Get transaction status from urban fox
   * @param {*} deliveryId 
   */
  static getTransactionInfo(options, isBulk) {
    loggingHelper.log('info', `getTransactionInfo from urbanfox: ${isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true)}`);
    let params = {
      query: `{ getOrder(referenceNumber1: "${isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true)}") {referenceNumber1, status, remarks, fulfillments {id, trackingNumber}}}`
    };
    return this.sendRequest({
      method: 'POST',
      body: params
    })
      .then(result => result)
      .catch(error => {
        loggingHelper.log('error', `Create transaction from Urbanfox error: ${error.message}`);
      });
  }

  /**
   * Send cancel request to urbanfox
   * @param {*} options 
   * @param {*} isBulk 
   */
  static cancelOrder(options, isBulk) {
    loggingHelper.log('info', `cancelOrder from urbanfox: ${isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true)}`);
    // build param
    let params = {
      mutation: {
        updateOrder: {
          __args: {
            referenceNumber1: isBulk ? OrderHelper.fromatBulkOrderNumber(options, true) : OrderHelper.fromatOrderNumber(options, true),
            status: new EnumType('CANCELLED')
          },
          id: true, status: true
        }
      }
    };
    return this.sendRequest({
      method: 'POST',
      body: {
        query: jsonToGraphQLQuery(params)
      }
    })
      .then(result => result);
  }
}

export default UrbanFoxHelper;
