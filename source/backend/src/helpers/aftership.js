import Promise from 'bluebird';
import Aftership from 'aftership';
import _ from 'underscore';
import config from '../config';
import OrderHelper from './order-helper';
import { loggingHelper } from './logging-helper';

class AfterShip {
  constructor() {
    this.Aftership = Aftership(config.keyAftership.apiKey);
  }

  postTracking(order, isBulk) {
    loggingHelper.log('info', 'postTracking start');
    loggingHelper.log('info', `param: order = ${order}, isBulk = ${isBulk}`);
    return new Promise(resolve => {
      let body = {
        'tracking': {
          'slug': order.carrierAgent,
          'tracking_number': order.deliveryId,
          'smses': [order.DeliveryAddress.contactNumber],
          'emails': [order.email],
          'order_id': isBulk ? OrderHelper.fromatBulkOrderNumber(order) : OrderHelper.fromatOrderNumber(order),
          'order_id_path': `http://www.aftership.com/order_id=${isBulk ? OrderHelper.fromatBulkOrderNumber(order) : OrderHelper.fromatOrderNumber(order)}`
        }
      };
      console.log(`postTracking ===== ${JSON.stringify(body)}`);
      loggingHelper.log('info', `postTracking ===== ${JSON.stringify(body)}`);
      this.Aftership.call('POST', '/trackings', { body }, (err, result) => {
        if(err) {
          console.log(`POST tracking after ship error ${err}`);
          loggingHelper.log('error', `POST tracking after ship Data ${JSON.stringify(body)}. error ${err}`);
          resolve();
        } else {
          console.log(`Post aftership succeed ${JSON.stringify(result)}`);
          loggingHelper.log('info', `Post aftership succeed ${JSON.stringify(result)}`);
          resolve(result);
        }
      });
    });
  }

  doGetTracking(query) {
    loggingHelper.log('info', 'doGetTracking start');
    loggingHelper.log('info', `param: query = ${query}`);
    query = Object.assign({tag: 'Delivered'});
    return new Promise((resolve, reject) => {
      this.Aftership.call('GET', '/trackings', { query }, (err, result) => {
        if(err) {
          console.log(`GET tracking status after ship error ${err}`);
          loggingHelper.log('error', `GET tracking status after ship error ${err}`);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  getTrackings() {
    loggingHelper.log('info', 'getTrackings start');
    let trackings = [];
    return this.doGetTracking({})
      .then(result => {
        console.log(`getTrackings ===== ${JSON.stringify(result)}`);
        loggingHelper.log('info', `getTrackings ===== ${JSON.stringify(result)}`);
        trackings = trackings.concat(result.data.trackings);
        if(result.data.count <= 100) {
          return trackings;
        } else {
          // build option query
          let queries = [];
          let totalpage = Math.ceil(result.data.count / 100);
          for(let page = 2; page <= totalpage; page++) {
            queries.push({page});
          }
          return Promise.mapSeries(queries, query => this.doGetTracking(query)
            .then(_result => _result.trackings))
            .then(_result => _.flatten(_result, true))
            .then(_result => trackings.concat(_result));
        }
      });
  }
}

export let aftership = new AfterShip();
