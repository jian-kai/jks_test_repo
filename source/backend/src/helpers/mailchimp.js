import Promise from 'bluebird';
import request from 'request';
import config from '../config';
import _ from 'underscore';
import nodemailer from 'nodemailer';
import { loggingHelper } from './logging-helper';

const CONFIG = {
  host: 'api.mailchimp.com',
  prefix: '/3.0',
  headers: {
    'Content-Type': 'application/json',
    'User-Agent': 'MailChimp-V3/1.0.0'
  },
  debug: false
};

let instance = null;

class MailChimp {
  constructor(options) {
    this.options = options || {};

    // set default for mailchimp API key
    this._apiKey = this.options.apiKey ? this.options.apiKey : config.keyEmail.mailchimpApiKey;

    let apikeyRegex = /.+\-.+/;

    if(!apikeyRegex.test(this._apiKey)) {
      loggingHelper.log('error', `missing or invalid api key: ${this._apiKey}`);
      throw new Error(`missing or invalid api key: ${this._apiKey}`);
    }
    this._baseUrl = `https://${this._apiKey.split('-')[1]}.${CONFIG.host}${CONFIG.prefix}`;

    // init mandrill
    this.transporter = nodemailer.createTransport(config.keyEmail.mandrillConfig);
  }

  static getInstance() {
    if(!instance) {
      instance = new MailChimp();
    }
    return instance;
  }

  getLists(params) {
    loggingHelper.log('info', 'getLists start');
    params = params || {};
    return this._sendRequest({
      path: '/lists',
      query: params
    });
  }

  getMembers(listId) {
    loggingHelper.log('info', 'getMembers start');
    let members = [];
    return this._sendRequest({
      path: `/lists/${listId}`
    })
      .then(list => {
        console.log(`member in simple list === ${JSON.stringify(list.stats)}`);
        loggingHelper.log('info', `member in simple list === ${JSON.stringify(list.stats)}`);
        let memberCount = list.stats.member_count + list.stats.unsubscribe_count + list.stats.cleaned_count;
        let offsets = [];
        for(let offset = 0; offset < memberCount; offset += 50) {
          offsets.push(offset);
        }
        return Promise.mapSeries(offsets, offset => Promise.delay(10)
          .then(() => this._sendRequest({
            path: `/lists/${listId}/members?count=50&offset=${offset}`
          })
          .then(result => {
            members = members.concat(result.members);
          })))
          .then(() => members);
      });
  }

  getActiveMembers(listId) {
    loggingHelper.log('info', 'getActiveMembers start');
    return this._sendRequest({
      path: `/lists/${listId}/members?status=subscribed`
    });
  }
  createMember(listId, options) {
    loggingHelper.log('info', 'createMember start');
    return this._sendRequest({
      path: `/lists/${listId}/members`,
      method: 'post',
      body: options
    }).catch(error => {
      console.log(`create mail chimp member error == ${JSON.stringify(error)}`);
      loggingHelper.log('error', `create mail chimp member error == ${JSON.stringify(error)}`);
      if(error.title !== 'Member Exists') {
        throw new Error(error);
      }
      return {};
    });
  }
  updateMember(listId, memberId, options) {
    loggingHelper.log('info', 'updateMember start');
    return this._sendRequest({
      path: `/lists/${listId}/members/${memberId}`,
      method: 'patch',
      body: options
    }).catch(error => {
      console.log(`update mal chimp member error == ${JSON.stringify(error)}`);
      loggingHelper.log('error', `update mailchimp member error == ${JSON.stringify(error)}`);
      return {};
    });
  }
  createOrActiveMembers(listId, options) {
    loggingHelper.log('info', 'createOrActiveMembers start');
    return this.getMembers(listId)
      .then(members => {
        let member = _.findWhere(members, {'email_address': options.email_address});
        if(!member) {
          // add new member into list
          return this._sendRequest({
            path: `/lists/${listId}/members`,
            method: 'post',
            body: options
          });
        } else if(member.status !== 'subscribed') {
          return this._sendRequest({
            path: `/lists/${listId}/members/${member.id}`,
            method: 'patch',
            body: { status: 'subscribed' }
          });
        }
      })
      .catch(error => {
        console.log(`send email error == ${JSON.stringify(error)}`);
        loggingHelper.log('error', `send email error == ${JSON.stringify(error)}`);
        if(error.title !== 'Member Exists') {
          throw new Error(error);
        }
        return {};
      });
  }

  deleteMembers(listId, subscriberHash) {
    loggingHelper.log('info', 'deleteMembers start');
    return this._sendRequest({
      path: `/lists/${listId}/members/${subscriberHash}`,
      method: 'delete'
    });
  }

  createCampaign(options) {
    loggingHelper.log('info', 'createCampaign start');
    return this._sendRequest({
      path: '/campaigns',
      method: 'post',
      body: {
        type: 'regular',
        recipients: {
          'list_id': options.listId
        },
        settings: {
          'subject_line': options.subject,
          'from_name': options.fromName,
          'reply_to': config.email.replyToEmail
        }
      }
    });
  }

  putCampaignTemplate(campaignId, templateId, sections) {
    loggingHelper.log('info', 'putCampaignTemplate start');
    sections = sections || {};
    return this._sendRequest({
      path: `/campaigns/${campaignId}/content`,
      method: 'put',
      body: {
        'template': {
          id: templateId,
          sections
        }
      }
    });
  }

  putCampaignReceives(campaignId, listId) {
    loggingHelper.log('info', 'putCampaignReceives start');
    return this._sendRequest({
      path: `/campaigns/${campaignId}`,
      method: 'patch',
      body: {
        recipients: {
          'list_id': listId
        }
      }
    });
  }

  sendCampaign(campaignId) {
    loggingHelper.log('info', 'sendCampaign start');
    return this._sendRequest({
      path: `/campaigns/${campaignId}/actions/send`,
      method: 'post'
    });
  }

  _sendRequest(_options) {
    let method = _options.method || 'get';
    let path = _options.path || '';
    let body = _options.body || {};
    let query = _options.query || {};
    let options = {
      url: `${this._baseUrl}${path}`,
      method,
      headers: CONFIG.headers,
      json: body,
      qs: query,
      auth: {
        user: 'any',
        password: this._apiKey
      },
    };

    return new Promise((resolve, reject) => {
      request(options, (error, response, data) => {
        // console.log(`send requets ${error}, ${JSON.stringify(response)}`);
        if(!error && (response.statusCode === 200 || response.statusCode === 201 || response.statusCode === 204)) {
          resolve(data);
        } else {
          reject(response.body || error);
        }
      });
    });
  }

  /**
   * Send an transaction email by mailchimp to 1 receiver
   * @param {*} options (listId, email, subject, templateId, sections)
   */
  sendTransactionEmail(options) {
    loggingHelper.log('info', 'sendTransactionEmail start');
    options.from = config.email.fromEmail;
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(options, (error, info) => {
        if(error) {
          console.log(`Send email ERROR - ${JSON.stringify(error)}`);
          loggingHelper.log('error', `Send email ERROR - ${JSON.stringify(error)}`);
          reject(error);
        } else {
          console.log(`Message sent: ${JSON.stringify(info)}`);
          loggingHelper.log('info', `Message sent: ${JSON.stringify(info)}`);
          resolve(info);
        }
      });
    });
  }

  // Helper funciton for mailchimp
  addMemberToRegisterList(data) {
    return this.createMember(config.email.registerListId, data);
  }

  addMemberToSubscriptionList(data) {
    return this.createMember(config.email.subscriberListId, {
      'email_address': data.email,
      'status': 'subscribed',
      'merge_fields': {
        FNAME: data.firstName,
        LNAME: data.lastName,
        COUNTRY: data.Country.code.substr(0, 2),
        SOURCE: data.badgeId ? 'BA app' : 'Website'
      }
    });
  }

  // List for trial plan user
  addMemberToTrialSubscriptionList(data) {
    let utmDefaultData = this.getDefaultUtmData();
    return this.createMember(config.email.trialSubscriberList, {
      'email_address': data.user.email,
      'status': 'subscribed',
      'merge_fields': {
        FNAME: data.user.firstName,
        LNAME: data.user.lastName ? data.user.lastName : '',
        COUNTRY: data.user.Country.code.substr(0, 2),
        SOURCE: data.user.badgeId ? 'BA app' : 'Website',
        USOURCE: data.order.source ? data.order.source : this.getDefaultUtmData('source'),
        UMEDIUM: data.order.medium ? data.order.medium : utmDefaultData,
        UCAMPAIGN: data.order.campaign ? data.order.campaign : utmDefaultData,
        UCONTENT: data.order.content ? data.order.content : utmDefaultData,
        UTERM: data.order.term ? data.order.term : utmDefaultData
      }
    });
  }

  // List for custom plan user
  addMemberToCustomPlanList(data) {
    let utmDefaultData = this.getDefaultUtmData();
    return this.createMember(config.email.customPlanList, {
      'email_address': data.user.email,
      'status': 'subscribed',
      'merge_fields': {
        FNAME: data.user.firstName,
        LNAME: data.user.lastName ? data.user.lastName : '',
        COUNTRY: data.user.Country.code.substr(0, 2),
        SOURCE: data.user.badgeId ? 'BA app' : 'Website',
        USOURCE: data.order.source ? data.order.source : this.getDefaultUtmData('source'),
        UMEDIUM: data.order.medium ? data.order.medium : utmDefaultData,
        UCAMPAIGN: data.order.campaign ? data.order.campaign : utmDefaultData,
        UCONTENT: data.order.content ? data.order.content : utmDefaultData,
        UTERM: data.order.term ? data.order.term : utmDefaultData
      }
    });
  }

  addMemberToNewCustomerEmailForKoreaList(data) {
    return this.createMember(config.email.newCustomerEmailForKoreaList, {
      'email_address': data.email,
      'status': 'subscribed',
      'merge_fields': {
        FNAME: 'Guest',
        LNAME: 'Guest',
        COUNTRY: data.Country.code.substr(0, 2),
        SOURCE: 'Website'
      }
    });
  }

  getDefaultUtmData(field = '') {
    return field == 'source' ? 'direct' : 'none';
  }
}

export default MailChimp;
