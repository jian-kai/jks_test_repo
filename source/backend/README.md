#Shaves2U APIs Documentation
---

Shaves2U API is organized around REST. JSON will be returned in all responses from the API, including errors. All request payloads must be have `content-type` equal `application/json`

#### API Endpoint ####

**http://smartshave.bjdev.net/api/**

<span style="color:green; font-weight: bold; font-size: 18px">
NOTE : All of request that require token must be have <em>x-access-token</em> attribute in header(except <em>/login</em> and <em>/regiter</em> API)
</span>

**Object Model**

## User ##

```
{
    "id": 1,
    "email": "kimtn129@gmail.com",
    "password": "0192023a7bbd73250516f069df18b500",
    "firstName": "Kim",
    "lastName": "Thi Nhuoc",
    "socialId": null,
    "birthday": "1985-05-02",
    "gender": "male",
    "defaultLanguage": "en",
    "defaultDateFormat": "yyyy/MM/dd",
    "avatarUrl": "https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/user-avatars/fdc700b0-71bb-11e7-933c-cda77e0dd831.png",
    "createdAt": "2017-07-26T04:36:14.000Z",
    "updatedAt": "2017-07-26T04:37:30.000Z",
    "CountryId": 1,
    "UserTypeId": 1
}
```

## Notification ##

```
{
    "id": 1,
    "UserId": 1,
    "NotificationType": {
        "name": "xxx",
        "service_image_url": "http://xxx"
    },
    "content": "xxx",
    "happendDate": "2017/12/12 23:12:12"
}
```

## Country object ##

```
{
    "id": 1,
    "code": "EN",
    "name": "English",
    "currency": "GBP"
}
```

## City object ##

```
{
    "id": 1,
    "name": "London",
    "country": <country object>
}
```

## Delivery Address ##

```
{
    "id": 1,
    "city": <city object>
    "UserId": 1,
    "address": "xxx",
    "addressRegion": "yyy",
    "addressPortalCode": "zzz",
    "isDefault": true
}
```

## Category Object ##
```
{
    "id": 1,
    "name": "xxx",
    "description": "yyy"
}
```

## Product Object ##

```
{
    "id": 1,
    "sku": "ASK-2017",
    "rating": 0,
    "ProductType": {
        "id": 1,
        "name": "Shave kit"
    },
    "productImages": [
        {
            "id": 1,
            "url": "/assets/uploads/products/product_photo-1.jpg",
            "ProductId": 1,
            "isDefault": true
        },
        {
            "id": 2,
            "url": "/assets/uploads/products/product_photo-2.jpg",
            "ProductId": 1
        },
        {
            "id": 3,
            "url": "/assets/uploads/products/product_photo-3.jpg",
            "ProductId": 1
        }
    ],
    "productTranslate": [{
        "id": 1,
        "name": "AWESOME SHAVE KIT",
        "isDefault": true,
        "description": "Your Awesome Shave Kit contains a great grippy sturdy handle, a pack of each super ceramic coated 5 blade razors & 3 blade razors, \n                        cooling shave cream and soothing after shave cream. Basically the best of our goodies from www.shaves2u.com - hand-picked, packed & delivered 2 U with love."
    }],
    "productCountry": {
        "id": 1,
        "basedPrice": "10",
        "sellPrice": "12",
        "qty": 10,
        "Country": {
            "id": 1,
            "code": "MYS",
            "name": "Malaysia",
            "currency": "MYR"
        }
    }
}
```

## Card Object ##

```
[{
    "id": 1,
    "product": {
        "id": 1,
        "sku": "ASK-2017",
        "rating": 0,
        "productImageURL": "/assets/uploads/products/product_photo-1.jpg",
        "productTranslate": [{
            "id": 1,
            "name": "AWESOME SHAVE KIT",
            "isDefault": true
            "description": "Your Awesome Shave Kit contains a great grippy sturdy handle, a pack of each super ceramic coated 5 blade razors & 3 blade razors, \n                        cooling shave cream and soothing after shave cream. Basically the best of our goodies from www.shaves2u.com - hand-picked, packed & delivered 2 U with love."
        }],
        "productCountry": {
            "id": 1,
            "basedPrice": "10",
            "sellPrice": "12",
            "qty": 10,
            "Country": {
                "id": 1,
                "code": "MYS",
                "name": "Malaysia",
                "currency": "MYR"
            }
        }
    },
    "qty": 10
}]
```

## Plan Type Object ##

```
{
    "id": 1,
    "name": "xxx"
    "totalDeliverTime": 4,
    "subsequentDeliverDuration": 3,
    "isOneTimeCharge": trua
}
```

## Plan Object ##

```
{
    "id": 1,
    "price": 10
    "currency": "gbp",
    "planType": <plantype object>,
    "planTranslate": [
        {
            "id": 1,
            "country": <country object>,
            "plan_name": "xxx",
            "plan_description": "yyy"
        }
    ]
}
```

## Subscription Object ##

```
{
    "id": 1,
    "user": <user object>,
    "plan": <plan object>,
    "customerPaymentId": "xxx",
    "nextDeliverDate": "yyyy/MM/dd",
    "currentDeliverNumber": 1,
    "deliveryAddress": <delivery addresss object>,
    "totalDeliverTimes": 4,
    "status": "inprogress"
}
```

---

**Summary of Resource URL Patterns**

## Website : ##

##### Authentication APIs : #####
 - User Login: [POST: /users/login](#markdown-header-user-login)
 - Seller Login: [POST: /sellers/login](#markdown-header-sale-login)
 - Register: [POST: /users/register](#markdown-header-register)

##### User APIs : #####
 - Update user info: [PUT: /users/:userId](#markdown-header-update-user-info)
 - Reset password: [POST: /users/:userId/reset-password](#markdown-header-reset-password)
 - Send Active Email: [POST: /users/send-active-email](#markdown-header-send-active-email)
 - Check Email: [POST: /users/check-email](#markdown-header-check-email)
 - Invite friend: [POST: /users/:userId/invite-friend](#markdown-header-invite-friend)
 - Get notification: [GET: /users/:userId/notifications](#markdown-header-get-notifications)
 - Read notification: [POST: /users/:userId/notifications/read](#markdown-header-read-notifications)
 - Get delivery address: [GET: /users/:userId/deliveries](#markdown-header-get-delivery-address)
 - Add delivery address: [POST: /users/:userId/deliveries](#markdown-header-add-delivery-address)
 - Set default delivery address: [POST: /users/:userId/deliveries/:deliveryId/set-default](#markdown-header-set-default-delivery-address)
 - Update delivery address: [POST: /users/:userId/deliveries/:deliveryId](#markdown-header-update-delivery-address)
 - Delete delivery address: [DELETE: /users/:userId/deliveries/:deliveryId](#markdown-header-delete-delivery-address)
 - Get recently viewed: [GET: /users/:userId/recently-viewed](#markdown-header-get-recently-viewed)
 - Add recently viewed: [POST: /users/:userId/recently-viewed](#markdown-header-add-recently-viewed)
 - Sync cart: [POST: /users/:userId/carts](#markdown-header-sync-cart)
 - Get delivery address: [GET: /users/:userId/deliveries](#markdown-header-get-delivery-address)
 - Get orders: [GET: /users/:userId/orders](#markdown-header-get-orders)
 - Get order details: [GET: /users/:userId/orders/:orderId](#markdown-header-get-order-details)
 - Get subscriptions: [GET: /users/:userId/subscriptions](#markdown-header-get-subscriptions)
 - Get notification type: [GET: /notifications](#markdown-header-get-notifications)
 - Get Card: [GET: /users/:userId/cards](#markdown-header-get-card)
 - Add Card: [GET: /users/:userId/cards](#markdown-header-add-card)
 - Delete Card: [GET: /users/:userId/cards?:cardId](#markdown-header-delete-card)

##### Products APIs : #####
 - Get Product Type [GET: /product-types](#markdown-header-get-product-type)
 - Get Products: [GET: /products](#markdown-header-get-products)
 - Product detail: [GET: /products/:productId](#markdown-header-product-detail)
 - Add product vatiant: [POST: /products/vatiant](#markdown-header-add-product-vatiant)
 - Add product vatiant option: [POST: /products/vatiant-option](#markdown-header-add-product-vatiant-option)
 - Add review: [POST: /products/:productId/reviews](#markdown-header-add-review)

##### Article APIs : #####
 - Get Article: [GET: /articles](#markdown-header-get-article)
 - Add Article: [POST: /articles](#markdown-header-add-article)
 - Get Article Details: [GET: /articles/:articleId](#markdown-header-get-article-details)

##### Shopping : #####
 - Place Order: [POST /orders](#markdown-header-place-order)
 - Checkout Stripe: [POST /payment-gateway/stripe](#markdown-header-checkout-stripe)
 - Checkout Ipay88: [POST /payment-gateway/ipay88](#markdown-header-checkout-ipay88)
 - Get Order: [GET /orders](#markdown-header-get-order)

##### Store Locator : #####
 - Get Store Locator: [GET: /store-locators](#markdown-header-get-store-locator)

##### Subscription : #####
 - Get Plan Type: [GET: /plan-types](#markdown-header-get-plan-type)
 - Get Plans: [GET: /plans](#markdown-header-get-plans)
 - Post Plan: [POST: /plans](#markdown-header-post-plans)
 - Post Subscription: [POST: /subscriptions](#markdown-header-post-subscription)

##### Promotions : #####
 - Get Promotions: [GET: /promotions](#markdown-header-get-promotions)
 - Check Promo Code: [POST: /promotions/check](#markdown-header-check-promo-code)

##### Countries : #####
 - Get Countries: [GET: /countries](#markdown-header-get-countries)
 - Add Countries: [POST: /countries](#markdown-header-add-countries)
 - Update Countries: [PUT: /countries](#markdown-header-update-countries)

## Admin : ##

##### Authentication APIs : #####
 - Admin Login: [POST: /admin/login](#markdown-header-admin-login)
 - Add Admin: [POST: /admin](#markdown-header-add-admin)
 - Update Admin Info: [PUT: /admin/:id](#markdown-header-update-admin-info)
 - Change Admin Password: [POST: /admin/:id/password](#markdown-header-change-admin-password)

---

## User Login ##

**POST** [/users/login]()

**Input format:**

- Login with email

    ```
    {
        "email": "xxx@gmail.com",
        "password": "asdasd"
    }
    ```
- Login with facebook

    ```
    {
        "social_id": "xxxxx"
    }
    ```

**Response Success:**

```
{
    "token": <jwt-token> \\ set to header to make request payloads to sever
    "user" : <user object>
}
```
---

## Seller Login ##

**POST** [/sellers/login]()

**Input format:**

- Login with email

    ```
    {
        "email": "xxx@gmail.com",
        "password": "asdasd"
    }
    ```

**Response Success:**

```
{
    "token": <jwt-token> \\ set to header to make request payloads to sever
    "user" : <user object>
}
```
---

## Register ##

**POST** [/users/register]()

**Input format:**

```
{
	"email": "kimtn129@gmail.com",
	"password": "admin123",
	"firstName": "xxx",
	"lastName": "xxx",
    "gender": "male",
    "birthday": "2017/12/12",
    "socialId": "xxxxxx"
}
```

**Response Success:**

```
{
    "token": <jwt-token> \\ set to header to make request payloads to sever
    "user" : <user object>
}
```
---

## Update User Info ##

**PUT** [/users/:userId]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "avatar": <file: base64>, <optional>
    "fullname": "xxx", <optional>
    "gender": "male", <optional>
    "birthday": "2017/12/12", <optional>
    "defaultLanguage": "en", <optional>
    "defaultDateFormat": "yyyy/MM/dd", <optional>
}
```

**Response Success:**

```
{
    "token": <jwt-token> \\ set to header to make request payloads to sever
    "user" : <user object>
}
```
---

## Reset password ##

**POST** [/users/:userId/reset-password]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "oldPassword": "xxx",
    "newPassword": "xxx"
}
```

**Response Success:**

```
{
    "ok" : true
}
```
---

## Send Active Email ##

**POST** [/users/send-active-email]()

**Header**

**Input format:**

```
{
    "email": "xxx"
}
```

**Response Success:**

```
{
    "ok" : true
}
```
---

## Check Email ##

**POST** [/users/check-email]()

**Header**

**Input format:**

```
{
    "email": "xxx"
}
```

**Response Success:**

```
{
    "ok" : true
}
```
---

## Invite friend ##

**POST** [/users/:userId/invite-friend]()

**Header**

```
    - content-type: multipart/form-data
    - x-access-token: <login token>
```

**Input format:**

```
{
    "email": "xxx@gmail.com"
}
```

**Response Success:**

```
{
    "ok" : true
}
```
---

## Get notification ##

**GET** [/users/:userId/notifications]()

**Header**

```
    - content-type: multipart/form-data
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
{
    notifications: [<notification object>]
}
```
---

## Read notification ##

**POST** [/users/:userId/notifications/read]()

**Header**

```
    - content-type: multipart/form-data
    - x-access-token: <login token>
```

**Input format:**

```
{
    notification_ids: [<notification_id>]
}
```

**Response Success:**

```
{
    "ok" : true
}
```
---

## Get delivery address ##

**GET** [/users/:userId/deliveries]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
{
    delivery_addresses: [<delivery_address object>]
}
```
---

## Add delivery address ##

**POST** [/users/:userId/deliveries]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "CountryId": 1,
    "address": "18 Khue My Dong 21212",
    "city": "Da Nang",
    "contactNumber": "1659612968",
    "firstName": "Kim",
    "lastName": "Nhuoc",
    "portalCode": "550000",
    "state": "Kota Bharu",
    "addressType": ["billing", "shipping"]
}
```

**Response Success:**

```
{
    "ok": true,
    "data": [
        {
            "isDefault": true,
            "id": 5,
            "address": "18 Khue My Dong 21212",
            "contactNumber": "1659612968",
            "firstName": "Kim",
            "lastName": "Nhuoc",
            "portalCode": "550000",
            "state": "Kota Bharu",
            "addressType": "billing"
        },
        {
            "isDefault": true,
            "id": 6,
            "address": "18 Khue My Dong 21212",
            "contactNumber": "1659612968",
            "firstName": "Kim",
            "lastName": "Nhuoc",
            "portalCode": "550000",
            "state": "Kota Bharu",
            "addressType": "shipping"
        }
    ]
}
```

---

## Set Default Delivery Address ##

**POST** [/users/:userId/deliveries/:deliveryId/set-default]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
{
    "ok": true
}
```

---

## Update delivery address ##

**POST** [/users/:userId/deliveries/:deliveryId]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "firstName": "aaa",
    "lastName": "bbb",
    "contactNumber": "1111",
    "state": "aaaa"
    "address": "xxx",
    "portalCode": "zzz",
    "city": "www",
    "isDefault": true,
    "addressType": "shipping||billing",
    "CountryId": 1
}
```

**Response Success:**

```
{
    "ok": true
}
```
---

## Delete delivery address ##

**DELETE** [/users/:userId/deliveries/:deliveryId]()

**Header**

```
    - x-access-token: <login token>
```

**Response Success:**

```
{
    "ok": true
}
```

---

## Get Card ##

**GET** [/users/:userId/cards]()

**Header**

```
    - x-access-token: <login token>
```

**Response Success:**

```
[
    {
        "id": 1,
        "customerId": "cus_BEUR1njQ8JbGid",
        "cardNumber": "4242",
        "branchName": "Visa",
        "cardName": "Thi Nhuoc Kim",
        "expiredYear": "2018",
        "expiredMonth": "12",
        "isDefault": null
    }
]
```

---

## Add Card ##

**POST** [/users/:userId/cards]()

**Header**

```
    - x-access-token: <login token>
```

**Input**

```
{
  "stripeToken": "tok_1As1SiIPTYF0fPcNldkSemVV"
}
```

**Response Success:**

```
{
    "id": 1,
    "customerId": "cus_BEUR1njQ8JbGid",
    "cardNumber": "4242",
    "branchName": "Visa",
    "cardName": "Thi Nhuoc Kim",
    "expiredYear": "2018",
    "expiredMonth": "12",
    "isDefault": null
}
```

---

## Delete Card ##

**POST** [/users/:userId/cards/:cardId]()

**Header**

```
    - x-access-token: <login token>
```

**Input**

**Response Success:**

```
{
    "ok": true
}
```

---

## Get recently viewed ##

**GET** [/users/:userId/recently-viewed]()

**Header**

```
    - content-type: multipart/form-data
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
[
    {
        "id": 1,
        "UserId": 1,
        "product": <product object>
    }
    ...
]
```
---

## Add recently viewed ##

**POST** [/users/:userId/recently-viewed]()

**Header**

```
    - content-type: multipart/form-data
    - x-access-token: <login token>
```

**Input format:**

```
{
    "productCountryId": 1
}
```

**Response Success:**

```
{
    "id": 1,
    "UserId": 1,
    "product": <product object>
}
```
---

## Sync cart ##

**POST** [/users/:userId/carts]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "carts": [
        {
            "productCountryId" 1,
            "qty": 10
        }
    ]
}
```

**Response Success:**

```
{
    "ok": true
}
```
---

## Get Delivery Address ##

**GET** [/users/:userId/deliveries]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
[
    {
        "id": 1,
        "firstName": "Kim",
        "lastName": "Thi",
        "contactNumber": "+841659612968",
        "state": "Selangor",
        "address": "75 Kg Sg Ramal Luar",
        "portalCode": "43000",
        "addressType": "shipping||billing",
        "isDefault": true
    }
]
```
---

## Get Orders ##

**GET** [/users/:userId/orders]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
[
    {
        "id": 6,
        "status": "awaiting",
        "deliveryId": null,
        "totalPrice": "76",
        "currency": "MYR",
        "email": "kimtn129@gmail.com",
        "DeliveryAddressId": 1,
        "BillingAddressId": 1,
        "orderDetail": [
            {
                "id": 12,
                "qty": 2,
                "price": "22",
                "OrderId": 6,
                "ProductCountry": {
                    "id": 2,
                    "basedPrice": "20",
                    "sellPrice": "22",
                    "qty": 2,
                    "Product": {
                        "id": 2,
                        "sku": "H1",
                        "rating": 2,
                        "isFeatured": false,
                        "productTranslate": [
                            {
                                "id": 2,
                                "name": "MEN'S RAZOR HANDLE",
                                "description": "Crafted metal handle with soft rubber grip and chrome finish.\n                        ||Fits both S3 and S5 cartridges.\n                        ||Precision balanced grip for smooth shaving.\n                        ||A beautiful looking handle that has great heft.",
                                "langCode": "EN",
                                "isDefault": true,
                                "ProductId": 2
                            }
                        ],
                        "productImages": [
                            {
                                "id": 4,
                                "url": "/uploads/products/men-handle-main_1.jpg",
                                "isDefault": true,
                                "ProductId": 2
                            },
                            {
                                "id": 5,
                                "url": "/uploads/products/men-premium-razor-handle-01.jpg",
                                "isDefault": false,
                                "ProductId": 2
                            },
                            {
                                "id": 6,
                                "url": "/uploads/products/men-premium-razor-handle-02.jpg",
                                "isDefault": false,
                                "ProductId": 2
                            }
                        ]
                    }
                }
            }
        ]
    }
]
```
---

## Get Order Details ##

**GET** [/users/:userId/orders/:orderId]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
{
        "id": 6,
        "status": "awaiting",
        "deliveryId": null,
        "totalPrice": "76",
        "currency": "MYR",
        "email": "kimtn129@gmail.com",
        "DeliveryAddressId": 1,
        "BillingAddressId": 1,
        "orderDetail": [
            {
                "id": 12,
                "qty": 2,
                "price": "22",
                "OrderId": 6,
                "ProductCountry": {
                    "id": 2,
                    "basedPrice": "20",
                    "sellPrice": "22",
                    "qty": 2,
                    "Product": {
                        "id": 2,
                        "sku": "H1",
                        "rating": 2,
                        "isFeatured": false,
                        "productTranslate": [
                            {
                                "id": 2,
                                "name": "MEN'S RAZOR HANDLE",
                                "description": "Crafted metal handle with soft rubber grip and chrome finish.\n                        ||Fits both S3 and S5 cartridges.\n                        ||Precision balanced grip for smooth shaving.\n                        ||A beautiful looking handle that has great heft.",
                                "langCode": "EN",
                                "isDefault": true,
                                "ProductId": 2
                            }
                        ],
                        "productImages": [
                            {
                                "id": 4,
                                "url": "/uploads/products/men-handle-main_1.jpg",
                                "isDefault": true,
                                "ProductId": 2
                            },
                            {
                                "id": 5,
                                "url": "/uploads/products/men-premium-razor-handle-01.jpg",
                                "isDefault": false,
                                "ProductId": 2
                            },
                            {
                                "id": 6,
                                "url": "/uploads/products/men-premium-razor-handle-02.jpg",
                                "isDefault": false,
                                "ProductId": 2
                            }
                        ]
                    }
                }
            }
        ]
    }
```
---

## Get Subscriptions ##

**GET** [/users/:userId/subscriptions]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
[
    {
        "id": 1,
        "customerId": "cus_B9L4ZzAUj6n9vZ",
        "nextDeliverDate": null,
        "currentDeliverNumber": 0,
        "totalDeliverTimes": 1,
        "nextChargeDate": null,
        "currentChargeNumber": 0,
        "totalChargeTimes": 1,
        "currentOrderId": null,
        "status": "processing",
        "fullname": "thi nhuoc kim",
        "email": "xxx@gmail.com",
        "phone": "xxxxx",
        "createdAt": "2017-08-04T09:48:29.000Z",
        "updatedAt": "2017-08-04T09:48:29.000Z",
        "PlanId": 1,
        "UserId": 1,
        "DeliveryAddressId": 1,
        "BillingAddressId": 1,
        "Plan": {
            "id": 1,
            "price": "220",
            "createdAt": "2017-08-04T09:39:43.000Z",
            "updatedAt": "2017-08-04T09:39:43.000Z",
            "PlanTypeId": 1,
            "CountryId": 1,
            "planImages": [
                {
                    "id": 1,
                    "url": "/uploads/plans/plan-s3.jpg",
                    "isDefault": true,
                    "PlanId": 1
                },
                {
                    "id": 2,
                    "url": "/uploads/plans/plan-s3-1.jpg",
                    "isDefault": false,
                    "PlanId": 1
                }
            ],
            "planTranslate": []
        }
    }
]
```
---

## Get Notifications ##

**GET** [/notifications]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

**Response Success:**

```
[
    {
        "id": 1,
        "name": "New Promotion",
        "serviceImageUrl": null
    },
    {
        "id": 2,
        "name": "Subscription Card Error",
        "serviceImageUrl": null
    },
    {
        "id": 3,
        "name": "New Product",
        "serviceImageUrl": null
    }
]
```
---

## Get Products type ##

**GET** [/product-types]()

**Input format:**

**Response Success:**

```
[
    <product-type object>
]
```

---

## Get Products ##

**GET** [/products]()

**Input format:**

```
{
    "productType": 1, <optional>
    "name": 'xxx', <optional>
    "fromPrice": 10, <optional>
    "toPrice": 20, <optional>
    "limit": 1, <optional>
    "offset": 1, <optional>
    "orderBy": [array of column], <optional> // add _DESC to the column that you want to sort desc
}
```

**Response Success:**

```
[
    <product object>
]
```
---

## Get Products ##

**GET** [/products/:productId]()

**Input format:**


**Response Success:**

```
    "product": <product object>
    "reviews": [
        <review object>
    ]
```
---

## Add review ##

**POST** [/products/:productId/reviews]()

**Input format:**

```
{
    "comment": "xxx",
    "rating": 10
}
```

**Response Success:**

```
{
    "ok": true
}
```
---

## List Article ##

**GET** [ /articles]()

**Input format:**

**Response Success:**

```
[
    <article object>
]
```
---

## Add Article ##

**POST** [ /articles]()

**Header**

```
    - content-type: multipart/form-data
```

**Input format:**

```
{
    "banner": <image file>,
    "category_id": 1,
    "hash_tag": "#xxx, #yyyy",
    "article_translate" [
        {
            "country_id": 1,
            "content": "xxx"
        }
        ...
    ]
}
```

**Response Success:**

```
{
    "ok": true
}
```
---

## Get A-ticle D-tails ##

**GET** [ /articles/:articleId]()

**InpuO fo-mat:**

**Resp-nse Su-cesL:**

```
<article object>
```

---

## Plan Order ##

**POST** [ /orders]()

**Input format:**

```
{
    "deliveryAddressId": 1,
    "billingAddressId": 1,
    "promotionCode": 1,
    "orderDetail": [
        {
            "productCountryId": 1,
            "qty": 10
        },
        {
            "productCountryId": 2,
            "qty": 5
        }
    ],
    "email": "xxx@gmail.com"
}
```

**Response Success:**

```
<Order object>
```
---

## Checkout Stripe##

**POST** [ /payment-gateway/stripe]()

**Input format:**

```
{
    "orderId": 1,
    "cardId": 1
}
```

**Response Success:**

```
{
    "ok": true
}
```
---
## Checkout Ipay88##

**POST** [ /payment-gateway/ipay88]()

**Input format:**

```
{
    "orderId": 1
}
```

**Response Success:**

```
{
    "ok": true
}
```
---

## Get order ##

**GET** [ /orders]()

**Input format:**


**Response Success:**

```
[
    {
        "id": 1,
        "status": "paid", // paid|delivering|delivered
        "total_price": 100,
        "currency": "gbp",
        "promotion": {
            "name": "xxx",
            "discount": 10
        },
        "order_detail": [
            {
                "id": 1,
                "product": <product object>,
                "qty": 10,
                "price": 20
            }
        ],
        "receipt": {
            "id": 1,
            "charge_id": "xxx",
            "charge_fee": "yyy"
        }
    }
]
```
---

## Get Plan Type ##

**GET** [ /plan-types]()

**Input format:**


**Response Success:**

```
[
    <plan type object>
]
```
---

##Get Plan ##

**GET** [ /plans]()

**Input format:**

```
{
    CountryCode: 1,
    PlanTypeId: 1 <optional>
}
```

**Response Success:**

```
[
    <plan object>
]
```
---
## Post Plan ##

**POST** [ /plans]()

**Header**

```
    - x-access-token: <login token>
```

**Input format:**

```
{
    "PlanTypeId": 1,
    "price": 10,
    "CountryId": 1,
    "planDetails": [{
        "ProductCountryId": 1,
        "qty": 10,
        "deliverAt": "1||4"
    }],
    "planTranslate": [{
        "CounrtyId": 1,
        "name": "xxx",
        "description": "yyy",
        "sku": "sku"
    }],
    images: [<image file>],
    defaultImageIndex: 1
}
```


**Response Success:**

```
{
    "subscription": <subscription object>
}
```
---

## Post Subscription ##

**POST** [ /subscriptions]()

**Input format:**

```
{
	"planId": 1,
	"stripeToken": "tok_1AmICbIPTYF0fPcNSFWyGcj5",
    "deliveryAddressId": 1,
    "billingAddressId": 1,
    "orderDetail": [
        {
            "productCountryId": 3,
            "qty": 2
        },
        {
            "productCountryId": 2,
            "qty": 1
        }
    ],
    "email": "xxx@gmail.com",
    "phone": "xxxxx",
    "fullname": "thi nhuoc kim"
}
```


**Response Success:**

```
{
    "subscription": <subscription object>
}
```

---

##Get Promotions ##

**GET** [ /promotions]()

**Input format:**

**Response Success:**

```
[
    {
        "id": 1,
        "discount": 10,
        "bannerUrl": "/uploads/plans/plan-s3.jpg",
        "emailTemplateId": null,
        "emailTemplateOptions": null,
        "expiredAt": "2020-10-10",
        "Country": {
            "id": 1,
            "code": "MYS",
            "name": "Malaysia",
            "currency": "MYR",
            "defaultLang": "EN"
        },
        "promotionTranslate": [
            {
                "id": 1,
                "name": "thank you",
                "description": "discount for next purchasing",
                "langCode": "EN",
                "isDefault": false,
                "PromotionId": 1
            }
        ]
    }
]
```

---

## Check Promo Code ##

**POST** [ /promotions/check]()

**Input format:**

```
{
	"promotionCode": "codetest1",
	"productCountryIds": "[1, 2]"
}
```

**Response Success:**

```
{
    "id": 1,
    "discount": 10,
    "bannerUrl": "/uploads/plans/plan-s3.jpg",
    "emailTemplateId": null,
    "emailTemplateOptions": null,
    "expiredAt": "2020-10-10",
    "planIds": null,
    "productCountryIds": null,
    "promotionCodes": [
        {
            "id": 1,
            "name": "",
            "code": "codetest1",
            "isValid": true,
            "createdAt": "2017-08-08T06:46:43.000Z",
            "updatedAt": "2017-08-08T06:46:43.000Z",
            "PromotionId": 1
        }
    ]
}
```

---

## Get Countries ##

**GET** [ /countries]()

**Input format:**

**Response Success:**

```
{
    "id": 1,
    "code": "MYS",
    "name": "Malaysia",
    "currency": "MYR",
    "defaultLang": "EN",
    "isActive": true,
    "companyName": "Shaves2u Malaysia",
    "companyRegistrationNumber": "xxx",
    "companyAddress": "yyy",
    "taxRate": 5,
    "taxNumber": "12334",
    "email": "kimtn129@gmail.com",
    "phone": "+841659612968"
}
```

---

## Add Countries ##

**POST** [ /countries]()

**Input format:**

```
{
    "code": "MYS1",
    "name": "Malaysia1",
    "currency": "MYR1",
    "defaultLang": "EN",
    "isActive": true,
    "companyName": "Shaves2u Malaysia",
    "companyRegistrationNumber": "xxx",
    "companyAddress": "yyy",
    "taxRate": 5,
    "taxNumber": "12334",
    "email": "kimtn129@gmail.com",
    "phone": "+841659612968"
}
```

**Response Success:**

```
{
    "id": 12,
    "code": "MYS1",
    "name": "Malaysia1",
    "currency": "MYR1",
    "defaultLang": "EN",
    "isActive": true,
    "companyName": "Shaves2u Malaysia",
    "companyRegistrationNumber": "xxx",
    "companyAddress": "yyy",
    "taxRate": 5,
    "taxNumber": "12334",
    "email": "kimtn129@gmail.com",
    "phone": "+841659612968"
}
```

---

## Update Countries ##

**PUT** [ /countries]()

**Input format:**

```
{
    "code": "MYS1",
    "name": "Malaysia1",
    "currency": "MYR1",
    "defaultLang": "EN",
    "isActive": true,
    "companyName": "Shaves2u Malaysia",
    "companyRegistrationNumber": "xxx",
    "companyAddress": "yyy",
    "taxRate": 5,
    "taxNumber": "12334",
    "email": "kimtn129@gmail.com",
    "phone": "+841659612968"
}
```

**Response Success:**

```
{
    "id": 12,
    "code": "MYS1",
    "name": "Malaysia1",
    "currency": "MYR1",
    "defaultLang": "EN",
    "isActive": true,
    "companyName": "Shaves2u Malaysia",
    "companyRegistrationNumber": "xxx",
    "companyAddress": "yyy",
    "taxRate": 5,
    "taxNumber": "12334",
    "email": "kimtn129@gmail.com",
    "phone": "+841659612968"
}
```

---

## Admin Login ##

**POST** [ /admin/login]()

**Input format:**

```
{
	"email": "admin@gmail.com",
	"password": "admin123"
}
```

**Response Success:**

```
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImVtYWlsIjoiYWRtaW4zQGdtYWlsLmNvbSIsImlzU3VwZXJBZG1pbiI6ZmFsc2UsImlhdCI6MTUwMjI1NDU5MH0.uESi-4FLCr6jlf4YO82pBjgN35P2u3514xUQveLf_m4",
    "user": {
        "id": 11,
        "email": "admin3@gmail.com",
        "name": "Administrator",
        "staffId": "xxxxxxx",
        "isSuperAdmin": false,
        "phone": "+841659612968",
        "Country": {
            "id": 1,
            "code": "MYS",
            "name": "Malaysia",
            "currency": "MYR",
            "defaultLang": "EN",
            "isActive": true,
            "companyName": "Shaves2u Malaysia",
            "companyRegistrationNumber": "xxx",
            "companyAddress": "yyy",
            "taxRate": 5,
            "taxNumber": "12334",
            "email": "kimtn129@gmail.com",
            "phone": "+841659612968"
        }
    }
}
```

---

## Add Admin ##

**POST** [ /admin]()

**Input format:**

```
{
	"email": "admin1@gmail.com",
	"password": "admin123",
	"name": "Administrator",
    "staffId": "xxxxxxx",
    "CountryId": 1,
    "phone": "+841659612968",
    "isSuperAdmin": false
}
```

**Response Success:**

```
{
    "id": 5,
    "email": "admin1@gmail.com",
    "name": "Administrator",
    "staffId": "xxxxxxx",
    "phone": "+841659612968",
    "isSuperAdmin": false
}
```

---

## Update Admin Info ##

**PUT** [ /admin/:id]()

**Input format:**

```
{
	"email": "admin@gmail.com",
	"name": "Administrator",
    "staffId": "xxxxxxx",
    "phone": "+841659612968",
}
```

**Response Success:**

```
{
    "id": 5,
    "email": "admin1@gmail.com",
    "name": "Administrator",
    "staffId": "xxxxxxx",
    "isSuperAdmin": false,
    "phone": "+841659612968"
}
```

---

## Change Admin Password ##

**POST** [/admin/:id/password]()

**Input format:**

```
{
	"oldPassword": "admin1234",
	"newPassword": "admin123"
}
```

**Response Success:**

```
{
    "ok": true
}
```

---