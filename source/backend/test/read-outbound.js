import WareHouseHelper from '../src/helpers/warehouse-helper';
import config from '../src/config';
import initializeDb from '../src/db';
import AppContext from '../src/services/app-context';

// init app-context
global.appContext = new AppContext();

console.log('start reading outbound files');
initializeDb()
.then(db => {
  WareHouseHelper.readOrderConfirmation('WMSSHP_20171129145136000_SHP.txt', db);
});

// ftpHelper.putFile(`${config.warehouse.uploadFolder}/WMSORD_MYS_20171025184858.txt`, `${config.warehouse.ftpResultFolder}/WMSORD_MYS_20171025184858.txt`)
// //   .then(() => ftpHelper.getFile(`${config.warehouse.ftpResultFolder}/WMSSOH_20140629003004.txt`, `${config.warehouse.localDownloadFolder}/WMSSOH_20140629003004.txt`))
//   // .then(() => ftpHelper.deleteFile(`${config.warehouse.ftpResultFolder}/WMSORD_MYS_20171025184858.txt`))
//   .then(() => console.log('finish upload file'))
//   .catch(err => console.log(`asdasd == ${err}`));


// ftpHelper.listFiles(config.warehouse.ftpResultFolder)
//   .then(() => console.log('finish upload file'))
//   .catch(err => console.log(`asdasd == ${err}`));


// import PromiseFtp from 'promise-ftp';

// let ftp = new PromiseFtp();
// ftp.connect({
//   host: config.keyWarehouse.ftp.host,
//   user: config.keyWarehouse.ftp.username,
//   password: config.keyWarehouse.ftp.password})
// .then(serverMessage => {
//   console.log(`Server message: serverMessage ${serverMessage}`);
//   return ftp.list('/shaves01-my-sftp-test/inbox/');
// }).then(list => {
//   console.log(`Directory listing: ${JSON.stringify(list)}`);
//   console.dir(list);
//   return ftp.end();
// });
