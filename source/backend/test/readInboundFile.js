import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import json2csv from 'json2csv';
import moment from 'moment';

import _ from 'underscore';

import AppContext from '../src/services/app-context';
import initializeDb from '../src/db';
import OrderHelper from '../src/helpers/order-helper';

// init app-context
global.appContext = new AppContext();
const UPLOAD_PATH = `${__dirname}/../src/warehouse-upload-files/`;
const OUTPUT_PATH = `${__dirname}/../src/warehouse-output-files/`;

let fileCache = {};
function _readFileAsync(fileName, dir) {
  let dirName = dir ? dir : `${__dirname}/../src/config/warehouse-interface/`;
  return new Promise((resolve, reject) => {
    if(fileCache.fileName) {
      resolve(fileCache.fileName);
    } else {
      fs.readFile(path.resolve(dirName, fileName), (err, data) => {
        if(err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    }
  });
}

function _readData(options) {
  let returnObj = {};
  return _readFileAsync(options.fileName)
    .then(data => {
      let dataArray = data.toString().split(/\r?\n/);
      dataArray.forEach(object => {
        let keys = object.split(',');
        returnObj[keys[1]] = options.values.substr(keys[5] - 1, keys[4]).trim();
      });
      return returnObj;
    });
}

function readUploadFile(fileName) {
  // read outbound file
  let outBoundData = {detailsStrs: [], headerStr: {}, bodyStrs: [], fileName};
  return _readFileAsync(fileName, UPLOAD_PATH)
    .then(data => {
      let dataArray = data.toString().split(/\r?\n/);
      dataArray.forEach((obj, idx) => {
        if(idx === 0) {
          outBoundData.fileHeaderStr = obj;
        } else if(obj.substr(0, 5) === 'ORDHD') {
          outBoundData.headerStr = obj;
        } else if(obj.substr(0, 5) === 'ORDDT') {
          outBoundData.bodyStrs.push(obj);
        }
      });
    })
    .then(() => _readData({fileName: 'order-inbound/file-header.csv', values: outBoundData.fileHeaderStr}))
    .then(fileHeaderData => {
      outBoundData.fileHeaderData = fileHeaderData;
      return _readData({fileName: 'order-inbound/header.csv', values: outBoundData.headerStr});
    })
    .then(headersData => {
      outBoundData.headersData = headersData;
      return Promise.mapSeries(outBoundData.bodyStrs, bodyStr => _readData({fileName: 'order-inbound/details.csv', values: bodyStr}));
    })
    .then(bodyData => {
      // find order match with the outbound file
      outBoundData.bodyData = bodyData;
      return outBoundData;
    });
}

function readOrderConfirmation(fileName) {
  // read outbound file
  let outBoundData = {detailsStrs: [], headerStrs: [], fileName};
  let returnValue = [];
  return _readFileAsync(fileName, OUTPUT_PATH)
    .then(data => {
      let dataArray = data.toString().split(/\r?\n/);
      dataArray.forEach((obj, idx) => {
        if(idx === 0) {
          outBoundData.fileHeaderStr = obj;
        } else if(obj.substr(0, 5) === 'SHPHD') {
          outBoundData.headerStrs.push(obj);
        }
      });
    })
    .then(() => _readData({fileName: 'order-confirmation-outbound/file-header.csv', values: outBoundData.fileHeaderStr}))
    .then(fileHeaderData => {
      outBoundData.fileHeaderData = fileHeaderData;
      return Promise.mapSeries(outBoundData.headerStrs, headerStr => _readData({fileName: 'order-confirmation-outbound/header.csv', values: headerStr}));
    })
    .then(headersData => {
      outBoundData.headersData = headersData;
      outBoundData.headersData.forEach(headerData => {
        returnValue.push({
          fileName,
          orderNo: headerData.ExternOrderKey
        });
      });
      return returnValue;
    });
}


function readOrderOutBoundFile() {
  return new Promise((resolve, reject) => {
    fs.readdir(OUTPUT_PATH, (err, files) => {
      if(err) {
        reject(err)
      } else {
        Promise.mapSeries(files, file => {
          if(file.indexOf('WMSSHP') > -1) {
            return readOrderConfirmation(file);
          }
        })
          .then(result => resolve(_.flatten(result)))
          .catch(err => reject(err));
      }
    });
  });
}

console.log('start reading outbound files');

initializeDb()
  .then(db => {
    fs.readdir(UPLOAD_PATH, (err, files) => {
      if(err) {
        console.log(`Error: ${err}`);
      } else {
        let csvData = [];
        readOrderOutBoundFile()
          .then(outputOrders => Promise.mapSeries(files, file => {
            console.log(`filename: ${file}`);
            let inputData;
            if(file.indexOf('WMSORD') > -1) {
              return readUploadFile(file)
                .then(data => {
                  inputData = data;
                  let orderNo;
                  if(OrderHelper.checkOrderNumberType(data.headersData.ExternOrderKey) === 'order') {
                    orderNo = OrderHelper.getOrderNumber(data.headersData.ExternOrderKey);
                    return db.Order.findOne({
                      where: {
                        id: orderNo,
                        states: 'Processing'
                      },
                      include: ['Country']
                    });
                  } else {
                    orderNo = OrderHelper.getBulkOrderNumber(data.headersData.ExternOrderKey);
                    return db.BulkOrder.findOne({
                      where: {
                        id: orderNo,
                        states: 'Processing'
                      },
                      include: ['Country']
                    });
                  }
                })
                .then(order => {
                  if(order) {
                    let OrderId;
                    let BulkOrderId;
    
                    let orderOutboundName = _.findWhere(outputOrders, {orderNo: inputData.headersData.ExternOrderKey});
                    if(OrderHelper.checkOrderNumberType(inputData.headersData.ExternOrderKey) === 'order') {
                      OrderId = OrderHelper.getOrderNumber(inputData.headersData.ExternOrderKey);
                    } else {
                      BulkOrderId = OrderHelper.getBulkOrderNumber(inputData.headersData.ExternOrderKey);
                    }

                    let createdAt = moment(inputData.headersData.OrderDate, 'YYYYMMDDhhmmss');
    
                    // remove old file
                    csvData = _.reject(csvData, {orderNumber: inputData.headersData.ExternOrderKey});

                    inputData.bodyData.forEach(body => {
                      csvData.push({
                        doFileName: inputData.fileName,
                        wareHouseFileName: orderOutboundName ? orderOutboundName.fileName : '',
                        orderNumber: inputData.headersData.ExternOrderKey,
                        orderId: OrderId,
                        bulkOrderId: BulkOrderId,
                        sku: body.Sku,
                        qty: body.OriginalQty,
                        deliveryId: order ? order.deliveryId : '',
                        carrierAgent: order ? order.carrierAgent : '',
                        status: order ? order.states : '',
                        region: order ? order.Country.code : '',
                        createdAt: createdAt.isValid() ? createdAt : '2017-11-14',
                        updatedAt: createdAt.isValid() ? createdAt : '2017-11-14',
                      });
                    });
                  }
                });
            }
          })
            .then(() => {
              // let fields;
              // let fieldNames;
              // // fields = ['fileName', 'orderDate', 'orderNo', 'skus'];
              // // fieldNames = ['fileName', 'orderDate', 'orderNo', 'skus'];
              // fields = ['fileName', 'orderDate', 'orderNo', 'sku', 'qty', 'orderStatus', 'trackingNumber', 'carrierAgent', 'OrderId', 'BulkOrderId', 'wareHouseFileName'];
              // fieldNames = ['fileName', 'orderDate', 'orderNo', 'sku', 'qty', 'orderStatus', 'trackingNumber', 'carrierAgent', 'OrderId', 'BulkOrderId', 'wareHouseFileName'];
              // let csvFile = json2csv({
              //   data: csvData,
              //   fields,
              //   fieldNames
              // });
      
              // fs.writeFile('file.csv', csvFile, function(err) {
              //   if(err) throw err;
              //   console.log('file saved');
              // });
              console.log(`csvData == ${JSON.stringify(csvData)}`);
              return db.OrderUpload.bulkCreate(csvData);
            }))
            .then(() => console.log('Finished!'));
      }
    });
  });

// ftpHelper.putFile(`${config.warehouse.uploadFolder}/WMSORD_MYS_20171025184858.txt`, `${config.warehouse.ftpResultFolder}/WMSORD_MYS_20171025184858.txt`)
// //   .then(() => ftpHelper.getFile(`${config.warehouse.ftpResultFolder}/WMSSOH_20140629003004.txt`, `${config.warehouse.localDownloadFolder}/WMSSOH_20140629003004.txt`))
//   // .then(() => ftpHelper.deleteFile(`${config.warehouse.ftpResultFolder}/WMSORD_MYS_20171025184858.txt`))
//   .then(() => console.log('finish upload file'))
//   .catch(err => console.log(`asdasd == ${err}`));


// ftpHelper.listFiles(config.warehouse.ftpResultFolder)
//   .then(() => console.log('finish upload file'))
//   .catch(err => console.log(`asdasd == ${err}`));


// import PromiseFtp from 'promise-ftp';

// let ftp = new PromiseFtp();
// ftp.connect({
//   host: config.keyWarehouse.ftp.host,
//   user: config.keyWarehouse.ftp.username,
//   password: config.keyWarehouse.ftp.password})
// .then(serverMessage => {
//   console.log(`Server message: serverMessage ${serverMessage}`);
//   return ftp.list('/shaves01-my-sftp-test/inbox/');
// }).then(list => {
//   console.log(`Directory listing: ${JSON.stringify(list)}`);
//   console.dir(list);
//   return ftp.end();
// });
