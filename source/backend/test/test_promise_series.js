var Promise = require('bluebird');

var asyncFunction = function(time) {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      console.log('Runiing --', time);
      resolve(time);
    }, time);
  });
}

Promise.mapSeries([400, 200, 700, 500], function(time, index) {
  console.log('index =', index);
  asyncFunction(time);
  return time;
})
  .then(function(results) {
    console.log('Result --', results);
  });
