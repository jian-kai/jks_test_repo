import Promise from 'bluebird';
import MailChimp from '../src/helpers/mailchimp';
import initializeDb from '../src/db';
import SequelizeHelper from '../src/helpers/sequelize-helper';
import { emailHelper } from '../src/helpers/email-helper';

let mailChimp = new MailChimp();
let campaignId;
const nodemailer = require('nodemailer');


console.log('start reading outbound files');
initializeDb()
  .then(db => {
    // get user cart
    setTimeout(() => {
      db.Order.findAll({where: {
        $or: [
          {states: 'Completed'},
          {states: 'Delivering'}
        ],
        $and: [
          {updatedAt: {$lt: '2017-11-29 00:00:00'}},
          {updatedAt: {$gt: '2017-11-24 00:00:00'}}
        ]
      }})
      .then(orders => Promise.mapSeries(orders, order => emailHelper.sendReceiptsEmail({orderId: order.id}, db)))
      .catch(err => console.log(err));
      // emailHelper.sendPaymentFail({userId: 1}, db)
      //   .catch(err => console.log(err));
      // emailHelper.sendRenewalConfirm({email: 'kimtn129@gmail.com', firstName: 'Kim'})
      //   .catch(err => console.log(err));
      // emailHelper.sendAutoRenewalError({subscriptionId: 13}, db)
      //   .catch(err => console.log(err));
      // emailHelper.sendReceiptsEmail({orderId: 4246}, db)
      //   .catch(err => console.log(err));
      // emailHelper.sendActiveUserSample({email: 'kimtn129@gmail.com', fullName: 'Kim thi'})
      //   .catch(err => console.log(err));
      // db.User.findOne({
      //   where: { id: 1 },
      //   include: [
      //     {
      //       model: db.Cart,
      //       include: [
      //         {
      //           model: db.CartDetail,
      //           as: 'cartDetails',
      //           include: [
      //             {
      //               model: db.ProductCountry,
      //               include: [
      //                 'Country',
      //                 {
      //                   model: db.Product,
      //                   include: ['productTranslate', 'productImages']
      //                 }
      //               ]
      //             },
      //             {
      //               model: db.PlanCountry,
      //               include: [
      //                 'Country',
      //                 {
      //                   model: db.Plan,
      //                   include: ['PlanType', 'planTranslate', 'planImages']
      //                 }
      //               ]
      //             }
      //           ]
      //         }
      //       ]
      //     }
      //   ]
      // })
      //   .then(user => {
      //     user = SequelizeHelper.convertSeque2Json(user);
      //     return emailHelper.sendPendingCart(user);
      //   })
      //   .catch(err => console.log(err));
    }, 2000);
  });


// get all member in single list
// mailChimp.getMembers('2244989605')
//   .then(result => {
//     console.log(`result ==== ${JSON.stringify(result)}`);
//     if(result && result.members) {
//       return Promise.map(result.members, member => mailChimp.deleteMembers('2244989605', member.id));
//     } else {
//       return;
//     }
//   })
//   .then(() => mailChimp.createMembers('2244989605', {'email_address': 'kimtn129@gmail.com', 'status': 'subscribed'}))
//   .then(subscriber => {
//     console.log(`subscriber ==== ${JSON.stringify(subscriber)}`);
//     // update template 
//     return mailChimp.createCampaign({
//       listId: '2244989605',
//       subject: '[smartshave] Active email',
//       fromName: 'Smart Shave'
//     });
//   })
//   .then(campaign => {
//     console.log(`campaign == ${JSON.stringify(campaign)}`);
//     campaignId = campaign.id;
//     return mailChimp.putCampaignTemplate(campaignId, 24521, {
//       username: 'Kim Thi',
//       link: '<a href='http://google.com' target='_blank'>google</a>'
//     });
//   })
//   .then(result => {
//     console.log(`result 11 == ${JSON.stringify(result)}`);
//     return mailChimp.sendCampaign(campaignId);
//   })
//   .then(result => {
//     console.log(`Finished = ${JSON.stringify(result)}`);
//   })
//   .catch(err => {
//     console.log(`err ==== ${err}`);
//   });
// send campaign


// let mandrillClient = new mandrill.Mandrill('h9DNErihBBsaqvdtjdvyaA');
// var message = {
//   'html': '<p>Example HTML content</p>',
//   'text': 'Example text content',
//   'subject': 'example subject',
//   'from_email': 'smartshave@bjdev.net',
//   'from_name': 'Example Name',
//   'to': [{
//     'email': 'kimtn129@gmail.com',
//     'name': 'Thi Nhuoc Kim',
//     'type': 'to'
//   }],
//   'headers': {
//     'Reply-To': 'smartshave@bjdev.net'
//   },
//   'important': false,
//   'track_opens': true,
//   'track_clicks': true,
//   'auto_text': null,
//   'auto_html': null,
//   'inline_css': true,
//   'url_strip_qs': null,
//   'preserve_recipients': null,
//   'view_content_link': null,
//   'tracking_domain': null,
//   'signing_domain': null,
//   'return_path_domain': null,
//   'merge': true,
//   // 'merge_language': 'mailchimp',
//   // 'global_merge_vars': [{
//   //   'name': 'merge1',
//   //   'content': 'merge1 content'
//   // }],
//   // 'merge_vars': [{
//   //   'rcpt': 'recipient.email@example.com',
//   //   'vars': [{
//   //     'name': 'merge2',
//   //     'content': 'merge2 content'
//   //   }]
//   // }],
//   // 'tags': [
//   //   'password-resets'
//   // ],
//   // 'subaccount': 'customer-123',
//   // 'google_analytics_domains': [
//   //   'example.com'
//   // ],
//   // 'google_analytics_campaign': 'message.from_email@example.com',
//   // 'metadata': {
//   //   'website': 'www.example.com'
//   // },
//   // 'recipient_metadata': [{
//   //   'rcpt': 'recipient.email@example.com',
//   //   'values': {
//   //     'user_id': 123456
//   //   }
//   // }],
//   // 'attachments': [{
//   //   'type': 'text/plain',
//   //   'name': 'myfile.txt',
//   //   'content': 'ZXhhbXBsZSBmaWxl'
//   // }],
//   // 'images': [{
//   //   'type': 'image/png',
//   //   'name': 'IMAGECID',
//   //   'content': 'ZXhhbXBsZSBmaWxl'
//   // }]
// };
// var async = false;
// var sendAt = new Date();
// mandrillClient.messages.send({ message, async, 'send_at': sendAt }, result => {
//   console.log(`result ====== ${JSON.stringify(result)}`);
//   /*
//   [{
//           'email': 'recipient.email@example.com',
//           'status': 'sent',
//           'reject_reason': 'hard-bounce',
//           '_id': 'abc123abc123abc123abc123abc123'
//       }]
//   */
// }, e => {
//   // Mandrill returns the error as an object with name and message keys
//   console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
//   // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
// });



// // create reusable transporter object using the default SMTP transport
// let transporter = nodemailer.createTransport({
//   host: 'smtp.mandrillapp.com',
//   port: 587,
//   secure: false, // true for 465, false for other ports
//   auth: {
//     user: 'Smartshave', // generated ethereal user
//     pass: 'h9DNErihBBsaqvdtjdvyaA'  // generated ethereal password
//   }
// });

// // setup email data with unicode symbols
// let mailOptions = {
//   from: 'Smart Shaves<kim@bjdev.net>', // sender address
//   to: 'kimtn129@gmail.com', // list of receivers
//   subject: 'Hello ', // Subject line
//   text: 'Hello world?', // plain text body
//   html: '<b>Hello world?</b>' // html body
// };

// // send mail with defined transport object
// transporter.sendMail(mailOptions, (error, info) => {
//   if(error) {
//     return console.log(error);
//   }
//   console.log('Message sent: %s', info.messageId);
//   // Preview only available when sending through an Ethereal account
//   console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
//   // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
// });
