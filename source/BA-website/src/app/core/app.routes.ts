import { LoggedInGuard } from './guards/logged-in.guard';
import { AuthService } from '../auth/services/auth.service';
import { LoginComponent } from '../auth/components/login/login.component';
import { HomeComponent } from '../programs/components/home/home.component';
import { AlacarteListComponent } from '../programs/components/alacarte-list/alacarte-list.component';
import { SubscriptionListComponent } from '../programs/components/subscription-list/subscription-list.component';
import { SubscriptionGroupComponent } from '../programs/components/subscription-group/subscription-group.component';
import { SampleProductComponent } from '../programs/components/sample-product/sample-product.component';

//checkout
import { ReviewComponent } from '../programs/components/checkout/review/review.component';
import { PaymentReceiveComponent } from '../programs/components/checkout/payment-receive/payment-receive.component';
import { RoadshowDetailComponent } from '../programs/components/checkout/roadshow-detail/roadshow-detail.component';
import { SummaryComponent } from '../programs/components/checkout/summary/summary.component';

// order
import { OrderListComponent } from '../programs/components/order/order-list/order-list.component';
import { OrderDetailComponent } from '../programs/components/order/order-detail/order-detail.component';


export const routes = [
  { path: '', component: HomeComponent, canActivate: [AuthService] },
  { path: 'login', component: LoginComponent },
  { path: 'alacartes', component: AlacarteListComponent, canActivate: [AuthService] },
  { path: 'subscriptions/blade-type/:groupName', component: SubscriptionListComponent, canActivate: [AuthService] },
  { path: 'blade-type', component: SubscriptionGroupComponent, canActivate: [AuthService] },
  { path: 'sample-product', component: SampleProductComponent, canActivate: [AuthService] },
  { path: 'checkout/review/:type', component: ReviewComponent },
  { path: 'checkout/payment/:type', component: PaymentReceiveComponent },
  { path: 'checkout/payment/:type/roadshow', component: RoadshowDetailComponent },
  { path: 'checkout/summary/:type/:orderId', component: SummaryComponent },
  { path: 'orders', component: OrderListComponent },
  { path: 'orders/:countryCode/:id', component: OrderDetailComponent },
  
  // otherwise redirect to home
  { path: '**', redirectTo: '' }

];
