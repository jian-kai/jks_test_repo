import { Injectable, EventEmitter, Output } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class RouterService {
  @Output() routerObj: EventEmitter<Object> = new EventEmitter();
  @Output() limitChange: EventEmitter<Object> = new EventEmitter();

  constructor(private router: Router,
    private location: Location) { }

  appendRouterParams(options) {
    // get current params
    let root = this.location.path(false).split('?')[0];
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    Object.keys(options).forEach(key => {
      params.set(key, options[key]);
    })
    // change route
    let queryParams = {};
    params.paramsMap.forEach((value, key) => {
      queryParams[key] = value
    });
    this.router.navigate([root], { queryParams });
  }

  setRouterObj(_obj: any) {
    this.routerObj.emit(_obj);
  }

  /**
   * emit event when user change the limit
   */
  onLimitChange(event) {
    this.limitChange.emit(event);
  }
}
