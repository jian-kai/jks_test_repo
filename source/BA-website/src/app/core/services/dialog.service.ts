import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogConfirmComponent } from '../directives/dialog-confirm/dialog-confirm.component';
import { DialogResultComponent } from '../directives/dialog-result/dialog-result.component';
import { DialogErrorComponent } from '../directives/dialog-error/dialog-error.component';

@Injectable()
export class DialogService {

  constructor(public dialog: MatDialog) { }
  confirm(_content ?: string) {
    return this.dialog.open(DialogConfirmComponent, {
      data: {
        content: _content
      }
    });
  }
  result(_content ?: string) {
    return this.dialog.open(DialogResultComponent, {
      data: {
        content: _content
      }
    });
  }
  error(_content ?: string) {
    return this.dialog.open(DialogErrorComponent, {
      data: {
        content: _content
      }
    });
  }
}
