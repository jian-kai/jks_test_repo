import { Injectable } from '@angular/core';

import { StorageService } from './storage.service';
import { Headers } from '@angular/http';

@Injectable()
export class RequestService {
  constructor(private storage: StorageService) {}

  getAuthHeaders(contentType?: string) {
    let _headers = this.getHeaders(contentType);
    let authToken = this.storage.getAuthToken();
    // let country = this.storage.getCountry();
    // let countryCode = this.storage.getCountryCode();

    _headers.append('x-access-token', authToken);
    // _headers.append('country-code', country.code);
    return {headers: _headers};
  }

  getHeaders(contentType: string) {
    let country = this.storage.getCountry();
    let headers = new Headers();
    let _ctype = "";
    switch (contentType) {
      case "json":
        _ctype = "application/json";
        break;
      case "form-data":
        _ctype = "";
        break;
      case "multipart":
        _ctype = "multipart/form-data";
        break;
      default:
        _ctype = "application/json";
        break;
    }
    if (_ctype) {
      headers.append('Content-Type', _ctype);
    }

    headers.append('country-code', country.code);
    // set basic authentication
    headers.append('authorization', `basic ${btoa('shaves2u:shaves2u')}`);
    return headers;
  }

}
