import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PagerService {
  public getPager = new BehaviorSubject<number>(1);
  constructor() { }

  public setPage(page: number) {
    this.getPager.next(page);
  }
}
