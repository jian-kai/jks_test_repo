import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router} from  '@angular/router';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {RequestService} from 'app/core/services/request.service';

@Injectable()
export class HttpService {
    constructor(private _http: Http,
        private route: Router,
        private requestService: RequestService
    ) {

    }

    _getList(apiUrl: string) : Observable<any> {
        return this._http.get(apiUrl, this.requestService.getAuthHeaders())
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _getDetail(apiUrl: string) : Observable<any> {
        return this._http.get(apiUrl, this.requestService.getAuthHeaders())
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _create(apiUrl: string, _data: any): Observable<any> {
        return this._http.post(apiUrl, _data, this.requestService.getAuthHeaders())
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _post(apiUrl: string, _data: any) {
        return this._http.post(apiUrl, _data, this.requestService.getAuthHeaders())
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _createWithFormData(apiUrl: string, _data: any, form_data?:string): Observable<any> {
        return this._http.post(apiUrl, _data, this.requestService.getAuthHeaders(form_data))
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _update(apiUrl, _data, form_data?:string): Observable<any> {
        return this._http.put(apiUrl, _data, this.requestService.getAuthHeaders(form_data))
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    _delete(apiUrl: string) : Observable<any> {
        return this._http.delete(apiUrl, this.requestService.getAuthHeaders())
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    private extractData(res: Response) {
	    let body = res.json();
        return body || {};
    }
    private handleErrorObservable (error: Response | any) {
        console.log(`handleErrorObservable error ${error}`)
        if (error && error.status === 401) {
            return Observable.throw(error);
        }
        if (error._body)
            return Observable.throw(error._body);
        else
            return Observable.throw(error);
    }
    
}