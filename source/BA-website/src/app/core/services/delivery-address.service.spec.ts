import { TestBed, inject } from '@angular/core/testing';

import { DeliveryAddressService } from './delivery-address.service';

describe('DeliveryAddressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliveryAddressService]
    });
  });

  it('should be created', inject([DeliveryAddressService], (service: DeliveryAddressService) => {
    expect(service).toBeTruthy();
  }));
});
