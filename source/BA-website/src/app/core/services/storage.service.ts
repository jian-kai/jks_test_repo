const STORAGE_KEY = 'ba_auth_token';
const CURRENT_USER = 'ba_current_user';
const CART_ITEMS = 'cart_items';
const FLAG_SYNC_CART = 'flag_sync_cart';
const PLAN_CART = 'plan_cart';
const CURRENT_COUNTRY = 'current_country';
const COUNTRIES = 'countries';
const CURRENT_LANGUAGE = 'current_language';
const DELIVERY_ADDRESS_BY_GUEST= 'delivery_address_by_guest';
const CARD_BY_GUEST = 'card_by_guest';
const PROMO_CODE = 'ba_promo_code';
const CURRENT_LANGUAGE_CODE = 'current_language_code';
const CUSTOMER_DETAILS = 'customer_details';
const STRIPE_DETAILS = 'stripe_details';
const LOGIN_TIMER = 'login_timer';
const DEFAULT_COUNTRY = {
  code: 'MYS',
  name: 'Malaysia',
  currencyDisplay: 'RM',
  taxName: 'GST',
  taxRate: 0,
  includedTaxToProduct: 1,
  callingCode: '60',
  isDefault: true
}
const DEFAULT_LANGUAGE = {
  key: 'en',
  value: 'Malaysia',
  countryCode: 'MYS',
  isDefault: true
}
const GROUP_SELECTED_ID = 'group_selected_id';
const errorMessage = 'errorMessage';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class StorageService {
  getAuthToken() {
    return localStorage.getItem(STORAGE_KEY);
  }

  setAuthToken(token) {
    localStorage.setItem(STORAGE_KEY, token);
  }

  removeAuthToken() {
    localStorage.removeItem(STORAGE_KEY);
  }

  getLoginTimer() {
    return new Date(localStorage.getItem(LOGIN_TIMER));
  }
  setLoginTimer(timer) {
    localStorage.setItem(LOGIN_TIMER, timer);
  }
  removeLoginTimer() {
    localStorage.removeItem(LOGIN_TIMER);
  }

  // set current User localstorage
  setCurrentUser(data) {
    localStorage.setItem(CURRENT_USER, JSON.stringify(data));
  }
  // remove current User localstorage
  removeCurrentUser() {
    localStorage.removeItem(CURRENT_USER);
  }
  // get localstorage item
  getCurrentUserItem(): any {
    return JSON.parse(localStorage.getItem(CURRENT_USER));
  }

  // reset defaultShipping | defaultBilling
  resetDefaultShipping(_value): any {
    let user = this.getCurrentUserItem();
    user.defaultShipping = _value;
    this.setCurrentUser(user);
  }

  // reset defaultBilling
  resetDefaultBilling(_value): any {
    let user = this.getCurrentUserItem();
    user.defaultBilling = _value;
    this.setCurrentUser(user);
  }

  // set cart item localstorage
  setCartItem(item) {
    let country = this.getCountry();
    let _item = this.getCartItem();
    if (!_item) {
      _item = {};
    }
    _item[country.code] = item;
    
    localStorage.setItem(CART_ITEMS, JSON.stringify(_item));
  }
  // get cart item localstorage
  getCartItem(): any {
    return JSON.parse(localStorage.getItem(CART_ITEMS));
  }

  // clear cart
  clearCart(removeAll ?: any) {
    if (removeAll) {// remove cart items of all countries
      localStorage.removeItem(CART_ITEMS);
    } else { // remove cart items of current country
      let country = this.getCountry();
      let getCartItems = JSON.parse(localStorage.getItem(CART_ITEMS));
      if (getCartItems && getCartItems[country.code]) {
        delete getCartItems[country.code];
      }
      localStorage.setItem(CART_ITEMS, JSON.stringify(getCartItems));
    }
  }

  // set plan item
  setPlanCart(item) {
    this.clearPlan();
    localStorage.setItem(PLAN_CART, JSON.stringify(item));
  }

  // get plan item 
  getPlanCart(): any {
    return JSON.parse(localStorage.getItem(PLAN_CART));
  }

  // get plan cart items
  getPlanCartItems() {
    let planCart = this.getPlanCart();
    return planCart.carts ? planCart.carts : [];
  }

  // set cart item localstorage
  setPlanCartItem(items) {
    let planCart = this.getPlanCart();
    planCart.carts = items;
    localStorage.setItem(PLAN_CART, JSON.stringify(planCart));
  }

  // clear plan item
  clearPlan() {
    localStorage.removeItem(PLAN_CART);
  }

  // set country code
  setCountry(country) {
    localStorage.setItem(CURRENT_COUNTRY, JSON.stringify(country));
    this.setLanguageCode(country.defaultLang);
  }

  // get country code
  getCountry() {
    if (localStorage.getItem(CURRENT_COUNTRY)) {
      return JSON.parse(localStorage.getItem(CURRENT_COUNTRY));
    } else {
      return DEFAULT_COUNTRY;
    }
  }

  // set contries
  setCountries(countries) {
    localStorage.setItem(COUNTRIES, JSON.stringify(countries));
  }

  // get country code
  getCountries() {
    if (localStorage.getItem(COUNTRIES)) {
      return JSON.parse(localStorage.getItem(COUNTRIES));
    } else {
      return null;
    }
  }

  // set language
  setLanguage(language) {
    localStorage.setItem(CURRENT_LANGUAGE, JSON.stringify(language));
  }

  // set flag for country when the cart has been synced
  setFlagSyncCart() {
    let country = this.getCountry();
    let flagSync = {};
    if (localStorage.getItem(FLAG_SYNC_CART)) {
      flagSync = JSON.parse(localStorage.getItem(FLAG_SYNC_CART));
    }
    flagSync[country.code] = true;

    localStorage.setItem(FLAG_SYNC_CART, JSON.stringify(flagSync));
  }

  // check flag sync cart
  checkFlagSyncCart() {
    let country = this.getCountry();
    let flagSync = JSON.parse(localStorage.getItem(FLAG_SYNC_CART));
    if (flagSync && flagSync[country.code]) {
      return true;
    } else {
      return false;
    }
  }

  // clear flag sync cart
  clearFlagSyncCart() {
    localStorage.removeItem(FLAG_SYNC_CART);
  }

  // get language
  getLanguage() {
    if (localStorage.getItem(CURRENT_LANGUAGE)) {
      return JSON.parse(localStorage.getItem(CURRENT_LANGUAGE));
    } else {

      return DEFAULT_LANGUAGE;
    }
  }

  // set delivery address by guest
  setDeliveryAddressByGuest(_address) {
    let addressStorage = localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST);
    let arrTmp = [];
    let has = false;
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      _tmp.forEach(address => {
          if (address.id === _address.id) {
            arrTmp.push(_address);
            has = true;
          } else {
            arrTmp.push(address);
          }
      });
      if (!has) {
        arrTmp.push(_address);
      }
    } else {
      arrTmp.push(_address);
    }
    localStorage.setItem(DELIVERY_ADDRESS_BY_GUEST, JSON.stringify(arrTmp));
  }

  // get delivery address by guest
   getDeliveryAddressByGuest() {
    return JSON.parse(localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST));
   }

   // remove delivery address by guest
   removeDeliveryAddressByGuest(_address) {
     let addressStorage = localStorage.getItem(DELIVERY_ADDRESS_BY_GUEST);
     if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      let _tmp2 = _tmp.filter(address => address.id !== _address.id);
      localStorage.setItem(DELIVERY_ADDRESS_BY_GUEST, JSON.stringify(_tmp2));
     }
   }

   clearDeliveryAddressByGuest() {
    localStorage.removeItem(DELIVERY_ADDRESS_BY_GUEST);
   }

   // set delivery address by guest
  setCardByGuest(_card) {
    let addressStorage = localStorage.getItem(CARD_BY_GUEST);
    let arrTmp = [];
    let has = false;
    if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      _tmp.forEach(card => {
          if (card.id === _card.id) {
            arrTmp.push(_card);
            has = true;
          } else {
            arrTmp.push(card);
          }
      });
      if (!has) {
        arrTmp.push(_card);
      }
    } else {
      arrTmp.push(_card);
    }
    localStorage.setItem(CARD_BY_GUEST, JSON.stringify(arrTmp));
  }

  // get delivery address by guest
   getCardByGuest() {
    return JSON.parse(localStorage.getItem(CARD_BY_GUEST));
   }

   // remove delivery address by guest
   removeCardByGuest(_card) {
     let addressStorage = localStorage.getItem(CARD_BY_GUEST);
     if (addressStorage) {
      let _tmp = JSON.parse(addressStorage);
      let _tmp2 = _tmp.filter(address => address.id !== _card.id);
      localStorage.setItem(CARD_BY_GUEST, JSON.stringify(_tmp2));
     }
   }

  clearCardByGuest() {
    localStorage.removeItem(CARD_BY_GUEST);
  }

   // set flag for country when the cart has been synced
  setPromoCode(value: string) {
    localStorage.setItem(PROMO_CODE, value);
  }

  // check flag sync cart
  getPromoCode() {
    return localStorage.getItem(PROMO_CODE) ? localStorage.getItem(PROMO_CODE) : '';
  }

  // clear flag sync cart
  clearPromoCode() {
    localStorage.removeItem(PROMO_CODE);
  }

  // set language
  setLanguageCode(language) {
    localStorage.setItem(CURRENT_LANGUAGE_CODE, JSON.stringify(language));
  }

  // get language
  getLanguageCode() {
    if (localStorage.getItem(CURRENT_LANGUAGE_CODE)) {
      return JSON.parse(localStorage.getItem(CURRENT_LANGUAGE_CODE));
    }
  }

  // set customer details
  setCustomerDetails(value) {
    localStorage.setItem(CUSTOMER_DETAILS, JSON.stringify(value));
  }

  // check customer details
  getCustomerDetails(): any {
    return JSON.parse(localStorage.getItem(CUSTOMER_DETAILS));
  }

  // clear customer details
  clearCustomerDetails() {
    localStorage.removeItem(CUSTOMER_DETAILS);
  }

  // set payment type
  setStripeDetail(value) {
    localStorage.setItem(STRIPE_DETAILS, JSON.stringify(value));
  }

  // check customer details
  getStripeDetail(): any {
    return JSON.parse(localStorage.getItem(STRIPE_DETAILS));
  }

  // clear customer details
  clearStripeDetail() {
    localStorage.removeItem(STRIPE_DETAILS);
  }

  // set group id
  setGroupId(value) {
    localStorage.setItem(GROUP_SELECTED_ID, value);
  }

  // check group id
  getGroupId(): any {
    return localStorage.getItem(GROUP_SELECTED_ID);
  }

  // clear group id
  clearGroupId() {
    localStorage.removeItem(GROUP_SELECTED_ID);
  }
   
  // set group id
  setStripeError(value) {
    localStorage.setItem(errorMessage, value);
  }
  
  // check group id
  getStripeError(): any {
    return localStorage.getItem(errorMessage);
  }
  
  // clear group id
  clearStripeError() {
    localStorage.removeItem(errorMessage);
  }
}
