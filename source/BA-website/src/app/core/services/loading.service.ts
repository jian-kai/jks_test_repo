import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
@Injectable()
export class LoadingService {
  public status = new BehaviorSubject<Boolean>(false);
  constructor() { }

  public display(value: Boolean) {
    this.status.next(value);
  }

}
