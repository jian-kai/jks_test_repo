import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { lengthValidator } from '../../core/validators/length.validator';

@Injectable()
export class DeliveryAddressService {
  @Output() deliveryAddressAction: EventEmitter<any> = new EventEmitter<any>();
  constructor(private builder: FormBuilder,) { }

  setDeliveryAddressAction(_data) {
    this.deliveryAddressAction.emit(_data);
  }

  // initialize Product Translate
  initAddress() {
    return this.builder.group({
      id: [undefined],
      // email: ['', Validators.required],
      // firstName: ['', Validators.required],
      // lastName: ['', Validators.required],
      // callingCode: [''],
      // contactNumber: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      portalCode: ['', [Validators.required, lengthValidator({min: 5})]],
    });
  }

  initAddressSGP() {
    return this.builder.group({
      id: [undefined],
      address: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      portalCode: ['', [Validators.required, lengthValidator({min: 6, max: 6})]],
    });
  }

  initContact() {
    return this.builder.group({
      email: ['', Validators.required],
    });
  }

}
