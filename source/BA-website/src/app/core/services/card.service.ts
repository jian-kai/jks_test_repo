import { Component, ElementRef, OnInit, Output, EventEmitter, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Injectable()
export class CardService {
  constructor(private builder: FormBuilder,) { }
  @Output() cardAction: EventEmitter<any> = new EventEmitter<any>();

  setCardAction(_data) {
    this.cardAction.emit(_data);
  }
  // initialize Product Translate
  initCard() {
    return this.builder.group({
      cardName: ['', Validators.required],
      cardNumber: ['', Validators.required],
      // expiryMonth: ['', Validators.required],
      // expiryYear: ['', Validators.required],
      expiry: ['', Validators.required],
      cvc: ['', Validators.required],
      cardType: ['']
    });
  }

}
