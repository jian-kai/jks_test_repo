import { Injectable, Output, EventEmitter } from '@angular/core';
import { Item } from '../../core/models/item';
import { StorageService } from '../../core/services/storage.service';
import { HttpService } from '../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';

@Injectable()
export class CartService {
  promotion: any = {};
  @Output() hasUpdate: EventEmitter<boolean> = new EventEmitter(true);
  @Output() justAddedItem: EventEmitter<Object> = new EventEmitter();
  public cart: Item[] = [];
  public cartRules: any;
  public stepDatas: boolean;
  constructor(private storage: StorageService,
    private httpService: HttpService,
    private appConfig: AppConfig
  ) {
    
    // console.log(``)
    // this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
    //   data => {
    //     this.cartRules = data;
    //   }
    // )
  }

  applyAddItemEvent() {
    this.justAddedItem.emit(true);
  }

  reloadSubscriptions() {
    let currentUser = this.storage.getCurrentUserItem();
    if (currentUser) {
      this.httpService._getList(`${this.appConfig.config.api.subscription}`.replace('[userID]', currentUser.id)).subscribe(
        data => {
          currentUser.subscriptions = data;
          this.storage.setCurrentUser(currentUser);
        },
        error => {
          console.log(error.error)
        }
      )
    }
  }

  // get shipping fee configuration
  getShippingFeeConfig() {

  }

  updateCart() {
    this.hasUpdate.emit(true);
    // return this.saveCartToDB()
    //   .then(() => console.log('succeed'))
    //   .catch(error => console.log(error));
  }

  // add product item to cart
  addItem(item: Item) {
    let isNull = false;
    this.cart = this.getCart();
    if (!this.cart || (this.cart && this.cart.length <= 0)) {
      this.cart = [];
      isNull = true;
    }
    //productImageURL
    if(item.product) {
      // process for product
      if (!item.product.productImageURL && item.product.productImages.length > 0) {
        item.product.productImageURL = '';
        item.product.productImages.forEach(element => {
          if (element.isDefault) {
            item.product.productImageURL = element.url;
          }
        });
        if (item.product.productImageURL === '') {
          item.product.productImageURL = item.product.productImages[0].url;
        }
      }
    }
    if(item.plan) {
      // process for subscription
      if (!item.plan.planImageURL && item.plan.planImages.length > 0) {
        item.plan.planImageURL = '';
        item.plan.planImages.forEach(element => {
          if (element.isDefault) {
            item.plan.planImageURL = element.url;
          }
        });
        if (item.plan.planImageURL === '') {
          item.plan.planImageURL = item.plan.planImages[0].url;
        }
      }
    }

    if (isNull) {
      this.cart.push(item);
      this.storage.setCartItem(this.cart);
      return;
    }
    this.cart = this.getCart();
    let isExists = false;
    this.cart.forEach( (cartItem, index ) => {
      if(cartItem.product && item.product) {
        if (cartItem.product.id === item.product.id) {
          isExists = true;
          if (typeof item.qty === 'string') {
            item.qty = parseInt(item.qty);
          }
          if (typeof cartItem.qty === 'string') {
            cartItem.qty = parseInt(cartItem.qty);
          }
          cartItem.qty += item.qty;
          this.cart[index] = cartItem;
        }
      }
      if(cartItem.plan && item.plan) {
        if (cartItem.plan.id === item.plan.id) {
          isExists = true;
          if (typeof item.qty === 'string') {
            item.qty = parseInt(item.qty);
          }
          if (typeof cartItem.qty === 'string') {
            cartItem.qty = parseInt(cartItem.qty);
          }
          cartItem.qty += item.qty;
          this.cart[index] = cartItem;
        }
      }
    });
    if (!isExists) {
      this.cart.push(item);
    }
    this.storage.setCartItem(this.cart);
  }

  //save cart to DB
  saveCartToDB() {
    return new Promise((resolve, reject) => {
      let curUser = this.storage.getCurrentUserItem();
      if (curUser) {
        this.cart = this.getCart();
        let _carts = {carts:[]};
        _carts.carts = [];
        if(this.cart) {
          this.cart.forEach(ele => {
            if(ele.product) {
              _carts.carts.push({ProductCountryId: ele.product.id, qty: ele.qty})
            }
            if(ele.plan) {
              _carts.carts.push({PlanCountryId: ele.plan.id, qty: ele.qty})
            }
          });
        }
        this.httpService._create(`${this.appConfig.config.api.carts}`.replace('[userID]', curUser.id), _carts)
        .subscribe(
          data => {
            resolve();
          },
          error => {
            reject(error);
          }
        )
      }
    });
  }

  // get cart item
  getCart(_type?: string): Item[] {
    let country = this.storage.getCountry();
    let cartItems = this.storage.getCartItem();
    let countryCart = [];
    if (cartItems && cartItems[country.code]) {
      countryCart = cartItems[country.code];
      if(_type && _type === 'alacartes') {
        countryCart = cartItems[country.code].filter(item => item.product);
      }
      if(_type && _type === 'subscriptions') {
        countryCart = cartItems[country.code].filter(item => item.plan);
      }
    }
    return countryCart;
  }

  //remove item
  removeItem(item: Item) {
    this.cart = this.getCart();
    let cartTemp: Item[] = [];

    if(item.product) {
      // this.cart = this.cart.filter(cartItem => cartItem.product.id !== item.product.id); 
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.product) {
          if(cartItem.product.id !== item.product.id) {
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    if(item.plan) {
      // this.cart = this.cart.filter(cartItem => cartItem.plan.id !== item.plan.id); 
      this.cart.forEach( (cartItem, index ) => {
        if(cartItem.plan) {
          if(cartItem.plan.id !== item.plan.id) {
            cartTemp.push(cartItem);
          }
        } else {
          cartTemp.push(cartItem);
        }
      });
    }
    this.cart = cartTemp;
    this.storage.setCartItem(this.cart);
    // return this.updateCart()
    //   .then(() => this.cart)
    //   .catch(error => console.log(error));
  }

  // refresh item
  refreshItem(_cartItems: any): Item[] {
    this.cart = this.getCart();
    let tmpItem: any;
    _cartItems.forEach(item => {
      tmpItem = this.cart.reduce((getItem, cartItem)=> {
        if(cartItem.product && item.product) {
          if (cartItem.product.id === item.product.id) {
            if (typeof item.qty === 'string') {
              item.qty = parseInt(item.qty);
            }
            cartItem.qty = item.qty, getItem;
          }
        }
        if(cartItem.plan && item.plan) {
          if (cartItem.plan.id === item.plan.id) {
            if (typeof item.qty === 'string') {
              item.qty = parseInt(item.qty);
            }
            cartItem.qty = item.qty, getItem;
          }
        }
        return getItem.push(cartItem), getItem;

      }, [])
    });
    this.cart = tmpItem;
    this.storage.setCartItem(this.cart);
    this.updateCart();
    return this.getCart();
  }

  // calculate receipt amount
  getReceipt(cartRules: any, _type?: string) {
    let receipt = {
      totalPrice: 0,
      totalPriceNotDivided: 0,
      specialDiscount: 0,
      discount: 0,
      subTotal: 0,
      shippingFee: 0,
      totalIclTax: 0,
      taxRate: 0,
      applyDiscount: 0,
      taxName: ''
    }

    let currentUser = this.storage.getCurrentUserItem();

    let cart = this.getCart(_type);
    if (!cart || !(Array.isArray(cart))) {
      return receipt;
    }

    // check cart has subscription
    let hasSubscription: boolean = false;
    if (_type === 'subscriptions') {
      hasSubscription = true;
    }

    // this.cart.forEach( (cartItem, index ) => {
    //   if(cartItem.plan) {
    //     hasSubscription = true;
    //   }
    // });


    if((cartRules && !hasSubscription) || (hasSubscription && cartRules && this.promotion && this.promotion.planCountryIds && this.promotion.promotionCodes[0].code === 'EVENTRM1')) {
      receipt = this.caculationDiscountPrice(receipt, currentUser, hasSubscription, cartRules, _type);
    }
 
    // // calculate discount price and total price
    // this.cart.forEach( (cartItem, index ) => {
    //   if(cartItem.product) {
    //     // process for product
    //     receipt.totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty;
    //     receipt.totalPriceNotDivided += +cartItem.product.productCountry.sellPrice * cartItem.qty;

    //     this.cartRules.forEach( (cartRuleItem, index ) => {
    //       if(cartRuleItem.productSKU) {
    //         if(currentUser && currentUser.subscriptions.length > 0
    //           && cartItem.product.sku === cartRuleItem.productSKU && cartRuleItem.isActiveWeb) {
    //           receipt.specialDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (parseInt(cartRuleItem.discount) / 100);
    //         }
    //       } else if(hasSubscription && cartRuleItem.isActiveWeb) {
    //         receipt.specialDiscount += (cartItem.product.productCountry.sellPrice * cartItem.qty) * (parseInt(cartRuleItem.discount) / 100);
    //       } else if(this.promotion && this.promotion.productCountryIds && 
    //                 (this.promotion.productCountryIds.indexOf(cartItem.product.productCountry.id) > -1 || this.promotion.productCountryIds === 'all')) {
    //         receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
    //       }
    //     });
    //   }
    //   if(cartItem.plan) {
    //     if(cartItem.plan.planCountry[0]) {
    //       cartItem.plan.planCountry = cartItem.plan.planCountry[0];
    //     }
    //     // process for subscription
    //     receipt.totalPrice += +(cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes;
    //     receipt.totalPriceNotDivided += +cartItem.plan.planCountry.sellPrice * cartItem.qty;
        
    //     // calculate discount
    //     if(this.promotion && this.promotion.planCountryIds && 
    //               (this.promotion.planCountryIds.indexOf(cartItem.plan.planCountry.id) > -1 || this.promotion.planCountryIds === 'all')) {
    //       receipt.discount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes) * (this.promotion.discount / 100);
    //     }
    //   }
    // });
    // // console.log('this.promotion',this.promotion);
    // // check discount for promo: WELCOME10
    // if(receipt.specialDiscount > 0) {
    //   receipt.discount = receipt.specialDiscount;
    // } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
    //   receipt.discount = this.promotion.maxDiscount;
    // }

    // // calculate subTotal
    // receipt.subTotal = receipt.totalPrice - receipt.discount;

    let currentCountry = this.storage.getCountry();
    // calculate shipping fee
    // if(hasSubscription && receipt.totalPriceNotDivided < this.appConfig.getConfig('shippingFeeConfig').minAmount) {
    //   receipt.shippingFee = this.appConfig.getConfig('shippingFeeConfig').fee;
    // }
    if(hasSubscription) {
      receipt.shippingFee = this.appConfig.getConfig('shippingFeeConfig').fee;
    }
    // if(hasSubscription || (this.promotion && this.promotion.isFreeShipping)) {
    //   receipt.shippingFee = 0;
    // }

    if(!hasSubscription || (this.promotion && this.promotion.isFreeShipping) || (currentCountry.code !== 'MYS' && currentCountry.code !== 'SGP' && currentCountry.code !== 'KOR')) {
      receipt.shippingFee = 0;
    }

    // free shipping for product and free trial subscription
    // receipt.shippingFee = 0;a

    // get tax
    receipt.taxRate = currentCountry.taxRate;
    receipt.taxName = currentCountry.taxName;

    // calulate total include tax
    receipt.totalIclTax = receipt.subTotal + +receipt.shippingFee;
    if(!currentCountry.includedTaxToProduct) {
      // receipt.totalIclTax = receipt.totalIclTax + (receipt.totalIclTax * (receipt.taxRate / 100));
    }

    return receipt;
  }

  caculationDiscountPrice(receipt, currentUser, hasSubscription, cartRules, _type?: string) {
    console.log('this.promotion', this.promotion)
    // calculate discount price and total price
    let cart = this.getCart(_type);
    let cheapestPlan: object = null;

    // discount for cart rules
    cart.forEach( (cartItem, index ) => {
      if(cartItem.product && _type === 'alacartes') {
        // process for product
        receipt.totalPrice += +cartItem.product.productCountry.sellPrice * cartItem.qty;
        receipt.totalPriceNotDivided += +cartItem.product.productCountry.sellPrice * cartItem.qty;

        cartRules.forEach( (cartRuleItem, index ) => {
          if(cartRuleItem.productSKU) {
            if(currentUser && (currentUser.subscriptions && currentUser.subscriptions.length > 0)
              && cartItem.product.sku === cartRuleItem.productSKU && cartRuleItem.isActiveMobile) {
              receipt.specialDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (parseInt(cartRuleItem.discount) / 100);
            }
          } else if(hasSubscription && cartRuleItem.isActiveMobile) {
            receipt.specialDiscount += (cartItem.product.productCountry.sellPrice * cartItem.qty) * (parseInt(cartRuleItem.discount) / 100);
          } 
          // else if(this.promotion && this.promotion.productCountryIds && 
          //           (this.promotion.productCountryIds.indexOf(cartItem.product.productCountry.id) > -1 || this.promotion.productCountryIds === 'all')) {
          //   receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
          // }
        });
      }
      if(cartItem.plan && _type === 'subscriptions') {
        if(cartItem.plan.planCountry[0]) {
          cartItem.plan.planCountry = cartItem.plan.planCountry[0];
        }
        // process for subscription
        receipt.totalPrice += +(cartItem.plan.planCountry.pricePerCharge * cartItem.qty);
        receipt.totalPriceNotDivided += +cartItem.plan.planCountry.pricePerCharge * cartItem.qty;
        if (!cheapestPlan) {
          cheapestPlan = cartItem;
        } else if (( (Number(cheapestPlan['plan'].planCountry.pricePerCharge) )) > 
          ( (Number(cartItem.plan.planCountry.pricePerCharge)) )) {
            cheapestPlan = cartItem;
        }

        
        // calculate discount
        // if(this.promotion && this.promotion.planCountryIds && 
        //           (this.promotion.planCountryIds.indexOf(cartItem.plan.planCountry.id) > -1 || this.promotion.planCountryIds === 'all')) {
        //   receipt.discount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes) * (this.promotion.discount / 100);
        // }
        
        if(this.promotion && this.promotion.planCountryIds && this.promotion.promotionCodes[0].code === 'EVENTRM1' &&
                  (this.promotion.planCountryIds.indexOf(cartItem.plan.planCountry.id) > -1 || this.promotion.planCountryIds === 'all')) {
          receipt.discount += +(cartItem.plan.planCountry.sellPrice * cartItem.qty) * (this.promotion.discount / 100);
        }
      }
    });

    // check discout for subscriptionRule
    cartRules.forEach( (cartRuleItem, index ) => {
      if(cheapestPlan && cartRuleItem.type === 'subscriptionRule' && cartRuleItem.isActiveMobile && !cheapestPlan['plan'].isTrial) {
        receipt.specialDiscount += ((cheapestPlan['plan'].planCountry.pricePerCharge)) * (cartRuleItem.discount / 100);
      }
    });
    console.log('cheapestPlan', cheapestPlan);
    console.log('receipt.specialDiscount', receipt.specialDiscount)
    // calculate discount price
    if( this.promotion.minSpend && receipt.totalPrice < this.promotion.minSpend ) {
    } else {
      // discount for promotion
      cart.forEach( (cartItem, index ) => {
        if(cartItem.product) {
          // cartRules.forEach( (cartRuleItem, index ) => {
          //   if(cartRuleItem.productSKU) {
          //   } else if(hasSubscription && cartRuleItem.isActiveWeb) {
          //   } else if(this.promotion && this.promotion.productCountryIds && 
          //             (this.promotion.productCountryIds.indexOf(cartItem.product.productCountry.id) > -1 || this.promotion.productCountryIds === 'all')) {
          //     receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
          //   }
          // });
          if(this.promotion && this.promotion.productCountryIds && 
            (this.promotion.productCountryIds.indexOf(`${cartItem.product.productCountry.id}`) > -1 || this.promotion.productCountryIds === 'all')) {
            receipt.discount += +cartItem.product.productCountry.sellPrice * cartItem.qty * (this.promotion.discount / 100);
            receipt.applyDiscount += +cartItem.product.productCountry.sellPrice * cartItem.qty;
          }
        }
        if(cartItem.plan) {
          if(cartItem.plan.planCountry[0]) {
            cartItem.plan.planCountry = cartItem.plan.planCountry[0];
          }
          // calculate discount
          if(this.promotion && this.promotion.planCountryIds && 
                    (this.promotion.planCountryIds.indexOf(`${cartItem.plan.planCountry.id}`) > -1 || this.promotion.planCountryIds === 'all')) {
            receipt.discount += +((cartItem.plan.planCountry.pricePerCharge * cartItem.qty)) * (this.promotion.discount / 100);
            receipt.applyDiscount += +((cartItem.plan.planCountry.sellPrice * cartItem.qty) / cartItem.plan.PlanType.totalChargeTimes);
          }
        }
      });
      
      // check discount for promo: WELCOME10
      if(receipt.specialDiscount > 0) {
        receipt.discount = receipt.specialDiscount;
      } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
        receipt.discount = this.promotion.maxDiscount;
      }
    }

    // check discount for promo: WELCOME10
    // if(receipt.specialDiscount > 0) {
    //   receipt.discount = receipt.specialDiscount;
    // } else if(this.promotion.maxDiscount && receipt.discount > this.promotion.maxDiscount) {
    //   receipt.discount = this.promotion.maxDiscount;
    // }

    // calculate subTotal
    receipt.subTotal = receipt.totalPrice - receipt.discount;

    return receipt;
  }

  setPromotion(promotion?) {
    if(promotion) {
      this.promotion = promotion;
    } else {
      this.promotion = {};
    }
    this.hasUpdate.emit(true);
  }

  // getStepData
  getStepData() {
    // return true | false
    return this.stepDatas;
  }

  // getStepData
  setStepData(value) {
    // return true | false
    if(value) {
      this.stepDatas = value;
    } else {
      this.stepDatas = false;
    }
  }

  getTotalQty() {
    if (!this.getCart()) {
      return 0;
    }
    this.cart = this.getCart();
    let totalQty = 0;

    this.cart.forEach( (cartItem, index ) => {
      if (cartItem.product) {
        totalQty += cartItem.qty;
      }
      if (cartItem.plan) {
        totalQty += cartItem.qty;
      }
    });

    return totalQty;
  }

  // getPromoCode
  getPromoCode() {
    // return {code: 'abcxyz', price: 0};
    return this.promotion;
  }

  // clear cart
  clearCart(removeAll ?: any) {
    this.storage.clearCart(removeAll);
    this.cart = [];
    this.setPromotion();
    // emit cart change event
    this.hasUpdate.emit(true);
  }

  //sync Cart
  syncCart() {
    // this.cart = this.getCart();
    let curUser = this.storage.getCurrentUserItem();
    if (curUser && curUser.Cart && curUser.Cart.cartDetails.length > 0) {
      // for case: history of a shopping cart of a user is not null
      curUser.Cart.cartDetails.forEach(item => {
        this.addItem(item);
      })
    } else {
      // for case: history of a shopping cart of a user is null and localstorage cart is not null
      this.saveCartToDB()
        .catch(error => console.log(error));
    }
  }
  // reload cart by country
  reloadCart() {
    return new Promise((resolve, reject) => {
      let curUser = this.storage.getCurrentUserItem();
      this.httpService._getList(`${this.appConfig.config.api.carts}`.replace('[userID]', curUser.id))
      .subscribe(
        data => {
          resolve(data);
        },
        error => {
          reject(error);
        }
      )
    })
  }
}
