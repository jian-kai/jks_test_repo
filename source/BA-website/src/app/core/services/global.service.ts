import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, URLSearchParams } from '@angular/http';
import { Router } from  '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../../core/services/storage.service';
import { AppConfig } from 'app/config/app.config';
import { HttpService } from '../../core/services/http.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class GlobalService {
  constructor(private translate: TranslateService,
    private storageService: StorageService,
    private appConfig: AppConfig,
    private _http: HttpService,
  ) {
  }
  _URLSearchParams(options) {
    let params = new URLSearchParams();
    for(let key in options){
        params.set(key, options[key]) 
    }
    return params;
  }

  _fillterProductLanguage(arr, language) {
    let index = 0;
    for(var j = 0; j < arr.length; j++) {
      if(arr[j].langCode === language) {
        index = j;
      }
    }
    return index;
  }

  public copyArray(content) {
    let arr = [];
    content.forEach((x) => {
      arr.push(Object.assign({}, x));
    })
    arr.map((x) => {x.status = 'default'});
    return content.concat(arr);
  }

  public getCurrentUser(): Observable<any> {
    // this.storageService
    return;
  }

  // merge data
  public mergeObject(output: any, input: any,): any {
    return Object.assign(output, input);
  }
  
  //  Set Value for FormBuilder Control
  setValueFormBuilder(_formBuilder, _objectValue) {
    Object.keys(_objectValue).forEach(name => {
      if (_formBuilder.controls[name]) {
        _formBuilder.controls[name].setValue(_objectValue[name]);
      }
    });
    return _formBuilder;
  }

  deliverTimesArr(deliverTimes: number) {
    let deliverTimesArr : any = [];
    for (var index = 1; index <= deliverTimes; index++) {
      deliverTimesArr.push(index);
    }
    return deliverTimesArr;
  }
  
  // get next deliver date
  getNextDeliverDate(_startDate : any, _totalDeliverTimes: number, _deliverTime: number ) {
    // _totalDeliverTimes = 4;
    let _month : number = (12/_totalDeliverTimes) * (_deliverTime - 1);
    let startDate = new Date(_startDate);
    startDate.setMonth(startDate.getMonth() + _month);
    return startDate;
  }

  // get directory_countries
  getDirectoryCountries() {
    return new Promise((resolve, reject) => {
      this._http._getList(`${this.appConfig.config.api.directory_countries}`).subscribe(
        data => {
          resolve(data);
        },
        error => {
          resolve(error);
          console.log(error.error)
        })
    });
  }

  // get directory country
  getDirectoryCountry(directoryCountries: any) {
    console.log('directoryCountries', directoryCountries);
    console.log('contry code - ', this.storageService.getCountry().code);
    return directoryCountries.filter(country => country.code === this.storageService.getCountry().code); 
  }

  getCountryCodes() {
    return [ { "name": "Malaysia", "dial_code": "+60", "code": "MYS" },
              { "name": "Singapore", "dial_code": "+65", "code": "HKG" },
              { "name": "Hong Kong", "dial_code": "+852", "code": "IND" },
              { "name": "India", "dial_code": "+91", "code": "THA" },
              { "name": "Indonesia", "dial_code": "+62", "code": "PHL" },
              { "name": "Thailand", "dial_code": "+66", "code": "IDN" },
              { "name": "Philippines", "dial_code": "+63", "code": "SGP" },
              { "name": "South Korea", "dial_code": "+82", "code": "KOR" }
    ];
  }

  resetForm(element) {
    if (element && element.nativeElement) {
      let form = element.nativeElement;
      form.insertAdjacentHTML('beforeend', '<input type="reset" style="width: 0;height: 0;opacity: 0;display: none;visibility: hidden;">');
      form.querySelector("[type='reset']").click();
      form.querySelector("[type='reset']").remove();
    }
  }

  validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }


  getCurrencyDisplay(countries: any, currencyCode: string) {
    if (countries) {
      return countries.filter(item => item.currencyCode == currencyCode).length > 0 ? countries.filter(item => item.currencyCode == currencyCode)[0].currencyDisplay : '';
    }
    return '';
  }

  calculateWorkingDay(_beginDate, numWorkDays) {
    let beginDate = new Date(_beginDate);
    let nextDate = beginDate;

    for(let i = 1; i <= numWorkDays; i++) {
      nextDate = new Date(nextDate.setDate(nextDate.getDate() + 1));

      while (nextDate.getDay() == 6 || nextDate.getDay() == 0) {
        nextDate = new Date(nextDate.setDate(nextDate.getDate() + 1));
      }
    }

    return nextDate;
  }

  // get cart type
  getCardType(number)
  {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "Visa";

    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
    // if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) 
    var mastercardRegEx = /^5[1-5]|(2(?:2[2-9][^0]|2[3-9]|[3-6]|22[1-9]|7[0-1]|72[0]))\d*/;
    if (mastercardRegEx.test(number))
        return "Mastercard";

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "AMEX";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    return "";
  }

	// re-format date to param
	formatDate(_date: Date) {
		let year = _date.getFullYear().toString();
		let month = (_date.getMonth() + 1).toString();
		let day = _date.getDate().toString();

		if (month.length === 1) {
			month = '0' + month;
		}
		if (day.length === 1) {
			day = '0' + day;
		}

		return year + '-' + month + '-' + day;
	}

}