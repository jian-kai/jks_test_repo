import { Pipe, PipeTransform } from '@angular/core';

const countries = {
  'MYS': 'MY',
  'SGP': 'SG',
  'KOR': 'KR'
}

const size = 9;

@Pipe({
  name: 'orderNo'
})
export class OrderNoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let tpm = value + "";
    while (tpm.length < size) tpm = "0" + tpm;
    let countryCode = countries[args] || args;
    return `#${countryCode}${tpm}`;
  }

}
