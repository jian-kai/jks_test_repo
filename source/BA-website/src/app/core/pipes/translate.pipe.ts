import { TranslateService } from '@ngx-translate/core';
import { Pipe, PipeTransform } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';

@Pipe({
  name: 'translatePipe',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslateService,
    public storage: StorageService
  ) {}

  transform(arrayTranslate: any, args?: any, limitLength?: number): any {
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate = arrayTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    // this.translate.currentLang = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    // let translate = arrayTranslate.find(value => value.langCode === this.translate.currentLang);
    translate = translate || arrayTranslate.find(value => value.isDefault === true);
    if (!translate) {
      translate = arrayTranslate[0];
    }
    // console.log(`translate ${JSON.stringify(translate)} --- args: ${args} --- limit: ${limitLength}`);
    if (limitLength && translate[args]) {
      return translate[args].substring(0, 200);
    }
    return translate[args];
  }

}