import { Pipe, PipeTransform } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { CommonModule, CurrencyPipe } from '@angular/common';
@Pipe({
  name: 'smsCurrency',
  pure: false,
})
export class SmsCurrencyPipe implements PipeTransform {
  constructor(private storage: StorageService, private currencyPipe: CurrencyPipe) { }
  transform(value: any, args?: any): any {
    let country = this.storage.getCountry();
    let lang = this.storage.getLanguageCode();
    if (country.code.toUpperCase() === 'KOR' && lang.toUpperCase() === 'KO') {
      value = typeof value === 'number' ? value.toFixed(2) : value;
      return `${this.currencyPipe.transform(value).replace('USD', '').replace('.00', '')}원`;
    }
    else {
      let country = this.storage.getCountry();
      value = typeof value === 'number' ? value.toFixed(2) : value;
      return `${country.currencyDisplay}${this.currencyPipe.transform(value).replace('USD', '')}`;
    }
  }

}
