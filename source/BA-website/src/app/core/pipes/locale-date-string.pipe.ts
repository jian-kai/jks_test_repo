import { TranslateService, } from '@ngx-translate/core';
import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'date',
  pure: false
})
export class LocaleDateStringPipe extends DatePipe implements PipeTransform {

  constructor(
    public storage: StorageService) {
      super('en-US');
    }
  // public storage: StorageService;

  transform(dateString: any, pattern?: any): any {
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    if(currentLang === 'KO') {
      let locale = 'ko-KR';
      let tmpDate = new Date(dateString);
      let option = {};
      if(pattern.indexOf('d') > -1) {
        if(pattern.indexOf('dd') > -1) {
          option['day'] = '2-digit';
        } else {
          option['day'] = 'numeric';
        }
      }
      if(pattern.indexOf('MM') > -1) {
        if(pattern.indexOf('MMM') > -1) {
          option['month'] = 'long';
        } else {
          option['month'] = '2-digit';
        }
      }
      if(pattern.indexOf('y') > -1) {
        if(pattern.indexOf('yyy') > -1) {
          option['year'] = 'numeric';
        } else {
          option['year'] = '2-digit';
        }
      }
      if(pattern.indexOf('h') > -1) {
        if(pattern.indexOf('hh') > -1) {
          option['hour'] = '2-digit';
        } else {
          option['hour'] = 'numeric';
        }
      }
      if(pattern.indexOf('m') > -1) {
        if(pattern.indexOf('mm') > -1) {
          option['minute'] = '2-digit';
        } else {
          option['minute'] = 'numeric';
        }
      }
      return tmpDate.toLocaleDateString(locale, option);
      // }
    }
    return super.transform(dateString, pattern);
  }

}
