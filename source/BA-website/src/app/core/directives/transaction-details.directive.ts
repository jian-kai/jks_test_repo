import { Directive, ElementRef, Input } from '@angular/core';
import showdown from 'showdown';

@Directive({
  selector: 'appTransactionDetails'
})
export class TransactionDetailsDirective {
  @Input() details: string;
  converter: any;

  constructor(private el: ElementRef) {
    this.converter = new showdown.Converter();
  }

  ngOnInit() {
    this.el.nativeElement.innerHTML = this.converter.makeHtml(this.details.replace(/\\n/g, '\n'));
    let ul = this.el.nativeElement.querySelector('ul');
    if(ul) {
      ul.style = 'list-style: inside;';
    }
  }
}
