import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Converter } from 'showdown';
import { StorageService } from '../../core/services/storage.service';
@Directive({
  selector: 'appProductDescription'
})
export class ProductDescriptionDirective implements OnInit {
  @Input() productTranslate: Array<any> = [];
  @Input() isSubsequent: Boolean = false;
  converter: any;

  constructor(private el: ElementRef,
    private translate: TranslateService,
    public storage: StorageService
    ) {
    this.converter = new Converter({'simpleLineBreaks': true});
  }

  ngOnInit() {
    // let currentLang = this.translate.currentLang ? this.translate.currentLang.toUpperCase() : 'EN';
    let currentLang = this.storage.getLanguageCode() ? this.storage.getLanguageCode().toUpperCase() : 'EN';
    let translate = this.productTranslate.find(value => value.langCode.toUpperCase() === currentLang);
    this.el.nativeElement.innerHTML = this.converter.makeHtml(translate.description.replace(/\\n/g, '\n'));
    if(this.isSubsequent) {
      this.el.nativeElement.innerHTML = this.converter.makeHtml(translate.subsequentDescription.replace(/\\n/g, '\n'));
    }
    // let ul = this.el.nativeElement.querySelector('ul');
    // if(ul) {
    //   // ul.style = 'list-style: inside;';
    //   // ul.addClass('description-details');
    //   // ul.classList.add('description-details');
    //   // var d = document.getElementById("ul");
    //   ul.className += "description-details";
    // }
    let ul = this.el.nativeElement.querySelectorAll('ul');
    // ul.forEach(element => {
    //   element.className += "description-details";
    // });
    for(let i = 0; i < ul.length; i++) {
      // element.style = 'list-style: inside;';
      ul[i].className += "description-details";
    }
  }
}
