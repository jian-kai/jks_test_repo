import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PgwSliderComponent } from './pgw-slider.component';

describe('PgwSliderComponent', () => {
  let component: PgwSliderComponent;
  let fixture: ComponentFixture<PgwSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PgwSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PgwSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
