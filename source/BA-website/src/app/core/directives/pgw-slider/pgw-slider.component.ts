import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { AppConfig } from 'app/config/app.config';
declare var jQuery: any;

@Component({
  selector: 'app-pgw-slider',
  templateUrl: './pgw-slider.component.html',
  styleUrls: ['./pgw-slider.component.scss']
})
export class PgwSliderComponent implements AfterViewInit {
  @Input() addClass: string = '';
  @Input() items: any = [];
  @Input() options: any = {};
  @ViewChild('slider') slider: ElementRef;
  public apiURL: String;
  constructor(private appConfig: AppConfig) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngAfterViewInit() {
    if(this.items.length > 0)
      jQuery(this.slider.nativeElement).pgwSlider(this.options);
  }

}
