import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild, Renderer } from '@angular/core';
declare var jQuery: any;
import { FileValidator } from '../../directives/required-file.directive';

@Component({
  selector: 'single-file-upload',
  templateUrl: './single-file-upload.component.html',
  styleUrls: ['./single-file-upload.component.scss']
})
export class SingleFileUploadComponent implements OnInit {
  @Input() group;
  @Input() oldFile : string;
  @Input() controlName: string;
  public file : any;
  private url : string;
  public show: boolean = true;
  constructor( private _elemRef: ElementRef, private _renderer: Renderer ) {}

  ngOnInit() {
    console.log('this.group first ', this.group)
  }
  fileChange(e) {
    let file: File = (e.srcElement || e.target).files[0];
    if (file) {
      // for (var _index = 0; _index < fileList.length; _index++) {
        // let fileData: File = fileList[_index];
        let file_tmp : any = {name: file.name}
        let reader = new FileReader();
        let target:EventTarget;
        reader.onload = (event: any) => {
          file_tmp.url = event.target.result;
        }
        this.file = (file_tmp);
        reader.readAsDataURL(file);
      // }
      this.group.controls[this.controlName].setValue(file);
      this.show = false;
    }
  }

  removeAddCase() {
    this.file = null;
    this.show = true;
    this.group.controls[this.controlName].setValue('');
  }



}
