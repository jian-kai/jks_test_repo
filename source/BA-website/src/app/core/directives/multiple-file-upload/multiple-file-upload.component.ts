import { Component, OnInit, Input, ElementRef, Inject, AfterViewInit, ViewChild } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'multiple-file-upload',
  templateUrl: './multiple-file-upload.component.html',
  styleUrls: ['./multiple-file-upload.component.scss']
})
export class MultipleFileUploadComponent implements OnInit {
  @Input() oldFiles = [];
  @Input() group;
  @Input() controlName: string;
  public show: boolean = true;
  private defaultImageIndex : boolean = false;
  private newFiles = [];
  public files = [];
  private url : string;
  @ViewChild('ulImages') ulImages: ElementRef;
  constructor() {}

  ngOnInit() {
    if (!this.oldFiles) {
      this.oldFiles = [];
    }
    if (this.group.controls['defaultImageIndex']) {
      this.defaultImageIndex = true;
    }
    console.log('imagesModel', this.oldFiles);
  }
  fileChange(e) {
    this.show = false;
    let fileList: FileList = (e.srcElement || e.target).files;
    if (fileList.length > 0) {
      for (var _index = 0; _index < fileList.length; _index++) {
        let file: File = fileList[_index];
        let file_tmp : any = {index : this.files.length, name: file.name}
        this.newFiles.push(file);
        let reader = new FileReader();
        let target:EventTarget;
        reader.onload = (event: any) => {
          file_tmp.url = event.target.result;
        }
        this.files.push(file_tmp);
        reader.readAsDataURL(file);
      }
      this.group.controls[this.controlName].setValue(this.newFiles);
      if (this.group.controls['defaultImageIndex'] && !this.group.controls['defaultImageIndex'].value) {
        this.group.controls['defaultImageIndex'].setValue(0);
      }
    }
    this.show = true;
  }

  removeAddCase(_index: number) {
    this.files = this.files.filter(file => file.index != _index);
    this.newFiles = this.newFiles.filter((file, index) => index != _index);
    if (this.newFiles.length <= 0) {
      this.group.controls[this.controlName].setValue('');
    } else {
      this.group.controls[this.controlName].setValue(this.newFiles);
    }
    if (this.group.controls['defaultImageIndex'] && _index == this.group.controls['defaultImageIndex'].value) {
      this.group.controls['defaultImageIndex'].setValue(0);
    }
  }

}
