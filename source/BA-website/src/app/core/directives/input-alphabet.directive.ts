import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[inputAlphabet]'
})

export class InputAlphabetDirective {

  constructor(private el: ElementRef,) {
    $(el.nativeElement).on('change', (e) => {
      if (e.target.value && !/^[A-Za-z]+$/.test(e.target.value)) {
        $(el.nativeElement).val('');
      }
    });
  }
  @HostListener('keypress', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    const pattern = /.*[^A-Za-z].*/;
    // let inputChar = String.fromCharCode(event.keyCode);
    let inputChar = String.fromCharCode(event.which);

    if (pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
}
