import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[appSpecialCharacter]'
})
export class SpecialCharacterDirective {
  constructor(private el: ElementRef,) {
    $(el.nativeElement).on('change', (e) => {
      if (e.target.value && !/^[A-Za-z0-9]+$/.test(e.target.value)) {
        $(el.nativeElement).val('');
      }
    });
  }
  @HostListener('keypress', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    const pattern = /.*[^A-Za-z0-9].*/;
    let inputChar = String.fromCharCode(event.which);
    if (pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
