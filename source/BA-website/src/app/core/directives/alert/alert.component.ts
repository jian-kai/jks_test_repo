import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { fadeOutAnimation } from '../../../core/animations/fade-out.animation';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  animations: [fadeOutAnimation]
})
export class AlertComponent implements OnChanges, OnInit {
  @Input() data: object;
  constructor() {}

  ngOnInit() {
    // if (!this.data) {
    //   this.data = null;
    // }
  }

  ngOnChanges(changes) {
    // this.data['message'] = (Array.isArray(this.data['message'])) ? this.data['message'] : (this.data['message']) ? [this.data['message']] : [];
  }
}
