import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: 'appStatus'
})
export class StatusDirective {
  @Input() status: string;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.status = this.status || '';
    this.status = this.status.toString();
    switch(this.status){
      case 'processing':
        this.el.nativeElement.innerHTML = `<div class="label label-processing">${this.status}</div>`;
        break;
      case 'awaiting payment':
        this.el.nativeElement.innerHTML = `<div class="label label-info">${this.status}</div>`;
        break;
      case 'dealing':
        this.el.nativeElement.innerHTML = `<div class="label label-primary">${this.status}</div>`;
        break;
      case 'delivering':
        this.el.nativeElement.innerHTML = `<div class="label label-delivering">${this.status}</div>`;
        break;
      case 'completed':
        this.el.nativeElement.innerHTML = `<div class="label label-success">${this.status}</div>`;
        break;
      case 'canceled':
        this.el.nativeElement.innerHTML = `<div class="label label-warning">${this.status}</div>`;
        break;
      case 'no show':
        this.el.nativeElement.innerHTML = `<div class="label label-danger">${this.status}</div>`;
        break;
      case 'active':
        this.el.nativeElement.innerHTML = `<div class="label label-success">${this.status}</div>`;
        break;
      case 'deactive':
        this.el.nativeElement.innerHTML = `<div class="label label-warning">${this.status}</div>`;
        break;
      case 'pending':
        this.el.nativeElement.innerHTML = `<div class="label label-info">${this.status}</div>`;
        break;
      case 'true':
        this.el.nativeElement.innerHTML = `<div class="label label-success">${this.status}</div>`;
        break;
      case 'false':
        this.el.nativeElement.innerHTML = `<div class="label label-warning">${this.status}</div>`;
        break;
    }
  }
}
