import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  public isLoading: boolean;
  constructor(private loadingService: LoadingService) { }

  ngOnInit() {
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }

}
