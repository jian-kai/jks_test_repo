import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { AppConfig } from 'app/config/app.config';

@Component({
  selector: 'display-image',
  templateUrl: './display-image.component.html',
  styleUrls: ['./display-image.component.scss', '../../../../assets/sass/image.scss']
})
export class DisplayImageComponent implements AfterViewInit {
  @Input() images: any = [];
  @Input() id: string = "";
  @Input() class: string = "";
  @Input() alt: string = "";
  public urlImage: string = "";
  private apiURL: string = "";
  constructor(private appConfig: AppConfig) {
    this.apiURL = appConfig.config.apiURL;
  }

  ngAfterViewInit() {
    
    if(this.images.length > 0) {
      let i = 0;
      this.images.forEach((element, index) => {
        if(element.isDefault) {
          i = index;
          return;
        }
      });
      setTimeout(_ => this.urlImage = this.images[i].url);
    }
  }

}
