import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { URLSearchParams, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PagerService } from '../../services/pager-service.service';
import { RouterService } from '../../helpers/router.service';

const NUM_OF_PAGES: number = 10;

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss', '../../../../assets/sass/pagination.scss']
})
export class PaginationComponent implements OnInit, OnChanges {
  // array of all items to be paged
  private totalPages: number;
  private pages: Array<number> = [];
  private start: number;
  private end: number;
  private hasNext: boolean = true;
  private hasPrev: boolean = true;

  @Input() totalItems: number = 0;
  @Input() currentPage: number = 0;
  @Input() limit: number = 20;
  @Input() showing: boolean = true;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();

  constructor(private router: Router,
    private location: Location,
    private routerService: RouterService) { }

    ngOnInit() {
      // calculate total page
      this.totalPages = Math.ceil(this.totalItems / this.limit);
      this.calculateStartEndPages();
    }
  
    ngOnChanges(changes) {
      this.ngOnInit();
      console.log(`ngOnchange ${JSON.stringify(changes)}`);
      // console.log('this.totalItems ===', this.totalItems);
      // this.calculateStartEndPages();
    }
  
    /**
     * onLimitChange
     * @param: event object
     */
    onLimitChange(event) {
      console.log('event ===', event);
      this.currentPage = 1;
      this.limit = event.currentTarget.value;
      this.routerService.onLimitChange(event);
    }
  
    calculateStartEndPages() {
      // reset value
      this.hasNext = true;
      this.hasPrev = true;
  
      if (this.totalPages <= 10) {// less than 10 total pages so show all
        this.start = 1;
        this.end = this.totalPages;
      } else {// more than 10 total pages so calculate start and end pages
        if (this.currentPage <= 6) {
            this.start = 1;
            this.end = 10;
        } else if (this.currentPage + 4 >= this.totalPages) {
            this.start = this.totalPages - 9;
            this.end = this.totalPages;
        } else {
            this.start = this.currentPage - 5;
            this.end = this.currentPage + 4;
        }
      }
  
      // determine has next and has previous
      if (this.currentPage === this.totalPages) {
        this.hasNext = false;
      }
      if (this.currentPage === 1) {
        this.hasPrev = false;
      }
  
      // calculate number of page displayed
      this.pages = [];
      // for (let i = this.start; (i <= this.end); i++) {
      //   this.pages.push(i);
      // }
      for (let i = 1; (i <= this.totalPages); i++) {
        this.pages.push(i);
      }
    }
  
    goto(page) {
      this.routerService.appendRouterParams({ page });
      this.pageChange.emit(page);
    }
  
    onNext() {
      if (this.hasNext) {
        this.goto(this.currentPage + 1);
      }
    }
  
    onPrev() {
      if (this.hasPrev) {
        this.goto(this.currentPage - 1);
      }
    }
  }
  