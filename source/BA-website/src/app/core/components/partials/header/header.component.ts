import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { RouterService } from '../../../../core/helpers/router.service';
import { UsersService } from '../../../../auth/services/users.service';
import { CartService } from '../../../../core/services/cart.service';
import { GlobalService } from '../../../../core/services/global.service';
import { StorageService } from '../../../../core/services/storage.service';
import { TranslateService } from '@ngx-translate/core';
import { CountriesService } from '../../../../core/services/countries.service';
import * as $ from 'jquery';

// const screen_name = {
//   // '/': 'Select a category to begin',
//   '/': 'Home',
//   '/alacartes': 'Ala Carte',
//   '/subscriptions': 'Introductory Subscription',
//   '/sample-product': 'Free Trial',
//   // '/checkout/review/alacartes': 'Review & pay',
//   // '/checkout/review/subscriptions': 'Review & pay',
//   '/checkout/review/alacartes': 'Shopping Cart',
//   '/checkout/review/subscriptions': 'Shopping Cart',
//   '/checkout/payment/alacartes': 'Checkout',
//   '/checkout/payment/subscriptions': 'Checkout',
//   'summary/alacartes': 'Order Confirmed',
//   'summary/subscriptions': 'Order Confirmed',
//   '/orders' : 'Sales History',
//   '/blade-type': 'Introductory Subscription',
// }

const countries = {
  'MYS': 'MY',
  'SGP': 'SG'
}
const size = 9;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  screenName: string;
  public routerName: string;
  public showBackButton: boolean = true;
  public i18n: any;
  public screen_name: any = {};
  public currentUser: any;
  languagesList: Array<any> = [];
  currentLang: any = '';
  constructor(
    private location: Location,
    private router: Router,
    private routerService: RouterService,
    private cartService: CartService,
    public userService: UsersService,
    public globalService: GlobalService,
    private translate: TranslateService,
    private storage: StorageService,
    private countriesService: CountriesService
  ) {
    this.currentUser = this.storage.getCurrentUserItem();

    this.getScreenName();

    router.events.subscribe((router:any) => setTimeout(() => {
      this.routerName = router.url;
      if (router.url.indexOf("summary/alacartes") > -1) {
        this.screenName = this.screen_name["summary/alacartes"];
        this.showBackButton = false;
      } else if (router.url.indexOf("summary/subscriptions") > -1) {
        this.screenName = this.screen_name["summary/subscriptions"];
        this.showBackButton = false;
      } else if(router.url.search(/subscriptions\/blade-type/i) > -1) {
        this.screenName = this.screen_name["/subscriptions"];
        this.showBackButton = true;
      } else if(router.url.indexOf("/orders/") > -1) {
        let params = router.url.replace("/orders/", '');
        // {{ item.id | orderNo: item.Country.code }}
        let tpm = params.split('/')[1] + "";
        while (tpm.length < size) tpm = "0" + tpm;
        let countryCode = countries[params.split('/')[0]] || params.split('/')[0];
        this.screenName = `#${countryCode}${tpm}`;
        this.showBackButton = true;
      } else {
        this.showBackButton = true;
        if (router.url) {
          if(router.url.indexOf("/orders?") > -1) {
            this.screenName = this.screen_name["/orders?"];
          } else {
            this.screenName = this.screen_name[router.url];
          }
        } else {
          this.screenName = this.screen_name['/'];
        }
        
      }
    }));

    this.currentLang = this.storage.getLanguageCode();
    
    if(this.currentUser.Country.SupportedLanguages && this.currentUser.Country.SupportedLanguages[0]) {
      let directoryCountries:any = [];
      this.globalService.getDirectoryCountries().then(_directoryCountries => {
        directoryCountries = _directoryCountries;
        let languageName = '';
        let dirCountry: any;
        let country = this.currentUser.Country;
        country.displayLang = `${country.name} (${country.DirectoryCountries[0].languageName})`;
        country.valueLang = country.defaultLang;
        this.languagesList.push(Object.assign({}, country));

        country.SupportedLanguages.forEach(supLanguage => {
          languageName = '';
          dirCountry = directoryCountries.filter(dirCountry => dirCountry.languageCode.toUpperCase() === supLanguage.languageCode.toUpperCase())[0];
          if(dirCountry) {
            languageName = dirCountry.languageName;
          }
          country.displayLang = `${country.name} (${languageName})`;
          country.valueLang = dirCountry.languageCode;
          this.languagesList.push(Object.assign({}, country));
        });
      })
    }
  }

  ngOnInit() {
  }

  getScreenName() {
    this.translate.get('title').subscribe(res => {
      this.i18n = res;
    });

    this.screen_name = {
      // '/': 'Select a category to begin',
      '/': this.i18n.home,
      '/alacartes': this.i18n.alacartes,
      '/subscriptions': this.i18n.subscriptions,
      '/sample-product': this.i18n.sample_product,
      // '/checkout/review/alacartes': 'Review & pay',
      // '/checkout/review/subscriptions': 'Review & pay',
      '/checkout/review/alacartes':  this.i18n.shopping,
      '/checkout/review/subscriptions': this.i18n.shopping,
      '/checkout/payment/alacartes': this.i18n.checkout,
      '/checkout/payment/subscriptions': this.i18n.checkout,
      '/checkout/payment/alacartes/roadshow': this.i18n.checkout,
      '/checkout/payment/subscriptions/roadshow': this.i18n.checkout,
      'summary/alacartes': this.i18n.order_confirm,
      'summary/subscriptions': this.i18n.order_confirm,
      '/orders' : this.i18n.sale_history,
      '/orders?' : this.i18n.sale_history,
      '/blade-type': this.i18n.subscriptions,
    }

    this.routerName = this.router.url;
    if (this.router.url.indexOf("summary/alacartes") > -1) {
      this.screenName = this.screen_name["summary/alacartes"];
      this.showBackButton = false;
    } else if (this.router.url.indexOf("summary/subscriptions") > -1) {
      this.screenName = this.screen_name["summary/subscriptions"];
      this.showBackButton = false;
    } else if(this.router.url.search(/subscriptions\/blade-type/i) > -1) {
      this.screenName = this.screen_name["/subscriptions"];
      this.showBackButton = true;
    } else if(this.router.url.indexOf("/orders/") > -1) {
      let params = this.router.url.replace("/orders/", '');
      // {{ item.id | orderNo: item.Country.code }}
      let tpm = params.split('/')[1] + "";
      while (tpm.length < size) tpm = "0" + tpm;
      let countryCode = countries[params.split('/')[0]] || params.split('/')[0];
      this.screenName = `#${countryCode}${tpm}`;
      this.showBackButton = true;
    } else {
      this.showBackButton = true;
      if (this.router.url) {
        if(this.router.url.indexOf("/orders?") > -1) {
          this.screenName = this.screen_name["/orders?"];
        } else {
          this.screenName = this.screen_name[this.router.url];
        }
      } else {
        this.screenName = this.screen_name['/'];
      }
      
    }
  }

  back() {
    let res = '';
    let str_sub = this.routerName.substr(this.routerName.lastIndexOf("/"));
    res = this.routerName.replace(str_sub, '');
    if (res.indexOf("summary/alacartes") > -1) {
      this.router.navigate(['/']);
    } else if (res.indexOf("summary/subscriptions") > -1) {
      this.router.navigate(['/']);
    } else {
      this.location.back();
    }
  }

  getRouterObj() {
    this.routerService.routerObj.subscribe(data => {
      if (data && data.header) {
        this.screenName = data.header;
      }
    })
  }

  logout(event) {
    if (!$('.btn-top-menu-collapse').hasClass('collapsed')) {
      $('.btn-top-menu-collapse').trigger('click');
    }
    event.preventDefault();
    this.cartService.clearCart();
    this.storage.clearCustomerDetails();
    this.storage.clearStripeDetail();
    this.userService.logout();
    this.router.navigate(['/login']);
  }

  changeLanguage(language: any) {
    this.storage.setLanguageCode(language.valueLang.toLowerCase());
    this.currentLang = language.valueLang;
    // this.translate.setDefaultLang(this.currentLang.toLowerCase());
    this.translate.use(this.currentLang.toLowerCase());
    this.getScreenName();
    this.countriesService.changeLanguage();
  }
}
