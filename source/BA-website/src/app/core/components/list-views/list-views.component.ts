import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Location } from '@angular/common';
import { RouterService } from '../../../core/helpers/router.service';

@Component({
  selector: 'app-list-views',
  templateUrl: './list-views.component.html',
  styleUrls: ['./list-views.component.scss']
})
export class ListViewsComponent implements OnInit {
  currentPage: number = 1;
  limit: number = 20;
  totalItems: number = 0;
  currentSortField: string;
  options: URLSearchParams;
  limitPageList: Array<number> = [10, 25, 50, 100];
  constructor(private location: Location,
    private routerService: RouterService,
    private el: ElementRef) { }

    ngOnInit() {
      let options = new URLSearchParams(this.location.path(false).split('?')[1]);
      if(options.get('limit')) {
        this.limit = +options.get('limit');
      } else {
        options.set('limit', `${this.limit}`);
      }
      if(options.get('page')) {
        this.currentPage = +options.get('page');
      }
      if(options.get('orderBy')) {
        this.currentSortField = options.get('orderBy');
      }
      this.options = options;
      this.getData();
    }

    ngAfterViewInit() {
      this.checkSortField();
    }
  
    getData() {
      // interface for fetch data
    }

    onLimitChange(event) {
      console.log('onLimitChange');
      this.limit = event.currentTarget.value;
      this.currentPage = 1;
      this.options.set('page', `${this.currentPage}`);
      this.options.set('limit', `${this.limit}`);
      this.routerService.appendRouterParams({
        page: this.currentPage,
        limit: this.limit
      });
      this.getData();
    }
  
    pageChange(page) {
      this.currentPage = page;
      this.options.set('page', page);
      this.getData();
    }
  
    checkSortField() {
      if(this.currentSortField) {
        let sortField = this.el.nativeElement.querySelector(`th[data-field=${this.currentSortField.replace('_DESC', '')}]`);
        if(this.currentSortField.indexOf('_DESC') > -1) {
          sortField.classList.add('sorting_desc');
        } else {
          sortField.classList.add('sorting_asc');
        }
        sortField.classList.remove('sorting');
      }
    }
  
    sortBy(field, $event) {
      console.log('sortBy');
      // reset all sorting class
      if(field !== this.currentSortField) {
        if(this.el.nativeElement.querySelector('.sorting_asc')) {
          this.el.nativeElement.querySelector('.sorting_asc').classList.add('sorting');
          this.el.nativeElement.querySelector('.sorting_asc').classList.remove('sorting_asc');
        }
        if(this.el.nativeElement.querySelector('.sorting_desc')) {
          this.el.nativeElement.querySelector('.sorting_desc').classList.add('sorting');
          this.el.nativeElement.querySelector('.sorting_desc').classList.remove('sorting_desc');
        }
      }
  
      // perform sorting
      let currentEle = $event.currentTarget;
      if(currentEle.classList.value === 'sorting') {
        this.options.set('orderBy', `${field}`);
        this.routerService.appendRouterParams({orderBy: field});
        currentEle.classList.add('sorting_asc');
        currentEle.classList.remove('sorting');
      } else if(currentEle.classList.value === 'sorting_asc') {
        this.options.set('orderBy', `${field}_DESC`);
        this.routerService.appendRouterParams({orderBy: `${field}_DESC`});
        currentEle.classList.add('sorting_desc');
        currentEle.classList.remove('sorting_asc');
      } else {
        this.options.set('orderBy', `${field}`);
        this.routerService.appendRouterParams({orderBy: field});
        currentEle.classList.add('sorting_asc');
        currentEle.classList.remove('sorting_desc');
      }
      this.currentSortField = field;
      this.getData();
    }
  
    onSearch(event) {
      let query = event.currentTarget.value;
      this.options.set('search', query);
      this.getData();
    }
}
