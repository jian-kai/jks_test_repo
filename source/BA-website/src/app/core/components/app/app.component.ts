import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { translator } from '../../../i18n';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { LoadingService } from '../../../core/services/loading.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public routerName: string;
  public currentDate: any;
  public loginDate: any;
  public timeOut: any;
  constructor(private http: Http,
    private translate: TranslateService,
    private storage: StorageService,
    private loadingService: LoadingService,
    private appConfig: AppConfig,
    public userService: UsersService,
    private httpService: HttpService,
    private router: Router
  ) {
    // set transatation data
    translate.setTranslation('en', translator.en);
    translate.setTranslation('vn', translator.vn);
    translate.setTranslation('ko', translator.ko);

    // use english as default language
      let currentLang = this.storage.getLanguageCode()
      if(currentLang) {
        translate.use(currentLang.toLowerCase());
        this.storage.setLanguageCode(currentLang.toLowerCase());
      } else {
        translate.use('en');
        this.storage.setLanguageCode('en');
      }
    // translate.setDefaultLang('en');
    // if (this.storage.getCurrentUserItem()) {
    //   let _currentUser = this.storage.getCurrentUserItem();
    //   translate.setDefaultLang(_currentUser.Country.defaultLang);
    //   if(this.storage.getCurrentUserItem().defaultLang  ='KO' && this.storage.getLanguageCode() === 'EN') {
    //     this.storage.setLanguageCode('KO');
    //   }
    // }

    this.userService._loggedIn.subscribe(
      data => {
        if (data) {
          let _currentUser = this.storage.getCurrentUserItem();
          if(currentLang) {
            translate.use(currentLang.toLowerCase());
          } else {
            translate.use(_currentUser.Country.defaultLang.toLowerCase());
          }
          this.autoLogout();
        } else {
          translate.use('en');
          if(this.timeOut) {
            clearTimeout(this.timeOut);
            this.timeOut = null;
          }
        }
      }
    )

    router.events.subscribe((router: any) => {
      this.routerName = router.url;
      if (router instanceof NavigationStart) {
        window.scrollTo(0, 0);
       if (!$('.btn-top-menu-collapse').hasClass('collapsed')) {
          $('.btn-top-menu-collapse').trigger('click');
       }
      }
    });
  }
  ngOnInit() {
    if (this.storage.getAuthToken()) {
      this.autoLogout();
     }
  }

  autoLogout() {
    let limit_hours = this.appConfig.config.logout_timer;
    this.currentDate = new Date();
    this.loginDate = this.storage.getLoginTimer();
    let hourDiff = Math.abs(this.currentDate - this.loginDate);
    if(hourDiff > limit_hours) {
      this.userService.logout();
      this.router.navigate(['/login']);
    }else {
      let autoTime = Math.abs(limit_hours - hourDiff);
      if(!this.timeOut) {
        this.timeOut = setTimeout(()=>{
          this.userService.logout();
          this.router.navigate(['/login']);
        }, autoTime);
      }
    }
  }
}