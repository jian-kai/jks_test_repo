import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormControl } from '@angular/forms';

export function valueValidator (prms : any): ValidatorFn {
  return (control: FormControl): {[key: string]: boolean} => {
    if(Validators.required(control)) {
      return null;
    }

    let val: string = control.value;
    const patternNum = /.*[^0-9].*/;
    const patternAlp = /.*[^A-Za-z].*/;
    let error = {
      number: true,
      alphabet: true,
    };

    if (prms === 'number' && patternNum.test(val)) {
      error.number = true;
    } else {
      error.number = false;
    }

    if (prms === 'alphabet' && patternAlp.test(val)) {
      error.alphabet = true;
    } else {
      error.alphabet = false;
    }

    if (!error.number && !error.alphabet) {
      return null;
    }

    return error;

  };
}