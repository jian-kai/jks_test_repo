export class BaseConfig {
  private config: any;

  constructor(private options: any) {
    this.config = {
      apiURL: options.apiURL,
      baseUrl: options.baseUrl,
      api: {
        login: options.apiURL + '/api/sellers/login',
        products: options.apiURL + '/api/products',
        cart_rules: options.apiURL + '/api/promotions/cart-rules',
        stripe_pubkey: options.apiURL + '/api/countries/stripe-pubkey',
        shipping_fee_configuration: options.apiURL + '/api/countries/shipping-fee',
        check_promo_code: options.apiURL + '/api/promotions/check',
        countries: options.apiURL + '/api/countries',
        users: options.apiURL + '/api/users',
        cards: options.apiURL + '/api/sellers/[sellerID]/cards',
        place_order: options.apiURL + '/api/payment/orders',
        place_trial: options.apiURL + '/api/payment/trial-plan',
        order: options.apiURL + '/api/sellers/[sellerID]/orders',
        plans: options.apiURL + '/api/plans',
        checkEmail: options.apiURL + '/api/sellers/check-email',
        checkToken: options.apiURL + '/api/sellers/check-token',
        delivery_address: options.apiURL + '/api/sellers/[sellerID]/deliveries',
        cancel_order: options.apiURL + '/api/orders/[orderID]',
        sample_product: options.apiURL +  '/api/payment/orders/sample',
        free_trial: options.apiURL +  '/api/plans/free-trial',
        trial_plan_group: options.apiURL +  '/api/trial-plans/groups',
        cancel_subscriptions: options.apiURL + '/api/subscriptions/[orderID]',
        directory_countries: options.apiURL + '/api/directory-countries',
        channel_type: options.apiURL + '/api/sellers/channel-type',

      },
      groupPlanTrialSku: [
        {type: '2 Months', sku: '2-'},
        {type: '3 Months', sku: '3-'},
        {type: '4 Months', sku: '4-'}
      ],
      groupPlanTrialSkuNew: [
        {type: 'Pay as you go'},
        {type: 'Annual Payment'}
      ],
      groupPlanTrialName: [
        'TK3', 'TK5', 'TK6'
      ],
      trial_payment_types: ['Pay as you go', 'Annual Payment'],
      logout_timer: 12 * 60 * 60 * 1000, //12 hours
    };
  }

  public getConfig() {
    return this.config;
  }
}