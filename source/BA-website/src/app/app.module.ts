import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { routes } from './core/app.routes';

// import config
import { AppConfig } from './config/app.config';

// import service
import { StorageService } from './core/services/storage.service';
import { RequestService } from './core/services/request.service';
import { UsersService } from './auth/services/users.service';
import { ModalService } from './core/services/modal.service';
import { AuthService } from './auth/services/auth.service';
import { HttpService } from './core/services/http.service';
import { LoadingService } from './core/services/loading.service';
import { PagerService } from './core/services/pager-service.service';
import { GlobalService } from './core/services/global.service';
import { RouterService } from './core/helpers/router.service';
import { CartService } from './core/services/cart.service';
import { CountriesService } from './core/services/countries.service';
import { DeliveryAddressService } from './core/services/delivery-address.service';
import { CardService } from './core/services/card.service';
import { StripeService } from './core/services/stripe.service';
import { MatDialogModule  } from '@angular/material';
import { DialogService  } from './core/services/dialog.service';

// import component
import { AppComponent } from './core/components/app/app.component';
import { LoginComponent } from './auth/components/login/login.component';
import { ModalComponent } from './core/directives/modal/modal.component';
import { AlertComponent } from './core/directives/alert/alert.component';
import { LoadingComponent } from './core/directives/loading/loading.component';
import { HeaderComponent } from './core/components/partials/header/header.component';
import { RouterLinkActiveDirective } from './core/directives/router-link-active.directive';
import { PaginationComponent } from './core/directives/pagination/pagination.component';
import { TranslatePipe } from './core/pipes/translate.pipe';
import { SmsCurrencyPipe } from './core/pipes/sms-currency.pipe';
import { DisplayImageComponent } from './core/directives/display-image/display-image.component';

import { ListViewsComponent } from './core/components/list-views/list-views.component';
import { TransactionDetailsDirective } from './core/directives/transaction-details.directive';
import { StatusDirective } from './core/directives/status.directive';
import { HomeComponent } from './programs/components/home/home.component';
import { AlacarteListComponent } from './programs/components/alacarte-list/alacarte-list.component';
import { SubscriptionListComponent } from './programs/components/subscription-list/subscription-list.component';
import { ProductDescriptionDirective } from './core/directives/product-description.directive';
import { ReviewComponent } from './programs/components/checkout/review/review.component';
import { PaymentReceiveComponent } from './programs/components/checkout/payment-receive/payment-receive.component';
import { SummaryComponent } from './programs/components/checkout/summary/summary.component';
import { OrdinalPipe } from './core/pipes/ordinal.pipe';
import { OrderListComponent } from './programs/components/order/order-list/order-list.component';
import { OrderResultModalComponent } from './programs/components/order/order-result-modal/order-result-modal.component';
import { DialogConfirmComponent } from './core/directives/dialog-confirm/dialog-confirm.component';
import { DialogResultComponent } from './core/directives/dialog-result/dialog-result.component';
import { DialogErrorComponent } from './core/directives/dialog-error/dialog-error.component';
import { InputNumberDirective } from './core/directives/input-number.directive';
import { SampleProductComponent } from './programs/components/sample-product/sample-product.component';
import { OrderNoPipe } from './core/pipes/order-no.pipe';
import { LocaleDateStringPipe } from './core/pipes/locale-date-string.pipe';
import { SubscriptionGroupComponent } from './programs/components/subscription-group/subscription-group.component';

// directives
import { EqualValidator } from './core/directives/equal-validator.directive';
import { FileValueAccessorDirective } from './core/directives/file-value-accessor.directive';
import { FileValidator } from './core/directives/required-file.directive';
import { MultipleFileUploadComponent } from './core/directives/multiple-file-upload/multiple-file-upload.component';
import { PgwSliderComponent } from './core/directives/pgw-slider/pgw-slider.component';
import { SingleFileUploadComponent } from './core/directives/single-file-upload/single-file-upload.component';
import { ProductTypeTranslatePipe } from './core/pipes/product-type-translate.pipe';
import { OrderDetailComponent } from './programs/components/order/order-detail/order-detail.component';
import { RoadshowDetailComponent } from './programs/components/checkout/roadshow-detail/roadshow-detail.component';
import { InputAlphabetDirective } from './core/directives/input-alphabet.directive';
import { SpecialCharacterDirective } from './core/directives/special-character.directive';

// Add this function
export function initConfig(config: AppConfig) {
  return () => config.load()
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ModalComponent,
    AlertComponent,
    LoadingComponent,
    HeaderComponent,
    RouterLinkActiveDirective,
    PaginationComponent,
    TranslatePipe,
    SmsCurrencyPipe,
    DisplayImageComponent,
    TransactionDetailsDirective,
    StatusDirective,
    HomeComponent,
    AlacarteListComponent,
    SubscriptionListComponent,
    ProductDescriptionDirective,
    ReviewComponent,
    PaymentReceiveComponent,
    SummaryComponent,
    OrdinalPipe,
    OrderListComponent,
    OrderResultModalComponent,
    DialogConfirmComponent,
    DialogResultComponent,
    DialogErrorComponent,
    InputNumberDirective,
    SampleProductComponent,
    OrderNoPipe,
    LocaleDateStringPipe,
    SubscriptionGroupComponent,
    ListViewsComponent,
    EqualValidator,
    FileValueAccessorDirective,
    FileValidator,
    MultipleFileUploadComponent,
    PgwSliderComponent,
    SingleFileUploadComponent,
    ProductTypeTranslatePipe,
    OrderDetailComponent,
    RoadshowDetailComponent,
    InputAlphabetDirective,
    SpecialCharacterDirective

  ],

  imports: [
    HttpModule,
    BrowserModule,
    TranslateModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    RouterModule.forRoot(routes, {
      // useHash: true
    })
  ],
  providers: [StorageService,
    RequestService,
    UsersService,
    ModalService,
    AuthService,
    HttpService,
    LoadingService,
    PagerService,
    GlobalService,
    AppConfig,
    RouterService,
    DatePipe,
    CartService,
    CountriesService,
    DeliveryAddressService,
    CardService,
    StripeService,
    DialogService,
    CurrencyPipe,
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true }
  ],
  entryComponents: [DialogConfirmComponent, DialogResultComponent, DialogErrorComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
