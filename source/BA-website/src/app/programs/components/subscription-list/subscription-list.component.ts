import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';

import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CartService } from '../../../core/services/cart.service';
import { PlanService } from '../../../core/services/plan.service';
import { Item } from '../../../core/models/item';
import { ActivatedRoute } from '@angular/router';
declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-subscription-list',
  templateUrl: './subscription-list.component.html',
  styleUrls: ['./subscription-list.component.scss'],
  providers: [PlanService]
})
export class SubscriptionListComponent implements OnInit {
  public planList: Object = {};
  public planTypes: Array<String> = [];
  private promoCodeForm: FormGroup;
  public cart: Item[] = [];
  public groupName: string;
  public groupPlanTypes: any = [];
  public isloading : boolean = true;
  public planTypesName: any = [];
  public currentUser: any;
  constructor(private httpService: HttpService,
    private storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private router: Router,
    private route: ActivatedRoute,
    private cartService: CartService,
    private builder: FormBuilder,
    private planService: PlanService) {
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser.Country.isBaSubscription || !this.currentUser.Country.isBaEcommerce) {
        this.storage.clearGroupId();
        this.router.navigate(['/']);
      }
      this.promoCodeForm = builder.group({
        promoCode: ['', Validators.required],
      });
      this.loadingService.status.subscribe((value : boolean) => {
        this.isloading = value;
      })
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params.groupName) {
        this.groupName = params.groupName;
      } else {
        this.storage.clearGroupId();
        this.router.navigate(['/']);
      }
    });
    this.cartService.clearCart(true);
    this.storage.clearCustomerDetails();
    this.storage.clearStripeDetail();
    this.cart = this.cartService.getCart('subscriptions');
    this.getPlanList();
    console.log('this.cart', this.cart)
  }

  getPlanList() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.planList = [];
    // get all plans
    let options = {
      userTypeId: 2
    };
    let params = this.globalService._URLSearchParams(options);
    let groupId = this.storage.getGroupId();
    this.httpService._getList(`${this.appConfig.config.api.trial_plan_group}/${groupId}?` + params.toString()).subscribe(
      data => {
        console.log('data', data);
        console.log('planService 22222', this.planService.groupPlanTrialNew(data));
        let _planGroups = this.planService.groupPlanTrialNew(data);
        this.appConfig.config.groupPlanTrialSkuNew.forEach(element => {
          this.planTypesName.push(element.type);
        });
        this.groupPlanTypes = _planGroups[0]
        // this.planGroups = this.planService.groupPlanTrial(data);
        console.log('this.groupPlanTypes --- ', this.groupPlanTypes);


        // console.log('data', data);
        // console.log('planService', this.planService.groupPlan(data));
        // let groupPlan = this.planService.groupPlan(data);
        // let _groupPlanType = groupPlan.filter(group => group.name === this.groupName);
        // let groupPlanTypeY = groupPlan.filter(group => group.name === 'Y-' + this.groupName);
        // console.log('_groupPlanType', _groupPlanType);
        // console.log('groupPlanTypeY', groupPlanTypeY);
        // if ((_groupPlanType && _groupPlanType.length === 0) || (_groupPlanType && _groupPlanType.length === 0)) {
        //   this.router.navigate(['/blade-type']);
        //   return false;
        // }
        // _groupPlanType[0].item.forEach(plan => {
        //   this.planTypesName.push(plan.PlanType.name)
        //   this.groupPlanTypes[plan.PlanType.name] = [];
        //   this.groupPlanTypes[plan.PlanType.name][0] = plan;
        //   let spl = plan.sku.split(plan.groupName);
        //   let planY = groupPlanTypeY[0].item.filter(plan => plan.sku === (spl[0] + 'Y-' + this.groupName))
        //   console.log('spl', spl[0])
        //   console.log('planY', planY)
        //   this.groupPlanTypes[plan.PlanType.name][1] = planY[0];
        // });
        // console.log('groupPlanTypes', this.groupPlanTypes)
        // console.log('planTypesName', this.planTypesName.sort())


        // this.groupPlanTypes.forEach((key, element) => {
        //   console.log('key', key)
        //   console.log('element', element)
        //   this.planTypesName.push(key)
        // });
        // console.log('planTypesName', this.planTypesName)
        // if (data && data.length > 0) {
        //   // this.planList = data;
        //   data.forEach((plan, index) => {
        //     // push to product list
        //     plan.qty = 0;
        //     let i = -1;
        //     if (this.cart) {
        //       i = this.checkItemExistsInCarts(plan);
        //     }
            
        //     if (i > -1) {
        //       plan.qty = this.cart[i].qty;
        //     }

        //     if(this.planList[plan.PlanType.name]) {
        //       this.planList[plan.PlanType.name].push(plan);
        //     } else {
        //       this.planList[plan.PlanType.name] = [plan];
        //     }

        //     // get productType
        //     this.planTypes = Object.keys(this.planList);
        //   });
        // }
        console.log('planList', this.planList)
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
      }
    )
  }

  // //minus quatity of the item
  // minusQuatity(index, productType) {
  //   if (this.planList[productType][index].qty > 0) {
  //     this.planList[productType][index].qty -= 1;
  //   }
  //   let item = this.planList[productType][index];
  //   let i = this.checkItemExistsInCarts(item);
  //   // this.cart.forEach((_item, _index) => {
  //   //   if (item.id === _item.id) {
  //   //     i = _index;
  //   //     return;
  //   //   }
  //   // });

  //   // has in carts
  //   if ( item && this.planList[productType][index].qty > 0 && i > -1) {     
  //       this.cart[i].qty = this.planList[productType][index].qty;
  //       this.cartService.refreshItem(this.cart);
  //   }

  //   // has in carts then remove item
  //   if ( item && this.planList[productType][index].qty === 0 && i > -1) {
  //       this.cartService.removeItem(this.cart[i]);
  //   }

  // }

  // //plus quatity of the item
  // plusQuatity(index, productType) {
  //   if (this.planList[productType][index].qty < 10000) {
  //     this.planList[productType][index].qty += 1;
  //   }
  //   let item = this.planList[productType][index];
  //   let i = this.checkItemExistsInCarts(item);
  //   // has in carts
  //   if ( item && this.planList[productType][index].qty > 0 && i > -1) {
  //     this.cart[i].qty = this.planList[productType][index].qty;
  //     this.cartService.refreshItem(this.cart);
  //   }

  //   // has not in carts
  //   if (item && this.planList[productType][index].qty > 0 && i === 1) {
  //     this.addToCart(this.planList[productType][index]);
  //   }
  // }

  // add item to cart
  addToCart(_item: any, _planType: any, _event?: any) {
    this.loadingService.display(true);
    let item:any;
    if(_item.planCountry) {
      item = {
        id: _item.id,
        plan: _item,
        qty: 1
      }
    }
    console.log('this.planTypesName', this.planTypesName)
    
    if(this.cart.filter(cartItem => cartItem.id === item.id).length > 0) {
      this.planTypesName.forEach((__planType) => {
        console.log('element', __planType)
        this.groupPlanTypes[__planType].forEach(element => {
           delete element.disableSelect;
        });
      });
      this.groupPlanTypes[_planType].forEach(element => {
        if (element.id !== _item.id) {
          delete element.disableSelect;
        }
      });
      this.cartService.removeItem(item);
      // sync cart item to server
      // this.cartService.updateCart();
      this.cart = this.cartService.getCart();
      this.loadingService.display(false);
      if (_event) {
        let target = _event.target || _event.srcElement;
        $(_event.target).removeClass("selected"); 
        let userCountryType = this.storage.getCurrentUserItem();
        let countryOfOrigin = userCountryType.Country.id;
		let countrylangCode = this.storage.getLanguageCode();
        if (countryOfOrigin !== 8) {
          target.innerHTML = 'SELECT';
        }
        if (countryOfOrigin === 8) {
          if (countrylangCode === "ko") {
            target.innerHTML = '선택';
          } else {
            target.innerHTML = 'SELECT';
          }
        }
      }
    } else {
      this.planTypesName.forEach((__planType) => {
        console.log('element', __planType)
        this.groupPlanTypes[__planType].forEach(element => {
          if (element.id === _item.id) {
            delete element.disableSelect;
          } else {
            element.disableSelect = true;
          }
        });
      });
      // this.groupPlanTypes[_planType].forEach(element => {
      //   if (element.id !== _item.id) {
      //     element.disableSelect = true;
      //   }
      // });
      console.log('this.groupPlanTypes[_planType]', this.groupPlanTypes[_planType])
      this.cartService.addItem(item);
      // sync cart item to server
      this.cartService.updateCart();
      this.cart = this.cartService.getCart();
      this.loadingService.display(false);
      if (_event) {
        let target = _event.target || _event.srcElement;
        $(_event.target).addClass("selected"); 
        let userCountryType = this.storage.getCurrentUserItem();
        let countryOfOrigin = userCountryType.Country.id;
		let countrylangCode = this.storage.getLanguageCode();
        if (countryOfOrigin !== 8) {
          target.innerHTML = 'SELECTED';  
        }
        if (countryOfOrigin === 8) {
          if (countrylangCode === "ko") {
            target.innerHTML = '선택됨';
          }
          else {
            target.innerHTML = 'SELECTED';
          }
        }
      }
    }
  }


  //change quatity of the item
  changeQuatity(index, productType) {
    if (this.planList[productType][index].qty < 0) {
      this.planList[productType][index].qty = 0;
    }
    if (this.planList[productType][index].qty > 10000) {
      this.planList[productType][index].qty = 10000;
    }
    this.cartService.refreshItem(this.planList[productType][index]);
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item);
    this.cart = this.cartService.getCart()
  }

  checkItemExistsInCarts(item) {
    let has = -1;
    this.cart.forEach((_item, _index) => {
      if (item.id == _item.id) {
        has = _index;
      }
    });
    return has;
  }

  nextTo() {
    this.cartService.setStepData(true);
    this.router.navigate(['/checkout/review/subscriptions']);
  }
  

}
