import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormArray } from '@angular/forms';
import { forkJoin } from "rxjs/observable/forkJoin";
import { Observable } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';

import { CartService } from '../../../../core/services/cart.service';
import { CardService } from '../../../../core/services/card.service';
import { Item } from '../../../../core/models/item';
import { DialogService } from '../../../../core/services/dialog.service';

import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../../core/services/global.service';
import { HttpService } from '../../../../core/services/http.service';
import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { DeliveryAddressService } from '../../../../core/services/delivery-address.service';
import { StripeService } from '../../../../core/services/stripe.service';
import { lengthValidator } from '../../../../core/validators/length.validator';

declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-payment-receive',
  templateUrl: './payment-receive.component.html',
  styleUrls: ['./payment-receive.component.scss']
})
export class PaymentReceiveComponent implements OnInit {
  public itemType: string = '';
  public customerDetailsForm: FormGroup;
  private roadshowDetailsForm: FormGroup;
  public cart: Item[] = [];
  public callingCodes: any;
  public deliveryAddressForm;
  public makeBillingDifferentShipping: boolean = false;
  public resError: any;
  public countryStates: Array<any>;
  public paymentMethod: string;
  public cardForm: FormGroup;
  public cardType: string;
  private element: any;
  public Stripe: any;
  public selectedShippingAddress: any;
  public selectedBillingAddress: any;
  public selectedCard: any;
  public emailExist: any;
  public addNewAddress: boolean = true;
  public i18nDialog: any;
  public isValidWord: boolean;
  public currentCountry;
  public receipt = {
    totalPrice: 0,
    totalPriceNotDivided: 0,
    specialDiscount: 0,
    discount: 0,
    subTotal: 0,
    shippingFee: 0,
    totalIclTax: 0
  };
  public isLoading: boolean = true;
  public cartRules: any;
  public currentUser: any;
  public errorMsg: any;
  
  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private cartService: CartService,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private storage: StorageService,
    private globalService: GlobalService,
    private deliveryAddressService: DeliveryAddressService,
    private _http: HttpService,
    private cardService: CardService,
    private stripeService: StripeService,
    private dialogService: DialogService, 
    private translate: TranslateService,) {
    this.currentUser = this.storage.getCurrentUserItem();
    if (this.storage.getStripeError()) {
      this.errorMsg = this.storage.getStripeError();
      if (this.errorMsg) {
        if (this.errorMsg.toUpperCase().search("INSUFFICIENT FUNDS") == -1) {
          this.dialogService.error(this.errorMsg).afterClosed().subscribe(result => {
            this.errorMsg = "";
          })
        } else {
          this.dialogService.error("Unable to process your payment<br>due to insufficient funds.<br>Please try again.").afterClosed().subscribe(result => {
            this.errorMsg = "";
          })

        }

      }
    }
      this.storage.clearStripeError();

    if(this.storage.getCountry().code === 'KOR') {
      this.customerDetailsForm = builder.group({
        email: ['', Validators.required],
        callingCode: [''],
        //contactNumber: ['', lengthValidator({ min: 11 })],
        contactNumber: ['', [Validators.required, lengthValidator({min: 11})]],
        fullName: ['', Validators.required],
      });
    }else if(this.storage.getCountry().code === 'SGP'){
      this.customerDetailsForm = builder.group({
        email: ['', Validators.required],
        callingCode: [''],
       // contactNumber: ['', lengthValidator({ min: 8, max: 8 })],
        contactNumber: ['', [Validators.required, lengthValidator({min: 8, max: 8})]],
        fullName: ['', Validators.required],
      });
    }
    else {
      this.customerDetailsForm = builder.group({
        email: ['', Validators.required],
        callingCode: [''],
       // contactNumber: ['', lengthValidator({ min: 8 })],
        contactNumber: ['', [Validators.required, lengthValidator({min: 8})]],
        fullName: ['', Validators.required],
      });
    }

    this.roadshowDetailsForm = builder.group({
      channelType: ['', Validators.required],
      eventLocationCode: ['', Validators.required]
    });

    this.deliveryAddressForm = this.builder.array([]);
    this.cardForm = this.cardService.initCard();

    this.element = $(el.nativeElement);
    this.Stripe = stripeService.getInstance();
  }

  ngOnInit() {
    this.isValidWord = true;
    this.currentCountry = this.storage.getCountry().code;
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });

    if (!this.cartService.getStepData()) {
      setTimeout(_ => {
        this.router.navigate(['/']);
      });
    } else {
      // this.paymentMethod = 'cash';
      this.paymentMethod = this.storage.getStripeDetail() ? 'stripe' : 'cash';
      this.route.params.subscribe(params => {
        if (params.type && (params.type === 'alacartes' || params.type === 'subscriptions')) {
          this.itemType = params.type;
        } else {
          this.router.navigate(['/']);
        }
      });

      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.cart = this.cartService.getCart('alacartes');
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        if (!this.currentUser.Country.isBaStripe) {
          this.router.navigate(['/']);
        }
        this.cart = this.cartService.getCart('subscriptions');
        this.paymentMethod = 'stripe';
      }

      if (!this.cart || (this.cart && this.cart.length < 1)) {
        this.router.navigate(['/' + this.itemType]);
      } else {
        this.getCartRules();
      }

      this.makeBillingDifferentShipping = false;
      this.callingCodes = this.globalService.getCountryCodes();
      if (this.storage.getCurrentUserItem()) {
        this.customerDetailsForm.patchValue({
          // email: this.storage.getCurrentUserItem().email,
          fullName: '',
          callingCode: '+' + this.storage.getCurrentUserItem().Country.callingCode
        });
      }

      this.checkCartItemsExists();

      if (this.itemType === 'subscriptions') {
        this.initDeliveryAddress();
        jQuery(function ($) {
          setTimeout(() => {
            jQuery('[data-stripe="number"]').payment('formatCardNumber');
            jQuery('[data-stripe="exp"]').payment('formatCardExpiry');
            jQuery('[data-stripe="cvc"]').payment('formatCardCVC');
          });
        });
        this.getCountryStates();
      }


      // this.deliveryAddressForm.controls[0].valueChanges.subscribe(() => {
      //   this.resError = null;
      //   alert()
      // });

      this.cardForm.controls.cardNumber.valueChanges.subscribe(data => {
        if (data) {
          this.changeCardNumber();
        } else {
          this.cardType = '';
        }
      })
    }

    let customerDetails = this.storage.getCustomerDetails();
    if(customerDetails && customerDetails.customerDetailsForm.email) {
      this.customerDetailsForm.patchValue({
        email: customerDetails.customerDetailsForm.email,
        fullName: customerDetails.customerDetailsForm.fullName,
        callingCode: customerDetails.customerDetailsForm.callingCode,
        contactNumber: customerDetails.customerDetailsForm.contactNumber
      });
    }
    if(customerDetails && customerDetails.deliveryAddressForm[0]) {
      this.deliveryAddressForm = this.builder.array([]);
      this.initDeliveryAddress(customerDetails.deliveryAddressForm[0]);
      if(customerDetails.deliveryAddressForm[1]) {
        this.makeBillingDifferentShipping = true;
        this.initDeliveryAddress(customerDetails.deliveryAddressForm[1]);
      }
    }
    if(customerDetails && customerDetails.cardForm.cardNumber) {
      this.paymentMethod = 'stripe';
      this.cardForm.patchValue({
        cardName: customerDetails.cardForm.cardName,
        cardNumber: customerDetails.cardForm.cardNumber,
        expiry: customerDetails.cardForm.expiry,
        cvc: customerDetails.cardForm.cvc,
        cardType: customerDetails.cardForm.cardType
      });
    }
  }
  getCartRules() {
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        this.receipt = this.cartService.getReceipt(this.cartRules, this.itemType);
        console.log('this.receipt', this.receipt)
      }
    )
  }

  initDeliveryAddress(value?: any) {
    let control = <FormArray>this.deliveryAddressForm;
    let initAddressCtrl;
    if(this.storage.getCountry().code === 'SGP') {
      initAddressCtrl = this.deliveryAddressService.initAddressSGP();
    }else{
      initAddressCtrl = this.deliveryAddressService.initAddress();
    }
   
    if(!value) {
      initAddressCtrl.patchValue({ callingCode: '+' + this.storage.getCountry().callingCode });
      if (this.storage.getCurrentUserItem()) {
        // initAddressCtrl.patchValue({firstName: this.storage.getCurrentUserItem().firstName, lastName: this.storage.getCurrentUserItem().lastName});
      }
      if (this.countryStates && this.countryStates.length > 0) {
        if (this.storage.getCountry().code === 'MYS') {
          initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name });
        }
        if (this.storage.getCountry().code === 'SGP') {
          initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Singapore')[0].name });
        }
      }
      control.push(initAddressCtrl);
    } else {
      initAddressCtrl.patchValue({
        address: value.address,
        state: value.state,
        city: value.city,
        portalCode: value.portalCode
      });
      control.push(initAddressCtrl);
    }
  }

  // get country states
  getCountryStates() {
    if (!this.countryStates || !this.storage.getCurrentUserItem()) {
      this._http._getList(`${this.appConfig.config.api.countries}/states`).subscribe(
        data => {
          this.countryStates = data;
          if (this.countryStates && this.countryStates.length > 0) {
            // if (this.storage.getCountry().code === 'MYS') {
            //   this.deliveryAddressForm.controls[0].controls.state.setValue(this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name);
            // }
            // if (this.storage.getCountry().code === 'SGP') {
            //   this.deliveryAddressForm.controls[0].controls.state.setValue(this.countryStates.filter(country => country.name == 'Singapore')[0].name);
            // }
            let state = this.countryStates.filter(state => state.isDefault)[0];
            state = state ? state : this.countryStates[0];
            this.deliveryAddressForm.controls[0].controls.state.setValue(state.name);
          }
        }, error => {
          console.log(error.error)
        }
      )
    }
  }

  checkCartItemsExists() {
    if (this.cart && this.cart.length < 1) {
      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.router.navigate(['/alacartes']);
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        this.router.navigate(['/subscriptions']);
      }

    }
  }

  addressChange(event) {
    if (!event.target.checked) {
      this.makeBillingDifferentShipping = false;
      let control = <FormArray>this.deliveryAddressForm;
      control.removeAt(1);
      if (this.selectedBillingAddress && this.selectedBillingAddress.id) {
        this.selectedBillingAddress = null;
      }
      // this.initDeliveryAddress();
    } else {
      this.makeBillingDifferentShipping = true;
      // let control = <FormArray>this.deliveryAddressForm;
      // control.removeAt(1);
      this.initDeliveryAddress();
    }
  }

  changePaymentMethod(_type: string) {
    this.paymentMethod = _type;
    if (_type === 'stripe') {
      jQuery(function ($) {
        jQuery('[data-stripe="number"]').payment('formatCardNumber');
        jQuery('[data-stripe="exp"]').payment('formatCardExpiry');
        jQuery('[data-stripe="cvc"]').payment('formatCardCVC');
      });
    }
  }
  // onchange card number
  changeCardNumber() {
    this.cardType = this.globalService.getCardType(this.cardForm.controls['cardNumber'].value).toUpperCase();
  }

  onChangeEmail() {
    if (this.itemType === 'subscriptions') {
      let email = this.customerDetailsForm.value.email;
      let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
      if(email.includes('unavailable') || email.includes('notavailable') || email.includes('inavailable')){
        this.isValidWord = false;
        this.customerDetailsForm.value['email'] = '';
      }else{
        this.isValidWord = true;
      }
      if (re.test(email)) {
        this.checkEmail(email).then(user => {
          console.log('user', user);
          if (user && user['data'] && user['data'].deliveryAddress) {
            console.log("user['data'].billingAddress", user['data'].billingAddress)

            // if shipping address same billing address
            if (user['data'] && user['data'].billingAddress && user['data'].deliveryAddress &&
              user['data'].billingAddress.id === user['data'].deliveryAddress.id) {
              this.deliveryAddressForm = this.builder.array([]);
              this.initDeliveryAddress();
              this.deliveryAddressForm.controls[0].patchValue(user['data'].deliveryAddress)
              this.selectedBillingAddress = this.selectedShippingAddress = user['data'].deliveryAddress;
            }
            // if shipping address difference billing address
            if (user['data'] && user['data'].billingAddress && user['data'].deliveryAddress &&
              user['data'].billingAddress.id !== user['data'].deliveryAddress.id) {
              this.deliveryAddressForm = this.builder.array([]);
              this.initDeliveryAddress();
              this.deliveryAddressForm.controls[0].patchValue(user['data'].deliveryAddress);
              this.initDeliveryAddress();
              this.deliveryAddressForm.controls[1].patchValue(user['data'].billingAddress);
              this.makeBillingDifferentShipping = true;
              this.selectedBillingAddress = user['data'].billingAddress;
              this.selectedShippingAddress = user['data'].deliveryAddress;
            }

            if (user['data'].subscriptions && user['data'].subscriptions.length > 0) {
              let checkIsTrialSub : boolean = false;
              user['data'].subscriptions.forEach(item => {
                if (item.isTrial && item.status !== "Canceled") {
                  checkIsTrialSub = true;
                }
              });
              if (checkIsTrialSub) {
                this.dialogService.result(this.i18nDialog.confirm.notDiscountForIsTrial).afterClosed().subscribe((result => {
                  if (result) {
                    this.router.navigate(['/']);
                  }
                }));
              }
            }
          } else {
            this.deliveryAddressForm = this.builder.array([]);
            this.initDeliveryAddress();
            this.selectedBillingAddress = this.selectedShippingAddress = null;
          }
          console.log('this.deliveryAddressForm.controls', this.deliveryAddressForm.controls)
          this.deliveryAddressForm.controls[0].valueChanges.subscribe(() => {
            this.resError = null;
            this.selectedShippingAddress = null;
          });
          if (this.deliveryAddressForm.controls.length > 1) {
            this.deliveryAddressForm.controls[1].valueChanges.subscribe(() => {
              this.resError = null;
              this.selectedBillingAddress = null;
            });
          }

        });
      } else {
        this.deliveryAddressForm = this.builder.array([]);
        this.initDeliveryAddress();
        this.selectedBillingAddress = this.selectedShippingAddress = null;
      }
    }
    if(this.itemType === 'alacartes') {
      let email_alacart = this.customerDetailsForm.value.email;
      if(email_alacart.includes('unavailable') || email_alacart.includes('notavailable') || email_alacart.includes('inavailable')){
        this.isValidWord = false;
        this.customerDetailsForm.value['email'] = '';
      }else{
        this.isValidWord = true;
      }
    }

  }

  checkEmail(_email: string) {
    return new Promise((resolved, reject) => {
      // check email is exist or not
      this._http._create(this.appConfig.config.api.checkEmail, { email: _email })
        .subscribe(res => {
          if (res && res.user) {
            if (res.user.isActive) {
              // todo
            } else {
              // todo
            }
          } else {
            // todo
          }
          resolved(res);
        },
        error => {
          reject(error);
        });
    });
  }

  // create stripe token
  createToken() {
    let $form = this.element.find('#cardForm');
    return new Promise((resolve, reject) => {
      this.Stripe.card.createToken($form, (status, response) => {
        if (response.error) {
          this.resError = {
            type: 'error',
            message: response.error.message
          }
          this.loadingService.display(false);
          // this.alertService.error([{'ok':false, 'message': response.error.message}]);
          reject(response.error.message);
        } else {
          this.resError = null;
          resolve(response.id);
        }
      });
    });
  }
  
  // create stripe customer
  createCustomer(stripeToken) {
    if (this.storage.getCurrentUserItem()) { // For case signed in
      return new Promise((resolve, reject) => {
        this._http._create(`${this.appConfig.config.api.cards}`.replace('[sellerID]', this.storage.getCurrentUserItem().id), { stripeToken, email: this.customerDetailsForm.value.email }).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.resError = {
              type: 'error',
              message: typeof error === 'string' ? error : JSON.parse(error).message
            };
            this.loadingService.display(false);
            reject(error);
          }
        );
      });
    } else { // do not login
      this.router.navigate(['login']);
    }
  }

  gotoRoadshow() {
    this.loadingService.display(true);
    let customerDetails = {
      customerDetailsForm: {},
      deliveryAddressForm: {},
      cardForm: {}
    };

    if (this.itemType && this.customerDetailsForm.valid) {
      if (this.itemType === 'alacartes' && this.paymentMethod === 'cash') {
        customerDetails.customerDetailsForm = this.customerDetailsForm.value;
        this.storage.setCustomerDetails(customerDetails);
        this.loadingService.display(false);
        if(this.isValidWord){
          this.router.navigate([`/checkout/payment/${this.itemType}/roadshow`]);
        }
      } else {
        this.createToken()
          .then(stripeToken => this.createCustomer(stripeToken))
          .then(customer => {
            this.storage.setStripeDetail(customer);
        
            if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes
              if (this.paymentMethod === 'stripe' && this.cardForm.valid && this.customerDetailsForm.valid) {
                if(this.cardForm.value.expiry.length > 7) {
                  this.cardForm.value.expiry = this.cardForm.value.expiry.substring(0, this.cardForm.value.expiry.length - 1);
                }
                customerDetails.customerDetailsForm = this.customerDetailsForm.value;
                customerDetails.cardForm = this.cardForm.value;
                this.storage.setCustomerDetails(customerDetails);
                if(this.isValidWord){
                  this.router.navigate([`/checkout/payment/${this.itemType}/roadshow`]);
                }
              }
            }
            if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions
              if (this.paymentMethod === 'stripe' && this.cardForm.valid && this.customerDetailsForm.valid && this.deliveryAddressForm.valid) {
                if(this.cardForm.value.expiry.length > 7) {
                  this.cardForm.value.expiry = this.cardForm.value.expiry.substring(0, this.cardForm.value.expiry.length - 1);
                }
                customerDetails.customerDetailsForm = this.customerDetailsForm.value;
                customerDetails.cardForm = this.cardForm.value;
                customerDetails.deliveryAddressForm = this.deliveryAddressForm.value;
                this.storage.setCustomerDetails(customerDetails);
                if(this.isValidWord){
                  this.router.navigate([`/checkout/payment/${this.itemType}/roadshow`]);
                }
              }
            }
            this.loadingService.display(false);
          })
          .catch(error => {
            console.log(error, typeof error);
            this.loadingService.display(false);
            this.resError = {
              type: 'error',
              message: typeof error === 'string' ? JSON.parse(error).message : error.message
            };
          });
      }
    } else {
      this.loadingService.display(false);
    }
  }

}
