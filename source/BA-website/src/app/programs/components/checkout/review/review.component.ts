import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { CartService } from '../../../../core/services/cart.service';
import { Item } from '../../../../core/models/item';

import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../../core/services/global.service';
import { HttpService } from '../../../../core/services/http.service';
import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { CountriesService } from '../../../../core/services/countries.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';
declare var jQuery: any;

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  public itemType: string = '';
  public cart: Item[] = [];
  public productCart: Item[] = [];
  public planCart: Item[] = [];
  public isTrial: boolean = false;
  public receipt = {
    totalPrice: 0,
    totalPriceNotDivided: 0,
    specialDiscount: 0,
    discount: 0,
    subTotal: 0,
    shippingFee: 0,
    totalIclTax: 0,
    applyDiscount: 0
  };
  public promoCode: string;
  public checkPromoCodeApplied: boolean = false;
  public promoCodeError: any;
  public notAllowPromoCode: boolean = false;
  private apiURL: string;
  public isLoading: boolean = true;
  public cartRules: any;
  private promoCodeForm: FormGroup;
  public currentDate: any;
  private currentUser: any;
  public directTrial: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    public cartService: CartService,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    public userService: UsersService,
    private builder: FormBuilder,
    private storage: StorageService,
    public countriesService: CountriesService
    ) {
      this.promoCodeForm = builder.group({
        promoCode: ['', Validators.required],
      });

      cartService.hasUpdate.subscribe(hasUpdate => {
        this.cart = this.cartService.getCart(this.itemType);
        this.receipt = this.cartService.getReceipt(this.cartRules, this.itemType);
        
        this.notAllowPromoCode = false;
        if(this.receipt.specialDiscount) {
          this.notAllowPromoCode = true;
          // this.promoCode = null;
          this.promoCodeForm.controls.promoCode.setValue(null);
        }
        
        let promoCode = this.cartService.getPromoCode();
        if(promoCode.id && this.itemType === 'alacartes') {
          if((promoCode.minSpend && this.receipt.totalPrice < promoCode.minSpend && promoCode.allowMinSpendForTotalAmount)
          || (promoCode.minSpend && this.receipt.applyDiscount < promoCode.minSpend && !promoCode.allowMinSpendForTotalAmount)) {
            // this.cartService.setPromotion();
            this.promoCodeError = {
              type: 'error',
              message: 'Your total price must greater than ' + promoCode.minSpend
            };
          } else {
            this.promoCodeError = null;
          }
        }
      });
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser || !this.currentUser.id) {
        console.log('User is not exist');
        return;
      }

    }

  ngOnInit() {
   this.reloadPage();
    this.countriesService.language.subscribe(
      data => {
        this.reloadPage();
      }
    )
  }

  reloadPage() {
    this.storage.clearCustomerDetails();
    this.storage.clearStripeDetail();
    console.log('setStepData', this.cartService.getStepData());
    if(!this.cartService.getStepData()) {
      setTimeout(_ => {
        this.router.navigate(['/']);
      });
    } else {
      this.currentDate = new Date();
      this.route.params.subscribe(params => {
        if(params.type && (params.type === 'alacartes' || params.type === 'subscriptions')) {
          this.itemType = params.type;
        } else {
          this.router.navigate(['/']);
        }
      });

      if (!(this.cartService.getPromoCode() && this.cartService.getPromoCode().id)) {
        // if (this.storage.getPromoCode()) {
        //   this.promoCodeForm.controls.promoCode.setValue(this.storage.getPromoCode());
        // }
      } else {
        this.promoCodeForm.controls.promoCode.setValue(this.cartService.getPromoCode().promotionCodes[0].code);
      }

      // this.checkPromoCodeApplied = false;
      // if(this.cartService.getPromoCode().id) {
      //   this.promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
      //   this.checkPromoCodeApplied = true;
      // }
      
      if (this.promoCodeForm.value.promoCode) {
        this.applyPromoCode(this.promoCodeForm.value.promoCode);
        this.checkPromoCodeApplied = true;
      }
      
      this.productCart = [];
      this.planCart = [];
      this.cart = this.cartService.getCart();
      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.getProductCart();
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        this.getPlanCart();
      }

      this.directTrial = this.cart[0].directTrial;

      this.checkCartItemsExists();
      if (this.cart && this.cart.length > 0) {
        this.isLoading = true;
        this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
          data => {
            this.cartRules = data;
            this.receipt = this.cartService.getReceipt(this.cartRules, this.itemType);
            if(this.receipt.specialDiscount) {
              this.notAllowPromoCode = true;
              // this.promoCode = null;
              this.promoCodeForm.controls.promoCode.setValue(null);
            }
            this.isLoading = false;
            console.log('this.receipt', this.receipt)
          }
        )

        this.getCartRules();
        this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
          if(value === '') {
            this.cartService.setPromotion();
            this.storage.setPromoCode('');
          }
        })
      }
    }
  }
  
  checkCartItemsExists() {
    if (this.cart && this.cart.length < 1) {
      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.router.navigate(['/alacartes']);
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        this.router.navigate(['/subscriptions']);
      }
  
    }
  }

  getCartRules() {
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        this.receipt = this.cartService.getReceipt(this.cartRules, this.itemType);
      },
      error => {
        this.loadingService.display(false);
        if(error.status === 401) {
          this.cartService.clearCart();
          this.storage.clearCustomerDetails();
          this.storage.clearStripeDetail();
          this.userService.logout();
          this.router.navigate(['/login']);
        }
      }
    )
  }

  getProductCart() {
    if (this.cart) {
      this.productCart = this.cart.filter(item => item.product);
    }
    console.log('this.productCart', this.productCart);
  }

  getPlanCart() {
    if (this.cart) {
      this.planCart = this.cart.filter(item => item.plan);
      this.planCart.forEach(plan => {
        let currDate = new Date();
        plan['first_delivery'] = currDate.setDate(currDate.getDate() + 13);
        plan['delivery_schedules'] = [];
        let i = 0;
        for (var index = 0; index < plan.plan['PlanType'].totalDeliverTimes; index++) {
          plan['delivery_schedules'].push(i);
          i += 12/plan.plan['PlanType'].totalDeliverTimes;
        }

        if(plan.plan.isTrial) {
          this.isTrial = true;
        }
      });
      console.log('this.planCart', this.planCart)
    }
  }

  applyPromoCode(_promoCode: string) {
    this.storage.setPromoCode(_promoCode);
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let dataObj = {};
    if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes
      let productCountryIds = [];
      this.cart.forEach(cartItem => {
        productCountryIds.push(cartItem.product.productCountry.id);
      })
      dataObj = { promotionCode: _promoCode,
        productCountryIds: productCountryIds}
    }
    if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
      // this.getPlanCart();
      let planCountryIds = [];
      this.cart.forEach(cartItem => {
        if(cartItem.plan.planCountry[0]) {
          planCountryIds.push(cartItem.plan.planCountry[0].id);
        } else {
          planCountryIds.push(cartItem.plan.planCountry.id);
        }
      })
      dataObj = { promotionCode: _promoCode,
        planCountryIds: planCountryIds}
    }

    // let productCountryIds = [];
    // this.cart.forEach(cartItem => {
    //   productCountryIds.push(cartItem.product.productCountry.id);
    // })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, dataObj).subscribe(
      data => {
        this.cartService.setPromotion(data);
        console.log('data', data)
        if(data.minSpend && this.receipt.totalPrice < data.minSpend && this.itemType === 'alacartes') {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: 'Your total price must greater than ' + data.minSpend
          };
        }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        console.log(error)
      }
    )
  }

  nextStep() {
    if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
      this.router.navigate(['checkout/payment/alacartes']);
    }
    if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
      this.router.navigate(['checkout/payment/subscriptions']);
    }
    console.log('directTrial', this.directTrial);

    this.cart.forEach(item => {
      item.directTrial = this.directTrial;
    });

    this.storage.setCartItem(this.cart);
  }

}
