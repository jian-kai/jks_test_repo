import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { CartService } from '../../../../core/services/cart.service';
import { CardService } from '../../../../core/services/card.service';
import { Item } from '../../../../core/models/item';

import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../../core/services/global.service';
import { HttpService } from '../../../../core/services/http.service';
import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { DeliveryAddressService } from '../../../../core/services/delivery-address.service';
import { LocaleDateStringPipe } from '../../../../core/pipes/locale-date-string.pipe';
declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  providers: [LocaleDateStringPipe]
})
export class SummaryComponent implements OnInit {
  public itemType: string = '';
  public cart: Item[] = [];
  public callingCodes: any;
  public deliveryAddressForm;
  public makeBillingAsShipping: boolean = false;
  public resError: any;
  public countryStates: Array<any>;
  public paymentMethod: string;
  public cardType: string;
  private element: any;
  public Stripe: any;
  public selectedShippingAddress: any;
  public selectedBillingAddress: any;
  public selectedCard: any;
  public isLoading: boolean;
  public currentUser: any;
  public orderId: string;
  public orderInfo: any;
  public deliverDate1: any;
  public deliverDate2: any;

  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private cartService: CartService,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private storage: StorageService,
    private globalService: GlobalService,
    private deliveryAddressService: DeliveryAddressService,
    private _http: HttpService,
    private cardService: CardService) { }

  ngOnInit() {
    if (this.itemType && this.itemType === 'alacartes' ) {
      this.itemType = 'alacartes';
    }

    if (this.itemType && this.itemType === 'subscriptions' ) {
      this.itemType = 'subscriptions';
    }
    this.currentUser = this.storage.getCurrentUserItem();
    this.route.params.subscribe(params => {
      if(params.type && (params.type === 'alacartes' || params.type === 'subscriptions') && params.orderId) {
        this.itemType = params.type;
        this.orderId = params.orderId;
        this.getOrderDetail();
      } else {
        this.router.navigate(['/']);
      }
    });

    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })

  }

   getOrderDetail() {
    this.isLoading = true;
    if(this.currentUser) {
      this.httpService._getDetail(`${this.appConfig.config.api.order}/${this.orderId}`.replace('[sellerID]', this.currentUser.id)).subscribe(
        data => {
          if(data && Object.keys(data).length > 0) {
            this.orderInfo = data;
            this.orderInfo.Receipt.total_tax = parseFloat(this.orderInfo.Receipt.subTotalPrice) + parseFloat(this.orderInfo.Receipt.shippingFee);
            console.log(' this.orderInfo', this.orderInfo);
            if(!this.orderInfo.Subscriptions[0].startDeliverDate) {
              let currDate = new Date(this.orderInfo.Subscriptions[0].createdAt);
              this.deliverDate1 = currDate.setDate(currDate.getDate() + 13);
              this.deliverDate2 = currDate.setDate(currDate.getDate() + 3);
            }
            
            this.loadingService.display(false);
          } else {
            this.loadingService.display(false);
            this.router.navigate(['/']);
          }
        },
        error => {
          console.log(error.error);
        }
      )
    } else {
      this.router.navigate(['/login']);
    }
  }

  back() {
    this.router.navigate(['/']);
    // if (this.itemType && this.itemType === 'alacartes' ) {
    //   this.router.navigate(['/alacartes']);
    // }

    // if (this.itemType && this.itemType === 'subscriptions' ) {
    //   this.router.navigate(['/subscriptions']);
    // }
  }

}
