import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadshowDetailComponent } from './roadshow-detail.component';

describe('RoadshowDetailComponent', () => {
  let component: RoadshowDetailComponent;
  let fixture: ComponentFixture<RoadshowDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadshowDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadshowDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
