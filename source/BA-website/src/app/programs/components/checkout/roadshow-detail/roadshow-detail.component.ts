import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormArray, FormControl } from '@angular/forms';
import { forkJoin } from "rxjs/observable/forkJoin";
import { Observable } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';

import { CartService } from '../../../../core/services/cart.service';
import { CardService } from '../../../../core/services/card.service';
import { Item } from '../../../../core/models/item';
import { DialogService } from '../../../../core/services/dialog.service';

import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../../core/services/global.service';
import { HttpService } from '../../../../core/services/http.service';
import { StorageService } from '../../../../core/services/storage.service';
import { UsersService } from '../../../../auth/services/users.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { DeliveryAddressService } from '../../../../core/services/delivery-address.service';
import { StripeService } from '../../../../core/services/stripe.service';
import { lengthValidator } from '../../../../core/validators/length.validator';
import { valueValidator } from '../../../../core/validators/value.validator';

declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-roadshow-detail',
  templateUrl: './roadshow-detail.component.html',
  styleUrls: ['./roadshow-detail.component.scss']
})
export class RoadshowDetailComponent implements OnInit {
  public itemType: string = '';
  private customerDetailsForm: FormGroup;
  public roadshowDetailsForm: FormGroup;
  public cart: Item[] = [];
  public callingCodes: any;
  public deliveryAddressForm;
  public makeBillingDifferentShipping: boolean = false;
  public resError: any;
  public countryStates: Array<any>;
  public paymentMethod: string;
  public cardForm: FormGroup;
  public cardType: string;
  private element: any;
  public Stripe: any;
  public selectedShippingAddress: any;
  public selectedBillingAddress: any;
  public selectedCard: any;
  public emailExist: any;
  public i18nDialog: any;
  public isLoading: boolean = true;
  public currentUser: any;
  public currentCountry;

  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private cartService: CartService,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private storage: StorageService,
    private globalService: GlobalService,
    private deliveryAddressService: DeliveryAddressService,
    private _http: HttpService,
    private cardService: CardService,
    public userService: UsersService,
    private stripeService: StripeService,
    private dialogService: DialogService, 
    private translate: TranslateService,) {
    this.currentUser = this.storage.getCurrentUserItem();

    if(this.storage.getCountry().code === 'KOR') {
      this.customerDetailsForm = builder.group({
        email: ['', Validators.required],
        callingCode: [''],
        contactNumber: ['', lengthValidator({ min: 11 })],
        fullName: ['', Validators.required],
      });
    } else {
      this.customerDetailsForm = builder.group({
        email: ['', Validators.required],
        callingCode: [''],
        contactNumber: ['', lengthValidator({ min: 8 })],
        fullName: ['', Validators.required],
      });
    }

    this.roadshowDetailsForm = builder.group({
      channelType: ['', Validators.required],
      eventLocationCode: ['', Validators.required]
    });

    this.deliveryAddressForm = this.builder.array([]);
    this.cardForm = this.cardService.initCard();

    this.element = $(el.nativeElement);
    this.Stripe = stripeService.getInstance();
  }

  ngOnInit() {
    this.currentCountry = this.storage.getCountry().code;
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });

    if (!this.cartService.getStepData()) {
      setTimeout(_ => {
        this.router.navigate(['/']);
      });
    } else {
      this.paymentMethod = this.storage.getStripeDetail() ? 'stripe' : 'cash';
      this.route.params.subscribe(params => {
        if (params.type && (params.type === 'alacartes' || params.type === 'subscriptions')) {
          this.itemType = params.type;
        } else {
          this.router.navigate(['/']);
        }
      });

      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.cart = this.cartService.getCart('alacartes');
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        if (!this.currentUser.Country.isBaStripe) {
          this.router.navigate(['/']);
        }
        this.cart = this.cartService.getCart('subscriptions');
        this.paymentMethod = 'stripe';
      }

      if (!this.cart || (this.cart && this.cart.length < 1)) {
        this.router.navigate(['/' + this.itemType]);
      }

      this.callingCodes = this.globalService.getCountryCodes();

      this.checkCartItemsExists();
    }

    let customerDetails = this.storage.getCustomerDetails();
    if(customerDetails && customerDetails.customerDetailsForm) {
      this.customerDetailsForm.patchValue({
        email: customerDetails.customerDetailsForm.email,
        fullName: customerDetails.customerDetailsForm.fullName,
        callingCode: customerDetails.customerDetailsForm.callingCode,
        contactNumber: customerDetails.customerDetailsForm.contactNumber
      });
    }
    if(customerDetails && customerDetails.deliveryAddressForm) {
      this.deliveryAddressForm = this.builder.array([]);
      this.initDeliveryAddress(customerDetails.deliveryAddressForm[0]);
      if(customerDetails.deliveryAddressForm[1]) {
        this.makeBillingDifferentShipping = true;
        this.initDeliveryAddress(customerDetails.deliveryAddressForm[1]);
      }
    }
    if(customerDetails && customerDetails.cardForm) {
      this.cardForm.patchValue({
        cardName: customerDetails.cardForm.cardName,
        cardNumber: customerDetails.cardForm.cardNumber,
        expiry: customerDetails.cardForm.expiry,
        cvc: customerDetails.cardForm.cvc,
        cardType: customerDetails.cardForm.cardType
      });
    }
  }

  initDeliveryAddress(value?: any) {
    let control = <FormArray>this.deliveryAddressForm;
    let initAddressCtrl = this.deliveryAddressService.initAddress();
    if(!value) {
      initAddressCtrl.patchValue({ callingCode: '+' + this.storage.getCountry().callingCode });
      if (this.storage.getCurrentUserItem()) {
        // initAddressCtrl.patchValue({firstName: this.storage.getCurrentUserItem().firstName, lastName: this.storage.getCurrentUserItem().lastName});
      }
      if (this.countryStates && this.countryStates.length > 0) {
        if (this.storage.getCountry().code === 'MYS') {
          initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Kuala Lumpur')[0].name });
        }
        if (this.storage.getCountry().code === 'SGP') {
          initAddressCtrl.patchValue({ state: this.countryStates.filter(country => country.name == 'Singapore')[0].name });
        }
      }
      control.push(initAddressCtrl);
    } else {
      initAddressCtrl.patchValue({
        address: value.address,
        state: value.state,
        city: value.city,
        portalCode: value.portalCode
      });
      control.push(initAddressCtrl);
    }
  }

  checkCartItemsExists() {
    if (this.cart && this.cart.length < 1) {
      if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
        this.router.navigate(['/alacartes']);
      }
      if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions 
        this.router.navigate(['/subscriptions']);
      }

    }
  }

  // submit Dredit/Debit card
  submitCard(_address?: any) {
    this.loadingService.display(true);

    if (_address) {
      if (_address[1]) {
        this.selectedShippingAddress = _address[0].data.deliveryAddress;
        this.selectedBillingAddress = _address[1].data.deliveryAddress;
      } else {
        this.selectedShippingAddress = this.selectedBillingAddress = _address[0].data.deliveryAddress;
      }
      console.log('address', _address);
    }

    return new Promise((resolve, reject) => {
      // this.createToken()
      //   .then(stripeToken => this.createCustomer(stripeToken))
      //   .then(customer => {
      //     this.loadingService.display(false);
      //     resolve(customer);
      //   },
      //   error => {
      //     this.loadingService.display(false);
      //     this.resError = {
      //       type: 'error',
      //       message: typeof error === 'string' ? error : JSON.parse(error).message
      //     };
      //     reject(error);
      //   });
      resolve(this.storage.getStripeDetail());
    });
  }

  // create stripe token
  createToken() {
    let $form = this.element.find('#cardForm');
    return new Promise((resolve, reject) => {
      this.Stripe.card.createToken($form, (status, response) => {
        if (response.error) {
          this.resError = {
            type: 'error',
            message: response.error.message
          }
          this.loadingService.display(false);
          // this.alertService.error([{'ok':false, 'message': response.error.message}]);
          reject(response.error.message);
        } else {
          this.resError = null;
          resolve(response.id);
        }
      });
    });
  }

  // create stripe customer
  createCustomer(stripeToken) {
    if (this.storage.getCurrentUserItem()) { // For case signed in
      return new Promise((resolve, reject) => {
        this._http._create(`${this.appConfig.config.api.cards}`.replace('[sellerID]', this.storage.getCurrentUserItem().id), { stripeToken, email: this.customerDetailsForm.value.email }).subscribe(
          data => {
            resolve(data);
          },
          error => {
            this.resError = {
              type: 'error',
              message: typeof error === 'string' ? JSON.parse(error).message : error.message
            };
            this.loadingService.display(false);
            reject(error);
          }
        );
      });
    } else { // do not login
      this.router.navigate(['login']);
    }
  }

  // do add new address
  doAddDeliveryAddress(data) {
    let curUser = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    if (data.length) {
      data[0].CountryId = this.storage.getCountry().id;
      // data[0].contactNumber = data[0].callingCode + ' ' + data[0].contactNumber;
      data[0].fullname = this.customerDetailsForm.value.fullName;
      data[0].email = this.customerDetailsForm.value.email;
      delete data[0]['id'];
      if (data.length > 1) {
        data[1].CountryId = this.storage.getCountry().id;
        // data[1].contactNumber = data[1].callingCode + ' ' + data[1].contactNumber;
        data[1].fullname = this.customerDetailsForm.value.fullName;
        data[1].email = this.customerDetailsForm.value.email;
        delete data[1]['id'];
      }
    }

    if (curUser) {

      return new Promise((resolved, reject) => {
        return Observable.forkJoin(
          data.map(
            address => this._http._post(`${this.appConfig.config.api.delivery_address}`.replace('[sellerID]', curUser.id), address)
          )
        ).subscribe(
          data => {
            resolved(data)
          },
          err => {
            reject(err);
          }
          );
      });
    } else { // do not login
      this.router.navigate(['login']);
    }
  }

  nextTo() {
    this.loadingService.display(true);
    this.resError = null;
    console.log('asdad ' + this.paymentMethod);
    if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes
      if (this.paymentMethod === 'stripe' && this.cardForm.valid && this.customerDetailsForm.valid && this.roadshowDetailsForm.valid) {
        // this.submitCard().then(card => {
        //   this.selectedCard;
        //   this.payNow(card);
        // }, error => {
        //   this.loadingService.display(false);
        //   this.resError = {
        //     type: 'error',
        //     message: typeof error === 'string' ? error : JSON.parse(error).message
        //   };
        // })
        this.actionPayWithoutCreateAddress();
      } else if (this.paymentMethod === 'cash' && this.customerDetailsForm.valid && this.roadshowDetailsForm.valid) {
        this.payNow();
      } else {
        this.loadingService.display(false);
      }
    } else if (this.itemType && this.itemType === 'subscriptions') { // if it is subscriptions
      if (this.paymentMethod === 'stripe' && this.cardForm.valid && this.customerDetailsForm.valid && this.deliveryAddressForm.valid && this.roadshowDetailsForm.valid) {
        this.cart = this.cartService.getCart('subscriptions');
        if (this.selectedShippingAddress && this.selectedBillingAddress && this.deliveryAddressForm) {
          this.actionPayWithoutCreateAddress();
        } else {
          this.actionPayWithCreateAddress();
        }

      } else {
        this.loadingService.display(false);
      }
    } else {
      this.loadingService.display(false);
    }
  }

  actionPayWithoutCreateAddress() {
    this.submitCard().then(card => {
      this.selectedCard;
      this.payNow(card);
    }, error => {
      this.loadingService.display(false);
      this.resError = {
        type: 'error',
        message: typeof error === 'string' ? JSON.parse(error).message : error.message
      };
    })
  }

  actionPayWithCreateAddress() {
    this.doAddDeliveryAddress(this.deliveryAddressForm.value).then(address => this.submitCard(address)).then(card => {
      console.log('card', card)
      if (card) {
        this.selectedCard;
        this.payNow(card);
      }
    }, error => {
      this.loadingService.display(false);
      this.resError = {
        type: 'error',
        message: typeof error === 'string' ? JSON.parse(error).message : error.message
      };
    })
  }

  payNow(_card?: any) {
    this.loadingService.display(true);
    let email = this.customerDetailsForm.value.email;
    this.checkEmail(email).then(user => {
      console.log('user ======== ',user);
      if (user && user['data'] && user['data'].subscriptions && user['data'].subscriptions.length > 0 && this.itemType && this.itemType === 'subscriptions') {
        let checkIsTrialSub : boolean = false;
        user['data'].subscriptions.forEach(item => {
          if (item.isTrial && item.status !== "Canceled") {
            checkIsTrialSub = true;
          }
        });
        if (checkIsTrialSub) {
          this.loadingService.display(false);
          this.dialogService.result(this.i18nDialog.confirm.notDiscountForIsTrial).afterClosed().subscribe((result => {
            if (result) {
              this.router.navigate(['/']);
            }
          }));
        } else {
          this.createPayment(_card);
        }
      } else {
        this.createPayment(_card);
      }
    });
  }

  createPayment(_card?: any) {
    let order;
    let orderData = {};
    if (this.itemType && this.itemType === 'alacartes') { // if it is alacartes 
      let cartDetails = this.cartService.getCart('alacartes');
      let alaItems = [];
      cartDetails.forEach(item => {
        if (item.product) {
          alaItems.push({
            ProductCountryId: item.product.productCountry.id,
            qty: item.qty
          });
        }
      });
      orderData = {
        // CardId: _card.id,
        // promotionCode: promoCode,
        userTypeId: 2,
        alaItems: alaItems,
        email: this.customerDetailsForm.value.email,
        fullName: this.customerDetailsForm.value.fullName,
        phone: this.customerDetailsForm.value.contactNumber ? this.customerDetailsForm.value.callingCode + this.customerDetailsForm.value.contactNumber : null,
        channelType: this.roadshowDetailsForm.value.channelType,
        eventLocationCode: this.roadshowDetailsForm.value.eventLocationCode
      }

      if (_card) {
        orderData['CardId'] = _card.id;
      }

      let _promotionCode: string = '';

      if (!(this.cartService.getPromoCode() && this.cartService.getPromoCode().id)) {
        // if (this.storage.getPromoCode()) {
        //   _promotionCode = this.storage.getPromoCode();
        // }
      } else {
        _promotionCode = this.cartService.getPromoCode().promotionCodes[0].code;
      }

      if (_promotionCode) {
        orderData['promotionCode'] = _promotionCode;
      }
    }

    if (this.itemType && this.itemType === 'subscriptions') {
      let cartDetails = this.cartService.getCart('subscriptions');
      let planCountryId = cartDetails.length > 0 ? cartDetails[0].plan.planCountry[0].id : null;
      let directTrial = cartDetails.length > 0 ? cartDetails[0].directTrial : false;
      // cartDetails.forEach(item => {
      //   if (item.plan) {
      //     if(item.plan.isTrial) {
      //       subsItems.push({
      //         PlanCountryId: item.plan.planCountry[0].id,
      //         qty: item.qty,
      //         isTrial: true
      //       });
      //     } else {
      //       subsItems.push({
      //         PlanCountryId: item.plan.planCountry[0].id,
      //         qty: item.qty
      //       });
      //     }
      //   }
      // });
      orderData = {
        // CardId: _card.id,
        // promotionCode: promoCode,
        userTypeId: 2,
        planCountryId: planCountryId,
        email: this.customerDetailsForm.value.email,
        fullName: this.customerDetailsForm.value.fullName,
        phone: this.customerDetailsForm.value.contactNumber ? this.customerDetailsForm.value.callingCode + this.customerDetailsForm.value.contactNumber : null,
        BillingAddressId: this.selectedBillingAddress.id,
        DeliveryAddressId: this.selectedShippingAddress.id,
        channelType: this.roadshowDetailsForm.value.channelType,
        eventLocationCode: this.roadshowDetailsForm.value.eventLocationCode,
        directTrial: directTrial
      }

      if (_card) {
        orderData['CardId'] = _card.id;
      }

      let _promotionCode: string = '';

      if (!(this.cartService.getPromoCode() && this.cartService.getPromoCode().id)) {
        // if (this.storage.getPromoCode()) {
        //   _promotionCode = this.storage.getPromoCode();
        // }
      } else {
        _promotionCode = this.cartService.getPromoCode().promotionCodes[0].code;
      }

      if (_promotionCode) {
        orderData['promotionCode'] = _promotionCode;
      }
    }

    this.createOrder(orderData)
    .then(result => {
      // this.cartService.setPromotion();
      // this.cartService.reloadSubscriptions();
      order = result['result']['order'];

      // redirect to orders page
      this.router.navigate([`/checkout/summary/${this.itemType}/${order.id}`]);
      this.loadingService.display(false);
    })
    .catch(error => {
      this.loadingService.display(false);
      console.log(`payment error ${error}`);
      this.resError = {
        type: 'error',
        message: typeof error === 'string' ? JSON.parse(error).message : error.message
      };
      let S = this.resError.message.toUpperCase();
      if (S.search("INSUFFICIENT FUNDS") == -1) {
      }
     else
      {
      //  console.log(`actionPayWithoutCreateAddress stripe error ${JSON.stringify(this.resError)}`);
        this.storage.setStripeError(this.resError.message);
        window.history.back();
      }
    });
  }

  createOrder(orderData) {
    let apiUrl = '';
    if (orderData && orderData.CardId) { // pay to Credit Cart
      if (this.itemType && this.itemType === 'alacartes') {
        apiUrl = `${this.appConfig.config.api.place_order}/stripe`;
      } else {
        apiUrl = `${this.appConfig.config.api.place_trial}/stripe`;
      }
    } else { // pay to cash
      apiUrl = `${this.appConfig.config.api.place_order}/cash`;
    }
    return new Promise((resolved, reject) => {
      this.httpService._create(`${apiUrl}`, orderData)
        .subscribe(
        res => {
          console.log(`create order ${JSON.stringify(res)}`);
          resolved(res);
        },
        error => {
          this.resError = {
            type: 'error',
            message: typeof error === 'string' ? JSON.parse(error).message : error.message
          };
          reject(error);
        }
        );
    });
  }

  checkEmail(_email: string) {
    return new Promise((resolved, reject) => {
      // check email is exist or not
      this._http._create(this.appConfig.config.api.checkEmail, { email: _email })
        .subscribe(res => {
          if (res && res.user) {
            if (res.user.isActive) {
              // todo
            } else {
              // todo
            }
          } else {
            // todo
          }
          resolved(res);
        },
        error => {
          reject(error);
          this.loadingService.display(false);
          if(error.status === 401) {
            this.cartService.clearCart();
            this.storage.clearCustomerDetails();
            this.storage.clearStripeDetail();
            this.userService.logout();
            this.router.navigate(['/login']);
          }
          this.resError = {
            type: 'error',
            message: typeof error === 'string' ? JSON.parse(error).message :  error.message
          };
        });
    });
  }

  changeValue(){
    this.roadshowDetailsForm.removeControl("eventLocationCode");
    if(this.storage.getCountry().code === 'SGP' && this.roadshowDetailsForm.value.channelType === 'Event') {
      this.roadshowDetailsForm.addControl("eventLocationCode", new FormControl('', [Validators.required]));
    }else if(this.storage.getCountry().code === 'SGP' && this.roadshowDetailsForm.value.channelType !== 'Event') {
      this.roadshowDetailsForm.addControl("eventLocationCode", new FormControl('', [Validators.required, lengthValidator({ min: 4 })]));
    }else if(this.storage.getCountry().code !== 'SGP' && this.roadshowDetailsForm.value.channelType === 'Event'){
      this.roadshowDetailsForm.addControl("eventLocationCode", new FormControl('', [Validators.required, valueValidator('number'), lengthValidator({ max: 5 })]));
    }else{
      this.roadshowDetailsForm.addControl("eventLocationCode", new FormControl('', [Validators.required, valueValidator('alphabet'), lengthValidator({ min: 4 }), lengthValidator({ max: 5 })]));
    }
  }
}
