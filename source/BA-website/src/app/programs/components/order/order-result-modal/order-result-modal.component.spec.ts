import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderResultModalComponent } from './order-result-modal.component';

describe('OrderResultModalComponent', () => {
  let component: OrderResultModalComponent;
  let fixture: ComponentFixture<OrderResultModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderResultModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderResultModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
