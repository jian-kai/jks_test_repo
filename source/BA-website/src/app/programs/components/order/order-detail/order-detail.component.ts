import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { ListViewsComponent } from '../../../../core/components/list-views/list-views.component';
import { RouterService } from '../../../../core/helpers/router.service';
import { GlobalService } from '../../../../core/services/global.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { OrderNoPipe } from '../../../../core/pipes/order-no.pipe';
import { DatePipe } from '@angular/common';
import { LocaleDateStringPipe } from '../../../../core/pipes/locale-date-string.pipe';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
  providers: [OrderNoPipe, DatePipe,LocaleDateStringPipe]
})
export class OrderDetailComponent extends ListViewsComponent {
  totalItems: number = 0;
  options: URLSearchParams;
  public orderId: number;
  public activeOrderList: boolean = true;
  public orderList: any;
  // public currentOrders: any = [];
  public orderDetail: any = [];
  public currentUser : any;
  public isLoading: boolean = true;
  public orderCurrencyCode: string = '';
  public countries: any;
  public i18nDialog: any;

  constructor(location: Location,
    routerService: RouterService,
    el: ElementRef,
    private _http: HttpService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private route: ActivatedRoute,
    private loadingService: LoadingService,
    private router: Router,
    private globalService: GlobalService,
    private translate: TranslateService,
    private dialogService: DialogService,
    private orderNoPipe: OrderNoPipe,
    private datePipe: DatePipe,
  ) {
    super(location, routerService, el);
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
  }

  // viewDetails(id: number, event: Event) {
  //   this.router.navigate([`/user/orders/${id}`]);
  // }

  // get emit from order detail page
  getEmitActiveOrderList(status: boolean) {
    this.activeOrderList = status;
  }

  // get order list of current user
  getData() {
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
    this.loadingService.display(true);
    // let oldHistoryOrders = this.historyOrders;
    this.orderDetail = [];
    
    let id;
    this.route.params.subscribe(params => {
      id = params['id'];
    });
    // this.options.set('id', 'createdAt_DESC');
    let options = {id: id};
    let params = this.globalService._URLSearchParams(options);

    this._http._getList(`${this.appConfig.config.api.order.replace('[sellerID]', this.currentUser.id)}?${params.toString()}`).subscribe(
      result => {
        this.orderDetail = result.rows;
        console.log('this.orderDetail', this.orderDetail)
        if (this.orderDetail && this.orderDetail.length > 0) {
          this.orderDetail.forEach(item => {
            item.currency = item.orderDetail[0].currency;
            item.basketSize = 0;
            item.orderDetail.forEach(order => {
              if (order.PlanCountry) {
                item.hasSubscription = true;
              }
              if (order.PlanCountry && order.PlanCountry.Plan && order.PlanCountry.Plan.isTrial) {
                item.isTrial = true;
              }
              item.basketSize += +order.qty;
            });
          });
          console.log('this.orderDetail2', this.orderDetail)
          // this.totalItems = result.length;
        }
        // if(JSON.stringify(oldHistoryOrders) === JSON.stringify(this.historyOrders)) {
        //   this.getData();
        // }else {
          this.loadingService.display(false);
        // }
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  // getCountries() {
  //   return new Promise((resolve, reject) => {
  //     this._http._getList(this.appConfig.config.api.countries).subscribe(
  //       data => {
  //         // this.loadingService.display(false);
  //         this.countries = data;
  //       },
  //       error => {
  //         // this.loadingService.display(false);
  //         console.log(`Cannot get country: ${error}`)
  //       }
  //     ) 
  //   });

  // }

  // cancelOrder(item) {
  //   console.log('item',item);
  //   let _orderId = item.id;
  //   let orderIDPP = this.orderNoPipe.transform(item.id, item.Country.code);
  //   let cancleMsg = 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?'.replace('{{value}}', orderIDPP);
  //   let urlAPI: string = '';
  //   // let cancleMsg = this.i18nDialog.confirm.cancelling_order.replace('{{value}}', _orderId);
  //   this.dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
  //     if (result) {
  //       this.loadingService.display(true);
  //       let options = {userTypeId: 2};
  //       let params = this.globalService._URLSearchParams(options);
  //       urlAPI = `${this.appConfig.config.api.cancel_order.replace('[orderID]', _orderId)}?${params.toString()}`;
  //       item.orderDetail.forEach(orderDetail => {
  //         if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan.isTrial) {
  //           urlAPI = `${this.appConfig.config.api.cancel_subscriptions.replace('[orderID]', _orderId)}?${params.toString()}`;
  //         }
  //       });

  //       this._http._delete(urlAPI).subscribe(
  //         data => {
  //           // this.loadingService.display(false);
  //           this.dialogService.result(this.i18nDialog.confirm.order_cancelled_succ).afterClosed().subscribe((result => {
  //             this.getData();
  //           }));
  //         },
  //         error => {
  //           // this.resError = {
  //           //   type: 'error',
  //           //   message: JSON.parse(error).message
  //           // };
  //           let message = typeof error === 'string' ? error : JSON.parse(error).message
  //           this.loadingService.display(false);
  //           this.dialogService.result(message).afterClosed().subscribe((result => {
  //             this.loadingService.display(false);
  //           }));
  //           console.log(error);
  //         }
  //       );

  //     }
  //   }));
  // }

  // checkAllowToCancel(createdAt) {
  //   let currentDate = new Date(); 
  //   let _createdAt = new Date(createdAt);
  //   var seconds: any  = 0;
  //   seconds = (currentDate.getTime() - _createdAt.getTime());
  //   var one_day=1000*60*60*24; // miniseconds in 1 day
  //   let diffHrs = Math.abs((seconds) / 36e5);
  //   if (diffHrs > 24) {
  //     return false;
  //   }
  //   return true;
  // }

  goToHome() {
    this.router.navigate(['/']);
  }

}
