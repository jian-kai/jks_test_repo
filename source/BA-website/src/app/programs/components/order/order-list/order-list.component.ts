import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { HttpService } from '../../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../../core/services/storage.service';
import { LoadingService } from '../../../../core/services/loading.service';
import { ListViewsComponent } from '../../../../core/components/list-views/list-views.component';
import { RouterService } from '../../../../core/helpers/router.service';
import { GlobalService } from '../../../../core/services/global.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { CartService } from '../../../../core/services/cart.service';
import { UsersService } from '../../../../auth/services/users.service';
import { TranslateService } from '@ngx-translate/core';
import { OrderNoPipe } from '../../../../core/pipes/order-no.pipe';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss'],
  providers: [OrderNoPipe, DatePipe]
})
export class OrderListComponent extends ListViewsComponent {
  totalItems: number = 0;
  options: URLSearchParams;
  public orderId: number;
  public activeOrderList: boolean = true;
  public orderList: any;
  public currentOrders: any = [];
  public historyOrders: any = [];
  public currentUser : any;
  public isLoading: boolean = true;
  public orderCurrencyCode: string = '';
  public countries: any;
  public i18nDialog: any;

  constructor(location: Location,
    routerService: RouterService,
    el: ElementRef,
    private _http: HttpService,
    private appConfig: AppConfig,
    private storage: StorageService,
    private loadingService: LoadingService,
    private router: Router,
    public userService: UsersService,
    private cartService: CartService,
    private globalService: GlobalService,
    private translate: TranslateService,
    private dialogService: DialogService,
    private orderNoPipe: OrderNoPipe,
    private datePipe: DatePipe,
  ) {
    super(location, routerService, el);
    this.currentUser = this.storage.getCurrentUserItem();
    if (!this.currentUser || !this.currentUser.id) {
      console.log('User is not exist');
      return;
    }
  }

  viewDetails(id: number, code: any, event: Event) {
    this.router.navigate([`/orders/${code}/${id}`]);
  }

  // get emit from order detail page
  getEmitActiveOrderList(status: boolean) {
    this.activeOrderList = status;
  }

  // get order list of current user
  getData() {
    this.translate.get('dialog').subscribe(res => {
      this.i18nDialog = res;
    });
    this.loadingService.display(true);
    let oldHistoryOrders = this.historyOrders;
    this.historyOrders = [];
    
    this.options.set('orderBy', 'createdAt_DESC');
    // this.options.delete('limit');

    this._http._getList(`${this.appConfig.config.api.order.replace('[sellerID]', this.currentUser.id)}?${this.options.toString()}`).subscribe(
      result => {
        this.historyOrders = result;
        console.log('this.historyOrders', this.historyOrders)
        if (this.historyOrders && this.historyOrders.rows && this.historyOrders.rows.length > 0) {
          let groupOrders = {createdAt: '', items: []};
          let groupOrdersList = [];
          let createdAt;
          this.historyOrders.rows.forEach(item => {
            item.currency = item.orderDetail[0].currency;
            item.orderDetail.forEach(order => {
              if (order.PlanCountry) {
                item.hasSubscription = true;
              }
              if (order.PlanCountry && order.PlanCountry.Plan && order.PlanCountry.Plan.isTrial) {
                item.isTrial = true;
              }
            });

            if(!createdAt || createdAt !== this.globalService.formatDate(new Date(item.createdAt))) {
              if(groupOrders.items.length > 0) {
                groupOrdersList.push(groupOrders);
              }
              groupOrders = {createdAt: '', items: []};
              createdAt = this.globalService.formatDate(new Date(item.createdAt));
              groupOrders.createdAt = createdAt;
            }
            groupOrders.items.push(item);
          });
          if(groupOrders.items.length > 0) {
            groupOrdersList.push(groupOrders);
          }
          this.historyOrders.rows = groupOrdersList;
          console.log('groupOrdersList', groupOrdersList)
          this.totalItems = result.count;
        }
        // if(JSON.stringify(oldHistoryOrders) === JSON.stringify(this.historyOrders)) {
        //   this.getData();
        // }else {
          this.loadingService.display(false);
        // }
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
        if(error.status === 401) {
          this.cartService.clearCart();
          this.storage.clearCustomerDetails();
          this.storage.clearStripeDetail();
          this.userService.logout();
          this.router.navigate(['/login']);
        }
      }
    )
    
    this.loadingService.status.subscribe((value: boolean) => {
      this.isLoading = value;
    })
  }
  
  getCountries() {
    return new Promise((resolve, reject) => {
      this._http._getList(this.appConfig.config.api.countries).subscribe(
        data => {
          // this.loadingService.display(false);
          this.countries = data;
        },
        error => {
          // this.loadingService.display(false);
          console.log(`Cannot get country: ${error}`)
        }
      ) 
    });

  }

  cancelOrder(item) {
    console.log('item',item);
    let _orderId = item.id;
    let orderIDPP = this.orderNoPipe.transform(item.id, item.Country.code);
    let cancleMsg = 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?'.replace('{{value}}', orderIDPP);
    let urlAPI: string = '';
    // let cancleMsg = this.i18nDialog.confirm.cancelling_order.replace('{{value}}', _orderId);
    this.dialogService.confirm(cancleMsg).afterClosed().subscribe((result => {
      if (result) {
        this.loadingService.display(true);
        let options = {userTypeId: 2};
        let params = this.globalService._URLSearchParams(options);
        urlAPI = `${this.appConfig.config.api.cancel_order.replace('[orderID]', _orderId)}?${params.toString()}`;
        item.orderDetail.forEach(orderDetail => {
          if(orderDetail.PlanCountry && orderDetail.PlanCountry.Plan.isTrial) {
            urlAPI = `${this.appConfig.config.api.cancel_subscriptions.replace('[orderID]', _orderId)}?${params.toString()}`;
          }
        });

        this._http._delete(urlAPI).subscribe(
          data => {
            // this.loadingService.display(false);
            this.dialogService.result(this.i18nDialog.confirm.order_cancelled_succ).afterClosed().subscribe((result => {
              this.getData();
            }));
          },
          error => {
            // this.resError = {
            //   type: 'error',
            //   message: JSON.parse(error).message
            // };
            let message = typeof error === 'string' ? error : JSON.parse(error).message
            this.loadingService.display(false);
            this.dialogService.result(message).afterClosed().subscribe((result => {
              this.loadingService.display(false);
            }));
            console.log(error);
          }
        );

      }
    }));
  }

  checkAllowToCancel(createdAt) {
    let currentDate = new Date(); 
    let _createdAt = new Date(createdAt);
    var seconds: any  = 0;
    seconds = (currentDate.getTime() - _createdAt.getTime());
    var one_day=1000*60*60*24; // miniseconds in 1 day
    let diffHrs = Math.abs((seconds) / 36e5);
    if (diffHrs > 24) {
      return false;
    }
    return true;
  }

  goToHome() {
    this.router.navigate(['/']);
  }

}
