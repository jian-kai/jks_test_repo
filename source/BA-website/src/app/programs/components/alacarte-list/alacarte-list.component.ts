import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';

import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CartService } from '../../../core/services/cart.service';
import { Item } from '../../../core/models/item';

@Component({
  selector: 'app-alacarte-list',
  templateUrl: './alacarte-list.component.html',
  styleUrls: ['./alacarte-list.component.scss']
})
export class AlacarteListComponent implements OnInit {
  public productList: Object = {};
  public productTypes: Array<String> = [];
  private promoCodeForm: FormGroup;
  public cart: Item[] = [];
  public promoCodeError: any;
  public receipt = {
    totalPrice: 0,
    totalPriceNotDivided: 0,
    specialDiscount: 0,
    discount: 0,
    subTotal: 0,
    shippingFee: 0,
    totalIclTax: 0,
    applyDiscount: 0
  };
  public cartRules: any;
  public checkPromoCodeApplied: boolean = false;
  public notAllowPromoCode: boolean = false;
  public promoCode: string;
  public currentUser: any;
  constructor(private httpService: HttpService,
    private storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private router: Router,
    private cartService: CartService,
    private builder: FormBuilder,) {
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser.Country.isBaAlaCarte) {
        this.router.navigate(['/']);
      }
      this.promoCodeForm = builder.group({
        promoCode: ['', Validators.required],
      });

    cartService.hasUpdate.subscribe(hasUpdate => {
      this.cart = this.cartService.getCart('alacartes');
      this.receipt = this.cartService.getReceipt(this.cartRules, 'alacartes');
      
      this.notAllowPromoCode = false;
      if(this.receipt.specialDiscount) {
        this.notAllowPromoCode = true;
        this.promoCode = null;
      }
      
      let promoCode = this.cartService.getPromoCode();
      if(promoCode.id) {
        if((promoCode.minSpend && this.receipt.totalPriceNotDivided < promoCode.minSpend && promoCode.allowMinSpendForTotalAmount)
        || (promoCode.minSpend && this.receipt.applyDiscount < promoCode.minSpend && !promoCode.allowMinSpendForTotalAmount)) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: 'Your total price must greater than ' + promoCode.minSpend
          };
        } else {
          this.promoCodeError = null;
        }
      }
    });

    this.checkPromoCodeApplied = false;
    if(this.cartService.getPromoCode().id) {
      this.promoCode = this.cartService.getPromoCode().promotionCodes[0].code;
      this.checkPromoCodeApplied = true;
    }
  }

  getCartRules() {
    this.httpService._getList(`${this.appConfig.config.api.cart_rules}`).subscribe(
      data => {
        this.cartRules = data;
        this.receipt = this.cartService.getReceipt(this.cartRules, 'alacartes');
      }
    )
  }

  ngOnInit() {
    this.cartService.clearCart(true);
    this.storage.clearCustomerDetails();
    this.storage.clearStripeDetail();
    this.cart = this.cartService.getCart('alacartes');
    this.getProductList();
    // this.getCartRules();

    // this.promoCodeForm.controls.promoCode.valueChanges.subscribe(value => {
    //   if(value === '') {
    //     this.cartService.setPromotion();
    //     this.storage.setPromoCode('');
    //   }
    // })
    
  }

  getProductList() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.productList = [];
    // get all products
    let options = {
      userTypeId: 2
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        if (data && data.length > 0) {
          // this.productList = data;
          data.forEach((product, index) => {
            // push to product list
            product.qty = 0;
            let i = -1;
            if (this.cart) {
              i = this.checkItemExistsInCarts(product);
            }
            
            if (i > -1) {
              product.qty = this.cart[i].qty;
            }

            if(this.productList[product.ProductType.name]) {
              this.productList[product.ProductType.name].push(product);
            } else {
              this.productList[product.ProductType.name] = [product];
              this.productTypes.push(product.ProductType);
            }

            // get productType
            // this.productTypes = Object.keys(this.productList);
          });
        }
        
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
      }
    )
  }

  //minus quatity of the item
  minusQuatity(index, productType) {
    if (this.productList[productType][index].qty > 0) {
      this.productList[productType][index].qty -= 1;
    }
    let item = this.productList[productType][index];
    let i = this.checkItemExistsInCarts(item);
    // this.cart.forEach((_item, _index) => {
    //   if (item.id === _item.id) {
    //     i = _index;
    //     return;
    //   }
    // });

    // has in carts
    if ( item && this.productList[productType][index].qty > 0 && i > -1) {     
        this.cart[i].qty = this.productList[productType][index].qty;
        this.cartService.refreshItem(this.cart);
    }

    // has in carts then remove item
    if ( item && this.productList[productType][index].qty === 0 && i > -1) {
        this.cartService.removeItem(this.cart[i]);
    }
    this.cart = this.cartService.getCart('alacartes');

  }

  //plus quatity of the item
  plusQuatity(index, productType) {
    if (this.productList[productType][index].qty < 10000) {
      this.productList[productType][index].qty += 1;
    }
    let item = this.productList[productType][index];
    let i = this.checkItemExistsInCarts(item);
    // has in carts
    if ( item && this.productList[productType][index].qty > 0 && i > -1) {
      this.cart[i].qty = this.productList[productType][index].qty;
      this.cartService.refreshItem(this.cart);
    }

    // has not in carts
    if (item && this.productList[productType][index].qty > 0 && i === -1) {
      this.addToCart(this.productList[productType][index]);
    }
    this.cart = this.cartService.getCart('alacartes');
  }

  // add item to cart
  addToCart(_item: any) {
    this.loadingService.display(true);
    let item:any;
    if(_item.productCountry) {
      item = {
        id: _item.id,
        product: _item,
        qty: _item.qty ? _item.qty : 1
      }
    }
    
    this.cartService.addItem(item);
    // sync cart item to server
    this.cartService.updateCart();
    this.cart = this.cartService.getCart('alacartes');
    this.loadingService.display(false);
  }


  //change quatity of the item
  changeQuatity(index, productType) {
    if (this.productList[productType][index].qty < 0) {
      this.productList[productType][index].qty = 0;
    }
    if (this.productList[productType][index].qty > 10000) {
      this.productList[productType][index].qty = 10000;
    }
    let item = this.productList[productType][index];
    let i = this.checkItemExistsInCarts(item);
    // has in carts
    if ( item && this.productList[productType][index].qty > 0 && i > -1) {
      this.cart[i].qty = this.productList[productType][index].qty;
      this.cartService.refreshItem(this.cart);
    }

    // has not in carts
    if (item && this.productList[productType][index].qty > 0 && i === -1) {
      this.addToCart(this.productList[productType][index]);
    }
    this.cart = this.cartService.getCart('alacartes');
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item);
    this.cart = this.cartService.getCart('alacartes');
  }

  checkItemExistsInCarts(item) {
    let has = -1;
    this.cart.forEach((_item, _index) => {
      if (item.id == _item.id) {
        has = _index;
      }
    });
    return has;
  }

  nextTo() {
    this.cartService.setStepData(true);
    this.router.navigate(['/checkout/review/alacartes']);
  }


  applyPromoCode(_promoCode: string) {
    this.storage.setPromoCode(_promoCode);
    this.promoCodeError = null;
    this.loadingService.display(true);
    // get all product/plan country id
    let productCountryIds = [];
    this.cart.forEach(cartItem => {
      productCountryIds.push(cartItem.product.productCountry.id);
    })

    this.httpService._create(`${this.appConfig.config.api.check_promo_code}`, {
      promotionCode: _promoCode,
      productCountryIds: productCountryIds,
    }).subscribe(
      data => {
        this.cartService.setPromotion(data);
        if(data.minSpend && this.receipt.totalPriceNotDivided < data.minSpend) {
          // this.cartService.setPromotion();
          this.promoCodeError = {
            type: 'error',
            message: 'Your total price must greater than ' + data.minSpend
          };
        }

        this.loadingService.display(false);
      },
      error => {
        this.cartService.setPromotion();
        // this.promoCodeError = true;
        this.promoCodeError = {
          type: 'error',
          message: JSON.parse(error).message
        };
        this.loadingService.display(false);
        console.log(error)
      }
    )
  }

}
