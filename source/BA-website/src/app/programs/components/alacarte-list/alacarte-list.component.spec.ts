import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlacarteListComponent } from './alacarte-list.component';

describe('AlacarteListComponent', () => {
  let component: AlacarteListComponent;
  let fixture: ComponentFixture<AlacarteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlacarteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlacarteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
