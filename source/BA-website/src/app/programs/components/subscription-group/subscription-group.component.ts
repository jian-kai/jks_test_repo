import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl } from '@angular/forms';

import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { StorageService } from '../../../core/services/storage.service';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';
import { CartService } from '../../../core/services/cart.service';
import { PlanService } from '../../../core/services/plan.service';
import { Item } from '../../../core/models/item';
declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-subscription-group',
  templateUrl: './subscription-group.component.html',
  styleUrls: ['./subscription-group.component.scss'],
  providers: [PlanService]
})
export class SubscriptionGroupComponent implements OnInit {
  public planList: Object = {};
  public planTypes: Array<String> = [];
  private promoCodeForm: FormGroup;
  public cart: Item[] = [];
  public planGroups: any = [];
  public groupSelected: any;
  public currentUser: any;
  constructor(private httpService: HttpService,
    private storage: StorageService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
    private router: Router,
    private cartService: CartService,
    private builder: FormBuilder,
    private planService: PlanService) {
      this.currentUser = this.storage.getCurrentUserItem();
      if (!this.currentUser.Country.isBaSubscription || !this.currentUser.Country.isBaEcommerce) {
        this.router.navigate(['/']);
      }
      this.promoCodeForm = builder.group({
        promoCode: ['', Validators.required],
      });
    }

  ngOnInit() {
    this.cartService.clearCart(true);
    this.storage.clearCustomerDetails();
    this.storage.clearStripeDetail();
    this.storage.clearGroupId();
    this.cart = this.cartService.getCart('subscriptions');
    this.getPlanList();
    console.log('this.cart', this.cart)
  }

  getPlanList() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    this.planList = [];
    // get all plans
    let options = {
      userTypeId: 2
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.trial_plan_group}?` + params.toString()).subscribe(
      data => {
        console.log('data', data);
        this.planGroups = data;
        // console.log('planService', this.planService.groupPlanTrial(data));
        // let _planGroups = this.planService.groupPlanTrial(data);
        // console.log('_planGroups ', _planGroups)
        // this.appConfig.config.groupPlanTrialName.forEach(element => {
        //   if (_planGroups[element]) {
        //     this.planGroups.push(_planGroups[element]);
        //   }
        // });
        // // this.planGroups = this.planService.groupPlanTrial(data);
        // console.log('this.planGroups --- ', this.planGroups);
        // if (this.planGroups && this.planGroups.length > 0) {
          
        // } else {
        //   console.log('ssssssss --- ', this.planGroups);
        // }
        
        console.log('planList', this.planList)
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
      }
    )
  }

  //minus quatity of the item
  minusQuatity(index, productType) {
    if (this.planList[productType][index].qty > 0) {
      this.planList[productType][index].qty -= 1;
    }
    let item = this.planList[productType][index];
    let i = this.checkItemExistsInCarts(item);
    // this.cart.forEach((_item, _index) => {
    //   if (item.id === _item.id) {
    //     i = _index;
    //     return;
    //   }
    // });

    // has in carts
    if ( item && this.planList[productType][index].qty > 0 && i > -1) {     
        this.cart[i].qty = this.planList[productType][index].qty;
        this.cartService.refreshItem(this.cart);
    }

    // has in carts then remove item
    if ( item && this.planList[productType][index].qty === 0 && i > -1) {
        this.cartService.removeItem(this.cart[i]);
    }

  }

  //plus quatity of the item
  plusQuatity(index, productType) {
    if (this.planList[productType][index].qty < 10000) {
      this.planList[productType][index].qty += 1;
    }
    let item = this.planList[productType][index];
    let i = this.checkItemExistsInCarts(item);
    // has in carts
    if ( item && this.planList[productType][index].qty > 0 && i > -1) {
      this.cart[i].qty = this.planList[productType][index].qty;
      this.cartService.refreshItem(this.cart);
    }

    // has not in carts
    if (item && this.planList[productType][index].qty > 0 && i === 1) {
      this.addToCart(this.planList[productType][index]);
    }
  }

  // add item to cart
  addToCart(_item: any, _event?: any) {
    this.loadingService.display(true);
    let item:any;
    if(_item.planCountry) {
      item = {
        id: _item.id,
        plan: _item,
        qty: 1
      }
    }
    
    if(this.cart.filter(cartItem => cartItem.id === item.id).length > 0) {
      this.cartService.removeItem(item);
      // sync cart item to server
      // this.cartService.updateCart();
      this.cart = this.cartService.getCart();
      this.loadingService.display(false);
      if (_event) {
        let target = _event.target || _event.srcElement;
        $(_event.target).removeClass("selected"); 
        target.innerHTML = 'SELECT'  ;
      }
    } else {
      this.cartService.addItem(item);
      // sync cart item to server
      this.cartService.updateCart();
      this.cart = this.cartService.getCart();
      this.loadingService.display(false);
      if (_event) {
        let target = _event.target || _event.srcElement;
        $(_event.target).addClass("selected"); 
        target.innerHTML = 'SELECTED'  ;
      }
    }
  }


  //change quatity of the item
  changeQuatity(index, productType) {
    if (this.planList[productType][index].qty < 0) {
      this.planList[productType][index].qty = 0;
    }
    if (this.planList[productType][index].qty > 10000) {
      this.planList[productType][index].qty = 10000;
    }
    this.cartService.refreshItem(this.planList[productType][index]);
  }

  // handle click remove item
  removeItem(item) {
    this.cartService.removeItem(item);
    this.cart = this.cartService.getCart()
  }

  checkItemExistsInCarts(item) {
    let has = -1;
    this.cart.forEach((_item, _index) => {
      if (item.id == _item.id) {
        has = _index;
      }
    });
    return has;
  }

  nextTo() {
    this.cartService.setStepData(true);
    // this.router.navigate(['/checkout/review/subscriptions']);
    this.router.navigate(['/subscriptions/blade-type/' + this.groupSelected.name]);
    this.storage.setGroupId(this.groupSelected.id);
  }

  selectGroup(_group) {
    // this.router.navigate(['/subscriptions/blade-type/' + _group.name]);
    if(this.groupSelected && _group && this.groupSelected.name === _group.name) {
      this.groupSelected = null;
    } else {
      this.groupSelected = _group;
    }
  }
  

}
