import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { StorageService } from '../../../core/services/storage.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { LoadingService } from '../../../core/services/loading.service';
import { GlobalService } from '../../../core/services/global.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public currentUser: any;
  public hasProduct: boolean = false;
  constructor(
    private router: Router,
    private storage: StorageService,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    private appConfig: AppConfig,
  ) {
    this.currentUser = this.storage.getCurrentUserItem();
  }

  ngOnInit() {
    this.getProductList();
  }

  goto(routerName) {
    this.router.navigate([routerName]);
  }

  getProductList() {
    let cur = this.storage.getCurrentUserItem();
    this.loadingService.display(true);
    // get all products
    let options = {
      userTypeId: 2
    };
    let params = this.globalService._URLSearchParams(options);
    this.httpService._getList(`${this.appConfig.config.api.products}?` + params.toString()).subscribe(
      data => {
        if (data && data.length > 0) {
          this.hasProduct = true;
        }
        
        this.loadingService.display(false);
      },
      error => {
        this.loadingService.display(false);
        console.log(error.error)
      }
    )
  }
}
