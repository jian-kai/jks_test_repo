import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Rx';


import { AppConfig } from 'app/config/app.config';
import { GlobalService } from '../../../core/services/global.service';
import { HttpService } from '../../../core/services/http.service';
import { StorageService } from '../../../core/services/storage.service';
import { UsersService } from '../../../auth/services/users.service';
import { LoadingService } from '../../../core/services/loading.service';
import { lengthValidator } from '../../../core/validators/length.validator';
declare var jQuery: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-sample-product',
  templateUrl: './sample-product.component.html',
  styleUrls: ['./sample-product.component.scss'],
})
export class SampleProductComponent implements OnInit {
  public itemType: string = '';
  public customerDetailsForm: FormGroup;
  public callingCodes: any;
  public deliveryAddressForm;
  public makeBillingDifferentShipping: boolean = false;
  public resError: any;
  public countryStates: Array<any>;
  private element: any;
  public selectedCard: any;
  public emailExist: any;

  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private appConfig: AppConfig,
    private httpService: HttpService,
    private loadingService: LoadingService,
    private builder: FormBuilder,
    private storage: StorageService,
    private globalService: GlobalService,
    private _http: HttpService,
  ) {
    this.customerDetailsForm = builder.group({
      email: ['', Validators.required],
      callingCode: [''],
      contactNumber: ['', [Validators.required, lengthValidator({min: 8})]],
      fullName: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.callingCodes = this.globalService.getCountryCodes();
    if (this.storage.getCurrentUserItem()) {
      this.customerDetailsForm.patchValue({
        // email: this.storage.getCurrentUserItem().email,
        fullName: '',
        callingCode: '+' + this.storage.getCurrentUserItem().Country.callingCode});
    }
  }

  submit() {
    if (this.customerDetailsForm.valid) {
      let data = this.customerDetailsForm.value;
      data.fullname = data.fullName;
      delete data.fullName;
      this.loadingService.display(true);
      this._http._create(`${this.appConfig.config.api.sample_product}`, data).subscribe(
        data => {
          if (data && data.ok) {
            this.router.navigate(['/']);
          }
          console.log('data', data)
          this.loadingService.display(false);
        },
        error => {
          this.resError = {
            type: 'error',
            message: typeof error === 'string' ? error : JSON.parse(error).message
          };
          this.loadingService.display(false);
        }
      );
    }
  }

}
