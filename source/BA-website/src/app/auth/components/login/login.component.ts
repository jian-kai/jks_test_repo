import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { LoadingService } from '../../../core/services/loading.service';
import { ModalComponent } from '../../../core/directives/modal/modal.component';
import { ModalService } from '../../../core/services/modal.service';
import { UsersService } from '../../services/users.service';
import { AuthService } from '../../services/auth.service';
import { StorageService } from '../../../core/services/storage.service';
import { HttpService } from '../../../core/services/http.service';
import { AppConfig } from 'app/config/app.config';
import { CountriesService } from '../../../core/services/countries.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  alert: object = {
    type: 'error',
    message: []
  }
  private _usersService: UsersService;
  private _router: Router;
  private loginForm: FormGroup;
  public stage: string; // signin | emailForm | sendEmail
  private emailForm: FormGroup;
  public resError: any;
  // public channelTypeList: any;

  constructor(
    el: ElementRef,
    usersService: UsersService,
    router: Router,
    builder: FormBuilder,
    private authService: AuthService,
    private storage: StorageService,
    private _http: HttpService,
    private translate: TranslateService,
    private appConfig: AppConfig,
    private loadingService: LoadingService,
    private countriesService: CountriesService
  ) {
    this.loadingService.display(true);
    this._usersService = usersService;
    this._router = router;
    this.isLogged();

    // set controls for login form
    this.loginForm = builder.group({
      badgeId: ['', Validators.required],
      icNumber: ['', Validators.required],
      // channelType: ['', Validators.required],
      // eventLocationCode: ['', Validators.required]
    });

    // this._http._getList(`${this.appConfig.config.api.channel_type}`).subscribe(
    //   data => {
    //     this.channelTypeList = data;
    //     console.log('channelTypeList', this.channelTypeList);
    //     this.loadingService.display(false);
    //   },
    //   error => {
    //     console.log(error.error)
    //     this.loadingService.display(false);
    // })
    this.loadingService.display(false);
  }

  ngOnInit() {
    this.resError = null;
    this.stage = 'signin';

    this.loginForm.valueChanges.subscribe(_ => {
      this.resError = null;
    })
  }

  // check if logged in so redirect
  isLogged() {
    if (this.storage.getCurrentUserItem()) {
      // logged in so redirect to dashboard page
      this._router.navigate([''], { queryParams: {} });
    }
  }

  onSubmit(credentials) {
    // this.alert['message'] = [];
    this.loadingService.display(true);
    // this._http._create(this.appConfig.config.api.login, credentials).subscribe(
    //   data => {
    //     // store token into localstore
    //     this.storage.setAuthToken(data.token);
    //     this.storage.setCountry(data.user.Country);
    //     this._usersService._loggedIn.next(true);
    //     this.storage.setCurrentUser(data.user);
    //     this._router.navigate([''], { queryParams: {} });
    //   },
    //   error => {
    //     this.alert['message'] = [JSON.parse(error).message];
    //   }
    // );
    this._usersService.login(credentials).subscribe(
      data => {

        if(data.token) {
          // store token into localstore
          this.storage.setCurrentUser(data.user);
          this.storage.setAuthToken(data.token);
          this.storage.setCountry(data.user.Country);
          this.storage.setLoginTimer(new Date());
          this._usersService._loggedIn.next(true);
          this._router.navigate(['/'], { queryParams: {} });
          this.countriesService.changeCountry(data.user.Country);
          this.translate.use(data.user.Country.defaultLang.toLowerCase());
        } else {
          
        }
        this.loadingService.display(false);
         this.resError = null;
      },
      error => {
        this.loadingService.display(false);
        // this.alert['message'] = [JSON.parse(error).message];
        this.resError = {
          type: 'error',
          message: JSON.parse(error).message
        };
      }
    );

  }
}
