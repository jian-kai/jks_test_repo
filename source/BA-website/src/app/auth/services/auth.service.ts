import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class AuthService implements CanActivate {
	public code: string;
	constructor(private location: Location, private router: Router,
		private storage: StorageService,
	) {
		let params = new URLSearchParams(this.location.path(false).split('?')[1]);
		this.code = params.get('code');
		if (this.code) {
			// alert(this.code);
		}

	}
	canActivate(): boolean {
		if (this.storage.getCurrentUserItem()) {
			// logged in so return true
			return true;
		}

		// not logged in so redirect to login page with the return url
		this.router.navigate(['/login'], { queryParams: {} });
		return false;
	}

}