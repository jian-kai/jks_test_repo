import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { StorageService } from '../../core/services/storage.service';
import { RequestService } from '../../core/services/request.service';
import { AppConfig } from 'app/config/app.config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class UsersService {

  _loggedIn = new BehaviorSubject(false);

  constructor(private storage: StorageService,
    private request: RequestService,
    private appConfig: AppConfig,
    private http: Http) {
    if (!!this.storage.getAuthToken()) {
      this._loggedIn.next(true);
    }
  }

  isLoggedIn() {
    return this._loggedIn.getValue();
  }

  getLoggedIn() {
    return this._loggedIn;
  }

  logout() {
    this.storage.removeAuthToken();
    this.storage.removeCurrentUser();
    this.storage.removeLoginTimer();
    this._loggedIn.next(false);
    // window.location.reload();
  }

  login(credentials) {
    return this.http
      .post(`${this.appConfig.config.api.login}`, credentials, { headers: this.request.getHeaders('json') })
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  // response data successfully
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  // handle if have errors
  private handleErrorObservable(error: Response | any) {
    if (error.status === 400) {
      return Observable.throw(error._body);
    }
    return Observable.throw(error.json());
  }

}
