export const ko = {
  menu: {
    home: '홈',
    logout: '로그아웃',
    order_history: '세일즈 히스토리',
    change_language: '언어 변경 ({{lang}})',
    select_a_category: '카테고리를 선택 '
  },
  title: {
    home: '홈',
    alacartes: '알라 카르트 (따로 담기)',
    subscriptions: '체험하기',
    sample_product: '체험하기',
    shopping: '쇼핑 카트',
    checkout: '체크아웃',
    order_confirm: '주문 확인',
    sale_history : '세일즈 히스토리',
  },
  product: {
    field: {
      product: '제품',
      price: '가격',
      quantity: '수량',
      subtotal: '소계',
    }    
  },
  trial: {
    pay_as_you_go: '선불',
  },
  // message
  msg: {
    validation: {
      required: '{{value}}는 필수 입력 사항입니다',
      character_string: '최소 한 개의 알파벳을 입력해주세요',
      special_characters: "Can't enter special characters",
      select_required: '값을 선택해 주세요',
      email: '유효한 이메일 주소를 입력하기 바랍니다',
      email_exists: '이 이메일 주소는 이미 사용중입니다',
      maxlength: '최대 {{value}} 자까지 입력이 가능합니다',
      minlength: '최소 {{value}} 자까지 입력이 가능합니다',
      minlength_spg: '최소 {{value}} 자까지 입력이 가능합니다',
      maxDigits: '최대 XXX 숫자값 입력이 가능합니다',
      minDigits: '최소 {{value}} 숫자값까지 입력이 가능합니다',
      password_format: '비밀번호는 최소 6자 이상이어야 하며 최소 1개의 숫자 혹은 부호를 포함해야 합니다',
      password_format_lenght: '최소 6자 이상 입력 바랍니다',
      password_format_contain: '최소 1개의 숫자 혹은 부호를 입력 바랍니다',
      password_not_match: "Password confirmation doesn’t match Password",
      old_pw_invalid: '오래된 비밀번호로 유효하지 않습니다',
      birthday_invalid: '유효하지 않은 생일입니다',
      phone: '전화번호는 유효한 11자리여야 합니다.',
      valid : '유효한 값 {{value}}.을 입력 바랍니다',
      valid_word: 'Please enter a valid E-Mail Address, this attempt has been logged',
      phone_sgp: '전화번호는 유효한 8자리여야 합니다.',    
      valid_postcode: 'Must be a valid 6 digits post code'      
    },
    field: {
      email: '이메일 주소',
      phone: '전화 번호',
      postcode: '우편 번호',
    },
    status: {
      canceled: '취소됨',
      process: '처리 중',
      on_hold: '정지',
      payment_received: '결제 완료',
      completed: '완료됨',
      pending: '계류중인',
      delivering: '배송 중',
      loading: '로딩'
    },
  },

  // btn
  btn : {
    apply: '적용',
    next: '다음',
    ok : '네',
    select : '선택',
    detail: '세부 사항',
    cancel: '취소',
    submit: '제출',
    confirm:'확인'
  },
  // dialog
  dialog: {
    confirm: {
      title: '확인',
      content: '맞습니까?',
      cancelling_order: '<font color="#fe5000">{{value}}</font> 주문을 취소하려고 합니다. 맞습니까?',
      change_plan: '',
      change_payment: '',
      order_cancelled_succ : '주문 취소 요청이 성공적으로 완료되었습니다.',
      notDiscount: '',
      notDiscountForIsTrial: '',
    }
  },

  promo: {
    heading: '할인 코드',
    place_holder: '코드를 여기에 입력하세요',
    na : '없음'
  },

  price: {
    total: '합계',
    subTotal: '소계',
    discount: '할인',
    shipping_fee : '배송비',
    total_tax : '합계 (세금 불포함)',
    grand_total_tax : '총계 (세금 포함)',
    grand_total : '총계',
    free: '무료'

  },
  login: {
    badge_id: {
      label: '배지 ID',
      placeholder: '배지 ID',
      error_message: {
        empty: '배지 ID는 필수 입력사항 입니다',
      }
    },
    ic_number: {
      label: 'IC 번호',
      placeholder: 'IC 번호',
      error_message: {
        empty: 'IC 번호는 필수 입력사항 입니다',
      }
    },
    channelType: {
      label: '채널 종류',
      placeholder: '채널 종류',
      error_message: {
        empty: '채널 종류는 필수 입력사항 입니다',
    }
  },
  eventLocationCode: {
    label: '벤트/로케이션 코드',
    placeholder: '벤트/로케이션 코드',
    error_message: {
      empty: '벤트/로케이션 코드는 필수 입력사항 입니다',
    }
  }
},
  home: {
    heading: '시작하려는 카테고리를 선택 바랍니다',
    subscription: '체험하기',
    alacarte : '상품',
  },
    //  shave plan page
  plan: {
    plan_section: {
      select_blade_type: '배지 종류 선택하기',
      select_shave_plan: '쉐이브 플랜을 선택하세요',
      select_plan: 'Select a Blade type',
      select_payment: 'Which Payment Plan Would You Prefer?',
      plan_details : 'What you’ll get',
      subtitle: '남성용 {{value}}-칼날 카트리지',
      subtitle_Women: '여성용 {{value}}-칼날 카트리지',
      totalText: 'per annum',
      btn_discount: '저장',
      note: "We'll send a reminder email before each shipment so you have enough time to modify or cancel your plan.",
      btn_add_to_cart: '결제 진행하기',
      suggest_payment: '우리는 수락합니다'
    },
    frequently_asked_questions: '자주 묻는 질문',
  },

  review: {
    item_selected: '{{value}} 항목 선택됨',
    item_price: '제품 가격',
    billed_every: '',
    subs_start: '',
    delivery_schedules: '배송 일정',
    delivery: '배송',
    in_months: '{{value}} 개월 내',
    free_trial_info: {
      heading: '',
      details_text_1: '',
      details_text_2: '~에',
      details_text_3: '모든',
      details_text_4: '시작',
      details_text_between: 'between {{date1}} - {{date2}}',
      every: '마다',
      what_you_get: '받을 상품',
      discount_off_first_delivery: '{{price}} ({{discount}}% discount off first delivery)',
      has_deliver_date: {
        text_1: '구독이',
        text_2: '에',
        text_3: '에 시작합니다'
      },
      not_deliver_date: {
        text_1: '계정으로 신청된 정기구매는',
        text_2: '에',
        text_3: '사이에 시작됩니다'
      },
    },
    collect_my_trial_kit_now: '지금 바로 체험상품 받기',
    discount: '할인',
    subtotal: '소계',
    total_tax: '합계 (세금 포함)',
    promo_code: '프로모션 코드',
    grand_total_tax: '총액 (세금 포함)'
  },

  payment: {
    customer_details: {
      heading: '고객 세부 사항',
      email: {
        label: '이메일',
        place_holder : 'john@email.com',
      },
      name: {
        label: '성명',
        place_holder: '',
        place_holder_firstName: '이름',
        place_holder_lastName: '성'
      },
      phone: {
        label: '연락처',
        place_holder: '01023456789',
        place_holder_sgp: '0102345678',
      },
      privacy_note: '계속 할 경우 , 사용자는 Shaves2U의 \s <a href="https://shaves2u.com/privacy-policy" target="_blank">개인정보 보호정책에 동의합니다</a>',
    },
    // Step address
      					
    address: {
      shipping_heading: '배송지',
      billing_heading: '청구지 주소',
      heading_add: '',
      heading_edit: '',
      email: {
        label: '이메일',
        place_holder : 'john@email.com',
        note_email_exists: '',
        have_an_account: '',
        login_text: '',
        end_text: ''
      },
      name: {
        label: '성명',
        place_holder_firstName: '이름',
        place_holder_lastName: '성'
      },
      address: {
        label: '주소',
        place_holder : '주소',
        place_holder_sgp : 'Unit No./Street/Area'
      },
      phone: {
        label: '전화번호',
        place_holder: '123456789'
      },
      region: {
        label: 'Region',
        defaultOption: 'Choose a region'
      },
      city:{
        label: '군/구/동',
        place_holder: '군/구/동'
      },
      postal_code:{
        label: '우편번호',
        place_holder: '10000',
        place_holder_sgp: '100000',
      },
      state: {
        label: '도',
        defaultOption: '도/시'
      },
        billing: {
        // checkbox_label: '',
        checkbox_label: '청구주소 다르게 설정하기',

      },

      shipping_address_1: 'Address (line 1)'
    },

    payment_details: {
      heading: '지불 세부 사항',
      cash: {
        heading: '현금',
        total: '총 지불액',
      },
      card: {
        heading: '신용카드,',  
        btn_add_new: '새로운 카드 추가',
        name: {
          label: '카드 소유주명',
          place_holder: '',
        },
        number: {
          label: '카드 번호',
          place_holder: ''
        },
        expiry: {
          label: '만료일자',
          place_holder: '월/년'
        },
        code: {
          label: '보안 코드',
          place_holder: '',
          tooltip: '고객님 카드 뒷면에 표시된 숫자의 마지막 3자리입니다'
        }
      },
    },
    roadshow_details: {
      heading: '로드쇼 세부 정보',
    }

  },

  summary: {
    order: {
      heading : '주문 요약',
      order_no: '주문 번호',
      total : '합계',
      date : '날짜',
      delivery: 'Direct Delivery',
      yes: '네',
      no: '아니오',
      status : '상태'

    },
    items: {
      heading: '제품',
      qty : '수량',
      price : '제품 가격'

    },
    customer_details : {
      heading: '고객 세부 사항',
      full_name: '성명',
      email : '이메일',
      phone : '전화번호',
      exist_customer : '기존 고객',
      no : '번호',
      address: '주소'
    }
  },

  order: {
    // user-order-detail.component
    detail: {
      header_history: '주문 히스토리',
      order_id: '주문 번호',
      tax_invoice_id: '세금 계산서 번호',
      total: '합계',
      date: '날짜',
      customer_name: '고객 성명',
      channel_type: '채널 종류',
      location_code: '벤트/로케이션 코드',
      status: '상태',
      payment_type: '지불 방식',
      cash: '현금',
      quantity: '수량',
      basket_size: '체크 아웃 크기',
    },
    // user-order-list.component
    list: {
      header_history: '주문 히스토리',
      date: '날짜',
      order_id: '주문 번호',
      items: '제품',
      price: '가격',
      name: '성명',
      status: '상태',
      total: '합계',
      link_view_details: '세부 사항',
      empty_order: '고객님의 주문 히스토리가 없습니다',
      shop_now: '지금 쇼핑하기',
      txt_item: '제품',
      txt_items: '제품들',
    },
  },
  channel_type: {
    event: '이벤트',
    streets: '스트리트',
    b2b: 'B2B',
    res: '자택 방문',
  }
};
