export const en = {
  menu: {
    home: 'Home',
    logout: 'Logout',
    order_history: 'Sales History',
    change_language: 'Change Language ({{lang}})',
    select_a_category: 'Select a category'
  },
  title: {
    home: 'Home',
    alacartes: 'Ala Carte',
    subscriptions: 'Trial',
    sample_product: 'Trial',
    shopping: 'Shopping Cart',
    checkout: 'Checkout',
    order_confirm: 'Order Confirmed',
    sale_history : 'Sales History',
  },
  product: {
    field: {
      product: 'Product',
      price: 'Price',
      quantity: 'Quantity',
      subtotal: 'Subtotal',
    }    
  },
  trial: {
    pay_as_you_go: 'Pay as you go',
  },
  // message
  msg: {
    validation: {
      required: '{{value}} is a mandatory field',
      character_string: 'Contents for this textbox must have at least one Alphabetic character',
      special_characters: "Can't enter special characters",
      select_required: 'Please select a value for this field.',
      email: 'Please enter a valid email address',
      email_exists: 'This email already exists.',
      number: 'This field only allows numeric digits.',
      alphabet: 'This field only allows alphabets.',
      maxlength: 'This field only allows max {{value}} characters (alphabets only), please revise accordingly.',
      minlength: 'This field only allows min {{value}} characters (alphabets only), please revise accordingly.',
      minlength_spg: 'This field only allows min {{value}} characters, please revise accordingly.',
      maxDigits: 'This field only allows max {{value}} numeric digits, please revise accordingly.',
      minDigits: 'This field can be at least {{value}} numeric digits',
      password_format: 'Password should be at least 6 characters long and include at least one number or symbol.',
      password_format_lenght: 'Be at least 6 characters long.',
      password_format_contain: 'Include at least one number or symbol.',
      password_not_match: "Password confirmation doesn’t match Password",
      old_pw_invalid: 'Old password do not match',
      birthday_invalid: 'Birthday is invalid',
      phone: 'Must be a valid 10 digits phone number',
      valid : 'Please enter a valid {{value}}.',
      valid_word: 'Please enter a valid E-Mail Address, this attempt has been logged',
      phone_sgp: 'Must be a valid 8 digits phone number',
      valid_postcode: 'Must be a valid 6 digits post code'
    },
    field: {
      email: 'email address',
      phone: 'phone number',
      postcode: 'postcode',
    },
    status: {
      canceled: 'Canceled',
      process: 'Processing',
      on_hold: 'On Hold',
      payment_received: 'Payment Received',
      completed: 'Completed',
      pending: 'Pending',
      delivering: 'Delivering',
      loading: 'Loading'
    }
  },

  // btn
  btn : {
    apply: 'APPLY',
    next: 'Next',
    ok : 'Ok',
    select : 'Select',
    detail: 'Detail',
    cancel: 'Cancel',
    submit: 'Submit',
    confirm: 'Confirm'
  },
  // dialog
  dialog: {
    confirm: {
      title: 'Confirm',
      content: 'Are you sure?',
      cancelling_order: 'You are cancelling your order <font color="#fe5000">{{value}}</font>. Are you sure?',
      change_plan: 'You are updating your shave plan from <font color="#fe5000">{{from}}</font> to <font color="#fe5000">{{to}}</font>. Are you sure?',
      change_payment: 'You are updating your shave plan from <font color="#fe5000">{{from}} billing</font> to <font color="#fe5000">{{to}} billing</font>. Are you sure?',
      order_cancelled_succ : 'Order cancellation request was successful',
      notDiscount: 'Please be informed your account is not eligible for Subscription plan 20% off first 12 months promotion.',
      notDiscountForIsTrial: 'Please be informed your account is not eligible for Shave plan Free Trial',
    }
  },

  promo: {
    heading: 'Promo Code',
    place_holder: 'ENTER CODE HERE',
    na : 'N/A'
  },

  price: {
    total: 'Total',
    subTotal: 'Subtotal',
    discount: 'Discount',
    shipping_fee : 'Shipping Cost',
    total_tax : 'Total (Excl. tax)',
    grand_total_tax : 'Grand Total (Incl. tax)',
    grand_total : 'Grand Total',
    free: 'FREE'

  },
  login: {
    badge_id: {
      label: 'Badge ID',
      placeholder: 'Badge ID',
      error_message: {
        empty: 'Badge ID is a mandatory field.',
      }
    },
    ic_number: {
      label: 'IC Number',
      placeholder: 'IC Number',
      error_message: {
        empty: 'IC Number is a mandatory field.',
      }
    },
    channelType: {
      label: 'Channel Type',
      placeholder: 'Channel Type',
      error_message: {
        empty: 'Channel Type is a mandatory field.',
      }
    },
    eventLocationCode: {
      label: 'Event/Location Code',
      placeholder: 'Event/Location Code',
      error_message: {
        empty: 'Event/Location Code is a mandatory field.',
      }
    }
  },
  home: {
    heading: 'Select a category to begin',
    subscription: 'Trial',
    alacarte : 'Product',
  },
    //  shave plan page
  plan: {
    plan_section: {
      select_blade_type: 'Select a Blade type',
      select_shave_plan: 'Select Shave Plan',
      select_plan: 'Select a Blade type',
      select_payment: 'Which Payment Plan Would You Prefer?',
      plan_details : 'What you’ll get',
      subtitle: 'Men’s {{value}}-Blade Cartridges',
      subtitle_Women: 'Women’s {{value}}-Blade Cartridges',
      totalText: 'per annum',
      btn_discount: 'Save',
      note: "We'll send a reminder email before each shipment so you have enough time to modify or cancel your plan.",
      btn_add_to_cart: 'PROCEED TO PAYMENT',
      suggest_payment: 'We accept'
    },
    frequently_asked_questions: 'Frequently Asked Questions',
  },

  review: {
    item_selected: '{{value}} item(s) selected',
    item_price: 'Item Price',
    billed_every: 'billed every',
    subs_start: 'Subscription Starts',
    delivery_schedules: 'Delivery Schedules',
    delivery: 'Delivery',
    in_months: 'In {{value}} months',
    free_trial_info: {
      heading: 'FREE TRIAL',
      details_text_1: 'Your subscription for ',
      details_text_2: ' at ',
      details_text_3: ' every ',
      details_text_4: ' starts ',
      details_text_between: 'between {{date1}} - {{date2}}',
      every: ' Every ',
      what_you_get: 'What you\'ll get',
      discount_off_first_delivery: '{{price}} ({{discount}}% discount off first delivery)',
      has_deliver_date: {
        text_1: '',
        text_2: '',
        text_3: ''
      },
      not_deliver_date: {
        text_1: '',
        text_2: '',
        text_3: ''
      },
    },
    collect_my_trial_kit_now: 'Collect my Trial Kit now!',
    discount: 'Discount',
    subtotal: 'Subtotal',
    total_tax: 'Grand Total (tax incl.)',
    promo_code: 'Promo Code',
    grand_total_tax: 'Grand Total (tax incl.)'
  },

  payment: {
    customer_details: {
      heading: 'Customer Details',
      email: {
        label: 'Email',
        place_holder : 'john@email.com',
      },
      name: {
        label: 'Full name',
        place_holder: '',
        place_holder_firstName: 'First',
        place_holder_lastName: 'Last'
      },
      phone: {
        label: 'Phone',
        place_holder: '123456789',
        place_holder_sgp: '12345678',
      },
      privacy_note: 'By continuing, the user agrees to Shaves2U\'s <a href="https://shaves2u.com/privacy-policy" target="_blank">Privacy Policy</a>',
    },
    // Step address
      					
    address: {
      shipping_heading: 'Shipping Address',
      billing_heading: 'Billing Address',
      heading_add: 'Add New Address',
      heading_edit: 'Edit Address',
      email: {
        label: 'Email',
        place_holder : 'john@email.com',
        note_email_exists: 'Looks like you are an existing Shaves2U customer. Speed up your checkout by logging in via the link above.',
        have_an_account: 'Have an account?',
        login_text: 'Login',
        end_text: 'for speedier checkout.'
      },
      name: {
        label: 'Name',
        place_holder_firstName: 'First',
        place_holder_lastName: 'Last'
      },
      address: {
        label: 'Address',
        place_holder : 'Street/Area',
        place_holder_sgp : 'Unit No./Street/Area'
      },
      phone: {
        label: 'Phone',
        place_holder: '123456789'
      },
      region: {
        label: 'Region',
        defaultOption: 'Choose a region'
      },
      city:{
        label: 'Town/City',
        place_holder: 'Town/City'
      },
      postal_code:{
        label: 'Postal Code',
        place_holder: '10000',
        place_holder_sgp: '100000',
      },
      state: {
        label: 'State',
        defaultOption: 'Choose a state'
      },
        billing: {
        // checkbox_label: 'Make Billing address same as Shipping Address',
        checkbox_label: 'Bill to a different address',

      },

      shipping_address_1: 'Address (line 1)'
    },

    payment_details: {
      heading: 'Payment Details',
      cash: {
        heading: 'Cash',
        total: 'Total payable',
      },
      card: {
        heading: 'Credit Card',  
        btn_add_new: 'Add New card',
        name: {
          label: 'Name on Card',
          place_holder: '',
        },
        number: {
          label: 'Card Number',
          place_holder: ''
        },
        expiry: {
          label: 'Expiry Date',
          place_holder: 'MM/YY'
        },
        code: {
          label: 'Security Code',
          place_holder: '',
          tooltip: 'The last 3 digits displayed on the back of your card.'
        }
      },
    },
    roadshow_details: {
      heading: 'Roadshow Details',
    }

  },

  summary: {
    order: {
      heading : 'Order Summary',
      order_no: 'Order No.',
      total : 'Total',
      date : 'Date',
      delivery: 'Direct Delivery',
      yes: 'Yes',
      no: 'No',
      status : 'Status'

    },
    items: {
      heading: 'Items',
      qty : 'Quantity',
      price : 'Items Price'

    },
    customer_details : {
      heading: 'Customer Details',
      full_name: 'Full Name',
      email : 'Email',
      phone : 'Phone',
      exist_customer : 'Existing Customer',
      no : 'No',
      address: 'Address'
    }
  },

  order: {
    // user-order-detail.component
    detail: {
      header_history: 'Order History',
      order_id: 'Order No.',
      tax_invoice_id: 'Tax Invoice No.',
      total: 'Total',
      date: 'Date',
      customer_name: 'Customer Name',
      channel_type: 'Channel Type',
      location_code: 'Event/Location Code',
      status: 'Status',
      payment_type: 'Payment Type',
      cash: 'Cash',
      quantity: 'Quantity',
      basket_size: 'Checkout size',
    },
    // user-order-list.component
    list: {
      header_history: 'Order History',
      date: 'Date',
      order_id: 'Order No.',
      items: 'Items',
      price: 'Price',
      name: 'Name',
      status: 'Status',
      total: 'Total',
      link_view_details: 'Details',
      empty_order: 'You don\'t have any order history.',
      shop_now: 'Shop now',
      txt_item: 'item',
      txt_items: 'items',
    },
  },
  channel_type: {
    event: 'Event',
    streets: 'Streets',
    b2b: 'B2B',
    res: 'RES',
  }
};
