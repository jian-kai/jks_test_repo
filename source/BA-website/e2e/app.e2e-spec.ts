import { SmartShavesPage } from './app.po';

describe('smart-shaves App', () => {
  let page: SmartShavesPage;

  beforeEach(() => {
    page = new SmartShavesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
