var gulp = require('gulp'),
bourbon = require('node-bourbon'), // add bourbon sass
extender = require('gulp-html-extend');

gulp.task('express', function() {
  var express = require('express');
  var app = express();
  app.use(require('connect-livereload')({port: 35729}));
  app.use(express.static(__dirname+'/build'));
  app.listen(4000, '0.0.0.0');
});

var tinylr;
gulp.task('livereload', function() {
  tinylr = require('tiny-lr')();
    tinylr.listen(35729);
});

function notifyLiveReload(event) {
  var fileName = require('path').relative(__dirname+'build', event.path);

  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}
gulp.task('copy_img', function (){
  gulp.src('edm-source/images/**/*')
      .pipe(gulp.dest('build/images'))
});

gulp.task('extend', function () {
  gulp.src('edm-source/*.html')
    .pipe(extender({annotations:true,verbose:false})) // default options
    .pipe(gulp.dest('build'))
});

//Watch all change in project folder
gulp.task('watch', function() {
  gulp.watch(['edm-source/*.html','edm-source/partials/*.html'], ['extend'])
  gulp.watch('edm-source/*.html', notifyLiveReload);
  gulp.watch('edm-source/**/*.html', notifyLiveReload);
  gulp.watch('/build/*.html', notifyLiveReload);
});

gulp.task('default', ['copy_img','extend','express','livereload','watch'], function() {

});
